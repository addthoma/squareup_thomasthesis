.class public final Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;
.super Lcom/squareup/wire/Message;
.source "CreateAttachmentsResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse$ProtoAdapter_CreateAttachmentsResponse;,
        Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;",
        "Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FAILED_ATTACHMENTS:Ljava/lang/Integer;

.field public static final DEFAULT_SUCCEEDED_ATTACHMENTS:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final attachment_responses:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.AttachmentResponse#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttachmentResponse;",
            ">;"
        }
    .end annotation
.end field

.field public final attachments:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Attachment#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Attachment;",
            ">;"
        }
    .end annotation
.end field

.field public final failed_attachments:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final succeeded_attachments:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse$ProtoAdapter_CreateAttachmentsResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse$ProtoAdapter_CreateAttachmentsResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 27
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->DEFAULT_SUCCEEDED_ATTACHMENTS:Ljava/lang/Integer;

    .line 29
    sput-object v0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->DEFAULT_FAILED_ATTACHMENTS:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/Status;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttachmentResponse;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Attachment;",
            ">;",
            "Lcom/squareup/protos/client/Status;",
            ")V"
        }
    .end annotation

    .line 68
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/Status;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/Status;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttachmentResponse;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Attachment;",
            ">;",
            "Lcom/squareup/protos/client/Status;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 74
    sget-object v0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 75
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->succeeded_attachments:Ljava/lang/Integer;

    .line 76
    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->failed_attachments:Ljava/lang/Integer;

    const-string p1, "attachment_responses"

    .line 77
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->attachment_responses:Ljava/util/List;

    const-string p1, "attachments"

    .line 78
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->attachments:Ljava/util/List;

    .line 79
    iput-object p5, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->status:Lcom/squareup/protos/client/Status;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 97
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 98
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;

    .line 99
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->succeeded_attachments:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->succeeded_attachments:Ljava/lang/Integer;

    .line 100
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->failed_attachments:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->failed_attachments:Ljava/lang/Integer;

    .line 101
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->attachment_responses:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->attachment_responses:Ljava/util/List;

    .line 102
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->attachments:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->attachments:Ljava/util/List;

    .line 103
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->status:Lcom/squareup/protos/client/Status;

    .line 104
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 109
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 111
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->succeeded_attachments:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->failed_attachments:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->attachment_responses:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->attachments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 117
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse$Builder;
    .locals 2

    .line 84
    new-instance v0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse$Builder;-><init>()V

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->succeeded_attachments:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse$Builder;->succeeded_attachments:Ljava/lang/Integer;

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->failed_attachments:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse$Builder;->failed_attachments:Ljava/lang/Integer;

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->attachment_responses:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse$Builder;->attachment_responses:Ljava/util/List;

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->attachments:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse$Builder;->attachments:Ljava/util/List;

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 90
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->newBuilder()Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->succeeded_attachments:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", succeeded_attachments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->succeeded_attachments:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->failed_attachments:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", failed_attachments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->failed_attachments:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 127
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->attachment_responses:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", attachment_responses="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->attachment_responses:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 128
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->attachments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", attachments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->attachments:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 129
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_4

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/CreateAttachmentsResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CreateAttachmentsResponse{"

    .line 130
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
