.class public final Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StartTimecardRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/StartTimecardRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/StartTimecardRequest;",
        "Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public clockin_unit_token:Ljava/lang/String;

.field public employee_token:Ljava/lang/String;

.field public job_token:Ljava/lang/String;

.field public use_job_token:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 134
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/timecards/StartTimecardRequest;
    .locals 7

    .line 166
    new-instance v6, Lcom/squareup/protos/client/timecards/StartTimecardRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->employee_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->clockin_unit_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->use_job_token:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->job_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/timecards/StartTimecardRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 125
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->build()Lcom/squareup/protos/client/timecards/StartTimecardRequest;

    move-result-object v0

    return-object v0
.end method

.method public clockin_unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->clockin_unit_token:Ljava/lang/String;

    return-object p0
.end method

.method public employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->employee_token:Ljava/lang/String;

    return-object p0
.end method

.method public job_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->job_token:Ljava/lang/String;

    return-object p0
.end method

.method public use_job_token(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->use_job_token:Ljava/lang/Boolean;

    return-object p0
.end method
