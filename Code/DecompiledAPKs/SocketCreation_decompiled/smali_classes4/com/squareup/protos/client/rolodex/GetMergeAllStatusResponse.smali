.class public final Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;
.super Lcom/squareup/wire/Message;
.source "GetMergeAllStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$ProtoAdapter_GetMergeAllStatusResponse;,
        Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;",
        "Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_COMPLETE:Ljava/lang/Boolean;

.field public static final DEFAULT_TOTAL_DUPLICATE_COUNT:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final complete:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final total_duplicate_count:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 23
    new-instance v0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$ProtoAdapter_GetMergeAllStatusResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$ProtoAdapter_GetMergeAllStatusResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 27
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->DEFAULT_COMPLETE:Ljava/lang/Boolean;

    .line 29
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->DEFAULT_TOTAL_DUPLICATE_COUNT:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Ljava/lang/Boolean;Ljava/lang/Integer;)V
    .locals 1

    .line 50
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/lang/Boolean;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Ljava/lang/Boolean;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 55
    sget-object v0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 56
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->status:Lcom/squareup/protos/client/Status;

    .line 57
    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->complete:Ljava/lang/Boolean;

    .line 58
    iput-object p3, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->total_duplicate_count:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 74
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 75
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;

    .line 76
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->status:Lcom/squareup/protos/client/Status;

    .line 77
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->complete:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->complete:Ljava/lang/Boolean;

    .line 78
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->total_duplicate_count:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->total_duplicate_count:Ljava/lang/Integer;

    .line 79
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 84
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->complete:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->total_duplicate_count:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 90
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$Builder;
    .locals 2

    .line 63
    new-instance v0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$Builder;-><init>()V

    .line 64
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 65
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->complete:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$Builder;->complete:Ljava/lang/Boolean;

    .line 66
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->total_duplicate_count:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$Builder;->total_duplicate_count:Ljava/lang/Integer;

    .line 67
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->newBuilder()Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 99
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->complete:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", complete="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->complete:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 100
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->total_duplicate_count:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", total_duplicate_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;->total_duplicate_count:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetMergeAllStatusResponse{"

    .line 101
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
