.class public final Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DiagnosticReportRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;",
        "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public description:Ljava/lang/String;

.field public destinations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Destination;",
            ">;"
        }
    .end annotation
.end field

.field public device_metadata:Lcom/squareup/protos/client/tarkin/SquidProductManifest;

.field public links:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;",
            ">;"
        }
    .end annotation
.end field

.field public num_xmit_retries:Ljava/lang/Integer;

.field public primary_uuid:Ljava/lang/String;

.field public report_generated_localtime_sec:Ljava/lang/Long;

.field public report_triggered_localtime_sec:Ljava/lang/Long;

.field public reporter:Ljava/lang/String;

.field public source:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public uuid:Ljava/lang/String;

.field public zip:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 305
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 306
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->links:Ljava/util/List;

    .line 307
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->destinations:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;
    .locals 17

    move-object/from16 v0, p0

    .line 418
    new-instance v16, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;

    iget-object v2, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->uuid:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->primary_uuid:Ljava/lang/String;

    iget-object v4, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->device_metadata:Lcom/squareup/protos/client/tarkin/SquidProductManifest;

    iget-object v5, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->source:Ljava/lang/String;

    iget-object v6, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->title:Ljava/lang/String;

    iget-object v7, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->description:Ljava/lang/String;

    iget-object v8, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->num_xmit_retries:Ljava/lang/Integer;

    iget-object v9, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->report_triggered_localtime_sec:Ljava/lang/Long;

    iget-object v10, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->report_generated_localtime_sec:Ljava/lang/Long;

    iget-object v11, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->zip:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    iget-object v12, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->links:Ljava/util/List;

    iget-object v13, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->destinations:Ljava/util/List;

    iget-object v14, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->reporter:Ljava/lang/String;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v15

    move-object/from16 v1, v16

    invoke-direct/range {v1 .. v15}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/tarkin/SquidProductManifest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v16
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 278
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->build()Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;

    move-result-object v0

    return-object v0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;
    .locals 0

    .line 354
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public destinations(Ljava/util/List;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Destination;",
            ">;)",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;"
        }
    .end annotation

    .line 403
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 404
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->destinations:Ljava/util/List;

    return-object p0
.end method

.method public device_metadata(Lcom/squareup/protos/client/tarkin/SquidProductManifest;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;
    .locals 0

    .line 330
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->device_metadata:Lcom/squareup/protos/client/tarkin/SquidProductManifest;

    return-object p0
.end method

.method public links(Ljava/util/List;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;",
            ">;)",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;"
        }
    .end annotation

    .line 394
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 395
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->links:Ljava/util/List;

    return-object p0
.end method

.method public num_xmit_retries(Ljava/lang/Integer;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;
    .locals 0

    .line 362
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->num_xmit_retries:Ljava/lang/Integer;

    return-object p0
.end method

.method public primary_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;
    .locals 0

    .line 322
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->primary_uuid:Ljava/lang/String;

    return-object p0
.end method

.method public report_generated_localtime_sec(Ljava/lang/Long;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;
    .locals 0

    .line 378
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->report_generated_localtime_sec:Ljava/lang/Long;

    return-object p0
.end method

.method public report_triggered_localtime_sec(Ljava/lang/Long;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;
    .locals 0

    .line 370
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->report_triggered_localtime_sec:Ljava/lang/Long;

    return-object p0
.end method

.method public reporter(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;
    .locals 0

    .line 412
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->reporter:Ljava/lang/String;

    return-object p0
.end method

.method public source(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;
    .locals 0

    .line 338
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->source:Ljava/lang/String;

    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;
    .locals 0

    .line 346
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->title:Ljava/lang/String;

    return-object p0
.end method

.method public uuid(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;
    .locals 0

    .line 314
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->uuid:Ljava/lang/String;

    return-object p0
.end method

.method public zip(Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;
    .locals 0

    .line 386
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->zip:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    return-object p0
.end method
