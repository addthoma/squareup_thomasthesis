.class public final Lcom/squareup/protos/client/bills/CompleteBillRequest;
.super Lcom/squareup/wire/Message;
.source "CompleteBillRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CompleteBillRequest$ProtoAdapter_CompleteBillRequest;,
        Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;,
        Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/CompleteBillRequest;",
        "Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CompleteBillRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_AMENDMENT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_COMPLETE_BILL_TYPE:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

.field public static final DEFAULT_IS_AMENDABLE:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final amendment_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final bill_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final cart:Lcom/squareup/protos/client/bills/Cart;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final complete_bill_type:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CompleteBillRequest$CompleteBillType#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final dates:Lcom/squareup/protos/client/bills/Bill$Dates;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Bill$Dates#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final is_amendable:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field

.field public final merchant:Lcom/squareup/protos/client/Merchant;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Merchant#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final removed_amending_tender_ids:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xa
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;"
        }
    .end annotation
.end field

.field public final square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.SquareProductAttributes#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final tender:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CompleteTender#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/CompleteTender;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$ProtoAdapter_CompleteBillRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CompleteBillRequest$ProtoAdapter_CompleteBillRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->DEFAULT_COMPLETE_BILL_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    sput-object v0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->DEFAULT_COMPLETE_BILL_TYPE:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    const/4 v0, 0x0

    .line 34
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->DEFAULT_IS_AMENDABLE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/Merchant;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/bills/Bill$Dates;Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;Lcom/squareup/protos/client/bills/SquareProductAttributes;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/protos/client/Merchant;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/CompleteTender;",
            ">;",
            "Lcom/squareup/protos/client/bills/Cart;",
            "Lcom/squareup/protos/client/bills/Bill$Dates;",
            "Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;",
            "Lcom/squareup/protos/client/bills/SquareProductAttributes;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;)V"
        }
    .end annotation

    .line 139
    sget-object v11, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/client/bills/CompleteBillRequest;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/Merchant;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/bills/Bill$Dates;Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;Lcom/squareup/protos/client/bills/SquareProductAttributes;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/Merchant;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/bills/Bill$Dates;Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;Lcom/squareup/protos/client/bills/SquareProductAttributes;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/protos/client/Merchant;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/CompleteTender;",
            ">;",
            "Lcom/squareup/protos/client/bills/Cart;",
            "Lcom/squareup/protos/client/bills/Bill$Dates;",
            "Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;",
            "Lcom/squareup/protos/client/bills/SquareProductAttributes;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 146
    sget-object v0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p11}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 148
    iput-object p2, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    const-string p1, "tender"

    .line 149
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->tender:Ljava/util/List;

    .line 150
    iput-object p4, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 151
    iput-object p5, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    .line 152
    iput-object p6, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->complete_bill_type:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    .line 153
    iput-object p7, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    .line 154
    iput-object p8, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->amendment_token:Ljava/lang/String;

    .line 155
    iput-object p9, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->is_amendable:Ljava/lang/Boolean;

    const-string p1, "removed_amending_tender_ids"

    .line 156
    invoke-static {p1, p10}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->removed_amending_tender_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 179
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 180
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;

    .line 181
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CompleteBillRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CompleteBillRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 182
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    .line 183
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->tender:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->tender:Ljava/util/List;

    .line 184
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 185
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    .line 186
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->complete_bill_type:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->complete_bill_type:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    .line 187
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    .line 188
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->amendment_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->amendment_token:Ljava/lang/String;

    .line 189
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->is_amendable:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->is_amendable:Ljava/lang/Boolean;

    .line 190
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->removed_amending_tender_ids:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->removed_amending_tender_ids:Ljava/util/List;

    .line 191
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 196
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_8

    .line 198
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CompleteBillRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 199
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 200
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/Merchant;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 201
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->tender:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 202
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 203
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Bill$Dates;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 204
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->complete_bill_type:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 205
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/SquareProductAttributes;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 206
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->amendment_token:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 207
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->is_amendable:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->removed_amending_tender_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 209
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;
    .locals 2

    .line 161
    new-instance v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;-><init>()V

    .line 162
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 163
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->merchant:Lcom/squareup/protos/client/Merchant;

    .line 164
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->tender:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->tender:Ljava/util/List;

    .line 165
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 166
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    .line 167
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->complete_bill_type:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->complete_bill_type:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    .line 168
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    .line 169
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->amendment_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->amendment_token:Ljava/lang/String;

    .line 170
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->is_amendable:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->is_amendable:Ljava/lang/Boolean;

    .line 171
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->removed_amending_tender_ids:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->removed_amending_tender_ids:Ljava/util/List;

    .line 172
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CompleteBillRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CompleteBillRequest;->newBuilder()Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 216
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 217
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", bill_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 218
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    if-eqz v1, :cond_1

    const-string v1, ", merchant="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 219
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->tender:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->tender:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 220
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v1, :cond_3

    const-string v1, ", cart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 221
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    if-eqz v1, :cond_4

    const-string v1, ", dates="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 222
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->complete_bill_type:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    if-eqz v1, :cond_5

    const-string v1, ", complete_bill_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->complete_bill_type:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 223
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    if-eqz v1, :cond_6

    const-string v1, ", square_product_attributes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 224
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->amendment_token:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", amendment_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->amendment_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->is_amendable:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", is_amendable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->is_amendable:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 226
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->removed_amending_tender_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, ", removed_amending_tender_ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->removed_amending_tender_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_9
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CompleteBillRequest{"

    .line 227
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
