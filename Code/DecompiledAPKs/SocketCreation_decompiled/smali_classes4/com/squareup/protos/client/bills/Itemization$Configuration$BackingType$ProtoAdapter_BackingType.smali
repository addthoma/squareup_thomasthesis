.class final Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType$ProtoAdapter_BackingType;
.super Lcom/squareup/wire/EnumAdapter;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_BackingType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 1772
    const-class v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;
    .locals 0

    .line 1777
    invoke-static {p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->fromValue(I)Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 1770
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType$ProtoAdapter_BackingType;->fromValue(I)Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    move-result-object p1

    return-object p1
.end method
