.class final Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail$ProtoAdapter_LinkCardAuthenticationDetail;
.super Lcom/squareup/wire/ProtoAdapter;
.source "LinkCardRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_LinkCardAuthenticationDetail"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 296
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 314
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail$Builder;-><init>()V

    .line 315
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 316
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 320
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 318
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail$Builder;->password(Ljava/lang/String;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail$Builder;

    goto :goto_0

    .line 324
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 325
    invoke-virtual {v0}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail$Builder;->build()Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 294
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail$ProtoAdapter_LinkCardAuthenticationDetail;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 308
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;->password:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 309
    invoke-virtual {p2}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 294
    check-cast p2, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail$ProtoAdapter_LinkCardAuthenticationDetail;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;)I
    .locals 3

    .line 301
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;->password:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 302
    invoke-virtual {p1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 294
    check-cast p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail$ProtoAdapter_LinkCardAuthenticationDetail;->encodedSize(Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;
    .locals 1

    .line 330
    invoke-virtual {p1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;->newBuilder()Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 331
    iput-object v0, p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail$Builder;->password:Ljava/lang/String;

    .line 332
    invoke-virtual {p1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 333
    invoke-virtual {p1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail$Builder;->build()Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 294
    check-cast p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail$ProtoAdapter_LinkCardAuthenticationDetail;->redact(Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    move-result-object p1

    return-object p1
.end method
