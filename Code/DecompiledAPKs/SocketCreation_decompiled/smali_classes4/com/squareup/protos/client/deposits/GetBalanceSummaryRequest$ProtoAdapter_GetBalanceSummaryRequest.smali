.class final Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest$ProtoAdapter_GetBalanceSummaryRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetBalanceSummaryRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetBalanceSummaryRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 106
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 123
    new-instance v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest$Builder;-><init>()V

    .line 124
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 125
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 129
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 127
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest$Builder;->include_recent_activity(Ljava/lang/Boolean;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest$Builder;

    goto :goto_0

    .line 133
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 134
    invoke-virtual {v0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest$Builder;->build()Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 104
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest$ProtoAdapter_GetBalanceSummaryRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 117
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;->include_recent_activity:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 118
    invoke-virtual {p2}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 104
    check-cast p2, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest$ProtoAdapter_GetBalanceSummaryRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;)I
    .locals 3

    .line 111
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;->include_recent_activity:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 112
    invoke-virtual {p1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 104
    check-cast p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest$ProtoAdapter_GetBalanceSummaryRequest;->encodedSize(Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;
    .locals 0

    .line 139
    invoke-virtual {p1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;->newBuilder()Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest$Builder;

    move-result-object p1

    .line 140
    invoke-virtual {p1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 141
    invoke-virtual {p1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest$Builder;->build()Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 104
    check-cast p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest$ProtoAdapter_GetBalanceSummaryRequest;->redact(Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;

    move-result-object p1

    return-object p1
.end method
