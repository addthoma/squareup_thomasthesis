.class public final Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AttributeSchema.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public enum_allow_multiple:Ljava/lang/Boolean;

.field public enum_values:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public is_configurable:Ljava/lang/Boolean;

.field public is_default:Ljava/lang/Boolean;

.field public is_read_only:Ljava/lang/Boolean;

.field public is_visible_in_profile:Ljava/lang/Boolean;

.field public key:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 422
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 423
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->enum_values:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;
    .locals 12

    .line 497
    new-instance v11, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    iget-object v4, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->is_visible_in_profile:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->is_read_only:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->enum_allow_multiple:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->enum_values:Ljava/util/List;

    iget-object v8, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->is_default:Ljava/lang/Boolean;

    iget-object v9, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->is_configurable:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 403
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    move-result-object v0

    return-object v0
.end method

.method public enum_allow_multiple(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;
    .locals 0

    .line 467
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->enum_allow_multiple:Ljava/lang/Boolean;

    return-object p0
.end method

.method public enum_values(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;"
        }
    .end annotation

    .line 472
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 473
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->enum_values:Ljava/util/List;

    return-object p0
.end method

.method public is_configurable(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;
    .locals 0

    .line 491
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->is_configurable:Ljava/lang/Boolean;

    return-object p0
.end method

.method public is_default(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;
    .locals 0

    .line 482
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->is_default:Ljava/lang/Boolean;

    return-object p0
.end method

.method public is_read_only(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;
    .locals 0

    .line 459
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->is_read_only:Ljava/lang/Boolean;

    return-object p0
.end method

.method public is_visible_in_profile(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;
    .locals 0

    .line 451
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->is_visible_in_profile:Ljava/lang/Boolean;

    return-object p0
.end method

.method public key(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;
    .locals 0

    .line 430
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->key:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;
    .locals 0

    .line 438
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;)Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;
    .locals 0

    .line 443
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    return-object p0
.end method
