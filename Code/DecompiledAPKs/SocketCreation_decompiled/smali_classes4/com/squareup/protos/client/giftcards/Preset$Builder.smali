.class public final Lcom/squareup/protos/client/giftcards/Preset$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Preset.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/giftcards/Preset;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/giftcards/Preset;",
        "Lcom/squareup/protos/client/giftcards/Preset$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allow_custom_value:Ljava/lang/Boolean;

.field public preset_values:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 104
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 105
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/giftcards/Preset$Builder;->preset_values:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public allow_custom_value(Ljava/lang/Boolean;)Lcom/squareup/protos/client/giftcards/Preset$Builder;
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/Preset$Builder;->allow_custom_value:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/giftcards/Preset;
    .locals 4

    .line 129
    new-instance v0, Lcom/squareup/protos/client/giftcards/Preset;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Preset$Builder;->preset_values:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/giftcards/Preset$Builder;->allow_custom_value:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/giftcards/Preset;-><init>(Ljava/util/List;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/Preset$Builder;->build()Lcom/squareup/protos/client/giftcards/Preset;

    move-result-object v0

    return-object v0
.end method

.method public preset_values(Ljava/util/List;)Lcom/squareup/protos/client/giftcards/Preset$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Lcom/squareup/protos/client/giftcards/Preset$Builder;"
        }
    .end annotation

    .line 114
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/Preset$Builder;->preset_values:Ljava/util/List;

    return-object p0
.end method
