.class public final Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "EmployeeJobInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;",
        "Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public job_title:Ljava/lang/String;

.field public job_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 104
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;
    .locals 4

    .line 122
    new-instance v0, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    iget-object v1, p0, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo$Builder;->job_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo$Builder;->job_title:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo$Builder;->build()Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    move-result-object v0

    return-object v0
.end method

.method public job_title(Ljava/lang/String;)Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo$Builder;
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo$Builder;->job_title:Ljava/lang/String;

    return-object p0
.end method

.method public job_token(Ljava/lang/String;)Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo$Builder;->job_token:Ljava/lang/String;

    return-object p0
.end method
