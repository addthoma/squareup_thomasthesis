.class public final Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;
.super Lcom/squareup/wire/Message;
.source "AddedTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddedTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CartPunchStatus"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$ProtoAdapter_CartPunchStatus;,
        Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;",
        "Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CURRENT_PUNCH_COUNT:Ljava/lang/Long;

.field public static final DEFAULT_EARNS_PUNCH:Ljava/lang/Boolean;

.field public static final DEFAULT_REQUIRED_PUNCH_COUNT:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final current_punch_count:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x2
    .end annotation
.end field

.field public final earns_punch:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final required_punch_count:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 780
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$ProtoAdapter_CartPunchStatus;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$ProtoAdapter_CartPunchStatus;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 784
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->DEFAULT_EARNS_PUNCH:Ljava/lang/Boolean;

    const-wide/16 v0, 0x0

    .line 786
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->DEFAULT_CURRENT_PUNCH_COUNT:Ljava/lang/Long;

    .line 788
    sput-object v0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->DEFAULT_REQUIRED_PUNCH_COUNT:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 1

    .line 819
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;-><init>(Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1

    .line 824
    sget-object v0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 825
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->earns_punch:Ljava/lang/Boolean;

    .line 826
    iput-object p2, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->current_punch_count:Ljava/lang/Long;

    .line 827
    iput-object p3, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->required_punch_count:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 843
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 844
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;

    .line 845
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->earns_punch:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->earns_punch:Ljava/lang/Boolean;

    .line 846
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->current_punch_count:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->current_punch_count:Ljava/lang/Long;

    .line 847
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->required_punch_count:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->required_punch_count:Ljava/lang/Long;

    .line 848
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 853
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 855
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 856
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->earns_punch:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 857
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->current_punch_count:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 858
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->required_punch_count:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 859
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$Builder;
    .locals 2

    .line 832
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$Builder;-><init>()V

    .line 833
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->earns_punch:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$Builder;->earns_punch:Ljava/lang/Boolean;

    .line 834
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->current_punch_count:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$Builder;->current_punch_count:Ljava/lang/Long;

    .line 835
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->required_punch_count:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$Builder;->required_punch_count:Ljava/lang/Long;

    .line 836
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 779
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->newBuilder()Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 866
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 867
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->earns_punch:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", earns_punch="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->earns_punch:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 868
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->current_punch_count:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", current_punch_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->current_punch_count:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 869
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->required_punch_count:Ljava/lang/Long;

    if-eqz v1, :cond_2

    const-string v1, ", required_punch_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->required_punch_count:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CartPunchStatus{"

    .line 870
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
