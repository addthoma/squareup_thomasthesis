.class public final enum Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;
.super Ljava/lang/Enum;
.source "CardTender.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardTender$Emv;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CardholderVerificationMethod"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod$ProtoAdapter_CardholderVerificationMethod;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DEFAULT_CVM_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

.field public static final enum NO_CVM:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

.field public static final enum ON_DEVICE:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

.field public static final enum PIN:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

.field public static final enum PIN_AND_SIGNATURE:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

.field public static final enum SIGNATURE:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 1773
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    const/4 v1, 0x0

    const-string v2, "DEFAULT_CVM_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->DEFAULT_CVM_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    .line 1775
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    const/4 v2, 0x1

    const-string v3, "NO_CVM"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->NO_CVM:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    .line 1777
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    const/4 v3, 0x2

    const-string v4, "SIGNATURE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->SIGNATURE:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    .line 1779
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    const/4 v4, 0x3

    const-string v5, "PIN"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->PIN:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    .line 1781
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    const/4 v5, 0x4

    const-string v6, "PIN_AND_SIGNATURE"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->PIN_AND_SIGNATURE:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    .line 1783
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    const/4 v6, 0x5

    const-string v7, "ON_DEVICE"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->ON_DEVICE:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    .line 1772
    sget-object v7, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->DEFAULT_CVM_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->NO_CVM:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->SIGNATURE:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->PIN:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->PIN_AND_SIGNATURE:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->ON_DEVICE:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->$VALUES:[Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    .line 1785
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod$ProtoAdapter_CardholderVerificationMethod;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod$ProtoAdapter_CardholderVerificationMethod;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1789
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1790
    iput p3, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 1803
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->ON_DEVICE:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    return-object p0

    .line 1802
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->PIN_AND_SIGNATURE:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    return-object p0

    .line 1801
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->PIN:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    return-object p0

    .line 1800
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->SIGNATURE:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    return-object p0

    .line 1799
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->NO_CVM:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    return-object p0

    .line 1798
    :cond_5
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->DEFAULT_CVM_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;
    .locals 1

    .line 1772
    const-class v0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;
    .locals 1

    .line 1772
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->$VALUES:[Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 1810
    iget v0, p0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->value:I

    return v0
.end method
