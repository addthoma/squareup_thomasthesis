.class public final Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StartTimecardBreakRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest;",
        "Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public break_definition_token:Ljava/lang/String;

.field public timecard_token:Ljava/lang/String;

.field public version_number:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 125
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public break_definition_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest$Builder;
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest$Builder;->break_definition_token:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest;
    .locals 5

    .line 158
    new-instance v0, Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest$Builder;->timecard_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest$Builder;->break_definition_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest$Builder;->version_number:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest$Builder;->build()Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest;

    move-result-object v0

    return-object v0
.end method

.method public timecard_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest$Builder;->timecard_token:Ljava/lang/String;

    return-object p0
.end method

.method public version_number(Ljava/lang/Long;)Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest$Builder;
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakRequest$Builder;->version_number:Ljava/lang/Long;

    return-object p0
.end method
