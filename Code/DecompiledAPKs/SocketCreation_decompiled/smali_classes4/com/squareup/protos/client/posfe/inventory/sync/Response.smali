.class public final Lcom/squareup/protos/client/posfe/inventory/sync/Response;
.super Lcom/squareup/wire/Message;
.source "Response.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/posfe/inventory/sync/Response$ProtoAdapter_Response;,
        Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/posfe/inventory/sync/Response;",
        "Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/posfe/inventory/sync/Response;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ERROR:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

.field private static final serialVersionUID:J


# instance fields
.field public final error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.posfe.inventory.sync.SyncError#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final get_response:Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.posfe.inventory.sync.GetResponse#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final put_response:Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.posfe.inventory.sync.PutResponse#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/Response$ProtoAdapter_Response;

    invoke-direct {v0}, Lcom/squareup/protos/client/posfe/inventory/sync/Response$ProtoAdapter_Response;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 24
    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;->DO_NOT_USE_SYNC_ERROR:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    sput-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->DEFAULT_ERROR:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;)V
    .locals 1

    .line 45
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/posfe/inventory/sync/Response;-><init>(Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;Lokio/ByteString;)V
    .locals 1

    .line 50
    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 51
    invoke-static {p2, p3}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p4

    const/4 v0, 0x1

    if-gt p4, v0, :cond_0

    .line 54
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    .line 55
    iput-object p2, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->get_response:Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;

    .line 56
    iput-object p3, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->put_response:Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;

    return-void

    .line 52
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of get_response, put_response may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 72
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/posfe/inventory/sync/Response;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 73
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/posfe/inventory/sync/Response;

    .line 74
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    iget-object v3, p1, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    .line 75
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->get_response:Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;

    iget-object v3, p1, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->get_response:Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;

    .line 76
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->put_response:Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->put_response:Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;

    .line 77
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 82
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->get_response:Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->put_response:Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 88
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;
    .locals 2

    .line 61
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;-><init>()V

    .line 62
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    iput-object v1, v0, Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;->error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    .line 63
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->get_response:Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;

    iput-object v1, v0, Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;->get_response:Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;

    .line 64
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->put_response:Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;

    iput-object v1, v0, Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;->put_response:Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;

    .line 65
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->newBuilder()Lcom/squareup/protos/client/posfe/inventory/sync/Response$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    if-eqz v1, :cond_0

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 97
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->get_response:Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;

    if-eqz v1, :cond_1

    const-string v1, ", get_response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->get_response:Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 98
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->put_response:Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;

    if-eqz v1, :cond_2

    const-string v1, ", put_response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Response;->put_response:Lcom/squareup/protos/client/posfe/inventory/sync/PutResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Response{"

    .line 99
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
