.class public final Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "QueueBillResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult;",
        "Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public id:Lcom/squareup/protos/client/IdPair;

.field public status:Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 102
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult;
    .locals 4

    .line 124
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Builder;->status:Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Status;

    iget-object v2, p0, Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Builder;->id:Lcom/squareup/protos/client/IdPair;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult;-><init>(Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Status;Lcom/squareup/protos/client/IdPair;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Builder;->build()Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult;

    move-result-object v0

    return-object v0
.end method

.method public id(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Builder;
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Builder;->id:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Status;)Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Builder;
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Builder;->status:Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Status;

    return-object p0
.end method
