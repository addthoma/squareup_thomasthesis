.class public final Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListTendersAwaitingMerchantTipResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;",
        "Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error_message:Ljava/lang/String;

.field public error_title:Ljava/lang/String;

.field public next_request_backoff_seconds:Ljava/lang/Integer;

.field public success:Ljava/lang/Boolean;

.field public tender_awaiting_merchant_tip_list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;",
            ">;"
        }
    .end annotation
.end field

.field public tender_with_bill_id:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/TenderWithBillId;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 197
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 198
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->tender_awaiting_merchant_tip_list:Ljava/util/List;

    .line 199
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->tender_with_bill_id:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;
    .locals 9

    .line 269
    new-instance v8, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->success:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->error_message:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->error_title:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->tender_awaiting_merchant_tip_list:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->next_request_backoff_seconds:Ljava/lang/Integer;

    iget-object v6, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->tender_with_bill_id:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/util/List;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 184
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->build()Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;

    move-result-object v0

    return-object v0
.end method

.method public error_message(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->error_message:Ljava/lang/String;

    return-object p0
.end method

.method public error_title(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->error_title:Ljava/lang/String;

    return-object p0
.end method

.method public next_request_backoff_seconds(Ljava/lang/Integer;)Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;
    .locals 0

    .line 249
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->next_request_backoff_seconds:Ljava/lang/Integer;

    return-object p0
.end method

.method public success(Ljava/lang/Boolean;)Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;
    .locals 0

    .line 207
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method public tender_awaiting_merchant_tip_list(Ljava/util/List;)Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;",
            ">;)",
            "Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 238
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 239
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->tender_awaiting_merchant_tip_list:Ljava/util/List;

    return-object p0
.end method

.method public tender_with_bill_id(Ljava/util/List;)Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/TenderWithBillId;",
            ">;)",
            "Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;"
        }
    .end annotation

    .line 262
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 263
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->tender_with_bill_id:Ljava/util/List;

    return-object p0
.end method
