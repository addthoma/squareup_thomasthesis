.class final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$ProtoAdapter_FulfillmentCreationDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_FulfillmentCreationDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 7559
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7589
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;-><init>()V

    .line 7590
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 7591
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 7615
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 7613
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->pickup_details(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;

    goto :goto_0

    .line 7612
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->note(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;

    goto :goto_0

    .line 7611
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->location_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;

    goto :goto_0

    .line 7605
    :pswitch_3
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->state(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 7607
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 7597
    :pswitch_4
    :try_start_1
    sget-object v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->type(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 7599
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 7594
    :pswitch_5
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->items:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentItems;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 7593
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->recipient(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;

    goto :goto_0

    .line 7619
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 7620
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7557
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$ProtoAdapter_FulfillmentCreationDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7577
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->recipient:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7578
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentItems;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->items:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7579
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7580
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7581
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->location_id:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7582
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->note:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7583
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->pickup_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7584
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7557
    check-cast p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$ProtoAdapter_FulfillmentCreationDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;)I
    .locals 4

    .line 7564
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->recipient:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentItems;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 7565
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->items:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;

    const/4 v3, 0x3

    .line 7566
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    const/4 v3, 0x4

    .line 7567
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->location_id:Ljava/lang/String;

    const/4 v3, 0x5

    .line 7568
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->note:Ljava/lang/String;

    const/4 v3, 0x6

    .line 7569
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->pickup_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;

    const/4 v3, 0x7

    .line 7570
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7571
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 7557
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$ProtoAdapter_FulfillmentCreationDetails;->encodedSize(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;
    .locals 2

    .line 7625
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;

    move-result-object p1

    .line 7626
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->recipient:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->recipient:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->recipient:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;

    .line 7627
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->items:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentItems;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 7628
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->pickup_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->pickup_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->pickup_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;

    .line 7629
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 7630
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 7557
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$ProtoAdapter_FulfillmentCreationDetails;->redact(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;

    move-result-object p1

    return-object p1
.end method
