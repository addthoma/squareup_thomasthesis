.class public final Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;
.super Lcom/squareup/wire/Message;
.source "LoyaltyOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/LoyaltyOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoyaltyAccountMappingOptions"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions$ProtoAdapter_LoyaltyAccountMappingOptions;,
        Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;",
        "Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_INCLUDE_RAW_PHONE:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final include_raw_phone:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 321
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions$ProtoAdapter_LoyaltyAccountMappingOptions;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions$ProtoAdapter_LoyaltyAccountMappingOptions;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 325
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;->DEFAULT_INCLUDE_RAW_PHONE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;)V
    .locals 1

    .line 339
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 343
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 344
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;->include_raw_phone:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 358
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 359
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;

    .line 360
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;->include_raw_phone:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;->include_raw_phone:Ljava/lang/Boolean;

    .line 361
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 366
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 368
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 369
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;->include_raw_phone:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 370
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions$Builder;
    .locals 2

    .line 349
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions$Builder;-><init>()V

    .line 350
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;->include_raw_phone:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions$Builder;->include_raw_phone:Ljava/lang/Boolean;

    .line 351
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 320
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;->newBuilder()Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 377
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 378
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;->include_raw_phone:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", include_raw_phone="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;->include_raw_phone:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "LoyaltyAccountMappingOptions{"

    .line 379
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
