.class public final Lcom/squareup/protos/client/bills/CardTender$Emv;
.super Lcom/squareup/wire/Message;
.source "CardTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Emv"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CardTender$Emv$ProtoAdapter_Emv;,
        Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;,
        Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;,
        Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/CardTender$Emv;",
        "Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CardTender$Emv;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BUYER_SELECTED_ACCOUNT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_IS_EMV_CAPABLE_READER_PRESENT:Ljava/lang/Boolean;

.field public static final DEFAULT_READ_ONLY_APPLICATION_PREFERRED_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_READ_ONLY_CARDHOLDER_VERIFICATION_METHOD_USED:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

.field public static final DEFAULT_READ_ONLY_CHIP_CARD_APPLICATION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_READ_ONLY_CHIP_CARD_PREFERRED_LANGUAGE_CODE:Ljava/lang/String; = ""

.field public static final DEFAULT_READ_ONLY_ENCRYPTED_EMV_DATA:Lokio/ByteString;

.field public static final DEFAULT_READ_ONLY_PLAINTEXT_EMV_DATA:Lokio/ByteString;

.field public static final DEFAULT_WRITE_ONLY_HARDWARE_SERIAL_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_WRITE_ONLY_PROCESSING_TYPE:Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;

.field public static final DEFAULT_WRITE_ONLY_READER_FIRMWARE_VERSION:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final buyer_selected_account_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xc
    .end annotation
.end field

.field public final is_emv_capable_reader_present:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final read_only_application_preferred_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final read_only_cardholder_verification_method_used:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardTender$Emv$CardholderVerificationMethod#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final read_only_chip_card_application_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final read_only_chip_card_preferred_language_code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xb
    .end annotation
.end field

.field public final read_only_encrypted_emv_data:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x1
    .end annotation
.end field

.field public final read_only_plaintext_emv_data:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x6
    .end annotation
.end field

.field public final write_only_hardware_serial_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final write_only_processing_type:Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardTender$Emv$ProcessingType#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final write_only_reader_firmware_version:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1350
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Emv$ProtoAdapter_Emv;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardTender$Emv$ProtoAdapter_Emv;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Emv;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1354
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Emv;->DEFAULT_READ_ONLY_ENCRYPTED_EMV_DATA:Lokio/ByteString;

    .line 1356
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;->UNKNOWN:Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Emv;->DEFAULT_WRITE_ONLY_PROCESSING_TYPE:Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;

    .line 1364
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Emv;->DEFAULT_READ_ONLY_PLAINTEXT_EMV_DATA:Lokio/ByteString;

    const/4 v0, 0x0

    .line 1366
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Emv;->DEFAULT_IS_EMV_CAPABLE_READER_PRESENT:Ljava/lang/Boolean;

    .line 1370
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->DEFAULT_CVM_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Emv;->DEFAULT_READ_ONLY_CARDHOLDER_VERIFICATION_METHOD_USED:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13

    .line 1491
    sget-object v12, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/client/bills/CardTender$Emv;-><init>(Lokio/ByteString;Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 1501
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Emv;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p12}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1502
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_encrypted_emv_data:Lokio/ByteString;

    .line 1503
    iput-object p2, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->write_only_processing_type:Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;

    .line 1504
    iput-object p3, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_chip_card_application_id:Ljava/lang/String;

    .line 1505
    iput-object p4, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->write_only_reader_firmware_version:Ljava/lang/String;

    .line 1506
    iput-object p5, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->write_only_hardware_serial_number:Ljava/lang/String;

    .line 1507
    iput-object p6, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_plaintext_emv_data:Lokio/ByteString;

    .line 1508
    iput-object p7, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->is_emv_capable_reader_present:Ljava/lang/Boolean;

    .line 1509
    iput-object p8, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_application_preferred_name:Ljava/lang/String;

    .line 1510
    iput-object p9, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_cardholder_verification_method_used:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    .line 1511
    iput-object p10, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_chip_card_preferred_language_code:Ljava/lang/String;

    .line 1512
    iput-object p11, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->buyer_selected_account_name:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1536
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/CardTender$Emv;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1537
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/CardTender$Emv;

    .line 1538
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$Emv;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardTender$Emv;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_encrypted_emv_data:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_encrypted_emv_data:Lokio/ByteString;

    .line 1539
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->write_only_processing_type:Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardTender$Emv;->write_only_processing_type:Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;

    .line 1540
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_chip_card_application_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_chip_card_application_id:Ljava/lang/String;

    .line 1541
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->write_only_reader_firmware_version:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardTender$Emv;->write_only_reader_firmware_version:Ljava/lang/String;

    .line 1542
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->write_only_hardware_serial_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardTender$Emv;->write_only_hardware_serial_number:Ljava/lang/String;

    .line 1543
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_plaintext_emv_data:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_plaintext_emv_data:Lokio/ByteString;

    .line 1544
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->is_emv_capable_reader_present:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardTender$Emv;->is_emv_capable_reader_present:Ljava/lang/Boolean;

    .line 1545
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_application_preferred_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_application_preferred_name:Ljava/lang/String;

    .line 1546
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_cardholder_verification_method_used:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_cardholder_verification_method_used:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    .line 1547
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_chip_card_preferred_language_code:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_chip_card_preferred_language_code:Ljava/lang/String;

    .line 1548
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->buyer_selected_account_name:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CardTender$Emv;->buyer_selected_account_name:Ljava/lang/String;

    .line 1549
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1554
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_b

    .line 1556
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$Emv;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1557
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_encrypted_emv_data:Lokio/ByteString;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1558
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->write_only_processing_type:Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1559
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_chip_card_application_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1560
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->write_only_reader_firmware_version:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1561
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->write_only_hardware_serial_number:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1562
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_plaintext_emv_data:Lokio/ByteString;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1563
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->is_emv_capable_reader_present:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1564
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_application_preferred_name:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1565
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_cardholder_verification_method_used:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1566
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_chip_card_preferred_language_code:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1567
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->buyer_selected_account_name:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_a
    add-int/2addr v0, v2

    .line 1568
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_b
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;
    .locals 2

    .line 1517
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;-><init>()V

    .line 1518
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_encrypted_emv_data:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->read_only_encrypted_emv_data:Lokio/ByteString;

    .line 1519
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->write_only_processing_type:Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->write_only_processing_type:Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;

    .line 1520
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_chip_card_application_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->read_only_chip_card_application_id:Ljava/lang/String;

    .line 1521
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->write_only_reader_firmware_version:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->write_only_reader_firmware_version:Ljava/lang/String;

    .line 1522
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->write_only_hardware_serial_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->write_only_hardware_serial_number:Ljava/lang/String;

    .line 1523
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_plaintext_emv_data:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->read_only_plaintext_emv_data:Lokio/ByteString;

    .line 1524
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->is_emv_capable_reader_present:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->is_emv_capable_reader_present:Ljava/lang/Boolean;

    .line 1525
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_application_preferred_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->read_only_application_preferred_name:Ljava/lang/String;

    .line 1526
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_cardholder_verification_method_used:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->read_only_cardholder_verification_method_used:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    .line 1527
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_chip_card_preferred_language_code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->read_only_chip_card_preferred_language_code:Ljava/lang/String;

    .line 1528
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->buyer_selected_account_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->buyer_selected_account_name:Ljava/lang/String;

    .line 1529
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$Emv;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1349
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$Emv;->newBuilder()Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1575
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1576
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_encrypted_emv_data:Lokio/ByteString;

    if-eqz v1, :cond_0

    const-string v1, ", read_only_encrypted_emv_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_encrypted_emv_data:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1577
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->write_only_processing_type:Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;

    if-eqz v1, :cond_1

    const-string v1, ", write_only_processing_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->write_only_processing_type:Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1578
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_chip_card_application_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", read_only_chip_card_application_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_chip_card_application_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1579
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->write_only_reader_firmware_version:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", write_only_reader_firmware_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->write_only_reader_firmware_version:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1580
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->write_only_hardware_serial_number:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", write_only_hardware_serial_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->write_only_hardware_serial_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1581
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_plaintext_emv_data:Lokio/ByteString;

    if-eqz v1, :cond_5

    const-string v1, ", read_only_plaintext_emv_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_plaintext_emv_data:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1582
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->is_emv_capable_reader_present:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", is_emv_capable_reader_present="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->is_emv_capable_reader_present:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1583
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_application_preferred_name:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", read_only_application_preferred_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_application_preferred_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1584
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_cardholder_verification_method_used:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    if-eqz v1, :cond_8

    const-string v1, ", read_only_cardholder_verification_method_used="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_cardholder_verification_method_used:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1585
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_chip_card_preferred_language_code:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", read_only_chip_card_preferred_language_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_chip_card_preferred_language_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1586
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->buyer_selected_account_name:Ljava/lang/String;

    if-eqz v1, :cond_a

    const-string v1, ", buyer_selected_account_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Emv;->buyer_selected_account_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Emv{"

    .line 1587
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
