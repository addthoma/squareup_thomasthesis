.class public final Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DiningOptionLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails;",
        "Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public dining_option:Lcom/squareup/api/items/DiningOption;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 207
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails;
    .locals 3

    .line 220
    new-instance v0, Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails$Builder;->dining_option:Lcom/squareup/api/items/DiningOption;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails;-><init>(Lcom/squareup/api/items/DiningOption;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 204
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails;

    move-result-object v0

    return-object v0
.end method

.method public dining_option(Lcom/squareup/api/items/DiningOption;)Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails$Builder;
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails$Builder;->dining_option:Lcom/squareup/api/items/DiningOption;

    return-object p0
.end method
