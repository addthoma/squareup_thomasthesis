.class public final Lcom/squareup/protos/client/bills/CardData$UnencryptedCard$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;",
        "Lcom/squareup/protos/client/bills/CardData$UnencryptedCard$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public track2_data:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 834
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;
    .locals 3

    .line 847
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard$Builder;->track2_data:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 831
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard$Builder;->build()Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

    move-result-object v0

    return-object v0
.end method

.method public track2_data(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$UnencryptedCard$Builder;
    .locals 0

    .line 841
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard$Builder;->track2_data:Ljava/lang/String;

    return-object p0
.end method
