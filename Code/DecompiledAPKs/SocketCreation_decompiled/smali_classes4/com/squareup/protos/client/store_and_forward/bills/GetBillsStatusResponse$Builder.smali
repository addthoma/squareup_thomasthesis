.class public final Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetBillsStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse;",
        "Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill_processing_result:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;",
            ">;"
        }
    .end annotation
.end field

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 94
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 95
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse$Builder;->bill_processing_result:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bill_processing_result(Ljava/util/List;)Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;",
            ">;)",
            "Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse$Builder;"
        }
    .end annotation

    .line 104
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 105
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse$Builder;->bill_processing_result:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse;
    .locals 4

    .line 111
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse$Builder;->bill_processing_result:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse$Builder;->build()Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse;

    move-result-object v0

    return-object v0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse$Builder;
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
