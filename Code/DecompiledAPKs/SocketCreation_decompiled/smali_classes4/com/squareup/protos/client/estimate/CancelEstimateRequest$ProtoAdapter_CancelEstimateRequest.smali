.class final Lcom/squareup/protos/client/estimate/CancelEstimateRequest$ProtoAdapter_CancelEstimateRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CancelEstimateRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/estimate/CancelEstimateRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CancelEstimateRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/estimate/CancelEstimateRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 138
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/estimate/CancelEstimateRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 159
    new-instance v0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;-><init>()V

    .line 160
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 161
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 167
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 165
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;->send_email_to_recipients(Ljava/lang/Boolean;)Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;

    goto :goto_0

    .line 164
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;->version(Ljava/lang/Integer;)Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;

    goto :goto_0

    .line 163
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;->token_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;

    goto :goto_0

    .line 171
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 172
    invoke-virtual {v0}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;->build()Lcom/squareup/protos/client/estimate/CancelEstimateRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 136
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$ProtoAdapter_CancelEstimateRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/estimate/CancelEstimateRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/estimate/CancelEstimateRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 151
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->token_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 152
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->version:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 153
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 154
    invoke-virtual {p2}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 136
    check-cast p2, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$ProtoAdapter_CancelEstimateRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/estimate/CancelEstimateRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/estimate/CancelEstimateRequest;)I
    .locals 4

    .line 143
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->token_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->version:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 144
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    const/4 v3, 0x3

    .line 145
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 146
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 136
    check-cast p1, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$ProtoAdapter_CancelEstimateRequest;->encodedSize(Lcom/squareup/protos/client/estimate/CancelEstimateRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/estimate/CancelEstimateRequest;)Lcom/squareup/protos/client/estimate/CancelEstimateRequest;
    .locals 2

    .line 177
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->newBuilder()Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;

    move-result-object p1

    .line 178
    iget-object v0, p1, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;->token_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;->token_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;->token_pair:Lcom/squareup/protos/client/IdPair;

    .line 179
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 180
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;->build()Lcom/squareup/protos/client/estimate/CancelEstimateRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 136
    check-cast p1, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$ProtoAdapter_CancelEstimateRequest;->redact(Lcom/squareup/protos/client/estimate/CancelEstimateRequest;)Lcom/squareup/protos/client/estimate/CancelEstimateRequest;

    move-result-object p1

    return-object p1
.end method
