.class public final Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LinkBankAccountRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest;",
        "Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bank_account_details:Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

.field public idempotence_key:Ljava/lang/String;

.field public instant_payout_support_required:Ljava/lang/Boolean;

.field public password:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 139
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bank_account_details(Lcom/squareup/protos/client/bankaccount/BankAccountDetails;)Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;->bank_account_details:Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest;
    .locals 7

    .line 177
    new-instance v6, Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;->bank_account_details:Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    iget-object v2, p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;->password:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;->idempotence_key:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;->instant_payout_support_required:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest;-><init>(Lcom/squareup/protos/client/bankaccount/BankAccountDetails;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;->build()Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest;

    move-result-object v0

    return-object v0
.end method

.method public idempotence_key(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;->idempotence_key:Ljava/lang/String;

    return-object p0
.end method

.method public instant_payout_support_required(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;->instant_payout_support_required:Ljava/lang/Boolean;

    return-object p0
.end method

.method public password(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;->password:Ljava/lang/String;

    return-object p0
.end method
