.class final Lcom/squareup/protos/client/bills/Itemization$Configuration$ProtoAdapter_Configuration;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$Configuration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Configuration"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Itemization$Configuration;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1784
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Itemization$Configuration;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$Configuration;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1807
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;-><init>()V

    .line 1808
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1809
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 1823
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1821
    :cond_0
    sget-object v3, Lcom/squareup/api/items/MenuCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/MenuCategory;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->category(Lcom/squareup/api/items/MenuCategory;)Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;

    goto :goto_0

    .line 1820
    :cond_1
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->item_variation_price_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;

    goto :goto_0

    .line 1814
    :cond_2
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->backing_type(Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;)Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 1816
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 1811
    :cond_3
    sget-object v3, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->selected_options(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;)Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;

    goto :goto_0

    .line 1827
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1828
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Configuration;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1782
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$ProtoAdapter_Configuration;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$Configuration;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$Configuration;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1798
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1799
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration;->backing_type:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1800
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1801
    sget-object v0, Lcom/squareup/api/items/MenuCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration;->category:Lcom/squareup/api/items/MenuCategory;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1802
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Itemization$Configuration;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1782
    check-cast p2, Lcom/squareup/protos/client/bills/Itemization$Configuration;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Itemization$Configuration$ProtoAdapter_Configuration;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$Configuration;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Itemization$Configuration;)I
    .locals 4

    .line 1789
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->backing_type:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    const/4 v3, 0x2

    .line 1790
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x3

    .line 1791
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/MenuCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->category:Lcom/squareup/api/items/MenuCategory;

    const/4 v3, 0x4

    .line 1792
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1793
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1782
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$Configuration;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$ProtoAdapter_Configuration;->encodedSize(Lcom/squareup/protos/client/bills/Itemization$Configuration;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Itemization$Configuration;)Lcom/squareup/protos/client/bills/Itemization$Configuration;
    .locals 2

    .line 1833
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;

    move-result-object p1

    .line 1834
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    .line 1835
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    .line 1836
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->category:Lcom/squareup/api/items/MenuCategory;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/api/items/MenuCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->category:Lcom/squareup/api/items/MenuCategory;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/MenuCategory;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->category:Lcom/squareup/api/items/MenuCategory;

    .line 1837
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1838
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Configuration;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1782
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$Configuration;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$ProtoAdapter_Configuration;->redact(Lcom/squareup/protos/client/bills/Itemization$Configuration;)Lcom/squareup/protos/client/bills/Itemization$Configuration;

    move-result-object p1

    return-object p1
.end method
