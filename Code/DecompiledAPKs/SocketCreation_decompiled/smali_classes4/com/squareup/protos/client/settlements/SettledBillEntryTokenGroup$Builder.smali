.class public final Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SettledBillEntryTokenGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;",
        "Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill_token:Ljava/lang/String;

.field public payment_token:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public refund_uuid:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 116
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 117
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup$Builder;->payment_token:Ljava/util/List;

    .line 118
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup$Builder;->refund_uuid:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bill_token(Ljava/lang/String;)Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup$Builder;->bill_token:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;
    .locals 5

    .line 146
    new-instance v0, Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup$Builder;->bill_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup$Builder;->payment_token:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup$Builder;->refund_uuid:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup$Builder;->build()Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;

    move-result-object v0

    return-object v0
.end method

.method public payment_token(Ljava/util/List;)Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup$Builder;"
        }
    .end annotation

    .line 130
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup$Builder;->payment_token:Ljava/util/List;

    return-object p0
.end method

.method public refund_uuid(Ljava/util/List;)Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup$Builder;"
        }
    .end annotation

    .line 139
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 140
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup$Builder;->refund_uuid:Ljava/util/List;

    return-object p0
.end method
