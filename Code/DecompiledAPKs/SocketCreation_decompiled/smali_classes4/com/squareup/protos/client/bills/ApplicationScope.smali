.class public final enum Lcom/squareup/protos/client/bills/ApplicationScope;
.super Ljava/lang/Enum;
.source "ApplicationScope.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/ApplicationScope$ProtoAdapter_ApplicationScope;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/ApplicationScope;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/ApplicationScope;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/ApplicationScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CART_LEVEL:Lcom/squareup/protos/client/bills/ApplicationScope;

.field public static final enum CART_LEVEL_FIXED:Lcom/squareup/protos/client/bills/ApplicationScope;

.field public static final enum ITEMIZATION_LEVEL:Lcom/squareup/protos/client/bills/ApplicationScope;

.field public static final enum ITEMIZATION_LEVEL_PER_QUANTITY:Lcom/squareup/protos/client/bills/ApplicationScope;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/bills/ApplicationScope;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 20
    new-instance v0, Lcom/squareup/protos/client/bills/ApplicationScope;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/ApplicationScope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/ApplicationScope;->UNKNOWN:Lcom/squareup/protos/client/bills/ApplicationScope;

    .line 29
    new-instance v0, Lcom/squareup/protos/client/bills/ApplicationScope;

    const/4 v2, 0x1

    const-string v3, "ITEMIZATION_LEVEL"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/ApplicationScope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/ApplicationScope;->ITEMIZATION_LEVEL:Lcom/squareup/protos/client/bills/ApplicationScope;

    .line 39
    new-instance v0, Lcom/squareup/protos/client/bills/ApplicationScope;

    const/4 v3, 0x2

    const-string v4, "CART_LEVEL"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/ApplicationScope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/ApplicationScope;->CART_LEVEL:Lcom/squareup/protos/client/bills/ApplicationScope;

    .line 51
    new-instance v0, Lcom/squareup/protos/client/bills/ApplicationScope;

    const/4 v4, 0x3

    const-string v5, "CART_LEVEL_FIXED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bills/ApplicationScope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/ApplicationScope;->CART_LEVEL_FIXED:Lcom/squareup/protos/client/bills/ApplicationScope;

    .line 66
    new-instance v0, Lcom/squareup/protos/client/bills/ApplicationScope;

    const/4 v5, 0x4

    const-string v6, "ITEMIZATION_LEVEL_PER_QUANTITY"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/bills/ApplicationScope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/ApplicationScope;->ITEMIZATION_LEVEL_PER_QUANTITY:Lcom/squareup/protos/client/bills/ApplicationScope;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/client/bills/ApplicationScope;

    .line 19
    sget-object v6, Lcom/squareup/protos/client/bills/ApplicationScope;->UNKNOWN:Lcom/squareup/protos/client/bills/ApplicationScope;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/ApplicationScope;->ITEMIZATION_LEVEL:Lcom/squareup/protos/client/bills/ApplicationScope;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/ApplicationScope;->CART_LEVEL:Lcom/squareup/protos/client/bills/ApplicationScope;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bills/ApplicationScope;->CART_LEVEL_FIXED:Lcom/squareup/protos/client/bills/ApplicationScope;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bills/ApplicationScope;->ITEMIZATION_LEVEL_PER_QUANTITY:Lcom/squareup/protos/client/bills/ApplicationScope;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/client/bills/ApplicationScope;->$VALUES:[Lcom/squareup/protos/client/bills/ApplicationScope;

    .line 68
    new-instance v0, Lcom/squareup/protos/client/bills/ApplicationScope$ProtoAdapter_ApplicationScope;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ApplicationScope$ProtoAdapter_ApplicationScope;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/ApplicationScope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 72
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 73
    iput p3, p0, Lcom/squareup/protos/client/bills/ApplicationScope;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/ApplicationScope;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 85
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/ApplicationScope;->ITEMIZATION_LEVEL_PER_QUANTITY:Lcom/squareup/protos/client/bills/ApplicationScope;

    return-object p0

    .line 84
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/ApplicationScope;->CART_LEVEL_FIXED:Lcom/squareup/protos/client/bills/ApplicationScope;

    return-object p0

    .line 83
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/ApplicationScope;->CART_LEVEL:Lcom/squareup/protos/client/bills/ApplicationScope;

    return-object p0

    .line 82
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bills/ApplicationScope;->ITEMIZATION_LEVEL:Lcom/squareup/protos/client/bills/ApplicationScope;

    return-object p0

    .line 81
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/bills/ApplicationScope;->UNKNOWN:Lcom/squareup/protos/client/bills/ApplicationScope;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/ApplicationScope;
    .locals 1

    .line 19
    const-class v0, Lcom/squareup/protos/client/bills/ApplicationScope;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/ApplicationScope;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/ApplicationScope;
    .locals 1

    .line 19
    sget-object v0, Lcom/squareup/protos/client/bills/ApplicationScope;->$VALUES:[Lcom/squareup/protos/client/bills/ApplicationScope;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/ApplicationScope;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/ApplicationScope;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 92
    iget v0, p0, Lcom/squareup/protos/client/bills/ApplicationScope;->value:I

    return v0
.end method
