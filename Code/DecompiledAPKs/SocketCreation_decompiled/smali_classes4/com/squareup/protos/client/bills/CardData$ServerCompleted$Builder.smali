.class public final Lcom/squareup/protos/client/bills/CardData$ServerCompleted$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData$ServerCompleted;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CardData$ServerCompleted;",
        "Lcom/squareup/protos/client/bills/CardData$ServerCompleted$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public brand:Lcom/squareup/protos/client/bills/CardData$ServerCompleted$ServerCompletedBrand;

.field public preauth_idempotency_key:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2599
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public brand(Lcom/squareup/protos/client/bills/CardData$ServerCompleted$ServerCompletedBrand;)Lcom/squareup/protos/client/bills/CardData$ServerCompleted$Builder;
    .locals 0

    .line 2611
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$ServerCompleted$Builder;->brand:Lcom/squareup/protos/client/bills/CardData$ServerCompleted$ServerCompletedBrand;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/CardData$ServerCompleted;
    .locals 4

    .line 2617
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$ServerCompleted;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$ServerCompleted$Builder;->preauth_idempotency_key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/CardData$ServerCompleted$Builder;->brand:Lcom/squareup/protos/client/bills/CardData$ServerCompleted$ServerCompletedBrand;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/CardData$ServerCompleted;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/CardData$ServerCompleted$ServerCompletedBrand;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2594
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$ServerCompleted$Builder;->build()Lcom/squareup/protos/client/bills/CardData$ServerCompleted;

    move-result-object v0

    return-object v0
.end method

.method public preauth_idempotency_key(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$ServerCompleted$Builder;
    .locals 0

    .line 2606
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$ServerCompleted$Builder;->preauth_idempotency_key:Ljava/lang/String;

    return-object p0
.end method
