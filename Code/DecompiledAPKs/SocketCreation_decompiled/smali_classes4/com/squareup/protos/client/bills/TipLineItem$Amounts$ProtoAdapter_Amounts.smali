.class final Lcom/squareup/protos/client/bills/TipLineItem$Amounts$ProtoAdapter_Amounts;
.super Lcom/squareup/wire/ProtoAdapter;
.source "TipLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/TipLineItem$Amounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Amounts"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/TipLineItem$Amounts;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 222
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/TipLineItem$Amounts;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 239
    new-instance v0, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;-><init>()V

    .line 240
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 241
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 245
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 243
    :cond_0
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;->applied_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;

    goto :goto_0

    .line 249
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 250
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 220
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$ProtoAdapter_Amounts;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/TipLineItem$Amounts;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 233
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 234
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 220
    check-cast p2, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$ProtoAdapter_Amounts;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/TipLineItem$Amounts;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/TipLineItem$Amounts;)I
    .locals 3

    .line 227
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 228
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 220
    check-cast p1, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$ProtoAdapter_Amounts;->encodedSize(Lcom/squareup/protos/client/bills/TipLineItem$Amounts;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/TipLineItem$Amounts;)Lcom/squareup/protos/client/bills/TipLineItem$Amounts;
    .locals 2

    .line 255
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->newBuilder()Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;

    move-result-object p1

    .line 256
    iget-object v0, p1, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;->applied_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;->applied_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;->applied_money:Lcom/squareup/protos/common/Money;

    .line 257
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 258
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 220
    check-cast p1, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$ProtoAdapter_Amounts;->redact(Lcom/squareup/protos/client/bills/TipLineItem$Amounts;)Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    move-result-object p1

    return-object p1
.end method
