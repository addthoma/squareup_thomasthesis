.class public final Lcom/squareup/protos/client/bizbank/Stamp$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Stamp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/Stamp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/Stamp;",
        "Lcom/squareup/protos/client/bizbank/Stamp$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public accessibility_text:Ljava/lang/String;

.field public min_scale:Ljava/lang/Integer;

.field public name:Ljava/lang/String;

.field public svg:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 143
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public accessibility_text(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/Stamp$Builder;
    .locals 0

    .line 158
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/Stamp$Builder;->accessibility_text:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bizbank/Stamp;
    .locals 7

    .line 180
    new-instance v6, Lcom/squareup/protos/client/bizbank/Stamp;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/Stamp$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/Stamp$Builder;->accessibility_text:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bizbank/Stamp$Builder;->svg:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/bizbank/Stamp$Builder;->min_scale:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bizbank/Stamp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 134
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/Stamp$Builder;->build()Lcom/squareup/protos/client/bizbank/Stamp;

    move-result-object v0

    return-object v0
.end method

.method public min_scale(Ljava/lang/Integer;)Lcom/squareup/protos/client/bizbank/Stamp$Builder;
    .locals 0

    .line 174
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/Stamp$Builder;->min_scale:Ljava/lang/Integer;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/Stamp$Builder;
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/Stamp$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public svg(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/Stamp$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/Stamp$Builder;->svg:Ljava/lang/String;

    return-object p0
.end method
