.class final Lcom/squareup/protos/client/posfe/inventory/sync/Request$ProtoAdapter_Request;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Request.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/posfe/inventory/sync/Request;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Request"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/posfe/inventory/sync/Request;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 135
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/posfe/inventory/sync/Request;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/posfe/inventory/sync/Request;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 156
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;-><init>()V

    .line 157
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 158
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 164
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 162
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->put_request(Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;)Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;

    goto :goto_0

    .line 161
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->get_request(Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;)Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;

    goto :goto_0

    .line 160
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->scope(Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;)Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;

    goto :goto_0

    .line 168
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 169
    invoke-virtual {v0}, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->build()Lcom/squareup/protos/client/posfe/inventory/sync/Request;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 133
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/posfe/inventory/sync/Request$ProtoAdapter_Request;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/posfe/inventory/sync/Request;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/posfe/inventory/sync/Request;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 148
    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->scope:Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 149
    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->get_request:Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 150
    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->put_request:Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 151
    invoke-virtual {p2}, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 133
    check-cast p2, Lcom/squareup/protos/client/posfe/inventory/sync/Request;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/posfe/inventory/sync/Request$ProtoAdapter_Request;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/posfe/inventory/sync/Request;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/posfe/inventory/sync/Request;)I
    .locals 4

    .line 140
    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->scope:Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->get_request:Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    const/4 v3, 0x2

    .line 141
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->put_request:Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;

    const/4 v3, 0x3

    .line 142
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 143
    invoke-virtual {p1}, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 133
    check-cast p1, Lcom/squareup/protos/client/posfe/inventory/sync/Request;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/posfe/inventory/sync/Request$ProtoAdapter_Request;->encodedSize(Lcom/squareup/protos/client/posfe/inventory/sync/Request;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/posfe/inventory/sync/Request;)Lcom/squareup/protos/client/posfe/inventory/sync/Request;
    .locals 2

    .line 174
    invoke-virtual {p1}, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->newBuilder()Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;

    move-result-object p1

    .line 175
    iget-object v0, p1, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->scope:Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->scope:Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;

    iput-object v0, p1, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->scope:Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;

    .line 176
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->get_request:Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->get_request:Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    iput-object v0, p1, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->get_request:Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    .line 177
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->put_request:Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->put_request:Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;

    iput-object v0, p1, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->put_request:Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;

    .line 178
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 179
    invoke-virtual {p1}, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->build()Lcom/squareup/protos/client/posfe/inventory/sync/Request;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 133
    check-cast p1, Lcom/squareup/protos/client/posfe/inventory/sync/Request;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/posfe/inventory/sync/Request$ProtoAdapter_Request;->redact(Lcom/squareup/protos/client/posfe/inventory/sync/Request;)Lcom/squareup/protos/client/posfe/inventory/sync/Request;

    move-result-object p1

    return-object p1
.end method
