.class public final Lcom/squareup/protos/client/onboard/FreeProcessingBalance$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FreeProcessingBalance.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/onboard/FreeProcessingBalance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/onboard/FreeProcessingBalance;",
        "Lcom/squareup/protos/client/onboard/FreeProcessingBalance$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public expires_at:Lcom/squareup/protos/common/time/DateTime;

.field public free_processing:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 99
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/onboard/FreeProcessingBalance;
    .locals 4

    .line 120
    new-instance v0, Lcom/squareup/protos/client/onboard/FreeProcessingBalance;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/FreeProcessingBalance$Builder;->free_processing:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/client/onboard/FreeProcessingBalance$Builder;->expires_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/onboard/FreeProcessingBalance;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/FreeProcessingBalance$Builder;->build()Lcom/squareup/protos/client/onboard/FreeProcessingBalance;

    move-result-object v0

    return-object v0
.end method

.method public expires_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/onboard/FreeProcessingBalance$Builder;
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/FreeProcessingBalance$Builder;->expires_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public free_processing(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/onboard/FreeProcessingBalance$Builder;
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/FreeProcessingBalance$Builder;->free_processing:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
