.class public final Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ConfirmIssueCardRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest;",
        "Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public card_issue_token:Ljava/lang/String;

.field public customization_token:Ljava/lang/String;

.field public shipping_address:Lcom/squareup/protos/common/location/GlobalAddress;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 111
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest;
    .locals 5

    .line 131
    new-instance v0, Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest$Builder;->card_issue_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest$Builder;->shipping_address:Lcom/squareup/protos/common/location/GlobalAddress;

    iget-object v3, p0, Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest$Builder;->customization_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 104
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest;

    move-result-object v0

    return-object v0
.end method

.method public card_issue_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest$Builder;->card_issue_token:Ljava/lang/String;

    return-object p0
.end method

.method public customization_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest$Builder;->customization_token:Ljava/lang/String;

    return-object p0
.end method

.method public shipping_address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest$Builder;->shipping_address:Lcom/squareup/protos/common/location/GlobalAddress;

    return-object p0
.end method
