.class public final Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;",
        "Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public abbreviation:Ljava/lang/String;

.field public cogs_object_id:Ljava/lang/String;

.field public color:Ljava/lang/String;

.field public description:Ljava/lang/String;

.field public image_url:Ljava/lang/String;

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2605
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public abbreviation(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;
    .locals 0

    .line 2627
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;->abbreviation:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;
    .locals 9

    .line 2646
    new-instance v8, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;->color:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;->description:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;->name:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;->abbreviation:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;->image_url:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;->cogs_object_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2592
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;

    move-result-object v0

    return-object v0
.end method

.method public cogs_object_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;
    .locals 0

    .line 2640
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;->cogs_object_id:Ljava/lang/String;

    return-object p0
.end method

.method public color(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;
    .locals 0

    .line 2612
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;->color:Ljava/lang/String;

    return-object p0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;
    .locals 0

    .line 2617
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public image_url(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;
    .locals 0

    .line 2632
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;->image_url:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;
    .locals 0

    .line 2622
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;->name:Ljava/lang/String;

    return-object p0
.end method
