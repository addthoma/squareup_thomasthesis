.class public final enum Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;
.super Ljava/lang/Enum;
.source "CardActivityEvent.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/CardActivityEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ActivityType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType$ProtoAdapter_ActivityType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ATM_WITHDRAWAL:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

.field public static final enum ATM_WITHDRAWAL_FEE:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

.field public static final enum CARD_PAYMENT:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

.field public static final enum CARD_PAYMENT_REWARD:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

.field public static final enum DEPOSIT:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

.field public static final enum DO_NOT_USE:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

.field public static final enum INSTANT_DEPOSIT:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

.field public static final enum INSTANT_DEPOSIT_FEE:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 307
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    .line 309
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    const/4 v2, 0x1

    const-string v3, "CARD_PAYMENT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->CARD_PAYMENT:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    .line 311
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    const/4 v3, 0x2

    const-string v4, "DEPOSIT"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->DEPOSIT:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    .line 313
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    const/4 v4, 0x3

    const-string v5, "INSTANT_DEPOSIT"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->INSTANT_DEPOSIT:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    .line 315
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    const/4 v5, 0x4

    const-string v6, "INSTANT_DEPOSIT_FEE"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->INSTANT_DEPOSIT_FEE:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    .line 317
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    const/4 v6, 0x5

    const-string v7, "ATM_WITHDRAWAL"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->ATM_WITHDRAWAL:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    .line 319
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    const/4 v7, 0x6

    const-string v8, "ATM_WITHDRAWAL_FEE"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->ATM_WITHDRAWAL_FEE:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    .line 321
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    const/4 v8, 0x7

    const-string v9, "CARD_PAYMENT_REWARD"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->CARD_PAYMENT_REWARD:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    .line 306
    sget-object v9, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    aput-object v9, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->CARD_PAYMENT:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->DEPOSIT:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->INSTANT_DEPOSIT:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->INSTANT_DEPOSIT_FEE:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->ATM_WITHDRAWAL:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->ATM_WITHDRAWAL_FEE:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->CARD_PAYMENT_REWARD:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    aput-object v1, v0, v8

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->$VALUES:[Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    .line 323
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType$ProtoAdapter_ActivityType;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType$ProtoAdapter_ActivityType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 327
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 328
    iput p3, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 343
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->CARD_PAYMENT_REWARD:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    return-object p0

    .line 342
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->ATM_WITHDRAWAL_FEE:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    return-object p0

    .line 341
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->ATM_WITHDRAWAL:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    return-object p0

    .line 340
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->INSTANT_DEPOSIT_FEE:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    return-object p0

    .line 339
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->INSTANT_DEPOSIT:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    return-object p0

    .line 338
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->DEPOSIT:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    return-object p0

    .line 337
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->CARD_PAYMENT:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    return-object p0

    .line 336
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;
    .locals 1

    .line 306
    const-class v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;
    .locals 1

    .line 306
    sget-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->$VALUES:[Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 350
    iget v0, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->value:I

    return v0
.end method
