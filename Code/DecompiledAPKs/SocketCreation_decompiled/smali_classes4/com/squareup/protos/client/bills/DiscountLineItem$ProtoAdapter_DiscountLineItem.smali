.class final Lcom/squareup/protos/client/bills/DiscountLineItem$ProtoAdapter_DiscountLineItem;
.super Lcom/squareup/wire/ProtoAdapter;
.source "DiscountLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/DiscountLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_DiscountLineItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/DiscountLineItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1209
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/DiscountLineItem;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/DiscountLineItem;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1244
    new-instance v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;-><init>()V

    .line 1245
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1246
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 1273
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1271
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->discount_quantity(Ljava/lang/String;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    goto :goto_0

    .line 1265
    :pswitch_1
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->application_method(Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 1267
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 1257
    :pswitch_2
    :try_start_1
    sget-object v4, Lcom/squareup/protos/client/bills/ApplicationScope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/ApplicationScope;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->application_scope(Lcom/squareup/protos/client/bills/ApplicationScope;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 1259
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 1254
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    goto :goto_0

    .line 1253
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->configuration(Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    goto :goto_0

    .line 1252
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->write_only_deleted(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    goto :goto_0

    .line 1251
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->amounts(Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    goto :goto_0

    .line 1250
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->read_only_display_details(Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    goto/16 :goto_0

    .line 1249
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->write_only_backing_details(Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    goto/16 :goto_0

    .line 1248
    :pswitch_9
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->discount_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    goto/16 :goto_0

    .line 1277
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1278
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1207
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$ProtoAdapter_DiscountLineItem;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/DiscountLineItem;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/DiscountLineItem;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1229
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1230
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/DiscountLineItem;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1231
    sget-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/DiscountLineItem;->configuration:Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1232
    sget-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1233
    sget-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1234
    sget-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1235
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_deleted:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1236
    sget-object v0, Lcom/squareup/protos/client/bills/ApplicationScope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_scope:Lcom/squareup/protos/client/bills/ApplicationScope;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1237
    sget-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_method:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1238
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_quantity:Ljava/lang/String;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1239
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/DiscountLineItem;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1207
    check-cast p2, Lcom/squareup/protos/client/bills/DiscountLineItem;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/DiscountLineItem$ProtoAdapter_DiscountLineItem;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/DiscountLineItem;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/DiscountLineItem;)I
    .locals 4

    .line 1214
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x7

    .line 1215
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->configuration:Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    const/4 v3, 0x6

    .line 1216
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    const/4 v3, 0x2

    .line 1217
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    const/4 v3, 0x3

    .line 1218
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    const/4 v3, 0x4

    .line 1219
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_deleted:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 1220
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/ApplicationScope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_scope:Lcom/squareup/protos/client/bills/ApplicationScope;

    const/16 v3, 0x8

    .line 1221
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_method:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

    const/16 v3, 0x9

    .line 1222
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_quantity:Ljava/lang/String;

    const/16 v3, 0xa

    .line 1223
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1224
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/DiscountLineItem;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1207
    check-cast p1, Lcom/squareup/protos/client/bills/DiscountLineItem;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$ProtoAdapter_DiscountLineItem;->encodedSize(Lcom/squareup/protos/client/bills/DiscountLineItem;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/DiscountLineItem;)Lcom/squareup/protos/client/bills/DiscountLineItem;
    .locals 2

    .line 1283
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/DiscountLineItem;->newBuilder()Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object p1

    .line 1284
    iget-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->discount_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->discount_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->discount_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 1285
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 1286
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->configuration:Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->configuration:Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->configuration:Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    .line 1287
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    .line 1288
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    .line 1289
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    .line 1290
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1291
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1207
    check-cast p1, Lcom/squareup/protos/client/bills/DiscountLineItem;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$ProtoAdapter_DiscountLineItem;->redact(Lcom/squareup/protos/client/bills/DiscountLineItem;)Lcom/squareup/protos/client/bills/DiscountLineItem;

    move-result-object p1

    return-object p1
.end method
