.class public final Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public create_reason:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;

.field public inherited_event_id_pairs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 5074
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 5075
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;->inherited_event_id_pairs:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;
    .locals 4

    .line 5095
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;->inherited_event_id_pairs:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;->create_reason:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;-><init>(Ljava/util/List;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 5069
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;

    move-result-object v0

    return-object v0
.end method

.method public create_reason(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;
    .locals 0

    .line 5089
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;->create_reason:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;

    return-object p0
.end method

.method public inherited_event_id_pairs(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;"
        }
    .end annotation

    .line 5083
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 5084
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;->inherited_event_id_pairs:Ljava/util/List;

    return-object p0
.end method
