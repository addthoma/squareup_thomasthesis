.class public final enum Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;
.super Ljava/lang/Enum;
.source "Tender.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ReasonForNoStars"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars$ProtoAdapter_ReasonForNoStars;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum BUYER_DECLINED:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

.field public static final enum CLIENT_DISABLED_LOYALTY:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

.field public static final enum MISSING_LOYALTY_INFO:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

.field public static final enum NETWORK_TIMEOUT:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

.field public static final enum NOT_YET_SUBSCRIBED:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

.field public static final enum NO_QUALIFYING_ITEMS:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

.field public static final enum OFFLINE_MODE:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

.field public static final enum PURCHASE_DID_NOT_QUALIFY:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

.field public static final enum RETURNED_DURING_LOADING:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

.field public static final enum RETURNED_FROM_LOYALTY_SCREEN:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

.field public static final enum RETURNED_FROM_RECEIPT_SCREEN:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

.field public static final enum SKIPPED_SCREEN:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

.field public static final enum SPEND_TOO_LOW:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 2398
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->UNKNOWN:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    .line 2404
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    const/4 v2, 0x1

    const-string v3, "SPEND_TOO_LOW"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->SPEND_TOO_LOW:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    .line 2410
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    const/4 v3, 0x2

    const-string v4, "NO_QUALIFYING_ITEMS"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->NO_QUALIFYING_ITEMS:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    .line 2416
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    const/4 v4, 0x3

    const-string v5, "SKIPPED_SCREEN"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->SKIPPED_SCREEN:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    .line 2421
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    const/4 v5, 0x4

    const-string v6, "NOT_YET_SUBSCRIBED"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->NOT_YET_SUBSCRIBED:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    .line 2426
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    const/4 v6, 0x5

    const-string v7, "CLIENT_DISABLED_LOYALTY"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->CLIENT_DISABLED_LOYALTY:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    .line 2431
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    const/4 v7, 0x6

    const-string v8, "RETURNED_FROM_RECEIPT_SCREEN"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->RETURNED_FROM_RECEIPT_SCREEN:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    .line 2436
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    const/4 v8, 0x7

    const-string v9, "RETURNED_FROM_LOYALTY_SCREEN"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->RETURNED_FROM_LOYALTY_SCREEN:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    .line 2441
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    const/16 v9, 0x8

    const-string v10, "BUYER_DECLINED"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->BUYER_DECLINED:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    .line 2446
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    const/16 v10, 0x9

    const-string v11, "OFFLINE_MODE"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->OFFLINE_MODE:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    .line 2451
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    const/16 v11, 0xa

    const-string v12, "NETWORK_TIMEOUT"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->NETWORK_TIMEOUT:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    .line 2456
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    const/16 v12, 0xb

    const-string v13, "MISSING_LOYALTY_INFO"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->MISSING_LOYALTY_INFO:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    .line 2461
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    const/16 v13, 0xc

    const-string v14, "RETURNED_DURING_LOADING"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->RETURNED_DURING_LOADING:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    .line 2466
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    const/16 v14, 0xd

    const-string v15, "PURCHASE_DID_NOT_QUALIFY"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->PURCHASE_DID_NOT_QUALIFY:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    const/16 v0, 0xe

    new-array v0, v0, [Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    .line 2393
    sget-object v15, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->UNKNOWN:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    aput-object v15, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->SPEND_TOO_LOW:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->NO_QUALIFYING_ITEMS:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->SKIPPED_SCREEN:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->NOT_YET_SUBSCRIBED:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->CLIENT_DISABLED_LOYALTY:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->RETURNED_FROM_RECEIPT_SCREEN:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->RETURNED_FROM_LOYALTY_SCREEN:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->BUYER_DECLINED:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->OFFLINE_MODE:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->NETWORK_TIMEOUT:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->MISSING_LOYALTY_INFO:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->RETURNED_DURING_LOADING:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->PURCHASE_DID_NOT_QUALIFY:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    aput-object v1, v0, v14

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->$VALUES:[Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    .line 2468
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars$ProtoAdapter_ReasonForNoStars;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars$ProtoAdapter_ReasonForNoStars;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 2472
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2473
    iput p3, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 2494
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->PURCHASE_DID_NOT_QUALIFY:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    return-object p0

    .line 2493
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->RETURNED_DURING_LOADING:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    return-object p0

    .line 2492
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->MISSING_LOYALTY_INFO:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    return-object p0

    .line 2491
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->NETWORK_TIMEOUT:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    return-object p0

    .line 2490
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->OFFLINE_MODE:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    return-object p0

    .line 2489
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->BUYER_DECLINED:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    return-object p0

    .line 2488
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->RETURNED_FROM_LOYALTY_SCREEN:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    return-object p0

    .line 2487
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->RETURNED_FROM_RECEIPT_SCREEN:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    return-object p0

    .line 2486
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->CLIENT_DISABLED_LOYALTY:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    return-object p0

    .line 2485
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->NOT_YET_SUBSCRIBED:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    return-object p0

    .line 2484
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->SKIPPED_SCREEN:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    return-object p0

    .line 2483
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->NO_QUALIFYING_ITEMS:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    return-object p0

    .line 2482
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->SPEND_TOO_LOW:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    return-object p0

    .line 2481
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->UNKNOWN:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;
    .locals 1

    .line 2393
    const-class v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;
    .locals 1

    .line 2393
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->$VALUES:[Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 2501
    iget v0, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->value:I

    return v0
.end method
