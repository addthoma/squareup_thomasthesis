.class final Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$ProtoAdapter_PaperSignatureTender;
.super Lcom/squareup/wire/ProtoAdapter;
.source "PaperSignatureTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_PaperSignatureTender"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 231
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 262
    new-instance v0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;-><init>()V

    .line 263
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 264
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 282
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 280
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->sale_time_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;

    goto :goto_0

    .line 279
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->tax_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;

    goto :goto_0

    .line 278
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;

    goto :goto_0

    .line 277
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->subtotal_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;

    goto :goto_0

    .line 276
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;

    goto :goto_0

    .line 270
    :pswitch_5
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/paper_signature/TenderState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/paper_signature/TenderState;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->current_state(Lcom/squareup/protos/client/paper_signature/TenderState;)Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 272
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 267
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->bill_id(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;

    goto :goto_0

    .line 266
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->tender_id(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;

    goto :goto_0

    .line 286
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 287
    invoke-virtual {v0}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->build()Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 229
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$ProtoAdapter_PaperSignatureTender;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 249
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tender_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 250
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->bill_id:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 251
    sget-object v0, Lcom/squareup/protos/client/paper_signature/TenderState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->current_state:Lcom/squareup/protos/client/paper_signature/TenderState;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 252
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->total_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 253
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->subtotal_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 254
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tip_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 255
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tax_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 256
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->sale_time_at:Lcom/squareup/protos/common/time/DateTime;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 257
    invoke-virtual {p2}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 229
    check-cast p2, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$ProtoAdapter_PaperSignatureTender;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;)I
    .locals 4

    .line 236
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tender_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->bill_id:Ljava/lang/String;

    const/4 v3, 0x2

    .line 237
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/paper_signature/TenderState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->current_state:Lcom/squareup/protos/client/paper_signature/TenderState;

    const/4 v3, 0x3

    .line 238
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->total_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x4

    .line 239
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->subtotal_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x5

    .line 240
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tip_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x6

    .line 241
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tax_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x7

    .line 242
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->sale_time_at:Lcom/squareup/protos/common/time/DateTime;

    const/16 v3, 0x8

    .line 243
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 244
    invoke-virtual {p1}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 229
    check-cast p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$ProtoAdapter_PaperSignatureTender;->encodedSize(Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;)Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;
    .locals 2

    .line 292
    invoke-virtual {p1}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->newBuilder()Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;

    move-result-object p1

    .line 293
    iget-object v0, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->total_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->total_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->total_money:Lcom/squareup/protos/common/Money;

    .line 294
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->subtotal_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->subtotal_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->subtotal_money:Lcom/squareup/protos/common/Money;

    .line 295
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    .line 296
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->tax_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->tax_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->tax_money:Lcom/squareup/protos/common/Money;

    .line 297
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->sale_time_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->sale_time_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->sale_time_at:Lcom/squareup/protos/common/time/DateTime;

    .line 298
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 299
    invoke-virtual {p1}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->build()Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 229
    check-cast p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$ProtoAdapter_PaperSignatureTender;->redact(Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;)Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;

    move-result-object p1

    return-object p1
.end method
