.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public instrument_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

.field public tender_id_pair:Lcom/squareup/protos/client/IdPair;

.field public total_money:Lcom/squareup/protos/common/Money;

.field public type:Lcom/squareup/protos/client/bills/Tender$Type;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4963
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;
    .locals 7

    .line 4994
    new-instance v6, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;->instrument_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;-><init>(Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;Lcom/squareup/protos/client/IdPair;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 4954
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;

    move-result-object v0

    return-object v0
.end method

.method public instrument_details(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;
    .locals 0

    .line 4980
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;->instrument_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    return-object p0
.end method

.method public tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;
    .locals 0

    .line 4988
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;
    .locals 0

    .line 4972
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;->total_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/bills/Tender$Type;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;
    .locals 0

    .line 4967
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    return-object p0
.end method
