.class public final Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetResidualBillResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public source_bill_server_token:Ljava/lang/String;

.field public source_tender_server_token:Ljava/lang/String;

.field public tender:Lcom/squareup/protos/client/bills/Tender;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 774
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;
    .locals 5

    .line 810
    new-instance v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;->source_bill_server_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;->source_tender_server_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;->tender:Lcom/squareup/protos/client/bills/Tender;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Tender;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 767
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;->build()Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;

    move-result-object v0

    return-object v0
.end method

.method public source_bill_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;
    .locals 0

    .line 781
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;->source_bill_server_token:Ljava/lang/String;

    return-object p0
.end method

.method public source_tender_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;
    .locals 0

    .line 789
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;->source_tender_server_token:Ljava/lang/String;

    return-object p0
.end method

.method public tender(Lcom/squareup/protos/client/bills/Tender;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;
    .locals 0

    .line 804
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;->tender:Lcom/squareup/protos/client/bills/Tender;

    return-object p0
.end method
