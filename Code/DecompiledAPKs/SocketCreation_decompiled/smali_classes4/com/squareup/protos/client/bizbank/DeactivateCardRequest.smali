.class public final Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;
.super Lcom/squareup/wire/Message;
.source "DeactivateCardRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$ProtoAdapter_DeactivateCardRequest;,
        Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;,
        Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;",
        "Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CARD_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_DEACTIVATION_REASON:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

.field private static final serialVersionUID:J


# instance fields
.field public final card_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final deactivation_reason:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bizbank.DeactivateCardRequest$DeactivationReason#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$ProtoAdapter_DeactivateCardRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$ProtoAdapter_DeactivateCardRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    sput-object v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;->DEFAULT_DEACTIVATION_REASON:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;)V
    .locals 1

    .line 43
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;Lokio/ByteString;)V
    .locals 1

    .line 48
    sget-object v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 49
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;->card_token:Ljava/lang/String;

    .line 50
    iput-object p2, p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;->deactivation_reason:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 65
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 66
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;

    .line 67
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;->card_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;->card_token:Ljava/lang/String;

    .line 68
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;->deactivation_reason:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;->deactivation_reason:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    .line 69
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 74
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 76
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 77
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;->card_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 78
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;->deactivation_reason:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 79
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;
    .locals 2

    .line 55
    new-instance v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;-><init>()V

    .line 56
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;->card_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;->card_token:Ljava/lang/String;

    .line 57
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;->deactivation_reason:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;->deactivation_reason:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    .line 58
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;->newBuilder()Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;->card_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", card_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;->card_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;->deactivation_reason:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    if-eqz v1, :cond_1

    const-string v1, ", deactivation_reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;->deactivation_reason:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DeactivateCardRequest{"

    .line 89
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
