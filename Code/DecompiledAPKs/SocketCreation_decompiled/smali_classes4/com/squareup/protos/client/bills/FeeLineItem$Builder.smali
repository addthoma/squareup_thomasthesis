.class public final Lcom/squareup/protos/client/bills/FeeLineItem$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FeeLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/FeeLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/FeeLineItem;",
        "Lcom/squareup/protos/client/bills/FeeLineItem$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

.field public created_at:Lcom/squareup/protos/client/ISO8601Date;

.field public fee_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

.field public read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

.field public read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

.field public write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

.field public write_only_deleted:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 188
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amounts(Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;
    .locals 0

    .line 225
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/FeeLineItem;
    .locals 10

    .line 245
    new-instance v9, Lcom/squareup/protos/client/bills/FeeLineItem;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->fee_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->write_only_deleted:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/bills/FeeLineItem;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;Lcom/squareup/protos/client/bills/TranslatedName;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 173
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->build()Lcom/squareup/protos/client/bills/FeeLineItem;

    move-result-object v0

    return-object v0
.end method

.method public created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;
    .locals 0

    .line 204
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public fee_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;
    .locals 0

    .line 195
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->fee_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public read_only_display_details(Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    return-object p0
.end method

.method public read_only_translated_name(Lcom/squareup/protos/client/bills/TranslatedName;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;
    .locals 0

    .line 230
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    return-object p0
.end method

.method public write_only_backing_details(Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;
    .locals 0

    .line 212
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    return-object p0
.end method

.method public write_only_deleted(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;
    .locals 0

    .line 239
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->write_only_deleted:Ljava/lang/Boolean;

    return-object p0
.end method
