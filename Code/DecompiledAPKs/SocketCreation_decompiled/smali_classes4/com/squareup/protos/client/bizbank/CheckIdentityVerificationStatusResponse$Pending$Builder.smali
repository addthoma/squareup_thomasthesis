.class public final Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CheckIdentityVerificationStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;",
        "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public poll_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 433
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;
    .locals 3

    .line 443
    new-instance v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending$Builder;->poll_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 430
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending$Builder;->build()Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    move-result-object v0

    return-object v0
.end method

.method public poll_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending$Builder;
    .locals 0

    .line 437
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending$Builder;->poll_token:Ljava/lang/String;

    return-object p0
.end method
