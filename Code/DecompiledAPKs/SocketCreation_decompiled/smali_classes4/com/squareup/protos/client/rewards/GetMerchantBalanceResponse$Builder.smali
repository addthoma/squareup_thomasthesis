.class public final Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetMerchantBalanceResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse;",
        "Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public status:Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse$Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 87
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse;
    .locals 3

    .line 103
    new-instance v0, Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse$Builder;->status:Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse$Status;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse;-><init>(Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse$Status;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse$Builder;->build()Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse;

    move-result-object v0

    return-object v0
.end method

.method public status(Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse$Status;)Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse$Builder;
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse$Builder;->status:Lcom/squareup/protos/client/rewards/GetMerchantBalanceResponse$Status;

    return-object p0
.end method
