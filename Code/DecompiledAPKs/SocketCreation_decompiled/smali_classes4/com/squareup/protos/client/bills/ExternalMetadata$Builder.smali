.class public final Lcom/squareup/protos/client/bills/ExternalMetadata$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ExternalMetadata.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ExternalMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/ExternalMetadata;",
        "Lcom/squareup/protos/client/bills/ExternalMetadata$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public content_type:Ljava/lang/String;

.field public metadata:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 104
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/ExternalMetadata;
    .locals 4

    .line 125
    new-instance v0, Lcom/squareup/protos/client/bills/ExternalMetadata;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ExternalMetadata$Builder;->content_type:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/ExternalMetadata$Builder;->metadata:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/ExternalMetadata;-><init>(Ljava/lang/String;Lokio/ByteString;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ExternalMetadata$Builder;->build()Lcom/squareup/protos/client/bills/ExternalMetadata;

    move-result-object v0

    return-object v0
.end method

.method public content_type(Ljava/lang/String;)Lcom/squareup/protos/client/bills/ExternalMetadata$Builder;
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ExternalMetadata$Builder;->content_type:Ljava/lang/String;

    return-object p0
.end method

.method public metadata(Lokio/ByteString;)Lcom/squareup/protos/client/bills/ExternalMetadata$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ExternalMetadata$Builder;->metadata:Lokio/ByteString;

    return-object p0
.end method
