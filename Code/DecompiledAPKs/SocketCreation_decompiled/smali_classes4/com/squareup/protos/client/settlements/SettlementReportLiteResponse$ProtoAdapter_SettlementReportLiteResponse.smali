.class final Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$ProtoAdapter_SettlementReportLiteResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SettlementReportLiteResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SettlementReportLiteResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 236
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 255
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;-><init>()V

    .line 256
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 257
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 262
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 260
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;->batch_response(Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;)Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;

    goto :goto_0

    .line 259
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;->settlements:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/settlements/SettlementReportLite;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 266
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 267
    invoke-virtual {v0}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;->build()Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 234
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$ProtoAdapter_SettlementReportLiteResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 248
    sget-object v0, Lcom/squareup/protos/client/settlements/SettlementReportLite;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->settlements:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 249
    sget-object v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->batch_response:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 250
    invoke-virtual {p2}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 234
    check-cast p2, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$ProtoAdapter_SettlementReportLiteResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;)I
    .locals 4

    .line 241
    sget-object v0, Lcom/squareup/protos/client/settlements/SettlementReportLite;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->settlements:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->batch_response:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    const/4 v3, 0x2

    .line 242
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 243
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 234
    check-cast p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$ProtoAdapter_SettlementReportLiteResponse;->encodedSize(Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;)Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;
    .locals 2

    .line 272
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->newBuilder()Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;

    move-result-object p1

    .line 273
    iget-object v0, p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;->settlements:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/settlements/SettlementReportLite;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 274
    iget-object v0, p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;->batch_response:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;->batch_response:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    iput-object v0, p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;->batch_response:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    .line 275
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 276
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;->build()Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 234
    check-cast p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$ProtoAdapter_SettlementReportLiteResponse;->redact(Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;)Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    move-result-object p1

    return-object p1
.end method
