.class public final Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;
.super Lcom/squareup/wire/Message;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Tender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoyaltyDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ProtoAdapter_LoyaltyDetails;,
        Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;,
        Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;,
        Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;",
        "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_NEW_ENROLLMENT:Ljava/lang/Boolean;

.field public static final DEFAULT_REASON:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

.field public static final DEFAULT_REWARDS_EARNED:Ljava/lang/Integer;

.field public static final DEFAULT_REWARD_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_STARS_EARNED:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final account_contact:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Tender$LoyaltyDetails$AccountContact#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final new_enrollment:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final reason:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Tender$LoyaltyDetails$ReasonForNoStars#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final reward_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final rewards_earned:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field

.field public final stars_earned:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 2016
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ProtoAdapter_LoyaltyDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ProtoAdapter_LoyaltyDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 2020
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->DEFAULT_STARS_EARNED:Ljava/lang/Integer;

    .line 2022
    sput-object v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->DEFAULT_REWARDS_EARNED:Ljava/lang/Integer;

    .line 2024
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->DEFAULT_NEW_ENROLLMENT:Ljava/lang/Boolean;

    .line 2028
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->UNKNOWN:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->DEFAULT_REASON:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;)V
    .locals 8

    .line 2086
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;Lokio/ByteString;)V
    .locals 1

    .line 2092
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 2093
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->stars_earned:Ljava/lang/Integer;

    .line 2094
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->rewards_earned:Ljava/lang/Integer;

    .line 2095
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->new_enrollment:Ljava/lang/Boolean;

    .line 2096
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->reward_name:Ljava/lang/String;

    .line 2097
    iput-object p5, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->reason:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    .line 2098
    iput-object p6, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->account_contact:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 2117
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2118
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    .line 2119
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->stars_earned:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->stars_earned:Ljava/lang/Integer;

    .line 2120
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->rewards_earned:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->rewards_earned:Ljava/lang/Integer;

    .line 2121
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->new_enrollment:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->new_enrollment:Ljava/lang/Boolean;

    .line 2122
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->reward_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->reward_name:Ljava/lang/String;

    .line 2123
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->reason:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->reason:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    .line 2124
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->account_contact:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->account_contact:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    .line 2125
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 2130
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 2132
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 2133
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->stars_earned:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2134
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->rewards_earned:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2135
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->new_enrollment:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2136
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->reward_name:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2137
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->reason:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2138
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->account_contact:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 2139
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;
    .locals 2

    .line 2103
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;-><init>()V

    .line 2104
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->stars_earned:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->stars_earned:Ljava/lang/Integer;

    .line 2105
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->rewards_earned:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->rewards_earned:Ljava/lang/Integer;

    .line 2106
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->new_enrollment:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->new_enrollment:Ljava/lang/Boolean;

    .line 2107
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->reward_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->reward_name:Ljava/lang/String;

    .line 2108
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->reason:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->reason:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    .line 2109
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->account_contact:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->account_contact:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    .line 2110
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 2015
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->newBuilder()Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 2146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2147
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->stars_earned:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", stars_earned="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->stars_earned:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2148
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->rewards_earned:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", rewards_earned="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->rewards_earned:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2149
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->new_enrollment:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", new_enrollment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->new_enrollment:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2150
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->reward_name:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", reward_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->reward_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2151
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->reason:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    if-eqz v1, :cond_4

    const-string v1, ", reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->reason:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2152
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->account_contact:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    if-eqz v1, :cond_5

    const-string v1, ", account_contact="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->account_contact:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "LoyaltyDetails{"

    .line 2153
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
