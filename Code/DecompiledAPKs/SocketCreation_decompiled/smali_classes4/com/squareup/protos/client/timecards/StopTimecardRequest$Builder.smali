.class public final Lcom/squareup/protos/client/timecards/StopTimecardRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StopTimecardRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/StopTimecardRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/StopTimecardRequest;",
        "Lcom/squareup/protos/client/timecards/StopTimecardRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public initiating_employee_token:Ljava/lang/String;

.field public should_also_end_breaks:Ljava/lang/Boolean;

.field public timecard_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 119
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/timecards/StopTimecardRequest;
    .locals 5

    .line 146
    new-instance v0, Lcom/squareup/protos/client/timecards/StopTimecardRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardRequest$Builder;->timecard_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/StopTimecardRequest$Builder;->should_also_end_breaks:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/client/timecards/StopTimecardRequest$Builder;->initiating_employee_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/timecards/StopTimecardRequest;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 112
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StopTimecardRequest$Builder;->build()Lcom/squareup/protos/client/timecards/StopTimecardRequest;

    move-result-object v0

    return-object v0
.end method

.method public initiating_employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/StopTimecardRequest$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardRequest$Builder;->initiating_employee_token:Ljava/lang/String;

    return-object p0
.end method

.method public should_also_end_breaks(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/StopTimecardRequest$Builder;
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardRequest$Builder;->should_also_end_breaks:Ljava/lang/Boolean;

    return-object p0
.end method

.method public timecard_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/StopTimecardRequest$Builder;
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardRequest$Builder;->timecard_token:Ljava/lang/String;

    return-object p0
.end method
