.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FulfillmentEntry"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$ProtoAdapter_FulfillmentEntry;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FULFILLMENT_ENTRY_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_QUANTITY:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final fulfillment_entry_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final itemization_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final quantity:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 5843
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$ProtoAdapter_FulfillmentEntry;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$ProtoAdapter_FulfillmentEntry;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;)V
    .locals 1

    .line 5878
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 5883
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 5884
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->fulfillment_entry_id:Ljava/lang/String;

    .line 5885
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 5886
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->quantity:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 5902
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 5903
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;

    .line 5904
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->fulfillment_entry_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->fulfillment_entry_id:Ljava/lang/String;

    .line 5905
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 5906
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->quantity:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->quantity:Ljava/lang/String;

    .line 5907
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 5912
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 5914
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 5915
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->fulfillment_entry_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5916
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5917
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->quantity:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 5918
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;
    .locals 2

    .line 5891
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;-><init>()V

    .line 5892
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->fulfillment_entry_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;->fulfillment_entry_id:Ljava/lang/String;

    .line 5893
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 5894
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->quantity:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;->quantity:Ljava/lang/String;

    .line 5895
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 5842
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 5925
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 5926
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->fulfillment_entry_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", fulfillment_entry_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->fulfillment_entry_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5927
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_1

    const-string v1, ", itemization_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5928
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->quantity:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->quantity:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FulfillmentEntry{"

    .line 5929
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
