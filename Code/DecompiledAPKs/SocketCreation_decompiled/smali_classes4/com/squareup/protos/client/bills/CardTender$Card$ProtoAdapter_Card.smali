.class final Lcom/squareup/protos/client/bills/CardTender$Card$ProtoAdapter_Card;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CardTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardTender$Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Card"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/CardTender$Card;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 727
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/CardTender$Card;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CardTender$Card;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 758
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;-><init>()V

    .line 759
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 760
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 799
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 797
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->felica_sprwid(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    goto :goto_0

    .line 796
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->felica_masked_card_number(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    goto :goto_0

    .line 790
    :pswitch_2
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->felica_brand(Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 792
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 787
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->write_only_can_swipe_chip_card(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    goto :goto_0

    .line 781
    :pswitch_4
    :try_start_1
    sget-object v4, Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->chip_card_fallback_indicator(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 783
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 778
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->pan_suffix(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    goto :goto_0

    .line 772
    :pswitch_6
    :try_start_2
    sget-object v4, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->brand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;
    :try_end_2
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v4

    .line 774
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 764
    :pswitch_7
    :try_start_3
    sget-object v4, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;
    :try_end_3
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v4

    .line 766
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 803
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 804
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->build()Lcom/squareup/protos/client/bills/CardTender$Card;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 725
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardTender$Card$ProtoAdapter_Card;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CardTender$Card;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CardTender$Card;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 745
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardTender$Card;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 746
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 747
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardTender$Card;->pan_suffix:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 748
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardTender$Card;->chip_card_fallback_indicator:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 749
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardTender$Card;->write_only_can_swipe_chip_card:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 750
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 751
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_masked_card_number:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 752
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_sprwid:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 753
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/CardTender$Card;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 725
    check-cast p2, Lcom/squareup/protos/client/bills/CardTender$Card;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/CardTender$Card$ProtoAdapter_Card;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CardTender$Card;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/CardTender$Card;)I
    .locals 4

    .line 732
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardTender$Card;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v3, 0x2

    .line 733
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardTender$Card;->pan_suffix:Ljava/lang/String;

    const/4 v3, 0x3

    .line 734
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardTender$Card;->chip_card_fallback_indicator:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    const/4 v3, 0x4

    .line 735
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardTender$Card;->write_only_can_swipe_chip_card:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 736
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    const/4 v3, 0x6

    .line 737
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_masked_card_number:Ljava/lang/String;

    const/4 v3, 0x7

    .line 738
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_sprwid:Ljava/lang/String;

    const/16 v3, 0x8

    .line 739
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 740
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardTender$Card;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 725
    check-cast p1, Lcom/squareup/protos/client/bills/CardTender$Card;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardTender$Card$ProtoAdapter_Card;->encodedSize(Lcom/squareup/protos/client/bills/CardTender$Card;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/CardTender$Card;)Lcom/squareup/protos/client/bills/CardTender$Card;
    .locals 1

    .line 809
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardTender$Card;->newBuilder()Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 810
    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->pan_suffix:Ljava/lang/String;

    .line 811
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 812
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->build()Lcom/squareup/protos/client/bills/CardTender$Card;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 725
    check-cast p1, Lcom/squareup/protos/client/bills/CardTender$Card;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardTender$Card$ProtoAdapter_Card;->redact(Lcom/squareup/protos/client/bills/CardTender$Card;)Lcom/squareup/protos/client/bills/CardTender$Card;

    move-result-object p1

    return-object p1
.end method
