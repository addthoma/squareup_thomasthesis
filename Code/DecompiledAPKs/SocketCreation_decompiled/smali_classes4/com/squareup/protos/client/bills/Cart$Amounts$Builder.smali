.class public final Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$Amounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$Amounts;",
        "Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public discount_money:Lcom/squareup/protos/common/Money;

.field public surcharge_money:Lcom/squareup/protos/common/Money;

.field public tax_money:Lcom/squareup/protos/common/Money;

.field public tip_money:Lcom/squareup/protos/common/Money;

.field public total_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 781
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$Amounts;
    .locals 8

    .line 829
    new-instance v7, Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->tax_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->discount_money:Lcom/squareup/protos/common/Money;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->surcharge_money:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bills/Cart$Amounts;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 770
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/Cart$Amounts;

    move-result-object v0

    return-object v0
.end method

.method public discount_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;
    .locals 0

    .line 807
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->discount_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public surcharge_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;
    .locals 0

    .line 823
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->surcharge_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public tax_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;
    .locals 0

    .line 799
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->tax_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;
    .locals 0

    .line 815
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;
    .locals 0

    .line 788
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->total_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
