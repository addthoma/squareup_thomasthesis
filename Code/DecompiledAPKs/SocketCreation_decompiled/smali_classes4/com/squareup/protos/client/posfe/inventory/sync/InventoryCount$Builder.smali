.class public final Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InventoryCount.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;",
        "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public current_count:Ljava/lang/Long;

.field public inventory_type:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;

.field public product_identifier:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

.field public state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;

.field public updated_at:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 157
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;
    .locals 8

    .line 202
    new-instance v7, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;

    iget-object v2, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->current_count:Ljava/lang/Long;

    iget-object v3, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->product_identifier:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

    iget-object v4, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->inventory_type:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;

    iget-object v5, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->updated_at:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;-><init>(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;Ljava/lang/Long;Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 146
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->build()Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

    move-result-object v0

    return-object v0
.end method

.method public current_count(Ljava/lang/Long;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->current_count:Ljava/lang/Long;

    return-object p0
.end method

.method public inventory_type(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->inventory_type:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryType;

    return-object p0
.end method

.method public product_identifier(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;
    .locals 0

    .line 180
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->product_identifier:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

    return-object p0
.end method

.method public state(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryState;

    return-object p0
.end method

.method public updated_at(Ljava/lang/Long;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;
    .locals 0

    .line 196
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount$Builder;->updated_at:Ljava/lang/Long;

    return-object p0
.end method
