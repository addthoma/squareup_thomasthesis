.class public final Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AssetUpdateRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;",
        "Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public libcardreader_comms_version:Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

.field public manifest:Lokio/ByteString;

.field public reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 114
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;
    .locals 5

    .line 138
    new-instance v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;->manifest:Lokio/ByteString;

    iget-object v2, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;->libcardreader_comms_version:Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    iget-object v3, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;-><init>(Lokio/ByteString;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;Lcom/squareup/protos/client/tarkin/ReaderType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;->build()Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;

    move-result-object v0

    return-object v0
.end method

.method public libcardreader_comms_version(Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;->libcardreader_comms_version:Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    return-object p0
.end method

.method public manifest(Lokio/ByteString;)Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;->manifest:Lokio/ByteString;

    return-object p0
.end method

.method public reader_type(Lcom/squareup/protos/client/tarkin/ReaderType;)Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    return-object p0
.end method
