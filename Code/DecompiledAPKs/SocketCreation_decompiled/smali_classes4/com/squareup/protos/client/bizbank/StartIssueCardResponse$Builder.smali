.class public final Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StartIssueCardResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;",
        "Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public address_normalization_result:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

.field public card_issue_token:Ljava/lang/String;

.field public corrected_field:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;",
            ">;"
        }
    .end annotation
.end field

.field public normalized_address:Lcom/squareup/protos/common/location/GlobalAddress;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 150
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 151
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;->corrected_field:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public address_normalization_result(Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;)Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;
    .locals 0

    .line 175
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;->address_normalization_result:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;
    .locals 8

    .line 188
    new-instance v7, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;->card_issue_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;->normalized_address:Lcom/squareup/protos/common/location/GlobalAddress;

    iget-object v4, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;->address_normalization_result:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    iget-object v5, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;->corrected_field:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;Ljava/util/List;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 139
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;

    move-result-object v0

    return-object v0
.end method

.method public card_issue_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;->card_issue_token:Ljava/lang/String;

    return-object p0
.end method

.method public corrected_field(Ljava/util/List;)Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;",
            ">;)",
            "Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;"
        }
    .end annotation

    .line 181
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 182
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;->corrected_field:Ljava/util/List;

    return-object p0
.end method

.method public normalized_address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;->normalized_address:Lcom/squareup/protos/common/location/GlobalAddress;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
