.class public final Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "EstimateDefaults.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/estimate/EstimateDefaults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/estimate/EstimateDefaults;",
        "Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

.field public message:Ljava/lang/String;

.field public relative_expiry_on:Ljava/lang/Integer;

.field public relative_send_on:Ljava/lang/Integer;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 142
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/estimate/EstimateDefaults;
    .locals 8

    .line 172
    new-instance v7, Lcom/squareup/protos/client/estimate/EstimateDefaults;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->title:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->message:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    iget-object v4, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->relative_send_on:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->relative_expiry_on:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/estimate/EstimateDefaults;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 131
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->build()Lcom/squareup/protos/client/estimate/EstimateDefaults;

    move-result-object v0

    return-object v0
.end method

.method public delivery_method(Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;)Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    return-object p0
.end method

.method public message(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->message:Ljava/lang/String;

    return-object p0
.end method

.method public relative_expiry_on(Ljava/lang/Integer;)Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->relative_expiry_on:Ljava/lang/Integer;

    return-object p0
.end method

.method public relative_send_on(Ljava/lang/Integer;)Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;
    .locals 0

    .line 161
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->relative_send_on:Ljava/lang/Integer;

    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->title:Ljava/lang/String;

    return-object p0
.end method
