.class public final Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;
.super Lcom/squareup/wire/Message;
.source "CheckIdentityVerificationStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$ProtoAdapter_CheckIdentityVerificationStatusResponse;,
        Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;,
        Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;,
        Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;,
        Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;,
        Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;",
        "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bizbank.CheckIdentityVerificationStatusResponse$Error#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final pending:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bizbank.CheckIdentityVerificationStatusResponse$Pending#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final response:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bizbank.CheckIdentityVerificationStatusResponse$Response#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$ProtoAdapter_CheckIdentityVerificationStatusResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$ProtoAdapter_CheckIdentityVerificationStatusResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;)V
    .locals 1

    .line 47
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;-><init>(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;Lokio/ByteString;)V
    .locals 1

    .line 52
    sget-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 53
    invoke-static {p1, p2, p3}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p4

    const/4 v0, 0x1

    if-gt p4, v0, :cond_0

    .line 56
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->pending:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    .line 57
    iput-object p2, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->response:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    .line 58
    iput-object p3, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

    return-void

    .line 54
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of pending, response, error may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 74
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 75
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;

    .line 76
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->pending:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->pending:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    .line 77
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->response:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->response:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    .line 78
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

    .line 79
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 84
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->pending:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->response:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 90
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;
    .locals 2

    .line 63
    new-instance v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;-><init>()V

    .line 64
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->pending:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->pending:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    .line 65
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->response:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->response:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    .line 66
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

    .line 67
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->newBuilder()Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->pending:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    if-eqz v1, :cond_0

    const-string v1, ", pending="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->pending:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Pending;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 99
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->response:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    if-eqz v1, :cond_1

    const-string v1, ", response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->response:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Response;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 100
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

    if-eqz v1, :cond_2

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;->error:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$Error;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CheckIdentityVerificationStatusResponse{"

    .line 101
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
