.class final Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$ProtoAdapter_CallToAction;
.super Lcom/squareup/wire/ProtoAdapter;
.source "InvoiceTimeline.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CallToAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 515
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 538
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;-><init>()V

    .line 539
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 540
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 554
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 552
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->call_to_action_link(Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;)Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;

    goto :goto_0

    .line 551
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->target_url(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;

    goto :goto_0

    .line 550
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->text(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;

    goto :goto_0

    .line 544
    :cond_3
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->target(Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;)Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 546
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 558
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 559
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 513
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$ProtoAdapter_CallToAction;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 529
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->target:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 530
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->text:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 531
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->target_url:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 532
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->call_to_action_link:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 533
    invoke-virtual {p2}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 513
    check-cast p2, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$ProtoAdapter_CallToAction;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;)I
    .locals 4

    .line 520
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->target:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->text:Ljava/lang/String;

    const/4 v3, 0x2

    .line 521
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->target_url:Ljava/lang/String;

    const/4 v3, 0x3

    .line 522
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->call_to_action_link:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    const/4 v3, 0x4

    .line 523
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 524
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 513
    check-cast p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$ProtoAdapter_CallToAction;->encodedSize(Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;)Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;
    .locals 2

    .line 564
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->newBuilder()Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;

    move-result-object p1

    .line 565
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->call_to_action_link:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->call_to_action_link:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->call_to_action_link:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    .line 566
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 567
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 513
    check-cast p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$ProtoAdapter_CallToAction;->redact(Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;)Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    move-result-object p1

    return-object p1
.end method
