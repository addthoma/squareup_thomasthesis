.class public final Lcom/squareup/protos/client/rolodex/UpsertNoteResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpsertNoteResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/UpsertNoteResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/UpsertNoteResponse;",
        "Lcom/squareup/protos/client/rolodex/UpsertNoteResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public note:Lcom/squareup/protos/client/rolodex/Note;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 94
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/UpsertNoteResponse;
    .locals 4

    .line 112
    new-instance v0, Lcom/squareup/protos/client/rolodex/UpsertNoteResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpsertNoteResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/UpsertNoteResponse$Builder;->note:Lcom/squareup/protos/client/rolodex/Note;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/rolodex/UpsertNoteResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/rolodex/Note;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/UpsertNoteResponse$Builder;->build()Lcom/squareup/protos/client/rolodex/UpsertNoteResponse;

    move-result-object v0

    return-object v0
.end method

.method public note(Lcom/squareup/protos/client/rolodex/Note;)Lcom/squareup/protos/client/rolodex/UpsertNoteResponse$Builder;
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpsertNoteResponse$Builder;->note:Lcom/squareup/protos/client/rolodex/Note;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/rolodex/UpsertNoteResponse$Builder;
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpsertNoteResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
