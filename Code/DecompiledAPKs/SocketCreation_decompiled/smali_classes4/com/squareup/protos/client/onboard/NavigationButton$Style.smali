.class public final enum Lcom/squareup/protos/client/onboard/NavigationButton$Style;
.super Ljava/lang/Enum;
.source "NavigationButton.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/onboard/NavigationButton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Style"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/onboard/NavigationButton$Style$ProtoAdapter_Style;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/onboard/NavigationButton$Style;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/onboard/NavigationButton$Style;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/onboard/NavigationButton$Style;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum BACK_ARROW:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

.field public static final enum PRIMARY:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

.field public static final enum SECONDARY:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

.field public static final enum STYLE_DO_NOT_USE:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

.field public static final enum X_MARK:Lcom/squareup/protos/client/onboard/NavigationButton$Style;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 136
    new-instance v0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    const/4 v1, 0x0

    const-string v2, "STYLE_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/onboard/NavigationButton$Style;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->STYLE_DO_NOT_USE:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    .line 138
    new-instance v0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    const/4 v2, 0x1

    const-string v3, "PRIMARY"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/onboard/NavigationButton$Style;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->PRIMARY:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    .line 140
    new-instance v0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    const/4 v3, 0x2

    const-string v4, "SECONDARY"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/onboard/NavigationButton$Style;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->SECONDARY:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    .line 142
    new-instance v0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    const/4 v4, 0x3

    const-string v5, "BACK_ARROW"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/onboard/NavigationButton$Style;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->BACK_ARROW:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    .line 144
    new-instance v0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    const/4 v5, 0x4

    const-string v6, "X_MARK"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/onboard/NavigationButton$Style;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->X_MARK:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    .line 135
    sget-object v6, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->STYLE_DO_NOT_USE:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->PRIMARY:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->SECONDARY:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->BACK_ARROW:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->X_MARK:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->$VALUES:[Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    .line 146
    new-instance v0, Lcom/squareup/protos/client/onboard/NavigationButton$Style$ProtoAdapter_Style;

    invoke-direct {v0}, Lcom/squareup/protos/client/onboard/NavigationButton$Style$ProtoAdapter_Style;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 150
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 151
    iput p3, p0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/onboard/NavigationButton$Style;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 163
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->X_MARK:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    return-object p0

    .line 162
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->BACK_ARROW:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    return-object p0

    .line 161
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->SECONDARY:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    return-object p0

    .line 160
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->PRIMARY:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    return-object p0

    .line 159
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->STYLE_DO_NOT_USE:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/NavigationButton$Style;
    .locals 1

    .line 135
    const-class v0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/onboard/NavigationButton$Style;
    .locals 1

    .line 135
    sget-object v0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->$VALUES:[Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/onboard/NavigationButton$Style;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 170
    iget v0, p0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->value:I

    return v0
.end method
