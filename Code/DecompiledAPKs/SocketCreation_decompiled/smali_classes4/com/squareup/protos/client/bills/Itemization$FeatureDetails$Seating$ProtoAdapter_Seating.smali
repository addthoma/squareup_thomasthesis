.class final Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$ProtoAdapter_Seating;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Seating"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 4608
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4625
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;-><init>()V

    .line 4626
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 4627
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 4631
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 4629
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;->destination(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;

    goto :goto_0

    .line 4635
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 4636
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4606
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$ProtoAdapter_Seating;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4619
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;->destination:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4620
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4606
    check-cast p2, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$ProtoAdapter_Seating;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;)I
    .locals 3

    .line 4613
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;->destination:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 4614
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 4606
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$ProtoAdapter_Seating;->encodedSize(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;
    .locals 2

    .line 4641
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;

    move-result-object p1

    .line 4642
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;->destination:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;->destination:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;->destination:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    .line 4643
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 4644
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 4606
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$ProtoAdapter_Seating;->redact(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    move-result-object p1

    return-object p1
.end method
