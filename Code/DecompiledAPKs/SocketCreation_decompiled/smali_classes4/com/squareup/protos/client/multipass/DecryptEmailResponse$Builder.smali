.class public final Lcom/squareup/protos/client/multipass/DecryptEmailResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DecryptEmailResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/multipass/DecryptEmailResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/multipass/DecryptEmailResponse;",
        "Lcom/squareup/protos/client/multipass/DecryptEmailResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public decrypted_email:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 84
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/multipass/DecryptEmailResponse;
    .locals 3

    .line 98
    new-instance v0, Lcom/squareup/protos/client/multipass/DecryptEmailResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/multipass/DecryptEmailResponse$Builder;->decrypted_email:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/multipass/DecryptEmailResponse;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 81
    invoke-virtual {p0}, Lcom/squareup/protos/client/multipass/DecryptEmailResponse$Builder;->build()Lcom/squareup/protos/client/multipass/DecryptEmailResponse;

    move-result-object v0

    return-object v0
.end method

.method public decrypted_email(Ljava/lang/String;)Lcom/squareup/protos/client/multipass/DecryptEmailResponse$Builder;
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/squareup/protos/client/multipass/DecryptEmailResponse$Builder;->decrypted_email:Ljava/lang/String;

    return-object p0
.end method
