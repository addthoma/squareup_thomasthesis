.class public final Lcom/squareup/protos/client/cbms/InformationRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InformationRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cbms/InformationRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/cbms/InformationRequest;",
        "Lcom/squareup/protos/client/cbms/InformationRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

.field public completed_at:Lcom/squareup/protos/common/time/DateTime;

.field public customer_due_at:Lcom/squareup/protos/common/time/DateTime;

.field public dashboard_form:Ljava/lang/Boolean;

.field public form_token:Ljava/lang/String;

.field public form_url:Ljava/lang/String;

.field public request_type:Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;

.field public requested_at:Lcom/squareup/protos/common/time/DateTime;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 195
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public actionable_status(Lcom/squareup/protos/client/cbms/ActionableStatus;)Lcom/squareup/protos/client/cbms/InformationRequest$Builder;
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/cbms/InformationRequest;
    .locals 11

    .line 249
    new-instance v10, Lcom/squareup/protos/client/cbms/InformationRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->form_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->form_url:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->request_type:Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;

    iget-object v4, p0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    iget-object v5, p0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->requested_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v6, p0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->customer_due_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v7, p0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->completed_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v8, p0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->dashboard_form:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/cbms/InformationRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;Lcom/squareup/protos/client/cbms/ActionableStatus;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 178
    invoke-virtual {p0}, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->build()Lcom/squareup/protos/client/cbms/InformationRequest;

    move-result-object v0

    return-object v0
.end method

.method public completed_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/cbms/InformationRequest$Builder;
    .locals 0

    .line 235
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->completed_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public customer_due_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/cbms/InformationRequest$Builder;
    .locals 0

    .line 230
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->customer_due_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public dashboard_form(Ljava/lang/Boolean;)Lcom/squareup/protos/client/cbms/InformationRequest$Builder;
    .locals 0

    .line 243
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->dashboard_form:Ljava/lang/Boolean;

    return-object p0
.end method

.method public form_token(Ljava/lang/String;)Lcom/squareup/protos/client/cbms/InformationRequest$Builder;
    .locals 0

    .line 202
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->form_token:Ljava/lang/String;

    return-object p0
.end method

.method public form_url(Ljava/lang/String;)Lcom/squareup/protos/client/cbms/InformationRequest$Builder;
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->form_url:Ljava/lang/String;

    return-object p0
.end method

.method public request_type(Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;)Lcom/squareup/protos/client/cbms/InformationRequest$Builder;
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->request_type:Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;

    return-object p0
.end method

.method public requested_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/cbms/InformationRequest$Builder;
    .locals 0

    .line 225
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->requested_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method
