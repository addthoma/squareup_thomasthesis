.class public final Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListManualMergeProposalResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse;",
        "Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public merge_proposal:Lcom/squareup/protos/client/rolodex/MergeProposal;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 92
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse;
    .locals 4

    .line 107
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse$Builder;->merge_proposal:Lcom/squareup/protos/client/rolodex/MergeProposal;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/rolodex/MergeProposal;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 87
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse$Builder;->build()Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse;

    move-result-object v0

    return-object v0
.end method

.method public merge_proposal(Lcom/squareup/protos/client/rolodex/MergeProposal;)Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse$Builder;
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse$Builder;->merge_proposal:Lcom/squareup/protos/client/rolodex/MergeProposal;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse$Builder;
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
