.class final Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$ProtoAdapter_GetUnitItemCountsResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetUnitItemCountsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetUnitItemCountsResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 241
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 258
    new-instance v0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$Builder;-><init>()V

    .line 259
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 260
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 264
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 262
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$Builder;->unit_item_counts:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 268
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 269
    invoke-virtual {v0}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$Builder;->build()Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 239
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$ProtoAdapter_GetUnitItemCountsResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 252
    sget-object v0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;->unit_item_counts:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 253
    invoke-virtual {p2}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 239
    check-cast p2, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$ProtoAdapter_GetUnitItemCountsResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;)I
    .locals 3

    .line 246
    sget-object v0, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;->unit_item_counts:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 247
    invoke-virtual {p1}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 239
    check-cast p1, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$ProtoAdapter_GetUnitItemCountsResponse;->encodedSize(Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;)Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;
    .locals 2

    .line 274
    invoke-virtual {p1}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;->newBuilder()Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$Builder;

    move-result-object p1

    .line 275
    iget-object v0, p1, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$Builder;->unit_item_counts:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$UnitItemCount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 276
    invoke-virtual {p1}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 277
    invoke-virtual {p1}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$Builder;->build()Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 239
    check-cast p1, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse$ProtoAdapter_GetUnitItemCountsResponse;->redact(Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;)Lcom/squareup/protos/client/retail/inventory/GetUnitItemCountsResponse;

    move-result-object p1

    return-object p1
.end method
