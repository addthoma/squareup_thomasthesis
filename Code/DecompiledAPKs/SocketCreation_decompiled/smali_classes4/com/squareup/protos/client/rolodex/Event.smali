.class public final Lcom/squareup/protos/client/rolodex/Event;
.super Lcom/squareup/wire/Message;
.source "Event.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/Event$ProtoAdapter_Event;,
        Lcom/squareup/protos/client/rolodex/Event$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/Event;",
        "Lcom/squareup/protos/client/rolodex/Event$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/Event;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_EVENT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_LINK_RELATIVE_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_SUBTITLE:Ljava/lang/String; = ""

.field public static final DEFAULT_TITLE:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/client/rolodex/EventType;

.field private static final serialVersionUID:J


# instance fields
.field public final event_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final link_relative_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final occurred_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final subtitle:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/client/rolodex/EventType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.EventType#ADAPTER"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/client/rolodex/Event$ProtoAdapter_Event;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Event$ProtoAdapter_Event;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 33
    sget-object v0, Lcom/squareup/protos/client/rolodex/EventType;->SKIPPED:Lcom/squareup/protos/client/rolodex/EventType;

    sput-object v0, Lcom/squareup/protos/client/rolodex/Event;->DEFAULT_TYPE:Lcom/squareup/protos/client/rolodex/EventType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/client/rolodex/EventType;)V
    .locals 8

    .line 73
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/rolodex/Event;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/client/rolodex/EventType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/client/rolodex/EventType;Lokio/ByteString;)V
    .locals 1

    .line 78
    sget-object v0, Lcom/squareup/protos/client/rolodex/Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 79
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Event;->event_token:Ljava/lang/String;

    .line 80
    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Event;->title:Ljava/lang/String;

    .line 81
    iput-object p3, p0, Lcom/squareup/protos/client/rolodex/Event;->subtitle:Ljava/lang/String;

    .line 82
    iput-object p4, p0, Lcom/squareup/protos/client/rolodex/Event;->link_relative_url:Ljava/lang/String;

    .line 83
    iput-object p5, p0, Lcom/squareup/protos/client/rolodex/Event;->occurred_at:Lcom/squareup/protos/common/time/DateTime;

    .line 84
    iput-object p6, p0, Lcom/squareup/protos/client/rolodex/Event;->type:Lcom/squareup/protos/client/rolodex/EventType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 103
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/Event;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 104
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/Event;

    .line 105
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Event;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Event;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->event_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Event;->event_token:Ljava/lang/String;

    .line 106
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Event;->title:Ljava/lang/String;

    .line 107
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->subtitle:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Event;->subtitle:Ljava/lang/String;

    .line 108
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->link_relative_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Event;->link_relative_url:Ljava/lang/String;

    .line 109
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->occurred_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Event;->occurred_at:Lcom/squareup/protos/common/time/DateTime;

    .line 110
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->type:Lcom/squareup/protos/client/rolodex/EventType;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Event;->type:Lcom/squareup/protos/client/rolodex/EventType;

    .line 111
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 116
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Event;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->event_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->title:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->subtitle:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->link_relative_url:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->occurred_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->type:Lcom/squareup/protos/client/rolodex/EventType;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/EventType;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 125
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/Event$Builder;
    .locals 2

    .line 89
    new-instance v0, Lcom/squareup/protos/client/rolodex/Event$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Event$Builder;-><init>()V

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->event_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Event$Builder;->event_token:Ljava/lang/String;

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Event$Builder;->title:Ljava/lang/String;

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->subtitle:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Event$Builder;->subtitle:Ljava/lang/String;

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->link_relative_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Event$Builder;->link_relative_url:Ljava/lang/String;

    .line 94
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->occurred_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Event$Builder;->occurred_at:Lcom/squareup/protos/common/time/DateTime;

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->type:Lcom/squareup/protos/client/rolodex/EventType;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Event$Builder;->type:Lcom/squareup/protos/client/rolodex/EventType;

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Event;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/Event$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Event;->newBuilder()Lcom/squareup/protos/client/rolodex/Event$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->event_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", event_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->event_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->title:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->subtitle:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", subtitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->subtitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->link_relative_url:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", link_relative_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->link_relative_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->occurred_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_4

    const-string v1, ", occurred_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->occurred_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 138
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->type:Lcom/squareup/protos/client/rolodex/EventType;

    if-eqz v1, :cond_5

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event;->type:Lcom/squareup/protos/client/rolodex/EventType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Event{"

    .line 139
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
