.class public final Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListInvoicesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/ListInvoicesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/ListInvoicesRequest;",
        "Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public check_archive_for_matches:Ljava/lang/Boolean;

.field public contact_token:Ljava/lang/String;

.field public date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

.field public display_state_filter:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;",
            ">;"
        }
    .end annotation
.end field

.field public include_archived:Ljava/lang/Boolean;

.field public invoice_id_pair:Lcom/squareup/protos/client/IdPair;

.field public limit:Ljava/lang/Integer;

.field public paging_key:Ljava/lang/String;

.field public parent_series_token:Ljava/lang/String;

.field public query:Ljava/lang/String;

.field public sort_ascending:Ljava/lang/Boolean;

.field public state_filter:Lcom/squareup/protos/client/invoice/StateFilter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 290
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 291
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->display_state_filter:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/ListInvoicesRequest;
    .locals 15

    .line 396
    new-instance v14, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->paging_key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->limit:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->query:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->display_state_filter:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->sort_ascending:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->invoice_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v7, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->state_filter:Lcom/squareup/protos/client/invoice/StateFilter;

    iget-object v8, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->contact_token:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    iget-object v10, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->parent_series_token:Ljava/lang/String;

    iget-object v11, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->check_archive_for_matches:Ljava/lang/Boolean;

    iget-object v12, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->include_archived:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v13

    move-object v0, v14

    invoke-direct/range {v0 .. v13}, Lcom/squareup/protos/client/invoice/ListInvoicesRequest;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/invoice/StateFilter;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTimeInterval;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v14
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 265
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->build()Lcom/squareup/protos/client/invoice/ListInvoicesRequest;

    move-result-object v0

    return-object v0
.end method

.method public check_archive_for_matches(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;
    .locals 0

    .line 380
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->check_archive_for_matches:Ljava/lang/Boolean;

    return-object p0
.end method

.method public contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;
    .locals 0

    .line 358
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->contact_token:Ljava/lang/String;

    return-object p0
.end method

.method public date_range(Lcom/squareup/protos/common/time/DateTimeInterval;)Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;
    .locals 0

    .line 363
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    return-object p0
.end method

.method public display_state_filter(Ljava/util/List;)Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;"
        }
    .end annotation

    .line 325
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 326
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->display_state_filter:Ljava/util/List;

    return-object p0
.end method

.method public include_archived(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;
    .locals 0

    .line 390
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->include_archived:Ljava/lang/Boolean;

    return-object p0
.end method

.method public invoice_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;
    .locals 0

    .line 342
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->invoice_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;
    .locals 0

    .line 307
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->limit:Ljava/lang/Integer;

    return-object p0
.end method

.method public paging_key(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;
    .locals 0

    .line 298
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->paging_key:Ljava/lang/String;

    return-object p0
.end method

.method public parent_series_token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;
    .locals 0

    .line 371
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->parent_series_token:Ljava/lang/String;

    return-object p0
.end method

.method public query(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;
    .locals 0

    .line 315
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->query:Ljava/lang/String;

    return-object p0
.end method

.method public sort_ascending(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;
    .locals 0

    .line 334
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->sort_ascending:Ljava/lang/Boolean;

    return-object p0
.end method

.method public state_filter(Lcom/squareup/protos/client/invoice/StateFilter;)Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;
    .locals 0

    .line 350
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesRequest$Builder;->state_filter:Lcom/squareup/protos/client/invoice/StateFilter;

    return-object p0
.end method
