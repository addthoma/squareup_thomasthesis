.class public final Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FeeLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;",
        "Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public backing_type:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;

.field public fee:Lcom/squareup/api/items/Fee;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 329
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public backing_type(Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;)Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;
    .locals 0

    .line 345
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;->backing_type:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;
    .locals 4

    .line 351
    new-instance v0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;->fee:Lcom/squareup/api/items/Fee;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;->backing_type:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;-><init>(Lcom/squareup/api/items/Fee;Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$BackingType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 324
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    move-result-object v0

    return-object v0
.end method

.method public fee(Lcom/squareup/api/items/Fee;)Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;
    .locals 0

    .line 336
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails$Builder;->fee:Lcom/squareup/api/items/Fee;

    return-object p0
.end method
