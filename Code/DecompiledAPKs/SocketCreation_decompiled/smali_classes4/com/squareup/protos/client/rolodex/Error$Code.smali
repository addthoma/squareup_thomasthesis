.class public final enum Lcom/squareup/protos/client/rolodex/Error$Code;
.super Ljava/lang/Enum;
.source "Error.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/Error;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Code"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/Error$Code$ProtoAdapter_Code;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/rolodex/Error$Code;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/rolodex/Error$Code;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/Error$Code;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum SUCCESS_CODE:Lcom/squareup/protos/client/rolodex/Error$Code;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 219
    new-instance v0, Lcom/squareup/protos/client/rolodex/Error$Code;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "SUCCESS_CODE"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/protos/client/rolodex/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Error$Code;->SUCCESS_CODE:Lcom/squareup/protos/client/rolodex/Error$Code;

    new-array v0, v1, [Lcom/squareup/protos/client/rolodex/Error$Code;

    .line 218
    sget-object v1, Lcom/squareup/protos/client/rolodex/Error$Code;->SUCCESS_CODE:Lcom/squareup/protos/client/rolodex/Error$Code;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/client/rolodex/Error$Code;->$VALUES:[Lcom/squareup/protos/client/rolodex/Error$Code;

    .line 221
    new-instance v0, Lcom/squareup/protos/client/rolodex/Error$Code$ProtoAdapter_Code;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Error$Code$ProtoAdapter_Code;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Error$Code;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 225
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 226
    iput p3, p0, Lcom/squareup/protos/client/rolodex/Error$Code;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/rolodex/Error$Code;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 234
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/rolodex/Error$Code;->SUCCESS_CODE:Lcom/squareup/protos/client/rolodex/Error$Code;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Error$Code;
    .locals 1

    .line 218
    const-class v0, Lcom/squareup/protos/client/rolodex/Error$Code;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/rolodex/Error$Code;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/rolodex/Error$Code;
    .locals 1

    .line 218
    sget-object v0, Lcom/squareup/protos/client/rolodex/Error$Code;->$VALUES:[Lcom/squareup/protos/client/rolodex/Error$Code;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/rolodex/Error$Code;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/rolodex/Error$Code;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 241
    iget v0, p0, Lcom/squareup/protos/client/rolodex/Error$Code;->value:I

    return v0
.end method
