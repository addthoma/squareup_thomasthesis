.class final Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$ProtoAdapter_Destination;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Destination"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 4551
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4572
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;-><init>()V

    .line 4573
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 4574
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 4587
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 4585
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;

    goto :goto_0

    .line 4584
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->seat_id_pair:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 4578
    :cond_2
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->type(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 4580
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 4591
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 4592
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4549
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$ProtoAdapter_Destination;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4564
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->type:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4565
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->seat_id_pair:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4566
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4567
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4549
    check-cast p2, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$ProtoAdapter_Destination;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;)I
    .locals 4

    .line 4556
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->type:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 4557
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->seat_id_pair:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x3

    .line 4558
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4559
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 4549
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$ProtoAdapter_Destination;->encodedSize(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;
    .locals 2

    .line 4597
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;

    move-result-object p1

    .line 4598
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->seat_id_pair:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 4599
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 4600
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 4601
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 4549
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$ProtoAdapter_Destination;->redact(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    move-result-object p1

    return-object p1
.end method
