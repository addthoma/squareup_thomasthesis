.class public final Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SwitchJobsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/SwitchJobsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/SwitchJobsRequest;",
        "Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public employee_token:Ljava/lang/String;

.field public job_token:Ljava/lang/String;

.field public timecard_token:Ljava/lang/String;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 126
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/timecards/SwitchJobsRequest;
    .locals 7

    .line 151
    new-instance v6, Lcom/squareup/protos/client/timecards/SwitchJobsRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;->employee_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;->timecard_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;->unit_token:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;->job_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/timecards/SwitchJobsRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;->build()Lcom/squareup/protos/client/timecards/SwitchJobsRequest;

    move-result-object v0

    return-object v0
.end method

.method public employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;->employee_token:Ljava/lang/String;

    return-object p0
.end method

.method public job_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;->job_token:Ljava/lang/String;

    return-object p0
.end method

.method public timecard_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;->timecard_token:Ljava/lang/String;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsRequest$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
