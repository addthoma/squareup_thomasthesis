.class public final Lcom/squareup/protos/client/orders/UpdateOrderResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpdateOrderResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/UpdateOrderResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/orders/UpdateOrderResponse;",
        "Lcom/squareup/protos/client/orders/UpdateOrderResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public order:Lcom/squareup/orders/model/Order;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 103
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/orders/UpdateOrderResponse;
    .locals 4

    .line 129
    new-instance v0, Lcom/squareup/protos/client/orders/UpdateOrderResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/orders/UpdateOrderResponse$Builder;->order:Lcom/squareup/orders/model/Order;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/orders/UpdateOrderResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/orders/model/Order;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 98
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/UpdateOrderResponse$Builder;->build()Lcom/squareup/protos/client/orders/UpdateOrderResponse;

    move-result-object v0

    return-object v0
.end method

.method public order(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/UpdateOrderResponse$Builder;
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/client/orders/UpdateOrderResponse$Builder;->order:Lcom/squareup/orders/model/Order;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/orders/UpdateOrderResponse$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/client/orders/UpdateOrderResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
