.class public final Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateCardCustomizationRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;",
        "Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public idempotence_key:Ljava/lang/String;

.field public image_bytes:Lokio/ByteString;

.field public mime_type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 112
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;
    .locals 5

    .line 132
    new-instance v0, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;->idempotence_key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;->image_bytes:Lokio/ByteString;

    iget-object v3, p0, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;->mime_type:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;-><init>(Ljava/lang/String;Lokio/ByteString;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 105
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;

    move-result-object v0

    return-object v0
.end method

.method public idempotence_key(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;->idempotence_key:Ljava/lang/String;

    return-object p0
.end method

.method public image_bytes(Lokio/ByteString;)Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;->image_bytes:Lokio/ByteString;

    return-object p0
.end method

.method public mime_type(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest$Builder;->mime_type:Ljava/lang/String;

    return-object p0
.end method
