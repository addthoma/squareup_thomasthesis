.class final Lcom/squareup/protos/client/timecards/TimecardBreak$ProtoAdapter_TimecardBreak;
.super Lcom/squareup/wire/ProtoAdapter;
.source "TimecardBreak.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/TimecardBreak;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_TimecardBreak"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/timecards/TimecardBreak;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 341
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/timecards/TimecardBreak;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/TimecardBreak;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 376
    new-instance v0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;-><init>()V

    .line 377
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 378
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 391
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 389
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->minimum_duration_seconds(Ljava/lang/Integer;)Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;

    goto :goto_0

    .line 388
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->expected_duration_seconds(Ljava/lang/Integer;)Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;

    goto :goto_0

    .line 387
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->break_name(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;

    goto :goto_0

    .line 386
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->is_paid(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;

    goto :goto_0

    .line 385
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->timecard_break_definition_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;

    goto :goto_0

    .line 384
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->end_timestamp(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;

    goto :goto_0

    .line 383
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->start_timestamp(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;

    goto :goto_0

    .line 382
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;

    goto :goto_0

    .line 381
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->timecard_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;

    goto :goto_0

    .line 380
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;

    goto/16 :goto_0

    .line 395
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 396
    invoke-virtual {v0}, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->build()Lcom/squareup/protos/client/timecards/TimecardBreak;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 339
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/TimecardBreak$ProtoAdapter_TimecardBreak;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/TimecardBreak;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/TimecardBreak;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 361
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/TimecardBreak;->token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 362
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/TimecardBreak;->timecard_token:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 363
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/TimecardBreak;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 364
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/TimecardBreak;->start_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 365
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/TimecardBreak;->end_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 366
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/TimecardBreak;->timecard_break_definition_token:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 367
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/TimecardBreak;->is_paid:Ljava/lang/Boolean;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 368
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/TimecardBreak;->break_name:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 369
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/TimecardBreak;->expected_duration_seconds:Ljava/lang/Integer;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 370
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/TimecardBreak;->minimum_duration_seconds:Ljava/lang/Integer;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 371
    invoke-virtual {p2}, Lcom/squareup/protos/client/timecards/TimecardBreak;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 339
    check-cast p2, Lcom/squareup/protos/client/timecards/TimecardBreak;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/timecards/TimecardBreak$ProtoAdapter_TimecardBreak;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/TimecardBreak;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/timecards/TimecardBreak;)I
    .locals 4

    .line 346
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;->token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;->timecard_token:Ljava/lang/String;

    const/4 v3, 0x2

    .line 347
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;->merchant_token:Ljava/lang/String;

    const/4 v3, 0x3

    .line 348
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;->start_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x4

    .line 349
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;->end_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x5

    .line 350
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;->timecard_break_definition_token:Ljava/lang/String;

    const/4 v3, 0x6

    .line 351
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;->is_paid:Ljava/lang/Boolean;

    const/4 v3, 0x7

    .line 352
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;->break_name:Ljava/lang/String;

    const/16 v3, 0x8

    .line 353
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;->expected_duration_seconds:Ljava/lang/Integer;

    const/16 v3, 0x9

    .line 354
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;->minimum_duration_seconds:Ljava/lang/Integer;

    const/16 v3, 0xa

    .line 355
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 356
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/TimecardBreak;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 339
    check-cast p1, Lcom/squareup/protos/client/timecards/TimecardBreak;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/TimecardBreak$ProtoAdapter_TimecardBreak;->encodedSize(Lcom/squareup/protos/client/timecards/TimecardBreak;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/timecards/TimecardBreak;)Lcom/squareup/protos/client/timecards/TimecardBreak;
    .locals 2

    .line 401
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/TimecardBreak;->newBuilder()Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;

    move-result-object p1

    .line 402
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->start_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->start_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->start_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    .line 403
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->end_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->end_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->end_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    .line 404
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 405
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->build()Lcom/squareup/protos/client/timecards/TimecardBreak;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 339
    check-cast p1, Lcom/squareup/protos/client/timecards/TimecardBreak;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/TimecardBreak$ProtoAdapter_TimecardBreak;->redact(Lcom/squareup/protos/client/timecards/TimecardBreak;)Lcom/squareup/protos/client/timecards/TimecardBreak;

    move-result-object p1

    return-object p1
.end method
