.class public final Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListConversationsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/dialogue/ListConversationsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/dialogue/ListConversationsRequest;",
        "Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public limit:Ljava/lang/Integer;

.field public list_options:Lcom/squareup/protos/client/dialogue/ListOptions;

.field public paging_key:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 120
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/dialogue/ListConversationsRequest;
    .locals 5

    .line 147
    new-instance v0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;->list_options:Lcom/squareup/protos/client/dialogue/ListOptions;

    iget-object v2, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;->limit:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;->paging_key:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;-><init>(Lcom/squareup/protos/client/dialogue/ListOptions;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 113
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;->build()Lcom/squareup/protos/client/dialogue/ListConversationsRequest;

    move-result-object v0

    return-object v0
.end method

.method public limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;->limit:Ljava/lang/Integer;

    return-object p0
.end method

.method public list_options(Lcom/squareup/protos/client/dialogue/ListOptions;)Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;->list_options:Lcom/squareup/protos/client/dialogue/ListOptions;

    return-object p0
.end method

.method public paging_key(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;->paging_key:Ljava/lang/String;

    return-object p0
.end method
