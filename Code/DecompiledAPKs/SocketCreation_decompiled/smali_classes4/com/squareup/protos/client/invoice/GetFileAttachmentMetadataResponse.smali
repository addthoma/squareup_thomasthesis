.class public final Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;
.super Lcom/squareup/wire/Message;
.source "GetFileAttachmentMetadataResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse$ProtoAdapter_GetFileAttachmentMetadataResponse;,
        Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;",
        "Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final attachment:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.FileAttachmentMetadata#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse$ProtoAdapter_GetFileAttachmentMetadataResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse$ProtoAdapter_GetFileAttachmentMetadataResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V
    .locals 1

    .line 38
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;Lokio/ByteString;)V
    .locals 1

    .line 43
    sget-object v0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 44
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;->status:Lcom/squareup/protos/client/Status;

    .line 45
    iput-object p2, p0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;->attachment:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 60
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 61
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;

    .line 62
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;->status:Lcom/squareup/protos/client/Status;

    .line 63
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;->attachment:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;->attachment:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    .line 64
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 69
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 71
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 72
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;->attachment:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 74
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse$Builder;
    .locals 2

    .line 50
    new-instance v0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse$Builder;-><init>()V

    .line 51
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;->status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 52
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;->attachment:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse$Builder;->attachment:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    .line 53
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;->newBuilder()Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 82
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 83
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;->attachment:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    if-eqz v1, :cond_1

    const-string v1, ", attachment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetFileAttachmentMetadataResponse;->attachment:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetFileAttachmentMetadataResponse{"

    .line 84
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
