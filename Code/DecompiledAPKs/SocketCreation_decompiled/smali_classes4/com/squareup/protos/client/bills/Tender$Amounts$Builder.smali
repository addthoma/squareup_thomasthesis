.class public final Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Tender$Amounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Tender$Amounts;",
        "Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public auto_gratuity_money:Lcom/squareup/protos/common/Money;

.field public auto_gratuity_percentage:Ljava/lang/String;

.field public surcharge_money:Lcom/squareup/protos/common/Money;

.field public tip_money:Lcom/squareup/protos/common/Money;

.field public tip_percentage:Ljava/lang/String;

.field public total_money:Lcom/squareup/protos/common/Money;

.field public write_only_tax_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1348
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public auto_gratuity_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;
    .locals 0

    .line 1407
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public auto_gratuity_percentage(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;
    .locals 0

    .line 1416
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->auto_gratuity_percentage:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/Tender$Amounts;
    .locals 10

    .line 1422
    new-instance v9, Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->write_only_tax_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->tip_percentage:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->surcharge_money:Lcom/squareup/protos/common/Money;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->auto_gratuity_percentage:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/bills/Tender$Amounts;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1333
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Amounts;

    move-result-object v0

    return-object v0
.end method

.method public surcharge_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;
    .locals 0

    .line 1398
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->surcharge_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;
    .locals 0

    .line 1355
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public tip_percentage(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;
    .locals 0

    .line 1390
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->tip_percentage:Ljava/lang/String;

    return-object p0
.end method

.method public total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;
    .locals 0

    .line 1375
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->total_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public write_only_tax_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1366
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->write_only_tax_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
