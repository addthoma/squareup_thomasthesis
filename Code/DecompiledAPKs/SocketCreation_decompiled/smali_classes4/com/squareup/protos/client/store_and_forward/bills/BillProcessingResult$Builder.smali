.class public final Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BillProcessingResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;",
        "Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill_or_payment_id:Lcom/squareup/protos/client/IdPair;

.field public status:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

.field public tender_id:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;"
        }
    .end annotation
.end field

.field public type:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 143
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 144
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;->tender_id:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bill_or_payment_id(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;->bill_or_payment_id:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;
    .locals 7

    .line 186
    new-instance v6, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;->type:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    iget-object v2, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;->bill_or_payment_id:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;->tender_id:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;->status:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;-><init>(Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;Lcom/squareup/protos/client/IdPair;Ljava/util/List;Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 134
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;->build()Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

    move-result-object v0

    return-object v0
.end method

.method public status(Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;)Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;
    .locals 0

    .line 180
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;->status:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    return-object p0
.end method

.method public tender_id(Ljava/util/List;)Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;)",
            "Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;"
        }
    .end annotation

    .line 174
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 175
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;->tender_id:Ljava/util/List;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;)Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;
    .locals 0

    .line 148
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;->type:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    return-object p0
.end method
