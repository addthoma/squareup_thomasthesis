.class public final enum Lcom/squareup/protos/client/bills/SquareProduct;
.super Ljava/lang/Enum;
.source "SquareProduct.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/SquareProduct$ProtoAdapter_SquareProduct;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/SquareProduct;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/SquareProduct;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/SquareProduct;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum APPOINTMENTS:Lcom/squareup/protos/client/bills/SquareProduct;

.field public static final enum BILLING:Lcom/squareup/protos/client/bills/SquareProduct;

.field public static final enum CAPITAL:Lcom/squareup/protos/client/bills/SquareProduct;

.field public static final enum CASH:Lcom/squareup/protos/client/bills/SquareProduct;

.field public static final enum EXTERNAL_API:Lcom/squareup/protos/client/bills/SquareProduct;

.field public static final enum INVOICES:Lcom/squareup/protos/client/bills/SquareProduct;

.field public static final enum MARKET:Lcom/squareup/protos/client/bills/SquareProduct;

.field public static final enum ONLINE_STORE:Lcom/squareup/protos/client/bills/SquareProduct;

.field public static final enum ORDER:Lcom/squareup/protos/client/bills/SquareProduct;

.field public static final enum PAYROLL:Lcom/squareup/protos/client/bills/SquareProduct;

.field public static final enum REGISTER:Lcom/squareup/protos/client/bills/SquareProduct;

.field public static final enum STARBUCKS:Lcom/squareup/protos/client/bills/SquareProduct;

.field public static final enum STORE:Lcom/squareup/protos/client/bills/SquareProduct;

.field public static final enum UNKNOWN_DO_NOT_SET:Lcom/squareup/protos/client/bills/SquareProduct;

.field public static final enum WHITE_LABEL:Lcom/squareup/protos/client/bills/SquareProduct;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 17
    new-instance v0, Lcom/squareup/protos/client/bills/SquareProduct;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_DO_NOT_SET"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/SquareProduct;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/SquareProduct;->UNKNOWN_DO_NOT_SET:Lcom/squareup/protos/client/bills/SquareProduct;

    .line 22
    new-instance v0, Lcom/squareup/protos/client/bills/SquareProduct;

    const/4 v2, 0x1

    const-string v3, "REGISTER"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/SquareProduct;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/SquareProduct;->REGISTER:Lcom/squareup/protos/client/bills/SquareProduct;

    .line 27
    new-instance v0, Lcom/squareup/protos/client/bills/SquareProduct;

    const/4 v3, 0x2

    const-string v4, "CASH"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/SquareProduct;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/SquareProduct;->CASH:Lcom/squareup/protos/client/bills/SquareProduct;

    .line 32
    new-instance v0, Lcom/squareup/protos/client/bills/SquareProduct;

    const/4 v4, 0x3

    const-string v5, "MARKET"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bills/SquareProduct;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/SquareProduct;->MARKET:Lcom/squareup/protos/client/bills/SquareProduct;

    .line 37
    new-instance v0, Lcom/squareup/protos/client/bills/SquareProduct;

    const/4 v5, 0x4

    const-string v6, "ORDER"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/bills/SquareProduct;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/SquareProduct;->ORDER:Lcom/squareup/protos/client/bills/SquareProduct;

    .line 42
    new-instance v0, Lcom/squareup/protos/client/bills/SquareProduct;

    const/4 v6, 0x5

    const-string v7, "EXTERNAL_API"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/bills/SquareProduct;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/SquareProduct;->EXTERNAL_API:Lcom/squareup/protos/client/bills/SquareProduct;

    .line 47
    new-instance v0, Lcom/squareup/protos/client/bills/SquareProduct;

    const/4 v7, 0x6

    const-string v8, "BILLING"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/bills/SquareProduct;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/SquareProduct;->BILLING:Lcom/squareup/protos/client/bills/SquareProduct;

    .line 52
    new-instance v0, Lcom/squareup/protos/client/bills/SquareProduct;

    const/4 v8, 0x7

    const-string v9, "APPOINTMENTS"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/bills/SquareProduct;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/SquareProduct;->APPOINTMENTS:Lcom/squareup/protos/client/bills/SquareProduct;

    .line 57
    new-instance v0, Lcom/squareup/protos/client/bills/SquareProduct;

    const/16 v9, 0x8

    const-string v10, "STORE"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/bills/SquareProduct;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/SquareProduct;->STORE:Lcom/squareup/protos/client/bills/SquareProduct;

    .line 62
    new-instance v0, Lcom/squareup/protos/client/bills/SquareProduct;

    const/16 v10, 0x9

    const-string v11, "INVOICES"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/client/bills/SquareProduct;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/SquareProduct;->INVOICES:Lcom/squareup/protos/client/bills/SquareProduct;

    .line 68
    new-instance v0, Lcom/squareup/protos/client/bills/SquareProduct;

    const/16 v11, 0xa

    const-string v12, "ONLINE_STORE"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/client/bills/SquareProduct;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/SquareProduct;->ONLINE_STORE:Lcom/squareup/protos/client/bills/SquareProduct;

    .line 73
    new-instance v0, Lcom/squareup/protos/client/bills/SquareProduct;

    const/16 v12, 0xb

    const-string v13, "STARBUCKS"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/client/bills/SquareProduct;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/SquareProduct;->STARBUCKS:Lcom/squareup/protos/client/bills/SquareProduct;

    .line 78
    new-instance v0, Lcom/squareup/protos/client/bills/SquareProduct;

    const/16 v13, 0xc

    const-string v14, "WHITE_LABEL"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/protos/client/bills/SquareProduct;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/SquareProduct;->WHITE_LABEL:Lcom/squareup/protos/client/bills/SquareProduct;

    .line 83
    new-instance v0, Lcom/squareup/protos/client/bills/SquareProduct;

    const/16 v14, 0xd

    const-string v15, "PAYROLL"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/protos/client/bills/SquareProduct;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/SquareProduct;->PAYROLL:Lcom/squareup/protos/client/bills/SquareProduct;

    .line 88
    new-instance v0, Lcom/squareup/protos/client/bills/SquareProduct;

    const/16 v15, 0xe

    const-string v14, "CAPITAL"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/protos/client/bills/SquareProduct;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/SquareProduct;->CAPITAL:Lcom/squareup/protos/client/bills/SquareProduct;

    const/16 v0, 0xf

    new-array v0, v0, [Lcom/squareup/protos/client/bills/SquareProduct;

    .line 13
    sget-object v14, Lcom/squareup/protos/client/bills/SquareProduct;->UNKNOWN_DO_NOT_SET:Lcom/squareup/protos/client/bills/SquareProduct;

    aput-object v14, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/SquareProduct;->REGISTER:Lcom/squareup/protos/client/bills/SquareProduct;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/SquareProduct;->CASH:Lcom/squareup/protos/client/bills/SquareProduct;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bills/SquareProduct;->MARKET:Lcom/squareup/protos/client/bills/SquareProduct;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bills/SquareProduct;->ORDER:Lcom/squareup/protos/client/bills/SquareProduct;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/bills/SquareProduct;->EXTERNAL_API:Lcom/squareup/protos/client/bills/SquareProduct;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/bills/SquareProduct;->BILLING:Lcom/squareup/protos/client/bills/SquareProduct;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/bills/SquareProduct;->APPOINTMENTS:Lcom/squareup/protos/client/bills/SquareProduct;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/bills/SquareProduct;->STORE:Lcom/squareup/protos/client/bills/SquareProduct;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/bills/SquareProduct;->INVOICES:Lcom/squareup/protos/client/bills/SquareProduct;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/client/bills/SquareProduct;->ONLINE_STORE:Lcom/squareup/protos/client/bills/SquareProduct;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/client/bills/SquareProduct;->STARBUCKS:Lcom/squareup/protos/client/bills/SquareProduct;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/client/bills/SquareProduct;->WHITE_LABEL:Lcom/squareup/protos/client/bills/SquareProduct;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/protos/client/bills/SquareProduct;->PAYROLL:Lcom/squareup/protos/client/bills/SquareProduct;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/SquareProduct;->CAPITAL:Lcom/squareup/protos/client/bills/SquareProduct;

    aput-object v1, v0, v15

    sput-object v0, Lcom/squareup/protos/client/bills/SquareProduct;->$VALUES:[Lcom/squareup/protos/client/bills/SquareProduct;

    .line 90
    new-instance v0, Lcom/squareup/protos/client/bills/SquareProduct$ProtoAdapter_SquareProduct;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/SquareProduct$ProtoAdapter_SquareProduct;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/SquareProduct;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 94
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 95
    iput p3, p0, Lcom/squareup/protos/client/bills/SquareProduct;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/SquareProduct;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 117
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/bills/SquareProduct;->CAPITAL:Lcom/squareup/protos/client/bills/SquareProduct;

    return-object p0

    .line 116
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/bills/SquareProduct;->PAYROLL:Lcom/squareup/protos/client/bills/SquareProduct;

    return-object p0

    .line 115
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/bills/SquareProduct;->WHITE_LABEL:Lcom/squareup/protos/client/bills/SquareProduct;

    return-object p0

    .line 114
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/bills/SquareProduct;->STARBUCKS:Lcom/squareup/protos/client/bills/SquareProduct;

    return-object p0

    .line 113
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/bills/SquareProduct;->ONLINE_STORE:Lcom/squareup/protos/client/bills/SquareProduct;

    return-object p0

    .line 112
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/bills/SquareProduct;->INVOICES:Lcom/squareup/protos/client/bills/SquareProduct;

    return-object p0

    .line 111
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/bills/SquareProduct;->STORE:Lcom/squareup/protos/client/bills/SquareProduct;

    return-object p0

    .line 110
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/bills/SquareProduct;->APPOINTMENTS:Lcom/squareup/protos/client/bills/SquareProduct;

    return-object p0

    .line 109
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/bills/SquareProduct;->BILLING:Lcom/squareup/protos/client/bills/SquareProduct;

    return-object p0

    .line 108
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/bills/SquareProduct;->EXTERNAL_API:Lcom/squareup/protos/client/bills/SquareProduct;

    return-object p0

    .line 107
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/client/bills/SquareProduct;->ORDER:Lcom/squareup/protos/client/bills/SquareProduct;

    return-object p0

    .line 106
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/client/bills/SquareProduct;->MARKET:Lcom/squareup/protos/client/bills/SquareProduct;

    return-object p0

    .line 105
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/client/bills/SquareProduct;->CASH:Lcom/squareup/protos/client/bills/SquareProduct;

    return-object p0

    .line 104
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/client/bills/SquareProduct;->REGISTER:Lcom/squareup/protos/client/bills/SquareProduct;

    return-object p0

    .line 103
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/client/bills/SquareProduct;->UNKNOWN_DO_NOT_SET:Lcom/squareup/protos/client/bills/SquareProduct;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/SquareProduct;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/client/bills/SquareProduct;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/SquareProduct;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/SquareProduct;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/client/bills/SquareProduct;->$VALUES:[Lcom/squareup/protos/client/bills/SquareProduct;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/SquareProduct;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/SquareProduct;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 124
    iget v0, p0, Lcom/squareup/protos/client/bills/SquareProduct;->value:I

    return v0
.end method
