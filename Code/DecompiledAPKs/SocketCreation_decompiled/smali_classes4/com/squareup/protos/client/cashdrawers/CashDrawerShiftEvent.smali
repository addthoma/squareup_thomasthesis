.class public final Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;
.super Lcom/squareup/wire/Message;
.source "CashDrawerShiftEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$ProtoAdapter_CashDrawerShiftEvent;,
        Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;,
        Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;",
        "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CLIENT_EVENT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CLIENT_PAYMENT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CLIENT_REFUND_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_EMPLOYEE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_EVENT_TYPE:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final client_event_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final client_payment_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final client_refund_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cashdrawers.EmployeeInfo#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final employee_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final event_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cashdrawers.CashDrawerShiftEvent$Type#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final occurred_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$ProtoAdapter_CashDrawerShiftEvent;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$ProtoAdapter_CashDrawerShiftEvent;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 37
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->UNKNOWN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->DEFAULT_EVENT_TYPE:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;)V
    .locals 11

    .line 116
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;Lokio/ByteString;)V
    .locals 1

    .line 122
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->client_event_id:Ljava/lang/String;

    .line 124
    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->employee_id:Ljava/lang/String;

    .line 125
    iput-object p3, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->description:Ljava/lang/String;

    .line 126
    iput-object p4, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    .line 127
    iput-object p5, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_money:Lcom/squareup/protos/common/Money;

    .line 128
    iput-object p6, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 129
    iput-object p7, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->client_payment_id:Ljava/lang/String;

    .line 130
    iput-object p8, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->client_refund_id:Ljava/lang/String;

    .line 131
    iput-object p9, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 153
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 154
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;

    .line 155
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->client_event_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->client_event_id:Ljava/lang/String;

    .line 156
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->employee_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->employee_id:Ljava/lang/String;

    .line 157
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->description:Ljava/lang/String;

    .line 158
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    .line 159
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_money:Lcom/squareup/protos/common/Money;

    .line 160
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 161
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->client_payment_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->client_payment_id:Ljava/lang/String;

    .line 162
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->client_refund_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->client_refund_id:Ljava/lang/String;

    .line 163
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    .line 164
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 169
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_9

    .line 171
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 172
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->client_event_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->employee_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 174
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->description:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 176
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->client_payment_id:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 179
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->client_refund_id:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 180
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    .line 181
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_9
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;
    .locals 2

    .line 136
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;-><init>()V

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->client_event_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->client_event_id:Ljava/lang/String;

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->employee_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->employee_id:Ljava/lang/String;

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->description:Ljava/lang/String;

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->event_money:Lcom/squareup/protos/common/Money;

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->client_payment_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->client_payment_id:Ljava/lang/String;

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->client_refund_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->client_refund_id:Ljava/lang/String;

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    .line 146
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->newBuilder()Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 189
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->client_event_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", client_event_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->client_event_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->employee_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", employee_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->employee_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->description:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", description=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    if-eqz v1, :cond_3

    const-string v1, ", event_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 193
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", event_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 194
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_5

    const-string v1, ", occurred_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 195
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->client_payment_id:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", client_payment_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->client_payment_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->client_refund_id:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", client_refund_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->client_refund_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    if-eqz v1, :cond_8

    const-string v1, ", employee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CashDrawerShiftEvent{"

    .line 198
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
