.class public final Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CloseCashDrawerShiftRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest;",
        "Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public client_cash_drawer_shift_id:Ljava/lang/String;

.field public client_unique_key:Ljava/lang/String;

.field public closed_at:Lcom/squareup/protos/client/ISO8601Date;

.field public closed_cash_money:Lcom/squareup/protos/common/Money;

.field public closing_employee_id:Ljava/lang/String;

.field public device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

.field public entered_description:Ljava/lang/String;

.field public merchant_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 204
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest;
    .locals 11

    .line 264
    new-instance v10, Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;->client_unique_key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;->merchant_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;->client_cash_drawer_shift_id:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;->closing_employee_id:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;->closed_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v6, p0, Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;->closed_cash_money:Lcom/squareup/protos/common/Money;

    iget-object v7, p0, Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iget-object v8, p0, Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;->entered_description:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/cashdrawers/DeviceInfo;Ljava/lang/String;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 187
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;->build()Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest;

    move-result-object v0

    return-object v0
.end method

.method public client_cash_drawer_shift_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;
    .locals 0

    .line 224
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;->client_cash_drawer_shift_id:Ljava/lang/String;

    return-object p0
.end method

.method public client_unique_key(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;
    .locals 0

    .line 211
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;->client_unique_key:Ljava/lang/String;

    return-object p0
.end method

.method public closed_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;
    .locals 0

    .line 234
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;->closed_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public closed_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;
    .locals 0

    .line 242
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;->closed_cash_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public closing_employee_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;
    .locals 0

    .line 229
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;->closing_employee_id:Ljava/lang/String;

    return-object p0
.end method

.method public device_info(Lcom/squareup/protos/client/cashdrawers/DeviceInfo;)Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;
    .locals 0

    .line 250
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    return-object p0
.end method

.method public entered_description(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;
    .locals 0

    .line 258
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;->entered_description:Ljava/lang/String;

    return-object p0
.end method

.method public merchant_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest$Builder;->merchant_id:Ljava/lang/String;

    return-object p0
.end method
