.class final Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$ProtoAdapter_ContactBatchActionStatus;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ContactBatchActionStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ContactBatchActionStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 120
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 139
    new-instance v0, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$Builder;-><init>()V

    .line 140
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 141
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 146
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 144
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$Builder;->total_error_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$Builder;

    goto :goto_0

    .line 143
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$Builder;->total_success_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$Builder;

    goto :goto_0

    .line 150
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 151
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$Builder;->build()Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 118
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$ProtoAdapter_ContactBatchActionStatus;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 132
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;->total_success_count:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 133
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;->total_error_count:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 134
    invoke-virtual {p2}, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 118
    check-cast p2, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$ProtoAdapter_ContactBatchActionStatus;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;)I
    .locals 4

    .line 125
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;->total_success_count:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;->total_error_count:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 126
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 127
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 118
    check-cast p1, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$ProtoAdapter_ContactBatchActionStatus;->encodedSize(Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;)Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;
    .locals 0

    .line 156
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;->newBuilder()Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$Builder;

    move-result-object p1

    .line 157
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 158
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$Builder;->build()Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 118
    check-cast p1, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$ProtoAdapter_ContactBatchActionStatus;->redact(Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;)Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;

    move-result-object p1

    return-object p1
.end method
