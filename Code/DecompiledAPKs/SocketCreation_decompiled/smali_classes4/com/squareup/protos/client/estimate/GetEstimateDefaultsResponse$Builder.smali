.class public final Lcom/squareup/protos/client/estimate/GetEstimateDefaultsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetEstimateDefaultsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/estimate/GetEstimateDefaultsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/estimate/GetEstimateDefaultsResponse;",
        "Lcom/squareup/protos/client/estimate/GetEstimateDefaultsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public estimate_defaults:Lcom/squareup/protos/client/estimate/EstimateDefaults;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 92
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/estimate/GetEstimateDefaultsResponse;
    .locals 4

    .line 107
    new-instance v0, Lcom/squareup/protos/client/estimate/GetEstimateDefaultsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/GetEstimateDefaultsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/estimate/GetEstimateDefaultsResponse$Builder;->estimate_defaults:Lcom/squareup/protos/client/estimate/EstimateDefaults;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/estimate/GetEstimateDefaultsResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/estimate/EstimateDefaults;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 87
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/GetEstimateDefaultsResponse$Builder;->build()Lcom/squareup/protos/client/estimate/GetEstimateDefaultsResponse;

    move-result-object v0

    return-object v0
.end method

.method public estimate_defaults(Lcom/squareup/protos/client/estimate/EstimateDefaults;)Lcom/squareup/protos/client/estimate/GetEstimateDefaultsResponse$Builder;
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/GetEstimateDefaultsResponse$Builder;->estimate_defaults:Lcom/squareup/protos/client/estimate/EstimateDefaults;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/estimate/GetEstimateDefaultsResponse$Builder;
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/GetEstimateDefaultsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
