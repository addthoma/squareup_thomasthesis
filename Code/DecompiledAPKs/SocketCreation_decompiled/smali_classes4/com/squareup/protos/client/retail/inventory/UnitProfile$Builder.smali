.class public final Lcom/squareup/protos/client/retail/inventory/UnitProfile$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UnitProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/retail/inventory/UnitProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/retail/inventory/UnitProfile;",
        "Lcom/squareup/protos/client/retail/inventory/UnitProfile$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public address:Lcom/squareup/protos/common/location/GlobalAddress;

.field public email_address:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public phone_number:Ljava/lang/String;

.field public token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 149
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/retail/inventory/UnitProfile$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/protos/client/retail/inventory/UnitProfile$Builder;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/retail/inventory/UnitProfile;
    .locals 8

    .line 182
    new-instance v7, Lcom/squareup/protos/client/retail/inventory/UnitProfile;

    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/UnitProfile$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/retail/inventory/UnitProfile$Builder;->token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/retail/inventory/UnitProfile$Builder;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    iget-object v4, p0, Lcom/squareup/protos/client/retail/inventory/UnitProfile$Builder;->phone_number:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/retail/inventory/UnitProfile$Builder;->email_address:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/retail/inventory/UnitProfile;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 138
    invoke-virtual {p0}, Lcom/squareup/protos/client/retail/inventory/UnitProfile$Builder;->build()Lcom/squareup/protos/client/retail/inventory/UnitProfile;

    move-result-object v0

    return-object v0
.end method

.method public email_address(Ljava/lang/String;)Lcom/squareup/protos/client/retail/inventory/UnitProfile$Builder;
    .locals 0

    .line 176
    iput-object p1, p0, Lcom/squareup/protos/client/retail/inventory/UnitProfile$Builder;->email_address:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/retail/inventory/UnitProfile$Builder;
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/squareup/protos/client/retail/inventory/UnitProfile$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/retail/inventory/UnitProfile$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/client/retail/inventory/UnitProfile$Builder;->phone_number:Ljava/lang/String;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/client/retail/inventory/UnitProfile$Builder;
    .locals 0

    .line 158
    iput-object p1, p0, Lcom/squareup/protos/client/retail/inventory/UnitProfile$Builder;->token:Ljava/lang/String;

    return-object p0
.end method
