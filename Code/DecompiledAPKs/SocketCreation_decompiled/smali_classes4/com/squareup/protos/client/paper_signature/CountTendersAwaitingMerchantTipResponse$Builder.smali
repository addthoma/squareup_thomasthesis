.class public final Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CountTendersAwaitingMerchantTipResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;",
        "Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error_message:Ljava/lang/String;

.field public error_title:Ljava/lang/String;

.field public next_request_backoff_seconds:Ljava/lang/Integer;

.field public success:Ljava/lang/Boolean;

.field public tender_awaiting_merchant_tip_count:Ljava/lang/Integer;

.field public tender_awaiting_merchant_tip_expiring_soon_age_seconds:Ljava/lang/Integer;

.field public tender_awaiting_merchant_tip_expiring_soon_count:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 207
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;
    .locals 10

    .line 277
    new-instance v9, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->success:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->error_message:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->error_title:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->tender_awaiting_merchant_tip_count:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->next_request_backoff_seconds:Ljava/lang/Integer;

    iget-object v6, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->tender_awaiting_merchant_tip_expiring_soon_count:Ljava/lang/Integer;

    iget-object v7, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->tender_awaiting_merchant_tip_expiring_soon_age_seconds:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 192
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->build()Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;

    move-result-object v0

    return-object v0
.end method

.method public error_message(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->error_message:Ljava/lang/String;

    return-object p0
.end method

.method public error_title(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;
    .locals 0

    .line 231
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->error_title:Ljava/lang/String;

    return-object p0
.end method

.method public next_request_backoff_seconds(Ljava/lang/Integer;)Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;
    .locals 0

    .line 250
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->next_request_backoff_seconds:Ljava/lang/Integer;

    return-object p0
.end method

.method public success(Ljava/lang/Boolean;)Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method public tender_awaiting_merchant_tip_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;
    .locals 0

    .line 240
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->tender_awaiting_merchant_tip_count:Ljava/lang/Integer;

    return-object p0
.end method

.method public tender_awaiting_merchant_tip_expiring_soon_age_seconds(Ljava/lang/Integer;)Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;
    .locals 0

    .line 271
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->tender_awaiting_merchant_tip_expiring_soon_age_seconds:Ljava/lang/Integer;

    return-object p0
.end method

.method public tender_awaiting_merchant_tip_expiring_soon_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;
    .locals 0

    .line 261
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->tender_awaiting_merchant_tip_expiring_soon_count:Ljava/lang/Integer;

    return-object p0
.end method
