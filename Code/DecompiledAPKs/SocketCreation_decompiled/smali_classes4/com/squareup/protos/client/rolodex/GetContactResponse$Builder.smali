.class public final Lcom/squareup/protos/client/rolodex/GetContactResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetContactResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/GetContactResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/GetContactResponse;",
        "Lcom/squareup/protos/client/rolodex/GetContactResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public contact:Lcom/squareup/protos/client/rolodex/Contact;

.field public loyalty_status:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;",
            ">;"
        }
    .end annotation
.end field

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 115
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 116
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/GetContactResponse$Builder;->loyalty_status:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/GetContactResponse;
    .locals 5

    .line 143
    new-instance v0, Lcom/squareup/protos/client/rolodex/GetContactResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetContactResponse$Builder;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/GetContactResponse$Builder;->loyalty_status:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/GetContactResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/rolodex/GetContactResponse;-><init>(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;Lcom/squareup/protos/client/Status;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 108
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/GetContactResponse$Builder;->build()Lcom/squareup/protos/client/rolodex/GetContactResponse;

    move-result-object v0

    return-object v0
.end method

.method public contact(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/protos/client/rolodex/GetContactResponse$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GetContactResponse$Builder;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object p0
.end method

.method public loyalty_status(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/GetContactResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/GetContactResponse$Builder;"
        }
    .end annotation

    .line 131
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GetContactResponse$Builder;->loyalty_status:Ljava/util/List;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/rolodex/GetContactResponse$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GetContactResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
