.class public final Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;
.super Lcom/squareup/wire/Message;
.source "GetCardActivityResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$ProtoAdapter_GetCardActivityResponse;,
        Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;,
        Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;",
        "Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final activity_list:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bizbank.CardActivityEvent#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;"
        }
    .end annotation
.end field

.field public final balance:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final batch_response:Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bizbank.GetCardActivityResponse$BatchResponse#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final fetched_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final max_instant_deposit_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$ProtoAdapter_GetCardActivityResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$ProtoAdapter_GetCardActivityResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;",
            "Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;",
            "Lcom/squareup/protos/client/Status;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            ")V"
        }
    .end annotation

    .line 80
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;-><init>(Ljava/util/List;Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;",
            "Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;",
            "Lcom/squareup/protos/client/Status;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 86
    sget-object v0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p7, "activity_list"

    .line 87
    invoke-static {p7, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->activity_list:Ljava/util/List;

    .line 88
    iput-object p2, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->batch_response:Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    .line 89
    iput-object p3, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->status:Lcom/squareup/protos/client/Status;

    .line 90
    iput-object p4, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->fetched_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 91
    iput-object p5, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->balance:Lcom/squareup/protos/common/Money;

    .line 92
    iput-object p6, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 111
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 112
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;

    .line 113
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->activity_list:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->activity_list:Ljava/util/List;

    .line 114
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->batch_response:Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->batch_response:Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    .line 115
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->status:Lcom/squareup/protos/client/Status;

    .line 116
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->fetched_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->fetched_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 117
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->balance:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->balance:Lcom/squareup/protos/common/Money;

    .line 118
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    .line 119
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 124
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 126
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->activity_list:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->batch_response:Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 130
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->fetched_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 131
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->balance:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 133
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;
    .locals 2

    .line 97
    new-instance v0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;-><init>()V

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->activity_list:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->activity_list:Ljava/util/List;

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->batch_response:Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->batch_response:Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->fetched_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->fetched_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->balance:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->balance:Lcom/squareup/protos/common/Money;

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    .line 104
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->newBuilder()Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->activity_list:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", activity_list="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->activity_list:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 142
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->batch_response:Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    if-eqz v1, :cond_1

    const-string v1, ", batch_response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->batch_response:Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 143
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_2

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 144
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->fetched_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_3

    const-string v1, ", fetched_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->fetched_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 145
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->balance:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", balance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->balance:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 146
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    const-string v1, ", max_instant_deposit_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetCardActivityResponse{"

    .line 147
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
