.class public final enum Lcom/squareup/protos/client/Signature$DataType;
.super Ljava/lang/Enum;
.source "Signature.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/Signature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DataType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/Signature$DataType$ProtoAdapter_DataType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/Signature$DataType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/Signature$DataType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/Signature$DataType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/Signature$DataType;

.field public static final enum VECTOR_V1:Lcom/squareup/protos/client/Signature$DataType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 128
    new-instance v0, Lcom/squareup/protos/client/Signature$DataType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/Signature$DataType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/Signature$DataType;->UNKNOWN:Lcom/squareup/protos/client/Signature$DataType;

    .line 133
    new-instance v0, Lcom/squareup/protos/client/Signature$DataType;

    const/4 v2, 0x1

    const-string v3, "VECTOR_V1"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/Signature$DataType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/Signature$DataType;->VECTOR_V1:Lcom/squareup/protos/client/Signature$DataType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/protos/client/Signature$DataType;

    .line 124
    sget-object v3, Lcom/squareup/protos/client/Signature$DataType;->UNKNOWN:Lcom/squareup/protos/client/Signature$DataType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/client/Signature$DataType;->VECTOR_V1:Lcom/squareup/protos/client/Signature$DataType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/client/Signature$DataType;->$VALUES:[Lcom/squareup/protos/client/Signature$DataType;

    .line 135
    new-instance v0, Lcom/squareup/protos/client/Signature$DataType$ProtoAdapter_DataType;

    invoke-direct {v0}, Lcom/squareup/protos/client/Signature$DataType$ProtoAdapter_DataType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/Signature$DataType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 139
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 140
    iput p3, p0, Lcom/squareup/protos/client/Signature$DataType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/Signature$DataType;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 149
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/Signature$DataType;->VECTOR_V1:Lcom/squareup/protos/client/Signature$DataType;

    return-object p0

    .line 148
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/Signature$DataType;->UNKNOWN:Lcom/squareup/protos/client/Signature$DataType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/Signature$DataType;
    .locals 1

    .line 124
    const-class v0, Lcom/squareup/protos/client/Signature$DataType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/Signature$DataType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/Signature$DataType;
    .locals 1

    .line 124
    sget-object v0, Lcom/squareup/protos/client/Signature$DataType;->$VALUES:[Lcom/squareup/protos/client/Signature$DataType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/Signature$DataType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/Signature$DataType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 156
    iget v0, p0, Lcom/squareup/protos/client/Signature$DataType;->value:I

    return v0
.end method
