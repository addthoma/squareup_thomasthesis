.class public final Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;
.super Lcom/squareup/wire/Message;
.source "DayOfWeekDepositSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$ProtoAdapter_DayOfWeekDepositSettings;,
        Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;",
        "Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DAY_OF_WEEK:Lcom/squareup/protos/common/time/DayOfWeek;

.field private static final serialVersionUID:J


# instance fields
.field public final day_of_week:Lcom/squareup/protos/common/time/DayOfWeek;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DayOfWeek#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.depositsettings.DepositInterval#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$ProtoAdapter_DayOfWeekDepositSettings;

    invoke-direct {v0}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$ProtoAdapter_DayOfWeekDepositSettings;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lcom/squareup/protos/common/time/DayOfWeek;->UNKNOWN_DO_NOT_USE:Lcom/squareup/protos/common/time/DayOfWeek;

    sput-object v0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->DEFAULT_DAY_OF_WEEK:Lcom/squareup/protos/common/time/DayOfWeek;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/time/DayOfWeek;Lcom/squareup/protos/client/depositsettings/DepositInterval;)V
    .locals 1

    .line 51
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;-><init>(Lcom/squareup/protos/common/time/DayOfWeek;Lcom/squareup/protos/client/depositsettings/DepositInterval;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/time/DayOfWeek;Lcom/squareup/protos/client/depositsettings/DepositInterval;Lokio/ByteString;)V
    .locals 1

    .line 56
    sget-object v0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 57
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->day_of_week:Lcom/squareup/protos/common/time/DayOfWeek;

    .line 58
    iput-object p2, p0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 73
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 74
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->day_of_week:Lcom/squareup/protos/common/time/DayOfWeek;

    iget-object v3, p1, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->day_of_week:Lcom/squareup/protos/common/time/DayOfWeek;

    .line 76
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;

    iget-object p1, p1, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;

    .line 77
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 82
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->day_of_week:Lcom/squareup/protos/common/time/DayOfWeek;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DayOfWeek;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/depositsettings/DepositInterval;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 87
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;
    .locals 2

    .line 63
    new-instance v0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;-><init>()V

    .line 64
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->day_of_week:Lcom/squareup/protos/common/time/DayOfWeek;

    iput-object v1, v0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;->day_of_week:Lcom/squareup/protos/common/time/DayOfWeek;

    .line 65
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;

    iput-object v1, v0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;->deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;

    .line 66
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->newBuilder()Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->day_of_week:Lcom/squareup/protos/common/time/DayOfWeek;

    if-eqz v1, :cond_0

    const-string v1, ", day_of_week="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->day_of_week:Lcom/squareup/protos/common/time/DayOfWeek;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 96
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;

    if-eqz v1, :cond_1

    const-string v1, ", deposit_interval="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DayOfWeekDepositSettings{"

    .line 97
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
