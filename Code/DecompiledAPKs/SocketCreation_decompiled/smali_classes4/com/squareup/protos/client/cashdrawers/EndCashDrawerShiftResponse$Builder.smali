.class public final Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "EndCashDrawerShiftResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftResponse;",
        "Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public status:Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftResponse$Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 81
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftResponse;
    .locals 3

    .line 91
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftResponse$Builder;->status:Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftResponse$Status;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftResponse;-><init>(Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftResponse$Status;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftResponse$Builder;->build()Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftResponse;

    move-result-object v0

    return-object v0
.end method

.method public status(Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftResponse$Status;)Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftResponse$Builder;
    .locals 0

    .line 85
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftResponse$Builder;->status:Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftResponse$Status;

    return-object p0
.end method
