.class public final Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;
.super Lcom/squareup/wire/Message;
.source "PerformFulfillmentActionRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$ProtoAdapter_PerformFulfillmentActionRequest;,
        Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;",
        "Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ORDER_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_ORDER_VERSION:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final action:Lcom/squareup/protos/client/orders/FulfillmentAction;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.orders.FulfillmentAction#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final client_support:Lcom/squareup/protos/client/orders/ClientSupport;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.orders.ClientSupport#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final order_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final order_version:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$ProtoAdapter_PerformFulfillmentActionRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$ProtoAdapter_PerformFulfillmentActionRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 27
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->DEFAULT_ORDER_VERSION:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/orders/ClientSupport;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/protos/client/orders/FulfillmentAction;)V
    .locals 6

    .line 55
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;-><init>(Lcom/squareup/protos/client/orders/ClientSupport;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/protos/client/orders/FulfillmentAction;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/orders/ClientSupport;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/protos/client/orders/FulfillmentAction;Lokio/ByteString;)V
    .locals 1

    .line 60
    sget-object v0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 61
    iput-object p1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    .line 62
    iput-object p2, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->order_id:Ljava/lang/String;

    .line 63
    iput-object p3, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->order_version:Ljava/lang/Integer;

    .line 64
    iput-object p4, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->action:Lcom/squareup/protos/client/orders/FulfillmentAction;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 81
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 82
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;

    .line 83
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    .line 84
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->order_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->order_id:Ljava/lang/String;

    .line 85
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->order_version:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->order_version:Ljava/lang/Integer;

    .line 86
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->action:Lcom/squareup/protos/client/orders/FulfillmentAction;

    iget-object p1, p1, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->action:Lcom/squareup/protos/client/orders/FulfillmentAction;

    .line 87
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 92
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/ClientSupport;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->order_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->order_version:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->action:Lcom/squareup/protos/client/orders/FulfillmentAction;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/FulfillmentAction;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 99
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;
    .locals 2

    .line 69
    new-instance v0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;-><init>()V

    .line 70
    iget-object v1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    .line 71
    iget-object v1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->order_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;->order_id:Ljava/lang/String;

    .line 72
    iget-object v1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->order_version:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;->order_version:Ljava/lang/Integer;

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->action:Lcom/squareup/protos/client/orders/FulfillmentAction;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;->action:Lcom/squareup/protos/client/orders/FulfillmentAction;

    .line 74
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->newBuilder()Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    if-eqz v1, :cond_0

    const-string v1, ", client_support="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 108
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->order_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", order_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->order_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->order_version:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", order_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->order_version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 110
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->action:Lcom/squareup/protos/client/orders/FulfillmentAction;

    if-eqz v1, :cond_3

    const-string v1, ", action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;->action:Lcom/squareup/protos/client/orders/FulfillmentAction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PerformFulfillmentActionRequest{"

    .line 111
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
