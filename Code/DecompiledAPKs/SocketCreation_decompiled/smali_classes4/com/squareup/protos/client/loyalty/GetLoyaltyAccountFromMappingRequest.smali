.class public final Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;
.super Lcom/squareup/wire/Message;
.source "GetLoyaltyAccountFromMappingRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$ProtoAdapter_GetLoyaltyAccountFromMappingRequest;,
        Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyAccountMapping#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyOptions#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$ProtoAdapter_GetLoyaltyAccountFromMappingRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$ProtoAdapter_GetLoyaltyAccountFromMappingRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;Lcom/squareup/protos/client/loyalty/LoyaltyOptions;)V
    .locals 1

    .line 51
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;-><init>(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;Lcom/squareup/protos/client/loyalty/LoyaltyOptions;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;Lcom/squareup/protos/client/loyalty/LoyaltyOptions;Lokio/ByteString;)V
    .locals 1

    .line 56
    sget-object v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 57
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    .line 58
    iput-object p2, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 73
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 74
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    .line 76
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    .line 77
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 82
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 87
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;
    .locals 2

    .line 63
    new-instance v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;-><init>()V

    .line 64
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    .line 65
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    .line 66
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->newBuilder()Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    if-eqz v1, :cond_0

    const-string v1, ", loyalty_account_mapping="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 96
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    if-eqz v1, :cond_1

    const-string v1, ", options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetLoyaltyAccountFromMappingRequest{"

    .line 97
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
