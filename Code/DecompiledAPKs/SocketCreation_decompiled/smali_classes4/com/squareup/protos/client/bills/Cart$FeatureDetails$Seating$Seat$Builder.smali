.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public created_at:Lcom/squareup/protos/client/ISO8601Date;

.field public enabled:Ljava/lang/Boolean;

.field public ordinal:Ljava/lang/Integer;

.field public seat_id_pair:Lcom/squareup/protos/client/IdPair;

.field public source_open_ticket_client_id:Ljava/lang/String;

.field public source_open_ticket_name:Ljava/lang/String;

.field public source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4370
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;
    .locals 10

    .line 4432
    new-instance v9, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->seat_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->ordinal:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->enabled:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->source_open_ticket_client_id:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->source_open_ticket_name:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 4355
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    move-result-object v0

    return-object v0
.end method

.method public created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;
    .locals 0

    .line 4401
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;
    .locals 0

    .line 4393
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;
    .locals 0

    .line 4385
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->ordinal:Ljava/lang/Integer;

    return-object p0
.end method

.method public seat_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;
    .locals 0

    .line 4377
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->seat_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public source_open_ticket_client_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;
    .locals 0

    .line 4409
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->source_open_ticket_client_id:Ljava/lang/String;

    return-object p0
.end method

.method public source_open_ticket_name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;
    .locals 0

    .line 4414
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->source_open_ticket_name:Ljava/lang/String;

    return-object p0
.end method

.method public source_ticket_information(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;
    .locals 0

    .line 4426
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    return-object p0
.end method
