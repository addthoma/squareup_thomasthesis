.class final Lcom/squareup/protos/client/State$ProtoAdapter_State;
.super Lcom/squareup/wire/EnumAdapter;
.source "State.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/State;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/client/State;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 46
    const-class v0, Lcom/squareup/protos/client/State;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/client/State;
    .locals 0

    .line 51
    invoke-static {p1}, Lcom/squareup/protos/client/State;->fromValue(I)Lcom/squareup/protos/client/State;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 44
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/State$ProtoAdapter_State;->fromValue(I)Lcom/squareup/protos/client/State;

    move-result-object p1

    return-object p1
.end method
