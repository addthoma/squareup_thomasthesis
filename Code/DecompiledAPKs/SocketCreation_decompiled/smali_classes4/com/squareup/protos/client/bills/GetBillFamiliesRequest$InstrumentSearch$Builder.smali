.class public final Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetBillFamiliesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 373
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;
    .locals 4

    .line 394
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;-><init>(Lcom/squareup/protos/client/bills/PaymentInstrument;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 368
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;->build()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;

    move-result-object v0

    return-object v0
.end method

.method public entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;
    .locals 0

    .line 388
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p0
.end method

.method public payment_instrument(Lcom/squareup/protos/client/bills/PaymentInstrument;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;
    .locals 0

    .line 380
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    return-object p0
.end method
