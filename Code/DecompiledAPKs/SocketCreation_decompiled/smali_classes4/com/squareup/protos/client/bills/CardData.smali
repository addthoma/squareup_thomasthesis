.class public final Lcom/squareup/protos/client/bills/CardData;
.super Lcom/squareup/wire/Message;
.source "CardData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CardData$ProtoAdapter_CardData;,
        Lcom/squareup/protos/client/bills/CardData$ReaderType;,
        Lcom/squareup/protos/client/bills/CardData$ServerCompleted;,
        Lcom/squareup/protos/client/bills/CardData$S3Card;,
        Lcom/squareup/protos/client/bills/CardData$T2Card;,
        Lcom/squareup/protos/client/bills/CardData$MCRCard;,
        Lcom/squareup/protos/client/bills/CardData$X2Card;,
        Lcom/squareup/protos/client/bills/CardData$R12Card;,
        Lcom/squareup/protos/client/bills/CardData$A10Card;,
        Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;,
        Lcom/squareup/protos/client/bills/CardData$R6Card;,
        Lcom/squareup/protos/client/bills/CardData$R4Card;,
        Lcom/squareup/protos/client/bills/CardData$S1Card;,
        Lcom/squareup/protos/client/bills/CardData$O1Card;,
        Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;,
        Lcom/squareup/protos/client/bills/CardData$KeyedCard;,
        Lcom/squareup/protos/client/bills/CardData$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/CardData;",
        "Lcom/squareup/protos/client/bills/CardData$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CardData;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_READER_TYPE:Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field private static final serialVersionUID:J


# instance fields
.field public final a10_card:Lcom/squareup/protos/client/bills/CardData$A10Card;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardData$A10Card#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final encrypted_keyed_card:Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardData$EncryptedKeyedCard#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final keyed_card:Lcom/squareup/protos/client/bills/CardData$KeyedCard;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardData$KeyedCard#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final mcr_card:Lcom/squareup/protos/client/bills/CardData$MCRCard;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardData$MCRCard#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final o1_card:Lcom/squareup/protos/client/bills/CardData$O1Card;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardData$O1Card#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final r12_card:Lcom/squareup/protos/client/bills/CardData$R12Card;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardData$R12Card#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final r4_card:Lcom/squareup/protos/client/bills/CardData$R4Card;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardData$R4Card#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final r6_card:Lcom/squareup/protos/client/bills/CardData$R6Card;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardData$R6Card#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final reader_type:Lcom/squareup/protos/client/bills/CardData$ReaderType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardData$ReaderType#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final s1_card:Lcom/squareup/protos/client/bills/CardData$S1Card;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardData$S1Card#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final s3_card:Lcom/squareup/protos/client/bills/CardData$S3Card;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardData$S3Card#ADAPTER"
        tag = 0xf
    .end annotation
.end field

.field public final server_completed:Lcom/squareup/protos/client/bills/CardData$ServerCompleted;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardData$ServerCompleted#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final t2_card:Lcom/squareup/protos/client/bills/CardData$T2Card;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardData$T2Card#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final unencrypted_card:Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardData$UnencryptedCard#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final x2_card:Lcom/squareup/protos/client/bills/CardData$X2Card;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardData$X2Card#ADAPTER"
        tag = 0xb
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$ProtoAdapter_CardData;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$ProtoAdapter_CardData;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 27
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->UNKNOWN:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sput-object v0, Lcom/squareup/protos/client/bills/CardData;->DEFAULT_READER_TYPE:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lcom/squareup/protos/client/bills/CardData$KeyedCard;Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;Lcom/squareup/protos/client/bills/CardData$O1Card;Lcom/squareup/protos/client/bills/CardData$S1Card;Lcom/squareup/protos/client/bills/CardData$R4Card;Lcom/squareup/protos/client/bills/CardData$R6Card;Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;Lcom/squareup/protos/client/bills/CardData$A10Card;Lcom/squareup/protos/client/bills/CardData$R12Card;Lcom/squareup/protos/client/bills/CardData$X2Card;Lcom/squareup/protos/client/bills/CardData$MCRCard;Lcom/squareup/protos/client/bills/CardData$T2Card;Lcom/squareup/protos/client/bills/CardData$ServerCompleted;Lcom/squareup/protos/client/bills/CardData$S3Card;)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    .line 132
    sget-object v16, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct/range {v0 .. v16}, Lcom/squareup/protos/client/bills/CardData;-><init>(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lcom/squareup/protos/client/bills/CardData$KeyedCard;Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;Lcom/squareup/protos/client/bills/CardData$O1Card;Lcom/squareup/protos/client/bills/CardData$S1Card;Lcom/squareup/protos/client/bills/CardData$R4Card;Lcom/squareup/protos/client/bills/CardData$R6Card;Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;Lcom/squareup/protos/client/bills/CardData$A10Card;Lcom/squareup/protos/client/bills/CardData$R12Card;Lcom/squareup/protos/client/bills/CardData$X2Card;Lcom/squareup/protos/client/bills/CardData$MCRCard;Lcom/squareup/protos/client/bills/CardData$T2Card;Lcom/squareup/protos/client/bills/CardData$ServerCompleted;Lcom/squareup/protos/client/bills/CardData$S3Card;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lcom/squareup/protos/client/bills/CardData$KeyedCard;Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;Lcom/squareup/protos/client/bills/CardData$O1Card;Lcom/squareup/protos/client/bills/CardData$S1Card;Lcom/squareup/protos/client/bills/CardData$R4Card;Lcom/squareup/protos/client/bills/CardData$R6Card;Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;Lcom/squareup/protos/client/bills/CardData$A10Card;Lcom/squareup/protos/client/bills/CardData$R12Card;Lcom/squareup/protos/client/bills/CardData$X2Card;Lcom/squareup/protos/client/bills/CardData$MCRCard;Lcom/squareup/protos/client/bills/CardData$T2Card;Lcom/squareup/protos/client/bills/CardData$ServerCompleted;Lcom/squareup/protos/client/bills/CardData$S3Card;Lokio/ByteString;)V
    .locals 3

    move-object v0, p0

    .line 140
    sget-object v1, Lcom/squareup/protos/client/bills/CardData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v2, p16

    invoke-direct {p0, v1, v2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    move-object v1, p1

    .line 141
    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData;->reader_type:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-object v1, p2

    .line 142
    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData;->keyed_card:Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    move-object v1, p3

    .line 143
    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData;->unencrypted_card:Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

    move-object v1, p4

    .line 144
    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData;->o1_card:Lcom/squareup/protos/client/bills/CardData$O1Card;

    move-object v1, p5

    .line 145
    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData;->s1_card:Lcom/squareup/protos/client/bills/CardData$S1Card;

    move-object v1, p6

    .line 146
    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData;->r4_card:Lcom/squareup/protos/client/bills/CardData$R4Card;

    move-object v1, p7

    .line 147
    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData;->r6_card:Lcom/squareup/protos/client/bills/CardData$R6Card;

    move-object v1, p8

    .line 148
    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData;->encrypted_keyed_card:Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    move-object v1, p9

    .line 149
    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData;->a10_card:Lcom/squareup/protos/client/bills/CardData$A10Card;

    move-object v1, p10

    .line 150
    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData;->r12_card:Lcom/squareup/protos/client/bills/CardData$R12Card;

    move-object v1, p11

    .line 151
    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData;->x2_card:Lcom/squareup/protos/client/bills/CardData$X2Card;

    move-object v1, p12

    .line 152
    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData;->mcr_card:Lcom/squareup/protos/client/bills/CardData$MCRCard;

    move-object/from16 v1, p13

    .line 153
    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData;->t2_card:Lcom/squareup/protos/client/bills/CardData$T2Card;

    move-object/from16 v1, p14

    .line 154
    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData;->server_completed:Lcom/squareup/protos/client/bills/CardData$ServerCompleted;

    move-object/from16 v1, p15

    .line 155
    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData;->s3_card:Lcom/squareup/protos/client/bills/CardData$S3Card;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 183
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/CardData;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 184
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/CardData;

    .line 185
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->reader_type:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardData;->reader_type:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 186
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->keyed_card:Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardData;->keyed_card:Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    .line 187
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->unencrypted_card:Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardData;->unencrypted_card:Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

    .line 188
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->o1_card:Lcom/squareup/protos/client/bills/CardData$O1Card;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardData;->o1_card:Lcom/squareup/protos/client/bills/CardData$O1Card;

    .line 189
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->s1_card:Lcom/squareup/protos/client/bills/CardData$S1Card;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardData;->s1_card:Lcom/squareup/protos/client/bills/CardData$S1Card;

    .line 190
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->r4_card:Lcom/squareup/protos/client/bills/CardData$R4Card;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardData;->r4_card:Lcom/squareup/protos/client/bills/CardData$R4Card;

    .line 191
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->r6_card:Lcom/squareup/protos/client/bills/CardData$R6Card;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardData;->r6_card:Lcom/squareup/protos/client/bills/CardData$R6Card;

    .line 192
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->encrypted_keyed_card:Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardData;->encrypted_keyed_card:Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    .line 193
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->a10_card:Lcom/squareup/protos/client/bills/CardData$A10Card;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardData;->a10_card:Lcom/squareup/protos/client/bills/CardData$A10Card;

    .line 194
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->r12_card:Lcom/squareup/protos/client/bills/CardData$R12Card;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardData;->r12_card:Lcom/squareup/protos/client/bills/CardData$R12Card;

    .line 195
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->x2_card:Lcom/squareup/protos/client/bills/CardData$X2Card;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardData;->x2_card:Lcom/squareup/protos/client/bills/CardData$X2Card;

    .line 196
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->mcr_card:Lcom/squareup/protos/client/bills/CardData$MCRCard;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardData;->mcr_card:Lcom/squareup/protos/client/bills/CardData$MCRCard;

    .line 197
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->t2_card:Lcom/squareup/protos/client/bills/CardData$T2Card;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardData;->t2_card:Lcom/squareup/protos/client/bills/CardData$T2Card;

    .line 198
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->server_completed:Lcom/squareup/protos/client/bills/CardData$ServerCompleted;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardData;->server_completed:Lcom/squareup/protos/client/bills/CardData$ServerCompleted;

    .line 199
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->s3_card:Lcom/squareup/protos/client/bills/CardData$S3Card;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CardData;->s3_card:Lcom/squareup/protos/client/bills/CardData$S3Card;

    .line 200
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 205
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_f

    .line 207
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->reader_type:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 209
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->keyed_card:Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 210
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->unencrypted_card:Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 211
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->o1_card:Lcom/squareup/protos/client/bills/CardData$O1Card;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$O1Card;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->s1_card:Lcom/squareup/protos/client/bills/CardData$S1Card;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$S1Card;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 213
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->r4_card:Lcom/squareup/protos/client/bills/CardData$R4Card;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$R4Card;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 214
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->r6_card:Lcom/squareup/protos/client/bills/CardData$R6Card;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$R6Card;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 215
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->encrypted_keyed_card:Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 216
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->a10_card:Lcom/squareup/protos/client/bills/CardData$A10Card;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$A10Card;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 217
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->r12_card:Lcom/squareup/protos/client/bills/CardData$R12Card;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$R12Card;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 218
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->x2_card:Lcom/squareup/protos/client/bills/CardData$X2Card;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$X2Card;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 219
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->mcr_card:Lcom/squareup/protos/client/bills/CardData$MCRCard;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$MCRCard;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 220
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->t2_card:Lcom/squareup/protos/client/bills/CardData$T2Card;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$T2Card;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 221
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->server_completed:Lcom/squareup/protos/client/bills/CardData$ServerCompleted;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$ServerCompleted;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 222
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->s3_card:Lcom/squareup/protos/client/bills/CardData$S3Card;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$S3Card;->hashCode()I

    move-result v2

    :cond_e
    add-int/2addr v0, v2

    .line 223
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_f
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 2

    .line 160
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$Builder;-><init>()V

    .line 161
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->reader_type:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 162
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->keyed_card:Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->keyed_card:Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    .line 163
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->unencrypted_card:Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->unencrypted_card:Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

    .line 164
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->o1_card:Lcom/squareup/protos/client/bills/CardData$O1Card;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->o1_card:Lcom/squareup/protos/client/bills/CardData$O1Card;

    .line 165
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->s1_card:Lcom/squareup/protos/client/bills/CardData$S1Card;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->s1_card:Lcom/squareup/protos/client/bills/CardData$S1Card;

    .line 166
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->r4_card:Lcom/squareup/protos/client/bills/CardData$R4Card;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->r4_card:Lcom/squareup/protos/client/bills/CardData$R4Card;

    .line 167
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->r6_card:Lcom/squareup/protos/client/bills/CardData$R6Card;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->r6_card:Lcom/squareup/protos/client/bills/CardData$R6Card;

    .line 168
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->encrypted_keyed_card:Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->encrypted_keyed_card:Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    .line 169
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->a10_card:Lcom/squareup/protos/client/bills/CardData$A10Card;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->a10_card:Lcom/squareup/protos/client/bills/CardData$A10Card;

    .line 170
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->r12_card:Lcom/squareup/protos/client/bills/CardData$R12Card;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->r12_card:Lcom/squareup/protos/client/bills/CardData$R12Card;

    .line 171
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->x2_card:Lcom/squareup/protos/client/bills/CardData$X2Card;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->x2_card:Lcom/squareup/protos/client/bills/CardData$X2Card;

    .line 172
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->mcr_card:Lcom/squareup/protos/client/bills/CardData$MCRCard;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->mcr_card:Lcom/squareup/protos/client/bills/CardData$MCRCard;

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->t2_card:Lcom/squareup/protos/client/bills/CardData$T2Card;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->t2_card:Lcom/squareup/protos/client/bills/CardData$T2Card;

    .line 174
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->server_completed:Lcom/squareup/protos/client/bills/CardData$ServerCompleted;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->server_completed:Lcom/squareup/protos/client/bills/CardData$ServerCompleted;

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->s3_card:Lcom/squareup/protos/client/bills/CardData$S3Card;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->s3_card:Lcom/squareup/protos/client/bills/CardData$S3Card;

    .line 176
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData;->newBuilder()Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 230
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 231
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->reader_type:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-eqz v1, :cond_0

    const-string v1, ", reader_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->reader_type:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 232
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->keyed_card:Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    if-eqz v1, :cond_1

    const-string v1, ", keyed_card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->keyed_card:Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 233
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->unencrypted_card:Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

    if-eqz v1, :cond_2

    const-string v1, ", unencrypted_card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->unencrypted_card:Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 234
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->o1_card:Lcom/squareup/protos/client/bills/CardData$O1Card;

    if-eqz v1, :cond_3

    const-string v1, ", o1_card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->o1_card:Lcom/squareup/protos/client/bills/CardData$O1Card;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 235
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->s1_card:Lcom/squareup/protos/client/bills/CardData$S1Card;

    if-eqz v1, :cond_4

    const-string v1, ", s1_card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->s1_card:Lcom/squareup/protos/client/bills/CardData$S1Card;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 236
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->r4_card:Lcom/squareup/protos/client/bills/CardData$R4Card;

    if-eqz v1, :cond_5

    const-string v1, ", r4_card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->r4_card:Lcom/squareup/protos/client/bills/CardData$R4Card;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 237
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->r6_card:Lcom/squareup/protos/client/bills/CardData$R6Card;

    if-eqz v1, :cond_6

    const-string v1, ", r6_card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->r6_card:Lcom/squareup/protos/client/bills/CardData$R6Card;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 238
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->encrypted_keyed_card:Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    if-eqz v1, :cond_7

    const-string v1, ", encrypted_keyed_card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->encrypted_keyed_card:Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 239
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->a10_card:Lcom/squareup/protos/client/bills/CardData$A10Card;

    if-eqz v1, :cond_8

    const-string v1, ", a10_card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->a10_card:Lcom/squareup/protos/client/bills/CardData$A10Card;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 240
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->r12_card:Lcom/squareup/protos/client/bills/CardData$R12Card;

    if-eqz v1, :cond_9

    const-string v1, ", r12_card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->r12_card:Lcom/squareup/protos/client/bills/CardData$R12Card;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 241
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->x2_card:Lcom/squareup/protos/client/bills/CardData$X2Card;

    if-eqz v1, :cond_a

    const-string v1, ", x2_card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->x2_card:Lcom/squareup/protos/client/bills/CardData$X2Card;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 242
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->mcr_card:Lcom/squareup/protos/client/bills/CardData$MCRCard;

    if-eqz v1, :cond_b

    const-string v1, ", mcr_card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->mcr_card:Lcom/squareup/protos/client/bills/CardData$MCRCard;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 243
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->t2_card:Lcom/squareup/protos/client/bills/CardData$T2Card;

    if-eqz v1, :cond_c

    const-string v1, ", t2_card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->t2_card:Lcom/squareup/protos/client/bills/CardData$T2Card;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 244
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->server_completed:Lcom/squareup/protos/client/bills/CardData$ServerCompleted;

    if-eqz v1, :cond_d

    const-string v1, ", server_completed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->server_completed:Lcom/squareup/protos/client/bills/CardData$ServerCompleted;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 245
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->s3_card:Lcom/squareup/protos/client/bills/CardData$S3Card;

    if-eqz v1, :cond_e

    const-string v1, ", s3_card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData;->s3_card:Lcom/squareup/protos/client/bills/CardData$S3Card;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_e
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CardData{"

    .line 246
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
