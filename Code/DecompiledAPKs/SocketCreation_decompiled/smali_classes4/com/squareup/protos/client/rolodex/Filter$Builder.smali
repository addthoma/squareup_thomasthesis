.class public final Lcom/squareup/protos/client/rolodex/Filter$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Filter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/Filter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/Filter;",
        "Lcom/squareup/protos/client/rolodex/Filter$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allow_multiples_in_conjunction:Ljava/lang/Boolean;

.field public cab_attribute_options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public cab_attribute_token:Ljava/lang/String;

.field public cab_attribute_value:Lcom/squareup/protos/client/rolodex/Filter$Option;

.field public cae_attribute_token:Ljava/lang/String;

.field public cae_attribute_value:Ljava/lang/String;

.field public caen_attribute_options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public caen_attribute_token:Ljava/lang/String;

.field public caen_attribute_values:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public cap_attribute_token:Ljava/lang/String;

.field public cap_attribute_value:Ljava/lang/String;

.field public cat_attribute_token:Ljava/lang/String;

.field public cat_attribute_value:Ljava/lang/String;

.field public cat_condition:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

.field public cs_creation_source_types:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public cs_creation_source_types_options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public display_name:Ljava/lang/String;

.field public fb_sentiment_type:Lcom/squareup/protos/client/rolodex/Filter$Option;

.field public fb_sentiment_type_options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public group_token:Ljava/lang/String;

.field public hcof_has_card_on_file:Lcom/squareup/protos/client/rolodex/Filter$Option;

.field public hcof_has_card_on_file_options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public hl_has_loyalty:Lcom/squareup/protos/client/rolodex/Filter$Option;

.field public hl_has_loyalty_options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public hvl_locations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public hvl_locations_options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public iip_is_instant_profile:Lcom/squareup/protos/client/rolodex/Filter$Option;

.field public iip_is_instant_profile_options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public lpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

.field public lpilyd_y_days_options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public mg_group_tokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public npilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

.field public npilyd_y_days_options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public r_x_payments:Ljava/lang/Integer;

.field public r_x_payments_field_name:Ljava/lang/String;

.field public r_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

.field public r_y_days_field_name:Ljava/lang/String;

.field public r_y_days_options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public sq_full_email_address:Ljava/lang/String;

.field public sq_full_phone_number:Ljava/lang/String;

.field public sq_search_query:Ljava/lang/String;

.field public type:Lcom/squareup/protos/client/rolodex/Filter$Type;

.field public xpilyd_x_payments:Ljava/lang/Integer;

.field public xpilyd_x_payments_field_name:Ljava/lang/String;

.field public xpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

.field public xpilyd_y_days_field_name:Ljava/lang/String;

.field public xpilyd_y_days_options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 904
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 905
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->mg_group_tokens:Ljava/util/List;

    .line 906
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_y_days_options:Ljava/util/List;

    .line 907
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->lpilyd_y_days_options:Ljava/util/List;

    .line 908
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->npilyd_y_days_options:Ljava/util/List;

    .line 909
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_y_days_options:Ljava/util/List;

    .line 910
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->caen_attribute_values:Ljava/util/List;

    .line 911
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->caen_attribute_options:Ljava/util/List;

    .line 912
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cab_attribute_options:Ljava/util/List;

    .line 913
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->fb_sentiment_type_options:Ljava/util/List;

    .line 914
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hcof_has_card_on_file_options:Ljava/util/List;

    .line 915
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hl_has_loyalty_options:Ljava/util/List;

    .line 916
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hvl_locations:Ljava/util/List;

    .line 917
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hvl_locations_options:Ljava/util/List;

    .line 918
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cs_creation_source_types:Ljava/util/List;

    .line 919
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cs_creation_source_types_options:Ljava/util/List;

    .line 920
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->iip_is_instant_profile_options:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public allow_multiples_in_conjunction(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 940
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->allow_multiples_in_conjunction:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/rolodex/Filter;
    .locals 2

    .line 1240
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/client/rolodex/Filter;-><init>(Lcom/squareup/protos/client/rolodex/Filter$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 809
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->build()Lcom/squareup/protos/client/rolodex/Filter;

    move-result-object v0

    return-object v0
.end method

.method public cab_attribute_options(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Filter$Builder;"
        }
    .end annotation

    .line 1123
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1124
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cab_attribute_options:Ljava/util/List;

    return-object p0
.end method

.method public cab_attribute_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 1110
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cab_attribute_token:Ljava/lang/String;

    return-object p0
.end method

.method public cab_attribute_value(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 1115
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cab_attribute_value:Lcom/squareup/protos/client/rolodex/Filter$Option;

    return-object p0
.end method

.method public cae_attribute_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 1070
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cae_attribute_token:Ljava/lang/String;

    return-object p0
.end method

.method public cae_attribute_value(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 1075
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cae_attribute_value:Ljava/lang/String;

    return-object p0
.end method

.method public caen_attribute_options(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Filter$Builder;"
        }
    .end annotation

    .line 1104
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1105
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->caen_attribute_options:Ljava/util/List;

    return-object p0
.end method

.method public caen_attribute_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 1090
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->caen_attribute_token:Ljava/lang/String;

    return-object p0
.end method

.method public caen_attribute_values(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Filter$Builder;"
        }
    .end annotation

    .line 1095
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1096
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->caen_attribute_values:Ljava/util/List;

    return-object p0
.end method

.method public cap_attribute_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 1080
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cap_attribute_token:Ljava/lang/String;

    return-object p0
.end method

.method public cap_attribute_value(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 1085
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cap_attribute_value:Ljava/lang/String;

    return-object p0
.end method

.method public cat_attribute_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 1055
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cat_attribute_token:Ljava/lang/String;

    return-object p0
.end method

.method public cat_attribute_value(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 1060
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cat_attribute_value:Ljava/lang/String;

    return-object p0
.end method

.method public cat_condition(Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 1065
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cat_condition:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    return-object p0
.end method

.method public cs_creation_source_types(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Filter$Builder;"
        }
    .end annotation

    .line 1210
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1211
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cs_creation_source_types:Ljava/util/List;

    return-object p0
.end method

.method public cs_creation_source_types_options(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Filter$Builder;"
        }
    .end annotation

    .line 1219
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1220
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cs_creation_source_types_options:Ljava/util/List;

    return-object p0
.end method

.method public display_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 932
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->display_name:Ljava/lang/String;

    return-object p0
.end method

.method public fb_sentiment_type(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 1129
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->fb_sentiment_type:Lcom/squareup/protos/client/rolodex/Filter$Option;

    return-object p0
.end method

.method public fb_sentiment_type_options(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Filter$Builder;"
        }
    .end annotation

    .line 1137
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1138
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->fb_sentiment_type_options:Ljava/util/List;

    return-object p0
.end method

.method public group_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 946
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->group_token:Ljava/lang/String;

    return-object p0
.end method

.method public hcof_has_card_on_file(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 1167
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hcof_has_card_on_file:Lcom/squareup/protos/client/rolodex/Filter$Option;

    return-object p0
.end method

.method public hcof_has_card_on_file_options(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Filter$Builder;"
        }
    .end annotation

    .line 1175
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1176
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hcof_has_card_on_file_options:Ljava/util/List;

    return-object p0
.end method

.method public hl_has_loyalty(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 1181
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hl_has_loyalty:Lcom/squareup/protos/client/rolodex/Filter$Option;

    return-object p0
.end method

.method public hl_has_loyalty_options(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Filter$Builder;"
        }
    .end annotation

    .line 1189
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1190
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hl_has_loyalty_options:Ljava/util/List;

    return-object p0
.end method

.method public hvl_locations(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Filter$Builder;"
        }
    .end annotation

    .line 1195
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1196
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hvl_locations:Ljava/util/List;

    return-object p0
.end method

.method public hvl_locations_options(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Filter$Builder;"
        }
    .end annotation

    .line 1204
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1205
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hvl_locations_options:Ljava/util/List;

    return-object p0
.end method

.method public iip_is_instant_profile(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 1225
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->iip_is_instant_profile:Lcom/squareup/protos/client/rolodex/Filter$Option;

    return-object p0
.end method

.method public iip_is_instant_profile_options(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Filter$Builder;"
        }
    .end annotation

    .line 1233
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1234
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->iip_is_instant_profile_options:Ljava/util/List;

    return-object p0
.end method

.method public lpilyd_y_days(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 992
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->lpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    return-object p0
.end method

.method public lpilyd_y_days_options(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Filter$Builder;"
        }
    .end annotation

    .line 1000
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1001
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->lpilyd_y_days_options:Ljava/util/List;

    return-object p0
.end method

.method public mg_group_tokens(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Filter$Builder;"
        }
    .end annotation

    .line 951
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 952
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->mg_group_tokens:Ljava/util/List;

    return-object p0
.end method

.method public npilyd_y_days(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 1006
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->npilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    return-object p0
.end method

.method public npilyd_y_days_options(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Filter$Builder;"
        }
    .end annotation

    .line 1014
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1015
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->npilyd_y_days_options:Ljava/util/List;

    return-object p0
.end method

.method public r_x_payments(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 1020
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_x_payments:Ljava/lang/Integer;

    return-object p0
.end method

.method public r_x_payments_field_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 1028
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_x_payments_field_name:Ljava/lang/String;

    return-object p0
.end method

.method public r_y_days(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 1033
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    return-object p0
.end method

.method public r_y_days_field_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 1041
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_y_days_field_name:Ljava/lang/String;

    return-object p0
.end method

.method public r_y_days_options(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Filter$Builder;"
        }
    .end annotation

    .line 1049
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1050
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_y_days_options:Ljava/util/List;

    return-object p0
.end method

.method public sq_full_email_address(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 1154
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->sq_full_email_address:Ljava/lang/String;

    return-object p0
.end method

.method public sq_full_phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 1162
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->sq_full_phone_number:Ljava/lang/String;

    return-object p0
.end method

.method public sq_search_query(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 1146
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->sq_search_query:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/rolodex/Filter$Type;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 924
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    return-object p0
.end method

.method public xpilyd_x_payments(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 957
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_x_payments:Ljava/lang/Integer;

    return-object p0
.end method

.method public xpilyd_x_payments_field_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 965
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_x_payments_field_name:Ljava/lang/String;

    return-object p0
.end method

.method public xpilyd_y_days(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 970
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    return-object p0
.end method

.method public xpilyd_y_days_field_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0

    .line 978
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_y_days_field_name:Ljava/lang/String;

    return-object p0
.end method

.method public xpilyd_y_days_options(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Filter$Builder;"
        }
    .end annotation

    .line 986
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 987
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_y_days_options:Ljava/util/List;

    return-object p0
.end method
