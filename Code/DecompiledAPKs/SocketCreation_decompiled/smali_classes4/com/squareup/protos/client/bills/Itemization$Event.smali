.class public final Lcom/squareup/protos/client/bills/Itemization$Event;
.super Lcom/squareup/wire/Message;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Event"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Itemization$Event$ProtoAdapter_Event;,
        Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;,
        Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;,
        Lcom/squareup/protos/client/bills/Itemization$Event$EventType;,
        Lcom/squareup/protos/client/bills/Itemization$Event$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Itemization$Event;",
        "Lcom/squareup/protos/client/bills/Itemization$Event$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Itemization$Event;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_EVENT_TYPE:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

.field public static final DEFAULT_REASON:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final created_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final creator_details:Lcom/squareup/protos/client/CreatorDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.CreatorDetails#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final event_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final event_type:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization$Event$EventType#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final kds_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization$Event$KdsEventDetails#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final reason:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final split_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization$Event$SplitEventDetails#ADAPTER"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 3096
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$ProtoAdapter_Event;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Event$ProtoAdapter_Event;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 3100
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->UNKNOWN:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Event;->DEFAULT_EVENT_TYPE:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Itemization$Event$EventType;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/CreatorDetails;Ljava/lang/String;Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;)V
    .locals 9

    .line 3165
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/bills/Itemization$Event;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Itemization$Event$EventType;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/CreatorDetails;Ljava/lang/String;Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Itemization$Event$EventType;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/CreatorDetails;Ljava/lang/String;Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;Lokio/ByteString;)V
    .locals 1

    .line 3171
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 3172
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 3173
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->event_type:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    .line 3174
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 3175
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 3176
    iput-object p5, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->reason:Ljava/lang/String;

    .line 3177
    iput-object p6, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->split_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;

    .line 3178
    iput-object p7, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->kds_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 3198
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Itemization$Event;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 3199
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$Event;

    .line 3200
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Event;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Event;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$Event;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 3201
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->event_type:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$Event;->event_type:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    .line 3202
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$Event;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 3203
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$Event;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 3204
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->reason:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$Event;->reason:Ljava/lang/String;

    .line 3205
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->split_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$Event;->split_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;

    .line 3206
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->kds_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Event;->kds_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;

    .line 3207
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 3212
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 3214
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Event;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 3215
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3216
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->event_type:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3217
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3218
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/CreatorDetails;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3219
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->reason:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3220
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->split_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3221
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->kds_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 3222
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Itemization$Event$Builder;
    .locals 2

    .line 3183
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;-><init>()V

    .line 3184
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 3185
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->event_type:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->event_type:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    .line 3186
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 3187
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 3188
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->reason:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->reason:Ljava/lang/String;

    .line 3189
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->split_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->split_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;

    .line 3190
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->kds_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->kds_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;

    .line 3191
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Event;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 3095
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Event;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Event$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 3229
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 3230
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", event_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3231
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->event_type:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    if-eqz v1, :cond_1

    const-string v1, ", event_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->event_type:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3232
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_2

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3233
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_3

    const-string v1, ", creator_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3234
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->reason:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->reason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3235
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->split_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;

    if-eqz v1, :cond_5

    const-string v1, ", split_event_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->split_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3236
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->kds_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;

    if-eqz v1, :cond_6

    const-string v1, ", kds_event_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event;->kds_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Event{"

    .line 3237
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
