.class public final Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StopTimecardResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/StopTimecardResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/StopTimecardResponse;",
        "Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public timecard:Lcom/squareup/protos/client/timecards/Timecard;

.field public timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

.field public valid:Ljava/lang/Boolean;

.field public workday_shift_summary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 125
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/timecards/StopTimecardResponse;
    .locals 7

    .line 150
    new-instance v6, Lcom/squareup/protos/client/timecards/StopTimecardResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object v3, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;->valid:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;->workday_shift_summary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/timecards/StopTimecardResponse;-><init>(Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/TimecardBreak;Ljava/lang/Boolean;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 116
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;->build()Lcom/squareup/protos/client/timecards/StopTimecardResponse;

    move-result-object v0

    return-object v0
.end method

.method public timecard(Lcom/squareup/protos/client/timecards/Timecard;)Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    return-object p0
.end method

.method public timecard_break(Lcom/squareup/protos/client/timecards/TimecardBreak;)Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    return-object p0
.end method

.method public valid(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;->valid:Ljava/lang/Boolean;

    return-object p0
.end method

.method public workday_shift_summary(Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;)Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;->workday_shift_summary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    return-object p0
.end method
