.class public final enum Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;
.super Ljava/lang/Enum;
.source "CheckIdentityVerificationStatusResponse.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "IdvError"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError$ProtoAdapter_IdvError;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DOES_NOT_MATCH:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

.field public static final enum DO_NOT_USE:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

.field public static final enum TOO_MANY_ATTEMPTS:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

.field public static final enum UNDER_AGE_OF_MAJORITY:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 490
    new-instance v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    .line 492
    new-instance v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    const/4 v2, 0x1

    const-string v3, "TOO_MANY_ATTEMPTS"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->TOO_MANY_ATTEMPTS:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    .line 494
    new-instance v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    const/4 v3, 0x2

    const-string v4, "UNDER_AGE_OF_MAJORITY"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->UNDER_AGE_OF_MAJORITY:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    .line 496
    new-instance v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    const/4 v4, 0x3

    const-string v5, "DOES_NOT_MATCH"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->DOES_NOT_MATCH:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    .line 489
    sget-object v5, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->TOO_MANY_ATTEMPTS:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->UNDER_AGE_OF_MAJORITY:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->DOES_NOT_MATCH:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->$VALUES:[Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    .line 498
    new-instance v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError$ProtoAdapter_IdvError;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError$ProtoAdapter_IdvError;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 502
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 503
    iput p3, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 514
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->DOES_NOT_MATCH:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    return-object p0

    .line 513
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->UNDER_AGE_OF_MAJORITY:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    return-object p0

    .line 512
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->TOO_MANY_ATTEMPTS:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    return-object p0

    .line 511
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;
    .locals 1

    .line 489
    const-class v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;
    .locals 1

    .line 489
    sget-object v0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->$VALUES:[Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 521
    iget v0, p0, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse$IdvError;->value:I

    return v0
.end method
