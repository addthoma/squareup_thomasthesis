.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FulfillmentCreationDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$ProtoAdapter_FulfillmentCreationDetails;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentItems;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_LOCATION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NOTE:Ljava/lang/String; = ""

.field public static final DEFAULT_STATE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;

.field private static final serialVersionUID:J


# instance fields
.field public final items:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentItems#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentItems;",
            ">;"
        }
    .end annotation
.end field

.field public final location_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final note:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final pickup_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final recipient:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 6687
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$ProtoAdapter_FulfillmentCreationDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$ProtoAdapter_FulfillmentCreationDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 6691
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;->FULFILLMENT_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->DEFAULT_TYPE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;

    .line 6693
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;->FULFILLMENT_STATE_DO_NOT_USE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->DEFAULT_STATE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentItems;",
            ">;",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;",
            ")V"
        }
    .end annotation

    .line 6770
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;-><init>(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentItems;",
            ">;",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 6777
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 6778
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->recipient:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;

    const-string p1, "items"

    .line 6779
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->items:Ljava/util/List;

    .line 6780
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;

    .line 6781
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    .line 6782
    iput-object p5, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->location_id:Ljava/lang/String;

    .line 6783
    iput-object p6, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->note:Ljava/lang/String;

    .line 6784
    iput-object p7, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->pickup_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 6804
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 6805
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;

    .line 6806
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->recipient:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->recipient:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;

    .line 6807
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->items:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->items:Ljava/util/List;

    .line 6808
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;

    .line 6809
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    .line 6810
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->location_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->location_id:Ljava/lang/String;

    .line 6811
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->note:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->note:Ljava/lang/String;

    .line 6812
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->pickup_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->pickup_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;

    .line 6813
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 6818
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 6820
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 6821
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->recipient:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6822
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6823
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6824
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6825
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->location_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6826
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->note:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6827
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->pickup_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 6828
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;
    .locals 2

    .line 6789
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;-><init>()V

    .line 6790
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->recipient:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->recipient:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;

    .line 6791
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->items:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->items:Ljava/util/List;

    .line 6792
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;

    .line 6793
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    .line 6794
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->location_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->location_id:Ljava/lang/String;

    .line 6795
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->note:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->note:Ljava/lang/String;

    .line 6796
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->pickup_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->pickup_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;

    .line 6797
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 6686
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 6835
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 6836
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->recipient:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;

    if-eqz v1, :cond_0

    const-string v1, ", recipient="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->recipient:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentRecipient;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6837
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->items:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6838
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;

    if-eqz v1, :cond_2

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6839
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    if-eqz v1, :cond_3

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6840
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->location_id:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", location_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->location_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6841
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->note:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", note="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->note:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6842
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->pickup_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;

    if-eqz v1, :cond_6

    const-string v1, ", pickup_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;->pickup_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentPickupDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FulfillmentCreationDetails{"

    .line 6843
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
