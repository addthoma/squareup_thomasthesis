.class final Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$ProtoAdapter_SetEGiftOrderConfigurationRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SetEGiftOrderConfigurationRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SetEGiftOrderConfigurationRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 150
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 172
    new-instance v0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;-><init>()V

    .line 173
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 174
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 180
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 178
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->client_request_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;

    goto :goto_0

    .line 177
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->new_custom_egift_themes:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/giftcards/EGiftTheme;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 176
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->order_configuration(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;)Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;

    goto :goto_0

    .line 184
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 185
    invoke-virtual {v0}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->build()Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 148
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$ProtoAdapter_SetEGiftOrderConfigurationRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 164
    sget-object v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 165
    sget-object v0, Lcom/squareup/protos/client/giftcards/EGiftTheme;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->new_custom_egift_themes:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 166
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->client_request_uuid:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 167
    invoke-virtual {p2}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 148
    check-cast p2, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$ProtoAdapter_SetEGiftOrderConfigurationRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;)I
    .locals 4

    .line 155
    sget-object v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/giftcards/EGiftTheme;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 156
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->new_custom_egift_themes:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->client_request_uuid:Ljava/lang/String;

    const/4 v3, 0x4

    .line 157
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 158
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 148
    check-cast p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$ProtoAdapter_SetEGiftOrderConfigurationRequest;->encodedSize(Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;)Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;
    .locals 2

    .line 190
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;->newBuilder()Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;

    move-result-object p1

    .line 191
    iget-object v0, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    iput-object v0, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    .line 192
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->new_custom_egift_themes:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/giftcards/EGiftTheme;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 193
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 194
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->build()Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 148
    check-cast p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$ProtoAdapter_SetEGiftOrderConfigurationRequest;->redact(Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;)Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;

    move-result-object p1

    return-object p1
.end method
