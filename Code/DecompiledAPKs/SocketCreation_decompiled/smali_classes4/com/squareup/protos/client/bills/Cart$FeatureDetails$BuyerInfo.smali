.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BuyerInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$ProtoAdapter_BuyerInfo;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CUSTOMER_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final available_instrument_details:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$InstrumentDetails#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;"
        }
    .end annotation
.end field

.field public final customer_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$BuyerInfo$DisplayDetails#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 2671
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$ProtoAdapter_BuyerInfo;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$ProtoAdapter_BuyerInfo;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;)V"
        }
    .end annotation

    .line 2707
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 2712
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 2713
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->customer_id:Ljava/lang/String;

    .line 2714
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    const-string p1, "available_instrument_details"

    .line 2715
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->available_instrument_details:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 2731
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2732
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    .line 2733
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->customer_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->customer_id:Ljava/lang/String;

    .line 2734
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    .line 2735
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->available_instrument_details:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->available_instrument_details:Ljava/util/List;

    .line 2736
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 2741
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 2743
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 2744
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->customer_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2745
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 2746
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->available_instrument_details:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2747
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;
    .locals 2

    .line 2720
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;-><init>()V

    .line 2721
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->customer_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;->customer_id:Ljava/lang/String;

    .line 2722
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    .line 2723
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->available_instrument_details:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;->available_instrument_details:Ljava/util/List;

    .line 2724
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 2670
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 2754
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2755
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->customer_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", customer_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->customer_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2756
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    if-eqz v1, :cond_1

    const-string v1, ", display_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2757
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->available_instrument_details:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", available_instrument_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->available_instrument_details:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "BuyerInfo{"

    .line 2758
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
