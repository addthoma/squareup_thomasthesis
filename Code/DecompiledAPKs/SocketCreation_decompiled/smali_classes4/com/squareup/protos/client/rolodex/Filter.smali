.class public final Lcom/squareup/protos/client/rolodex/Filter;
.super Lcom/squareup/wire/Message;
.source "Filter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/Filter$ProtoAdapter_Filter;,
        Lcom/squareup/protos/client/rolodex/Filter$Type;,
        Lcom/squareup/protos/client/rolodex/Filter$Option;,
        Lcom/squareup/protos/client/rolodex/Filter$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/Filter;",
        "Lcom/squareup/protos/client/rolodex/Filter$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ALLOW_MULTIPLES_IN_CONJUNCTION:Ljava/lang/Boolean;

.field public static final DEFAULT_CAB_ATTRIBUTE_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_CAEN_ATTRIBUTE_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_CAE_ATTRIBUTE_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_CAE_ATTRIBUTE_VALUE:Ljava/lang/String; = ""

.field public static final DEFAULT_CAP_ATTRIBUTE_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_CAP_ATTRIBUTE_VALUE:Ljava/lang/String; = ""

.field public static final DEFAULT_CAT_ATTRIBUTE_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_CAT_ATTRIBUTE_VALUE:Ljava/lang/String; = ""

.field public static final DEFAULT_CAT_CONDITION:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

.field public static final DEFAULT_DISPLAY_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_GROUP_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_R_X_PAYMENTS:Ljava/lang/Integer;

.field public static final DEFAULT_R_X_PAYMENTS_FIELD_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_R_Y_DAYS_FIELD_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_SQ_FULL_EMAIL_ADDRESS:Ljava/lang/String; = ""

.field public static final DEFAULT_SQ_FULL_PHONE_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_SQ_SEARCH_QUERY:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/client/rolodex/Filter$Type;

.field public static final DEFAULT_XPILYD_X_PAYMENTS:Ljava/lang/Integer;

.field public static final DEFAULT_XPILYD_X_PAYMENTS_FIELD_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_XPILYD_Y_DAYS_FIELD_NAME:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final allow_multiples_in_conjunction:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final cab_attribute_options:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3ea
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public final cab_attribute_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3e8
    .end annotation
.end field

.field public final cab_attribute_value:Lcom/squareup/protos/client/rolodex/Filter$Option;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        tag = 0x3e9
    .end annotation
.end field

.field public final cae_attribute_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2bc
    .end annotation
.end field

.field public final cae_attribute_value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2bd
    .end annotation
.end field

.field public final caen_attribute_options:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x386
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public final caen_attribute_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x384
    .end annotation
.end field

.field public final caen_attribute_values:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x385
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public final cap_attribute_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x320
    .end annotation
.end field

.field public final cap_attribute_value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x321
    .end annotation
.end field

.field public final cat_attribute_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x258
    .end annotation
.end field

.field public final cat_attribute_value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x259
    .end annotation
.end field

.field public final cat_condition:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.CustomAttributeText$Condition#ADAPTER"
        tag = 0x25a
    .end annotation
.end field

.field public final cs_creation_source_types:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6a4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public final cs_creation_source_types_options:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6a5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public final display_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final fb_sentiment_type:Lcom/squareup/protos/client/rolodex/Filter$Option;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        tag = 0x44c
    .end annotation
.end field

.field public final fb_sentiment_type_options:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x44d
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public final group_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x64
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final hcof_has_card_on_file:Lcom/squareup/protos/client/rolodex/Filter$Option;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        tag = 0x514
    .end annotation
.end field

.field public final hcof_has_card_on_file_options:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x515
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public final hl_has_loyalty:Lcom/squareup/protos/client/rolodex/Filter$Option;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        tag = 0x578
    .end annotation
.end field

.field public final hl_has_loyalty_options:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x579
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public final hvl_locations:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x640
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public final hvl_locations_options:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x641
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public final iip_is_instant_profile:Lcom/squareup/protos/client/rolodex/Filter$Option;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        tag = 0x708
    .end annotation
.end field

.field public final iip_is_instant_profile_options:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x709
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public final lpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        tag = 0x12c
    .end annotation
.end field

.field public final lpilyd_y_days_options:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x12d
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public final mg_group_tokens:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x65
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final npilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        tag = 0x190
    .end annotation
.end field

.field public final npilyd_y_days_options:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x191
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public final r_x_payments:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1f4
    .end annotation
.end field

.field public final r_x_payments_field_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1f5
    .end annotation
.end field

.field public final r_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        tag = 0x1f6
    .end annotation
.end field

.field public final r_y_days_field_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1f7
    .end annotation
.end field

.field public final r_y_days_options:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1f8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field

.field public final sq_full_email_address:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4b1
    .end annotation
.end field

.field public final sq_full_phone_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4b2
    .end annotation
.end field

.field public final sq_search_query:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4b0
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/client/rolodex/Filter$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Type#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final xpilyd_x_payments:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xc8
    .end annotation
.end field

.field public final xpilyd_x_payments_field_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xc9
    .end annotation
.end field

.field public final xpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        tag = 0xca
    .end annotation
.end field

.field public final xpilyd_y_days_field_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xcb
    .end annotation
.end field

.field public final xpilyd_y_days_options:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter$Option#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xcc
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 35
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$ProtoAdapter_Filter;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Filter$ProtoAdapter_Filter;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 39
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->UNKNOWN:Lcom/squareup/protos/client/rolodex/Filter$Type;

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter;->DEFAULT_TYPE:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/4 v0, 0x0

    .line 43
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter;->DEFAULT_ALLOW_MULTIPLES_IN_CONJUNCTION:Ljava/lang/Boolean;

    .line 47
    sput-object v1, Lcom/squareup/protos/client/rolodex/Filter;->DEFAULT_XPILYD_X_PAYMENTS:Ljava/lang/Integer;

    .line 53
    sput-object v1, Lcom/squareup/protos/client/rolodex/Filter;->DEFAULT_R_X_PAYMENTS:Ljava/lang/Integer;

    .line 63
    sget-object v0, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;->UNKNOWN:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter;->DEFAULT_CAT_CONDITION:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/rolodex/Filter$Builder;Lokio/ByteString;)V
    .locals 1

    .line 540
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 541
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    .line 542
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->display_name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->display_name:Ljava/lang/String;

    .line 543
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->allow_multiples_in_conjunction:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->allow_multiples_in_conjunction:Ljava/lang/Boolean;

    .line 544
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->group_token:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->group_token:Ljava/lang/String;

    .line 545
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->mg_group_tokens:Ljava/util/List;

    const-string v0, "mg_group_tokens"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->mg_group_tokens:Ljava/util/List;

    .line 546
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_x_payments:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_x_payments:Ljava/lang/Integer;

    .line 547
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_x_payments_field_name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_x_payments_field_name:Ljava/lang/String;

    .line 548
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 549
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_y_days_field_name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days_field_name:Ljava/lang/String;

    .line 550
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_y_days_options:Ljava/util/List;

    const-string/jumbo v0, "xpilyd_y_days_options"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days_options:Ljava/util/List;

    .line 551
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->lpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 552
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->lpilyd_y_days_options:Ljava/util/List;

    const-string v0, "lpilyd_y_days_options"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days_options:Ljava/util/List;

    .line 553
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->npilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 554
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->npilyd_y_days_options:Ljava/util/List;

    const-string v0, "npilyd_y_days_options"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days_options:Ljava/util/List;

    .line 555
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_x_payments:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_x_payments:Ljava/lang/Integer;

    .line 556
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_x_payments_field_name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_x_payments_field_name:Ljava/lang/String;

    .line 557
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 558
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_y_days_field_name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days_field_name:Ljava/lang/String;

    .line 559
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_y_days_options:Ljava/util/List;

    const-string v0, "r_y_days_options"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days_options:Ljava/util/List;

    .line 560
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cat_attribute_token:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->cat_attribute_token:Ljava/lang/String;

    .line 561
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cat_attribute_value:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->cat_attribute_value:Ljava/lang/String;

    .line 562
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cat_condition:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->cat_condition:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    .line 563
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cae_attribute_token:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->cae_attribute_token:Ljava/lang/String;

    .line 564
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cae_attribute_value:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->cae_attribute_value:Ljava/lang/String;

    .line 565
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cap_attribute_token:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->cap_attribute_token:Ljava/lang/String;

    .line 566
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cap_attribute_value:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->cap_attribute_value:Ljava/lang/String;

    .line 567
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->caen_attribute_token:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_token:Ljava/lang/String;

    .line 568
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->caen_attribute_values:Ljava/util/List;

    const-string v0, "caen_attribute_values"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_values:Ljava/util/List;

    .line 569
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->caen_attribute_options:Ljava/util/List;

    const-string v0, "caen_attribute_options"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_options:Ljava/util/List;

    .line 570
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cab_attribute_token:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_token:Ljava/lang/String;

    .line 571
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cab_attribute_value:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_value:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 572
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cab_attribute_options:Ljava/util/List;

    const-string v0, "cab_attribute_options"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_options:Ljava/util/List;

    .line 573
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->fb_sentiment_type:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 574
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->fb_sentiment_type_options:Ljava/util/List;

    const-string v0, "fb_sentiment_type_options"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type_options:Ljava/util/List;

    .line 575
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->sq_search_query:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->sq_search_query:Ljava/lang/String;

    .line 576
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->sq_full_email_address:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->sq_full_email_address:Ljava/lang/String;

    .line 577
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->sq_full_phone_number:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->sq_full_phone_number:Ljava/lang/String;

    .line 578
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hcof_has_card_on_file:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 579
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hcof_has_card_on_file_options:Ljava/util/List;

    const-string v0, "hcof_has_card_on_file_options"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file_options:Ljava/util/List;

    .line 580
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hl_has_loyalty:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 581
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hl_has_loyalty_options:Ljava/util/List;

    const-string v0, "hl_has_loyalty_options"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty_options:Ljava/util/List;

    .line 582
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hvl_locations:Ljava/util/List;

    const-string v0, "hvl_locations"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations:Ljava/util/List;

    .line 583
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hvl_locations_options:Ljava/util/List;

    const-string v0, "hvl_locations_options"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations_options:Ljava/util/List;

    .line 584
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cs_creation_source_types:Ljava/util/List;

    const-string v0, "cs_creation_source_types"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types:Ljava/util/List;

    .line 585
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cs_creation_source_types_options:Ljava/util/List;

    const-string v0, "cs_creation_source_types_options"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types_options:Ljava/util/List;

    .line 586
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->iip_is_instant_profile:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 587
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter$Builder;->iip_is_instant_profile_options:Ljava/util/List;

    const-string p2, "iip_is_instant_profile_options"

    invoke-static {p2, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile_options:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 647
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/Filter;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 648
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/Filter;

    .line 649
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Filter;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Filter;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    .line 650
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->display_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->display_name:Ljava/lang/String;

    .line 651
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->allow_multiples_in_conjunction:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->allow_multiples_in_conjunction:Ljava/lang/Boolean;

    .line 652
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->group_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->group_token:Ljava/lang/String;

    .line 653
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->mg_group_tokens:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->mg_group_tokens:Ljava/util/List;

    .line 654
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_x_payments:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_x_payments:Ljava/lang/Integer;

    .line 655
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_x_payments_field_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_x_payments_field_name:Ljava/lang/String;

    .line 656
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 657
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days_field_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days_field_name:Ljava/lang/String;

    .line 658
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days_options:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days_options:Ljava/util/List;

    .line 659
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 660
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days_options:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days_options:Ljava/util/List;

    .line 661
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 662
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days_options:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days_options:Ljava/util/List;

    .line 663
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_x_payments:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->r_x_payments:Ljava/lang/Integer;

    .line 664
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_x_payments_field_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->r_x_payments_field_name:Ljava/lang/String;

    .line 665
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 666
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days_field_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days_field_name:Ljava/lang/String;

    .line 667
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days_options:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days_options:Ljava/util/List;

    .line 668
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cat_attribute_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->cat_attribute_token:Ljava/lang/String;

    .line 669
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cat_attribute_value:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->cat_attribute_value:Ljava/lang/String;

    .line 670
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cat_condition:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->cat_condition:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    .line 671
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cae_attribute_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->cae_attribute_token:Ljava/lang/String;

    .line 672
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cae_attribute_value:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->cae_attribute_value:Ljava/lang/String;

    .line 673
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cap_attribute_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->cap_attribute_token:Ljava/lang/String;

    .line 674
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cap_attribute_value:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->cap_attribute_value:Ljava/lang/String;

    .line 675
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_token:Ljava/lang/String;

    .line 676
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_values:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_values:Ljava/util/List;

    .line 677
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_options:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_options:Ljava/util/List;

    .line 678
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_token:Ljava/lang/String;

    .line 679
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_value:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_value:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 680
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_options:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_options:Ljava/util/List;

    .line 681
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 682
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type_options:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type_options:Ljava/util/List;

    .line 683
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->sq_search_query:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->sq_search_query:Ljava/lang/String;

    .line 684
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->sq_full_email_address:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->sq_full_email_address:Ljava/lang/String;

    .line 685
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->sq_full_phone_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->sq_full_phone_number:Ljava/lang/String;

    .line 686
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 687
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file_options:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file_options:Ljava/util/List;

    .line 688
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 689
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty_options:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty_options:Ljava/util/List;

    .line 690
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations:Ljava/util/List;

    .line 691
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations_options:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations_options:Ljava/util/List;

    .line 692
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types:Ljava/util/List;

    .line 693
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types_options:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types_options:Ljava/util/List;

    .line 694
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 695
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile_options:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile_options:Ljava/util/List;

    .line 696
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 701
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1f

    .line 703
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Filter;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 704
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 705
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 706
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->allow_multiples_in_conjunction:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 707
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->group_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 708
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->mg_group_tokens:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 709
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_x_payments:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 710
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_x_payments_field_name:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 711
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Option;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 712
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days_field_name:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 713
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 714
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Option;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 715
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 716
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Option;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 717
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 718
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_x_payments:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 719
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_x_payments_field_name:Ljava/lang/String;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 720
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Option;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 721
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days_field_name:Ljava/lang/String;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 722
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 723
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cat_attribute_token:Ljava/lang/String;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 724
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cat_attribute_value:Ljava/lang/String;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 725
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cat_condition:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 726
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cae_attribute_token:Ljava/lang/String;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 727
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cae_attribute_value:Ljava/lang/String;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 728
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cap_attribute_token:Ljava/lang/String;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 729
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cap_attribute_value:Ljava/lang/String;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 730
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_token:Ljava/lang/String;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 731
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 732
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 733
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_token:Ljava/lang/String;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 734
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_value:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Option;->hashCode()I

    move-result v1

    goto :goto_17

    :cond_17
    const/4 v1, 0x0

    :goto_17
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 735
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 736
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Option;->hashCode()I

    move-result v1

    goto :goto_18

    :cond_18
    const/4 v1, 0x0

    :goto_18
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 737
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 738
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->sq_search_query:Ljava/lang/String;

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_19

    :cond_19
    const/4 v1, 0x0

    :goto_19
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 739
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->sq_full_email_address:Ljava/lang/String;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1a

    :cond_1a
    const/4 v1, 0x0

    :goto_1a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 740
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->sq_full_phone_number:Ljava/lang/String;

    if-eqz v1, :cond_1b

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1b

    :cond_1b
    const/4 v1, 0x0

    :goto_1b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 741
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v1, :cond_1c

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Option;->hashCode()I

    move-result v1

    goto :goto_1c

    :cond_1c
    const/4 v1, 0x0

    :goto_1c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 742
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 743
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v1, :cond_1d

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Option;->hashCode()I

    move-result v1

    goto :goto_1d

    :cond_1d
    const/4 v1, 0x0

    :goto_1d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 744
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 745
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 746
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 747
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 748
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 749
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v1, :cond_1e

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Option;->hashCode()I

    move-result v2

    :cond_1e
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 750
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 751
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1f
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/Filter$Builder;
    .locals 2

    .line 592
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Filter$Builder;-><init>()V

    .line 593
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    .line 594
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->display_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->display_name:Ljava/lang/String;

    .line 595
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->allow_multiples_in_conjunction:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->allow_multiples_in_conjunction:Ljava/lang/Boolean;

    .line 596
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->group_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->group_token:Ljava/lang/String;

    .line 597
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->mg_group_tokens:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->mg_group_tokens:Ljava/util/List;

    .line 598
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_x_payments:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_x_payments:Ljava/lang/Integer;

    .line 599
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_x_payments_field_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_x_payments_field_name:Ljava/lang/String;

    .line 600
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 601
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days_field_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_y_days_field_name:Ljava/lang/String;

    .line 602
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days_options:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_y_days_options:Ljava/util/List;

    .line 603
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->lpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 604
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days_options:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->lpilyd_y_days_options:Ljava/util/List;

    .line 605
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->npilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 606
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days_options:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->npilyd_y_days_options:Ljava/util/List;

    .line 607
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_x_payments:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_x_payments:Ljava/lang/Integer;

    .line 608
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_x_payments_field_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_x_payments_field_name:Ljava/lang/String;

    .line 609
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 610
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days_field_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_y_days_field_name:Ljava/lang/String;

    .line 611
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days_options:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->r_y_days_options:Ljava/util/List;

    .line 612
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cat_attribute_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cat_attribute_token:Ljava/lang/String;

    .line 613
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cat_attribute_value:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cat_attribute_value:Ljava/lang/String;

    .line 614
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cat_condition:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cat_condition:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    .line 615
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cae_attribute_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cae_attribute_token:Ljava/lang/String;

    .line 616
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cae_attribute_value:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cae_attribute_value:Ljava/lang/String;

    .line 617
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cap_attribute_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cap_attribute_token:Ljava/lang/String;

    .line 618
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cap_attribute_value:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cap_attribute_value:Ljava/lang/String;

    .line 619
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->caen_attribute_token:Ljava/lang/String;

    .line 620
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_values:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->caen_attribute_values:Ljava/util/List;

    .line 621
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_options:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->caen_attribute_options:Ljava/util/List;

    .line 622
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cab_attribute_token:Ljava/lang/String;

    .line 623
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_value:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cab_attribute_value:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 624
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_options:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cab_attribute_options:Ljava/util/List;

    .line 625
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->fb_sentiment_type:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 626
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type_options:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->fb_sentiment_type_options:Ljava/util/List;

    .line 627
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->sq_search_query:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->sq_search_query:Ljava/lang/String;

    .line 628
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->sq_full_email_address:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->sq_full_email_address:Ljava/lang/String;

    .line 629
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->sq_full_phone_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->sq_full_phone_number:Ljava/lang/String;

    .line 630
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hcof_has_card_on_file:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 631
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file_options:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hcof_has_card_on_file_options:Ljava/util/List;

    .line 632
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hl_has_loyalty:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 633
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty_options:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hl_has_loyalty_options:Ljava/util/List;

    .line 634
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hvl_locations:Ljava/util/List;

    .line 635
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations_options:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hvl_locations_options:Ljava/util/List;

    .line 636
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cs_creation_source_types:Ljava/util/List;

    .line 637
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types_options:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cs_creation_source_types_options:Ljava/util/List;

    .line 638
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->iip_is_instant_profile:Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 639
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile_options:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Filter$Builder;->iip_is_instant_profile_options:Ljava/util/List;

    .line 640
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Filter;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Filter;->newBuilder()Lcom/squareup/protos/client/rolodex/Filter$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 758
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 759
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    if-eqz v1, :cond_0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 760
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", display_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->display_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 761
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->allow_multiples_in_conjunction:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", allow_multiples_in_conjunction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->allow_multiples_in_conjunction:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 762
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->group_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", group_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->group_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 763
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->mg_group_tokens:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", mg_group_tokens="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->mg_group_tokens:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 764
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_x_payments:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    const-string v1, ", xpilyd_x_payments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_x_payments:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 765
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_x_payments_field_name:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", xpilyd_x_payments_field_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_x_payments_field_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 766
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v1, :cond_7

    const-string v1, ", xpilyd_y_days="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 767
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days_field_name:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", xpilyd_y_days_field_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days_field_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 768
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, ", xpilyd_y_days_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days_options:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 769
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v1, :cond_a

    const-string v1, ", lpilyd_y_days="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 770
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, ", lpilyd_y_days_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days_options:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 771
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v1, :cond_c

    const-string v1, ", npilyd_y_days="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 772
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d

    const-string v1, ", npilyd_y_days_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days_options:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 773
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_x_payments:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    const-string v1, ", r_x_payments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_x_payments:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 774
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_x_payments_field_name:Ljava/lang/String;

    if-eqz v1, :cond_f

    const-string v1, ", r_x_payments_field_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_x_payments_field_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 775
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v1, :cond_10

    const-string v1, ", r_y_days="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 776
    :cond_10
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days_field_name:Ljava/lang/String;

    if-eqz v1, :cond_11

    const-string v1, ", r_y_days_field_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days_field_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 777
    :cond_11
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_12

    const-string v1, ", r_y_days_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->r_y_days_options:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 778
    :cond_12
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cat_attribute_token:Ljava/lang/String;

    if-eqz v1, :cond_13

    const-string v1, ", cat_attribute_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cat_attribute_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 779
    :cond_13
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cat_attribute_value:Ljava/lang/String;

    if-eqz v1, :cond_14

    const-string v1, ", cat_attribute_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cat_attribute_value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 780
    :cond_14
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cat_condition:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    if-eqz v1, :cond_15

    const-string v1, ", cat_condition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cat_condition:Lcom/squareup/protos/client/rolodex/CustomAttributeText$Condition;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 781
    :cond_15
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cae_attribute_token:Ljava/lang/String;

    if-eqz v1, :cond_16

    const-string v1, ", cae_attribute_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cae_attribute_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 782
    :cond_16
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cae_attribute_value:Ljava/lang/String;

    if-eqz v1, :cond_17

    const-string v1, ", cae_attribute_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cae_attribute_value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 783
    :cond_17
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cap_attribute_token:Ljava/lang/String;

    if-eqz v1, :cond_18

    const-string v1, ", cap_attribute_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cap_attribute_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 784
    :cond_18
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cap_attribute_value:Ljava/lang/String;

    if-eqz v1, :cond_19

    const-string v1, ", cap_attribute_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cap_attribute_value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 785
    :cond_19
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_token:Ljava/lang/String;

    if-eqz v1, :cond_1a

    const-string v1, ", caen_attribute_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 786
    :cond_1a
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1b

    const-string v1, ", caen_attribute_values="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_values:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 787
    :cond_1b
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1c

    const-string v1, ", caen_attribute_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_options:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 788
    :cond_1c
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_token:Ljava/lang/String;

    if-eqz v1, :cond_1d

    const-string v1, ", cab_attribute_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 789
    :cond_1d
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_value:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v1, :cond_1e

    const-string v1, ", cab_attribute_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_value:Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 790
    :cond_1e
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1f

    const-string v1, ", cab_attribute_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_options:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 791
    :cond_1f
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v1, :cond_20

    const-string v1, ", fb_sentiment_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type:Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 792
    :cond_20
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_21

    const-string v1, ", fb_sentiment_type_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type_options:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 793
    :cond_21
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->sq_search_query:Ljava/lang/String;

    if-eqz v1, :cond_22

    const-string v1, ", sq_search_query="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->sq_search_query:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 794
    :cond_22
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->sq_full_email_address:Ljava/lang/String;

    if-eqz v1, :cond_23

    const-string v1, ", sq_full_email_address="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->sq_full_email_address:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 795
    :cond_23
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->sq_full_phone_number:Ljava/lang/String;

    if-eqz v1, :cond_24

    const-string v1, ", sq_full_phone_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->sq_full_phone_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 796
    :cond_24
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v1, :cond_25

    const-string v1, ", hcof_has_card_on_file="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file:Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 797
    :cond_25
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_26

    const-string v1, ", hcof_has_card_on_file_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file_options:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 798
    :cond_26
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v1, :cond_27

    const-string v1, ", hl_has_loyalty="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty:Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 799
    :cond_27
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_28

    const-string v1, ", hl_has_loyalty_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty_options:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 800
    :cond_28
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_29

    const-string v1, ", hvl_locations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 801
    :cond_29
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2a

    const-string v1, ", hvl_locations_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations_options:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 802
    :cond_2a
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2b

    const-string v1, ", cs_creation_source_types="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 803
    :cond_2b
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2c

    const-string v1, ", cs_creation_source_types_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types_options:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 804
    :cond_2c
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v1, :cond_2d

    const-string v1, ", iip_is_instant_profile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile:Lcom/squareup/protos/client/rolodex/Filter$Option;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 805
    :cond_2d
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2e

    const-string v1, ", iip_is_instant_profile_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile_options:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2e
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Filter{"

    .line 806
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
