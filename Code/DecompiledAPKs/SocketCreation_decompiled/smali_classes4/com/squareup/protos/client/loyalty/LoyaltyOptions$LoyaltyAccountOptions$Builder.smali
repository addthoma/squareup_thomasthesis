.class public final Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LoyaltyOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;",
        "Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public force_contact_creation:Ljava/lang/Boolean;

.field public include_active_mappings:Ljava/lang/Boolean;

.field public include_contact:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 236
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;
    .locals 5

    .line 268
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;->include_active_mappings:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;->include_contact:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;->force_contact_creation:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 229
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions;

    move-result-object v0

    return-object v0
.end method

.method public force_contact_creation(Ljava/lang/Boolean;)Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;
    .locals 0

    .line 262
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;->force_contact_creation:Ljava/lang/Boolean;

    return-object p0
.end method

.method public include_active_mappings(Ljava/lang/Boolean;)Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;
    .locals 0

    .line 244
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;->include_active_mappings:Ljava/lang/Boolean;

    return-object p0
.end method

.method public include_contact(Ljava/lang/Boolean;)Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;
    .locals 0

    .line 253
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountOptions$Builder;->include_contact:Ljava/lang/Boolean;

    return-object p0
.end method
