.class public final enum Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;
.super Ljava/lang/Enum;
.source "SafetyNetAttestation.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/flipper/SafetyNetAttestation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SafetyNetWrapperStatusCode"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode$ProtoAdapter_SafetyNetWrapperStatusCode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CONNECTION_FAILED:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

.field public static final enum FAILURE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

.field public static final enum GMS_RATE_LIMITED:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

.field public static final enum GMS_REQUIRES_UPDATE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

.field public static final enum INIT:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

.field public static final enum INTERNAL_FAILURE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

.field public static final enum NONCE_FAILURE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

.field public static final enum PROGRESS_CONNECTED:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

.field public static final enum PROGRESS_CONNECTING:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

.field public static final enum SUCCESS:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 198
    new-instance v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    const/4 v1, 0x0

    const-string v2, "INIT"

    const/4 v3, -0x1

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->INIT:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    .line 203
    new-instance v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    const/4 v2, 0x1

    const-string v3, "FAILURE"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->FAILURE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    .line 208
    new-instance v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    const/4 v3, 0x2

    const-string v4, "SUCCESS"

    invoke-direct {v0, v4, v3, v2}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->SUCCESS:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    .line 213
    new-instance v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    const/4 v4, 0x3

    const-string v5, "INTERNAL_FAILURE"

    invoke-direct {v0, v5, v4, v3}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->INTERNAL_FAILURE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    .line 218
    new-instance v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    const/4 v5, 0x4

    const-string v6, "NONCE_FAILURE"

    invoke-direct {v0, v6, v5, v4}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->NONCE_FAILURE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    .line 223
    new-instance v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    const/4 v6, 0x5

    const-string v7, "GMS_REQUIRES_UPDATE"

    invoke-direct {v0, v7, v6, v5}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->GMS_REQUIRES_UPDATE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    .line 228
    new-instance v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    const/4 v7, 0x6

    const-string v8, "GMS_RATE_LIMITED"

    invoke-direct {v0, v8, v7, v6}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->GMS_RATE_LIMITED:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    .line 233
    new-instance v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    const/4 v8, 0x7

    const-string v9, "CONNECTION_FAILED"

    invoke-direct {v0, v9, v8, v7}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->CONNECTION_FAILED:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    .line 238
    new-instance v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    const/16 v9, 0x8

    const-string v10, "PROGRESS_CONNECTING"

    const/16 v11, 0x65

    invoke-direct {v0, v10, v9, v11}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->PROGRESS_CONNECTING:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    .line 243
    new-instance v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    const/16 v10, 0x9

    const-string v11, "PROGRESS_CONNECTED"

    const/16 v12, 0x66

    invoke-direct {v0, v11, v10, v12}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->PROGRESS_CONNECTED:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    .line 194
    sget-object v11, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->INIT:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    aput-object v11, v0, v1

    sget-object v1, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->FAILURE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->SUCCESS:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->INTERNAL_FAILURE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->NONCE_FAILURE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->GMS_REQUIRES_UPDATE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->GMS_RATE_LIMITED:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->CONNECTION_FAILED:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->PROGRESS_CONNECTING:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->PROGRESS_CONNECTED:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    aput-object v1, v0, v10

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->$VALUES:[Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    .line 245
    new-instance v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode$ProtoAdapter_SafetyNetWrapperStatusCode;

    invoke-direct {v0}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode$ProtoAdapter_SafetyNetWrapperStatusCode;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 249
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 250
    iput p3, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;
    .locals 1

    const/16 v0, 0x65

    if-eq p0, v0, :cond_1

    const/16 v0, 0x66

    if-eq p0, v0, :cond_0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 265
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->CONNECTION_FAILED:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    return-object p0

    .line 264
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->GMS_RATE_LIMITED:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    return-object p0

    .line 263
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->GMS_REQUIRES_UPDATE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    return-object p0

    .line 262
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->NONCE_FAILURE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    return-object p0

    .line 261
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->INTERNAL_FAILURE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    return-object p0

    .line 260
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->SUCCESS:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    return-object p0

    .line 259
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->FAILURE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    return-object p0

    .line 258
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->INIT:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    return-object p0

    .line 267
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->PROGRESS_CONNECTED:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    return-object p0

    .line 266
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->PROGRESS_CONNECTING:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;
    .locals 1

    .line 194
    const-class v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;
    .locals 1

    .line 194
    sget-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->$VALUES:[Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 274
    iget v0, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->value:I

    return v0
.end method
