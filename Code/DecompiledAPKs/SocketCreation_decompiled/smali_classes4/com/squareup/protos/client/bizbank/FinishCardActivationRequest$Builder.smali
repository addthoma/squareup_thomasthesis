.class public final Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FinishCardActivationRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;",
        "Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public activation_token:Ljava/lang/String;

.field public card_track_data:Lcom/squareup/protos/client/bills/CardData;

.field public manual_entry_cvv_expiration:Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 119
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public activation_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->activation_token:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;
    .locals 5

    .line 148
    new-instance v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->activation_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->manual_entry_cvv_expiration:Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;

    iget-object v3, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->card_track_data:Lcom/squareup/protos/client/bills/CardData;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;Lcom/squareup/protos/client/bills/CardData;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 112
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;

    move-result-object v0

    return-object v0
.end method

.method public card_track_data(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->card_track_data:Lcom/squareup/protos/client/bills/CardData;

    const/4 p1, 0x0

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->manual_entry_cvv_expiration:Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;

    return-object p0
.end method

.method public manual_entry_cvv_expiration(Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;)Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->manual_entry_cvv_expiration:Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;

    const/4 p1, 0x0

    .line 133
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->card_track_data:Lcom/squareup/protos/client/bills/CardData;

    return-object p0
.end method
