.class public final Lcom/squareup/protos/client/orders/OrderDisplayStateData;
.super Lcom/squareup/wire/Message;
.source "OrderDisplayStateData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/orders/OrderDisplayStateData$ProtoAdapter_OrderDisplayStateData;,
        Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;,
        Lcom/squareup/protos/client/orders/OrderDisplayStateData$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/orders/OrderDisplayStateData;",
        "Lcom/squareup/protos/client/orders/OrderDisplayStateData$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/orders/OrderDisplayStateData;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DISPLAY_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_OVERDUE_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_STATE:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

.field private static final serialVersionUID:J


# instance fields
.field public final display_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final overdue_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.orders.OrderDisplayStateData$OrderDisplayState#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$ProtoAdapter_OrderDisplayStateData;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$ProtoAdapter_OrderDisplayStateData;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->DO_NOT_USE:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    sput-object v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->DEFAULT_STATE:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 72
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/orders/OrderDisplayStateData;-><init>(Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 77
    sget-object v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 78
    iput-object p1, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    .line 79
    iput-object p2, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->display_name:Ljava/lang/String;

    .line 80
    iput-object p3, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->overdue_at:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 96
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 97
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    .line 98
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    .line 99
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->display_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->display_name:Ljava/lang/String;

    .line 100
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->overdue_at:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->overdue_at:Ljava/lang/String;

    .line 101
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 106
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 108
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->overdue_at:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 112
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/orders/OrderDisplayStateData$Builder;
    .locals 2

    .line 85
    new-instance v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$Builder;-><init>()V

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$Builder;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->display_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$Builder;->display_name:Ljava/lang/String;

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->overdue_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$Builder;->overdue_at:Ljava/lang/String;

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->newBuilder()Lcom/squareup/protos/client/orders/OrderDisplayStateData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    if-eqz v1, :cond_0

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 121
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", display_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->display_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->overdue_at:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", overdue_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->overdue_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "OrderDisplayStateData{"

    .line 123
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
