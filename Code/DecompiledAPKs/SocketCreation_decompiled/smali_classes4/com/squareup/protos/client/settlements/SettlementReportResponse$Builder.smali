.class public final Lcom/squareup/protos/client/settlements/SettlementReportResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SettlementReportResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/settlements/SettlementReportResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/settlements/SettlementReportResponse;",
        "Lcom/squareup/protos/client/settlements/SettlementReportResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public settlement_report:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 84
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/settlements/SettlementReportResponse;
    .locals 3

    .line 97
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementReportResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportResponse$Builder;->settlement_report:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/settlements/SettlementReportResponse;-><init>(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 81
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettlementReportResponse$Builder;->build()Lcom/squareup/protos/client/settlements/SettlementReportResponse;

    move-result-object v0

    return-object v0
.end method

.method public settlement_report(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)Lcom/squareup/protos/client/settlements/SettlementReportResponse$Builder;
    .locals 0

    .line 91
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReportResponse$Builder;->settlement_report:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    return-object p0
.end method
