.class public final Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ProxyMessageRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/felica/ProxyMessageRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/felica/ProxyMessageRequest;",
        "Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public connection_id:Ljava/lang/String;

.field public packet_data:Lokio/ByteString;

.field public transaction_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 111
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/felica/ProxyMessageRequest;
    .locals 5

    .line 131
    new-instance v0, Lcom/squareup/protos/client/felica/ProxyMessageRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;->connection_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;->transaction_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;->packet_data:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/felica/ProxyMessageRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 104
    invoke-virtual {p0}, Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;->build()Lcom/squareup/protos/client/felica/ProxyMessageRequest;

    move-result-object v0

    return-object v0
.end method

.method public connection_id(Ljava/lang/String;)Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;->connection_id:Ljava/lang/String;

    return-object p0
.end method

.method public packet_data(Lokio/ByteString;)Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;->packet_data:Lokio/ByteString;

    return-object p0
.end method

.method public transaction_id(Ljava/lang/String;)Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;->transaction_id:Ljava/lang/String;

    return-object p0
.end method
