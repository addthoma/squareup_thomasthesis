.class public final Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;",
        "Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public buyer_selected_locale:Ljava/lang/String;

.field public buyer_signature:Lcom/squareup/protos/common/payment/SignatureType;

.field public display_custom_amount:Ljava/lang/Boolean;

.field public display_quick_tip_options:Ljava/lang/Boolean;

.field public display_signature_line_on_paper_receipt:Ljava/lang/Boolean;

.field public display_tip_options_on_paper_receipt:Ljava/lang/Boolean;

.field public remaining_tender_balance_money:Lcom/squareup/protos/common/Money;

.field public tipping_preferences:Lcom/squareup/protos/common/tipping/TippingPreferences;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1769
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;
    .locals 11

    .line 1845
    new-instance v10, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->display_signature_line_on_paper_receipt:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->display_tip_options_on_paper_receipt:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->tipping_preferences:Lcom/squareup/protos/common/tipping/TippingPreferences;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->display_quick_tip_options:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->display_custom_amount:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->remaining_tender_balance_money:Lcom/squareup/protos/common/Money;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->buyer_selected_locale:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->buyer_signature:Lcom/squareup/protos/common/payment/SignatureType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/common/tipping/TippingPreferences;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/common/payment/SignatureType;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1752
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->build()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    move-result-object v0

    return-object v0
.end method

.method public buyer_selected_locale(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;
    .locals 0

    .line 1831
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->buyer_selected_locale:Ljava/lang/String;

    return-object p0
.end method

.method public buyer_signature(Lcom/squareup/protos/common/payment/SignatureType;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;
    .locals 0

    .line 1839
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->buyer_signature:Lcom/squareup/protos/common/payment/SignatureType;

    return-object p0
.end method

.method public display_custom_amount(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;
    .locals 0

    .line 1813
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->display_custom_amount:Ljava/lang/Boolean;

    return-object p0
.end method

.method public display_quick_tip_options(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;
    .locals 0

    .line 1805
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->display_quick_tip_options:Ljava/lang/Boolean;

    return-object p0
.end method

.method public display_signature_line_on_paper_receipt(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;
    .locals 0

    .line 1777
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->display_signature_line_on_paper_receipt:Ljava/lang/Boolean;

    return-object p0
.end method

.method public display_tip_options_on_paper_receipt(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;
    .locals 0

    .line 1786
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->display_tip_options_on_paper_receipt:Ljava/lang/Boolean;

    return-object p0
.end method

.method public remaining_tender_balance_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;
    .locals 0

    .line 1821
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->remaining_tender_balance_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public tipping_preferences(Lcom/squareup/protos/common/tipping/TippingPreferences;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;
    .locals 0

    .line 1797
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->tipping_preferences:Lcom/squareup/protos/common/tipping/TippingPreferences;

    return-object p0
.end method
