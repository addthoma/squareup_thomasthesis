.class public final Lcom/squareup/protos/client/orders/SearchOrdersResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SearchOrdersResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/SearchOrdersResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/orders/SearchOrdersResponse;",
        "Lcom/squareup/protos/client/orders/SearchOrdersResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cursor:Ljava/lang/String;

.field public orders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;"
        }
    .end annotation
.end field

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 118
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 119
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/orders/SearchOrdersResponse$Builder;->orders:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/orders/SearchOrdersResponse;
    .locals 5

    .line 148
    new-instance v0, Lcom/squareup/protos/client/orders/SearchOrdersResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/orders/SearchOrdersResponse$Builder;->orders:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/orders/SearchOrdersResponse$Builder;->cursor:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/orders/SearchOrdersResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 111
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/SearchOrdersResponse$Builder;->build()Lcom/squareup/protos/client/orders/SearchOrdersResponse;

    move-result-object v0

    return-object v0
.end method

.method public cursor(Ljava/lang/String;)Lcom/squareup/protos/client/orders/SearchOrdersResponse$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/client/orders/SearchOrdersResponse$Builder;->cursor:Ljava/lang/String;

    return-object p0
.end method

.method public orders(Ljava/util/List;)Lcom/squareup/protos/client/orders/SearchOrdersResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;)",
            "Lcom/squareup/protos/client/orders/SearchOrdersResponse$Builder;"
        }
    .end annotation

    .line 133
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/client/orders/SearchOrdersResponse$Builder;->orders:Ljava/util/List;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/orders/SearchOrdersResponse$Builder;
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/squareup/protos/client/orders/SearchOrdersResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
