.class public final Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RecurringSchedule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/RecurringSchedule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/RecurringSchedule;",
        "Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public end_date:Lcom/squareup/protos/common/time/YearMonthDay;

.field public recurrence_rule:Ljava/lang/String;

.field public start_date:Lcom/squareup/protos/common/time/YearMonthDay;

.field public starts_at:Lcom/squareup/protos/client/ISO8601Date;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 134
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/RecurringSchedule;
    .locals 7

    .line 171
    new-instance v6, Lcom/squareup/protos/client/invoice/RecurringSchedule;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;->starts_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;->recurrence_rule:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;->start_date:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v4, p0, Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;->end_date:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/invoice/RecurringSchedule;-><init>(Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 125
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;->build()Lcom/squareup/protos/client/invoice/RecurringSchedule;

    move-result-object v0

    return-object v0
.end method

.method public end_date(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;->end_date:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object p0
.end method

.method public recurrence_rule(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;->recurrence_rule:Ljava/lang/String;

    return-object p0
.end method

.method public start_date(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;
    .locals 0

    .line 157
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;->start_date:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object p0
.end method

.method public starts_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;->starts_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method
