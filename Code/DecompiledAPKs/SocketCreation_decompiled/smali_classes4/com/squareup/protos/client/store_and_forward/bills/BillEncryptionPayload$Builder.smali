.class public final Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BillEncryptionPayload.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;",
        "Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public add_tenders_request:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddTendersRequest;",
            ">;"
        }
    .end annotation
.end field

.field public complete_bill_request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

.field public creator_credential:Ljava/lang/String;

.field public extra_tender_detail:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;",
            ">;"
        }
    .end annotation
.end field

.field public headers:Lcom/squareup/protos/common/Headers;

.field public session_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 199
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 200
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->add_tenders_request:Ljava/util/List;

    .line 201
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->extra_tender_detail:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public add_tenders_request(Ljava/util/List;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddTendersRequest;",
            ">;)",
            "Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;"
        }
    .end annotation

    .line 232
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 233
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->add_tenders_request:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;
    .locals 9

    .line 275
    new-instance v8, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->session_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->add_tenders_request:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->complete_bill_request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    iget-object v4, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->extra_tender_detail:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->headers:Lcom/squareup/protos/common/Headers;

    iget-object v6, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->creator_credential:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/bills/CompleteBillRequest;Ljava/util/List;Lcom/squareup/protos/common/Headers;Ljava/lang/String;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 186
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->build()Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;

    move-result-object v0

    return-object v0
.end method

.method public complete_bill_request(Lcom/squareup/protos/client/bills/CompleteBillRequest;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;
    .locals 0

    .line 238
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->complete_bill_request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    return-object p0
.end method

.method public creator_credential(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;
    .locals 0

    .line 269
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->creator_credential:Ljava/lang/String;

    return-object p0
.end method

.method public extra_tender_detail(Ljava/util/List;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;",
            ">;)",
            "Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;"
        }
    .end annotation

    .line 243
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 244
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->extra_tender_detail:Ljava/util/List;

    return-object p0
.end method

.method public headers(Lcom/squareup/protos/common/Headers;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;
    .locals 0

    .line 260
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->headers:Lcom/squareup/protos/common/Headers;

    return-object p0
.end method

.method public session_token(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->session_token:Ljava/lang/String;

    return-object p0
.end method
