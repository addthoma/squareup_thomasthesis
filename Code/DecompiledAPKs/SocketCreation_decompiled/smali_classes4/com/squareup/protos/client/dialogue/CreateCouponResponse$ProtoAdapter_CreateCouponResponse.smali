.class final Lcom/squareup/protos/client/dialogue/CreateCouponResponse$ProtoAdapter_CreateCouponResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CreateCouponResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/dialogue/CreateCouponResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CreateCouponResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/dialogue/CreateCouponResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 115
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/dialogue/CreateCouponResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/dialogue/CreateCouponResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 134
    new-instance v0, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;-><init>()V

    .line 135
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 136
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 141
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 139
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/dialogue/Conversation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/dialogue/Conversation;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;->conversation(Lcom/squareup/protos/client/dialogue/Conversation;)Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;

    goto :goto_0

    .line 138
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;

    goto :goto_0

    .line 145
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 146
    invoke-virtual {v0}, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;->build()Lcom/squareup/protos/client/dialogue/CreateCouponResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 113
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$ProtoAdapter_CreateCouponResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/dialogue/CreateCouponResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/dialogue/CreateCouponResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 127
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/dialogue/CreateCouponResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 128
    sget-object v0, Lcom/squareup/protos/client/dialogue/Conversation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/dialogue/CreateCouponResponse;->conversation:Lcom/squareup/protos/client/dialogue/Conversation;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 129
    invoke-virtual {p2}, Lcom/squareup/protos/client/dialogue/CreateCouponResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 113
    check-cast p2, Lcom/squareup/protos/client/dialogue/CreateCouponResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$ProtoAdapter_CreateCouponResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/dialogue/CreateCouponResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/dialogue/CreateCouponResponse;)I
    .locals 4

    .line 120
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/dialogue/CreateCouponResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/dialogue/Conversation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/dialogue/CreateCouponResponse;->conversation:Lcom/squareup/protos/client/dialogue/Conversation;

    const/4 v3, 0x2

    .line 121
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 122
    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/CreateCouponResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 113
    check-cast p1, Lcom/squareup/protos/client/dialogue/CreateCouponResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$ProtoAdapter_CreateCouponResponse;->encodedSize(Lcom/squareup/protos/client/dialogue/CreateCouponResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/dialogue/CreateCouponResponse;)Lcom/squareup/protos/client/dialogue/CreateCouponResponse;
    .locals 2

    .line 151
    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/CreateCouponResponse;->newBuilder()Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;

    move-result-object p1

    .line 152
    iget-object v0, p1, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/Status;

    iput-object v0, p1, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 153
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;->conversation:Lcom/squareup/protos/client/dialogue/Conversation;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/dialogue/Conversation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;->conversation:Lcom/squareup/protos/client/dialogue/Conversation;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/dialogue/Conversation;

    iput-object v0, p1, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;->conversation:Lcom/squareup/protos/client/dialogue/Conversation;

    .line 154
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 155
    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$Builder;->build()Lcom/squareup/protos/client/dialogue/CreateCouponResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 113
    check-cast p1, Lcom/squareup/protos/client/dialogue/CreateCouponResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/dialogue/CreateCouponResponse$ProtoAdapter_CreateCouponResponse;->redact(Lcom/squareup/protos/client/dialogue/CreateCouponResponse;)Lcom/squareup/protos/client/dialogue/CreateCouponResponse;

    move-result-object p1

    return-object p1
.end method
