.class public final Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;
.super Lcom/squareup/wire/Message;
.source "UploadTransactionLedgerRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$ProtoAdapter_UploadTransactionLedgerRequest;,
        Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;",
        "Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DIAGNOSTICS_DATA:Lokio/ByteString;

.field private static final serialVersionUID:J


# instance fields
.field public final diagnostics_data:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x3
    .end annotation
.end field

.field public final entries:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.transaction_ledger.TransactionLedgerEntry#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;",
            ">;"
        }
    .end annotation
.end field

.field public final id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$ProtoAdapter_UploadTransactionLedgerRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$ProtoAdapter_UploadTransactionLedgerRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 26
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->DEFAULT_DIAGNOSTICS_DATA:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 63
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/util/List;Lokio/ByteString;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Ljava/util/List;Lokio/ByteString;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;",
            ">;",
            "Lokio/ByteString;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 68
    sget-object v0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 69
    iput-object p1, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    const-string p1, "entries"

    .line 70
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->entries:Ljava/util/List;

    .line 71
    iput-object p3, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->diagnostics_data:Lokio/ByteString;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 87
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 88
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    .line 90
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->entries:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->entries:Ljava/util/List;

    .line 91
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->diagnostics_data:Lokio/ByteString;

    iget-object p1, p1, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->diagnostics_data:Lokio/ByteString;

    .line 92
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 97
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 99
    invoke-virtual {p0}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->entries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->diagnostics_data:Lokio/ByteString;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 103
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;
    .locals 2

    .line 76
    new-instance v0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;-><init>()V

    .line 77
    iget-object v1, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    .line 78
    iget-object v1, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->entries:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->entries:Ljava/util/List;

    .line 79
    iget-object v1, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->diagnostics_data:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->diagnostics_data:Lokio/ByteString;

    .line 80
    invoke-virtual {p0}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->newBuilder()Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 112
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->entries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", entries="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->entries:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 113
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->diagnostics_data:Lokio/ByteString;

    if-eqz v1, :cond_2

    const-string v1, ", diagnostics_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->diagnostics_data:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "UploadTransactionLedgerRequest{"

    .line 114
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
