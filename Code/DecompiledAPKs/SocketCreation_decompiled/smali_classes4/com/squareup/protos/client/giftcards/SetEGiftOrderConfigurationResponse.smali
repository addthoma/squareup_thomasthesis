.class public final Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;
.super Lcom/squareup/wire/Message;
.source "SetEGiftOrderConfigurationResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$ProtoAdapter_SetEGiftOrderConfigurationResponse;,
        Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;",
        "Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final all_egift_themes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.giftcards.EGiftTheme#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;"
        }
    .end annotation
.end field

.field public final order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.giftcards.EGiftOrderConfiguration#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$ProtoAdapter_SetEGiftOrderConfigurationResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$ProtoAdapter_SetEGiftOrderConfigurationResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Status;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;",
            "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
            ")V"
        }
    .end annotation

    .line 54
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Status;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;",
            "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 59
    sget-object v0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 60
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->status:Lcom/squareup/protos/client/Status;

    const-string p1, "all_egift_themes"

    .line 61
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->all_egift_themes:Ljava/util/List;

    .line 62
    iput-object p3, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 78
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 79
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;

    .line 80
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->status:Lcom/squareup/protos/client/Status;

    .line 81
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->all_egift_themes:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->all_egift_themes:Ljava/util/List;

    .line 82
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    .line 83
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 88
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 90
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->all_egift_themes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 94
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;
    .locals 2

    .line 67
    new-instance v0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;-><init>()V

    .line 68
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 69
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->all_egift_themes:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->all_egift_themes:Ljava/util/List;

    .line 70
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    .line 71
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->newBuilder()Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 103
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->all_egift_themes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", all_egift_themes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->all_egift_themes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 104
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    if-eqz v1, :cond_2

    const-string v1, ", order_configuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SetEGiftOrderConfigurationResponse{"

    .line 105
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
