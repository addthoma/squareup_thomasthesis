.class public final enum Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;
.super Ljava/lang/Enum;
.source "VerifyShippingAddressResponse.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AddressVerificationStatus"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus$ProtoAdapter_AddressVerificationStatus;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum FAILED:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

.field public static final enum UNKNOWN_STATUS:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

.field public static final enum VALIDATED:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

.field public static final enum VALIDATION_SERVICE_ERROR:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 216
    new-instance v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_STATUS"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->UNKNOWN_STATUS:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    .line 221
    new-instance v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    const/4 v2, 0x1

    const-string v3, "VALIDATED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->VALIDATED:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    .line 226
    new-instance v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    const/4 v3, 0x2

    const-string v4, "FAILED"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->FAILED:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    .line 231
    new-instance v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    const/4 v4, 0x3

    const-string v5, "VALIDATION_SERVICE_ERROR"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->VALIDATION_SERVICE_ERROR:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    .line 215
    sget-object v5, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->UNKNOWN_STATUS:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->VALIDATED:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->FAILED:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->VALIDATION_SERVICE_ERROR:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->$VALUES:[Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    .line 233
    new-instance v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus$ProtoAdapter_AddressVerificationStatus;

    invoke-direct {v0}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus$ProtoAdapter_AddressVerificationStatus;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 237
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 238
    iput p3, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 249
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->VALIDATION_SERVICE_ERROR:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    return-object p0

    .line 248
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->FAILED:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    return-object p0

    .line 247
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->VALIDATED:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    return-object p0

    .line 246
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->UNKNOWN_STATUS:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;
    .locals 1

    .line 215
    const-class v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;
    .locals 1

    .line 215
    sget-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->$VALUES:[Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 256
    iget v0, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->value:I

    return v0
.end method
