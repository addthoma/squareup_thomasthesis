.class public final Lcom/squareup/protos/client/bills/Itemization$Event$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Itemization$Event;",
        "Lcom/squareup/protos/client/bills/Itemization$Event$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public created_at:Lcom/squareup/protos/client/ISO8601Date;

.field public creator_details:Lcom/squareup/protos/client/CreatorDetails;

.field public event_id_pair:Lcom/squareup/protos/client/IdPair;

.field public event_type:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

.field public kds_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;

.field public reason:Ljava/lang/String;

.field public split_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3255
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Itemization$Event;
    .locals 10

    .line 3311
    new-instance v9, Lcom/squareup/protos/client/bills/Itemization$Event;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->event_type:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->reason:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->split_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->kds_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/bills/Itemization$Event;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Itemization$Event$EventType;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/CreatorDetails;Ljava/lang/String;Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 3240
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object v0

    return-object v0
.end method

.method public created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;
    .locals 0

    .line 3272
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;
    .locals 0

    .line 3280
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    return-object p0
.end method

.method public event_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;
    .locals 0

    .line 3259
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public event_type(Lcom/squareup/protos/client/bills/Itemization$Event$EventType;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;
    .locals 0

    .line 3264
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->event_type:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    return-object p0
.end method

.method public kds_event_details(Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;
    .locals 0

    .line 3305
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->kds_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;

    return-object p0
.end method

.method public reason(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;
    .locals 0

    .line 3289
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->reason:Ljava/lang/String;

    return-object p0
.end method

.method public split_event_details(Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;)Lcom/squareup/protos/client/bills/Itemization$Event$Builder;
    .locals 0

    .line 3297
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$Builder;->split_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;

    return-object p0
.end method
