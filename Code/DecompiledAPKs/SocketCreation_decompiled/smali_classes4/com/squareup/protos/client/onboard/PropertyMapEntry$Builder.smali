.class public final Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PropertyMapEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/onboard/PropertyMapEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/onboard/PropertyMapEntry;",
        "Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public boolean_value:Ljava/lang/Boolean;

.field public integer_value:Ljava/lang/Integer;

.field public string_value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 118
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public boolean_value(Ljava/lang/Boolean;)Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;->boolean_value:Ljava/lang/Boolean;

    const/4 p1, 0x0

    .line 130
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;->string_value:Ljava/lang/String;

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;->integer_value:Ljava/lang/Integer;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/onboard/PropertyMapEntry;
    .locals 5

    .line 144
    new-instance v0, Lcom/squareup/protos/client/onboard/PropertyMapEntry;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;->string_value:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;->boolean_value:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;->integer_value:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/onboard/PropertyMapEntry;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 111
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;->build()Lcom/squareup/protos/client/onboard/PropertyMapEntry;

    move-result-object v0

    return-object v0
.end method

.method public integer_value(Ljava/lang/Integer;)Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;->integer_value:Ljava/lang/Integer;

    const/4 p1, 0x0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;->string_value:Ljava/lang/String;

    .line 138
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;->boolean_value:Ljava/lang/Boolean;

    return-object p0
.end method

.method public string_value(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;->string_value:Ljava/lang/String;

    const/4 p1, 0x0

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;->boolean_value:Ljava/lang/Boolean;

    .line 124
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/PropertyMapEntry$Builder;->integer_value:Ljava/lang/Integer;

    return-object p0
.end method
