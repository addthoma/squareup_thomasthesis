.class public final Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetResidualBillResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public residual_surcharge:Lcom/squareup/protos/client/bills/SurchargeLineItem;

.field public source_bill_server_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1106
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;
    .locals 4

    .line 1128
    new-instance v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;->source_bill_server_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;->residual_surcharge:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/SurchargeLineItem;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1101
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;->build()Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;

    move-result-object v0

    return-object v0
.end method

.method public residual_surcharge(Lcom/squareup/protos/client/bills/SurchargeLineItem;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;
    .locals 0

    .line 1122
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;->residual_surcharge:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    return-object p0
.end method

.method public source_bill_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;
    .locals 0

    .line 1113
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;->source_bill_server_token:Ljava/lang/String;

    return-object p0
.end method
