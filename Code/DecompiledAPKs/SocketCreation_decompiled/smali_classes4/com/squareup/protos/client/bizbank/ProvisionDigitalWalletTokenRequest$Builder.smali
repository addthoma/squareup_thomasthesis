.class public final Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ProvisionDigitalWalletTokenRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;",
        "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public apple_pay_request:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;

.field public card_token:Ljava/lang/String;

.field public device_type:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

.field public google_pay_request:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 133
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public apple_pay_request(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->apple_pay_request:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;

    const/4 p1, 0x0

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->google_pay_request:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;
    .locals 7

    .line 163
    new-instance v6, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->card_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->device_type:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    iget-object v3, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->apple_pay_request:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;

    iget-object v4, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->google_pay_request:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;

    move-result-object v0

    return-object v0
.end method

.method public card_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->card_token:Ljava/lang/String;

    return-object p0
.end method

.method public device_type(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->device_type:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    return-object p0
.end method

.method public google_pay_request(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->google_pay_request:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;

    const/4 p1, 0x0

    .line 157
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->apple_pay_request:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;

    return-object p0
.end method
