.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Event"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$ProtoAdapter_Event;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_EVENT_TYPE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;

.field private static final serialVersionUID:J


# instance fields
.field public final created_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final creator_details:Lcom/squareup/protos/client/CreatorDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.CreatorDetails#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final event_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$CoursingOptions$Course$Event$EventType#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 3690
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$ProtoAdapter_Event;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$ProtoAdapter_Event;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 3694
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;->UNKNOWN:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->DEFAULT_EVENT_TYPE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/CreatorDetails;)V
    .locals 6

    .line 3734
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/CreatorDetails;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/CreatorDetails;Lokio/ByteString;)V
    .locals 1

    .line 3739
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 3740
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 3741
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;

    .line 3742
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 3743
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 3760
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 3761
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;

    .line 3762
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 3763
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;

    .line 3764
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 3765
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 3766
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 3771
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 3773
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 3774
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3775
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3776
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3777
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/CreatorDetails;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 3778
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;
    .locals 2

    .line 3748
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;-><init>()V

    .line 3749
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 3750
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;

    .line 3751
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 3752
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 3753
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 3689
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 3785
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 3786
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", event_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3787
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;

    if-eqz v1, :cond_1

    const-string v1, ", event_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3788
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_2

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3789
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_3

    const-string v1, ", creator_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Event{"

    .line 3790
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
