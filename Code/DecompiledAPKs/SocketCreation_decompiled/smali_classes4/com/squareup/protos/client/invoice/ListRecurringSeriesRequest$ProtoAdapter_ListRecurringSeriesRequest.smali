.class final Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$ProtoAdapter_ListRecurringSeriesRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ListRecurringSeriesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ListRecurringSeriesRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 252
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 277
    new-instance v0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;-><init>()V

    .line 278
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 279
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x5

    if-eq v3, v4, :cond_1

    const/4 v4, 0x6

    if-eq v3, v4, :cond_0

    .line 294
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 292
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->query(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;

    goto :goto_0

    .line 291
    :cond_1
    sget-object v3, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTimeInterval;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->date_range(Lcom/squareup/protos/common/time/DateTimeInterval;)Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;

    goto :goto_0

    .line 285
    :cond_2
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->state_filter(Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;)Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 287
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 282
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;

    goto :goto_0

    .line 281
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->paging_key(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;

    goto :goto_0

    .line 298
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 299
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->build()Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 250
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$ProtoAdapter_ListRecurringSeriesRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 267
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->paging_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 268
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->limit:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 269
    sget-object v0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->state_filter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 270
    sget-object v0, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 271
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->query:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 272
    invoke-virtual {p2}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 250
    check-cast p2, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$ProtoAdapter_ListRecurringSeriesRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;)I
    .locals 4

    .line 257
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->paging_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->limit:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 258
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->state_filter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    const/4 v3, 0x3

    .line 259
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    const/4 v3, 0x5

    .line 260
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->query:Ljava/lang/String;

    const/4 v3, 0x6

    .line 261
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 262
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 250
    check-cast p1, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$ProtoAdapter_ListRecurringSeriesRequest;->encodedSize(Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;)Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;
    .locals 2

    .line 304
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->newBuilder()Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;

    move-result-object p1

    .line 305
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTimeInterval;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 306
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 307
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->build()Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 250
    check-cast p1, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$ProtoAdapter_ListRecurringSeriesRequest;->redact(Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;)Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;

    move-result-object p1

    return-object p1
.end method
