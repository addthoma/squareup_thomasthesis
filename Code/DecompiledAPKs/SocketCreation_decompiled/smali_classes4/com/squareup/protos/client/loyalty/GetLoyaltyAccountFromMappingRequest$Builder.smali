.class public final Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetLoyaltyAccountFromMappingRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

.field public options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 105
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;
    .locals 4

    .line 130
    new-instance v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;-><init>(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;Lcom/squareup/protos/client/loyalty/LoyaltyOptions;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;

    move-result-object v0

    return-object v0
.end method

.method public loyalty_account_mapping(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;)Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    return-object p0
.end method

.method public options(Lcom/squareup/protos/client/loyalty/LoyaltyOptions;)Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;
    .locals 0

    .line 124
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    return-object p0
.end method
