.class public final enum Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;
.super Ljava/lang/Enum;
.source "LoyaltyStatus.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/LoyaltyStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ReasonForPointAccrual"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual$ProtoAdapter_ReasonForPointAccrual;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

.field public static final enum ACCRUAL_REASON_CARD_LINKED_TO_PHONE:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

.field public static final enum ACCRUAL_REASON_CUSTOMER_IN_CART:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

.field public static final enum ACCRUAL_REASON_PHONE_KEYED_IN:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

.field public static final enum ACCRUAL_REASON_RECEIPT_PREFERENCE:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

.field public static final enum ACCRUAL_REASON_UNKNOWN:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 486
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    const/4 v1, 0x0

    const-string v2, "ACCRUAL_REASON_UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ACCRUAL_REASON_UNKNOWN:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    .line 492
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    const/4 v2, 0x1

    const-string v3, "ACCRUAL_REASON_PHONE_KEYED_IN"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ACCRUAL_REASON_PHONE_KEYED_IN:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    .line 497
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    const/4 v3, 0x2

    const-string v4, "ACCRUAL_REASON_CUSTOMER_IN_CART"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ACCRUAL_REASON_CUSTOMER_IN_CART:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    .line 502
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    const/4 v4, 0x3

    const-string v5, "ACCRUAL_REASON_CARD_LINKED_TO_PHONE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ACCRUAL_REASON_CARD_LINKED_TO_PHONE:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    .line 507
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    const/4 v5, 0x4

    const-string v6, "ACCRUAL_REASON_RECEIPT_PREFERENCE"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ACCRUAL_REASON_RECEIPT_PREFERENCE:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    .line 485
    sget-object v6, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ACCRUAL_REASON_UNKNOWN:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ACCRUAL_REASON_PHONE_KEYED_IN:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ACCRUAL_REASON_CUSTOMER_IN_CART:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ACCRUAL_REASON_CARD_LINKED_TO_PHONE:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ACCRUAL_REASON_RECEIPT_PREFERENCE:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->$VALUES:[Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    .line 509
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual$ProtoAdapter_ReasonForPointAccrual;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual$ProtoAdapter_ReasonForPointAccrual;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 513
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 514
    iput p3, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 526
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ACCRUAL_REASON_RECEIPT_PREFERENCE:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    return-object p0

    .line 525
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ACCRUAL_REASON_CARD_LINKED_TO_PHONE:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    return-object p0

    .line 524
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ACCRUAL_REASON_CUSTOMER_IN_CART:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    return-object p0

    .line 523
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ACCRUAL_REASON_PHONE_KEYED_IN:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    return-object p0

    .line 522
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ACCRUAL_REASON_UNKNOWN:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;
    .locals 1

    .line 485
    const-class v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;
    .locals 1

    .line 485
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->$VALUES:[Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 533
    iget v0, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->value:I

    return v0
.end method
