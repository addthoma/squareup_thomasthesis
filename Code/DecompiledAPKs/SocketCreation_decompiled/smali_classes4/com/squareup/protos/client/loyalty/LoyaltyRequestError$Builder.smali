.class public final Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LoyaltyRequestError.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;",
        "Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public code:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

.field public details:Ljava/lang/String;

.field public field:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;

.field public raw_value:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 164
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;
    .locals 8

    .line 211
    new-instance v7, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;->field:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;->value:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;->raw_value:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;->code:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    iget-object v5, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;->details:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;-><init>(Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 153
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;

    move-result-object v0

    return-object v0
.end method

.method public code(Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;)Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;
    .locals 0

    .line 197
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;->code:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    return-object p0
.end method

.method public details(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;->details:Ljava/lang/String;

    return-object p0
.end method

.method public field(Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;)Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;->field:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;

    return-object p0
.end method

.method public raw_value(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;->raw_value:Ljava/lang/String;

    return-object p0
.end method

.method public value(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;
    .locals 0

    .line 181
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;->value:Ljava/lang/String;

    return-object p0
.end method
