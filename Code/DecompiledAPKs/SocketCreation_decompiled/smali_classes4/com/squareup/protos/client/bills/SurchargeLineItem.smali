.class public final Lcom/squareup/protos/client/bills/SurchargeLineItem;
.super Lcom/squareup/wire/Message;
.source "SurchargeLineItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/SurchargeLineItem$ProtoAdapter_SurchargeLineItem;,
        Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;,
        Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;,
        Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/SurchargeLineItem;",
        "Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/SurchargeLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_WRITE_ONLY_DELETED:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final amounts:Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.SurchargeLineItem$Amounts#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final backing_details:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.SurchargeLineItem$BackingDetails#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final fee:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.FeeLineItem#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public final surcharge_line_item_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final write_only_deleted:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$ProtoAdapter_SurchargeLineItem;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/SurchargeLineItem$ProtoAdapter_SurchargeLineItem;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 31
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->DEFAULT_WRITE_ONLY_DELETED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;Ljava/lang/Boolean;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;",
            "Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;)V"
        }
    .end annotation

    .line 69
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bills/SurchargeLineItem;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;",
            "Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 75
    sget-object v0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 76
    iput-object p1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->surcharge_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 77
    iput-object p2, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->backing_details:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    .line 78
    iput-object p3, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->amounts:Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    .line 79
    iput-object p4, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->write_only_deleted:Ljava/lang/Boolean;

    const-string p1, "fee"

    .line 80
    invoke-static {p1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->fee:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 98
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 99
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/SurchargeLineItem;

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/SurchargeLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/SurchargeLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->surcharge_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem;->surcharge_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 101
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->backing_details:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem;->backing_details:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    .line 102
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->amounts:Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem;->amounts:Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    .line 103
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->write_only_deleted:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem;->write_only_deleted:Ljava/lang/Boolean;

    .line 104
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->fee:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem;->fee:Ljava/util/List;

    .line 105
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 110
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 112
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/SurchargeLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->surcharge_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->backing_details:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->amounts:Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->write_only_deleted:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->fee:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 118
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;
    .locals 2

    .line 85
    new-instance v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;-><init>()V

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->surcharge_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->surcharge_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->backing_details:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->backing_details:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->amounts:Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->write_only_deleted:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->write_only_deleted:Ljava/lang/Boolean;

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->fee:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->fee:Ljava/util/List;

    .line 91
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/SurchargeLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/SurchargeLineItem;->newBuilder()Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->surcharge_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", surcharge_line_item_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->surcharge_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 127
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->backing_details:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    if-eqz v1, :cond_1

    const-string v1, ", backing_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->backing_details:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 128
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->amounts:Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    if-eqz v1, :cond_2

    const-string v1, ", amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->amounts:Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 129
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->write_only_deleted:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", write_only_deleted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->write_only_deleted:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 130
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->fee:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", fee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->fee:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SurchargeLineItem{"

    .line 131
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
