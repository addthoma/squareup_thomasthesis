.class public final enum Lcom/squareup/protos/client/irf/Field$FieldType;
.super Ljava/lang/Enum;
.source "Field.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/irf/Field;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FieldType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/irf/Field$FieldType$ProtoAdapter_FieldType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/irf/Field$FieldType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/irf/Field$FieldType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/irf/Field$FieldType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CHECKBOX:Lcom/squareup/protos/client/irf/Field$FieldType;

.field public static final enum FILE:Lcom/squareup/protos/client/irf/Field$FieldType;

.field public static final enum HTML:Lcom/squareup/protos/client/irf/Field$FieldType;

.field public static final enum MULTI_FILE:Lcom/squareup/protos/client/irf/Field$FieldType;

.field public static final enum RADIO:Lcom/squareup/protos/client/irf/Field$FieldType;

.field public static final enum SELECT:Lcom/squareup/protos/client/irf/Field$FieldType;

.field public static final enum TEXT:Lcom/squareup/protos/client/irf/Field$FieldType;

.field public static final enum TEXTAREA:Lcom/squareup/protos/client/irf/Field$FieldType;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/irf/Field$FieldType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 297
    new-instance v0, Lcom/squareup/protos/client/irf/Field$FieldType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/irf/Field$FieldType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/irf/Field$FieldType;->UNKNOWN:Lcom/squareup/protos/client/irf/Field$FieldType;

    .line 299
    new-instance v0, Lcom/squareup/protos/client/irf/Field$FieldType;

    const/4 v2, 0x1

    const-string v3, "CHECKBOX"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/irf/Field$FieldType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/irf/Field$FieldType;->CHECKBOX:Lcom/squareup/protos/client/irf/Field$FieldType;

    .line 301
    new-instance v0, Lcom/squareup/protos/client/irf/Field$FieldType;

    const/4 v3, 0x2

    const-string v4, "RADIO"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/irf/Field$FieldType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/irf/Field$FieldType;->RADIO:Lcom/squareup/protos/client/irf/Field$FieldType;

    .line 303
    new-instance v0, Lcom/squareup/protos/client/irf/Field$FieldType;

    const/4 v4, 0x3

    const-string v5, "FILE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/irf/Field$FieldType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/irf/Field$FieldType;->FILE:Lcom/squareup/protos/client/irf/Field$FieldType;

    .line 305
    new-instance v0, Lcom/squareup/protos/client/irf/Field$FieldType;

    const/4 v5, 0x4

    const-string v6, "MULTI_FILE"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/irf/Field$FieldType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/irf/Field$FieldType;->MULTI_FILE:Lcom/squareup/protos/client/irf/Field$FieldType;

    .line 307
    new-instance v0, Lcom/squareup/protos/client/irf/Field$FieldType;

    const/4 v6, 0x5

    const-string v7, "SELECT"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/irf/Field$FieldType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/irf/Field$FieldType;->SELECT:Lcom/squareup/protos/client/irf/Field$FieldType;

    .line 309
    new-instance v0, Lcom/squareup/protos/client/irf/Field$FieldType;

    const/4 v7, 0x6

    const-string v8, "TEXT"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/irf/Field$FieldType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/irf/Field$FieldType;->TEXT:Lcom/squareup/protos/client/irf/Field$FieldType;

    .line 311
    new-instance v0, Lcom/squareup/protos/client/irf/Field$FieldType;

    const/4 v8, 0x7

    const-string v9, "TEXTAREA"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/irf/Field$FieldType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/irf/Field$FieldType;->TEXTAREA:Lcom/squareup/protos/client/irf/Field$FieldType;

    .line 316
    new-instance v0, Lcom/squareup/protos/client/irf/Field$FieldType;

    const/16 v9, 0x8

    const-string v10, "HTML"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/irf/Field$FieldType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/irf/Field$FieldType;->HTML:Lcom/squareup/protos/client/irf/Field$FieldType;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/protos/client/irf/Field$FieldType;

    .line 296
    sget-object v10, Lcom/squareup/protos/client/irf/Field$FieldType;->UNKNOWN:Lcom/squareup/protos/client/irf/Field$FieldType;

    aput-object v10, v0, v1

    sget-object v1, Lcom/squareup/protos/client/irf/Field$FieldType;->CHECKBOX:Lcom/squareup/protos/client/irf/Field$FieldType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/irf/Field$FieldType;->RADIO:Lcom/squareup/protos/client/irf/Field$FieldType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/irf/Field$FieldType;->FILE:Lcom/squareup/protos/client/irf/Field$FieldType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/irf/Field$FieldType;->MULTI_FILE:Lcom/squareup/protos/client/irf/Field$FieldType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/irf/Field$FieldType;->SELECT:Lcom/squareup/protos/client/irf/Field$FieldType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/irf/Field$FieldType;->TEXT:Lcom/squareup/protos/client/irf/Field$FieldType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/irf/Field$FieldType;->TEXTAREA:Lcom/squareup/protos/client/irf/Field$FieldType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/irf/Field$FieldType;->HTML:Lcom/squareup/protos/client/irf/Field$FieldType;

    aput-object v1, v0, v9

    sput-object v0, Lcom/squareup/protos/client/irf/Field$FieldType;->$VALUES:[Lcom/squareup/protos/client/irf/Field$FieldType;

    .line 318
    new-instance v0, Lcom/squareup/protos/client/irf/Field$FieldType$ProtoAdapter_FieldType;

    invoke-direct {v0}, Lcom/squareup/protos/client/irf/Field$FieldType$ProtoAdapter_FieldType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/irf/Field$FieldType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 322
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 323
    iput p3, p0, Lcom/squareup/protos/client/irf/Field$FieldType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/irf/Field$FieldType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 339
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/irf/Field$FieldType;->HTML:Lcom/squareup/protos/client/irf/Field$FieldType;

    return-object p0

    .line 338
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/irf/Field$FieldType;->TEXTAREA:Lcom/squareup/protos/client/irf/Field$FieldType;

    return-object p0

    .line 337
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/irf/Field$FieldType;->TEXT:Lcom/squareup/protos/client/irf/Field$FieldType;

    return-object p0

    .line 336
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/irf/Field$FieldType;->SELECT:Lcom/squareup/protos/client/irf/Field$FieldType;

    return-object p0

    .line 335
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/irf/Field$FieldType;->MULTI_FILE:Lcom/squareup/protos/client/irf/Field$FieldType;

    return-object p0

    .line 334
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/irf/Field$FieldType;->FILE:Lcom/squareup/protos/client/irf/Field$FieldType;

    return-object p0

    .line 333
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/irf/Field$FieldType;->RADIO:Lcom/squareup/protos/client/irf/Field$FieldType;

    return-object p0

    .line 332
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/irf/Field$FieldType;->CHECKBOX:Lcom/squareup/protos/client/irf/Field$FieldType;

    return-object p0

    .line 331
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/irf/Field$FieldType;->UNKNOWN:Lcom/squareup/protos/client/irf/Field$FieldType;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/irf/Field$FieldType;
    .locals 1

    .line 296
    const-class v0, Lcom/squareup/protos/client/irf/Field$FieldType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/irf/Field$FieldType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/irf/Field$FieldType;
    .locals 1

    .line 296
    sget-object v0, Lcom/squareup/protos/client/irf/Field$FieldType;->$VALUES:[Lcom/squareup/protos/client/irf/Field$FieldType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/irf/Field$FieldType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/irf/Field$FieldType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 346
    iget v0, p0, Lcom/squareup/protos/client/irf/Field$FieldType;->value:I

    return v0
.end method
