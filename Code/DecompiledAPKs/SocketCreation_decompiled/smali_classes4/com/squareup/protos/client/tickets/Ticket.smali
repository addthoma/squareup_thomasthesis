.class public final Lcom/squareup/protos/client/tickets/Ticket;
.super Lcom/squareup/wire/Message;
.source "Ticket.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/tickets/Ticket$ProtoAdapter_Ticket;,
        Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;,
        Lcom/squareup/protos/client/tickets/Ticket$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/tickets/Ticket;",
        "Lcom/squareup/protos/client/tickets/Ticket$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/tickets/Ticket;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_NOTE:Ljava/lang/String; = ""

.field public static final DEFAULT_TENDER_ID_DEPRECATED:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final cart:Lcom/squareup/protos/client/bills/Cart;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final client_updated_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final close_event:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.tickets.Ticket$CloseEvent#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final closed_at_deprecated:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x4
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final note:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final tender_id_deprecated:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final ticket_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final total_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final transient_:Lcom/squareup/protos/client/tickets/Transient;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.tickets.Transient#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.tickets.VectorClock#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/protos/client/tickets/Ticket$ProtoAdapter_Ticket;

    invoke-direct {v0}, Lcom/squareup/protos/client/tickets/Ticket$ProtoAdapter_Ticket;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/tickets/Ticket;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/tickets/VectorClock;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/tickets/Transient;)V
    .locals 13

    .line 162
    sget-object v12, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/client/tickets/Ticket;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/tickets/VectorClock;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/tickets/Transient;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/tickets/VectorClock;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/tickets/Transient;Lokio/ByteString;)V
    .locals 1

    .line 169
    sget-object v0, Lcom/squareup/protos/client/tickets/Ticket;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p12}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 170
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/Ticket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 171
    iput-object p2, p0, Lcom/squareup/protos/client/tickets/Ticket;->vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;

    .line 172
    iput-object p3, p0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 173
    iput-object p4, p0, Lcom/squareup/protos/client/tickets/Ticket;->name:Ljava/lang/String;

    .line 174
    iput-object p5, p0, Lcom/squareup/protos/client/tickets/Ticket;->client_updated_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 175
    iput-object p6, p0, Lcom/squareup/protos/client/tickets/Ticket;->note:Ljava/lang/String;

    .line 176
    iput-object p7, p0, Lcom/squareup/protos/client/tickets/Ticket;->close_event:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    .line 177
    iput-object p8, p0, Lcom/squareup/protos/client/tickets/Ticket;->total_money:Lcom/squareup/protos/common/Money;

    .line 178
    iput-object p9, p0, Lcom/squareup/protos/client/tickets/Ticket;->tender_id_deprecated:Ljava/lang/String;

    .line 179
    iput-object p10, p0, Lcom/squareup/protos/client/tickets/Ticket;->closed_at_deprecated:Lcom/squareup/protos/client/ISO8601Date;

    .line 180
    iput-object p11, p0, Lcom/squareup/protos/client/tickets/Ticket;->transient_:Lcom/squareup/protos/client/tickets/Transient;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 204
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/tickets/Ticket;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 205
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/tickets/Ticket;

    .line 206
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/Ticket;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/Ticket;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/tickets/Ticket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 207
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;

    iget-object v3, p1, Lcom/squareup/protos/client/tickets/Ticket;->vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;

    .line 208
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v3, p1, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 209
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/tickets/Ticket;->name:Ljava/lang/String;

    .line 210
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->client_updated_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/tickets/Ticket;->client_updated_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 211
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->note:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/tickets/Ticket;->note:Ljava/lang/String;

    .line 212
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->close_event:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    iget-object v3, p1, Lcom/squareup/protos/client/tickets/Ticket;->close_event:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    .line 213
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/tickets/Ticket;->total_money:Lcom/squareup/protos/common/Money;

    .line 214
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->tender_id_deprecated:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/tickets/Ticket;->tender_id_deprecated:Ljava/lang/String;

    .line 215
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->closed_at_deprecated:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/tickets/Ticket;->closed_at_deprecated:Lcom/squareup/protos/client/ISO8601Date;

    .line 216
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->transient_:Lcom/squareup/protos/client/tickets/Transient;

    iget-object p1, p1, Lcom/squareup/protos/client/tickets/Ticket;->transient_:Lcom/squareup/protos/client/tickets/Transient;

    .line 217
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 222
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_b

    .line 224
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/Ticket;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 225
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 226
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/tickets/VectorClock;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 227
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 228
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->name:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 229
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->client_updated_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 230
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->note:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 231
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->close_event:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 232
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->total_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 233
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->tender_id_deprecated:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 234
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->closed_at_deprecated:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 235
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->transient_:Lcom/squareup/protos/client/tickets/Transient;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/client/tickets/Transient;->hashCode()I

    move-result v2

    :cond_a
    add-int/2addr v0, v2

    .line 236
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_b
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/tickets/Ticket$Builder;
    .locals 2

    .line 185
    new-instance v0, Lcom/squareup/protos/client/tickets/Ticket$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/tickets/Ticket$Builder;-><init>()V

    .line 186
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 187
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;

    iput-object v1, v0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;

    .line 188
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iput-object v1, v0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 189
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->name:Ljava/lang/String;

    .line 190
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->client_updated_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->client_updated_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 191
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->note:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->note:Ljava/lang/String;

    .line 192
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->close_event:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    iput-object v1, v0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->close_event:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    .line 193
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->total_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->total_money:Lcom/squareup/protos/common/Money;

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->tender_id_deprecated:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->tender_id_deprecated:Ljava/lang/String;

    .line 195
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->closed_at_deprecated:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->closed_at_deprecated:Lcom/squareup/protos/client/ISO8601Date;

    .line 196
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->transient_:Lcom/squareup/protos/client/tickets/Transient;

    iput-object v1, v0, Lcom/squareup/protos/client/tickets/Ticket$Builder;->transient_:Lcom/squareup/protos/client/tickets/Transient;

    .line 197
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/Ticket;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/Ticket;->newBuilder()Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 243
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 244
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", ticket_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 245
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;

    if-eqz v1, :cond_1

    const-string v1, ", vector_clock="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 246
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v1, :cond_2

    const-string v1, ", cart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 247
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->name:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->client_updated_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_4

    const-string v1, ", client_updated_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->client_updated_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 249
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->note:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", note="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->note:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->close_event:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    if-eqz v1, :cond_6

    const-string v1, ", close_event="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->close_event:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 251
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->total_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_7

    const-string v1, ", total_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->total_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 252
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->tender_id_deprecated:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", tender_id_deprecated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->tender_id_deprecated:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->closed_at_deprecated:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_9

    const-string v1, ", closed_at_deprecated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->closed_at_deprecated:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 254
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->transient_:Lcom/squareup/protos/client/tickets/Transient;

    if-eqz v1, :cond_a

    const-string v1, ", transient="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket;->transient_:Lcom/squareup/protos/client/tickets/Transient;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_a
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Ticket{"

    .line 255
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
