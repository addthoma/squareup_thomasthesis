.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LifecycleEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$ProtoAdapter_LifecycleEvent;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_EVENT_TYPE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;

.field private static final serialVersionUID:J


# instance fields
.field public final created_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final creator_details:Lcom/squareup/protos/client/CreatorDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.CreatorDetails#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final event_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$LifecycleEvent$EventType#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final ownership_transfer_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 6115
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$ProtoAdapter_LifecycleEvent;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$ProtoAdapter_LifecycleEvent;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 6119
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;->UNKNOWN:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->DEFAULT_EVENT_TYPE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;)V
    .locals 7

    .line 6162
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;Lokio/ByteString;)V
    .locals 1

    .line 6168
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 6169
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 6170
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;

    .line 6171
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 6172
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 6173
    iput-object p5, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->ownership_transfer_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 6191
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 6192
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;

    .line 6193
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 6194
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;

    .line 6195
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 6196
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 6197
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->ownership_transfer_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->ownership_transfer_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;

    .line 6198
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 6203
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 6205
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 6206
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6207
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6208
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6209
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/CreatorDetails;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6210
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->ownership_transfer_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 6211
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;
    .locals 2

    .line 6178
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;-><init>()V

    .line 6179
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 6180
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;

    .line 6181
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 6182
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 6183
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->ownership_transfer_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;->ownership_transfer_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;

    .line 6184
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 6114
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 6218
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 6219
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", event_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6220
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;

    if-eqz v1, :cond_1

    const-string v1, ", event_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6221
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_2

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6222
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_3

    const-string v1, ", creator_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6223
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->ownership_transfer_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;

    if-eqz v1, :cond_4

    const-string v1, ", ownership_transfer_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->ownership_transfer_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "LifecycleEvent{"

    .line 6224
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
