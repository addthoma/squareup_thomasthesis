.class public final Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CancelBillRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CancelBillRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CancelBillRequest;",
        "Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill_id_pair:Lcom/squareup/protos/client/IdPair;

.field public cancel_bill_type:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

.field public canceled_at:Lcom/squareup/protos/client/ISO8601Date;

.field public is_amending:Ljava/lang/Boolean;

.field public merchant_token:Ljava/lang/String;

.field public reason_deprecated:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 179
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;
    .locals 0

    .line 183
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/CancelBillRequest;
    .locals 9

    .line 234
    new-instance v8, Lcom/squareup/protos/client/bills/CancelBillRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->reason_deprecated:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->merchant_token:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->cancel_bill_type:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->is_amending:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/bills/CancelBillRequest;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;Ljava/lang/String;Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 166
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->build()Lcom/squareup/protos/client/bills/CancelBillRequest;

    move-result-object v0

    return-object v0
.end method

.method public cancel_bill_type(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;
    .locals 0

    .line 218
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->cancel_bill_type:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    return-object p0
.end method

.method public canceled_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 192
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public is_amending(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->is_amending:Ljava/lang/Boolean;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public reason_deprecated(Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;)Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 202
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->reason_deprecated:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    return-object p0
.end method
