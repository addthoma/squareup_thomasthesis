.class public final Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReturnItemization"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$ProtoAdapter_ReturnItemization;,
        Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;",
        "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final itemization:Lcom/squareup/protos/client/bills/Itemization;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final source_itemization_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8165
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$ProtoAdapter_ReturnItemization;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$ProtoAdapter_ReturnItemization;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Itemization;)V
    .locals 1

    .line 8195
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Itemization;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Itemization;Lokio/ByteString;)V
    .locals 1

    .line 8200
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 8201
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->source_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 8202
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 8217
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 8218
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    .line 8219
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->source_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->source_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 8220
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    .line 8221
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 8226
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 8228
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 8229
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->source_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8230
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 8231
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;
    .locals 2

    .line 8207
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;-><init>()V

    .line 8208
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->source_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->source_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 8209
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    .line 8210
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 8164
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->newBuilder()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 8238
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 8239
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->source_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", source_itemization_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->source_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8240
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    if-eqz v1, :cond_1

    const-string v1, ", itemization="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ReturnItemization{"

    .line 8241
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
