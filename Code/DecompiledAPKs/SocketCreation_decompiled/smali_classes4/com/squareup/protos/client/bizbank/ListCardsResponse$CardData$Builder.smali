.class public final Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListCardsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

.field public card_token:Ljava/lang/String;

.field public name_on_card:Ljava/lang/String;

.field public pan_last_four:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 241
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;
    .locals 8

    .line 271
    new-instance v7, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->card_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    iget-object v3, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iget-object v4, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->pan_last_four:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->name_on_card:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 230
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->build()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v0

    return-object v0
.end method

.method public card_brand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;
    .locals 0

    .line 255
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0
.end method

.method public card_state(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;)Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;
    .locals 0

    .line 250
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    return-object p0
.end method

.method public card_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;
    .locals 0

    .line 245
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->card_token:Ljava/lang/String;

    return-object p0
.end method

.method public name_on_card(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;
    .locals 0

    .line 265
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->name_on_card:Ljava/lang/String;

    return-object p0
.end method

.method public pan_last_four(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;
    .locals 0

    .line 260
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->pan_last_four:Ljava/lang/String;

    return-object p0
.end method
