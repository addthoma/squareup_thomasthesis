.class public final Lcom/squareup/protos/client/tickets/v2/TicketInfo$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TicketInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tickets/v2/TicketInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/tickets/v2/TicketInfo;",
        "Lcom/squareup/protos/client/tickets/v2/TicketInfo$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public ticket_id_pair:Lcom/squareup/protos/client/IdPair;

.field public vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 98
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/tickets/v2/TicketInfo;
    .locals 4

    .line 119
    new-instance v0, Lcom/squareup/protos/client/tickets/v2/TicketInfo;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/TicketInfo$Builder;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/tickets/v2/TicketInfo$Builder;->vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/tickets/v2/TicketInfo;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/tickets/VectorClock;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/v2/TicketInfo$Builder;->build()Lcom/squareup/protos/client/tickets/v2/TicketInfo;

    move-result-object v0

    return-object v0
.end method

.method public ticket_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/tickets/v2/TicketInfo$Builder;
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/v2/TicketInfo$Builder;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public vector_clock(Lcom/squareup/protos/client/tickets/VectorClock;)Lcom/squareup/protos/client/tickets/v2/TicketInfo$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/v2/TicketInfo$Builder;->vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;

    return-object p0
.end method
