.class final Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$ProtoAdapter_GetMergeAllStatusRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetMergeAllStatusRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetMergeAllStatusRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 104
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 121
    new-instance v0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$Builder;-><init>()V

    .line 122
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 123
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 127
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 125
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$Builder;->job_id(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$Builder;

    goto :goto_0

    .line 131
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 132
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 102
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$ProtoAdapter_GetMergeAllStatusRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 115
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;->job_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 116
    invoke-virtual {p2}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 102
    check-cast p2, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$ProtoAdapter_GetMergeAllStatusRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;)I
    .locals 3

    .line 109
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;->job_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 110
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 102
    check-cast p1, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$ProtoAdapter_GetMergeAllStatusRequest;->encodedSize(Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;)Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;
    .locals 0

    .line 137
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;->newBuilder()Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$Builder;

    move-result-object p1

    .line 138
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 139
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 102
    check-cast p1, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest$ProtoAdapter_GetMergeAllStatusRequest;->redact(Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;)Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;

    move-result-object p1

    return-object p1
.end method
