.class public final Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RecordMissedLoyaltyOpportunityRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;",
        "Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public creator_details:Lcom/squareup/protos/client/CreatorDetails;

.field public event_time:Lcom/squareup/protos/client/ISO8601Date;

.field public reason:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 273
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;
    .locals 5

    .line 302
    new-instance v0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;->reason:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;->event_time:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;-><init>(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/CreatorDetails;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 266
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;->build()Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    move-result-object v0

    return-object v0
.end method

.method public creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;
    .locals 0

    .line 296
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    return-object p0
.end method

.method public event_time(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;
    .locals 0

    .line 288
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;->event_time:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public reason(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;
    .locals 0

    .line 280
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;->reason:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    return-object p0
.end method
