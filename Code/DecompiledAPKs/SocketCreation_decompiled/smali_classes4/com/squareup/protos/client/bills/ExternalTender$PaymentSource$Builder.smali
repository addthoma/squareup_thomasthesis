.class public final Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ExternalTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;",
        "Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public application_id:Ljava/lang/String;

.field public application_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public application_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource$Builder;
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource$Builder;->application_id:Ljava/lang/String;

    return-object p0
.end method

.method public application_name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource$Builder;
    .locals 0

    .line 206
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource$Builder;->application_name:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;
    .locals 4

    .line 212
    new-instance v0, Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource$Builder;->application_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource$Builder;->application_name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 178
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource$Builder;->build()Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;

    move-result-object v0

    return-object v0
.end method
