.class public final Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;
.super Lcom/squareup/wire/Message;
.source "AddedTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddedTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoyaltyStatus"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$ProtoAdapter_LoyaltyStatus;,
        Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;,
        Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;",
        "Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_IS_ENROLLED:Ljava/lang/Boolean;

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

.field private static final serialVersionUID:J


# instance fields
.field public final is_enrolled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final punch_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyStatus#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.AddedTender$LoyaltyStatus$LoyaltyProgramType#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 962
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$ProtoAdapter_LoyaltyStatus;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$ProtoAdapter_LoyaltyStatus;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 966
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->DEFAULT_IS_ENROLLED:Ljava/lang/Boolean;

    .line 968
    sget-object v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;->UNKNOWN_DO_NOT_USE:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    sput-object v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->DEFAULT_TYPE:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/loyalty/LoyaltyStatus;Ljava/lang/Boolean;Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;)V
    .locals 1

    .line 994
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;-><init>(Lcom/squareup/protos/client/loyalty/LoyaltyStatus;Ljava/lang/Boolean;Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/loyalty/LoyaltyStatus;Ljava/lang/Boolean;Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;Lokio/ByteString;)V
    .locals 1

    .line 999
    sget-object v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1000
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->punch_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    .line 1001
    iput-object p2, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->is_enrolled:Ljava/lang/Boolean;

    .line 1002
    iput-object p3, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->type:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1018
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1019
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;

    .line 1020
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->punch_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->punch_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    .line 1021
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->is_enrolled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->is_enrolled:Ljava/lang/Boolean;

    .line 1022
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->type:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->type:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    .line 1023
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1028
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 1030
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1031
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->punch_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1032
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->is_enrolled:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1033
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->type:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 1034
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;
    .locals 2

    .line 1007
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;-><init>()V

    .line 1008
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->punch_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;->punch_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    .line 1009
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->is_enrolled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;->is_enrolled:Ljava/lang/Boolean;

    .line 1010
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->type:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;->type:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    .line 1011
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 961
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->newBuilder()Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1041
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1042
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->punch_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    if-eqz v1, :cond_0

    const-string v1, ", punch_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->punch_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1043
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->is_enrolled:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", is_enrolled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->is_enrolled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1044
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->type:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    if-eqz v1, :cond_2

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->type:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "LoyaltyStatus{"

    .line 1045
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
