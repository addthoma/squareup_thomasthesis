.class public final Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StartTimecardResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/StartTimecardResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/StartTimecardResponse;",
        "Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public employee_job_infos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;",
            ">;"
        }
    .end annotation
.end field

.field public timecard:Lcom/squareup/protos/client/timecards/Timecard;

.field public timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

.field public valid:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 124
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 125
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->employee_job_infos:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/timecards/StartTimecardResponse;
    .locals 7

    .line 151
    new-instance v6, Lcom/squareup/protos/client/timecards/StartTimecardResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object v3, p0, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->valid:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->employee_job_infos:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/timecards/StartTimecardResponse;-><init>(Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/TimecardBreak;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 115
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->build()Lcom/squareup/protos/client/timecards/StartTimecardResponse;

    move-result-object v0

    return-object v0
.end method

.method public employee_job_infos(Ljava/util/List;)Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;",
            ">;)",
            "Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;"
        }
    .end annotation

    .line 144
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->employee_job_infos:Ljava/util/List;

    return-object p0
.end method

.method public timecard(Lcom/squareup/protos/client/timecards/Timecard;)Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    return-object p0
.end method

.method public timecard_break(Lcom/squareup/protos/client/timecards/TimecardBreak;)Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    return-object p0
.end method

.method public valid(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->valid:Ljava/lang/Boolean;

    return-object p0
.end method
