.class public final Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ModifierOptionLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ModifierOptionLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem;",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

.field public feature_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

.field public hide_from_customer:Ljava/lang/Boolean;

.field public modifier_option_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

.field public read_only_display_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

.field public write_only_backing_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 169
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amounts(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;
    .locals 0

    .line 181
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem;
    .locals 9

    .line 216
    new-instance v8, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->modifier_option_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->hide_from_customer:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->feature_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;Ljava/lang/Boolean;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 156
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    move-result-object v0

    return-object v0
.end method

.method public feature_details(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->feature_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    return-object p0
.end method

.method public hide_from_customer(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->hide_from_customer:Ljava/lang/Boolean;

    return-object p0
.end method

.method public modifier_option_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;
    .locals 0

    .line 176
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->modifier_option_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public read_only_display_details(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    return-object p0
.end method

.method public write_only_backing_details(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;
    .locals 0

    .line 197
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    return-object p0
.end method
