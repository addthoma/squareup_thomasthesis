.class public final Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LoyaltyStatusWithId.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;",
        "Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

.field public status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 96
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;
    .locals 4

    .line 111
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;->status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;-><init>(Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;Lcom/squareup/protos/client/loyalty/LoyaltyStatus;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 91
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;

    move-result-object v0

    return-object v0
.end method

.method public customer_identifier(Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;)Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/loyalty/LoyaltyStatus;)Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;->status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    return-object p0
.end method
