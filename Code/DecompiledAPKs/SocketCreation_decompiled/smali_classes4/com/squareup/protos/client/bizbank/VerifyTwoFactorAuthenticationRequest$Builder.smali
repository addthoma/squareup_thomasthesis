.class public final Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "VerifyTwoFactorAuthenticationRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationRequest;",
        "Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public authentication_token:Ljava/lang/String;

.field public request_type:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 103
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public authentication_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationRequest$Builder;
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationRequest$Builder;->authentication_token:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationRequest;
    .locals 4

    .line 125
    new-instance v0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationRequest$Builder;->authentication_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationRequest$Builder;->request_type:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 98
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationRequest;

    move-result-object v0

    return-object v0
.end method

.method public request_type(Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;)Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationRequest$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationRequest$Builder;->request_type:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    return-object p0
.end method
