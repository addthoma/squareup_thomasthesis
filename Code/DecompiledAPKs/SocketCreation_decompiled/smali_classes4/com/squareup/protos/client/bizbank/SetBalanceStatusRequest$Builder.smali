.class public final Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SetBalanceStatusRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;",
        "Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bizbank_enabled:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 80
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bizbank_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest$Builder;
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest$Builder;->bizbank_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;
    .locals 3

    .line 90
    new-instance v0, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest$Builder;->bizbank_enabled:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;

    move-result-object v0

    return-object v0
.end method
