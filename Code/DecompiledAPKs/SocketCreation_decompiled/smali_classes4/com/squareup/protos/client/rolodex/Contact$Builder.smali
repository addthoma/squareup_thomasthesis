.class public final Lcom/squareup/protos/client/rolodex/Contact$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Contact.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/Contact;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "Lcom/squareup/protos/client/rolodex/Contact$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public app_fields:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AppField;",
            ">;"
        }
    .end annotation
.end field

.field public application_fields:Lcom/squareup/protos/client/rolodex/ApplicationFields;

.field public attachments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Attachment;",
            ">;"
        }
    .end annotation
.end field

.field public attributes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
            ">;"
        }
    .end annotation
.end field

.field public buyer_summary:Lcom/squareup/protos/client/rolodex/BuyerSummary;

.field public contact_token:Ljava/lang/String;

.field public created_at_ms:Ljava/lang/Long;

.field public creation_source:Ljava/lang/String;

.field public display_name:Ljava/lang/String;

.field public email_subscriptions:Lcom/squareup/protos/client/rolodex/EmailSubscriptions;

.field public email_token:Ljava/lang/String;

.field public frequent_items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/FrequentItem;",
            ">;"
        }
    .end annotation
.end field

.field public group:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation
.end field

.field public instruments_on_file:Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

.field public merchant_provided_id:Ljava/lang/String;

.field public note:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Note;",
            ">;"
        }
    .end annotation
.end field

.field public phone_token:Ljava/lang/String;

.field public profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

.field public schema_version:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 399
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 400
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->group:Ljava/util/List;

    .line 401
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->note:Ljava/util/List;

    .line 402
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->attributes:Ljava/util/List;

    .line 403
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->attachments:Ljava/util/List;

    .line 404
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->app_fields:Ljava/util/List;

    .line 405
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->frequent_items:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public app_fields(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AppField;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Contact$Builder;"
        }
    .end annotation

    .line 544
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 545
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->app_fields:Ljava/util/List;

    return-object p0
.end method

.method public application_fields(Lcom/squareup/protos/client/rolodex/ApplicationFields;)Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 0

    .line 570
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->application_fields:Lcom/squareup/protos/client/rolodex/ApplicationFields;

    return-object p0
.end method

.method public attachments(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Attachment;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Contact$Builder;"
        }
    .end annotation

    .line 527
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 528
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->attachments:Ljava/util/List;

    return-object p0
.end method

.method public attributes(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Contact$Builder;"
        }
    .end annotation

    .line 499
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 500
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->attributes:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 2

    .line 576
    new-instance v0, Lcom/squareup/protos/client/rolodex/Contact;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/client/rolodex/Contact;-><init>(Lcom/squareup/protos/client/rolodex/Contact$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 360
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    return-object v0
.end method

.method public buyer_summary(Lcom/squareup/protos/client/rolodex/BuyerSummary;)Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 0

    .line 449
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->buyer_summary:Lcom/squareup/protos/client/rolodex/BuyerSummary;

    return-object p0
.end method

.method public contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 0

    .line 414
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->contact_token:Ljava/lang/String;

    return-object p0
.end method

.method public created_at_ms(Ljava/lang/Long;)Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 0

    .line 482
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->created_at_ms:Ljava/lang/Long;

    return-object p0
.end method

.method public creation_source(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 0

    .line 536
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->creation_source:Ljava/lang/String;

    return-object p0
.end method

.method public display_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 0

    .line 423
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->display_name:Ljava/lang/String;

    return-object p0
.end method

.method public email_subscriptions(Lcom/squareup/protos/client/rolodex/EmailSubscriptions;)Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 0

    .line 518
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->email_subscriptions:Lcom/squareup/protos/client/rolodex/EmailSubscriptions;

    return-object p0
.end method

.method public email_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 0

    .line 466
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->email_token:Ljava/lang/String;

    return-object p0
.end method

.method public frequent_items(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/FrequentItem;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Contact$Builder;"
        }
    .end annotation

    .line 553
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 554
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->frequent_items:Ljava/util/List;

    return-object p0
.end method

.method public group(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Contact$Builder;"
        }
    .end annotation

    .line 443
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 444
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->group:Ljava/util/List;

    return-object p0
.end method

.method public instruments_on_file(Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;)Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 0

    .line 458
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->instruments_on_file:Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    return-object p0
.end method

.method public merchant_provided_id(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 0

    .line 562
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->merchant_provided_id:Ljava/lang/String;

    return-object p0
.end method

.method public note(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Note;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/Contact$Builder;"
        }
    .end annotation

    .line 490
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 491
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->note:Ljava/util/List;

    return-object p0
.end method

.method public phone_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 0

    .line 474
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->phone_token:Ljava/lang/String;

    return-object p0
.end method

.method public profile(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 0

    .line 433
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    return-object p0
.end method

.method public schema_version(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 0

    .line 508
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->schema_version:Ljava/lang/String;

    return-object p0
.end method
