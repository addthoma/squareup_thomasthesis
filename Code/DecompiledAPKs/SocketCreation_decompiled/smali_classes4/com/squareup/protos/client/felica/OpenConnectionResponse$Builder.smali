.class public final Lcom/squareup/protos/client/felica/OpenConnectionResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "OpenConnectionResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/felica/OpenConnectionResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/felica/OpenConnectionResponse;",
        "Lcom/squareup/protos/client/felica/OpenConnectionResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public connection_id:Ljava/lang/String;

.field public error:Lcom/squareup/protos/client/felica/Error;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 94
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/felica/OpenConnectionResponse;
    .locals 4

    .line 109
    new-instance v0, Lcom/squareup/protos/client/felica/OpenConnectionResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/felica/OpenConnectionResponse$Builder;->connection_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/felica/OpenConnectionResponse$Builder;->error:Lcom/squareup/protos/client/felica/Error;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/felica/OpenConnectionResponse;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/felica/Error;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/client/felica/OpenConnectionResponse$Builder;->build()Lcom/squareup/protos/client/felica/OpenConnectionResponse;

    move-result-object v0

    return-object v0
.end method

.method public connection_id(Ljava/lang/String;)Lcom/squareup/protos/client/felica/OpenConnectionResponse$Builder;
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/protos/client/felica/OpenConnectionResponse$Builder;->connection_id:Ljava/lang/String;

    return-object p0
.end method

.method public error(Lcom/squareup/protos/client/felica/Error;)Lcom/squareup/protos/client/felica/OpenConnectionResponse$Builder;
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/client/felica/OpenConnectionResponse$Builder;->error:Lcom/squareup/protos/client/felica/Error;

    return-object p0
.end method
