.class public final Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;",
        "Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public kds:Lcom/squareup/protos/client/kds/KdsStation;

.field public shared_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3821
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;
    .locals 4

    .line 3847
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails$Builder;->kds:Lcom/squareup/protos/client/kds/KdsStation;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails$Builder;->shared_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;-><init>(Lcom/squareup/protos/client/kds/KdsStation;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 3816
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails;

    move-result-object v0

    return-object v0
.end method

.method public kds(Lcom/squareup/protos/client/kds/KdsStation;)Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails$Builder;
    .locals 0

    .line 3828
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails$Builder;->kds:Lcom/squareup/protos/client/kds/KdsStation;

    return-object p0
.end method

.method public shared_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails$Builder;
    .locals 0

    .line 3841
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$KdsEventDetails$Builder;->shared_id:Ljava/lang/String;

    return-object p0
.end method
