.class public final Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FulfillmentClientDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/FulfillmentClientDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/orders/FulfillmentClientDetails;",
        "Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public available_actions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/Action;",
            ">;"
        }
    .end annotation
.end field

.field public courier:Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;

.field public fulfillment_window_ends_at:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 119
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 120
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->available_actions:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public available_actions(Ljava/util/List;)Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/Action;",
            ">;)",
            "Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;"
        }
    .end annotation

    .line 127
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 128
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->available_actions:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/orders/FulfillmentClientDetails;
    .locals 5

    .line 148
    new-instance v0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->available_actions:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->courier:Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;

    iget-object v3, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->fulfillment_window_ends_at:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;-><init>(Ljava/util/List;Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 112
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->build()Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    move-result-object v0

    return-object v0
.end method

.method public courier(Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;)Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->courier:Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;

    return-object p0
.end method

.method public fulfillment_window_ends_at(Ljava/lang/String;)Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->fulfillment_window_ends_at:Ljava/lang/String;

    return-object p0
.end method
