.class public final enum Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;
.super Ljava/lang/Enum;
.source "SetPasscodeResponse.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/SetPasscodeResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Result"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result$ProtoAdapter_Result;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum FAILED:Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

.field public static final enum FAILED_WEAK_PASSCODE:Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

.field public static final enum INVALID_CARD_TOKEN:Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

.field public static final enum SUCCESS:Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 96
    new-instance v0, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "SUCCESS"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;->SUCCESS:Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    .line 98
    new-instance v0, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    const/4 v3, 0x2

    const-string v4, "FAILED"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;->FAILED:Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    .line 100
    new-instance v0, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    const/4 v4, 0x3

    const-string v5, "INVALID_CARD_TOKEN"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;->INVALID_CARD_TOKEN:Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    .line 102
    new-instance v0, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    const/4 v5, 0x4

    const-string v6, "FAILED_WEAK_PASSCODE"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;->FAILED_WEAK_PASSCODE:Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    new-array v0, v5, [Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    .line 95
    sget-object v5, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;->SUCCESS:Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;->FAILED:Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;->INVALID_CARD_TOKEN:Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;->FAILED_WEAK_PASSCODE:Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;->$VALUES:[Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    .line 104
    new-instance v0, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result$ProtoAdapter_Result;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result$ProtoAdapter_Result;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 108
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 109
    iput p3, p0, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 120
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;->FAILED_WEAK_PASSCODE:Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    return-object p0

    .line 119
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;->INVALID_CARD_TOKEN:Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    return-object p0

    .line 118
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;->FAILED:Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    return-object p0

    .line 117
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;->SUCCESS:Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;
    .locals 1

    .line 95
    const-class v0, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;
    .locals 1

    .line 95
    sget-object v0, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;->$VALUES:[Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 127
    iget v0, p0, Lcom/squareup/protos/client/bizbank/SetPasscodeResponse$Result;->value:I

    return v0
.end method
