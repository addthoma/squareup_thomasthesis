.class public final Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SimpleTimeWorkedCalculationRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;",
        "Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public date_range:Lcom/squareup/protos/client/timecards/DateRange;

.field public date_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

.field public include_open_timecards:Ljava/lang/Boolean;

.field public next_cursor:Ljava/lang/String;

.field public request_filter:Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 192
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;
    .locals 8

    .line 237
    new-instance v7, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->date_range:Lcom/squareup/protos/client/timecards/DateRange;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->date_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    iget-object v3, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->request_filter:Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;

    iget-object v4, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->next_cursor:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->include_open_timecards:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;-><init>(Lcom/squareup/protos/client/timecards/DateRange;Lcom/squareup/protos/common/time/DateTimeInterval;Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 181
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->build()Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;

    move-result-object v0

    return-object v0
.end method

.method public date_range(Lcom/squareup/protos/client/timecards/DateRange;)Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->date_range:Lcom/squareup/protos/client/timecards/DateRange;

    return-object p0
.end method

.method public date_time_interval(Lcom/squareup/protos/common/time/DateTimeInterval;)Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;
    .locals 0

    .line 207
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->date_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    return-object p0
.end method

.method public include_open_timecards(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;
    .locals 0

    .line 231
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->include_open_timecards:Ljava/lang/Boolean;

    return-object p0
.end method

.method public next_cursor(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->next_cursor:Ljava/lang/String;

    return-object p0
.end method

.method public request_filter(Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;)Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->request_filter:Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;

    return-object p0
.end method
