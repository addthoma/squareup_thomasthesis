.class final Lcom/squareup/protos/client/bankaccount/BankAccount$ProtoAdapter_BankAccount;
.super Lcom/squareup/wire/ProtoAdapter;
.source "BankAccount.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bankaccount/BankAccount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_BankAccount"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bankaccount/BankAccount;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 378
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bankaccount/BankAccount;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bankaccount/BankAccount;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 415
    new-instance v0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;-><init>()V

    .line 416
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 417
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 445
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 443
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->bank_account_token(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;

    goto :goto_0

    .line 442
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->supports_instant_deposit(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;

    goto :goto_0

    .line 441
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->secondary_institution_number(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;

    goto :goto_0

    .line 440
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->primary_institution_number(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;

    goto :goto_0

    .line 439
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->verified_by(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;

    goto :goto_0

    .line 438
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->routing_number(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;

    goto :goto_0

    .line 437
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->account_number_suffix(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;

    goto :goto_0

    .line 436
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->account_name(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;

    goto :goto_0

    .line 430
    :pswitch_8
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bankaccount/BankAccountType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bankaccount/BankAccountType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->account_type(Lcom/squareup/protos/client/bankaccount/BankAccountType;)Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 432
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 427
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->bank_name(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;

    goto/16 :goto_0

    .line 421
    :pswitch_a
    :try_start_1
    sget-object v4, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->verification_state(Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v4

    .line 423
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 449
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 450
    invoke-virtual {v0}, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->build()Lcom/squareup/protos/client/bankaccount/BankAccount;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 376
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bankaccount/BankAccount$ProtoAdapter_BankAccount;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bankaccount/BankAccount;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bankaccount/BankAccount;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 399
    sget-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/BankAccount;->verification_state:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 400
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 401
    sget-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_type:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 402
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_name:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 403
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_number_suffix:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 404
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/BankAccount;->routing_number:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 405
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/BankAccount;->verified_by:Lcom/squareup/protos/common/time/DateTime;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 406
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/BankAccount;->primary_institution_number:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 407
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/BankAccount;->secondary_institution_number:Ljava/lang/String;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 408
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/BankAccount;->supports_instant_deposit:Ljava/lang/Boolean;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 409
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_account_token:Ljava/lang/String;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 410
    invoke-virtual {p2}, Lcom/squareup/protos/client/bankaccount/BankAccount;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 376
    check-cast p2, Lcom/squareup/protos/client/bankaccount/BankAccount;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bankaccount/BankAccount$ProtoAdapter_BankAccount;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bankaccount/BankAccount;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bankaccount/BankAccount;)I
    .locals 4

    .line 383
    sget-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->verification_state:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_name:Ljava/lang/String;

    const/4 v3, 0x2

    .line 384
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccountType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_type:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    const/4 v3, 0x3

    .line 385
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_name:Ljava/lang/String;

    const/4 v3, 0x4

    .line 386
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_number_suffix:Ljava/lang/String;

    const/4 v3, 0x5

    .line 387
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->routing_number:Ljava/lang/String;

    const/4 v3, 0x6

    .line 388
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->verified_by:Lcom/squareup/protos/common/time/DateTime;

    const/4 v3, 0x7

    .line 389
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->primary_institution_number:Ljava/lang/String;

    const/16 v3, 0x8

    .line 390
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->secondary_institution_number:Ljava/lang/String;

    const/16 v3, 0x9

    .line 391
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->supports_instant_deposit:Ljava/lang/Boolean;

    const/16 v3, 0xa

    .line 392
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_account_token:Ljava/lang/String;

    const/16 v3, 0xb

    .line 393
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 394
    invoke-virtual {p1}, Lcom/squareup/protos/client/bankaccount/BankAccount;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 376
    check-cast p1, Lcom/squareup/protos/client/bankaccount/BankAccount;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bankaccount/BankAccount$ProtoAdapter_BankAccount;->encodedSize(Lcom/squareup/protos/client/bankaccount/BankAccount;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bankaccount/BankAccount;)Lcom/squareup/protos/client/bankaccount/BankAccount;
    .locals 2

    .line 455
    invoke-virtual {p1}, Lcom/squareup/protos/client/bankaccount/BankAccount;->newBuilder()Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 456
    iput-object v0, p1, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->account_name:Ljava/lang/String;

    .line 457
    iget-object v0, p1, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->verified_by:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->verified_by:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p1, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->verified_by:Lcom/squareup/protos/common/time/DateTime;

    .line 458
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 459
    invoke-virtual {p1}, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->build()Lcom/squareup/protos/client/bankaccount/BankAccount;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 376
    check-cast p1, Lcom/squareup/protos/client/bankaccount/BankAccount;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bankaccount/BankAccount$ProtoAdapter_BankAccount;->redact(Lcom/squareup/protos/client/bankaccount/BankAccount;)Lcom/squareup/protos/client/bankaccount/BankAccount;

    move-result-object p1

    return-object p1
.end method
