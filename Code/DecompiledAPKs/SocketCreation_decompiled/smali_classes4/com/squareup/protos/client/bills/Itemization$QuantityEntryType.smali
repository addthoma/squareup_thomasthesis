.class public final enum Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;
.super Ljava/lang/Enum;
.source "Itemization.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "QuantityEntryType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType$ProtoAdapter_QuantityEntryType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DO_NOT_USE:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

.field public static final enum MANUALLY_ENTERED:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

.field public static final enum MANUAL_WITH_CONNECTED_SCALE:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

.field public static final enum READ_FROM_SCALE:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 436
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->DO_NOT_USE:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    .line 441
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    const/4 v2, 0x1

    const-string v3, "MANUALLY_ENTERED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->MANUALLY_ENTERED:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    .line 446
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    const/4 v3, 0x2

    const-string v4, "READ_FROM_SCALE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->READ_FROM_SCALE:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    .line 451
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    const/4 v4, 0x3

    const-string v5, "MANUAL_WITH_CONNECTED_SCALE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->MANUAL_WITH_CONNECTED_SCALE:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    .line 435
    sget-object v5, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->DO_NOT_USE:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->MANUALLY_ENTERED:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->READ_FROM_SCALE:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->MANUAL_WITH_CONNECTED_SCALE:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->$VALUES:[Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    .line 453
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType$ProtoAdapter_QuantityEntryType;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType$ProtoAdapter_QuantityEntryType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 457
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 458
    iput p3, p0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 469
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->MANUAL_WITH_CONNECTED_SCALE:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    return-object p0

    .line 468
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->READ_FROM_SCALE:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    return-object p0

    .line 467
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->MANUALLY_ENTERED:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    return-object p0

    .line 466
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->DO_NOT_USE:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;
    .locals 1

    .line 435
    const-class v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;
    .locals 1

    .line 435
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->$VALUES:[Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 476
    iget v0, p0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->value:I

    return v0
.end method
