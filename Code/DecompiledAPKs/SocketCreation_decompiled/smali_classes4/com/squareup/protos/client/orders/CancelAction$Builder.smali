.class public final Lcom/squareup/protos/client/orders/CancelAction$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CancelAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/CancelAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/orders/CancelAction;",
        "Lcom/squareup/protos/client/orders/CancelAction$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cancel_reason:Ljava/lang/String;

.field public canceled_at:Ljava/lang/String;

.field public canceled_line_items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/LineItemQuantity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 126
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 127
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/orders/CancelAction$Builder;->canceled_line_items:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/orders/CancelAction;
    .locals 5

    .line 159
    new-instance v0, Lcom/squareup/protos/client/orders/CancelAction;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/CancelAction$Builder;->cancel_reason:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/orders/CancelAction$Builder;->canceled_at:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/orders/CancelAction$Builder;->canceled_line_items:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/orders/CancelAction;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 119
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/CancelAction$Builder;->build()Lcom/squareup/protos/client/orders/CancelAction;

    move-result-object v0

    return-object v0
.end method

.method public cancel_reason(Ljava/lang/String;)Lcom/squareup/protos/client/orders/CancelAction$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/client/orders/CancelAction$Builder;->cancel_reason:Ljava/lang/String;

    return-object p0
.end method

.method public canceled_at(Ljava/lang/String;)Lcom/squareup/protos/client/orders/CancelAction$Builder;
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/client/orders/CancelAction$Builder;->canceled_at:Ljava/lang/String;

    return-object p0
.end method

.method public canceled_line_items(Ljava/util/List;)Lcom/squareup/protos/client/orders/CancelAction$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/LineItemQuantity;",
            ">;)",
            "Lcom/squareup/protos/client/orders/CancelAction$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 152
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 153
    iput-object p1, p0, Lcom/squareup/protos/client/orders/CancelAction$Builder;->canceled_line_items:Ljava/util/List;

    return-object p0
.end method
