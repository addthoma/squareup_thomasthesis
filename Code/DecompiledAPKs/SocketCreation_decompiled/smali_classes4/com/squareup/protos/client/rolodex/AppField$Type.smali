.class public final enum Lcom/squareup/protos/client/rolodex/AppField$Type;
.super Ljava/lang/Enum;
.source "AppField.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/AppField;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/AppField$Type$ProtoAdapter_Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/rolodex/AppField$Type;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/rolodex/AppField$Type;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/AppField$Type;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DATETIME:Lcom/squareup/protos/client/rolodex/AppField$Type;

.field public static final enum EMAIL_ADDRESS:Lcom/squareup/protos/client/rolodex/AppField$Type;

.field public static final enum INTEGER:Lcom/squareup/protos/client/rolodex/AppField$Type;

.field public static final enum PHONE_NUMBER:Lcom/squareup/protos/client/rolodex/AppField$Type;

.field public static final enum TEXT:Lcom/squareup/protos/client/rolodex/AppField$Type;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/rolodex/AppField$Type;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 204
    new-instance v0, Lcom/squareup/protos/client/rolodex/AppField$Type;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/rolodex/AppField$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/AppField$Type;->UNKNOWN:Lcom/squareup/protos/client/rolodex/AppField$Type;

    .line 206
    new-instance v0, Lcom/squareup/protos/client/rolodex/AppField$Type;

    const/4 v2, 0x1

    const-string v3, "TEXT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/rolodex/AppField$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/AppField$Type;->TEXT:Lcom/squareup/protos/client/rolodex/AppField$Type;

    .line 208
    new-instance v0, Lcom/squareup/protos/client/rolodex/AppField$Type;

    const/4 v3, 0x2

    const-string v4, "EMAIL_ADDRESS"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/rolodex/AppField$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/AppField$Type;->EMAIL_ADDRESS:Lcom/squareup/protos/client/rolodex/AppField$Type;

    .line 210
    new-instance v0, Lcom/squareup/protos/client/rolodex/AppField$Type;

    const/4 v4, 0x3

    const-string v5, "PHONE_NUMBER"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/rolodex/AppField$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/AppField$Type;->PHONE_NUMBER:Lcom/squareup/protos/client/rolodex/AppField$Type;

    .line 212
    new-instance v0, Lcom/squareup/protos/client/rolodex/AppField$Type;

    const/4 v5, 0x4

    const-string v6, "DATETIME"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/rolodex/AppField$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/AppField$Type;->DATETIME:Lcom/squareup/protos/client/rolodex/AppField$Type;

    .line 214
    new-instance v0, Lcom/squareup/protos/client/rolodex/AppField$Type;

    const/4 v6, 0x5

    const-string v7, "INTEGER"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/rolodex/AppField$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/AppField$Type;->INTEGER:Lcom/squareup/protos/client/rolodex/AppField$Type;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/client/rolodex/AppField$Type;

    .line 203
    sget-object v7, Lcom/squareup/protos/client/rolodex/AppField$Type;->UNKNOWN:Lcom/squareup/protos/client/rolodex/AppField$Type;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/AppField$Type;->TEXT:Lcom/squareup/protos/client/rolodex/AppField$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/rolodex/AppField$Type;->EMAIL_ADDRESS:Lcom/squareup/protos/client/rolodex/AppField$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/rolodex/AppField$Type;->PHONE_NUMBER:Lcom/squareup/protos/client/rolodex/AppField$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/rolodex/AppField$Type;->DATETIME:Lcom/squareup/protos/client/rolodex/AppField$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/rolodex/AppField$Type;->INTEGER:Lcom/squareup/protos/client/rolodex/AppField$Type;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/client/rolodex/AppField$Type;->$VALUES:[Lcom/squareup/protos/client/rolodex/AppField$Type;

    .line 216
    new-instance v0, Lcom/squareup/protos/client/rolodex/AppField$Type$ProtoAdapter_Type;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/AppField$Type$ProtoAdapter_Type;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/AppField$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 220
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 221
    iput p3, p0, Lcom/squareup/protos/client/rolodex/AppField$Type;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/rolodex/AppField$Type;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 234
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/rolodex/AppField$Type;->INTEGER:Lcom/squareup/protos/client/rolodex/AppField$Type;

    return-object p0

    .line 233
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/rolodex/AppField$Type;->DATETIME:Lcom/squareup/protos/client/rolodex/AppField$Type;

    return-object p0

    .line 232
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/rolodex/AppField$Type;->PHONE_NUMBER:Lcom/squareup/protos/client/rolodex/AppField$Type;

    return-object p0

    .line 231
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/rolodex/AppField$Type;->EMAIL_ADDRESS:Lcom/squareup/protos/client/rolodex/AppField$Type;

    return-object p0

    .line 230
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/rolodex/AppField$Type;->TEXT:Lcom/squareup/protos/client/rolodex/AppField$Type;

    return-object p0

    .line 229
    :cond_5
    sget-object p0, Lcom/squareup/protos/client/rolodex/AppField$Type;->UNKNOWN:Lcom/squareup/protos/client/rolodex/AppField$Type;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AppField$Type;
    .locals 1

    .line 203
    const-class v0, Lcom/squareup/protos/client/rolodex/AppField$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/rolodex/AppField$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/rolodex/AppField$Type;
    .locals 1

    .line 203
    sget-object v0, Lcom/squareup/protos/client/rolodex/AppField$Type;->$VALUES:[Lcom/squareup/protos/client/rolodex/AppField$Type;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/rolodex/AppField$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/rolodex/AppField$Type;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 241
    iget v0, p0, Lcom/squareup/protos/client/rolodex/AppField$Type;->value:I

    return v0
.end method
