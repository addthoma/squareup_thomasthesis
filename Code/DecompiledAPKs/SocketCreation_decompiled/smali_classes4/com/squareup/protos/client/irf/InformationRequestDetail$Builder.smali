.class public final Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InformationRequestDetail.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/irf/InformationRequestDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/irf/InformationRequestDetail;",
        "Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public form_type:Ljava/lang/String;

.field public page:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Page;",
            ">;"
        }
    .end annotation
.end field

.field public status:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

.field public token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 127
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 128
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;->page:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/irf/InformationRequestDetail;
    .locals 7

    .line 154
    new-instance v6, Lcom/squareup/protos/client/irf/InformationRequestDetail;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;->token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;->form_type:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;->status:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    iget-object v4, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;->page:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/irf/InformationRequestDetail;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;Ljava/util/List;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;->build()Lcom/squareup/protos/client/irf/InformationRequestDetail;

    move-result-object v0

    return-object v0
.end method

.method public form_type(Ljava/lang/String;)Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;->form_type:Ljava/lang/String;

    return-object p0
.end method

.method public page(Ljava/util/List;)Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Page;",
            ">;)",
            "Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;"
        }
    .end annotation

    .line 147
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 148
    iput-object p1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;->page:Ljava/util/List;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;)Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;->status:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;->token:Ljava/lang/String;

    return-object p0
.end method
