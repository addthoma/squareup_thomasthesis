.class public final Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SettlementReportLiteRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;",
        "Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public batch_size:Ljava/lang/Integer;

.field public batch_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 193
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public batch_size(Ljava/lang/Integer;)Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest$Builder;
    .locals 0

    .line 200
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest$Builder;->batch_size:Ljava/lang/Integer;

    return-object p0
.end method

.method public batch_token(Ljava/lang/String;)Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest$Builder;
    .locals 0

    .line 209
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest$Builder;->batch_token:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;
    .locals 4

    .line 215
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest$Builder;->batch_size:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest$Builder;->batch_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;-><init>(Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 188
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest$Builder;->build()Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;

    move-result-object v0

    return-object v0
.end method
