.class public final Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ModifierOptionLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public modifier_option_money:Lcom/squareup/protos/common/Money;

.field public modifier_option_times_quantity_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 312
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;
    .locals 4

    .line 348
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;->modifier_option_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;->modifier_option_times_quantity_money:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 307
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    move-result-object v0

    return-object v0
.end method

.method public modifier_option_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;
    .locals 0

    .line 323
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;->modifier_option_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public modifier_option_times_quantity_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;
    .locals 0

    .line 342
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;->modifier_option_times_quantity_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
