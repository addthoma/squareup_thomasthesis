.class public final Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;
.super Lcom/squareup/wire/Message;
.source "SetTransactionStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$ProtoAdapter_SetTransactionStatusResponse;,
        Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;",
        "Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CONNECTION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_ERROR:Lcom/squareup/protos/client/felica/Error;

.field public static final DEFAULT_STATUS:Lcom/squareup/protos/client/felica/Status;

.field public static final DEFAULT_TRANSACTION_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final connection_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final error:Lcom/squareup/protos/client/felica/Error;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.felica.Error#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/felica/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.felica.Status#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final transaction_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$ProtoAdapter_SetTransactionStatusResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$ProtoAdapter_SetTransactionStatusResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lcom/squareup/protos/client/felica/Error;->DO_NOT_USE:Lcom/squareup/protos/client/felica/Error;

    sput-object v0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->DEFAULT_ERROR:Lcom/squareup/protos/client/felica/Error;

    .line 30
    sget-object v0, Lcom/squareup/protos/client/felica/Status;->DEFAULT_DO_NOT_USE:Lcom/squareup/protos/client/felica/Status;

    sput-object v0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->DEFAULT_STATUS:Lcom/squareup/protos/client/felica/Status;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/felica/Error;Lcom/squareup/protos/client/felica/Status;)V
    .locals 6

    .line 58
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/felica/Error;Lcom/squareup/protos/client/felica/Status;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/felica/Error;Lcom/squareup/protos/client/felica/Status;Lokio/ByteString;)V
    .locals 1

    .line 63
    sget-object v0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 64
    iput-object p1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->connection_id:Ljava/lang/String;

    .line 65
    iput-object p2, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->transaction_id:Ljava/lang/String;

    .line 66
    iput-object p3, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->error:Lcom/squareup/protos/client/felica/Error;

    .line 67
    iput-object p4, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->status:Lcom/squareup/protos/client/felica/Status;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 84
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 85
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->connection_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->connection_id:Ljava/lang/String;

    .line 87
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->transaction_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->transaction_id:Ljava/lang/String;

    .line 88
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->error:Lcom/squareup/protos/client/felica/Error;

    iget-object v3, p1, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->error:Lcom/squareup/protos/client/felica/Error;

    .line 89
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->status:Lcom/squareup/protos/client/felica/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->status:Lcom/squareup/protos/client/felica/Status;

    .line 90
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 95
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->connection_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->transaction_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->error:Lcom/squareup/protos/client/felica/Error;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/felica/Error;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->status:Lcom/squareup/protos/client/felica/Status;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/felica/Status;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 102
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;
    .locals 2

    .line 72
    new-instance v0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;-><init>()V

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->connection_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;->connection_id:Ljava/lang/String;

    .line 74
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->transaction_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;->transaction_id:Ljava/lang/String;

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->error:Lcom/squareup/protos/client/felica/Error;

    iput-object v1, v0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;->error:Lcom/squareup/protos/client/felica/Error;

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->status:Lcom/squareup/protos/client/felica/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;->status:Lcom/squareup/protos/client/felica/Status;

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->newBuilder()Lcom/squareup/protos/client/felica/SetTransactionStatusResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->connection_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", connection_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->connection_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->transaction_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", transaction_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->transaction_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->error:Lcom/squareup/protos/client/felica/Error;

    if-eqz v1, :cond_2

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->error:Lcom/squareup/protos/client/felica/Error;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 113
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->status:Lcom/squareup/protos/client/felica/Status;

    if-eqz v1, :cond_3

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusResponse;->status:Lcom/squareup/protos/client/felica/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SetTransactionStatusResponse{"

    .line 114
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
