.class public final enum Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;
.super Ljava/lang/Enum;
.source "CardTender.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardTender$Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FelicaBrand"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand$ProtoAdapter_FelicaBrand;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DEFAULT_FELICA_BRAND_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

.field public static final enum FELICA_ID:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

.field public static final enum FELICA_QP:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

.field public static final enum FELICA_TRANSPORTATION:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 679
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    const/4 v1, 0x0

    const-string v2, "DEFAULT_FELICA_BRAND_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->DEFAULT_FELICA_BRAND_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    .line 681
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    const/4 v2, 0x1

    const-string v3, "FELICA_ID"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->FELICA_ID:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    .line 683
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    const/4 v3, 0x2

    const-string v4, "FELICA_TRANSPORTATION"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->FELICA_TRANSPORTATION:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    .line 685
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    const/4 v4, 0x3

    const-string v5, "FELICA_QP"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->FELICA_QP:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    .line 678
    sget-object v5, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->DEFAULT_FELICA_BRAND_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->FELICA_ID:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->FELICA_TRANSPORTATION:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->FELICA_QP:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->$VALUES:[Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    .line 687
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand$ProtoAdapter_FelicaBrand;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand$ProtoAdapter_FelicaBrand;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 691
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 692
    iput p3, p0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 703
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->FELICA_QP:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    return-object p0

    .line 702
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->FELICA_TRANSPORTATION:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    return-object p0

    .line 701
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->FELICA_ID:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    return-object p0

    .line 700
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->DEFAULT_FELICA_BRAND_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;
    .locals 1

    .line 678
    const-class v0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;
    .locals 1

    .line 678
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->$VALUES:[Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 710
    iget v0, p0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->value:I

    return v0
.end method
