.class public final Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LoyaltyCustomerIdentifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;",
        "Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public email_token:Ljava/lang/String;

.field public phone_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 98
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;
    .locals 4

    .line 115
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier$Builder;->email_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier$Builder;->phone_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    move-result-object v0

    return-object v0
.end method

.method public email_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier$Builder;
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier$Builder;->email_token:Ljava/lang/String;

    const/4 p1, 0x0

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier$Builder;->phone_token:Ljava/lang/String;

    return-object p0
.end method

.method public phone_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier$Builder;->phone_token:Ljava/lang/String;

    const/4 p1, 0x0

    .line 109
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier$Builder;->email_token:Ljava/lang/String;

    return-object p0
.end method
