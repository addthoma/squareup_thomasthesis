.class final Lcom/squareup/protos/client/bills/CompleteBillRequest$ProtoAdapter_CompleteBillRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CompleteBillRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CompleteBillRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CompleteBillRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/CompleteBillRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 409
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/CompleteBillRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CompleteBillRequest;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 444
    new-instance v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;-><init>()V

    .line 445
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 446
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 466
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 464
    :pswitch_0
    iget-object v3, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->removed_amending_tender_ids:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 463
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->is_amendable(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    goto :goto_0

    .line 462
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->amendment_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    goto :goto_0

    .line 461
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/client/bills/SquareProductAttributes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/SquareProductAttributes;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->square_product_attributes(Lcom/squareup/protos/client/bills/SquareProductAttributes;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    goto :goto_0

    .line 455
    :pswitch_4
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->complete_bill_type(Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 457
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 452
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/client/bills/Bill$Dates;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Bill$Dates;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->dates(Lcom/squareup/protos/client/bills/Bill$Dates;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    goto :goto_0

    .line 451
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    goto :goto_0

    .line 450
    :pswitch_7
    iget-object v3, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->tender:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/CompleteTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 449
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/client/Merchant;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/Merchant;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->merchant(Lcom/squareup/protos/client/Merchant;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    goto/16 :goto_0

    .line 448
    :pswitch_9
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    goto/16 :goto_0

    .line 470
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 471
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->build()Lcom/squareup/protos/client/bills/CompleteBillRequest;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 407
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CompleteBillRequest$ProtoAdapter_CompleteBillRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CompleteBillRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CompleteBillRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 429
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CompleteBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 430
    sget-object v0, Lcom/squareup/protos/client/Merchant;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CompleteBillRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 431
    sget-object v0, Lcom/squareup/protos/client/bills/CompleteTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CompleteBillRequest;->tender:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 432
    sget-object v0, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CompleteBillRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 433
    sget-object v0, Lcom/squareup/protos/client/bills/Bill$Dates;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CompleteBillRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 434
    sget-object v0, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CompleteBillRequest;->complete_bill_type:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 435
    sget-object v0, Lcom/squareup/protos/client/bills/SquareProductAttributes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CompleteBillRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 436
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CompleteBillRequest;->amendment_token:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 437
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CompleteBillRequest;->is_amendable:Ljava/lang/Boolean;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 438
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CompleteBillRequest;->removed_amending_tender_ids:Ljava/util/List;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 439
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/CompleteBillRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 407
    check-cast p2, Lcom/squareup/protos/client/bills/CompleteBillRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/CompleteBillRequest$ProtoAdapter_CompleteBillRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CompleteBillRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/CompleteBillRequest;)I
    .locals 4

    .line 414
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/Merchant;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    const/4 v3, 0x2

    .line 415
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CompleteTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 416
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->tender:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    const/4 v3, 0x4

    .line 417
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Bill$Dates;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    const/4 v3, 0x5

    .line 418
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->complete_bill_type:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    const/4 v3, 0x6

    .line 419
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/SquareProductAttributes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    const/4 v3, 0x7

    .line 420
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->amendment_token:Ljava/lang/String;

    const/16 v3, 0x8

    .line 421
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->is_amendable:Ljava/lang/Boolean;

    const/16 v3, 0x9

    .line 422
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 423
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;->removed_amending_tender_ids:Ljava/util/List;

    const/16 v3, 0xa

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 424
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CompleteBillRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 407
    check-cast p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CompleteBillRequest$ProtoAdapter_CompleteBillRequest;->encodedSize(Lcom/squareup/protos/client/bills/CompleteBillRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/CompleteBillRequest;)Lcom/squareup/protos/client/bills/CompleteBillRequest;
    .locals 2

    .line 476
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CompleteBillRequest;->newBuilder()Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    move-result-object p1

    .line 477
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 478
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->merchant:Lcom/squareup/protos/client/Merchant;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/Merchant;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->merchant:Lcom/squareup/protos/client/Merchant;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/Merchant;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->merchant:Lcom/squareup/protos/client/Merchant;

    .line 479
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->tender:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/CompleteTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 480
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 481
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/bills/Bill$Dates;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Bill$Dates;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    .line 482
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/client/bills/SquareProductAttributes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/SquareProductAttributes;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    .line 483
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->removed_amending_tender_ids:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 484
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 485
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->build()Lcom/squareup/protos/client/bills/CompleteBillRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 407
    check-cast p1, Lcom/squareup/protos/client/bills/CompleteBillRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CompleteBillRequest$ProtoAdapter_CompleteBillRequest;->redact(Lcom/squareup/protos/client/bills/CompleteBillRequest;)Lcom/squareup/protos/client/bills/CompleteBillRequest;

    move-result-object p1

    return-object p1
.end method
