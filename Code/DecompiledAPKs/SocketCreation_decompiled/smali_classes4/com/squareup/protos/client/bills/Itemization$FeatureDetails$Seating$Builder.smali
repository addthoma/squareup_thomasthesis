.class public final Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public destination:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4357
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;
    .locals 3

    .line 4367
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;->destination:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;-><init>(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 4354
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    move-result-object v0

    return-object v0
.end method

.method public destination(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;
    .locals 0

    .line 4361
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;->destination:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    return-object p0
.end method
