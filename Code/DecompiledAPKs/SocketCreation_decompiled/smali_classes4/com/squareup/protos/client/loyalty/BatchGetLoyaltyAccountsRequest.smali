.class public final Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;
.super Lcom/squareup/wire/Message;
.source "BatchGetLoyaltyAccountsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$ProtoAdapter_BatchGetLoyaltyAccountsRequest;,
        Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;",
        "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final loyalty_account_tokens:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyOptions#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$ProtoAdapter_BatchGetLoyaltyAccountsRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$ProtoAdapter_BatchGetLoyaltyAccountsRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/client/loyalty/LoyaltyOptions;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/protos/client/loyalty/LoyaltyOptions;",
            ")V"
        }
    .end annotation

    .line 51
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;-><init>(Ljava/util/List;Lcom/squareup/protos/client/loyalty/LoyaltyOptions;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/client/loyalty/LoyaltyOptions;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/protos/client/loyalty/LoyaltyOptions;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 56
    sget-object v0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p3, "loyalty_account_tokens"

    .line 57
    invoke-static {p3, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;->loyalty_account_tokens:Ljava/util/List;

    .line 58
    iput-object p2, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 73
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 74
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;->loyalty_account_tokens:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;->loyalty_account_tokens:Ljava/util/List;

    .line 76
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    .line 77
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 82
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;->loyalty_account_tokens:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 87
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;
    .locals 2

    .line 63
    new-instance v0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;-><init>()V

    .line 64
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;->loyalty_account_tokens:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;->loyalty_account_tokens:Ljava/util/List;

    .line 65
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    .line 66
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;->newBuilder()Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;->loyalty_account_tokens:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", loyalty_account_tokens="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;->loyalty_account_tokens:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 96
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    if-eqz v1, :cond_1

    const-string v1, ", options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "BatchGetLoyaltyAccountsRequest{"

    .line 97
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
