.class final Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse$ProtoAdapter_BatchResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SettlementReportLiteResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_BatchResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 194
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 211
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse$Builder;-><init>()V

    .line 212
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 213
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 217
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 215
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse$Builder;->next_batch_token(Ljava/lang/String;)Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse$Builder;

    goto :goto_0

    .line 221
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 222
    invoke-virtual {v0}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse$Builder;->build()Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 192
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse$ProtoAdapter_BatchResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 205
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;->next_batch_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 206
    invoke-virtual {p2}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 192
    check-cast p2, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse$ProtoAdapter_BatchResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;)I
    .locals 3

    .line 199
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;->next_batch_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 200
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 192
    check-cast p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse$ProtoAdapter_BatchResponse;->encodedSize(Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;)Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;
    .locals 0

    .line 227
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;->newBuilder()Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse$Builder;

    move-result-object p1

    .line 228
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 229
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse$Builder;->build()Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 192
    check-cast p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse$ProtoAdapter_BatchResponse;->redact(Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;)Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    move-result-object p1

    return-object p1
.end method
