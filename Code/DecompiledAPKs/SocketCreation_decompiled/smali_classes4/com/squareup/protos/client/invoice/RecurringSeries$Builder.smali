.class public final Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RecurringSeries.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/RecurringSeries;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/RecurringSeries;",
        "Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public recurrence_schedule:Lcom/squareup/protos/client/invoice/RecurringSchedule;

.field public template:Lcom/squareup/protos/client/invoice/Invoice;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 94
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/RecurringSeries;
    .locals 4

    .line 109
    new-instance v0, Lcom/squareup/protos/client/invoice/RecurringSeries;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;->recurrence_schedule:Lcom/squareup/protos/client/invoice/RecurringSchedule;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;->template:Lcom/squareup/protos/client/invoice/Invoice;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/invoice/RecurringSeries;-><init>(Lcom/squareup/protos/client/invoice/RecurringSchedule;Lcom/squareup/protos/client/invoice/Invoice;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;->build()Lcom/squareup/protos/client/invoice/RecurringSeries;

    move-result-object v0

    return-object v0
.end method

.method public recurrence_schedule(Lcom/squareup/protos/client/invoice/RecurringSchedule;)Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;->recurrence_schedule:Lcom/squareup/protos/client/invoice/RecurringSchedule;

    return-object p0
.end method

.method public template(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/RecurringSeries$Builder;->template:Lcom/squareup/protos/client/invoice/Invoice;

    return-object p0
.end method
