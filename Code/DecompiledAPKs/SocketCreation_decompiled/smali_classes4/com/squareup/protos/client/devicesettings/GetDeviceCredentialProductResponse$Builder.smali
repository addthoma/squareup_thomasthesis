.class public final Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetDeviceCredentialProductResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;",
        "Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public product:Lcom/squareup/protos/client/Device$Product;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 80
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;
    .locals 3

    .line 90
    new-instance v0, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse$Builder;->product:Lcom/squareup/protos/client/Device$Product;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;-><init>(Lcom/squareup/protos/client/Device$Product;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse$Builder;->build()Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse;

    move-result-object v0

    return-object v0
.end method

.method public product(Lcom/squareup/protos/client/Device$Product;)Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse$Builder;
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/squareup/protos/client/devicesettings/GetDeviceCredentialProductResponse$Builder;->product:Lcom/squareup/protos/client/Device$Product;

    return-object p0
.end method
