.class public final Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;
.super Lcom/squareup/wire/Message;
.source "AddedTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddedTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReceiptDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ProtoAdapter_ReceiptDetails;,
        Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;,
        Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;,
        Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;,
        Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;",
        "Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_RECEIPT_BEHAVIOR:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

.field private static final serialVersionUID:J


# instance fields
.field public final email:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.AddedTender$ReceiptDetails$Email#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final phone:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.AddedTender$ReceiptDetails$Phone#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final receipt_behavior:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.AddedTender$ReceiptDetails$ReceiptBehavior#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 275
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ProtoAdapter_ReceiptDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ProtoAdapter_ReceiptDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 279
    sget-object v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;->UNKNOWN:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    sput-object v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->DEFAULT_RECEIPT_BEHAVIOR:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;)V
    .locals 1

    .line 300
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;-><init>(Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;Lokio/ByteString;)V
    .locals 1

    .line 305
    sget-object v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 306
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->receipt_behavior:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    .line 307
    iput-object p2, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->email:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

    .line 308
    iput-object p3, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->phone:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 324
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 325
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    .line 326
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->receipt_behavior:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->receipt_behavior:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    .line 327
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->email:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->email:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

    .line 328
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->phone:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->phone:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

    .line 329
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 334
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 336
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 337
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->receipt_behavior:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 338
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->email:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 339
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->phone:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 340
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;
    .locals 2

    .line 313
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;-><init>()V

    .line 314
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->receipt_behavior:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->receipt_behavior:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    .line 315
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->email:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->email:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

    .line 316
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->phone:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->phone:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

    .line 317
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 274
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->newBuilder()Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 347
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 348
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->receipt_behavior:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    if-eqz v1, :cond_0

    const-string v1, ", receipt_behavior="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->receipt_behavior:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 349
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->email:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

    if-eqz v1, :cond_1

    const-string v1, ", email="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->email:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 350
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->phone:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

    if-eqz v1, :cond_2

    const-string v1, ", phone="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->phone:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ReceiptDetails{"

    .line 351
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
