.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public created_at:Lcom/squareup/protos/client/ISO8601Date;

.field public creator_details:Lcom/squareup/protos/client/CreatorDetails;

.field public event_id_pair:Lcom/squareup/protos/client/IdPair;

.field public event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;

.field public ownership_transfer_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6238
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;
    .locals 8

    .line 6278
    new-instance v7, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;->ownership_transfer_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 6227
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;

    move-result-object v0

    return-object v0
.end method

.method public created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;
    .locals 0

    .line 6255
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;
    .locals 0

    .line 6263
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    return-object p0
.end method

.method public event_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;
    .locals 0

    .line 6242
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;->event_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public event_type(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;
    .locals 0

    .line 6247
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$EventType;

    return-object p0
.end method

.method public ownership_transfer_details(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;
    .locals 0

    .line 6272
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$Builder;->ownership_transfer_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;

    return-object p0
.end method
