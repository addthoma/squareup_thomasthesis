.class public final Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;
.super Lcom/squareup/wire/Message;
.source "RedeemMobileAuthorizationCodeResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse$ProtoAdapter_RedeemMobileAuthorizationCodeResponse;,
        Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;",
        "Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SESSION_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final error:Lcom/squareup/protos/xp/v1/Error;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.xp.v1.Error#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final session_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse$ProtoAdapter_RedeemMobileAuthorizationCodeResponse;

    invoke-direct {v0}, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse$ProtoAdapter_RedeemMobileAuthorizationCodeResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/xp/v1/Error;)V
    .locals 1

    .line 56
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;-><init>(Ljava/lang/String;Lcom/squareup/protos/xp/v1/Error;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/xp/v1/Error;Lokio/ByteString;)V
    .locals 1

    .line 61
    sget-object v0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 62
    iput-object p1, p0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;->session_token:Ljava/lang/String;

    .line 63
    iput-object p2, p0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;->error:Lcom/squareup/protos/xp/v1/Error;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 78
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 79
    :cond_1
    check-cast p1, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;

    .line 80
    invoke-virtual {p0}, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;->session_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;->session_token:Ljava/lang/String;

    .line 81
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;->error:Lcom/squareup/protos/xp/v1/Error;

    iget-object p1, p1, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;->error:Lcom/squareup/protos/xp/v1/Error;

    .line 82
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 87
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;->session_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;->error:Lcom/squareup/protos/xp/v1/Error;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/xp/v1/Error;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 92
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse$Builder;
    .locals 2

    .line 68
    new-instance v0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse$Builder;-><init>()V

    .line 69
    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;->session_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse$Builder;->session_token:Ljava/lang/String;

    .line 70
    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;->error:Lcom/squareup/protos/xp/v1/Error;

    iput-object v1, v0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse$Builder;->error:Lcom/squareup/protos/xp/v1/Error;

    .line 71
    invoke-virtual {p0}, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;->newBuilder()Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;->session_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", session_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;->session_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;->error:Lcom/squareup/protos/xp/v1/Error;

    if-eqz v1, :cond_1

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;->error:Lcom/squareup/protos/xp/v1/Error;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "RedeemMobileAuthorizationCodeResponse{"

    .line 102
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
