.class public final Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateMobileAuthorizationCodeResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;",
        "Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public authorization_code:Ljava/lang/String;

.field public error:Lcom/squareup/protos/xp/v1/Error;

.field public expires_at:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 134
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public authorization_code(Ljava/lang/String;)Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$Builder;->authorization_code:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;
    .locals 5

    .line 172
    new-instance v0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;

    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$Builder;->authorization_code:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$Builder;->expires_at:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$Builder;->error:Lcom/squareup/protos/xp/v1/Error;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/xp/v1/Error;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 127
    invoke-virtual {p0}, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$Builder;->build()Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;

    move-result-object v0

    return-object v0
.end method

.method public error(Lcom/squareup/protos/xp/v1/Error;)Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$Builder;->error:Lcom/squareup/protos/xp/v1/Error;

    return-object p0
.end method

.method public expires_at(Ljava/lang/String;)Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$Builder;
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$Builder;->expires_at:Ljava/lang/String;

    return-object p0
.end method
