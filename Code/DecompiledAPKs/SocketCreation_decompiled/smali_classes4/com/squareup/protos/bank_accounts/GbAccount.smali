.class public final Lcom/squareup/protos/bank_accounts/GbAccount;
.super Lcom/squareup/wire/Message;
.source "GbAccount.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/bank_accounts/GbAccount$ProtoAdapter_GbAccount;,
        Lcom/squareup/protos/bank_accounts/GbAccount$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/bank_accounts/GbAccount;",
        "Lcom/squareup/protos/bank_accounts/GbAccount$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/bank_accounts/GbAccount;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SORT_CODE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final account_details:Lcom/squareup/protos/bank_accounts/GbAccountDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bank_accounts.GbAccountDetails#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final sort_code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/bank_accounts/GbAccount$ProtoAdapter_GbAccount;

    invoke-direct {v0}, Lcom/squareup/protos/bank_accounts/GbAccount$ProtoAdapter_GbAccount;-><init>()V

    sput-object v0, Lcom/squareup/protos/bank_accounts/GbAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/bank_accounts/GbAccountDetails;)V
    .locals 1

    .line 45
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/bank_accounts/GbAccount;-><init>(Ljava/lang/String;Lcom/squareup/protos/bank_accounts/GbAccountDetails;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/bank_accounts/GbAccountDetails;Lokio/ByteString;)V
    .locals 1

    .line 49
    sget-object v0, Lcom/squareup/protos/bank_accounts/GbAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 50
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/GbAccount;->sort_code:Ljava/lang/String;

    .line 51
    iput-object p2, p0, Lcom/squareup/protos/bank_accounts/GbAccount;->account_details:Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 66
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/bank_accounts/GbAccount;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 67
    :cond_1
    check-cast p1, Lcom/squareup/protos/bank_accounts/GbAccount;

    .line 68
    invoke-virtual {p0}, Lcom/squareup/protos/bank_accounts/GbAccount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/bank_accounts/GbAccount;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/GbAccount;->sort_code:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/bank_accounts/GbAccount;->sort_code:Ljava/lang/String;

    .line 69
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/GbAccount;->account_details:Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    iget-object p1, p1, Lcom/squareup/protos/bank_accounts/GbAccount;->account_details:Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    .line 70
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 75
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/bank_accounts/GbAccount;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 78
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/GbAccount;->sort_code:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 79
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/GbAccount;->account_details:Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/bank_accounts/GbAccountDetails;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 80
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/bank_accounts/GbAccount$Builder;
    .locals 2

    .line 56
    new-instance v0, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;-><init>()V

    .line 57
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/GbAccount;->sort_code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;->sort_code:Ljava/lang/String;

    .line 58
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/GbAccount;->account_details:Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    iput-object v1, v0, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;->account_details:Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    .line 59
    invoke-virtual {p0}, Lcom/squareup/protos/bank_accounts/GbAccount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/bank_accounts/GbAccount;->newBuilder()Lcom/squareup/protos/bank_accounts/GbAccount$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/GbAccount;->sort_code:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", sort_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/GbAccount;->sort_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/GbAccount;->account_details:Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    if-eqz v1, :cond_1

    const-string v1, ", account_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/GbAccount;->account_details:Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GbAccount{"

    .line 90
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
