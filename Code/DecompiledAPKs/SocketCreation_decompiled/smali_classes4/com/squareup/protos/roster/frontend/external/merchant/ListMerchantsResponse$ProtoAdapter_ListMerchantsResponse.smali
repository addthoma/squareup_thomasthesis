.class final Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$ProtoAdapter_ListMerchantsResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ListMerchantsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ListMerchantsResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 164
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 185
    new-instance v0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;-><init>()V

    .line 186
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 187
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 193
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 191
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;->cursor(Ljava/lang/Integer;)Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;

    goto :goto_0

    .line 190
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;->merchant:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 189
    :cond_2
    iget-object v3, v0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;->errors:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/connect/v2/resources/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 197
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 198
    invoke-virtual {v0}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;->build()Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 162
    invoke-virtual {p0, p1}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$ProtoAdapter_ListMerchantsResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 177
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;->errors:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 178
    sget-object v0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;->merchant:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 179
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;->cursor:Ljava/lang/Integer;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 180
    invoke-virtual {p2}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 162
    check-cast p2, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$ProtoAdapter_ListMerchantsResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;)I
    .locals 4

    .line 169
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;->errors:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 170
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;->merchant:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;->cursor:Ljava/lang/Integer;

    const/4 v3, 0x3

    .line 171
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    invoke-virtual {p1}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 162
    check-cast p1, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$ProtoAdapter_ListMerchantsResponse;->encodedSize(Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;)Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;
    .locals 2

    .line 203
    invoke-virtual {p1}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;->newBuilder()Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;

    move-result-object p1

    .line 204
    iget-object v0, p1, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;->errors:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 205
    iget-object v0, p1, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;->merchant:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 206
    invoke-virtual {p1}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 207
    invoke-virtual {p1}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;->build()Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 162
    check-cast p1, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$ProtoAdapter_ListMerchantsResponse;->redact(Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;)Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;

    move-result-object p1

    return-object p1
.end method
