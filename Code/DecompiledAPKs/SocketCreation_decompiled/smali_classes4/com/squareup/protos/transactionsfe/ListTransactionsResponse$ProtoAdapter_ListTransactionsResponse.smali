.class final Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$ProtoAdapter_ListTransactionsResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ListTransactionsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ListTransactionsResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1102
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1121
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;-><init>()V

    .line 1122
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1123
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 1128
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1126
    :cond_0
    sget-object v3, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;->next_page_request(Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;

    goto :goto_0

    .line 1125
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;->transaction:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1132
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1133
    invoke-virtual {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;->build()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1100
    invoke-virtual {p0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$ProtoAdapter_ListTransactionsResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1114
    sget-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->transaction:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1115
    sget-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->next_page_request:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1116
    invoke-virtual {p2}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1100
    check-cast p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$ProtoAdapter_ListTransactionsResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;)I
    .locals 4

    .line 1107
    sget-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->transaction:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->next_page_request:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    const/4 v3, 0x2

    .line 1108
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1109
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1100
    check-cast p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$ProtoAdapter_ListTransactionsResponse;->encodedSize(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;
    .locals 2

    .line 1138
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->newBuilder()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;

    move-result-object p1

    .line 1139
    iget-object v0, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;->transaction:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1140
    iget-object v0, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;->next_page_request:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;->next_page_request:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    iput-object v0, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;->next_page_request:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    .line 1141
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1142
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;->build()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1100
    check-cast p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$ProtoAdapter_ListTransactionsResponse;->redact(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;

    move-result-object p1

    return-object p1
.end method
