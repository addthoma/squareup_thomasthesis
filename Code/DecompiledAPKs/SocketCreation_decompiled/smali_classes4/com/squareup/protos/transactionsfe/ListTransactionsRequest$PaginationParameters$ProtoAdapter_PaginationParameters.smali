.class final Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$ProtoAdapter_PaginationParameters;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ListTransactionsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_PaginationParameters"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 378
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 397
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;-><init>()V

    .line 398
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 399
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 404
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 402
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;->cursor(Ljava/lang/String;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;

    goto :goto_0

    .line 401
    :cond_1
    sget-object v3, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTimeInterval;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;->effective_date_range(Lcom/squareup/protos/common/time/DateTimeInterval;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;

    goto :goto_0

    .line 408
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 409
    invoke-virtual {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;->build()Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 376
    invoke-virtual {p0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$ProtoAdapter_PaginationParameters;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 390
    sget-object v0, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;->effective_date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 391
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;->cursor:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 392
    invoke-virtual {p2}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 376
    check-cast p2, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$ProtoAdapter_PaginationParameters;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;)I
    .locals 4

    .line 383
    sget-object v0, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;->effective_date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;->cursor:Ljava/lang/String;

    const/4 v3, 0x2

    .line 384
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 385
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 376
    check-cast p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$ProtoAdapter_PaginationParameters;->encodedSize(Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;
    .locals 2

    .line 414
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;->newBuilder()Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;

    move-result-object p1

    .line 415
    iget-object v0, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;->effective_date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;->effective_date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTimeInterval;

    iput-object v0, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;->effective_date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 416
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 417
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;->build()Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 376
    check-cast p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$ProtoAdapter_PaginationParameters;->redact(Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    move-result-object p1

    return-object p1
.end method
