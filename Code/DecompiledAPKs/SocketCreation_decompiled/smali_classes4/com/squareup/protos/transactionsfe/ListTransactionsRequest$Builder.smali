.class public final Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListTransactionsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

.field public employee_token:Ljava/lang/String;

.field public merchant_token:Ljava/lang/String;

.field public pagination_parameters:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

.field public query:Lcom/squareup/protos/transactionsfe/Query;

.field public unit_token:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 188
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 189
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->unit_token:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;
    .locals 9

    .line 256
    new-instance v8, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->merchant_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->unit_token:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->employee_token:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    iget-object v5, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->query:Lcom/squareup/protos/transactionsfe/Query;

    iget-object v6, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->pagination_parameters:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTimeInterval;Lcom/squareup/protos/transactionsfe/Query;Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 175
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->build()Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    move-result-object v0

    return-object v0
.end method

.method public date_range(Lcom/squareup/protos/common/time/DateTimeInterval;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;
    .locals 0

    .line 232
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    return-object p0
.end method

.method public employee_token(Ljava/lang/String;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;
    .locals 0

    .line 222
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->employee_token:Ljava/lang/String;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;
    .locals 0

    .line 198
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public pagination_parameters(Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;
    .locals 0

    .line 250
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->pagination_parameters:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    return-object p0
.end method

.method public query(Lcom/squareup/protos/transactionsfe/Query;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;
    .locals 0

    .line 240
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->query:Lcom/squareup/protos/transactionsfe/Query;

    return-object p0
.end method

.method public unit_token(Ljava/util/List;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;"
        }
    .end annotation

    .line 210
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 211
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->unit_token:Ljava/util/List;

    return-object p0
.end method
