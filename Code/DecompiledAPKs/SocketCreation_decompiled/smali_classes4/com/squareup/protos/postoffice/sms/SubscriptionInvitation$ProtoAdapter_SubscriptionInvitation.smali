.class final Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$ProtoAdapter_SubscriptionInvitation;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SubscriptionInvitation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SubscriptionInvitation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 175
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 200
    new-instance v0, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;-><init>()V

    .line 201
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 202
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 210
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 208
    :cond_0
    sget-object v3, Lcom/squareup/protos/postoffice/sms/Subscriber;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/postoffice/sms/Subscriber;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->subscriber(Lcom/squareup/protos/postoffice/sms/Subscriber;)Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;

    goto :goto_0

    .line 207
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->terms(Ljava/lang/String;)Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;

    goto :goto_0

    .line 206
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->subtitle(Ljava/lang/String;)Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;

    goto :goto_0

    .line 205
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->title(Ljava/lang/String;)Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;

    goto :goto_0

    .line 204
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->id(Ljava/lang/String;)Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;

    goto :goto_0

    .line 214
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 215
    invoke-virtual {v0}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->build()Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 173
    invoke-virtual {p0, p1}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$ProtoAdapter_SubscriptionInvitation;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 190
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 191
    sget-object v0, Lcom/squareup/protos/postoffice/sms/Subscriber;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 192
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->title:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 193
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->subtitle:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 194
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->terms:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 195
    invoke-virtual {p2}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 173
    check-cast p2, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$ProtoAdapter_SubscriptionInvitation;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;)I
    .locals 4

    .line 180
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/postoffice/sms/Subscriber;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    const/4 v3, 0x5

    .line 181
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->title:Ljava/lang/String;

    const/4 v3, 0x2

    .line 182
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->subtitle:Ljava/lang/String;

    const/4 v3, 0x3

    .line 183
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->terms:Ljava/lang/String;

    const/4 v3, 0x4

    .line 184
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 185
    invoke-virtual {p1}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 173
    check-cast p1, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$ProtoAdapter_SubscriptionInvitation;->encodedSize(Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;)Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;
    .locals 2

    .line 220
    invoke-virtual {p1}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->newBuilder()Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;

    move-result-object p1

    .line 221
    iget-object v0, p1, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/postoffice/sms/Subscriber;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/postoffice/sms/Subscriber;

    iput-object v0, p1, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    .line 222
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 223
    invoke-virtual {p1}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$Builder;->build()Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 173
    check-cast p1, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation$ProtoAdapter_SubscriptionInvitation;->redact(Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;)Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    move-result-object p1

    return-object p1
.end method
