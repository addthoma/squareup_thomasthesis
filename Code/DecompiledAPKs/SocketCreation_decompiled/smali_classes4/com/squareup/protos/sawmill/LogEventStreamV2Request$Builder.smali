.class public final Lcom/squareup/protos/sawmill/LogEventStreamV2Request$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LogEventStreamV2Request.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/sawmill/LogEventStreamV2Request;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/sawmill/LogEventStreamV2Request;",
        "Lcom/squareup/protos/sawmill/LogEventStreamV2Request$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public events:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/sawmill/EventstreamV2Event;",
            ">;"
        }
    .end annotation
.end field

.field public sync:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 96
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 97
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/sawmill/LogEventStreamV2Request$Builder;->events:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/sawmill/LogEventStreamV2Request;
    .locals 4

    .line 113
    new-instance v0, Lcom/squareup/protos/sawmill/LogEventStreamV2Request;

    iget-object v1, p0, Lcom/squareup/protos/sawmill/LogEventStreamV2Request$Builder;->events:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/sawmill/LogEventStreamV2Request$Builder;->sync:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/sawmill/LogEventStreamV2Request;-><init>(Ljava/util/List;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 91
    invoke-virtual {p0}, Lcom/squareup/protos/sawmill/LogEventStreamV2Request$Builder;->build()Lcom/squareup/protos/sawmill/LogEventStreamV2Request;

    move-result-object v0

    return-object v0
.end method

.method public events(Ljava/util/List;)Lcom/squareup/protos/sawmill/LogEventStreamV2Request$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/sawmill/EventstreamV2Event;",
            ">;)",
            "Lcom/squareup/protos/sawmill/LogEventStreamV2Request$Builder;"
        }
    .end annotation

    .line 101
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 102
    iput-object p1, p0, Lcom/squareup/protos/sawmill/LogEventStreamV2Request$Builder;->events:Ljava/util/List;

    return-object p0
.end method

.method public sync(Ljava/lang/Boolean;)Lcom/squareup/protos/sawmill/LogEventStreamV2Request$Builder;
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/squareup/protos/sawmill/LogEventStreamV2Request$Builder;->sync:Ljava/lang/Boolean;

    return-object p0
.end method
