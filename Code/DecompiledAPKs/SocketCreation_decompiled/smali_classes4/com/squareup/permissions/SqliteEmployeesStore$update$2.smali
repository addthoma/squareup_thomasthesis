.class final Lcom/squareup/permissions/SqliteEmployeesStore$update$2;
.super Ljava/lang/Object;
.source "SqliteEmployeesStore.kt"

# interfaces
.implements Lio/reactivex/functions/Action;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/permissions/SqliteEmployeesStore;->update(Lcom/squareup/permissions/Employee;)Lio/reactivex/Completable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSqliteEmployeesStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SqliteEmployeesStore.kt\ncom/squareup/permissions/SqliteEmployeesStore$update$2\n+ 2 extensions.kt\ncom/squareup/sqlbrite3/ExtensionsKt\n*L\n1#1,195:1\n94#2,10:196\n*E\n*S KotlinDebug\n*F\n+ 1 SqliteEmployeesStore.kt\ncom/squareup/permissions/SqliteEmployeesStore$update$2\n*L\n105#1,10:196\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $employee:Lcom/squareup/permissions/Employee;

.field final synthetic this$0:Lcom/squareup/permissions/SqliteEmployeesStore;


# direct methods
.method constructor <init>(Lcom/squareup/permissions/SqliteEmployeesStore;Lcom/squareup/permissions/Employee;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/permissions/SqliteEmployeesStore$update$2;->this$0:Lcom/squareup/permissions/SqliteEmployeesStore;

    iput-object p2, p0, Lcom/squareup/permissions/SqliteEmployeesStore$update$2;->$employee:Lcom/squareup/permissions/Employee;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .line 105
    iget-object v0, p0, Lcom/squareup/permissions/SqliteEmployeesStore$update$2;->this$0:Lcom/squareup/permissions/SqliteEmployeesStore;

    invoke-static {v0}, Lcom/squareup/permissions/SqliteEmployeesStore;->access$getDb$p(Lcom/squareup/permissions/SqliteEmployeesStore;)Lcom/squareup/sqlbrite3/BriteDatabase;

    move-result-object v0

    .line 199
    invoke-virtual {v0}, Lcom/squareup/sqlbrite3/BriteDatabase;->newTransaction()Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;

    move-result-object v1

    :try_start_0
    const-string v2, "transaction"

    .line 201
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    iget-object v2, p0, Lcom/squareup/permissions/SqliteEmployeesStore$update$2;->this$0:Lcom/squareup/permissions/SqliteEmployeesStore;

    iget-object v3, p0, Lcom/squareup/permissions/SqliteEmployeesStore$update$2;->$employee:Lcom/squareup/permissions/Employee;

    invoke-static {v2, v3, v0}, Lcom/squareup/permissions/SqliteEmployeesStore;->access$insertEmployeeInDatabase(Lcom/squareup/permissions/SqliteEmployeesStore;Lcom/squareup/permissions/Employee;Lcom/squareup/sqlbrite3/BriteDatabase;)V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 202
    invoke-interface {v1}, Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;->markSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    invoke-interface {v1}, Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;->end()V

    return-void

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;->end()V

    throw v0
.end method
