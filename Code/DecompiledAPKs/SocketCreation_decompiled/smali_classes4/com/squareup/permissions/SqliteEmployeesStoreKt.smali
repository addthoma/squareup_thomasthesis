.class public final Lcom/squareup/permissions/SqliteEmployeesStoreKt;
.super Ljava/lang/Object;
.source "SqliteEmployeesStore.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSqliteEmployeesStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SqliteEmployeesStore.kt\ncom/squareup/permissions/SqliteEmployeesStoreKt\n*L\n1#1,195:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u0003H\u0002\u00a8\u0006\u0004"
    }
    d2 = {
        "toEmployees",
        "",
        "Lcom/squareup/permissions/Employee;",
        "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
        "employees_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$toEmployees(Lcom/squareup/sqlbrite3/SqlBrite$Query;)Ljava/util/Set;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/permissions/SqliteEmployeesStoreKt;->toEmployees(Lcom/squareup/sqlbrite3/SqlBrite$Query;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method

.method private static final toEmployees(Lcom/squareup/sqlbrite3/SqlBrite$Query;)Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ")",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Employee;",
            ">;"
        }
    .end annotation

    .line 185
    invoke-virtual {p0}, Lcom/squareup/sqlbrite3/SqlBrite$Query;->run()Landroid/database/Cursor;

    move-result-object p0

    if-eqz p0, :cond_2

    check-cast p0, Ljava/io/Closeable;

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/Throwable;

    :try_start_0
    move-object v1, p0

    check-cast v1, Landroid/database/Cursor;

    .line 186
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v2, Ljava/util/Set;

    .line 187
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 188
    sget-object v3, Lcom/squareup/permissions/Employee;->MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;

    invoke-interface {v3, v1}, Lcom/squareup/sqldelight/prerelease/RowMapper;->map(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v3

    const-string v4, "Employee.MAPPER.map(it)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lcom/squareup/permissions/Employee;

    .line 189
    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "duplicate employee"

    .line 190
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 192
    :cond_1
    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    invoke-static {p0, v0}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    return-object v1

    :catchall_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception v1

    invoke-static {p0, v0}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw v1

    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method
