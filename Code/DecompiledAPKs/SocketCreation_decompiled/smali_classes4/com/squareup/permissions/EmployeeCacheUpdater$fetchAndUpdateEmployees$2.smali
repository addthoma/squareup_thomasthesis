.class final Lcom/squareup/permissions/EmployeeCacheUpdater$fetchAndUpdateEmployees$2;
.super Ljava/lang/Object;
.source "EmployeeCacheUpdater.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/permissions/EmployeeCacheUpdater;->fetchAndUpdateEmployees(Z)Lio/reactivex/Completable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "Ljava/util/Set<",
        "+",
        "Lcom/squareup/permissions/Employee;",
        ">;",
        "Lio/reactivex/CompletableSource;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Completable;",
        "kotlin.jvm.PlatformType",
        "fetched",
        "",
        "Lcom/squareup/permissions/Employee;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/permissions/EmployeeCacheUpdater;


# direct methods
.method constructor <init>(Lcom/squareup/permissions/EmployeeCacheUpdater;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchAndUpdateEmployees$2;->this$0:Lcom/squareup/permissions/EmployeeCacheUpdater;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/util/Set;)Lio/reactivex/Completable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "+",
            "Lcom/squareup/permissions/Employee;",
            ">;)",
            "Lio/reactivex/Completable;"
        }
    .end annotation

    const-string v0, "fetched"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchAndUpdateEmployees$2;->this$0:Lcom/squareup/permissions/EmployeeCacheUpdater;

    invoke-static {v0}, Lcom/squareup/permissions/EmployeeCacheUpdater;->access$getEmployees$p(Lcom/squareup/permissions/EmployeeCacheUpdater;)Lcom/squareup/permissions/Employees;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/permissions/Employees;->updateStore(Ljava/util/Set;)Lio/reactivex/Completable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Ljava/util/Set;

    invoke-virtual {p0, p1}, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchAndUpdateEmployees$2;->apply(Ljava/util/Set;)Lio/reactivex/Completable;

    move-result-object p1

    return-object p1
.end method
