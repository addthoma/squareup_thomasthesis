.class public Lcom/squareup/permissions/TimeTrackingSettings;
.super Ljava/lang/Object;
.source "TimeTrackingSettings.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/permissions/TimeTrackingSettings$State;,
        Lcom/squareup/permissions/TimeTrackingSettings$TimeTrackingSettingsChangedEvent;,
        Lcom/squareup/permissions/TimeTrackingSettings$Event;
    }
.end annotation


# instance fields
.field private final employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

.field private final events:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/permissions/TimeTrackingSettings$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final state:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/permissions/TimeTrackingSettings$State;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/EmployeeManagementSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/permissions/TimeTrackingSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 22
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/permissions/TimeTrackingSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 25
    iput-object p1, p0, Lcom/squareup/permissions/TimeTrackingSettings;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    return-void
.end method

.method private initTimeTrackingSettings()V
    .locals 3

    .line 42
    iget-object v0, p0, Lcom/squareup/permissions/TimeTrackingSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/permissions/TimeTrackingSettings$State$Builder;

    invoke-direct {v1}, Lcom/squareup/permissions/TimeTrackingSettings$State$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/permissions/TimeTrackingSettings;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    .line 43
    invoke-virtual {v2}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isTimecardEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/permissions/TimeTrackingSettings$State$Builder;->setTimeTrackingEnabled(Z)Lcom/squareup/permissions/TimeTrackingSettings$State$Builder;

    move-result-object v1

    .line 44
    invoke-virtual {v1}, Lcom/squareup/permissions/TimeTrackingSettings$State$Builder;->build()Lcom/squareup/permissions/TimeTrackingSettings$State;

    move-result-object v1

    .line 42
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic lambda$NSvLsL9dJCd5RRmp3hr3nSkeAPI(Lcom/squareup/permissions/TimeTrackingSettings;Lcom/squareup/permissions/TimeTrackingSettings$Event;Lcom/squareup/permissions/TimeTrackingSettings$State;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/squareup/permissions/TimeTrackingSettings;->onEvent(Lcom/squareup/permissions/TimeTrackingSettings$Event;Lcom/squareup/permissions/TimeTrackingSettings$State;)V

    return-void
.end method

.method private onEvent(Lcom/squareup/permissions/TimeTrackingSettings$Event;Lcom/squareup/permissions/TimeTrackingSettings$State;)V
    .locals 1

    .line 52
    instance-of v0, p1, Lcom/squareup/permissions/TimeTrackingSettings$TimeTrackingSettingsChangedEvent;

    if-eqz v0, :cond_0

    .line 53
    check-cast p1, Lcom/squareup/permissions/TimeTrackingSettings$TimeTrackingSettingsChangedEvent;

    invoke-direct {p0, p1, p2}, Lcom/squareup/permissions/TimeTrackingSettings;->onTimeTrackingSettingsChangedEvent(Lcom/squareup/permissions/TimeTrackingSettings$TimeTrackingSettingsChangedEvent;Lcom/squareup/permissions/TimeTrackingSettings$State;)V

    :cond_0
    return-void
.end method

.method private onTimeTrackingSettingsChangedEvent(Lcom/squareup/permissions/TimeTrackingSettings$TimeTrackingSettingsChangedEvent;Lcom/squareup/permissions/TimeTrackingSettings$State;)V
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/squareup/permissions/TimeTrackingSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2}, Lcom/squareup/permissions/TimeTrackingSettings$State;->builder()Lcom/squareup/permissions/TimeTrackingSettings$State$Builder;

    move-result-object p2

    iget-boolean v1, p1, Lcom/squareup/permissions/TimeTrackingSettings$TimeTrackingSettingsChangedEvent;->shouldEnableTimeTracking:Z

    invoke-virtual {p2, v1}, Lcom/squareup/permissions/TimeTrackingSettings$State$Builder;->setTimeTrackingEnabled(Z)Lcom/squareup/permissions/TimeTrackingSettings$State$Builder;

    move-result-object p2

    .line 60
    invoke-virtual {p2}, Lcom/squareup/permissions/TimeTrackingSettings$State$Builder;->build()Lcom/squareup/permissions/TimeTrackingSettings$State;

    move-result-object p2

    .line 59
    invoke-virtual {v0, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 61
    iget-object p2, p0, Lcom/squareup/permissions/TimeTrackingSettings;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    sget-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->TIME_TRACKING:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    iget-boolean p1, p1, Lcom/squareup/permissions/TimeTrackingSettings$TimeTrackingSettingsChangedEvent;->shouldEnableTimeTracking:Z

    invoke-virtual {p2, v0, p1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->setBoolean(Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;Z)V

    return-void
.end method


# virtual methods
.method public enableOrDisableTimeTracking(Z)V
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/permissions/TimeTrackingSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/permissions/TimeTrackingSettings$TimeTrackingSettingsChangedEvent;

    invoke-direct {v1, p0, p1}, Lcom/squareup/permissions/TimeTrackingSettings$TimeTrackingSettingsChangedEvent;-><init>(Lcom/squareup/permissions/TimeTrackingSettings;Z)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 29
    invoke-direct {p0}, Lcom/squareup/permissions/TimeTrackingSettings;->initTimeTrackingSettings()V

    .line 30
    iget-object v0, p0, Lcom/squareup/permissions/TimeTrackingSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object v1, p0, Lcom/squareup/permissions/TimeTrackingSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 31
    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/jakewharton/rxrelay2/PublishRelay;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/permissions/-$$Lambda$TimeTrackingSettings$NSvLsL9dJCd5RRmp3hr3nSkeAPI;

    invoke-direct {v1, p0}, Lcom/squareup/permissions/-$$Lambda$TimeTrackingSettings$NSvLsL9dJCd5RRmp3hr3nSkeAPI;-><init>(Lcom/squareup/permissions/TimeTrackingSettings;)V

    invoke-static {v1}, Lcom/squareup/util/Rx2Tuples;->expandPair(Lio/reactivex/functions/BiConsumer;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 30
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public state()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/permissions/TimeTrackingSettings$State;",
            ">;"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/squareup/permissions/TimeTrackingSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method
