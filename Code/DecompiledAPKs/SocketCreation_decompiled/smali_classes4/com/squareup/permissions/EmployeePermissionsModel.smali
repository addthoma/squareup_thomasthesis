.class public interface abstract Lcom/squareup/permissions/EmployeePermissionsModel;
.super Ljava/lang/Object;
.source "EmployeePermissionsModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/permissions/EmployeePermissionsModel$InsertPermission;,
        Lcom/squareup/permissions/EmployeePermissionsModel$Factory;,
        Lcom/squareup/permissions/EmployeePermissionsModel$Mapper;,
        Lcom/squareup/permissions/EmployeePermissionsModel$Creator;
    }
.end annotation


# static fields
.field public static final CREATE_TABLE:Ljava/lang/String; = "CREATE TABLE employee_permissions_table (\n  employee_token TEXT NOT NULL REFERENCES employees_table,\n  permission TEXT\n)"

.field public static final EMPLOYEE_TOKEN:Ljava/lang/String; = "employee_token"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PERMISSION:Ljava/lang/String; = "permission"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TABLE_NAME:Ljava/lang/String; = "employee_permissions_table"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# virtual methods
.method public abstract employee_token()Ljava/lang/String;
.end method

.method public abstract permission()Ljava/lang/String;
.end method
