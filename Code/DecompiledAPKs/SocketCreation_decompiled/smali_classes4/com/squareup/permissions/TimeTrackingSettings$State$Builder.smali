.class public Lcom/squareup/permissions/TimeTrackingSettings$State$Builder;
.super Ljava/lang/Object;
.source "TimeTrackingSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissions/TimeTrackingSettings$State;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private timeTrackingEnabled:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/permissions/TimeTrackingSettings$State;
    .locals 2

    .line 97
    new-instance v0, Lcom/squareup/permissions/TimeTrackingSettings$State;

    iget-boolean v1, p0, Lcom/squareup/permissions/TimeTrackingSettings$State$Builder;->timeTrackingEnabled:Z

    invoke-direct {v0, v1}, Lcom/squareup/permissions/TimeTrackingSettings$State;-><init>(Z)V

    return-object v0
.end method

.method public setTimeTrackingEnabled(Z)Lcom/squareup/permissions/TimeTrackingSettings$State$Builder;
    .locals 0

    .line 92
    iput-boolean p1, p0, Lcom/squareup/permissions/TimeTrackingSettings$State$Builder;->timeTrackingEnabled:Z

    return-object p0
.end method
