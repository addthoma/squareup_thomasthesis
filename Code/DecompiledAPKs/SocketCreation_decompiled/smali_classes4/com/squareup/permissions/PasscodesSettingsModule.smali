.class public Lcom/squareup/permissions/PasscodesSettingsModule;
.super Ljava/lang/Object;
.source "PasscodesSettingsModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# static fields
.field private static final AFTER_LOG_OUT:Ljava/lang/String; = "require_passcode_after_log_out"

.field private static final BACKING_OUT_SALE:Ljava/lang/String; = "require_passcode_when_backing_out_of_sale"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideAfterLogOutSetting(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 24
    new-instance v0, Lcom/squareup/settings/BooleanLocalSetting;

    const-string v1, "require_passcode_after_log_out"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/settings/BooleanLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-object v0
.end method

.method static provideBackingOutOfSaleSetting(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 18
    new-instance v0, Lcom/squareup/settings/BooleanLocalSetting;

    const-string v1, "require_passcode_when_backing_out_of_sale"

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/settings/BooleanLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-object v0
.end method
