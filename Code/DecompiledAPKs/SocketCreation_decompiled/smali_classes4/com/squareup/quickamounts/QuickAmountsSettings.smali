.class public interface abstract Lcom/squareup/quickamounts/QuickAmountsSettings;
.super Ljava/lang/Object;
.source "QuickAmountsSettings.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/quickamounts/QuickAmountsSettings$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008f\u0018\u0000 \u000b2\u00020\u0001:\u0001\u000bJ\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H&J\u0010\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u0004H&\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/quickamounts/QuickAmountsSettings;",
        "",
        "amounts",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/quickamounts/QuickAmounts;",
        "setFeatureStatus",
        "",
        "status",
        "Lcom/squareup/quickamounts/QuickAmountsStatus;",
        "updateSetAmounts",
        "setAmounts",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/quickamounts/QuickAmountsSettings$Companion;

.field public static final MAX_VISIBLE_AMOUNTS:I = 0x3


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/quickamounts/QuickAmountsSettings$Companion;->$$INSTANCE:Lcom/squareup/quickamounts/QuickAmountsSettings$Companion;

    sput-object v0, Lcom/squareup/quickamounts/QuickAmountsSettings;->Companion:Lcom/squareup/quickamounts/QuickAmountsSettings$Companion;

    return-void
.end method


# virtual methods
.method public abstract amounts()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/quickamounts/QuickAmounts;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setFeatureStatus(Lcom/squareup/quickamounts/QuickAmountsStatus;)V
.end method

.method public abstract updateSetAmounts(Lcom/squareup/quickamounts/QuickAmounts;)V
.end method
