.class public final Lcom/squareup/quickamounts/ui/QuickAmountsTutorialAnalyticsKt;
.super Ljava/lang/Object;
.source "QuickAmountsTutorialAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u001a\u000c\u0010\u0004\u001a\u00020\u0005*\u00020\u0006H\u0000\u001a\u000c\u0010\u0007\u001a\u00020\u0005*\u00020\u0006H\u0000\u001a\u000c\u0010\u0008\u001a\u00020\u0005*\u00020\u0006H\u0000\u001a\u0014\u0010\t\u001a\u00020\u0005*\u00020\u00062\u0006\u0010\n\u001a\u00020\u0001H\u0002\u001a\u0014\u0010\u000b\u001a\u00020\u0005*\u00020\u00062\u0006\u0010\u000c\u001a\u00020\u0001H\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "CLICK_NOTIFICATION",
        "",
        "CLICK_TUTORIAL",
        "VIEW_WALKTHROUGH",
        "onClickedTutorial",
        "",
        "Lcom/squareup/analytics/Analytics;",
        "onClickedWalkthrough",
        "onCreatedWalkthrough",
        "sendClick",
        "description",
        "sendView",
        "to",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CLICK_NOTIFICATION:Ljava/lang/String; = "Quick Amounts Qualify Walkthrough: Keep"

.field private static final CLICK_TUTORIAL:Ljava/lang/String; = "Quick Amounts Tutorial: Use Quick Amounts"

.field private static final VIEW_WALKTHROUGH:Ljava/lang/String; = "Quick Amounts Qualify Walkthrough: Create"


# direct methods
.method public static final onClickedTutorial(Lcom/squareup/analytics/Analytics;)V
    .locals 1

    const-string v0, "$this$onClickedTutorial"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Quick Amounts Tutorial: Use Quick Amounts"

    .line 21
    invoke-static {p0, v0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialAnalyticsKt;->sendClick(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    return-void
.end method

.method public static final onClickedWalkthrough(Lcom/squareup/analytics/Analytics;)V
    .locals 1

    const-string v0, "$this$onClickedWalkthrough"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Quick Amounts Qualify Walkthrough: Keep"

    .line 17
    invoke-static {p0, v0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialAnalyticsKt;->sendClick(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    return-void
.end method

.method public static final onCreatedWalkthrough(Lcom/squareup/analytics/Analytics;)V
    .locals 1

    const-string v0, "$this$onCreatedWalkthrough"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Quick Amounts Qualify Walkthrough: Create"

    .line 13
    invoke-static {p0, v0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialAnalyticsKt;->sendView(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    return-void
.end method

.method private static final sendClick(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/analytics/event/ClickEvent;

    invoke-direct {v0, p1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method private static final sendView(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/analytics/event/ViewEvent;

    invoke-direct {v0, p1}, Lcom/squareup/analytics/event/ViewEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method
