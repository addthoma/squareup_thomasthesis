.class final Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1$1;
.super Ljava/lang/Object;
.source "RealQuickAmountsSettings.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1;->apply(Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/quickamounts/QuickAmounts;",
        "quickAmounts",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $source:Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;

.field final synthetic this$0:Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1;


# direct methods
.method constructor <init>(Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1;Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1$1;->this$0:Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1;

    iput-object p2, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1$1;->$source:Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;)Lcom/squareup/quickamounts/QuickAmounts;
    .locals 3

    const-string v0, "quickAmounts"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1$1;->$source:Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;

    .line 48
    instance-of v1, v0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1$1;->this$0:Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1;

    iget-object v0, v0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1;->this$0:Lcom/squareup/quickamounts/RealQuickAmountsSettings;

    iget-object v1, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1$1;->$source:Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;

    const-string v2, "source"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;

    invoke-static {v0, p1, v1}, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->access$fromCache(Lcom/squareup/quickamounts/RealQuickAmountsSettings;Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;)Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object p1

    goto :goto_0

    .line 49
    :cond_0
    instance-of v0, v0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$CatalogLocal;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1$1;->this$0:Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1;

    iget-object v0, v0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1;->this$0:Lcom/squareup/quickamounts/RealQuickAmountsSettings;

    invoke-static {v0, p1}, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->access$fromCogs(Lcom/squareup/quickamounts/RealQuickAmountsSettings;Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;)Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;

    invoke-virtual {p0, p1}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1$1;->apply(Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;)Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object p1

    return-object p1
.end method
