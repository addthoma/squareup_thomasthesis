.class public final Lcom/squareup/quickamounts/settings/ClearQuickAmountPlugin;
.super Lcom/squareup/noho/HideablePlugin;
.source "ClearQuickAmountPlugin.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0008\u0010\u000f\u001a\u00020\tH\u0016J\u0014\u0010\u000f\u001a\u00020\u00062\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005R\u0014\u0010\u0008\u001a\u00020\tX\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\nR\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/quickamounts/settings/ClearQuickAmountPlugin;",
        "Lcom/squareup/noho/HideablePlugin;",
        "context",
        "Landroid/content/Context;",
        "onClickHandler",
        "Lkotlin/Function0;",
        "",
        "(Landroid/content/Context;Lkotlin/jvm/functions/Function0;)V",
        "isClickable",
        "",
        "()Z",
        "description",
        "",
        "editDescription",
        "",
        "onClick",
        "handler",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final isClickable:Z

.field private onClickHandler:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkotlin/jvm/functions/Function0;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClickHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    new-instance v0, Lcom/squareup/noho/IconPlugin;

    .line 15
    sget-object v3, Lcom/squareup/noho/NohoEditRow$Side;->END:Lcom/squareup/noho/NohoEditRow$Side;

    .line 16
    sget v4, Lcom/squareup/vectoricons/R$drawable;->ui_x_filled_16:I

    .line 17
    sget v5, Lcom/squareup/noho/R$dimen;->noho_edit_default_margin:I

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    move-object v1, v0

    move-object v2, p1

    .line 13
    invoke-direct/range {v1 .. v8}, Lcom/squareup/noho/IconPlugin;-><init>(Landroid/content/Context;Lcom/squareup/noho/NohoEditRow$Side;IIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/noho/NohoEditRow$Plugin;

    const/4 p1, 0x1

    .line 12
    invoke-direct {p0, v0, p1}, Lcom/squareup/noho/HideablePlugin;-><init>(Lcom/squareup/noho/NohoEditRow$Plugin;Z)V

    iput-object p2, p0, Lcom/squareup/quickamounts/settings/ClearQuickAmountPlugin;->onClickHandler:Lkotlin/jvm/functions/Function0;

    .line 22
    iput-boolean p1, p0, Lcom/squareup/quickamounts/settings/ClearQuickAmountPlugin;->isClickable:Z

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    .line 11
    sget-object p2, Lcom/squareup/quickamounts/settings/ClearQuickAmountPlugin$1;->INSTANCE:Lcom/squareup/quickamounts/settings/ClearQuickAmountPlugin$1;

    check-cast p2, Lkotlin/jvm/functions/Function0;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/quickamounts/settings/ClearQuickAmountPlugin;-><init>(Landroid/content/Context;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method


# virtual methods
.method public description(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 1

    const-string v0, "editDescription"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-virtual {p0}, Lcom/squareup/quickamounts/settings/ClearQuickAmountPlugin;->getEdit()Lcom/squareup/noho/NohoEditRow;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditRow;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/quickamounts/settings/impl/R$string;->quick_amounts_settings_clear_value_accessibility:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "edit.resources.getString\u2026lear_value_accessibility)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public isClickable()Z
    .locals 1

    .line 22
    iget-boolean v0, p0, Lcom/squareup/quickamounts/settings/ClearQuickAmountPlugin;->isClickable:Z

    return v0
.end method

.method public final onClick(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "handler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iput-object p1, p0, Lcom/squareup/quickamounts/settings/ClearQuickAmountPlugin;->onClickHandler:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public onClick()Z
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/ClearQuickAmountPlugin;->onClickHandler:Lkotlin/jvm/functions/Function0;

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    const/4 v0, 0x0

    return v0
.end method
