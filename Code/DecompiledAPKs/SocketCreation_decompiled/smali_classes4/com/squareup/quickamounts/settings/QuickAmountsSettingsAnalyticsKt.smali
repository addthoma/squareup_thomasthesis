.class public final Lcom/squareup/quickamounts/settings/QuickAmountsSettingsAnalyticsKt;
.super Ljava/lang/Object;
.source "QuickAmountsSettingsAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nQuickAmountsSettingsAnalytics.kt\nKotlin\n*S Kotlin\n*F\n+ 1 QuickAmountsSettingsAnalytics.kt\ncom/squareup/quickamounts/settings/QuickAmountsSettingsAnalyticsKt\n*L\n1#1,55:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0008\u001a\u000c\u0010\u0007\u001a\u00020\u0008*\u00020\tH\u0000\u001a\u000c\u0010\n\u001a\u00020\u0008*\u00020\tH\u0000\u001a\"\u0010\u000b\u001a\u00020\u0008*\u00020\t2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r2\u0006\u0010\u000f\u001a\u00020\u0010H\u0000\u001a\u000c\u0010\u0011\u001a\u00020\u0008*\u00020\tH\u0000\u001a\u000c\u0010\u0012\u001a\u00020\u0008*\u00020\tH\u0000\u001a\u000c\u0010\u0013\u001a\u00020\u0008*\u00020\tH\u0000\u001a\u0014\u0010\u0014\u001a\u00020\u0008*\u00020\t2\u0006\u0010\u0015\u001a\u00020\u0001H\u0002\u001a\u0014\u0010\u0016\u001a\u00020\u0008*\u00020\t2\u0006\u0010\u0017\u001a\u00020\u0001H\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "CLICK_AUTO_AMOUNTS",
        "",
        "CLICK_CREATE_ITEM",
        "CLICK_CREATE_SET_AMOUNTS",
        "CLICK_OFF",
        "CLICK_SET_AMOUNTS",
        "VIEW_CREATE_ITEM",
        "onClickedAutoAmounts",
        "",
        "Lcom/squareup/analytics/Analytics;",
        "onClickedCreateItem",
        "onClickedCreateSetAmounts",
        "amounts",
        "",
        "Lcom/squareup/protos/connect/v2/common/Money;",
        "autoAmountsEnabled",
        "",
        "onClickedOff",
        "onClickedSetAmounts",
        "onCreatedCreateItem",
        "sendClick",
        "description",
        "sendView",
        "to",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CLICK_AUTO_AMOUNTS:Ljava/lang/String; = "Quick Amounts Settings: Auto Amounts"

.field private static final CLICK_CREATE_ITEM:Ljava/lang/String; = "Quick Amounts Create Item Modal: Create Items"

.field private static final CLICK_CREATE_SET_AMOUNTS:Ljava/lang/String; = "Quick Amounts Settings: Create Set Amounts"

.field private static final CLICK_OFF:Ljava/lang/String; = "Quick Amounts Settings: Off"

.field private static final CLICK_SET_AMOUNTS:Ljava/lang/String; = "Quick Amounts Settings: Set Amounts"

.field private static final VIEW_CREATE_ITEM:Ljava/lang/String; = "Quick Amounts Create Item Modal: Create"


# direct methods
.method public static final onClickedAutoAmounts(Lcom/squareup/analytics/Analytics;)V
    .locals 1

    const-string v0, "$this$onClickedAutoAmounts"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Quick Amounts Settings: Auto Amounts"

    .line 21
    invoke-static {p0, v0}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsAnalyticsKt;->sendClick(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    return-void
.end method

.method public static final onClickedCreateItem(Lcom/squareup/analytics/Analytics;)V
    .locals 1

    const-string v0, "$this$onClickedCreateItem"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Quick Amounts Create Item Modal: Create Items"

    .line 45
    invoke-static {p0, v0}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsAnalyticsKt;->sendClick(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    return-void
.end method

.method public static final onClickedCreateSetAmounts(Lcom/squareup/analytics/Analytics;Ljava/util/List;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/Analytics;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;Z)V"
        }
    .end annotation

    const-string v0, "$this$onClickedCreateSetAmounts"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amounts"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " {"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 34
    move-object v1, p1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_0

    .line 35
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\"amount"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v6, v4, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, "\":"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v4, v4, Lcom/squareup/protos/connect/v2/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v4, 0x2c

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v4, v6

    goto :goto_0

    .line 37
    :cond_0
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 38
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\"currency\":\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/connect/v2/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/common/Money;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "\","

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\"autoamounts_enabled\":"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 p2, 0x7d

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Quick Amounts Settings: Create Set Amounts"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsAnalyticsKt;->sendClick(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    return-void
.end method

.method public static final onClickedOff(Lcom/squareup/analytics/Analytics;)V
    .locals 1

    const-string v0, "$this$onClickedOff"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Quick Amounts Settings: Off"

    .line 29
    invoke-static {p0, v0}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsAnalyticsKt;->sendClick(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    return-void
.end method

.method public static final onClickedSetAmounts(Lcom/squareup/analytics/Analytics;)V
    .locals 1

    const-string v0, "$this$onClickedSetAmounts"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Quick Amounts Settings: Set Amounts"

    .line 25
    invoke-static {p0, v0}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsAnalyticsKt;->sendClick(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    return-void
.end method

.method public static final onCreatedCreateItem(Lcom/squareup/analytics/Analytics;)V
    .locals 1

    const-string v0, "$this$onCreatedCreateItem"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Quick Amounts Create Item Modal: Create"

    .line 17
    invoke-static {p0, v0}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsAnalyticsKt;->sendView(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    return-void
.end method

.method private static final sendClick(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V
    .locals 1

    .line 53
    new-instance v0, Lcom/squareup/analytics/event/ClickEvent;

    invoke-direct {v0, p1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method private static final sendView(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V
    .locals 1

    .line 49
    new-instance v0, Lcom/squareup/analytics/event/ViewEvent;

    invoke-direct {v0, p1}, Lcom/squareup/analytics/event/ViewEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method
