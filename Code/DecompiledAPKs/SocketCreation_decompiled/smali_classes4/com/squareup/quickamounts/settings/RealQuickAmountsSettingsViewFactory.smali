.class public final Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "RealQuickAmountsSettingsViewFactory.kt"

# interfaces
.implements Lcom/squareup/quickamounts/settings/QuickAmountsSettingsViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u000f\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsViewFactory;",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "quickAmountsFactory",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$Factory;",
        "(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$Factory;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "quickAmountsFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 11
    new-instance v1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$Binding;

    invoke-direct {v1, p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$Binding;-><init>(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$Factory;)V

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p1, 0x0

    aput-object v1, v0, p1

    .line 12
    sget-object p1, Lcom/squareup/quickamounts/settings/CreateItemDialogFactory;->Companion:Lcom/squareup/quickamounts/settings/CreateItemDialogFactory$Companion;

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 10
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
