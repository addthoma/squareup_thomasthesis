.class public abstract Lcom/squareup/pushmessages/PushMessageModule;
.super Ljava/lang/Object;
.source "PushMessageModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/pushmessages/PushMessageModule$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\'\u0018\u0000 \u00072\u00020\u0001:\u0001\u0007B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\'\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/pushmessages/PushMessageModule;",
        "",
        "()V",
        "bindPushMessageDelegate",
        "Lcom/squareup/pushmessages/PushMessageDelegate;",
        "registrar",
        "Lcom/squareup/pushmessages/RealPushMessageDelegate;",
        "Companion",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/pushmessages/PushMessageModule$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/pushmessages/PushMessageModule$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/pushmessages/PushMessageModule$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/pushmessages/PushMessageModule;->Companion:Lcom/squareup/pushmessages/PushMessageModule$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final providePushServiceEnabled(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .param p0    # Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
        .annotation runtime Lcom/squareup/settings/DeviceSettings;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/pushmessages/PushServiceEnabled;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/pushmessages/PushMessageModule;->Companion:Lcom/squareup/pushmessages/PushMessageModule$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/pushmessages/PushMessageModule$Companion;->providePushServiceEnabled(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method public static final providePushServiceRegistration(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;
    .locals 1
    .param p0    # Landroid/content/SharedPreferences;
        .annotation runtime Lcom/squareup/settings/DeviceSettings;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/pushmessages/PushMessageModule;->Companion:Lcom/squareup/pushmessages/PushMessageModule$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/pushmessages/PushMessageModule$Companion;->providePushServiceRegistration(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;

    move-result-object p0

    return-object p0
.end method

.method public static final providePushServicesAsSetForLoggedIn(Lcom/squareup/pushmessages/RealPushMessageDelegate;)Lmortar/Scoped;
    .locals 1
    .annotation runtime Lcom/squareup/ForApp;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/pushmessages/PushMessageModule;->Companion:Lcom/squareup/pushmessages/PushMessageModule$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/pushmessages/PushMessageModule$Companion;->providePushServicesAsSetForLoggedIn(Lcom/squareup/pushmessages/RealPushMessageDelegate;)Lmortar/Scoped;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public abstract bindPushMessageDelegate(Lcom/squareup/pushmessages/RealPushMessageDelegate;)Lcom/squareup/pushmessages/PushMessageDelegate;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
