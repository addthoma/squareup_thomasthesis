.class public final Lcom/squareup/pushmessages/PushMessageModule_ProvidePushServicesAsSetForLoggedInFactory;
.super Ljava/lang/Object;
.source "PushMessageModule_ProvidePushServicesAsSetForLoggedInFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lmortar/Scoped;",
        ">;"
    }
.end annotation


# instance fields
.field private final delegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/RealPushMessageDelegate;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/RealPushMessageDelegate;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/pushmessages/PushMessageModule_ProvidePushServicesAsSetForLoggedInFactory;->delegateProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/pushmessages/PushMessageModule_ProvidePushServicesAsSetForLoggedInFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/RealPushMessageDelegate;",
            ">;)",
            "Lcom/squareup/pushmessages/PushMessageModule_ProvidePushServicesAsSetForLoggedInFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/pushmessages/PushMessageModule_ProvidePushServicesAsSetForLoggedInFactory;

    invoke-direct {v0, p0}, Lcom/squareup/pushmessages/PushMessageModule_ProvidePushServicesAsSetForLoggedInFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static providePushServicesAsSetForLoggedIn(Lcom/squareup/pushmessages/RealPushMessageDelegate;)Lmortar/Scoped;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/pushmessages/PushMessageModule;->providePushServicesAsSetForLoggedIn(Lcom/squareup/pushmessages/RealPushMessageDelegate;)Lmortar/Scoped;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lmortar/Scoped;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/pushmessages/PushMessageModule_ProvidePushServicesAsSetForLoggedInFactory;->get()Lmortar/Scoped;

    move-result-object v0

    return-object v0
.end method

.method public get()Lmortar/Scoped;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/pushmessages/PushMessageModule_ProvidePushServicesAsSetForLoggedInFactory;->delegateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/pushmessages/RealPushMessageDelegate;

    invoke-static {v0}, Lcom/squareup/pushmessages/PushMessageModule_ProvidePushServicesAsSetForLoggedInFactory;->providePushServicesAsSetForLoggedIn(Lcom/squareup/pushmessages/RealPushMessageDelegate;)Lmortar/Scoped;

    move-result-object v0

    return-object v0
.end method
