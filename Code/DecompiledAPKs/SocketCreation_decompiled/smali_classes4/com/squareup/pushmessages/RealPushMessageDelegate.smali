.class public final Lcom/squareup/pushmessages/RealPushMessageDelegate;
.super Ljava/lang/Object;
.source "RealPushMessageDelegate.kt"

# interfaces
.implements Lcom/squareup/pushmessages/PushMessageDelegate;
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/AppScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u0002B\u0019\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0012\u0010\u000c\u001a\u00020\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\nH\u0002J&\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u00110\u0010\"\u0008\u0008\u0000\u0010\u0011*\u00020\n2\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u0002H\u00110\u0013H\u0016J\u0010\u0010\u0014\u001a\u00020\r2\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0008\u0010\u0017\u001a\u00020\rH\u0016R\u001c\u0010\u0008\u001a\u0010\u0012\u000c\u0012\n \u000b*\u0004\u0018\u00010\n0\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/pushmessages/RealPushMessageDelegate;",
        "Lcom/squareup/pushmessages/PushMessageDelegate;",
        "Lmortar/Scoped;",
        "pushService",
        "Lcom/squareup/push/PushService;",
        "scheduler",
        "Lio/reactivex/Scheduler;",
        "(Lcom/squareup/push/PushService;Lio/reactivex/Scheduler;)V",
        "pushMessageRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lcom/squareup/pushmessages/PushMessage;",
        "kotlin.jvm.PlatformType",
        "logPushMessage",
        "",
        "message",
        "observe",
        "Lio/reactivex/Observable;",
        "T",
        "clazz",
        "Ljava/lang/Class;",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final pushMessageRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/pushmessages/PushMessage;",
            ">;"
        }
    .end annotation
.end field

.field private final pushService:Lcom/squareup/push/PushService;

.field private final scheduler:Lio/reactivex/Scheduler;


# direct methods
.method public constructor <init>(Lcom/squareup/push/PushService;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Computation;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "pushService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/pushmessages/RealPushMessageDelegate;->pushService:Lcom/squareup/push/PushService;

    iput-object p2, p0, Lcom/squareup/pushmessages/RealPushMessageDelegate;->scheduler:Lio/reactivex/Scheduler;

    .line 26
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<PushMessage>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/pushmessages/RealPushMessageDelegate;->pushMessageRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method public static final synthetic access$logPushMessage(Lcom/squareup/pushmessages/RealPushMessageDelegate;Lcom/squareup/pushmessages/PushMessage;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/pushmessages/RealPushMessageDelegate;->logPushMessage(Lcom/squareup/pushmessages/PushMessage;)V

    return-void
.end method

.method private final logPushMessage(Lcom/squareup/pushmessages/PushMessage;)V
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 50
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[PUSH] Successfully parsed push message payload with type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x2e

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array v0, v0, [Ljava/lang/Object;

    .line 49
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-array p1, v0, [Ljava/lang/Object;

    const-string v0, "[PUSH] Failed to parse push message payload."

    .line 53
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public observe(Ljava/lang/Class;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/pushmessages/PushMessage;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)",
            "Lio/reactivex/Observable<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "clazz"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushMessageDelegate;->pushMessageRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "pushMessageRelay.ofType(clazz)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushMessageDelegate;->pushService:Lcom/squareup/push/PushService;

    invoke-interface {v0}, Lcom/squareup/push/PushService;->observeMessages()Lio/reactivex/Observable;

    move-result-object v0

    .line 30
    iget-object v1, p0, Lcom/squareup/pushmessages/RealPushMessageDelegate;->scheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "pushService.observeMessa\u2026    .observeOn(scheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    new-instance v1, Lcom/squareup/pushmessages/RealPushMessageDelegate$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/pushmessages/RealPushMessageDelegate$onEnterScope$1;-><init>(Lcom/squareup/pushmessages/RealPushMessageDelegate;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Kt;->mapNotNull(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object v0

    .line 35
    iget-object v1, p0, Lcom/squareup/pushmessages/RealPushMessageDelegate;->pushMessageRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "pushService.observeMessa\u2026bscribe(pushMessageRelay)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
