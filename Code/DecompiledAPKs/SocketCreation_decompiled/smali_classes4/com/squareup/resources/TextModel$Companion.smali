.class public final Lcom/squareup/resources/TextModel$Companion;
.super Ljava/lang/Object;
.source "TextModels.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/resources/TextModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u001b\u0010\u0003\u001a\u000c\u0012\u0004\u0012\u00020\u00050\u0004j\u0002`\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/resources/TextModel$Companion;",
        "",
        "()V",
        "empty",
        "Lcom/squareup/resources/FixedText;",
        "",
        "Lcom/squareup/resources/FixedString;",
        "getEmpty",
        "()Lcom/squareup/resources/FixedText;",
        "resources_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/resources/TextModel$Companion;

.field private static final empty:Lcom/squareup/resources/FixedText;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/resources/FixedText<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 16
    new-instance v0, Lcom/squareup/resources/TextModel$Companion;

    invoke-direct {v0}, Lcom/squareup/resources/TextModel$Companion;-><init>()V

    sput-object v0, Lcom/squareup/resources/TextModel$Companion;->$$INSTANCE:Lcom/squareup/resources/TextModel$Companion;

    .line 17
    new-instance v0, Lcom/squareup/resources/FixedText;

    const-string v1, ""

    check-cast v1, Ljava/lang/CharSequence;

    invoke-direct {v0, v1}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lcom/squareup/resources/TextModel$Companion;->empty:Lcom/squareup/resources/FixedText;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getEmpty()Lcom/squareup/resources/FixedText;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/FixedText<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 17
    sget-object v0, Lcom/squareup/resources/TextModel$Companion;->empty:Lcom/squareup/resources/FixedText;

    return-object v0
.end method
