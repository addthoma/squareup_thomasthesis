.class public final Lcom/squareup/referrals/ReferralCoordinator_Factory;
.super Ljava/lang/Object;
.source "ReferralCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/referrals/ReferralCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final referralRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/referrals/ReferralRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final rewardsCopyExperimentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;",
            ">;"
        }
    .end annotation
.end field

.field private final spinnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/referrals/ReferralRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/referrals/ReferralCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/referrals/ReferralCoordinator_Factory;->referralRunnerProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/referrals/ReferralCoordinator_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/referrals/ReferralCoordinator_Factory;->rewardsCopyExperimentProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p5, p0, Lcom/squareup/referrals/ReferralCoordinator_Factory;->spinnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/referrals/ReferralCoordinator_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/referrals/ReferralRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;)",
            "Lcom/squareup/referrals/ReferralCoordinator_Factory;"
        }
    .end annotation

    .line 46
    new-instance v6, Lcom/squareup/referrals/ReferralCoordinator_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/referrals/ReferralCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/referrals/ReferralRunner;Lcom/squareup/util/Device;Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/referrals/ReferralCoordinator;
    .locals 7

    .line 52
    new-instance v6, Lcom/squareup/referrals/ReferralCoordinator;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/referrals/ReferralCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/referrals/ReferralRunner;Lcom/squareup/util/Device;Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;Lcom/squareup/register/widgets/GlassSpinner;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/referrals/ReferralCoordinator;
    .locals 5

    .line 39
    iget-object v0, p0, Lcom/squareup/referrals/ReferralCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/referrals/ReferralCoordinator_Factory;->referralRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/referrals/ReferralRunner;

    iget-object v2, p0, Lcom/squareup/referrals/ReferralCoordinator_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Device;

    iget-object v3, p0, Lcom/squareup/referrals/ReferralCoordinator_Factory;->rewardsCopyExperimentProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;

    iget-object v4, p0, Lcom/squareup/referrals/ReferralCoordinator_Factory;->spinnerProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/register/widgets/GlassSpinner;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/referrals/ReferralCoordinator_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/referrals/ReferralRunner;Lcom/squareup/util/Device;Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/referrals/ReferralCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/referrals/ReferralCoordinator_Factory;->get()Lcom/squareup/referrals/ReferralCoordinator;

    move-result-object v0

    return-object v0
.end method
