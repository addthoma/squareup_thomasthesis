.class public Lcom/squareup/referrals/ReferralScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "ReferralScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/referrals/ReferralScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/referrals/ReferralScreen$Component;,
        Lcom/squareup/referrals/ReferralScreen$ParentComponent;,
        Lcom/squareup/referrals/ReferralScreen$Style;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/referrals/ReferralScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final parent:Lcom/squareup/ui/main/RegisterTreeKey;

.field final style:Lcom/squareup/referrals/ReferralScreen$Style;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 74
    sget-object v0, Lcom/squareup/referrals/-$$Lambda$ReferralScreen$5plCIIOlSrO3yijrgJnmpEZ75Z8;->INSTANCE:Lcom/squareup/referrals/-$$Lambda$ReferralScreen$5plCIIOlSrO3yijrgJnmpEZ75Z8;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/referrals/ReferralScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/referrals/ReferralScreen$Style;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/referrals/ReferralScreen;->parent:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 31
    iput-object p2, p0, Lcom/squareup/referrals/ReferralScreen;->style:Lcom/squareup/referrals/ReferralScreen$Style;

    return-void
.end method

.method public static forOnboarding(Lcom/squareup/ui/main/RegisterTreeKey;)Lcom/squareup/referrals/ReferralScreen;
    .locals 2

    .line 41
    new-instance v0, Lcom/squareup/referrals/ReferralScreen;

    sget-object v1, Lcom/squareup/referrals/ReferralScreen$Style;->ONBOARDING:Lcom/squareup/referrals/ReferralScreen$Style;

    invoke-direct {v0, p0, v1}, Lcom/squareup/referrals/ReferralScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/referrals/ReferralScreen$Style;)V

    return-object v0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/referrals/ReferralScreen;
    .locals 2

    .line 75
    const-class v0, Lcom/squareup/referrals/ReferralScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 76
    invoke-static {}, Lcom/squareup/referrals/ReferralScreen$Style;->values()[Lcom/squareup/referrals/ReferralScreen$Style;

    move-result-object v1

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    aget-object p0, v1, p0

    .line 77
    new-instance v1, Lcom/squareup/referrals/ReferralScreen;

    invoke-direct {v1, v0, p0}, Lcom/squareup/referrals/ReferralScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/referrals/ReferralScreen$Style;)V

    return-object v1
.end method

.method public static withActionBar(Lcom/squareup/ui/main/RegisterTreeKey;)Lcom/squareup/referrals/ReferralScreen;
    .locals 2

    .line 36
    new-instance v0, Lcom/squareup/referrals/ReferralScreen;

    sget-object v1, Lcom/squareup/referrals/ReferralScreen$Style;->ACTION_BAR:Lcom/squareup/referrals/ReferralScreen$Style;

    invoke-direct {v0, p0, v1}, Lcom/squareup/referrals/ReferralScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/referrals/ReferralScreen$Style;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/referrals/ReferralScreen;->parent:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 71
    iget-object p2, p0, Lcom/squareup/referrals/ReferralScreen;->style:Lcom/squareup/referrals/ReferralScreen$Style;

    invoke-virtual {p2}, Lcom/squareup/referrals/ReferralScreen$Style;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 66
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->HELP_FLOW_REFERRAL:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/referrals/ReferralScreen;->parent:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 85
    const-class v0, Lcom/squareup/referrals/ReferralScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/referrals/ReferralScreen$Component;

    invoke-interface {p1}, Lcom/squareup/referrals/ReferralScreen$Component;->referralCoordinator()Lcom/squareup/referrals/ReferralCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 49
    const-class v0, Lcom/squareup/referrals/ReferralScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/referrals/ReferralScreen$Component;

    .line 50
    invoke-interface {v0}, Lcom/squareup/referrals/ReferralScreen$Component;->runner()Lcom/squareup/referrals/ReferralRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 81
    sget v0, Lcom/squareup/referrals/R$layout;->referral_view:I

    return v0
.end method
