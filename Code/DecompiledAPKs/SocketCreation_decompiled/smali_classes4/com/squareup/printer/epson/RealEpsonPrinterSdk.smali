.class public final Lcom/squareup/printer/epson/RealEpsonPrinterSdk;
.super Ljava/lang/Object;
.source "RealEpsonPrinterSdk.kt"

# interfaces
.implements Lcom/squareup/printer/epson/EpsonPrinterSdk;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u0006\n\u0002\u0008\u0006\n\u0002\u0010\u000e\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u0003H\u0016JX\u0010\r\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u00032\u0006\u0010\u0016\u001a\u00020\u00032\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0003H\u0016J\u0018\u0010\u001a\u001a\u00020\u000b2\u0006\u0010\u001b\u001a\u00020\u00032\u0006\u0010\u001c\u001a\u00020\u0003H\u0016J\u0010\u0010\u001d\u001a\u00020\u000b2\u0006\u0010\u001e\u001a\u00020\u001fH\u0016J(\u0010 \u001a\u00020\u000b2\u0006\u0010!\u001a\u00020\u00032\u0006\u0010\"\u001a\u00020\u00032\u0006\u0010#\u001a\u00020\u00032\u0006\u0010$\u001a\u00020\u0003H\u0016J\u0008\u0010%\u001a\u00020\u000bH\u0016J\u0008\u0010&\u001a\u00020\u000bH\u0016J\u0018\u0010\'\u001a\u00020\u000b2\u0006\u0010(\u001a\u00020\u001f2\u0006\u0010)\u001a\u00020\u0003H\u0016J\u0008\u0010*\u001a\u00020\u000bH\u0016J\u0008\u0010+\u001a\u00020,H\u0016J\u0010\u0010-\u001a\u00020\u000b2\u0006\u0010)\u001a\u00020\u0003H\u0016JW\u0010.\u001a\u00020\u000b2M\u0010/\u001aI\u0012\u0013\u0012\u00110\u0003\u00a2\u0006\u000c\u00081\u0012\u0008\u00082\u0012\u0004\u0008\u0008(3\u0012\u0013\u0012\u00110,\u00a2\u0006\u000c\u00081\u0012\u0008\u00082\u0012\u0004\u0008\u0008(4\u0012\u0015\u0012\u0013\u0018\u00010\u001f\u00a2\u0006\u000c\u00081\u0012\u0008\u00082\u0012\u0004\u0008\u0008(5\u0012\u0004\u0012\u00020\u000b00H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00066"
    }
    d2 = {
        "Lcom/squareup/printer/epson/RealEpsonPrinterSdk;",
        "Lcom/squareup/printer/epson/EpsonPrinterSdk;",
        "printerCode",
        "",
        "language",
        "context",
        "Landroid/app/Application;",
        "(IILandroid/app/Application;)V",
        "actualEpsonPrinterSdk",
        "Lcom/epson/epos2/printer/Printer;",
        "addCut",
        "",
        "type",
        "addImage",
        "data",
        "Landroid/graphics/Bitmap;",
        "x",
        "y",
        "width",
        "height",
        "color",
        "mode",
        "halftone",
        "brightness",
        "",
        "compress",
        "addPulse",
        "drawer",
        "time",
        "addText",
        "text",
        "",
        "addTextStyle",
        "reverse",
        "underline",
        "bold",
        "textColor",
        "clearCommandBuffer",
        "clearReceiveEventListener",
        "connect",
        "target",
        "timeout",
        "disconnect",
        "getPrinterStatusInfo",
        "Lcom/squareup/printer/epson/PrinterStatusInfo;",
        "sendData",
        "setReceiveEventListener",
        "receiveEventListener",
        "Lkotlin/Function3;",
        "Lkotlin/ParameterName;",
        "name",
        "code",
        "printerStatusInfo",
        "printJobId",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actualEpsonPrinterSdk:Lcom/epson/epos2/printer/Printer;


# direct methods
.method public constructor <init>(IILandroid/app/Application;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Lcom/epson/epos2/printer/Printer;

    check-cast p3, Landroid/content/Context;

    invoke-direct {v0, p1, p2, p3}, Lcom/epson/epos2/printer/Printer;-><init>(IILandroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/printer/epson/RealEpsonPrinterSdk;->actualEpsonPrinterSdk:Lcom/epson/epos2/printer/Printer;

    return-void
.end method

.method public static final synthetic access$getActualEpsonPrinterSdk$p(Lcom/squareup/printer/epson/RealEpsonPrinterSdk;)Lcom/epson/epos2/printer/Printer;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/squareup/printer/epson/RealEpsonPrinterSdk;->actualEpsonPrinterSdk:Lcom/epson/epos2/printer/Printer;

    return-object p0
.end method


# virtual methods
.method public addCut(I)V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/printer/epson/RealEpsonPrinterSdk;->actualEpsonPrinterSdk:Lcom/epson/epos2/printer/Printer;

    invoke-virtual {v0, p1}, Lcom/epson/epos2/printer/Printer;->addCut(I)V

    return-void
.end method

.method public addImage(Landroid/graphics/Bitmap;IIIIIIIDI)V
    .locals 13

    const-string v0, "data"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    .line 52
    iget-object v1, v0, Lcom/squareup/printer/epson/RealEpsonPrinterSdk;->actualEpsonPrinterSdk:Lcom/epson/epos2/printer/Printer;

    move v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-wide/from16 v10, p9

    move/from16 v12, p11

    invoke-virtual/range {v1 .. v12}, Lcom/epson/epos2/printer/Printer;->addImage(Landroid/graphics/Bitmap;IIIIIIIDI)V

    return-void
.end method

.method public addPulse(II)V
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/printer/epson/RealEpsonPrinterSdk;->actualEpsonPrinterSdk:Lcom/epson/epos2/printer/Printer;

    invoke-virtual {v0, p1, p2}, Lcom/epson/epos2/printer/Printer;->addPulse(II)V

    return-void
.end method

.method public addText(Ljava/lang/String;)V
    .locals 1

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/printer/epson/RealEpsonPrinterSdk;->actualEpsonPrinterSdk:Lcom/epson/epos2/printer/Printer;

    invoke-virtual {v0, p1}, Lcom/epson/epos2/printer/Printer;->addText(Ljava/lang/String;)V

    return-void
.end method

.method public addTextStyle(IIII)V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/printer/epson/RealEpsonPrinterSdk;->actualEpsonPrinterSdk:Lcom/epson/epos2/printer/Printer;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/epson/epos2/printer/Printer;->addTextStyle(IIII)V

    return-void
.end method

.method public clearCommandBuffer()V
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/printer/epson/RealEpsonPrinterSdk;->actualEpsonPrinterSdk:Lcom/epson/epos2/printer/Printer;

    invoke-virtual {v0}, Lcom/epson/epos2/printer/Printer;->clearCommandBuffer()V

    return-void
.end method

.method public clearReceiveEventListener()V
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/printer/epson/RealEpsonPrinterSdk;->actualEpsonPrinterSdk:Lcom/epson/epos2/printer/Printer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/epson/epos2/printer/Printer;->setReceiveEventListener(Lcom/epson/epos2/printer/ReceiveListener;)V

    return-void
.end method

.method public connect(Ljava/lang/String;I)V
    .locals 1

    const-string v0, "target"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/squareup/printer/epson/RealEpsonPrinterSdk;->actualEpsonPrinterSdk:Lcom/epson/epos2/printer/Printer;

    invoke-virtual {v0, p1, p2}, Lcom/epson/epos2/printer/Printer;->connect(Ljava/lang/String;I)V

    return-void
.end method

.method public disconnect()V
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/printer/epson/RealEpsonPrinterSdk;->actualEpsonPrinterSdk:Lcom/epson/epos2/printer/Printer;

    invoke-virtual {v0}, Lcom/epson/epos2/printer/Printer;->disconnect()V

    return-void
.end method

.method public getPrinterStatusInfo()Lcom/squareup/printer/epson/PrinterStatusInfo;
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/squareup/printer/epson/RealEpsonPrinterSdk;->actualEpsonPrinterSdk:Lcom/epson/epos2/printer/Printer;

    invoke-virtual {v0}, Lcom/epson/epos2/printer/Printer;->getStatus()Lcom/epson/epos2/printer/PrinterStatusInfo;

    move-result-object v0

    const-string v1, "actualEpsonPrinterSdk.status"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/printer/epson/RealEpsonPrinterSdkKt;->access$toPrinterStatusInfo(Lcom/epson/epos2/printer/PrinterStatusInfo;)Lcom/squareup/printer/epson/PrinterStatusInfo;

    move-result-object v0

    return-object v0
.end method

.method public sendData(I)V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/printer/epson/RealEpsonPrinterSdk;->actualEpsonPrinterSdk:Lcom/epson/epos2/printer/Printer;

    invoke-virtual {v0, p1}, Lcom/epson/epos2/printer/Printer;->sendData(I)V

    return-void
.end method

.method public setReceiveEventListener(Lkotlin/jvm/functions/Function3;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Lcom/squareup/printer/epson/PrinterStatusInfo;",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "receiveEventListener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/printer/epson/RealEpsonPrinterSdk;->actualEpsonPrinterSdk:Lcom/epson/epos2/printer/Printer;

    new-instance v1, Lcom/squareup/printer/epson/RealEpsonPrinterSdk$setReceiveEventListener$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/printer/epson/RealEpsonPrinterSdk$setReceiveEventListener$1;-><init>(Lcom/squareup/printer/epson/RealEpsonPrinterSdk;Lkotlin/jvm/functions/Function3;)V

    check-cast v1, Lcom/epson/epos2/printer/ReceiveListener;

    invoke-virtual {v0, v1}, Lcom/epson/epos2/printer/Printer;->setReceiveEventListener(Lcom/epson/epos2/printer/ReceiveListener;)V

    return-void
.end method
