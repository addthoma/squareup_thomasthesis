.class final Lcom/squareup/printer/epson/EpsonPrinter$performPrint$2$sendPrintDataResult$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EpsonPrinter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/printer/epson/EpsonPrinter$performPrint$2;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/printer/epson/EpsonPrinterSdk;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "printer",
        "Lcom/squareup/printer/epson/EpsonPrinterSdk;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/printer/epson/EpsonPrinter$performPrint$2;


# direct methods
.method constructor <init>(Lcom/squareup/printer/epson/EpsonPrinter$performPrint$2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$2$sendPrintDataResult$1;->this$0:Lcom/squareup/printer/epson/EpsonPrinter$performPrint$2;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/printer/epson/EpsonPrinterSdk;

    invoke-virtual {p0, p1}, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$2$sendPrintDataResult$1;->invoke(Lcom/squareup/printer/epson/EpsonPrinterSdk;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/printer/epson/EpsonPrinterSdk;)V
    .locals 2

    const-string v0, "printer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    sget-object v0, Lcom/squareup/printer/epson/EpsonRenderRowsUtil;->INSTANCE:Lcom/squareup/printer/epson/EpsonRenderRowsUtil;

    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$2$sendPrintDataResult$1;->this$0:Lcom/squareup/printer/epson/EpsonPrinter$performPrint$2;

    iget-object v1, v1, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$2;->$renderedRows:Lcom/squareup/print/text/RenderedRows;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/printer/epson/EpsonRenderRowsUtil;->mapRenderedRowsToCommandBuffer(Lcom/squareup/printer/epson/EpsonPrinterSdk;Lcom/squareup/print/text/RenderedRows;)V

    return-void
.end method
