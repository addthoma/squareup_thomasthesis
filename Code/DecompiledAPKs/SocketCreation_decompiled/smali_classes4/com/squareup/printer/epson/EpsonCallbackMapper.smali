.class public final Lcom/squareup/printer/epson/EpsonCallbackMapper;
.super Ljava/lang/Object;
.source "EpsonCallbackMapper.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;,
        Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0008\u00c0\u0002\u0018\u00002\u00020\u0001:\u0002\t\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0008\u001a\u00020\u0006\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/printer/epson/EpsonCallbackMapper;",
        "",
        "()V",
        "classifyEpsonException",
        "Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
        "errorStatus",
        "",
        "classifyPrintResult",
        "code",
        "EpsonCallbackCode",
        "EpsonExceptionCode",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/printer/epson/EpsonCallbackMapper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 73
    new-instance v0, Lcom/squareup/printer/epson/EpsonCallbackMapper;

    invoke-direct {v0}, Lcom/squareup/printer/epson/EpsonCallbackMapper;-><init>()V

    sput-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper;->INSTANCE:Lcom/squareup/printer/epson/EpsonCallbackMapper;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final classifyEpsonException(I)Lcom/squareup/print/PrintJob$PrintAttempt$Result;
    .locals 1

    .line 75
    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->Companion:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode$Companion;->from(I)Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_FAILURE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    :goto_0
    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 80
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    sget-object p1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_UNRECOVERABLE_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    goto :goto_1

    .line 76
    :pswitch_1
    sget-object p1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_BUSY_FAILURE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    :goto_1
    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final classifyPrintResult(I)Lcom/squareup/print/PrintJob$PrintAttempt$Result;
    .locals 1

    .line 85
    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->Companion:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode$Companion;->from(I)Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->CODE_ERR_FAILURE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;

    :goto_0
    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonCallbackCode;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 104
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Printing callback code should be ignored."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 103
    :pswitch_1
    sget-object p1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->UNRELIABLE_FAILURE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    goto :goto_1

    .line 95
    :pswitch_2
    sget-object p1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_RECEIVE_BUFFER_OVERFLOW_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    goto :goto_1

    .line 93
    :pswitch_3
    sget-object p1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_PAPER_JAM_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    goto :goto_1

    .line 92
    :pswitch_4
    sget-object p1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_UNRECOVERABLE_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    goto :goto_1

    .line 91
    :pswitch_5
    sget-object p1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_PAPER_EMPTY:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    goto :goto_1

    .line 90
    :pswitch_6
    sget-object p1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_MECH_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    goto :goto_1

    .line 89
    :pswitch_7
    sget-object p1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_CUTTER_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    goto :goto_1

    .line 88
    :pswitch_8
    sget-object p1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_COVER_OPEN:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    goto :goto_1

    .line 87
    :pswitch_9
    sget-object p1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_BUSY_FAILURE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    goto :goto_1

    .line 86
    :pswitch_a
    sget-object p1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->SUCCESS:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    :goto_1
    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
