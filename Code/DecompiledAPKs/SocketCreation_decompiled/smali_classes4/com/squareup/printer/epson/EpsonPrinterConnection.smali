.class public final Lcom/squareup/printer/epson/EpsonPrinterConnection;
.super Ljava/lang/Object;
.source "EpsonPrinterConnection.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;,
        Lcom/squareup/printer/epson/EpsonPrinterConnection$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEpsonPrinterConnection.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EpsonPrinterConnection.kt\ncom/squareup/printer/epson/EpsonPrinterConnection\n*L\n1#1,313:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000n\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u0000 \'2\u00020\u0001:\u0002\'(B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J3\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0013J\u001c\u0010\u0014\u001a\u00020\u00152\n\u0010\u0016\u001a\u00060\u0017j\u0002`\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0019\u0010\u001b\u001a\u00020\u00152\u0006\u0010\u000b\u001a\u00020\u000cH\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001cJ`\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0008\u0008\u0002\u0010\u001f\u001a\u00020 2!\u0010!\u001a\u001d\u0012\u0013\u0012\u00110\u000c\u00a2\u0006\u000c\u0008#\u0012\u0008\u0008$\u0012\u0004\u0008\u0008(%\u0012\u0004\u0012\u00020\u00150\"H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010&R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/printer/epson/EpsonPrinterConnection;",
        "",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "delayedDisconnectScope",
        "Lkotlinx/coroutines/CoroutineScope;",
        "(Lcom/squareup/settings/server/Features;Lkotlinx/coroutines/CoroutineScope;)V",
        "delayedDisconnectJob",
        "Lkotlinx/coroutines/Job;",
        "connectWithRetry",
        "",
        "epsonPrinterSdk",
        "Lcom/squareup/printer/epson/EpsonPrinterSdk;",
        "epsonPrinterInfo",
        "Lcom/squareup/printer/epson/EpsonPrinterInfo;",
        "printTimingData",
        "Lcom/squareup/print/PrintTimingData;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "(Lcom/squareup/printer/epson/EpsonPrinterSdk;Lcom/squareup/printer/epson/EpsonPrinterInfo;Lcom/squareup/print/PrintTimingData;Lcom/squareup/util/Clock;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "debug",
        "",
        "e",
        "Ljava/lang/Exception;",
        "Lkotlin/Exception;",
        "message",
        "",
        "disconnectEpsonPrinter",
        "(Lcom/squareup/printer/epson/EpsonPrinterSdk;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "sendPrintData",
        "Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;",
        "delayedDisconnectDelayMs",
        "",
        "addPrintData",
        "Lkotlin/Function1;",
        "Lkotlin/ParameterName;",
        "name",
        "printer",
        "(Lcom/squareup/printer/epson/EpsonPrinterSdk;Lcom/squareup/printer/epson/EpsonPrinterInfo;Lcom/squareup/print/PrintTimingData;Lcom/squareup/util/Clock;JLkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "Companion",
        "SendPrintDataResult",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ALREADY_CONNECTED:I = -0x2

.field private static final CLEANUP_TIMEOUT_MS:J = 0x2710L

.field private static final CONNECT_RETRY_DELAYS_MS:[J

.field private static final CONNECT_TIMEOUT_MS:I = 0x3a98

.field public static final Companion:Lcom/squareup/printer/epson/EpsonPrinterConnection$Companion;

.field private static final EPSON_SDK_TIMEOUT_EXCEPTION:I = -0x2

.field private static final NO_CALLBACK_CODE:I = -0x1

.field private static final PENDING_DISCONNECT_PRINTER_DELAY_MS:J = 0x7d0L

.field private static final PRINT_RESULT_TIMEOUT_MS:J = 0xea60L

.field private static final SEND_DATA_TIMEOUT_MS:I = 0x3a98


# instance fields
.field private delayedDisconnectJob:Lkotlinx/coroutines/Job;

.field private final delayedDisconnectScope:Lkotlinx/coroutines/CoroutineScope;

.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/printer/epson/EpsonPrinterConnection$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/printer/epson/EpsonPrinterConnection;->Companion:Lcom/squareup/printer/epson/EpsonPrinterConnection$Companion;

    const/4 v0, 0x6

    new-array v0, v0, [J

    .line 304
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    const/4 v3, 0x0

    aput-wide v1, v0, v3

    .line 305
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    const/4 v3, 0x1

    aput-wide v1, v0, v3

    .line 306
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    const/4 v3, 0x2

    aput-wide v1, v0, v3

    .line 307
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x8

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    const/4 v3, 0x3

    aput-wide v1, v0, v3

    .line 308
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    const/4 v3, 0x4

    aput-wide v1, v0, v3

    .line 309
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    const/4 v3, 0x5

    aput-wide v1, v0, v3

    .line 303
    sput-object v0, Lcom/squareup/printer/epson/EpsonPrinterConnection;->CONNECT_RETRY_DELAYS_MS:[J

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/Features;Lkotlinx/coroutines/CoroutineScope;)V
    .locals 1

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "delayedDisconnectScope"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection;->delayedDisconnectScope:Lkotlinx/coroutines/CoroutineScope;

    return-void
.end method

.method public static final synthetic access$debug(Lcom/squareup/printer/epson/EpsonPrinterConnection;Ljava/lang/Exception;Ljava/lang/String;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2}, Lcom/squareup/printer/epson/EpsonPrinterConnection;->debug(Ljava/lang/Exception;Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$getCONNECT_RETRY_DELAYS_MS$cp()[J
    .locals 1

    .line 29
    sget-object v0, Lcom/squareup/printer/epson/EpsonPrinterConnection;->CONNECT_RETRY_DELAYS_MS:[J

    return-object v0
.end method

.method public static final synthetic access$getDelayedDisconnectJob$p(Lcom/squareup/printer/epson/EpsonPrinterConnection;)Lkotlinx/coroutines/Job;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection;->delayedDisconnectJob:Lkotlinx/coroutines/Job;

    return-object p0
.end method

.method public static final synthetic access$getFeatures$p(Lcom/squareup/printer/epson/EpsonPrinterConnection;)Lcom/squareup/settings/server/Features;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection;->features:Lcom/squareup/settings/server/Features;

    return-object p0
.end method

.method public static final synthetic access$setDelayedDisconnectJob$p(Lcom/squareup/printer/epson/EpsonPrinterConnection;Lkotlinx/coroutines/Job;)V
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection;->delayedDisconnectJob:Lkotlinx/coroutines/Job;

    return-void
.end method

.method private final debug(Ljava/lang/Exception;Ljava/lang/String;)V
    .locals 2

    .line 227
    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->EPSON_DEBUGGING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    check-cast p1, Ljava/lang/Throwable;

    invoke-static {p1, p2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_0
    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    .line 230
    invoke-static {p2, p1}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic sendPrintData$default(Lcom/squareup/printer/epson/EpsonPrinterConnection;Lcom/squareup/printer/epson/EpsonPrinterSdk;Lcom/squareup/printer/epson/EpsonPrinterInfo;Lcom/squareup/print/PrintTimingData;Lcom/squareup/util/Clock;JLkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 11

    and-int/lit8 v0, p9, 0x10

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x7d0

    move-wide v7, v0

    goto :goto_0

    :cond_0
    move-wide/from16 v7, p5

    :goto_0
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    .line 52
    invoke-virtual/range {v2 .. v10}, Lcom/squareup/printer/epson/EpsonPrinterConnection;->sendPrintData(Lcom/squareup/printer/epson/EpsonPrinterSdk;Lcom/squareup/printer/epson/EpsonPrinterInfo;Lcom/squareup/print/PrintTimingData;Lcom/squareup/util/Clock;JLkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final synthetic connectWithRetry(Lcom/squareup/printer/epson/EpsonPrinterSdk;Lcom/squareup/printer/epson/EpsonPrinterInfo;Lcom/squareup/print/PrintTimingData;Lcom/squareup/util/Clock;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/printer/epson/EpsonPrinterSdk;",
            "Lcom/squareup/printer/epson/EpsonPrinterInfo;",
            "Lcom/squareup/print/PrintTimingData;",
            "Lcom/squareup/util/Clock;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    move-object/from16 v0, p5

    instance-of v1, v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;

    iget v2, v1, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->label:I

    const/high16 v3, -0x80000000

    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    iget v0, v1, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->label:I

    sub-int/2addr v0, v3

    iput v0, v1, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->label:I

    move-object/from16 v2, p0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;

    move-object/from16 v2, p0

    invoke-direct {v1, v2, v0}, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;-><init>(Lcom/squareup/printer/epson/EpsonPrinterConnection;Lkotlin/coroutines/Continuation;)V

    :goto_0
    iget-object v0, v1, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->result:Ljava/lang/Object;

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v3

    .line 195
    iget v4, v1, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->label:I

    const/4 v6, 0x1

    if-eqz v4, :cond_2

    if-ne v4, v6, :cond_1

    iget v4, v1, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->I$2:I

    iget-object v7, v1, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->L$5:Ljava/lang/Object;

    check-cast v7, [J

    iget v8, v1, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->I$1:I

    iget-wide v9, v1, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->J$0:J

    iget v9, v1, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->I$0:I

    iget-object v10, v1, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->L$4:Ljava/lang/Object;

    check-cast v10, Lcom/squareup/util/Clock;

    iget-object v11, v1, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->L$3:Ljava/lang/Object;

    check-cast v11, Lcom/squareup/print/PrintTimingData;

    iget-object v12, v1, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->L$2:Ljava/lang/Object;

    check-cast v12, Lcom/squareup/printer/epson/EpsonPrinterInfo;

    iget-object v13, v1, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->L$1:Ljava/lang/Object;

    check-cast v13, Lcom/squareup/printer/epson/EpsonPrinterSdk;

    iget-object v14, v1, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->L$0:Ljava/lang/Object;

    check-cast v14, Lcom/squareup/printer/epson/EpsonPrinterConnection;

    invoke-static {v0}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    const/4 v2, 0x1

    const/4 v15, 0x0

    move-object/from16 v16, v12

    move-object v12, v1

    move-object v1, v11

    move v11, v9

    move-object v9, v13

    move-object v13, v14

    move-object v14, v3

    move-object v3, v10

    move-object/from16 v10, v16

    goto/16 :goto_2

    .line 220
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 195
    :cond_2
    invoke-static {v0}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    .line 202
    sget-object v0, Lcom/squareup/printer/epson/EpsonPrinterConnection;->CONNECT_RETRY_DELAYS_MS:[J

    array-length v4, v0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object v7, v0

    move-object v12, v1

    move-object v13, v2

    move-object v14, v3

    const/4 v8, 0x0

    const/4 v11, 0x0

    move-object/from16 v1, p3

    move-object/from16 v3, p4

    :goto_1
    if-ge v8, v4, :cond_6

    aget-wide v5, v7, v8

    if-eqz v1, :cond_3

    .line 203
    invoke-virtual {v1, v3}, Lcom/squareup/print/PrintTimingData;->connecting(Lcom/squareup/util/Clock;)V

    .line 205
    :cond_3
    :try_start_0
    invoke-virtual {v10}, Lcom/squareup/printer/epson/EpsonPrinterInfo;->getTarget()Ljava/lang/String;

    move-result-object v0

    const/16 v15, 0x3a98

    invoke-interface {v9, v0, v15}, Lcom/squareup/printer/epson/EpsonPrinterSdk;->connect(Ljava/lang/String;I)V
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    .line 210
    invoke-static {v0}, Lcom/squareup/printer/epson/EpsonPrinterExtensionsKt;->isConnectRetryableFlakyError(Lcom/epson/epos2/Epos2Exception;)Z

    move-result v15

    if-nez v15, :cond_4

    .line 211
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Aborting epson printer connect - Epos2Exception: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v15, 0x0

    new-array v1, v15, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    :cond_4
    const/4 v15, 0x0

    add-int/lit8 v11, v11, 0x1

    .line 215
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Retrying epson printer connect - retry count: "

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v15, 0x0

    new-array v2, v15, [Ljava/lang/Object;

    invoke-static {v0, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 218
    iput-object v13, v12, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->L$0:Ljava/lang/Object;

    iput-object v9, v12, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->L$1:Ljava/lang/Object;

    iput-object v10, v12, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->L$2:Ljava/lang/Object;

    iput-object v1, v12, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->L$3:Ljava/lang/Object;

    iput-object v3, v12, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->L$4:Ljava/lang/Object;

    iput v11, v12, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->I$0:I

    iput-wide v5, v12, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->J$0:J

    iput v8, v12, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->I$1:I

    iput-object v7, v12, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->L$5:Ljava/lang/Object;

    iput v4, v12, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->I$2:I

    const/4 v2, 0x1

    iput v2, v12, Lcom/squareup/printer/epson/EpsonPrinterConnection$connectWithRetry$1;->label:I

    invoke-static {v5, v6, v12}, Lkotlinx/coroutines/DelayKt;->delay(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, v14, :cond_5

    return-object v14

    :cond_5
    :goto_2
    add-int/2addr v8, v2

    const/4 v6, 0x1

    move-object/from16 v2, p0

    goto :goto_1

    .line 220
    :cond_6
    :goto_3
    invoke-static {v11}, Lkotlin/coroutines/jvm/internal/Boxing;->boxInt(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method final synthetic disconnectEpsonPrinter(Lcom/squareup/printer/epson/EpsonPrinterSdk;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/printer/epson/EpsonPrinterSdk;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    instance-of v0, p2, Lcom/squareup/printer/epson/EpsonPrinterConnection$disconnectEpsonPrinter$1;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$disconnectEpsonPrinter$1;

    iget v1, v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$disconnectEpsonPrinter$1;->label:I

    const/high16 v2, -0x80000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    iget p2, v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$disconnectEpsonPrinter$1;->label:I

    sub-int/2addr p2, v2

    iput p2, v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$disconnectEpsonPrinter$1;->label:I

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$disconnectEpsonPrinter$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/printer/epson/EpsonPrinterConnection$disconnectEpsonPrinter$1;-><init>(Lcom/squareup/printer/epson/EpsonPrinterConnection;Lkotlin/coroutines/Continuation;)V

    :goto_0
    iget-object p2, v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$disconnectEpsonPrinter$1;->result:Ljava/lang/Object;

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v1

    .line 179
    iget v2, v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$disconnectEpsonPrinter$1;->label:I

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    if-ne v2, v3, :cond_1

    iget-object p1, v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$disconnectEpsonPrinter$1;->L$1:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/printer/epson/EpsonPrinterSdk;

    iget-object p1, v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$disconnectEpsonPrinter$1;->L$0:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/printer/epson/EpsonPrinterConnection;

    :try_start_0
    invoke-static {p2}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lkotlinx/coroutines/TimeoutCancellationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception p2

    goto :goto_1

    :catch_1
    move-exception p2

    goto :goto_2

    .line 189
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 179
    :cond_2
    invoke-static {p2}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    const-wide/16 v4, 0x2710

    .line 181
    :try_start_1
    new-instance p2, Lcom/squareup/printer/epson/EpsonPrinterConnection$disconnectEpsonPrinter$2;

    const/4 v2, 0x0

    invoke-direct {p2, p1, v2}, Lcom/squareup/printer/epson/EpsonPrinterConnection$disconnectEpsonPrinter$2;-><init>(Lcom/squareup/printer/epson/EpsonPrinterSdk;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function2;

    iput-object p0, v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$disconnectEpsonPrinter$1;->L$0:Ljava/lang/Object;

    iput-object p1, v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$disconnectEpsonPrinter$1;->L$1:Ljava/lang/Object;

    iput v3, v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$disconnectEpsonPrinter$1;->label:I

    invoke-static {v4, v5, p2, v0}, Lkotlinx/coroutines/TimeoutKt;->withTimeout(JLkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lkotlinx/coroutines/TimeoutCancellationException; {:try_start_1 .. :try_end_1} :catch_2

    if-ne p1, v1, :cond_3

    return-object v1

    :catch_2
    move-exception p2

    move-object p1, p0

    .line 187
    :goto_1
    check-cast p2, Ljava/lang/Exception;

    const-string v0, "Failed to disconnect epson printer due to coroutine timeout"

    invoke-direct {p1, p2, v0}, Lcom/squareup/printer/epson/EpsonPrinterConnection;->debug(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_3

    :catch_3
    move-exception p2

    move-object p1, p0

    .line 185
    :goto_2
    move-object v0, p2

    check-cast v0, Ljava/lang/Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to disconnect epson printer: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result p2

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, v0, p2}, Lcom/squareup/printer/epson/EpsonPrinterConnection;->debug(Ljava/lang/Exception;Ljava/lang/String;)V

    .line 189
    :cond_3
    :goto_3
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final sendPrintData(Lcom/squareup/printer/epson/EpsonPrinterSdk;Lcom/squareup/printer/epson/EpsonPrinterInfo;Lcom/squareup/print/PrintTimingData;Lcom/squareup/util/Clock;JLkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 28
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/printer/epson/EpsonPrinterSdk;",
            "Lcom/squareup/printer/epson/EpsonPrinterInfo;",
            "Lcom/squareup/print/PrintTimingData;",
            "Lcom/squareup/util/Clock;",
            "J",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/printer/epson/EpsonPrinterSdk;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v0, p8

    instance-of v2, v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;

    if-eqz v2, :cond_0

    move-object v2, v0

    check-cast v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;

    iget v3, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->label:I

    const/high16 v4, -0x80000000

    and-int/2addr v3, v4

    if-eqz v3, :cond_0

    iget v0, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->label:I

    sub-int/2addr v0, v4

    iput v0, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->label:I

    goto :goto_0

    :cond_0
    new-instance v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;

    invoke-direct {v2, v1, v0}, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;-><init>(Lcom/squareup/printer/epson/EpsonPrinterConnection;Lkotlin/coroutines/Continuation;)V

    :goto_0
    iget-object v0, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->result:Ljava/lang/Object;

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v3

    .line 47
    iget v4, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->label:I

    const/4 v5, 0x3

    const/4 v6, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    if-eqz v4, :cond_4

    if-eq v4, v7, :cond_3

    if-eq v4, v6, :cond_2

    if-ne v4, v5, :cond_1

    iget-object v3, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$7:Ljava/lang/Object;

    check-cast v3, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;

    iget-object v4, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$6:Ljava/lang/Object;

    check-cast v4, Lkotlinx/coroutines/CompletableDeferred;

    iget-object v4, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$5:Ljava/lang/Object;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    iget-wide v4, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->J$0:J

    iget-object v4, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$4:Ljava/lang/Object;

    check-cast v4, Lcom/squareup/util/Clock;

    iget-object v5, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$3:Ljava/lang/Object;

    check-cast v5, Lcom/squareup/print/PrintTimingData;

    iget-object v6, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$2:Ljava/lang/Object;

    check-cast v6, Lcom/squareup/printer/epson/EpsonPrinterInfo;

    iget-object v6, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$1:Ljava/lang/Object;

    check-cast v6, Lcom/squareup/printer/epson/EpsonPrinterSdk;

    iget-object v2, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$0:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/printer/epson/EpsonPrinterConnection;

    invoke-static {v0}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    move-object v11, v3

    goto/16 :goto_5

    .line 171
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_2
    iget-object v4, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$6:Ljava/lang/Object;

    check-cast v4, Lkotlinx/coroutines/CompletableDeferred;

    iget-object v6, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$5:Ljava/lang/Object;

    check-cast v6, Lkotlin/jvm/functions/Function1;

    iget-wide v7, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->J$0:J

    iget-object v9, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$4:Ljava/lang/Object;

    check-cast v9, Lcom/squareup/util/Clock;

    iget-object v10, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$3:Ljava/lang/Object;

    check-cast v10, Lcom/squareup/print/PrintTimingData;

    iget-object v11, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$2:Ljava/lang/Object;

    check-cast v11, Lcom/squareup/printer/epson/EpsonPrinterInfo;

    iget-object v12, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$1:Ljava/lang/Object;

    check-cast v12, Lcom/squareup/printer/epson/EpsonPrinterSdk;

    iget-object v13, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$0:Ljava/lang/Object;

    check-cast v13, Lcom/squareup/printer/epson/EpsonPrinterConnection;

    invoke-static {v0}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    move-object v5, v10

    move-object v10, v4

    move-object v4, v9

    move-object v9, v11

    goto/16 :goto_2

    :cond_3
    iget-object v4, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$7:Ljava/lang/Object;

    check-cast v4, Lkotlinx/coroutines/Job;

    iget-object v4, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$6:Ljava/lang/Object;

    check-cast v4, Lkotlinx/coroutines/Job;

    iget-object v4, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$5:Ljava/lang/Object;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    iget-wide v9, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->J$0:J

    iget-object v11, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$4:Ljava/lang/Object;

    check-cast v11, Lcom/squareup/util/Clock;

    iget-object v12, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$3:Ljava/lang/Object;

    check-cast v12, Lcom/squareup/print/PrintTimingData;

    iget-object v13, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$2:Ljava/lang/Object;

    check-cast v13, Lcom/squareup/printer/epson/EpsonPrinterInfo;

    iget-object v14, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$1:Ljava/lang/Object;

    check-cast v14, Lcom/squareup/printer/epson/EpsonPrinterSdk;

    iget-object v15, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$0:Ljava/lang/Object;

    check-cast v15, Lcom/squareup/printer/epson/EpsonPrinterConnection;

    invoke-static {v0}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    move-wide/from16 v26, v9

    move-object v10, v12

    move-object v9, v13

    move-wide/from16 v12, v26

    goto :goto_1

    :cond_4
    invoke-static {v0}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    .line 58
    iget-object v0, v1, Lcom/squareup/printer/epson/EpsonPrinterConnection;->delayedDisconnectJob:Lkotlinx/coroutines/Job;

    if-eqz v0, :cond_5

    .line 59
    invoke-static {v0, v8, v7, v8}, Lkotlinx/coroutines/Job$DefaultImpls;->cancel$default(Lkotlinx/coroutines/Job;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    .line 62
    iput-object v1, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$0:Ljava/lang/Object;

    move-object/from16 v4, p1

    iput-object v4, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$1:Ljava/lang/Object;

    move-object/from16 v9, p2

    iput-object v9, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$2:Ljava/lang/Object;

    move-object/from16 v10, p3

    iput-object v10, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$3:Ljava/lang/Object;

    move-object/from16 v11, p4

    iput-object v11, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$4:Ljava/lang/Object;

    move-wide/from16 v12, p5

    iput-wide v12, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->J$0:J

    move-object/from16 v14, p7

    iput-object v14, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$5:Ljava/lang/Object;

    iput-object v0, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$6:Ljava/lang/Object;

    iput-object v0, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$7:Ljava/lang/Object;

    iput v7, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->label:I

    invoke-interface {v0, v2}, Lkotlinx/coroutines/Job;->join(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, v3, :cond_6

    return-object v3

    :cond_5
    move-object/from16 v4, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    move-wide/from16 v12, p5

    move-object/from16 v14, p7

    :cond_6
    move-object v15, v1

    move-object/from16 v26, v14

    move-object v14, v4

    move-object/from16 v4, v26

    .line 64
    :goto_1
    move-object v0, v8

    check-cast v0, Lkotlinx/coroutines/Job;

    iput-object v0, v15, Lcom/squareup/printer/epson/EpsonPrinterConnection;->delayedDisconnectJob:Lkotlinx/coroutines/Job;

    .line 66
    invoke-static {v8, v7, v8}, Lkotlinx/coroutines/CompletableDeferredKt;->CompletableDeferred$default(Lkotlinx/coroutines/Job;ILjava/lang/Object;)Lkotlinx/coroutines/CompletableDeferred;

    move-result-object v0

    const-wide/32 v7, 0xea60

    .line 67
    new-instance v25, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;

    const/16 v24, 0x0

    move-object/from16 v16, v25

    move-object/from16 v17, v15

    move-object/from16 v18, v14

    move-object/from16 v19, v0

    move-object/from16 v20, v9

    move-object/from16 v21, v10

    move-object/from16 v22, v11

    move-object/from16 v23, v4

    invoke-direct/range {v16 .. v24}, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;-><init>(Lcom/squareup/printer/epson/EpsonPrinterConnection;Lcom/squareup/printer/epson/EpsonPrinterSdk;Lkotlinx/coroutines/CompletableDeferred;Lcom/squareup/printer/epson/EpsonPrinterInfo;Lcom/squareup/print/PrintTimingData;Lcom/squareup/util/Clock;Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V

    move-object/from16 v5, v25

    check-cast v5, Lkotlin/jvm/functions/Function2;

    iput-object v15, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$0:Ljava/lang/Object;

    iput-object v14, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$1:Ljava/lang/Object;

    iput-object v9, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$2:Ljava/lang/Object;

    iput-object v10, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$3:Ljava/lang/Object;

    iput-object v11, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$4:Ljava/lang/Object;

    iput-wide v12, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->J$0:J

    iput-object v4, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$5:Ljava/lang/Object;

    iput-object v0, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$6:Ljava/lang/Object;

    iput v6, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->label:I

    invoke-static {v7, v8, v5, v2}, Lkotlinx/coroutines/TimeoutKt;->withTimeoutOrNull(JLkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v5

    if-ne v5, v3, :cond_7

    return-object v3

    :cond_7
    move-object v6, v4

    move-object v4, v11

    move-wide v7, v12

    move-object v12, v14

    move-object v13, v15

    move-object/from16 v26, v10

    move-object v10, v0

    move-object v0, v5

    move-object/from16 v5, v26

    :goto_2
    check-cast v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;

    if-eqz v0, :cond_8

    goto :goto_3

    .line 137
    :cond_8
    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;

    .line 138
    sget-object v15, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_BUSY_FAILURE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/4 v11, -0x1

    .line 140
    invoke-static {v11}, Lkotlin/coroutines/jvm/internal/Boxing;->boxInt(I)Ljava/lang/Integer;

    move-result-object v19

    const/16 v20, 0xc

    const/16 v21, 0x0

    const-string v16, ""

    move-object v14, v0

    .line 137
    invoke-direct/range {v14 .. v21}, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;-><init>(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_3
    move-object v11, v0

    .line 144
    :try_start_0
    invoke-interface {v12}, Lcom/squareup/printer/epson/EpsonPrinterSdk;->clearCommandBuffer()V

    .line 145
    invoke-interface {v12}, Lcom/squareup/printer/epson/EpsonPrinterSdk;->clearReceiveEventListener()V
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    move-exception v0

    .line 147
    move-object v14, v0

    check-cast v14, Ljava/lang/Exception;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Failed to reset the epson printer object: "

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v0

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v13, v14, v0}, Lcom/squareup/printer/epson/EpsonPrinterConnection;->debug(Ljava/lang/Exception;Ljava/lang/String;)V

    .line 150
    :goto_4
    iget-object v0, v13, Lcom/squareup/printer/epson/EpsonPrinterConnection;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->RETAIN_PRINTER_CONNECTION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 155
    iget-object v0, v13, Lcom/squareup/printer/epson/EpsonPrinterConnection;->delayedDisconnectScope:Lkotlinx/coroutines/CoroutineScope;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-instance v3, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;

    const/4 v6, 0x0

    move-object/from16 p1, v3

    move-object/from16 p2, v13

    move-wide/from16 p3, v7

    move-object/from16 p5, v9

    move-object/from16 p6, v12

    move-object/from16 p7, v6

    invoke-direct/range {p1 .. p7}, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;-><init>(Lcom/squareup/printer/epson/EpsonPrinterConnection;JLcom/squareup/printer/epson/EpsonPrinterInfo;Lcom/squareup/printer/epson/EpsonPrinterSdk;Lkotlin/coroutines/Continuation;)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    const/4 v6, 0x3

    const/4 v7, 0x0

    move-object/from16 p1, v0

    move-object/from16 p2, v1

    move-object/from16 p3, v2

    move-object/from16 p4, v3

    move/from16 p5, v6

    move-object/from16 p6, v7

    invoke-static/range {p1 .. p6}, Lkotlinx/coroutines/BuildersKt;->launch$default(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    move-result-object v0

    iput-object v0, v13, Lcom/squareup/printer/epson/EpsonPrinterConnection;->delayedDisconnectJob:Lkotlinx/coroutines/Job;

    goto :goto_5

    .line 166
    :cond_9
    iput-object v13, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$0:Ljava/lang/Object;

    iput-object v12, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$1:Ljava/lang/Object;

    iput-object v9, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$2:Ljava/lang/Object;

    iput-object v5, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$3:Ljava/lang/Object;

    iput-object v4, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$4:Ljava/lang/Object;

    iput-wide v7, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->J$0:J

    iput-object v6, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$5:Ljava/lang/Object;

    iput-object v10, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$6:Ljava/lang/Object;

    iput-object v11, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->L$7:Ljava/lang/Object;

    const/4 v1, 0x3

    iput v1, v2, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$1;->label:I

    invoke-virtual {v13, v12, v2}, Lcom/squareup/printer/epson/EpsonPrinterConnection;->disconnectEpsonPrinter(Lcom/squareup/printer/epson/EpsonPrinterSdk;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, v3, :cond_a

    return-object v3

    :cond_a
    :goto_5
    if-eqz v5, :cond_b

    .line 169
    invoke-virtual {v5, v4}, Lcom/squareup/print/PrintTimingData;->finished(Lcom/squareup/util/Clock;)V

    :cond_b
    return-object v11
.end method
