.class public final Lcom/squareup/printer/epson/PrinterStatusInfo;
.super Ljava/lang/Object;
.source "EpsonPrinterSdk.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008*\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001Bm\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\u0008\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u0003\u0012\u0006\u0010\u000b\u001a\u00020\u0003\u0012\u0006\u0010\u000c\u001a\u00020\u0003\u0012\u0006\u0010\r\u001a\u00020\u0003\u0012\u0006\u0010\u000e\u001a\u00020\u0003\u0012\u0006\u0010\u000f\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0010J\t\u0010\u001f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010 \u001a\u00020\u0003H\u00c6\u0003J\t\u0010!\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\"\u001a\u00020\u0003H\u00c6\u0003J\t\u0010#\u001a\u00020\u0003H\u00c6\u0003J\t\u0010$\u001a\u00020\u0003H\u00c6\u0003J\t\u0010%\u001a\u00020\u0003H\u00c6\u0003J\t\u0010&\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\'\u001a\u00020\u0003H\u00c6\u0003J\t\u0010(\u001a\u00020\u0003H\u00c6\u0003J\t\u0010)\u001a\u00020\u0003H\u00c6\u0003J\t\u0010*\u001a\u00020\u0003H\u00c6\u0003J\t\u0010+\u001a\u00020\u0003H\u00c6\u0003J\u008b\u0001\u0010,\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00032\u0008\u0008\u0002\u0010\t\u001a\u00020\u00032\u0008\u0008\u0002\u0010\n\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u00032\u0008\u0008\u0002\u0010\r\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010-\u001a\u00020.2\u0008\u0010/\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u00100\u001a\u00020\u0003H\u00d6\u0001J\t\u00101\u001a\u000202H\u00d6\u0001R\u0011\u0010\u000e\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u000c\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0012R\u0011\u0010\u000f\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0012R\u0011\u0010\r\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0012R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0012R\u0011\u0010\n\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0012R\u0011\u0010\u000b\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0012R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0012R\u0011\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u0012R\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u0012R\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u0012R\u0011\u0010\t\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u0012\u00a8\u00063"
    }
    d2 = {
        "Lcom/squareup/printer/epson/PrinterStatusInfo;",
        "",
        "connection",
        "",
        "online",
        "coverOpen",
        "paper",
        "paperFeed",
        "panelSwitch",
        "waitOnline",
        "drawer",
        "errorStatus",
        "autoRecoverError",
        "buzzer",
        "adapter",
        "batteryLevel",
        "(IIIIIIIIIIIII)V",
        "getAdapter",
        "()I",
        "getAutoRecoverError",
        "getBatteryLevel",
        "getBuzzer",
        "getConnection",
        "getCoverOpen",
        "getDrawer",
        "getErrorStatus",
        "getOnline",
        "getPanelSwitch",
        "getPaper",
        "getPaperFeed",
        "getWaitOnline",
        "component1",
        "component10",
        "component11",
        "component12",
        "component13",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final adapter:I

.field private final autoRecoverError:I

.field private final batteryLevel:I

.field private final buzzer:I

.field private final connection:I

.field private final coverOpen:I

.field private final drawer:I

.field private final errorStatus:I

.field private final online:I

.field private final panelSwitch:I

.field private final paper:I

.field private final paperFeed:I

.field private final waitOnline:I


# direct methods
.method public constructor <init>(IIIIIIIIIIIII)V
    .locals 0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->connection:I

    iput p2, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->online:I

    iput p3, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->coverOpen:I

    iput p4, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->paper:I

    iput p5, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->paperFeed:I

    iput p6, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->panelSwitch:I

    iput p7, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->waitOnline:I

    iput p8, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->drawer:I

    iput p9, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->errorStatus:I

    iput p10, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->autoRecoverError:I

    iput p11, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->buzzer:I

    iput p12, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->adapter:I

    iput p13, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->batteryLevel:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/printer/epson/PrinterStatusInfo;IIIIIIIIIIIIIILjava/lang/Object;)Lcom/squareup/printer/epson/PrinterStatusInfo;
    .locals 14

    move-object v0, p0

    move/from16 v1, p14

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget v2, v0, Lcom/squareup/printer/epson/PrinterStatusInfo;->connection:I

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget v3, v0, Lcom/squareup/printer/epson/PrinterStatusInfo;->online:I

    goto :goto_1

    :cond_1
    move/from16 v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget v4, v0, Lcom/squareup/printer/epson/PrinterStatusInfo;->coverOpen:I

    goto :goto_2

    :cond_2
    move/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget v5, v0, Lcom/squareup/printer/epson/PrinterStatusInfo;->paper:I

    goto :goto_3

    :cond_3
    move/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget v6, v0, Lcom/squareup/printer/epson/PrinterStatusInfo;->paperFeed:I

    goto :goto_4

    :cond_4
    move/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget v7, v0, Lcom/squareup/printer/epson/PrinterStatusInfo;->panelSwitch:I

    goto :goto_5

    :cond_5
    move/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget v8, v0, Lcom/squareup/printer/epson/PrinterStatusInfo;->waitOnline:I

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget v9, v0, Lcom/squareup/printer/epson/PrinterStatusInfo;->drawer:I

    goto :goto_7

    :cond_7
    move/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget v10, v0, Lcom/squareup/printer/epson/PrinterStatusInfo;->errorStatus:I

    goto :goto_8

    :cond_8
    move/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget v11, v0, Lcom/squareup/printer/epson/PrinterStatusInfo;->autoRecoverError:I

    goto :goto_9

    :cond_9
    move/from16 v11, p10

    :goto_9
    and-int/lit16 v12, v1, 0x400

    if-eqz v12, :cond_a

    iget v12, v0, Lcom/squareup/printer/epson/PrinterStatusInfo;->buzzer:I

    goto :goto_a

    :cond_a
    move/from16 v12, p11

    :goto_a
    and-int/lit16 v13, v1, 0x800

    if-eqz v13, :cond_b

    iget v13, v0, Lcom/squareup/printer/epson/PrinterStatusInfo;->adapter:I

    goto :goto_b

    :cond_b
    move/from16 v13, p12

    :goto_b
    and-int/lit16 v1, v1, 0x1000

    if-eqz v1, :cond_c

    iget v1, v0, Lcom/squareup/printer/epson/PrinterStatusInfo;->batteryLevel:I

    goto :goto_c

    :cond_c
    move/from16 v1, p13

    :goto_c
    move p1, v2

    move/from16 p2, v3

    move/from16 p3, v4

    move/from16 p4, v5

    move/from16 p5, v6

    move/from16 p6, v7

    move/from16 p7, v8

    move/from16 p8, v9

    move/from16 p9, v10

    move/from16 p10, v11

    move/from16 p11, v12

    move/from16 p12, v13

    move/from16 p13, v1

    invoke-virtual/range {p0 .. p13}, Lcom/squareup/printer/epson/PrinterStatusInfo;->copy(IIIIIIIIIIIII)Lcom/squareup/printer/epson/PrinterStatusInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->connection:I

    return v0
.end method

.method public final component10()I
    .locals 1

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->autoRecoverError:I

    return v0
.end method

.method public final component11()I
    .locals 1

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->buzzer:I

    return v0
.end method

.method public final component12()I
    .locals 1

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->adapter:I

    return v0
.end method

.method public final component13()I
    .locals 1

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->batteryLevel:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->online:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->coverOpen:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->paper:I

    return v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->paperFeed:I

    return v0
.end method

.method public final component6()I
    .locals 1

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->panelSwitch:I

    return v0
.end method

.method public final component7()I
    .locals 1

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->waitOnline:I

    return v0
.end method

.method public final component8()I
    .locals 1

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->drawer:I

    return v0
.end method

.method public final component9()I
    .locals 1

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->errorStatus:I

    return v0
.end method

.method public final copy(IIIIIIIIIIIII)Lcom/squareup/printer/epson/PrinterStatusInfo;
    .locals 15

    new-instance v14, Lcom/squareup/printer/epson/PrinterStatusInfo;

    move-object v0, v14

    move/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    move/from16 v12, p12

    move/from16 v13, p13

    invoke-direct/range {v0 .. v13}, Lcom/squareup/printer/epson/PrinterStatusInfo;-><init>(IIIIIIIIIIIII)V

    return-object v14
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/printer/epson/PrinterStatusInfo;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/printer/epson/PrinterStatusInfo;

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->connection:I

    iget v1, p1, Lcom/squareup/printer/epson/PrinterStatusInfo;->connection:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->online:I

    iget v1, p1, Lcom/squareup/printer/epson/PrinterStatusInfo;->online:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->coverOpen:I

    iget v1, p1, Lcom/squareup/printer/epson/PrinterStatusInfo;->coverOpen:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->paper:I

    iget v1, p1, Lcom/squareup/printer/epson/PrinterStatusInfo;->paper:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->paperFeed:I

    iget v1, p1, Lcom/squareup/printer/epson/PrinterStatusInfo;->paperFeed:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->panelSwitch:I

    iget v1, p1, Lcom/squareup/printer/epson/PrinterStatusInfo;->panelSwitch:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->waitOnline:I

    iget v1, p1, Lcom/squareup/printer/epson/PrinterStatusInfo;->waitOnline:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->drawer:I

    iget v1, p1, Lcom/squareup/printer/epson/PrinterStatusInfo;->drawer:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->errorStatus:I

    iget v1, p1, Lcom/squareup/printer/epson/PrinterStatusInfo;->errorStatus:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->autoRecoverError:I

    iget v1, p1, Lcom/squareup/printer/epson/PrinterStatusInfo;->autoRecoverError:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->buzzer:I

    iget v1, p1, Lcom/squareup/printer/epson/PrinterStatusInfo;->buzzer:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->adapter:I

    iget v1, p1, Lcom/squareup/printer/epson/PrinterStatusInfo;->adapter:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->batteryLevel:I

    iget p1, p1, Lcom/squareup/printer/epson/PrinterStatusInfo;->batteryLevel:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAdapter()I
    .locals 1

    .line 83
    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->adapter:I

    return v0
.end method

.method public final getAutoRecoverError()I
    .locals 1

    .line 81
    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->autoRecoverError:I

    return v0
.end method

.method public final getBatteryLevel()I
    .locals 1

    .line 84
    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->batteryLevel:I

    return v0
.end method

.method public final getBuzzer()I
    .locals 1

    .line 82
    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->buzzer:I

    return v0
.end method

.method public final getConnection()I
    .locals 1

    .line 72
    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->connection:I

    return v0
.end method

.method public final getCoverOpen()I
    .locals 1

    .line 74
    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->coverOpen:I

    return v0
.end method

.method public final getDrawer()I
    .locals 1

    .line 79
    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->drawer:I

    return v0
.end method

.method public final getErrorStatus()I
    .locals 1

    .line 80
    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->errorStatus:I

    return v0
.end method

.method public final getOnline()I
    .locals 1

    .line 73
    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->online:I

    return v0
.end method

.method public final getPanelSwitch()I
    .locals 1

    .line 77
    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->panelSwitch:I

    return v0
.end method

.method public final getPaper()I
    .locals 1

    .line 75
    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->paper:I

    return v0
.end method

.method public final getPaperFeed()I
    .locals 1

    .line 76
    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->paperFeed:I

    return v0
.end method

.method public final getWaitOnline()I
    .locals 1

    .line 78
    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->waitOnline:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->connection:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->online:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->coverOpen:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->paper:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->paperFeed:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->panelSwitch:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->waitOnline:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->drawer:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->errorStatus:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->autoRecoverError:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->buzzer:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->adapter:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->batteryLevel:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PrinterStatusInfo(connection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->connection:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", online="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->online:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", coverOpen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->coverOpen:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", paper="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->paper:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", paperFeed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->paperFeed:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", panelSwitch="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->panelSwitch:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", waitOnline="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->waitOnline:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", drawer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->drawer:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", errorStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->errorStatus:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", autoRecoverError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->autoRecoverError:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", buzzer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->buzzer:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", adapter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->adapter:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", batteryLevel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/printer/epson/PrinterStatusInfo;->batteryLevel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
