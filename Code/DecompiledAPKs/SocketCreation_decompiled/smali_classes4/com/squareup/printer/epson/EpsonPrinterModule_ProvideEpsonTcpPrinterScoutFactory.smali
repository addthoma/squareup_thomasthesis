.class public final Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonTcpPrinterScoutFactory;
.super Ljava/lang/Object;
.source "EpsonPrinterModule_ProvideEpsonTcpPrinterScoutFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/EpsonTcpPrinterScout;",
        ">;"
    }
.end annotation


# instance fields
.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final epsonDiscovererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/printer/epson/EpsonDiscoverer;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/printer/epson/EpsonDiscoverer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonTcpPrinterScoutFactory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonTcpPrinterScoutFactory;->epsonDiscovererProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonTcpPrinterScoutFactory;->clockProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonTcpPrinterScoutFactory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonTcpPrinterScoutFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/printer/epson/EpsonDiscoverer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonTcpPrinterScoutFactory;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonTcpPrinterScoutFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonTcpPrinterScoutFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideEpsonTcpPrinterScout(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/printer/epson/EpsonDiscoverer;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;)Lcom/squareup/print/EpsonTcpPrinterScout;
    .locals 0

    .line 51
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/printer/epson/EpsonPrinterModule;->provideEpsonTcpPrinterScout(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/printer/epson/EpsonDiscoverer;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;)Lcom/squareup/print/EpsonTcpPrinterScout;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/EpsonTcpPrinterScout;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/print/EpsonTcpPrinterScout;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonTcpPrinterScoutFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonTcpPrinterScoutFactory;->epsonDiscovererProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/printer/epson/EpsonDiscoverer;

    iget-object v2, p0, Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonTcpPrinterScoutFactory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Clock;

    iget-object v3, p0, Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonTcpPrinterScoutFactory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonTcpPrinterScoutFactory;->provideEpsonTcpPrinterScout(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/printer/epson/EpsonDiscoverer;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;)Lcom/squareup/print/EpsonTcpPrinterScout;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonTcpPrinterScoutFactory;->get()Lcom/squareup/print/EpsonTcpPrinterScout;

    move-result-object v0

    return-object v0
.end method
