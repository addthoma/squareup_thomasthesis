.class public final Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "StandardRowSpec.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "TS;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStandardRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$Creator$bind$1\n+ 2 SalesReportStandardRowSpecs.kt\ncom/squareup/salesreport/util/SalesReportStandardRowSpecsKt\n*L\n1#1,87:1\n151#2,57:88\n252#2:145\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0006\"\u0008\u0008\u0003\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0004\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0005\u0010\u0005*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u0002H\u0004H\n\u00a2\u0006\u0004\u0008\n\u0010\u000b\u00a8\u0006\r"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "<anonymous parameter 0>",
        "",
        "item",
        "invoke",
        "(ILjava/lang/Object;)V",
        "com/squareup/cycler/StandardRowSpec$Creator$bind$1",
        "com/squareup/salesreport/util/SalesReportStandardRowSpecsKt$$special$$inlined$bind$5"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

.field final synthetic this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;


# direct methods
.method public constructor <init>(Lcom/squareup/cycler/StandardRowSpec$Creator;Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    iput-object p2, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->invoke(ILjava/lang/Object;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILjava/lang/Object;)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITS;)V"
        }
    .end annotation

    move-object/from16 v12, p0

    move-object/from16 v0, p2

    const-string v1, "item"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    move-object v9, v0

    check-cast v9, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;

    .line 89
    invoke-virtual {v9}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    iget-object v1, v12, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v1, v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$res$inlined:Lcom/squareup/util/Res;

    iget-object v2, v12, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v2, v2, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$localeProvider$inlined:Ljavax/inject/Provider;

    iget-object v3, v12, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v3, v3, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$localTimeFormatter$inlined:Lcom/squareup/salesreport/util/LocalTimeFormatter;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->access$currentLabel(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/salesreport/util/LocalTimeFormatter;)Ljava/lang/String;

    move-result-object v10

    .line 90
    invoke-virtual {v9}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    iget-object v1, v12, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v1, v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$res$inlined:Lcom/squareup/util/Res;

    iget-object v2, v12, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v2, v2, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$localeProvider$inlined:Ljavax/inject/Provider;

    invoke-static {v0, v1, v2}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->access$compareLabel(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/util/Res;Ljavax/inject/Provider;)Ljava/lang/String;

    move-result-object v11

    .line 93
    invoke-virtual {v9}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getSalesSummaryReport()Lcom/squareup/customreport/data/WithSalesSummaryReport;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/customreport/data/WithSalesSummaryReport;->getSalesSummary()Lcom/squareup/customreport/data/SalesSummary;

    move-result-object v0

    .line 94
    invoke-virtual {v9}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getChartSalesSelection()Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    move-result-object v1

    .line 95
    iget-object v2, v12, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v2, v2, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$exactValueMoneyFormatter$inlined:Lcom/squareup/text/Formatter;

    .line 96
    iget-object v3, v12, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v3, v3, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$exactValueNumberFormatter$inlined:Lcom/squareup/text/Formatter;

    .line 93
    invoke-static {v0, v1, v2, v3}, Lcom/squareup/salesreport/util/SalesSummariesKt;->textFromChartSalesSelection(Lcom/squareup/customreport/data/SalesSummary;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object v13

    .line 100
    invoke-virtual {v9}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getSalesSummaryReport()Lcom/squareup/customreport/data/WithSalesSummaryReport;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/customreport/data/WithSalesSummaryReport;->getComparisonSalesSummary()Lcom/squareup/customreport/data/SalesSummary;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 101
    invoke-virtual {v9}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getChartSalesSelection()Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    move-result-object v1

    .line 102
    iget-object v2, v12, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v2, v2, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$exactValueMoneyFormatter$inlined:Lcom/squareup/text/Formatter;

    .line 103
    iget-object v3, v12, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v3, v3, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$exactValueNumberFormatter$inlined:Lcom/squareup/text/Formatter;

    .line 100
    invoke-static {v0, v1, v2, v3}, Lcom/squareup/salesreport/util/SalesSummariesKt;->textFromChartSalesSelection(Lcom/squareup/customreport/data/SalesSummary;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v14, v0

    .line 106
    invoke-virtual {v9}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection;

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    .line 107
    iget-object v2, v12, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {v2}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    sget v3, Lcom/squareup/salesreport/impl/R$id;->current_label:I

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 108
    move-object v15, v2

    check-cast v15, Landroid/widget/TextView;

    .line 109
    move-object v2, v10

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v15, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v2, v12, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {v2}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    sget v3, Lcom/squareup/salesreport/impl/R$id;->compare_label:I

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 112
    move-object v8, v2

    check-cast v8, Landroid/widget/TextView;

    .line 113
    move-object v2, v8

    check-cast v2, Landroid/view/View;

    invoke-static {v2, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 114
    move-object v2, v11

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v8, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v2, v12, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {v2}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    sget v3, Lcom/squareup/salesreport/impl/R$id;->current_value:I

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 117
    move-object v7, v2

    check-cast v7, Landroid/widget/TextView;

    .line 118
    invoke-virtual {v7, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v2, v12, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {v2}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    sget v3, Lcom/squareup/salesreport/impl/R$id;->compare_value:I

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 121
    move-object v6, v2

    check-cast v6, Landroid/widget/TextView;

    .line 122
    invoke-virtual {v6, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    move-object v2, v6

    check-cast v2, Landroid/view/View;

    invoke-static {v2, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 126
    invoke-virtual {v9}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getSalesChart()Lcom/squareup/customreport/data/WithSalesChartReport;

    move-result-object v0

    .line 127
    iget-object v2, v12, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {v2}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    sget v3, Lcom/squareup/salesreport/impl/R$id;->sales_report_chart_animator:I

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const-string/jumbo v3, "view.findViewById(R.id.s\u2026es_report_chart_animator)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/widgets/SquareViewAnimator;

    .line 126
    invoke-static {v0, v2}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->access$pickChartView(Lcom/squareup/customreport/data/WithSalesChartReport;Lcom/squareup/widgets/SquareViewAnimator;)Lcom/squareup/chartography/ChartView;

    move-result-object v0

    .line 130
    iget-object v2, v12, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v3, v2, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$localeProvider$inlined:Ljavax/inject/Provider;

    .line 132
    iget-object v2, v12, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v5, v2, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$rangeMoneyFormatter$inlined:Lcom/squareup/text/Formatter;

    .line 133
    iget-object v2, v12, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v4, v2, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$rangeNumberFormatter$inlined:Lcom/squareup/text/Formatter;

    .line 134
    iget-object v2, v12, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v2, v2, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$currencyCode$inlined:Lcom/squareup/protos/common/CurrencyCode;

    .line 135
    iget-object v1, v12, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v1, v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$current24HourClockMode$inlined:Lcom/squareup/time/Current24HourClockMode;

    move-object/from16 v16, v2

    move-object v2, v0

    move-object/from16 v17, v4

    move-object v4, v9

    move-object/from16 v18, v6

    move-object/from16 v6, v17

    move-object/from16 v17, v7

    move-object/from16 v7, v16

    move-object/from16 v16, v8

    move-object v8, v1

    .line 129
    invoke-static/range {v2 .. v8}, Lcom/squareup/salesreport/chart/ChartViewsKt;->salesAdapterFromRow(Lcom/squareup/chartography/ChartView;Ljavax/inject/Provider;Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/time/Current24HourClockMode;)Lcom/squareup/salesreport/chart/SalesAdapter;

    move-result-object v3

    const/4 v1, -0x1

    .line 137
    invoke-virtual {v0, v1}, Lcom/squareup/chartography/ChartView;->setXLabelFrquency(I)V

    .line 138
    move-object v1, v3

    check-cast v1, Lcom/squareup/chartography/ChartAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/chartography/ChartView;->setAdapter(Lcom/squareup/chartography/ChartAdapter;)V

    .line 140
    invoke-virtual {v3}, Lcom/squareup/salesreport/chart/SalesAdapter;->getRangeNumSteps()I

    move-result v1

    if-lez v1, :cond_2

    const/4 v1, 0x0

    .line 141
    invoke-virtual {v3, v1}, Lcom/squareup/salesreport/chart/SalesAdapter;->rangeStepValue(I)D

    move-result-wide v4

    int-to-double v6, v1

    cmpl-double v2, v4, v6

    if-ltz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-virtual {v0, v1}, Lcom/squareup/chartography/ChartView;->setHideFirstYLabel(Z)V

    .line 144
    :cond_2
    new-instance v19, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;

    move-object v8, v0

    move-object/from16 v0, v19

    move-object v1, v15

    move-object v2, v9

    move-object/from16 v4, v16

    move-object/from16 v5, v17

    move-object/from16 v6, v18

    move-object v7, v10

    move-object v15, v8

    move-object v8, v11

    move-object v9, v13

    move-object v10, v14

    move-object/from16 v11, p0

    invoke-direct/range {v0 .. v11}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;-><init>(Landroid/widget/TextView;Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;Lcom/squareup/salesreport/chart/SalesAdapter;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;)V

    move-object/from16 v0, v19

    check-cast v0, Lcom/squareup/chartography/ChartView$ScrubbingListener;

    invoke-virtual {v15, v0}, Lcom/squareup/chartography/ChartView;->setScrubbingListener(Lcom/squareup/chartography/ChartView$ScrubbingListener;)V

    return-void
.end method
