.class final Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesOverviewRows$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SalesSummaryReports.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/util/SalesSummaryReportsKt;->salesOverviewRows(Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/salesreport/util/PercentageChangeFormatter;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Long;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000e\n\u0002\u0010\t\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001*\u0004\u0018\u00010\u0002H\n\u00a2\u0006\u0004\u0008\u0003\u0010\u0004"
    }
    d2 = {
        "format",
        "",
        "",
        "invoke",
        "(Ljava/lang/Long;)Ljava/lang/String;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $compactNumberFormatter:Lcom/squareup/text/Formatter;


# direct methods
.method constructor <init>(Lcom/squareup/text/Formatter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesOverviewRows$1;->$compactNumberFormatter:Lcom/squareup/text/Formatter;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesOverviewRows$1;->invoke(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Ljava/lang/Long;)Ljava/lang/String;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesOverviewRows$1;->$compactNumberFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
