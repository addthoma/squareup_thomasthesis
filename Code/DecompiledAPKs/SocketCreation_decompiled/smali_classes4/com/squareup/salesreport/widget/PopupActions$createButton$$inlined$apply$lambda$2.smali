.class public final Lcom/squareup/salesreport/widget/PopupActions$createButton$$inlined$apply$lambda$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/widget/PopupActions;->createButton(Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function0;Lcom/squareup/noho/NohoButtonType;I)Lcom/squareup/noho/NohoButton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 PopupActions.kt\ncom/squareup/salesreport/widget/PopupActions\n*L\n1#1,1322:1\n97#2,4:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0007"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release",
        "com/squareup/salesreport/widget/PopupActions$$special$$inlined$onClickDebounced$1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $action$inlined:Lkotlin/jvm/functions/Function0;

.field final synthetic $buttonEdges$inlined:I

.field final synthetic $label$inlined:Ljava/lang/String;

.field final synthetic $type$inlined:Lcom/squareup/noho/NohoButtonType;

.field final synthetic this$0:Lcom/squareup/salesreport/widget/PopupActions;


# direct methods
.method public constructor <init>(Lcom/squareup/salesreport/widget/PopupActions;Lcom/squareup/noho/NohoButtonType;Ljava/lang/String;ILkotlin/jvm/functions/Function0;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/widget/PopupActions$createButton$$inlined$apply$lambda$2;->this$0:Lcom/squareup/salesreport/widget/PopupActions;

    iput-object p2, p0, Lcom/squareup/salesreport/widget/PopupActions$createButton$$inlined$apply$lambda$2;->$type$inlined:Lcom/squareup/noho/NohoButtonType;

    iput-object p3, p0, Lcom/squareup/salesreport/widget/PopupActions$createButton$$inlined$apply$lambda$2;->$label$inlined:Ljava/lang/String;

    iput p4, p0, Lcom/squareup/salesreport/widget/PopupActions$createButton$$inlined$apply$lambda$2;->$buttonEdges$inlined:I

    iput-object p5, p0, Lcom/squareup/salesreport/widget/PopupActions$createButton$$inlined$apply$lambda$2;->$action$inlined:Lkotlin/jvm/functions/Function0;

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1106
    check-cast p1, Lcom/squareup/noho/NohoButton;

    .line 1323
    iget-object p1, p0, Lcom/squareup/salesreport/widget/PopupActions$createButton$$inlined$apply$lambda$2;->$action$inlined:Lkotlin/jvm/functions/Function0;

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 1324
    iget-object p1, p0, Lcom/squareup/salesreport/widget/PopupActions$createButton$$inlined$apply$lambda$2;->this$0:Lcom/squareup/salesreport/widget/PopupActions;

    invoke-static {p1}, Lcom/squareup/salesreport/widget/PopupActions;->access$getPopupWindow$p(Lcom/squareup/salesreport/widget/PopupActions;)Landroid/widget/PopupWindow;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1325
    :cond_0
    iget-object p1, p0, Lcom/squareup/salesreport/widget/PopupActions$createButton$$inlined$apply$lambda$2;->this$0:Lcom/squareup/salesreport/widget/PopupActions;

    invoke-static {p1}, Lcom/squareup/salesreport/widget/PopupActions;->access$getAlertDialog$p(Lcom/squareup/salesreport/widget/PopupActions;)Landroid/app/AlertDialog;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/app/AlertDialog;->dismiss()V

    :cond_1
    return-void
.end method
