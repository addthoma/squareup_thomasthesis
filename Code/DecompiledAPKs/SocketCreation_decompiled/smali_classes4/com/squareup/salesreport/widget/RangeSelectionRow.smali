.class public final Lcom/squareup/salesreport/widget/RangeSelectionRow;
.super Landroid/widget/FrameLayout;
.source "RangeSelectionRow.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRangeSelectionRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RangeSelectionRow.kt\ncom/squareup/salesreport/widget/RangeSelectionRow\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,84:1\n1103#2,7:85\n1103#2,7:92\n1103#2,7:99\n1103#2,7:106\n1103#2,7:113\n*E\n*S KotlinDebug\n*F\n+ 1 RangeSelectionRow.kt\ncom/squareup/salesreport/widget/RangeSelectionRow\n*L\n46#1,7:85\n49#1,7:92\n52#1,7:99\n55#1,7:106\n58#1,7:113\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\u0013\u001a\u00020\u000cH\u0002J\u0018\u0010\u0014\u001a\u00020\u000c2\u0006\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u001a\u0010\u0018\u001a\u00020\u000c2\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000c0\nJ\u000e\u0010\u0019\u001a\u00020\u000c2\u0006\u0010\u001a\u001a\u00020\u001bR\u001a\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000c0\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/salesreport/widget/RangeSelectionRow;",
        "Landroid/widget/FrameLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "rangeSelectionHandler",
        "Lkotlin/Function1;",
        "Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;",
        "",
        "timeLabel1D",
        "Lcom/squareup/noho/NohoLabel;",
        "timeLabel1M",
        "timeLabel1W",
        "timeLabel1Y",
        "timeLabel3M",
        "bindViews",
        "resetEdgeForLabel",
        "label",
        "selected",
        "",
        "setRangeSelectionHandler",
        "setSelectedTime",
        "rangeSelection",
        "Lcom/squareup/customreport/data/RangeSelection;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private rangeSelectionHandler:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private timeLabel1D:Lcom/squareup/noho/NohoLabel;

.field private timeLabel1M:Lcom/squareup/noho/NohoLabel;

.field private timeLabel1W:Lcom/squareup/noho/NohoLabel;

.field private timeLabel1Y:Lcom/squareup/noho/NohoLabel;

.field private timeLabel3M:Lcom/squareup/noho/NohoLabel;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/salesreport/widget/RangeSelectionRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/salesreport/widget/RangeSelectionRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    sget p2, Lcom/squareup/salesreport/impl/R$layout;->sales_report_time_selector_view:I

    move-object p3, p0

    check-cast p3, Landroid/view/ViewGroup;

    invoke-static {p1, p2, p3}, Landroid/widget/FrameLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 36
    invoke-direct {p0}, Lcom/squareup/salesreport/widget/RangeSelectionRow;->bindViews()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 22
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 23
    sget p3, Lcom/squareup/noho/R$attr;->sqLabelStyle:I

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/salesreport/widget/RangeSelectionRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$getRangeSelectionHandler$p(Lcom/squareup/salesreport/widget/RangeSelectionRow;)Lkotlin/jvm/functions/Function1;
    .locals 1

    .line 20
    iget-object p0, p0, Lcom/squareup/salesreport/widget/RangeSelectionRow;->rangeSelectionHandler:Lkotlin/jvm/functions/Function1;

    if-nez p0, :cond_0

    const-string v0, "rangeSelectionHandler"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setRangeSelectionHandler$p(Lcom/squareup/salesreport/widget/RangeSelectionRow;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    .line 20
    iput-object p1, p0, Lcom/squareup/salesreport/widget/RangeSelectionRow;->rangeSelectionHandler:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method private final bindViews()V
    .locals 2

    .line 40
    sget v0, Lcom/squareup/salesreport/impl/R$id;->sales_report_time_selector_1d:I

    invoke-virtual {p0, v0}, Lcom/squareup/salesreport/widget/RangeSelectionRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.sales_report_time_selector_1d)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoLabel;

    iput-object v0, p0, Lcom/squareup/salesreport/widget/RangeSelectionRow;->timeLabel1D:Lcom/squareup/noho/NohoLabel;

    .line 41
    sget v0, Lcom/squareup/salesreport/impl/R$id;->sales_report_time_selector_1w:I

    invoke-virtual {p0, v0}, Lcom/squareup/salesreport/widget/RangeSelectionRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.sales_report_time_selector_1w)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoLabel;

    iput-object v0, p0, Lcom/squareup/salesreport/widget/RangeSelectionRow;->timeLabel1W:Lcom/squareup/noho/NohoLabel;

    .line 42
    sget v0, Lcom/squareup/salesreport/impl/R$id;->sales_report_time_selector_1m:I

    invoke-virtual {p0, v0}, Lcom/squareup/salesreport/widget/RangeSelectionRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.sales_report_time_selector_1m)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoLabel;

    iput-object v0, p0, Lcom/squareup/salesreport/widget/RangeSelectionRow;->timeLabel1M:Lcom/squareup/noho/NohoLabel;

    .line 43
    sget v0, Lcom/squareup/salesreport/impl/R$id;->sales_report_time_selector_3m:I

    invoke-virtual {p0, v0}, Lcom/squareup/salesreport/widget/RangeSelectionRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.sales_report_time_selector_3m)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoLabel;

    iput-object v0, p0, Lcom/squareup/salesreport/widget/RangeSelectionRow;->timeLabel3M:Lcom/squareup/noho/NohoLabel;

    .line 44
    sget v0, Lcom/squareup/salesreport/impl/R$id;->sales_report_time_selector_1y:I

    invoke-virtual {p0, v0}, Lcom/squareup/salesreport/widget/RangeSelectionRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.sales_report_time_selector_1y)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoLabel;

    iput-object v0, p0, Lcom/squareup/salesreport/widget/RangeSelectionRow;->timeLabel1Y:Lcom/squareup/noho/NohoLabel;

    .line 46
    iget-object v0, p0, Lcom/squareup/salesreport/widget/RangeSelectionRow;->timeLabel1D:Lcom/squareup/noho/NohoLabel;

    if-nez v0, :cond_0

    const-string v1, "timeLabel1D"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    .line 85
    new-instance v1, Lcom/squareup/salesreport/widget/RangeSelectionRow$bindViews$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0}, Lcom/squareup/salesreport/widget/RangeSelectionRow$bindViews$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/salesreport/widget/RangeSelectionRow;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/salesreport/widget/RangeSelectionRow;->timeLabel1W:Lcom/squareup/noho/NohoLabel;

    if-nez v0, :cond_1

    const-string v1, "timeLabel1W"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    .line 92
    new-instance v1, Lcom/squareup/salesreport/widget/RangeSelectionRow$bindViews$$inlined$onClickDebounced$2;

    invoke-direct {v1, p0}, Lcom/squareup/salesreport/widget/RangeSelectionRow$bindViews$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/salesreport/widget/RangeSelectionRow;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/salesreport/widget/RangeSelectionRow;->timeLabel1M:Lcom/squareup/noho/NohoLabel;

    if-nez v0, :cond_2

    const-string v1, "timeLabel1M"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v0, Landroid/view/View;

    .line 99
    new-instance v1, Lcom/squareup/salesreport/widget/RangeSelectionRow$bindViews$$inlined$onClickDebounced$3;

    invoke-direct {v1, p0}, Lcom/squareup/salesreport/widget/RangeSelectionRow$bindViews$$inlined$onClickDebounced$3;-><init>(Lcom/squareup/salesreport/widget/RangeSelectionRow;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/salesreport/widget/RangeSelectionRow;->timeLabel3M:Lcom/squareup/noho/NohoLabel;

    if-nez v0, :cond_3

    const-string v1, "timeLabel3M"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v0, Landroid/view/View;

    .line 106
    new-instance v1, Lcom/squareup/salesreport/widget/RangeSelectionRow$bindViews$$inlined$onClickDebounced$4;

    invoke-direct {v1, p0}, Lcom/squareup/salesreport/widget/RangeSelectionRow$bindViews$$inlined$onClickDebounced$4;-><init>(Lcom/squareup/salesreport/widget/RangeSelectionRow;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/salesreport/widget/RangeSelectionRow;->timeLabel1Y:Lcom/squareup/noho/NohoLabel;

    if-nez v0, :cond_4

    const-string v1, "timeLabel1Y"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v0, Landroid/view/View;

    .line 113
    new-instance v1, Lcom/squareup/salesreport/widget/RangeSelectionRow$bindViews$$inlined$onClickDebounced$5;

    invoke-direct {v1, p0}, Lcom/squareup/salesreport/widget/RangeSelectionRow$bindViews$$inlined$onClickDebounced$5;-><init>(Lcom/squareup/salesreport/widget/RangeSelectionRow;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final resetEdgeForLabel(Lcom/squareup/noho/NohoLabel;Z)V
    .locals 4

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    const/16 p2, 0x8

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 81
    :goto_0
    new-instance v1, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;

    const/4 v2, -0x1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v1, v0, v2, v3, p2}, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;-><init>(IIFI)V

    check-cast v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoLabel;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public final setRangeSelectionHandler(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "rangeSelectionHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    iput-object p1, p0, Lcom/squareup/salesreport/widget/RangeSelectionRow;->rangeSelectionHandler:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setSelectedTime(Lcom/squareup/customreport/data/RangeSelection;)V
    .locals 2

    const-string v0, "rangeSelection"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/salesreport/widget/RangeSelectionRow;->timeLabel1D:Lcom/squareup/noho/NohoLabel;

    if-nez v0, :cond_0

    const-string v1, "timeLabel1D"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget-object v1, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneDay;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneDay;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/salesreport/widget/RangeSelectionRow;->resetEdgeForLabel(Lcom/squareup/noho/NohoLabel;Z)V

    .line 69
    iget-object v0, p0, Lcom/squareup/salesreport/widget/RangeSelectionRow;->timeLabel1W:Lcom/squareup/noho/NohoLabel;

    if-nez v0, :cond_1

    const-string v1, "timeLabel1W"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget-object v1, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneWeek;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneWeek;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/salesreport/widget/RangeSelectionRow;->resetEdgeForLabel(Lcom/squareup/noho/NohoLabel;Z)V

    .line 70
    iget-object v0, p0, Lcom/squareup/salesreport/widget/RangeSelectionRow;->timeLabel1M:Lcom/squareup/noho/NohoLabel;

    if-nez v0, :cond_2

    const-string v1, "timeLabel1M"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget-object v1, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneMonth;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneMonth;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/salesreport/widget/RangeSelectionRow;->resetEdgeForLabel(Lcom/squareup/noho/NohoLabel;Z)V

    .line 71
    iget-object v0, p0, Lcom/squareup/salesreport/widget/RangeSelectionRow;->timeLabel3M:Lcom/squareup/noho/NohoLabel;

    if-nez v0, :cond_3

    const-string v1, "timeLabel3M"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget-object v1, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/salesreport/widget/RangeSelectionRow;->resetEdgeForLabel(Lcom/squareup/noho/NohoLabel;Z)V

    .line 72
    iget-object v0, p0, Lcom/squareup/salesreport/widget/RangeSelectionRow;->timeLabel1Y:Lcom/squareup/noho/NohoLabel;

    if-nez v0, :cond_4

    const-string v1, "timeLabel1Y"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    sget-object v1, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneYear;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneYear;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/salesreport/widget/RangeSelectionRow;->resetEdgeForLabel(Lcom/squareup/noho/NohoLabel;Z)V

    return-void
.end method
