.class public final Lcom/squareup/radiography/FocusedActivityScanner;
.super Ljava/lang/Object;
.source "FocusedActivityScanner.java"


# instance fields
.field private focusedRootView:Landroid/view/View;

.field private final skippedIds:[I


# direct methods
.method public varargs constructor <init>([I)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/radiography/FocusedActivityScanner;->skippedIds:[I

    return-void
.end method


# virtual methods
.method public resetFocusedActivity()V
    .locals 1

    const/4 v0, 0x0

    .line 23
    invoke-virtual {p0, v0}, Lcom/squareup/radiography/FocusedActivityScanner;->setFocusedActivity(Landroid/app/Activity;)V

    return-void
.end method

.method public scanFocusedActivity()Ljava/lang/String;
    .locals 2

    .line 35
    iget-object v0, p0, Lcom/squareup/radiography/FocusedActivityScanner;->focusedRootView:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v0, "No focused root view"

    return-object v0

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/squareup/radiography/FocusedActivityScanner;->skippedIds:[I

    invoke-static {v0}, Lcom/squareup/radiography/Xrays;->withSkippedIds([I)Lcom/squareup/radiography/Xrays;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/radiography/FocusedActivityScanner;->focusedRootView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/squareup/radiography/Xrays;->scan(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setFocusedActivity(Landroid/app/Activity;)V
    .locals 1

    if-eqz p1, :cond_1

    .line 27
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 31
    :cond_0
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/radiography/FocusedActivityScanner;->focusedRootView:Landroid/view/View;

    return-void

    :cond_1
    :goto_0
    const/4 p1, 0x0

    .line 28
    iput-object p1, p0, Lcom/squareup/radiography/FocusedActivityScanner;->focusedRootView:Landroid/view/View;

    return-void
.end method
