.class public abstract Lcom/squareup/print/RegisterPrintModule$Prod;
.super Ljava/lang/Object;
.source "RegisterPrintModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/RegisterPrintModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Prod"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideBlockedPrinterLogRunner(Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/print/BlockedPrinterLogRunner;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 51
    new-instance v0, Lcom/squareup/print/RealBlockedPrinterLogRunner;

    invoke-direct {v0, p0}, Lcom/squareup/print/RealBlockedPrinterLogRunner;-><init>(Lcom/squareup/thread/executor/MainThread;)V

    return-object v0
.end method

.method static providePrinterRoleSupportChecker()Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 46
    invoke-static {}, Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;->rasterOnlyForTicketStubsAndReceipts()Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;

    move-result-object v0

    return-object v0
.end method

.method static provideTicketIdentifierServce(Lretrofit/RestAdapter;)Lcom/squareup/server/TicketIdentifierService;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 38
    const-class v0, Lcom/squareup/server/TicketIdentifierService;

    invoke-virtual {p0, v0}, Lretrofit/RestAdapter;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/TicketIdentifierService;

    return-object p0
.end method


# virtual methods
.method abstract provideDispatchingRenderer(Lcom/squareup/print/RegisterPayloadRenderer;)Lcom/squareup/print/PayloadRenderer;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
