.class public Lcom/squareup/print/RegisterPayloadRenderer;
.super Ljava/lang/Object;
.source "RegisterPayloadRenderer.java"

# interfaces
.implements Lcom/squareup/print/PayloadRenderer;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final bitmapBuilderFactory:Lcom/squareup/print/ThermalBitmapBuilder$Factory;


# direct methods
.method public constructor <init>(Lcom/squareup/print/ThermalBitmapBuilder$Factory;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/print/RegisterPayloadRenderer;->bitmapBuilderFactory:Lcom/squareup/print/ThermalBitmapBuilder$Factory;

    .line 25
    iput-object p2, p0, Lcom/squareup/print/RegisterPayloadRenderer;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method private logImpactPaperEvent(Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/text/RenderedRows;Lcom/squareup/print/PrintJob;)V
    .locals 5

    if-nez p1, :cond_0

    return-void

    .line 62
    :cond_0
    invoke-static {p2}, Lcom/squareup/print/text/RenderedRowsKt;->getTextContent(Lcom/squareup/print/text/RenderedRows;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object p2

    array-length v0, p2

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    aget-char v3, p2, v1

    const/16 v4, 0xa

    if-ne v3, v4, :cond_1

    add-int/lit8 v2, v2, 0x1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 66
    :cond_2
    invoke-static {v2}, Lcom/squareup/print/text/ImpactTextBuilder;->linesToInches(I)F

    move-result p2

    .line 68
    iget-object v0, p0, Lcom/squareup/print/RegisterPayloadRenderer;->analytics:Lcom/squareup/analytics/Analytics;

    .line 69
    invoke-static {p1, v2, p2, p3}, Lcom/squareup/print/PrintPaperEvent;->forImpactPaper(Lcom/squareup/analytics/RegisterActionName;IFLcom/squareup/print/PrintJob;)Lcom/squareup/print/PrintPaperEvent;

    move-result-object p1

    .line 68
    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private logThermalPaperEvent(Lcom/squareup/analytics/RegisterActionName;ILcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;Lcom/squareup/print/PrintJob;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 52
    :cond_0
    invoke-static {p2, p3}, Lcom/squareup/print/ThermalBitmapBuilder;->pxToInches(ILcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;)F

    move-result p3

    .line 53
    iget-object v0, p0, Lcom/squareup/print/RegisterPayloadRenderer;->analytics:Lcom/squareup/analytics/Analytics;

    .line 54
    invoke-static {p1, p2, p3, p4}, Lcom/squareup/print/PrintPaperEvent;->forThermalPaper(Lcom/squareup/analytics/RegisterActionName;IFLcom/squareup/print/PrintJob;)Lcom/squareup/print/PrintPaperEvent;

    move-result-object p1

    .line 53
    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method


# virtual methods
.method public renderBitmap(Lcom/squareup/print/PrintablePayload;Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;Lcom/squareup/print/PrintJob;)Landroid/graphics/Bitmap;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/print/RegisterPayloadRenderer;->bitmapBuilderFactory:Lcom/squareup/print/ThermalBitmapBuilder$Factory;

    .line 32
    invoke-virtual {v0, p2}, Lcom/squareup/print/ThermalBitmapBuilder$Factory;->get(Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;)Lcom/squareup/print/ThermalBitmapBuilder;

    move-result-object v0

    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->isReprint()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/print/PrintablePayload;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 33
    invoke-virtual {p1}, Lcom/squareup/print/PrintablePayload;->getAnalyticsName()Lcom/squareup/analytics/RegisterActionName;

    move-result-object p1

    .line 34
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-direct {p0, p1, v1, p2, p3}, Lcom/squareup/print/RegisterPayloadRenderer;->logThermalPaperEvent(Lcom/squareup/analytics/RegisterActionName;ILcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;Lcom/squareup/print/PrintJob;)V

    return-object v0
.end method

.method public renderText(Lcom/squareup/print/PrintablePayload;Lcom/squareup/print/PrintJob;I)Lcom/squareup/print/text/RenderedRows;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/print/text/ImpactTextBuilder;

    invoke-direct {v0, p3}, Lcom/squareup/print/text/ImpactTextBuilder;-><init>(I)V

    .line 42
    invoke-virtual {p2}, Lcom/squareup/print/PrintJob;->isReprint()Z

    move-result p3

    invoke-virtual {p1, v0, p3}, Lcom/squareup/print/PrintablePayload;->renderText(Lcom/squareup/print/text/ImpactTextBuilder;Z)Lcom/squareup/print/text/RenderedRows;

    move-result-object p3

    .line 43
    invoke-virtual {p1}, Lcom/squareup/print/PrintablePayload;->getAnalyticsName()Lcom/squareup/analytics/RegisterActionName;

    move-result-object p1

    .line 44
    invoke-direct {p0, p1, p3, p2}, Lcom/squareup/print/RegisterPayloadRenderer;->logImpactPaperEvent(Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/text/RenderedRows;Lcom/squareup/print/PrintJob;)V

    return-object p3
.end method
