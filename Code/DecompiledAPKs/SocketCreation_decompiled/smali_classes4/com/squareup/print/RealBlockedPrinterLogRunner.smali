.class public final Lcom/squareup/print/RealBlockedPrinterLogRunner;
.super Ljava/lang/Object;
.source "RealBlockedPrinterLogRunner.kt"

# interfaces
.implements Lcom/squareup/print/BlockedPrinterLogRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/RealBlockedPrinterLogRunner$TimeoutThrowable;,
        Lcom/squareup/print/RealBlockedPrinterLogRunner$TimeoutRunner;,
        Lcom/squareup/print/RealBlockedPrinterLogRunner$UniqueLogId;,
        Lcom/squareup/print/RealBlockedPrinterLogRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealBlockedPrinterLogRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealBlockedPrinterLogRunner.kt\ncom/squareup/print/RealBlockedPrinterLogRunner\n*L\n1#1,77:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0006\u0018\u0000 \u00102\u00020\u0001:\u0004\u0010\u0011\u0012\u0013B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0015\u0010\t\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\u000bH\u0001\u00a2\u0006\u0002\u0008\u000cJ\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0010\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\n\u001a\u00020\u000bH\u0016R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00080\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/print/RealBlockedPrinterLogRunner;",
        "Lcom/squareup/print/BlockedPrinterLogRunner;",
        "mainThread",
        "Lcom/squareup/thread/executor/MainThread;",
        "(Lcom/squareup/thread/executor/MainThread;)V",
        "jobToRunnableMap",
        "",
        "Lcom/squareup/print/RealBlockedPrinterLogRunner$UniqueLogId;",
        "Ljava/lang/Runnable;",
        "getUniqueLogId",
        "printJob",
        "Lcom/squareup/print/PrintJob;",
        "getUniqueLogId$hardware_release",
        "onJobFinished",
        "",
        "onJobStarted",
        "Companion",
        "TimeoutRunner",
        "TimeoutThrowable",
        "UniqueLogId",
        "hardware_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/print/RealBlockedPrinterLogRunner$Companion;

.field private static final DELAY_MILLIS:J


# instance fields
.field private final jobToRunnableMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/print/RealBlockedPrinterLogRunner$UniqueLogId;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/print/RealBlockedPrinterLogRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/print/RealBlockedPrinterLogRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/print/RealBlockedPrinterLogRunner;->Companion:Lcom/squareup/print/RealBlockedPrinterLogRunner$Companion;

    .line 55
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0xa

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/squareup/print/RealBlockedPrinterLogRunner;->DELAY_MILLIS:J

    return-void
.end method

.method public constructor <init>(Lcom/squareup/thread/executor/MainThread;)V
    .locals 1

    const-string v0, "mainThread"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 15
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner;->jobToRunnableMap:Ljava/util/Map;

    return-void
.end method

.method public static final synthetic access$getDELAY_MILLIS$cp()J
    .locals 2

    .line 12
    sget-wide v0, Lcom/squareup/print/RealBlockedPrinterLogRunner;->DELAY_MILLIS:J

    return-wide v0
.end method

.method public static final synthetic access$getJobToRunnableMap$p(Lcom/squareup/print/RealBlockedPrinterLogRunner;)Ljava/util/Map;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner;->jobToRunnableMap:Ljava/util/Map;

    return-object p0
.end method

.method public static final synthetic access$getMainThread$p(Lcom/squareup/print/RealBlockedPrinterLogRunner;)Lcom/squareup/thread/executor/MainThread;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-object p0
.end method


# virtual methods
.method public final getUniqueLogId$hardware_release(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/RealBlockedPrinterLogRunner$UniqueLogId;
    .locals 3

    const-string v0, "printJob"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    new-instance v0, Lcom/squareup/print/RealBlockedPrinterLogRunner$UniqueLogId;

    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "printJob.jobId"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getTargetId()Ljava/lang/String;

    move-result-object p1

    const-string v2, "printJob.targetId"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, p1}, Lcom/squareup/print/RealBlockedPrinterLogRunner$UniqueLogId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onJobFinished(Lcom/squareup/print/PrintJob;)V
    .locals 2

    const-string v0, "printJob"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    iget-object v0, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/print/RealBlockedPrinterLogRunner$onJobFinished$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/print/RealBlockedPrinterLogRunner$onJobFinished$1;-><init>(Lcom/squareup/print/RealBlockedPrinterLogRunner;Lcom/squareup/print/PrintJob;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onJobStarted(Lcom/squareup/print/PrintJob;)V
    .locals 2

    const-string v0, "printJob"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    iget-object v0, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/print/RealBlockedPrinterLogRunner$onJobStarted$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/print/RealBlockedPrinterLogRunner$onJobStarted$1;-><init>(Lcom/squareup/print/RealBlockedPrinterLogRunner;Lcom/squareup/print/PrintJob;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
