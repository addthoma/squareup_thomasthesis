.class Lcom/squareup/print/PrintSpooler$1;
.super Ljava/lang/Object;
.source "PrintSpooler.java"

# interfaces
.implements Lcom/squareup/print/PrintTarget;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/print/PrintSpooler;->enqueueForTestPrint(Lcom/squareup/print/HardwarePrinter;Lcom/squareup/print/PrintablePayload;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/print/PrintSpooler;

.field final synthetic val$hardwarePrinter:Lcom/squareup/print/HardwarePrinter;


# direct methods
.method constructor <init>(Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/HardwarePrinter;)V
    .locals 0

    .line 192
    iput-object p1, p0, Lcom/squareup/print/PrintSpooler$1;->this$0:Lcom/squareup/print/PrintSpooler;

    iput-object p2, p0, Lcom/squareup/print/PrintSpooler$1;->val$hardwarePrinter:Lcom/squareup/print/HardwarePrinter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    .line 194
    iget-object v0, p0, Lcom/squareup/print/PrintSpooler$1;->val$hardwarePrinter:Lcom/squareup/print/HardwarePrinter;

    invoke-virtual {v0}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "TestTarget"

    return-object v0
.end method
