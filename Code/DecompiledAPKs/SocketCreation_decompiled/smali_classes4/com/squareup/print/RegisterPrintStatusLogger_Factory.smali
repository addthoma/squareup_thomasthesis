.class public final Lcom/squareup/print/RegisterPrintStatusLogger_Factory;
.super Ljava/lang/Object;
.source "RegisterPrintStatusLogger_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/RegisterPrintStatusLogger;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/print/RegisterPrintStatusLogger_Factory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/print/RegisterPrintStatusLogger_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/print/RegisterPrintStatusLogger_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/print/RegisterPrintStatusLogger_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/print/RegisterPrintStatusLogger_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/analytics/Analytics;)Lcom/squareup/print/RegisterPrintStatusLogger;
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/print/RegisterPrintStatusLogger;

    invoke-direct {v0, p0}, Lcom/squareup/print/RegisterPrintStatusLogger;-><init>(Lcom/squareup/analytics/Analytics;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/print/RegisterPrintStatusLogger;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/print/RegisterPrintStatusLogger_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {v0}, Lcom/squareup/print/RegisterPrintStatusLogger_Factory;->newInstance(Lcom/squareup/analytics/Analytics;)Lcom/squareup/print/RegisterPrintStatusLogger;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/print/RegisterPrintStatusLogger_Factory;->get()Lcom/squareup/print/RegisterPrintStatusLogger;

    move-result-object v0

    return-object v0
.end method
