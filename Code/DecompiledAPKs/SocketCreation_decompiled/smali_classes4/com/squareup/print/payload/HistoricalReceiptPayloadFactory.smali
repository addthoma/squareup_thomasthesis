.class public Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;
.super Lcom/squareup/print/payload/ReceiptPayloadFactory;
.source "HistoricalReceiptPayloadFactory.java"


# instance fields
.field private final formatter:Lcom/squareup/print/ReceiptFormatter;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/print/papersig/TipSectionFactory;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/text/Formatter;Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;)V
    .locals 14
    .param p11    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/ForTaxPercentage;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/print/ReceiptFormatter;",
            "Lcom/squareup/settings/server/Features;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/print/papersig/TipSectionFactory;",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/PhoneNumberHelper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p4

    move-object/from16 v3, p6

    move-object/from16 v4, p7

    move-object/from16 v5, p8

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    move-object/from16 v8, p11

    move-object v9, p1

    move-object/from16 v10, p12

    move-object/from16 v11, p5

    move-object/from16 v12, p13

    move-object/from16 v13, p14

    .line 87
    invoke-direct/range {v0 .. v13}, Lcom/squareup/print/payload/ReceiptPayloadFactory;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/print/papersig/TipSectionFactory;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Ljavax/inject/Provider;Lcom/squareup/text/Formatter;Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;)V

    move-object/from16 v1, p3

    .line 90
    iput-object v1, v0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    return-void
.end method

.method private createCodesSection(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/print/sections/CodesSection;
    .locals 7

    if-eqz p1, :cond_0

    .line 238
    invoke-virtual {p0, p1}, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->buildReceiptFormatter(Lcom/squareup/locale/LocaleOverrideFactory;)Lcom/squareup/print/ReceiptFormatter;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    :goto_0
    move-object v1, p1

    .line 239
    iget-object p1, p3, Lcom/squareup/billhistory/model/TenderHistory;->receiptNumber:Ljava/lang/String;

    invoke-virtual {v1, p1}, Lcom/squareup/print/ReceiptFormatter;->receiptNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 241
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->getTicketName()Ljava/lang/String;

    move-result-object v6

    const/4 p1, 0x0

    .line 243
    iget-object v0, p3, Lcom/squareup/billhistory/model/TenderHistory;->sequentialTenderNumber:Ljava/lang/String;

    .line 244
    invoke-virtual {v1, v0}, Lcom/squareup/print/ReceiptFormatter;->sequentialTenderNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 246
    instance-of v0, p3, Lcom/squareup/billhistory/model/CreditCardTenderHistory;

    if-eqz v0, :cond_1

    .line 247
    check-cast p3, Lcom/squareup/billhistory/model/CreditCardTenderHistory;

    .line 248
    iget-object p1, p3, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->authorizationCode:Ljava/lang/String;

    .line 249
    invoke-virtual {v1, p1}, Lcom/squareup/print/ReceiptFormatter;->authorizationCodeOrNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_1
    move-object v4, p1

    .line 253
    iget-object v0, p0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v2, p2, Lcom/squareup/billhistory/model/BillHistory;->order:Lcom/squareup/payment/Order;

    invoke-static/range {v0 .. v6}, Lcom/squareup/print/sections/CodesSection;->fromOrder(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/print/sections/CodesSection;

    move-result-object p1

    return-object p1
.end method

.method private createEmvSection(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/print/sections/EmvSection;
    .locals 2

    if-eqz p1, :cond_0

    .line 263
    invoke-virtual {p0, p1}, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->buildReceiptFormatter(Lcom/squareup/locale/LocaleOverrideFactory;)Lcom/squareup/print/ReceiptFormatter;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    .line 265
    :goto_0
    instance-of v0, p2, Lcom/squareup/billhistory/model/CreditCardTenderHistory;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 266
    check-cast p2, Lcom/squareup/billhistory/model/CreditCardTenderHistory;

    .line 267
    iget-object v1, p2, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->applicationPreferredName:Ljava/lang/String;

    .line 268
    iget-object v0, p2, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->applicationId:Ljava/lang/String;

    .line 269
    invoke-virtual {p1, v0}, Lcom/squareup/print/ReceiptFormatter;->applicationIdOrNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 270
    iget-object p2, p2, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->cardholderVerificationMethod:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    .line 271
    invoke-virtual {p1, p2}, Lcom/squareup/print/ReceiptFormatter;->cardholderVerificationMethodUsedOrNull(Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    move-object p1, v1

    move-object v0, p1

    .line 274
    :goto_1
    new-instance p2, Lcom/squareup/print/sections/EmvSection;

    invoke-direct {p2, v1, v0, p1}, Lcom/squareup/print/sections/EmvSection;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object p2
.end method

.method private createRefundsSection(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/print/sections/RefundsSection;
    .locals 2

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "localeOverride cannot be null"

    .line 348
    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 351
    invoke-direct {p0, p2, p3}, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->findApplicableTenderRefunds(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;)Ljava/util/List;

    move-result-object p3

    .line 352
    iget-object p2, p2, Lcom/squareup/billhistory/model/BillHistory;->order:Lcom/squareup/payment/Order;

    .line 353
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p2

    invoke-direct {p0, p3, p2, p1}, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->formatRefunds(Ljava/util/List;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/locale/LocaleOverrideFactory;)Ljava/util/List;

    move-result-object p1

    .line 355
    new-instance p2, Lcom/squareup/print/sections/RefundsSection;

    invoke-direct {p2, p1}, Lcom/squareup/print/sections/RefundsSection;-><init>(Ljava/util/List;)V

    return-object p2
.end method

.method private createSubtotalAndAdjustmentsSection(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;
    .locals 7

    if-eqz p1, :cond_0

    .line 198
    invoke-virtual {p0, p1}, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->buildReceiptFormatter(Lcom/squareup/locale/LocaleOverrideFactory;)Lcom/squareup/print/ReceiptFormatter;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    :goto_0
    move-object v0, p1

    .line 199
    iget-object p1, p2, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    .line 201
    :goto_1
    invoke-static {p2, p3}, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->findSwedishRounding(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    if-eqz v1, :cond_2

    .line 204
    iget-object p1, p2, Lcom/squareup/billhistory/model/BillHistory;->tip:Lcom/squareup/protos/common/Money;

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    :goto_2
    move-object v2, p1

    .line 206
    iget-object v1, p2, Lcom/squareup/billhistory/model/BillHistory;->order:Lcom/squareup/payment/Order;

    iget-object p1, p0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    sget-object p2, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_AUTO_GRATUITY:Lcom/squareup/settings/server/Features$Feature;

    .line 211
    invoke-interface {p1, p2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 206
    invoke-static/range {v0 .. v6}, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->fromOrder(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZZZ)Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    move-result-object p1

    return-object p1
.end method

.method private createTenderSection(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;Z)Lcom/squareup/print/sections/TenderSection;
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    if-eqz p1, :cond_0

    .line 280
    invoke-virtual/range {p0 .. p1}, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->buildReceiptFormatter(Lcom/squareup/locale/LocaleOverrideFactory;)Lcom/squareup/print/ReceiptFormatter;

    move-result-object v2

    goto :goto_0

    :cond_0
    iget-object v2, v0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    :goto_0
    move-object v3, v2

    if-eqz p1, :cond_1

    .line 281
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v2

    goto :goto_1

    :cond_1
    iget-object v2, v0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->res:Lcom/squareup/util/Res;

    :goto_1
    const/4 v4, 0x0

    .line 284
    invoke-virtual {v1, v2}, Lcom/squareup/billhistory/model/TenderHistory;->getReceiptTenderName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v5

    .line 286
    invoke-virtual {v1, v2}, Lcom/squareup/billhistory/model/TenderHistory;->getDescription(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 287
    iget-object v7, v1, Lcom/squareup/billhistory/model/TenderHistory;->amount:Lcom/squareup/protos/common/Money;

    .line 294
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/billhistory/model/BillHistory;->isSplitTender()Z

    move-result v8

    const/4 v9, 0x0

    if-eqz v8, :cond_2

    .line 295
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/billhistory/model/TenderHistory;->amountExcludingTip()Lcom/squareup/protos/common/Money;

    move-result-object v8

    .line 296
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/billhistory/model/TenderHistory;->tip()Lcom/squareup/protos/common/Money;

    move-result-object v10

    move-object v12, v8

    goto :goto_2

    :cond_2
    move-object v10, v9

    move-object v12, v10

    .line 300
    :goto_2
    instance-of v8, v1, Lcom/squareup/billhistory/model/CashTenderHistory;

    const/4 v11, 0x1

    if-eqz v8, :cond_3

    .line 301
    check-cast v1, Lcom/squareup/billhistory/model/CashTenderHistory;

    .line 302
    iget-object v2, v1, Lcom/squareup/billhistory/model/CashTenderHistory;->tenderedAmount:Lcom/squareup/protos/common/Money;

    .line 303
    iget-object v1, v1, Lcom/squareup/billhistory/model/CashTenderHistory;->changeAmount:Lcom/squareup/protos/common/Money;

    move-object v8, v1

    move-object v11, v2

    move-object v2, v6

    move-object v14, v9

    move-object v15, v14

    move-object/from16 v16, v15

    const/4 v7, 0x1

    move-object/from16 v1, p2

    goto/16 :goto_9

    .line 305
    :cond_3
    instance-of v8, v1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;

    if-eqz v8, :cond_a

    .line 306
    check-cast v1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;

    .line 307
    iget-object v8, v1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->buyerSelectedAccountName:Ljava/lang/String;

    .line 309
    iget-object v13, v1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->brand:Lcom/squareup/Card$Brand;

    sget-object v14, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    if-ne v13, v14, :cond_4

    .line 310
    invoke-virtual {v1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->getRemainingBalanceMinusTip()Lcom/squareup/protos/common/Money;

    move-result-object v1

    move-object v2, v6

    move-object v11, v7

    move-object v14, v8

    move-object v8, v9

    move-object v15, v8

    move-object/from16 v16, v15

    const/4 v7, 0x0

    move-object v9, v5

    move-object v5, v1

    :goto_3
    move-object/from16 v1, p2

    goto/16 :goto_a

    .line 311
    :cond_4
    iget-object v13, v1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->brand:Lcom/squareup/Card$Brand;

    sget-object v14, Lcom/squareup/Card$Brand;->FELICA:Lcom/squareup/Card$Brand;

    if-ne v13, v14, :cond_9

    .line 312
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 313
    sget-object v6, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory$1;->$SwitchMap$com$squareup$protos$client$bills$CardTender$Card$FelicaBrand:[I

    iget-object v13, v1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->felicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    invoke-virtual {v13}, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->ordinal()I

    move-result v13

    aget v6, v6, v13

    if-eq v6, v11, :cond_8

    const/4 v11, 0x2

    if-eq v6, v11, :cond_6

    const/4 v11, 0x3

    if-eq v6, v11, :cond_5

    goto :goto_5

    .line 324
    :cond_5
    sget v6, Lcom/squareup/print/R$string;->receipt_tender_felica_suica:I

    invoke-interface {v2, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 325
    invoke-virtual {v1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->getRemainingBalanceMinusTip()Lcom/squareup/protos/common/Money;

    move-result-object v6

    .line 326
    new-instance v11, Lcom/squareup/print/payload/LabelAmountPair;

    sget v13, Lcom/squareup/print/R$string;->receipt_felica_suica_terminal_id:I

    .line 327
    invoke-interface {v2, v13}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v13, v1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->felicaTerminalId:Ljava/lang/String;

    invoke-direct {v11, v2, v13}, Lcom/squareup/print/payload/LabelAmountPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v9

    goto :goto_7

    .line 318
    :cond_6
    sget v6, Lcom/squareup/print/R$string;->receipt_tender_felica_id:I

    invoke-interface {v2, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p4, :cond_7

    .line 319
    sget v6, Lcom/squareup/print/R$string;->receipt_tender_felica_id_refund_location:I

    .line 320
    invoke-interface {v2, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    :cond_7
    sget v6, Lcom/squareup/print/R$string;->receipt_tender_felica_id_purchase_location:I

    .line 321
    invoke-interface {v2, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_4
    move-object v6, v9

    goto :goto_6

    .line 315
    :cond_8
    sget v6, Lcom/squareup/print/R$string;->receipt_tender_felica_qp:I

    invoke-interface {v2, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    move-object v2, v9

    move-object v6, v2

    :goto_6
    move-object v11, v6

    :goto_7
    const-string/jumbo v13, "\u00a0"

    .line 333
    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, v1, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->felicaMaskedCardNumber:Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v15, v2

    move-object v5, v6

    move-object v14, v8

    move-object v8, v9

    move-object/from16 v16, v11

    move-object v2, v1

    move-object v9, v2

    move-object v11, v7

    const/4 v7, 0x0

    goto :goto_3

    :cond_9
    move-object/from16 v1, p2

    move-object v2, v6

    move-object v11, v7

    move-object v14, v8

    move-object v8, v9

    move-object v15, v8

    goto :goto_8

    :cond_a
    move-object/from16 v1, p2

    move-object v2, v6

    move-object v11, v7

    move-object v8, v9

    move-object v14, v8

    move-object v15, v14

    :goto_8
    move-object/from16 v16, v15

    const/4 v7, 0x0

    :goto_9
    move-object v9, v5

    move-object/from16 v5, v16

    .line 340
    :goto_a
    iget-object v4, v1, Lcom/squareup/billhistory/model/BillHistory;->order:Lcom/squareup/payment/Order;

    const/4 v13, 0x0

    move-object v6, v10

    move-object v10, v2

    invoke-static/range {v3 .. v16}, Lcom/squareup/print/sections/TenderSection;->fromOrder(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZLcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/sections/TenderSection;

    move-result-object v1

    return-object v1
.end method

.method private createTotalsSection(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/TaxBreakdown;)Lcom/squareup/print/sections/TotalSection;
    .locals 2

    if-eqz p1, :cond_0

    .line 220
    invoke-virtual {p0, p1}, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->buildReceiptFormatter(Lcom/squareup/locale/LocaleOverrideFactory;)Lcom/squareup/print/ReceiptFormatter;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    .line 221
    :goto_0
    iget-object v0, p2, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    .line 223
    :goto_1
    iget-object v0, p2, Lcom/squareup/billhistory/model/BillHistory;->total:Lcom/squareup/protos/common/Money;

    if-nez v1, :cond_2

    .line 228
    iget-object v1, p2, Lcom/squareup/billhistory/model/BillHistory;->tip:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->subtractNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 231
    :cond_2
    iget-object p2, p2, Lcom/squareup/billhistory/model/BillHistory;->order:Lcom/squareup/payment/Order;

    iget-object v1, p0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {p1, p2, p3, v0, v1}, Lcom/squareup/print/sections/TotalSection;->fromOrder(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/util/TaxBreakdown;Lcom/squareup/protos/common/Money;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/print/sections/TotalSection;

    move-result-object p1

    return-object p1
.end method

.method private findApplicableTenderRefunds(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/RelatedBillHistory;",
            ">;"
        }
    .end annotation

    .line 379
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 381
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->getRelatedRefundBills()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 383
    invoke-virtual {v1}, Lcom/squareup/server/payment/RelatedBillHistory;->getTendersRefundMoney()Ljava/util/Map;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/squareup/server/payment/RelatedBillHistory;->getTendersRefundMoney()Ljava/util/Map;

    move-result-object v2

    iget-object v3, p2, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 385
    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method static findSwedishRounding(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/protos/common/Money;
    .locals 3

    .line 362
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->getTenders()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 363
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->getTenders()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/TenderHistory;

    const/4 v1, 0x0

    if-eq p1, v0, :cond_0

    return-object v1

    .line 368
    :cond_0
    iget-object p0, p0, Lcom/squareup/billhistory/model/BillHistory;->order:Lcom/squareup/payment/Order;

    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getOrderAdjustments()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/payment/OrderAdjustment;

    .line 369
    iget-object v0, p1, Lcom/squareup/payment/OrderAdjustment;->subtotalType:Lcom/squareup/checkout/SubtotalType;

    sget-object v2, Lcom/squareup/checkout/SubtotalType;->SWEDISH_ROUNDING:Lcom/squareup/checkout/SubtotalType;

    if-ne v0, v2, :cond_1

    .line 370
    iget-object p0, p1, Lcom/squareup/payment/OrderAdjustment;->appliedMoney:Lcom/squareup/protos/common/Money;

    return-object p0

    :cond_2
    return-object v1
.end method

.method private formatRefunds(Ljava/util/List;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/locale/LocaleOverrideFactory;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/RelatedBillHistory;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/RefundsSection$RefundSection;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_7

    .line 396
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_4

    :cond_0
    if-eqz p3, :cond_1

    .line 401
    invoke-virtual {p0, p3}, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->buildReceiptFormatter(Lcom/squareup/locale/LocaleOverrideFactory;)Lcom/squareup/print/ReceiptFormatter;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    :goto_0
    if-eqz p3, :cond_2

    .line 402
    invoke-virtual {p3}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p3

    goto :goto_1

    :cond_2
    iget-object p3, p0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->res:Lcom/squareup/util/Res;

    .line 403
    :goto_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 405
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 406
    invoke-virtual {v2}, Lcom/squareup/server/payment/RelatedBillHistory;->isFailedOrRejected()Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    .line 410
    :cond_3
    invoke-virtual {v2}, Lcom/squareup/server/payment/RelatedBillHistory;->getReasonOption()Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    move-result-object v3

    .line 411
    invoke-virtual {v2}, Lcom/squareup/server/payment/RelatedBillHistory;->getReason()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    .line 410
    invoke-static {v3, v4, p3, v5}, Lcom/squareup/util/RefundReasonsHelperKt;->getReasonName(Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/util/Res;Z)Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x0

    .line 414
    invoke-virtual {v2}, Lcom/squareup/server/payment/RelatedBillHistory;->getTendersRefundMoney()Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/protos/common/Money;

    .line 415
    iget-object v7, v7, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    add-long/2addr v4, v7

    goto :goto_3

    :cond_4
    neg-long v4, v4

    .line 418
    invoke-static {v4, v5, p2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 421
    invoke-virtual {v2}, Lcom/squareup/server/payment/RelatedBillHistory;->getCreatedAt()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 422
    invoke-virtual {v2}, Lcom/squareup/server/payment/RelatedBillHistory;->getCreatedAt()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    .line 423
    invoke-virtual {v0, v2}, Lcom/squareup/print/ReceiptFormatter;->getDateDetailString(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 426
    :cond_5
    new-instance v2, Lcom/squareup/print/sections/RefundsSection$RefundSection;

    invoke-direct {v2, v5, v4, v3}, Lcom/squareup/print/sections/RefundsSection$RefundSection;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    return-object v1

    .line 397
    :cond_7
    :goto_4
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public createPayload(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)Lcom/squareup/print/payload/ReceiptPayload;
    .locals 34

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    const/4 v3, 0x0

    if-nez p3, :cond_0

    .line 97
    iget-object v4, v2, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/billhistory/model/TenderHistory;

    goto :goto_0

    :cond_0
    move-object/from16 v4, p3

    .line 100
    :goto_0
    iget-object v7, v4, Lcom/squareup/billhistory/model/TenderHistory;->timestamp:Ljava/util/Date;

    .line 101
    iget-object v15, v2, Lcom/squareup/billhistory/model/BillHistory;->order:Lcom/squareup/payment/Order;

    .line 103
    iget-object v5, v2, Lcom/squareup/billhistory/model/BillHistory;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v5, v5, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object v5, v5, Lcom/squareup/protos/client/bills/Cart$LineItems;->fee:Ljava/util/List;

    const-string v6, "billHistory.cart.line_items.fee"

    .line 104
    invoke-static {v5, v6}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 103
    invoke-static {v15, v5}, Lcom/squareup/util/TaxBreakdown;->fromOrderSnapshot(Lcom/squareup/payment/Order;Ljava/util/List;)Lcom/squareup/util/TaxBreakdown;

    move-result-object v14

    .line 107
    iget-object v5, v0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    iget-object v6, v4, Lcom/squareup/billhistory/model/TenderHistory;->employeeToken:Ljava/lang/String;

    invoke-interface {v5, v6}, Lcom/squareup/permissions/EmployeeManagement;->getEmployeeByToken(Ljava/lang/String;)Lcom/squareup/permissions/Employee;

    move-result-object v8

    if-eqz v1, :cond_1

    .line 110
    invoke-virtual/range {p0 .. p1}, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->buildReceiptFormatter(Lcom/squareup/locale/LocaleOverrideFactory;)Lcom/squareup/print/ReceiptFormatter;

    move-result-object v5

    goto :goto_1

    :cond_1
    iget-object v5, v0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    :goto_1
    move-object v13, v5

    if-eqz v1, :cond_2

    .line 111
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v5

    goto :goto_2

    :cond_2
    iget-object v5, v0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->res:Lcom/squareup/util/Res;

    :goto_2
    move-object v12, v5

    .line 114
    invoke-direct {v0, v1, v2, v4}, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->createRefundsSection(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/print/sections/RefundsSection;

    move-result-object v11

    .line 116
    iget-object v5, v11, Lcom/squareup/print/sections/RefundsSection;->successfulRefunds:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    const/4 v6, 0x1

    xor-int/lit8 v10, v5, 0x1

    .line 119
    instance-of v5, v4, Lcom/squareup/billhistory/model/CreditCardTenderHistory;

    if-eqz v5, :cond_3

    .line 120
    move-object v5, v4

    check-cast v5, Lcom/squareup/billhistory/model/CreditCardTenderHistory;

    .line 121
    iget-object v5, v5, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->brand:Lcom/squareup/Card$Brand;

    sget-object v9, Lcom/squareup/Card$Brand;->FELICA:Lcom/squareup/Card$Brand;

    if-ne v5, v9, :cond_3

    const/4 v3, 0x1

    .line 124
    :cond_3
    iget-object v5, v0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v9, v0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    move-object v6, v13

    move-object/from16 v16, v9

    move-object/from16 v9, p4

    move/from16 p3, v10

    move-object/from16 v24, v11

    move v11, v3

    move-object v3, v12

    move-object/from16 v12, v16

    .line 125
    invoke-static/range {v5 .. v12}, Lcom/squareup/print/sections/HeaderSection;->createSection(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Ljava/util/Date;Lcom/squareup/permissions/Employee;Lcom/squareup/print/payload/ReceiptPayload$RenderType;ZZLcom/squareup/settings/server/Features;)Lcom/squareup/print/sections/HeaderSection;

    move-result-object v5

    .line 128
    invoke-direct {v0, v1, v2, v4}, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->createCodesSection(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/print/sections/CodesSection;

    move-result-object v6

    .line 129
    invoke-direct {v0, v1, v4}, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->createEmvSection(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/print/sections/EmvSection;

    move-result-object v7

    .line 131
    iget-object v8, v0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v12, v0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    iget-object v9, v0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->localeProvider:Ljavax/inject/Provider;

    .line 133
    invoke-interface {v9}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v9

    move-object/from16 v16, v9

    check-cast v16, Ljava/util/Locale;

    iget-object v9, v0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v9}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v17

    move-object v9, v13

    move-object v10, v15

    move-object v11, v14

    move-object/from16 v23, v7

    move-object v7, v13

    move-object/from16 v13, v16

    move-object/from16 v25, v6

    move-object v6, v14

    move/from16 v14, v17

    move-object/from16 v26, v15

    move-object v15, v3

    .line 132
    invoke-static/range {v8 .. v15}, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->fromOrder(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/util/TaxBreakdown;Lcom/squareup/settings/server/Features;Ljava/util/Locale;ZLcom/squareup/util/Res;)Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    move-result-object v14

    .line 136
    invoke-direct {v0, v1, v2, v4}, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->createSubtotalAndAdjustmentsSection(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    move-result-object v15

    .line 138
    invoke-direct {v0, v1, v2, v6}, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->createTotalsSection(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/TaxBreakdown;)Lcom/squareup/print/sections/TotalSection;

    move-result-object v27

    .line 142
    invoke-virtual/range {v26 .. v26}, Lcom/squareup/payment/Order;->hasReturn()Z

    move-result v8

    const/16 v28, 0x0

    if-eqz v8, :cond_4

    .line 143
    invoke-virtual/range {v26 .. v26}, Lcom/squareup/payment/Order;->getReturnCart()Lcom/squareup/checkout/ReturnCart;

    move-result-object v8

    .line 144
    invoke-static {v8}, Lcom/squareup/util/TaxBreakdown;->fromReturnCart(Lcom/squareup/checkout/ReturnCart;)Lcom/squareup/util/TaxBreakdown;

    move-result-object v19

    .line 145
    iget-object v9, v0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v10, v0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    iget-object v11, v0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    iget-object v12, v0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    .line 152
    invoke-virtual {v12}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v21

    move-object/from16 v16, v9

    move-object/from16 v17, v10

    move-object/from16 v18, v8

    move-object/from16 v20, v11

    move-object/from16 v22, v3

    .line 146
    invoke-static/range {v16 .. v22}, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->fromReturnCart(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/checkout/ReturnCart;Lcom/squareup/util/TaxBreakdown;Lcom/squareup/settings/server/Features;ZLcom/squareup/util/Res;)Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;

    move-result-object v9

    .line 156
    invoke-virtual/range {p0 .. p1}, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->buildReceiptFormatter(Lcom/squareup/locale/LocaleOverrideFactory;)Lcom/squareup/print/ReceiptFormatter;

    move-result-object v10

    .line 155
    invoke-static {v10, v8}, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->fromReturnCart(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/checkout/ReturnCart;)Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

    move-result-object v8

    move-object/from16 v30, v8

    move-object/from16 v29, v9

    move/from16 v8, p3

    goto :goto_3

    :cond_4
    move/from16 v8, p3

    move-object/from16 v29, v28

    move-object/from16 v30, v29

    .line 161
    :goto_3
    invoke-direct {v0, v1, v2, v4, v8}, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->createTenderSection(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;Z)Lcom/squareup/print/sections/TenderSection;

    move-result-object v1

    .line 164
    iget-object v2, v0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->tipSectionFactory:Lcom/squareup/print/papersig/TipSectionFactory;

    .line 165
    invoke-virtual {v2, v4, v7}, Lcom/squareup/print/papersig/TipSectionFactory;->createTipSectionForTenderHistory(Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/print/ReceiptFormatter;)Lcom/squareup/print/papersig/TipSections;

    move-result-object v2

    .line 166
    new-instance v8, Lcom/squareup/ui/buyer/signature/AgreementBuilder;

    invoke-direct {v8, v3}, Lcom/squareup/ui/buyer/signature/AgreementBuilder;-><init>(Lcom/squareup/util/Res;)V

    .line 167
    invoke-static {v4, v8}, Lcom/squareup/print/papersig/SignatureSection;->fromTenderHistory(Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/ui/buyer/signature/AgreementBuilder;)Lcom/squareup/print/papersig/SignatureSection;

    move-result-object v31

    .line 170
    iget-object v8, v0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {v8}, Lcom/squareup/print/sections/SectionUtils;->canShowTaxBreakDownTable(Lcom/squareup/settings/server/AccountStatusSettings;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 172
    invoke-static {v7, v6, v3}, Lcom/squareup/print/sections/MultipleTaxBreakdownSection;->fromTaxBreakdown(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/util/TaxBreakdown;Lcom/squareup/util/Res;)Lcom/squareup/print/sections/MultipleTaxBreakdownSection;

    move-result-object v3

    goto :goto_4

    :cond_5
    move-object/from16 v3, v28

    .line 176
    :goto_4
    iget-object v8, v0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v11, v0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v9, v7

    move-object/from16 v10, v26

    .line 177
    invoke-static/range {v8 .. v13}, Lcom/squareup/print/sections/FooterSection;->fromOrder(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/settings/server/Features;ZZ)Lcom/squareup/print/sections/FooterSection;

    move-result-object v6

    .line 180
    iget-object v7, v0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->receiptInfoProvider:Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;

    invoke-interface {v7}, Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;->shouldPrintBarcodes()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 181
    iget-object v7, v0, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->receiptInfoProvider:Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;

    iget-object v4, v4, Lcom/squareup/billhistory/model/TenderHistory;->receiptNumber:Ljava/lang/String;

    .line 182
    invoke-interface {v7, v4}, Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;->formatBarcodeForReceiptNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 183
    new-instance v7, Lcom/squareup/print/sections/BarcodeSection;

    invoke-direct {v7, v4}, Lcom/squareup/print/sections/BarcodeSection;-><init>(Ljava/lang/String;)V

    goto :goto_5

    :cond_6
    move-object/from16 v7, v28

    .line 186
    :goto_5
    new-instance v4, Lcom/squareup/print/payload/ReceiptPayload;

    move-object/from16 v16, v4

    sget-object v33, Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;->ITEMIZED_RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;

    move-object/from16 v17, v5

    move-object/from16 v18, v25

    move-object/from16 v19, v23

    move-object/from16 v20, v14

    move-object/from16 v21, v15

    move-object/from16 v22, v27

    move-object/from16 v23, v1

    move-object/from16 v25, v29

    move-object/from16 v26, v30

    move-object/from16 v27, v2

    move-object/from16 v28, v31

    move-object/from16 v29, v3

    move-object/from16 v30, v6

    move-object/from16 v31, v7

    move-object/from16 v32, p4

    invoke-direct/range {v16 .. v33}, Lcom/squareup/print/payload/ReceiptPayload;-><init>(Lcom/squareup/print/sections/HeaderSection;Lcom/squareup/print/sections/CodesSection;Lcom/squareup/print/sections/EmvSection;Lcom/squareup/print/sections/ItemsAndDiscountsSection;Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;Lcom/squareup/print/sections/TotalSection;Lcom/squareup/print/sections/TenderSection;Lcom/squareup/print/sections/RefundsSection;Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;Lcom/squareup/print/papersig/TipSections;Lcom/squareup/print/papersig/SignatureSection;Lcom/squareup/print/sections/MultipleTaxBreakdownSection;Lcom/squareup/print/sections/FooterSection;Lcom/squareup/print/sections/BarcodeSection;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;)V

    return-object v4
.end method
