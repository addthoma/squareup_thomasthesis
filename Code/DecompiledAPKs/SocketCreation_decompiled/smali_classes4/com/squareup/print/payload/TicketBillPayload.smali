.class public Lcom/squareup/print/payload/TicketBillPayload;
.super Lcom/squareup/print/PrintablePayload;
.source "TicketBillPayload.java"


# instance fields
.field public final codes:Lcom/squareup/print/sections/CodesSection;

.field public final footer:Lcom/squareup/print/sections/FooterSection;

.field public final header:Lcom/squareup/print/sections/HeaderSection;

.field public final itemsAndDiscounts:Lcom/squareup/print/sections/ItemsAndDiscountsSection;

.field public final subtotalAndAdjustments:Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

.field public final total:Lcom/squareup/print/sections/TotalSection;


# direct methods
.method public constructor <init>(Lcom/squareup/print/sections/HeaderSection;Lcom/squareup/print/sections/CodesSection;Lcom/squareup/print/sections/ItemsAndDiscountsSection;Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;Lcom/squareup/print/sections/TotalSection;Lcom/squareup/print/sections/FooterSection;)V
    .locals 1

    .line 32
    invoke-direct {p0}, Lcom/squareup/print/PrintablePayload;-><init>()V

    const-string v0, "header"

    .line 33
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/print/sections/HeaderSection;

    iput-object p1, p0, Lcom/squareup/print/payload/TicketBillPayload;->header:Lcom/squareup/print/sections/HeaderSection;

    const-string p1, "codes"

    .line 34
    invoke-static {p2, p1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/print/sections/CodesSection;

    iput-object p1, p0, Lcom/squareup/print/payload/TicketBillPayload;->codes:Lcom/squareup/print/sections/CodesSection;

    const-string p1, "itemsAndDiscounts"

    .line 35
    invoke-static {p3, p1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    iput-object p1, p0, Lcom/squareup/print/payload/TicketBillPayload;->itemsAndDiscounts:Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    const-string p1, "subtotalAndAdjustments"

    .line 36
    invoke-static {p4, p1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    iput-object p1, p0, Lcom/squareup/print/payload/TicketBillPayload;->subtotalAndAdjustments:Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    const-string p1, "totalSection"

    .line 37
    invoke-static {p5, p1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/print/sections/TotalSection;

    iput-object p1, p0, Lcom/squareup/print/payload/TicketBillPayload;->total:Lcom/squareup/print/sections/TotalSection;

    const-string p1, "footer"

    .line 38
    invoke-static {p6, p1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/print/sections/FooterSection;

    iput-object p1, p0, Lcom/squareup/print/payload/TicketBillPayload;->footer:Lcom/squareup/print/sections/FooterSection;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 53
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 54
    :cond_1
    check-cast p1, Lcom/squareup/print/payload/TicketBillPayload;

    .line 55
    iget-object v2, p0, Lcom/squareup/print/payload/TicketBillPayload;->header:Lcom/squareup/print/sections/HeaderSection;

    iget-object v3, p1, Lcom/squareup/print/payload/TicketBillPayload;->header:Lcom/squareup/print/sections/HeaderSection;

    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/print/payload/TicketBillPayload;->codes:Lcom/squareup/print/sections/CodesSection;

    iget-object v3, p1, Lcom/squareup/print/payload/TicketBillPayload;->codes:Lcom/squareup/print/sections/CodesSection;

    .line 56
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/print/payload/TicketBillPayload;->itemsAndDiscounts:Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    iget-object v3, p1, Lcom/squareup/print/payload/TicketBillPayload;->itemsAndDiscounts:Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    .line 57
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/print/payload/TicketBillPayload;->subtotalAndAdjustments:Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    iget-object v3, p1, Lcom/squareup/print/payload/TicketBillPayload;->subtotalAndAdjustments:Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    .line 58
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/print/payload/TicketBillPayload;->total:Lcom/squareup/print/sections/TotalSection;

    iget-object v3, p1, Lcom/squareup/print/payload/TicketBillPayload;->total:Lcom/squareup/print/sections/TotalSection;

    .line 59
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/print/payload/TicketBillPayload;->footer:Lcom/squareup/print/sections/FooterSection;

    iget-object p1, p1, Lcom/squareup/print/payload/TicketBillPayload;->footer:Lcom/squareup/print/sections/FooterSection;

    .line 60
    invoke-static {v2, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterActionName;
    .locals 1

    .line 48
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_BILL_PAPER:Lcom/squareup/analytics/RegisterActionName;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    .line 64
    iget-object v1, p0, Lcom/squareup/print/payload/TicketBillPayload;->header:Lcom/squareup/print/sections/HeaderSection;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/payload/TicketBillPayload;->codes:Lcom/squareup/print/sections/CodesSection;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/payload/TicketBillPayload;->itemsAndDiscounts:Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/payload/TicketBillPayload;->subtotalAndAdjustments:Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/payload/TicketBillPayload;->total:Lcom/squareup/print/sections/TotalSection;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/payload/TicketBillPayload;->footer:Lcom/squareup/print/sections/FooterSection;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;Z)Landroid/graphics/Bitmap;
    .locals 0

    .line 43
    new-instance p2, Lcom/squareup/print/TicketBillRenderer;

    invoke-direct {p2, p1}, Lcom/squareup/print/TicketBillRenderer;-><init>(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 44
    invoke-virtual {p2, p0}, Lcom/squareup/print/TicketBillRenderer;->renderBitmap(Lcom/squareup/print/payload/TicketBillPayload;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method
