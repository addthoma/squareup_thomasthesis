.class synthetic Lcom/squareup/print/PrinterStationEvent$1;
.super Ljava/lang/Object;
.source "PrinterStationEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/PrinterStationEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$print$PrinterStation$Role:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 83
    invoke-static {}, Lcom/squareup/print/PrinterStation$Role;->values()[Lcom/squareup/print/PrinterStation$Role;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/print/PrinterStationEvent$1;->$SwitchMap$com$squareup$print$PrinterStation$Role:[I

    :try_start_0
    sget-object v0, Lcom/squareup/print/PrinterStationEvent$1;->$SwitchMap$com$squareup$print$PrinterStation$Role:[I

    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    invoke-virtual {v1}, Lcom/squareup/print/PrinterStation$Role;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/print/PrinterStationEvent$1;->$SwitchMap$com$squareup$print$PrinterStation$Role:[I

    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    invoke-virtual {v1}, Lcom/squareup/print/PrinterStation$Role;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/print/PrinterStationEvent$1;->$SwitchMap$com$squareup$print$PrinterStation$Role:[I

    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->STUBS:Lcom/squareup/print/PrinterStation$Role;

    invoke-virtual {v1}, Lcom/squareup/print/PrinterStation$Role;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    return-void
.end method
