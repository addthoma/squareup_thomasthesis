.class public final Lcom/squareup/print/EpsonTcpPrinterScout;
.super Lcom/squareup/print/PrinterScout;
.source "EpsonTcpPrinterScout.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/EpsonTcpPrinterScout$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEpsonTcpPrinterScout.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EpsonTcpPrinterScout.kt\ncom/squareup/print/EpsonTcpPrinterScout\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n*L\n1#1,109:1\n1642#2,2:110\n1642#2,2:114\n151#3,2:112\n*E\n*S KotlinDebug\n*F\n+ 1 EpsonTcpPrinterScout.kt\ncom/squareup/print/EpsonTcpPrinterScout\n*L\n93#1,2:110\n104#1,2:114\n98#1,2:112\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0018\u0000 #2\u00020\u0001:\u0001#B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0017\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0014H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0015J\n\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0014J\u0008\u0010\u0018\u001a\u00020\u0019H\u0014J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J\u000e\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0014H\u0014J\u0016\u0010\u001f\u001a\u00020\u00192\u000c\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0014H\u0002J\u0008\u0010!\u001a\u00020\u0019H\u0016J\u0008\u0010\"\u001a\u00020\u0019H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/print/EpsonTcpPrinterScout;",
        "Lcom/squareup/print/PrinterScout;",
        "backgroundExecutor",
        "Lcom/squareup/thread/executor/StoppableSerialExecutor;",
        "mainThread",
        "Lcom/squareup/thread/executor/MainThread;",
        "epsonDiscoverer",
        "Lcom/squareup/printer/epson/EpsonDiscoverer;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/printer/epson/EpsonDiscoverer;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;)V",
        "recentPrinters",
        "",
        "Lcom/squareup/print/HardwarePrinter;",
        "",
        "scanning",
        "Ljava/util/concurrent/atomic/AtomicBoolean;",
        "discoverPrinters",
        "",
        "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "getConnectionType",
        "Lcom/squareup/print/ConnectionType;",
        "onScoutingDone",
        "",
        "registerOnBus",
        "Lio/reactivex/disposables/Disposable;",
        "bus",
        "Lcom/squareup/badbus/BadBus;",
        "scout",
        "setRecentPrinters",
        "printers",
        "start",
        "stop",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/print/EpsonTcpPrinterScout$Companion;

.field private static final DISCOVERY_DURATION_MS:J = 0x1388L

.field private static final LAST_SEEN_TIMEOUT_MS:J = 0xc350L


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final epsonDiscoverer:Lcom/squareup/printer/epson/EpsonDiscoverer;

.field private recentPrinters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/print/HardwarePrinter;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private scanning:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/print/EpsonTcpPrinterScout$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/print/EpsonTcpPrinterScout$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/print/EpsonTcpPrinterScout;->Companion:Lcom/squareup/print/EpsonTcpPrinterScout$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/printer/epson/EpsonDiscoverer;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;)V
    .locals 1

    const-string v0, "backgroundExecutor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThread"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "epsonDiscoverer"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0, p1, p2, p5}, Lcom/squareup/print/PrinterScout;-><init>(Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/Features;)V

    iput-object p3, p0, Lcom/squareup/print/EpsonTcpPrinterScout;->epsonDiscoverer:Lcom/squareup/printer/epson/EpsonDiscoverer;

    iput-object p4, p0, Lcom/squareup/print/EpsonTcpPrinterScout;->clock:Lcom/squareup/util/Clock;

    .line 28
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object p1, p0, Lcom/squareup/print/EpsonTcpPrinterScout;->scanning:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 33
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/EpsonTcpPrinterScout;->recentPrinters:Ljava/util/Map;

    return-void
.end method

.method public static final synthetic access$getRecentPrinters$p(Lcom/squareup/print/EpsonTcpPrinterScout;)Ljava/util/Map;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/print/EpsonTcpPrinterScout;->recentPrinters:Ljava/util/Map;

    return-object p0
.end method

.method public static final synthetic access$getScanning$p(Lcom/squareup/print/EpsonTcpPrinterScout;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/print/EpsonTcpPrinterScout;->scanning:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method public static final synthetic access$setRecentPrinters$p(Lcom/squareup/print/EpsonTcpPrinterScout;Ljava/util/Map;)V
    .locals 0

    .line 20
    iput-object p1, p0, Lcom/squareup/print/EpsonTcpPrinterScout;->recentPrinters:Ljava/util/Map;

    return-void
.end method

.method public static final synthetic access$setScanning$p(Lcom/squareup/print/EpsonTcpPrinterScout;Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 0

    .line 20
    iput-object p1, p0, Lcom/squareup/print/EpsonTcpPrinterScout;->scanning:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method private final setRecentPrinters(Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;)V"
        }
    .end annotation

    .line 90
    iget-object v0, p0, Lcom/squareup/print/EpsonTcpPrinterScout;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    .line 91
    new-instance v2, Ljava/util/HashMap;

    iget-object v3, p0, Lcom/squareup/print/EpsonTcpPrinterScout;->recentPrinters:Ljava/util/Map;

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 93
    check-cast p1, Ljava/lang/Iterable;

    .line 110
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/print/HardwarePrinter;

    .line 94
    move-object v4, v2

    check-cast v4, Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 97
    :cond_0
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    .line 98
    move-object v3, v2

    check-cast v3, Ljava/util/Map;

    .line 112
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/print/HardwarePrinter;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    const-string v7, "lastSeenMs"

    .line 99
    invoke-static {v5, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    sub-long v7, v0, v7

    const-wide/32 v9, 0xc350

    cmp-long v5, v7, v9

    if-lez v5, :cond_1

    const-string v5, "printer"

    .line 101
    invoke-static {v6, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 104
    :cond_2
    check-cast p1, Ljava/lang/Iterable;

    .line 114
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/HardwarePrinter;

    .line 104
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 106
    :cond_3
    iput-object v3, p0, Lcom/squareup/print/EpsonTcpPrinterScout;->recentPrinters:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method final synthetic discoverPrinters(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    instance-of v0, p1, Lcom/squareup/print/EpsonTcpPrinterScout$discoverPrinters$1;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/squareup/print/EpsonTcpPrinterScout$discoverPrinters$1;

    iget v1, v0, Lcom/squareup/print/EpsonTcpPrinterScout$discoverPrinters$1;->label:I

    const/high16 v2, -0x80000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    iget p1, v0, Lcom/squareup/print/EpsonTcpPrinterScout$discoverPrinters$1;->label:I

    sub-int/2addr p1, v2

    iput p1, v0, Lcom/squareup/print/EpsonTcpPrinterScout$discoverPrinters$1;->label:I

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/print/EpsonTcpPrinterScout$discoverPrinters$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/print/EpsonTcpPrinterScout$discoverPrinters$1;-><init>(Lcom/squareup/print/EpsonTcpPrinterScout;Lkotlin/coroutines/Continuation;)V

    :goto_0
    iget-object p1, v0, Lcom/squareup/print/EpsonTcpPrinterScout$discoverPrinters$1;->result:Ljava/lang/Object;

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v1

    .line 79
    iget v2, v0, Lcom/squareup/print/EpsonTcpPrinterScout$discoverPrinters$1;->label:I

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    if-ne v2, v3, :cond_1

    iget-object v0, v0, Lcom/squareup/print/EpsonTcpPrinterScout$discoverPrinters$1;->L$0:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/print/EpsonTcpPrinterScout;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_1

    .line 83
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 79
    :cond_2
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    .line 81
    iget-object p1, p0, Lcom/squareup/print/EpsonTcpPrinterScout;->epsonDiscoverer:Lcom/squareup/printer/epson/EpsonDiscoverer;

    const-wide/16 v4, 0x1388

    iput-object p0, v0, Lcom/squareup/print/EpsonTcpPrinterScout$discoverPrinters$1;->L$0:Ljava/lang/Object;

    iput v3, v0, Lcom/squareup/print/EpsonTcpPrinterScout$discoverPrinters$1;->label:I

    invoke-interface {p1, v4, v5, v0}, Lcom/squareup/printer/epson/EpsonDiscoverer;->startEpsonDiscovery(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v1, :cond_3

    return-object v1

    :cond_3
    move-object v0, p0

    .line 80
    :goto_1
    check-cast p1, Ljava/util/List;

    .line 82
    invoke-direct {v0, p1}, Lcom/squareup/print/EpsonTcpPrinterScout;->setRecentPrinters(Ljava/util/List;)V

    .line 83
    iget-object p1, v0, Lcom/squareup/print/EpsonTcpPrinterScout;->recentPrinters:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method protected getConnectionType()Lcom/squareup/print/ConnectionType;
    .locals 1

    .line 61
    sget-object v0, Lcom/squareup/print/ConnectionType;->TCP:Lcom/squareup/print/ConnectionType;

    return-object v0
.end method

.method protected onScoutingDone()V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/print/EpsonTcpPrinterScout;->scanning:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/squareup/print/EpsonTcpPrinterScout;->postScout()V

    :cond_0
    return-void
.end method

.method public registerOnBus(Lcom/squareup/badbus/BadBus;)Lio/reactivex/disposables/Disposable;
    .locals 1

    const-string v0, "bus"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-static {}, Lio/reactivex/disposables/Disposables;->disposed()Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string v0, "disposed()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method protected scout()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;"
        }
    .end annotation

    .line 64
    new-instance v0, Lcom/squareup/print/EpsonTcpPrinterScout$scout$1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/print/EpsonTcpPrinterScout$scout$1;-><init>(Lcom/squareup/print/EpsonTcpPrinterScout;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    const/4 v2, 0x1

    invoke-static {v1, v0, v2, v1}, Lkotlinx/coroutines/BuildersKt;->runBlocking$default(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public start()V
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/print/EpsonTcpPrinterScout;->scanning:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/squareup/print/EpsonTcpPrinterScout;->scanning:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 52
    invoke-virtual {p0}, Lcom/squareup/print/EpsonTcpPrinterScout;->postScout()V

    return-void
.end method

.method public stop()V
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/squareup/print/EpsonTcpPrinterScout;->scanning:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 57
    iget-object v0, p0, Lcom/squareup/print/EpsonTcpPrinterScout;->epsonDiscoverer:Lcom/squareup/printer/epson/EpsonDiscoverer;

    invoke-interface {v0}, Lcom/squareup/printer/epson/EpsonDiscoverer;->stopEpsonDiscovery()V

    return-void
.end method
