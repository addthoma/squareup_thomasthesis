.class public final enum Lcom/squareup/print/PrintJob$PrintAttempt$Result;
.super Ljava/lang/Enum;
.source "PrintJob.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/PrintJob$PrintAttempt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Result"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/print/PrintJob$PrintAttempt$Result;

.field public static final enum HARDWARE_PRINTER_NOT_AVAILABLE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

.field public static final enum NOT_YET_ATTEMPTED:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

.field public static final enum PRINTER_BUSY_FAILURE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

.field public static final enum PRINTER_COVER_OPEN:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

.field public static final enum PRINTER_CUTTER_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

.field public static final enum PRINTER_MECH_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

.field public static final enum PRINTER_OFFLINE_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

.field public static final enum PRINTER_PAPER_EMPTY:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

.field public static final enum PRINTER_PAPER_JAM_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

.field public static final enum PRINTER_RECEIVE_BUFFER_OVERFLOW_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

.field public static final enum PRINTER_UNRECOVERABLE_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

.field public static final enum SUCCESS:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

.field public static final enum TARGET_HAS_NO_PRINTER:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

.field public static final enum TARGET_UNRESOLVABLE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

.field public static final enum UNRELIABLE_FAILURE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 38
    new-instance v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/4 v1, 0x0

    const-string v2, "NOT_YET_ATTEMPTED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->NOT_YET_ATTEMPTED:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    .line 43
    new-instance v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/4 v2, 0x1

    const-string v3, "SUCCESS"

    invoke-direct {v0, v3, v2}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->SUCCESS:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    .line 49
    new-instance v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/4 v3, 0x2

    const-string v4, "TARGET_UNRESOLVABLE"

    invoke-direct {v0, v4, v3}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->TARGET_UNRESOLVABLE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    .line 54
    new-instance v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/4 v4, 0x3

    const-string v5, "TARGET_HAS_NO_PRINTER"

    invoke-direct {v0, v5, v4}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->TARGET_HAS_NO_PRINTER:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    .line 59
    new-instance v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/4 v5, 0x4

    const-string v6, "HARDWARE_PRINTER_NOT_AVAILABLE"

    invoke-direct {v0, v6, v5}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->HARDWARE_PRINTER_NOT_AVAILABLE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    .line 65
    new-instance v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/4 v6, 0x5

    const-string v7, "UNRELIABLE_FAILURE"

    invoke-direct {v0, v7, v6}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->UNRELIABLE_FAILURE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    .line 72
    new-instance v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/4 v7, 0x6

    const-string v8, "PRINTER_BUSY_FAILURE"

    invoke-direct {v0, v8, v7}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_BUSY_FAILURE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    .line 73
    new-instance v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/4 v8, 0x7

    const-string v9, "PRINTER_COVER_OPEN"

    invoke-direct {v0, v9, v8}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_COVER_OPEN:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    .line 74
    new-instance v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/16 v9, 0x8

    const-string v10, "PRINTER_PAPER_EMPTY"

    invoke-direct {v0, v10, v9}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_PAPER_EMPTY:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    .line 75
    new-instance v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/16 v10, 0x9

    const-string v11, "PRINTER_CUTTER_ERROR"

    invoke-direct {v0, v11, v10}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_CUTTER_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    .line 76
    new-instance v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/16 v11, 0xa

    const-string v12, "PRINTER_MECH_ERROR"

    invoke-direct {v0, v12, v11}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_MECH_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    .line 77
    new-instance v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/16 v12, 0xb

    const-string v13, "PRINTER_PAPER_JAM_ERROR"

    invoke-direct {v0, v13, v12}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_PAPER_JAM_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    .line 78
    new-instance v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/16 v13, 0xc

    const-string v14, "PRINTER_RECEIVE_BUFFER_OVERFLOW_ERROR"

    invoke-direct {v0, v14, v13}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_RECEIVE_BUFFER_OVERFLOW_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    .line 83
    new-instance v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/16 v14, 0xd

    const-string v15, "PRINTER_OFFLINE_ERROR"

    invoke-direct {v0, v15, v14}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_OFFLINE_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    .line 84
    new-instance v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/16 v15, 0xe

    const-string v14, "PRINTER_UNRECOVERABLE_ERROR"

    invoke-direct {v0, v14, v15}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_UNRECOVERABLE_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/16 v0, 0xf

    new-array v0, v0, [Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    .line 36
    sget-object v14, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->NOT_YET_ATTEMPTED:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    aput-object v14, v0, v1

    sget-object v1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->SUCCESS:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->TARGET_UNRESOLVABLE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->TARGET_HAS_NO_PRINTER:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->HARDWARE_PRINTER_NOT_AVAILABLE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->UNRELIABLE_FAILURE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_BUSY_FAILURE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_COVER_OPEN:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_PAPER_EMPTY:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_CUTTER_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_MECH_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_PAPER_JAM_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_RECEIVE_BUFFER_OVERFLOW_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_OFFLINE_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_UNRECOVERABLE_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    aput-object v1, v0, v15

    sput-object v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->$VALUES:[Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/print/PrintJob$PrintAttempt$Result;
    .locals 1

    .line 36
    const-class v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    return-object p0
.end method

.method public static values()[Lcom/squareup/print/PrintJob$PrintAttempt$Result;
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->$VALUES:[Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    invoke-virtual {v0}, [Lcom/squareup/print/PrintJob$PrintAttempt$Result;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    return-object v0
.end method
