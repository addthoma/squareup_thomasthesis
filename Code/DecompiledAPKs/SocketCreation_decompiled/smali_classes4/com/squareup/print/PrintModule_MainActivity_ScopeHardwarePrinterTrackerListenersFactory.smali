.class public final Lcom/squareup/print/PrintModule_MainActivity_ScopeHardwarePrinterTrackerListenersFactory;
.super Ljava/lang/Object;
.source "PrintModule_MainActivity_ScopeHardwarePrinterTrackerListenersFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lmortar/Scoped;",
        ">;"
    }
.end annotation


# instance fields
.field private final hardwarePrinterTrackerListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/print/HardwarePrinterTracker$Listener;",
            ">;>;"
        }
    .end annotation
.end field

.field private final hardwarePrinterTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/print/HardwarePrinterTracker$Listener;",
            ">;>;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/print/PrintModule_MainActivity_ScopeHardwarePrinterTrackerListenersFactory;->hardwarePrinterTrackerProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/print/PrintModule_MainActivity_ScopeHardwarePrinterTrackerListenersFactory;->hardwarePrinterTrackerListenersProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/PrintModule_MainActivity_ScopeHardwarePrinterTrackerListenersFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/print/HardwarePrinterTracker$Listener;",
            ">;>;)",
            "Lcom/squareup/print/PrintModule_MainActivity_ScopeHardwarePrinterTrackerListenersFactory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/print/PrintModule_MainActivity_ScopeHardwarePrinterTrackerListenersFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/print/PrintModule_MainActivity_ScopeHardwarePrinterTrackerListenersFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static scopeHardwarePrinterTrackerListeners(Lcom/squareup/print/HardwarePrinterTracker;Ljava/util/Set;)Lmortar/Scoped;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            "Ljava/util/Set<",
            "Lcom/squareup/print/HardwarePrinterTracker$Listener;",
            ">;)",
            "Lmortar/Scoped;"
        }
    .end annotation

    .line 44
    invoke-static {p0, p1}, Lcom/squareup/print/PrintModule$MainActivity;->scopeHardwarePrinterTrackerListeners(Lcom/squareup/print/HardwarePrinterTracker;Ljava/util/Set;)Lmortar/Scoped;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lmortar/Scoped;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/print/PrintModule_MainActivity_ScopeHardwarePrinterTrackerListenersFactory;->get()Lmortar/Scoped;

    move-result-object v0

    return-object v0
.end method

.method public get()Lmortar/Scoped;
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/print/PrintModule_MainActivity_ScopeHardwarePrinterTrackerListenersFactory;->hardwarePrinterTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/HardwarePrinterTracker;

    iget-object v1, p0, Lcom/squareup/print/PrintModule_MainActivity_ScopeHardwarePrinterTrackerListenersFactory;->hardwarePrinterTrackerListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-static {v0, v1}, Lcom/squareup/print/PrintModule_MainActivity_ScopeHardwarePrinterTrackerListenersFactory;->scopeHardwarePrinterTrackerListeners(Lcom/squareup/print/HardwarePrinterTracker;Ljava/util/Set;)Lmortar/Scoped;

    move-result-object v0

    return-object v0
.end method
