.class public final Lcom/squareup/print/PrinterEventKt;
.super Ljava/lang/Object;
.source "PrinterEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\u001a\u000e\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u001a\u0016\u0010\u0004\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006\u001a\u001e\u0010\u0004\u001a\u00020\u00012\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0006\u001a\u001c\u0010\t\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b\u001a\u001c\u0010\r\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b\u001a\u0016\u0010\u000e\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u0006\u001a\u000e\u0010\u0010\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u001a\u000e\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u0012\u001a\u00020\u0013\u00a8\u0006\u0014"
    }
    d2 = {
        "forPrintTest",
        "Lcom/squareup/print/PrinterEvent;",
        "hardwareInfo",
        "Lcom/squareup/print/HardwarePrinter$HardwareInfo;",
        "forPrinterBlocked",
        "detail",
        "",
        "jobId",
        "targetId",
        "forPrinterConnect",
        "stations",
        "",
        "Lcom/squareup/print/Station;",
        "forPrinterDisconnect",
        "forPrinterDisconnectPingHistory",
        "pingDataDetail",
        "forUnsupportedPrinterConnect",
        "getDetailString",
        "value",
        "",
        "hardware_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final forPrintTest(Lcom/squareup/print/HardwarePrinter$HardwareInfo;)Lcom/squareup/print/PrinterEvent;
    .locals 12

    const-string v0, "hardwareInfo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    new-instance v0, Lcom/squareup/print/PrinterEvent;

    sget-object v2, Lcom/squareup/eventstream/v1/EventStream$Name;->TAP:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PRINTER_TEST_PRINT:Lcom/squareup/analytics/RegisterTapName;

    iget-object v3, v1, Lcom/squareup/analytics/RegisterTapName;->value:Ljava/lang/String;

    const-string v1, "PRINTER_TEST_PRINT.value"

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xf8

    const/4 v11, 0x0

    move-object v1, v0

    move-object v4, p0

    invoke-direct/range {v1 .. v11}, Lcom/squareup/print/PrinterEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public static final forPrinterBlocked(Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;)Lcom/squareup/print/PrinterEvent;
    .locals 12

    const-string v0, "hardwareInfo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "detail"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    new-instance v0, Lcom/squareup/print/PrinterEvent;

    sget-object v2, Lcom/squareup/eventstream/v1/EventStream$Name;->ERROR:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINTER_BLOCKED:Lcom/squareup/analytics/RegisterActionName;

    iget-object v3, v1, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    const-string v1, "PRINTER_BLOCKED.value"

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xf0

    const/4 v11, 0x0

    move-object v1, v0

    move-object v4, p0

    move-object v5, p1

    invoke-direct/range {v1 .. v11}, Lcom/squareup/print/PrinterEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public static final forPrinterBlocked(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/print/PrinterEvent;
    .locals 12

    const-string v0, "jobId"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "targetId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "detail"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    new-instance v0, Lcom/squareup/print/PrinterEvent;

    sget-object v2, Lcom/squareup/eventstream/v1/EventStream$Name;->ERROR:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINTER_BLOCKED:Lcom/squareup/analytics/RegisterActionName;

    iget-object v3, v1, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    const-string v1, "PRINTER_BLOCKED.value"

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v10, 0x34

    const/4 v11, 0x0

    move-object v1, v0

    move-object v5, p2

    move-object v8, p1

    move-object v9, p0

    invoke-direct/range {v1 .. v11}, Lcom/squareup/print/PrinterEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public static final forPrinterConnect(Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/util/List;)Lcom/squareup/print/PrinterEvent;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/HardwarePrinter$HardwareInfo;",
            "Ljava/util/List<",
            "Lcom/squareup/print/Station;",
            ">;)",
            "Lcom/squareup/print/PrinterEvent;"
        }
    .end annotation

    const-string v0, "hardwareInfo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stations"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    new-instance v0, Lcom/squareup/print/PrinterEvent;

    sget-object v2, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINTER_CONNECT:Lcom/squareup/analytics/RegisterActionName;

    iget-object v3, v1, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    const-string v1, "PRINTER_CONNECT.value"

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xd8

    const/4 v11, 0x0

    move-object v1, v0

    move-object v4, p0

    move-object v7, p1

    invoke-direct/range {v1 .. v11}, Lcom/squareup/print/PrinterEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public static final forPrinterDisconnect(Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/util/List;)Lcom/squareup/print/PrinterEvent;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/HardwarePrinter$HardwareInfo;",
            "Ljava/util/List<",
            "Lcom/squareup/print/Station;",
            ">;)",
            "Lcom/squareup/print/PrinterEvent;"
        }
    .end annotation

    const-string v0, "hardwareInfo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stations"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    new-instance v0, Lcom/squareup/print/PrinterEvent;

    sget-object v2, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINTER_DISCONNECT:Lcom/squareup/analytics/RegisterActionName;

    iget-object v3, v1, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    const-string v1, "PRINTER_DISCONNECT.value"

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xd8

    const/4 v11, 0x0

    move-object v1, v0

    move-object v4, p0

    move-object v7, p1

    invoke-direct/range {v1 .. v11}, Lcom/squareup/print/PrinterEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public static final forPrinterDisconnectPingHistory(Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;)Lcom/squareup/print/PrinterEvent;
    .locals 12

    const-string v0, "hardwareInfo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pingDataDetail"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    new-instance v0, Lcom/squareup/print/PrinterEvent;

    .line 84
    sget-object v2, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINTER_DISCONNECT_TCP_PING_HISTORY:Lcom/squareup/analytics/RegisterActionName;

    iget-object v3, v1, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    const-string v1, "PRINTER_DISCONNECT_TCP_PING_HISTORY.value"

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xf0

    const/4 v11, 0x0

    move-object v1, v0

    move-object v4, p0

    move-object v5, p1

    .line 83
    invoke-direct/range {v1 .. v11}, Lcom/squareup/print/PrinterEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public static final forUnsupportedPrinterConnect(Lcom/squareup/print/HardwarePrinter$HardwareInfo;)Lcom/squareup/print/PrinterEvent;
    .locals 12

    const-string v0, "hardwareInfo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    new-instance v0, Lcom/squareup/print/PrinterEvent;

    .line 53
    sget-object v2, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINTER_CONNECT:Lcom/squareup/analytics/RegisterActionName;

    iget-object v3, v1, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    const-string v1, "PRINTER_CONNECT.value"

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xe8

    const/4 v11, 0x0

    move-object v1, v0

    move-object v4, p0

    .line 52
    invoke-direct/range {v1 .. v11}, Lcom/squareup/print/PrinterEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public static final getDetailString(Z)Ljava/lang/String;
    .locals 0

    if-eqz p0, :cond_0

    const-string p0, "ON"

    goto :goto_0

    :cond_0
    const-string p0, "OFF"

    :goto_0
    return-object p0
.end method
