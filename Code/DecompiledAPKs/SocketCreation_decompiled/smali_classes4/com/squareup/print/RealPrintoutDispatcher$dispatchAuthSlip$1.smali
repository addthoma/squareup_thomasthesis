.class final Lcom/squareup/print/RealPrintoutDispatcher$dispatchAuthSlip$1;
.super Ljava/lang/Object;
.source "PrintoutDispatcher.kt"

# interfaces
.implements Lio/reactivex/SingleOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/print/RealPrintoutDispatcher;->dispatchAuthSlip(Lcom/squareup/print/AuthSlipPrintout;Ljava/util/Collection;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/SingleOnSubscribe<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u001e\u0010\u0002\u001a\u001a\u0012\u0016\u0012\u0014 \u0006*\n\u0018\u00010\u0004j\u0004\u0018\u0001`\u00050\u0004j\u0002`\u00050\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/SingleEmitter;",
        "Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
        "Lcom/squareup/print/PrintJobResult;",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $printout:Lcom/squareup/print/AuthSlipPrintout;

.field final synthetic $target:Ljava/util/Collection;

.field final synthetic this$0:Lcom/squareup/print/RealPrintoutDispatcher;


# direct methods
.method constructor <init>(Lcom/squareup/print/RealPrintoutDispatcher;Lcom/squareup/print/AuthSlipPrintout;Ljava/util/Collection;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchAuthSlip$1;->this$0:Lcom/squareup/print/RealPrintoutDispatcher;

    iput-object p2, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchAuthSlip$1;->$printout:Lcom/squareup/print/AuthSlipPrintout;

    iput-object p3, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchAuthSlip$1;->$target:Ljava/util/Collection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/SingleEmitter;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/SingleEmitter<",
            "Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchAuthSlip$1;->this$0:Lcom/squareup/print/RealPrintoutDispatcher;

    invoke-static {v0}, Lcom/squareup/print/RealPrintoutDispatcher;->access$waitForEnqueuedJobResult(Lcom/squareup/print/RealPrintoutDispatcher;)Lio/reactivex/Single;

    move-result-object v0

    .line 133
    new-instance v1, Lcom/squareup/print/RealPrintoutDispatcher$dispatchAuthSlip$1$sub$1;

    invoke-direct {v1, p1}, Lcom/squareup/print/RealPrintoutDispatcher$dispatchAuthSlip$1$sub$1;-><init>(Lio/reactivex/SingleEmitter;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 134
    new-instance v1, Lcom/squareup/print/RealPrintoutDispatcher$dispatchAuthSlip$1$1;

    invoke-direct {v1, v0}, Lcom/squareup/print/RealPrintoutDispatcher$dispatchAuthSlip$1$1;-><init>(Lio/reactivex/disposables/Disposable;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    new-instance v0, Lcom/squareup/print/PrintoutDispatcherKt$sam$io_reactivex_functions_Cancellable$0;

    invoke-direct {v0, v1}, Lcom/squareup/print/PrintoutDispatcherKt$sam$io_reactivex_functions_Cancellable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lio/reactivex/functions/Cancellable;

    invoke-interface {p1, v0}, Lio/reactivex/SingleEmitter;->setCancellable(Lio/reactivex/functions/Cancellable;)V

    .line 136
    iget-object p1, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchAuthSlip$1;->$printout:Lcom/squareup/print/AuthSlipPrintout;

    invoke-virtual {p1}, Lcom/squareup/print/AuthSlipPrintout;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->getTenderForPrinting(Lcom/squareup/payment/PaymentReceipt;)Lcom/squareup/payment/tender/BaseTender;

    move-result-object v2

    .line 137
    iget-object p1, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchAuthSlip$1;->$printout:Lcom/squareup/print/AuthSlipPrintout;

    invoke-virtual {p1}, Lcom/squareup/print/AuthSlipPrintout;->getType()Lcom/squareup/print/PrintoutType;

    move-result-object p1

    sget-object v0, Lcom/squareup/print/PrintoutType;->MERCHANT_AUTH_SLIP_COPY:Lcom/squareup/print/PrintoutType;

    const/4 v1, 0x1

    const/4 v3, 0x0

    if-ne p1, v0, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    if-nez v4, :cond_1

    .line 138
    iget-object p1, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchAuthSlip$1;->$printout:Lcom/squareup/print/AuthSlipPrintout;

    invoke-virtual {p1}, Lcom/squareup/print/AuthSlipPrintout;->getExpectSlipCopy()Z

    move-result p1

    if-eqz p1, :cond_2

    :cond_1
    const/4 v3, 0x1

    .line 139
    :cond_2
    iget-object p1, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchAuthSlip$1;->this$0:Lcom/squareup/print/RealPrintoutDispatcher;

    invoke-static {p1}, Lcom/squareup/print/RealPrintoutDispatcher;->access$getOrderPrintingDispatcher$p(Lcom/squareup/print/RealPrintoutDispatcher;)Lcom/squareup/print/OrderPrintingDispatcher;

    move-result-object v0

    .line 140
    iget-object p1, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchAuthSlip$1;->$printout:Lcom/squareup/print/AuthSlipPrintout;

    invoke-virtual {p1}, Lcom/squareup/print/AuthSlipPrintout;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    iget-object v5, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchAuthSlip$1;->$target:Ljava/util/Collection;

    .line 139
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/print/OrderPrintingDispatcher;->printAuthSlip(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;ZZLjava/util/Collection;)V

    return-void
.end method
