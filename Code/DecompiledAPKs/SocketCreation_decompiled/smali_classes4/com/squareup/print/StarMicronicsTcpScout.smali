.class public Lcom/squareup/print/StarMicronicsTcpScout;
.super Lcom/squareup/print/PrinterScout;
.source "StarMicronicsTcpScout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;
    }
.end annotation


# static fields
.field private static final BROADCAST_TO_ALL_HOSTS:Ljava/lang/String; = "255.255.255.255"

.field static final LAST_SEEN_TIMEOUT_MS:I = 0xc350

.field private static final PING_RESPONSES_TO_KEEP:I = 0x14

.field private static final PRINTER_STATUS_PORT:I = 0x56ce

.field private static final RESPONSE_PACKET_IP_ADDRESS_START_OFFSET:I = 0x58

.field private static final RESPONSE_PACKET_MAC_ADDRESS_START_OFFSET:I = 0x4e

.field private static final RESPONSE_PACKET_MODEL_NAME_MAX_LENGTH:I = 0x40

.field private static final RESPONSE_PACKET_MODEL_NAME_START_OFFSET:I = 0xcc

.field private static final RESPONSE_PACKET_SIZE:I = 0x400

.field static final SCOUT_DURATION_MS:I = 0xbb8

.field private static final SEARCH_BROADCAST_PACKET:[B

.field private static final SEND_SEARCH_BROADCAST_PACKET_EVERY_MS:I = 0x1388

.field static final SOCKET_TIMEOUT_MS:I = 0x3e8


# instance fields
.field private final StarMicronicsPrinterFactory:Lcom/squareup/print/StarMicronicsPrinter$Factory;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final clock:Lcom/squareup/util/Clock;

.field private final context:Landroid/content/Context;

.field private expectingPingResponse:Z

.field private lastPingTime:J

.field private final mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private pingPacket:Ljava/net/DatagramPacket;

.field private final pingResponseLogHelper:Lcom/squareup/print/PingResponseLogHelper;

.field private printerResponses:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final scanning:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private socket:Ljava/net/DatagramSocket;

.field private final socketProvider:Lcom/squareup/print/SocketProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x1c

    new-array v0, v0, [B

    .line 42
    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/print/StarMicronicsTcpScout;->SEARCH_BROADCAST_PACKET:[B

    return-void

    :array_0
    .array-data 1
        0x53t
        0x54t
        0x52t
        0x5ft
        0x42t
        0x43t
        0x41t
        0x53t
        0x54t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x52t
        0x51t
        0x31t
        0x2et
        0x30t
        0x2et
        0x30t
        0x0t
        0x0t
        0x1ct
        0x64t
        0x31t
    .end array-data
.end method

.method constructor <init>(Landroid/app/Application;Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/print/SocketProvider;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/print/StarMicronicsPrinter$Factory;)V
    .locals 0

    .line 97
    invoke-direct {p0, p2, p4, p7}, Lcom/squareup/print/PrinterScout;-><init>(Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/Features;)V

    .line 98
    iput-object p1, p0, Lcom/squareup/print/StarMicronicsTcpScout;->context:Landroid/content/Context;

    .line 99
    iput-object p3, p0, Lcom/squareup/print/StarMicronicsTcpScout;->socketProvider:Lcom/squareup/print/SocketProvider;

    .line 100
    iput-object p5, p0, Lcom/squareup/print/StarMicronicsTcpScout;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    .line 101
    iput-object p6, p0, Lcom/squareup/print/StarMicronicsTcpScout;->clock:Lcom/squareup/util/Clock;

    .line 102
    iput-object p8, p0, Lcom/squareup/print/StarMicronicsTcpScout;->analytics:Lcom/squareup/analytics/Analytics;

    .line 103
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object p1, p0, Lcom/squareup/print/StarMicronicsTcpScout;->scanning:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 104
    new-instance p1, Lcom/squareup/print/PingResponseLogHelper;

    const/16 p2, 0x14

    invoke-direct {p1, p6, p2}, Lcom/squareup/print/PingResponseLogHelper;-><init>(Lcom/squareup/util/Clock;I)V

    iput-object p1, p0, Lcom/squareup/print/StarMicronicsTcpScout;->pingResponseLogHelper:Lcom/squareup/print/PingResponseLogHelper;

    .line 105
    iput-object p9, p0, Lcom/squareup/print/StarMicronicsTcpScout;->StarMicronicsPrinterFactory:Lcom/squareup/print/StarMicronicsPrinter$Factory;

    .line 107
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/print/StarMicronicsTcpScout;->printerResponses:Ljava/util/Map;

    return-void
.end method

.method private closeSocket()V
    .locals 2

    .line 202
    iget-object v0, p0, Lcom/squareup/print/StarMicronicsTcpScout;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    const-string v1, "Must be called on the printer\'s background thread!"

    invoke-interface {v0, v1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->forbid(Ljava/lang/String;)V

    .line 203
    iget-object v0, p0, Lcom/squareup/print/StarMicronicsTcpScout;->socket:Ljava/net/DatagramSocket;

    if-nez v0, :cond_0

    return-void

    .line 204
    :cond_0
    invoke-virtual {v0}, Ljava/net/DatagramSocket;->close()V

    const/4 v0, 0x0

    .line 205
    iput-object v0, p0, Lcom/squareup/print/StarMicronicsTcpScout;->socket:Ljava/net/DatagramSocket;

    return-void
.end method

.method private getNextPrinterResponse()Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;
    .locals 6

    .line 215
    invoke-direct {p0}, Lcom/squareup/print/StarMicronicsTcpScout;->sendPingIfNeeded()V

    const/16 v0, 0x400

    new-array v0, v0, [B

    .line 218
    new-instance v1, Ljava/net/DatagramPacket;

    array-length v2, v0

    invoke-direct {v1, v0, v2}, Ljava/net/DatagramPacket;-><init>([BI)V

    .line 220
    invoke-direct {p0, v1}, Lcom/squareup/print/StarMicronicsTcpScout;->getPacket(Ljava/net/DatagramPacket;)Z

    move-result v0

    const/4 v2, 0x0

    if-nez v0, :cond_0

    return-object v2

    .line 224
    :cond_0
    invoke-virtual {v1}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v0

    const/16 v3, 0x58

    aget-byte v0, v0, v3

    and-int/lit16 v0, v0, 0xff

    if-nez v0, :cond_1

    return-object v2

    .line 230
    :cond_1
    invoke-virtual {v1}, Ljava/net/DatagramPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    .line 231
    iget-object v2, p0, Lcom/squareup/print/StarMicronicsTcpScout;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v2}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v2

    .line 232
    new-instance v4, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;

    invoke-virtual {v1}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v1

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;-><init>(Ljava/lang/String;[BJ)V

    .line 233
    iget-wide v0, p0, Lcom/squareup/print/StarMicronicsTcpScout;->lastPingTime:J

    sub-long/2addr v2, v0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 235
    iget-object v5, v4, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->id:Ljava/lang/String;

    aput-object v5, v0, v1

    const/4 v1, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v0, v1

    const-string v1, "Printer %s responded %d ms after ping."

    invoke-virtual {p0, v1, v0}, Lcom/squareup/print/StarMicronicsTcpScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 236
    iget-object v0, p0, Lcom/squareup/print/StarMicronicsTcpScout;->pingResponseLogHelper:Lcom/squareup/print/PingResponseLogHelper;

    invoke-virtual {v4}, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/print/PingResponseLogHelper;->recordPingResponse(Ljava/lang/String;Ljava/lang/Long;)V

    return-object v4
.end method

.method private getPacket(Ljava/net/DatagramPacket;)Z
    .locals 2

    .line 269
    iget-object v0, p0, Lcom/squareup/print/StarMicronicsTcpScout;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    const-string v1, "Must be called on the printer\'s background thread!"

    invoke-interface {v0, v1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->forbid(Ljava/lang/String;)V

    .line 270
    iget-object v0, p0, Lcom/squareup/print/StarMicronicsTcpScout;->socket:Ljava/net/DatagramSocket;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 273
    :cond_0
    :try_start_0
    invoke-virtual {v0, p1}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    return v1
.end method

.method private getResults()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;"
        }
    .end annotation

    .line 303
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/print/StarMicronicsTcpScout;->printerResponses:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 305
    iget-object v1, p0, Lcom/squareup/print/StarMicronicsTcpScout;->printerResponses:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;

    .line 306
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "TCP:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v2, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->printerReportedIpAddress:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 307
    iget-object v3, v2, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->model:Ljava/lang/String;

    invoke-static {v3}, Lcom/squareup/print/StarMicronicsPrinter;->getCleanModelName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 308
    iget-object v5, p0, Lcom/squareup/print/StarMicronicsTcpScout;->StarMicronicsPrinterFactory:Lcom/squareup/print/StarMicronicsPrinter$Factory;

    iget-object v6, v2, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->manufacturer:Ljava/lang/String;

    sget-object v8, Lcom/squareup/print/ConnectionType;->TCP:Lcom/squareup/print/ConnectionType;

    iget-object v9, v2, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->macAddress:Ljava/lang/String;

    iget-object v11, p0, Lcom/squareup/print/StarMicronicsTcpScout;->context:Landroid/content/Context;

    invoke-virtual/range {v5 .. v11}, Lcom/squareup/print/StarMicronicsPrinter$Factory;->create(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/ConnectionType;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Lcom/squareup/print/StarMicronicsPrinter;

    move-result-object v2

    .line 310
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private openSocketIfNone()V
    .locals 5

    .line 172
    iget-object v0, p0, Lcom/squareup/print/StarMicronicsTcpScout;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    const-string v1, "Must be called on the printer\'s background thread!"

    invoke-interface {v0, v1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->forbid(Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/squareup/print/StarMicronicsTcpScout;->socket:Ljava/net/DatagramSocket;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 176
    :try_start_0
    iget-object v2, p0, Lcom/squareup/print/StarMicronicsTcpScout;->socketProvider:Lcom/squareup/print/SocketProvider;

    invoke-interface {v2}, Lcom/squareup/print/SocketProvider;->datagramSocket()Ljava/net/DatagramSocket;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/print/StarMicronicsTcpScout;->socket:Ljava/net/DatagramSocket;

    .line 177
    iget-object v2, p0, Lcom/squareup/print/StarMicronicsTcpScout;->socket:Ljava/net/DatagramSocket;

    const/16 v3, 0x3e8

    invoke-virtual {v2, v3}, Ljava/net/DatagramSocket;->setSoTimeout(I)V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    new-array v3, v1, [Ljava/lang/Object;

    .line 179
    invoke-virtual {v2}, Ljava/net/SocketException;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v0

    const-string v2, "Error setting socket timeout value: %s"

    invoke-virtual {p0, v2, v3}, Lcom/squareup/print/StarMicronicsTcpScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v2, 0x0

    .line 180
    iput-object v2, p0, Lcom/squareup/print/StarMicronicsTcpScout;->socket:Ljava/net/DatagramSocket;

    :goto_0
    :try_start_1
    const-string v2, "255.255.255.255"

    .line 185
    invoke-static {v2}, Ljava/net/Inet4Address;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_1

    .line 192
    new-instance v1, Ljava/net/DatagramPacket;

    sget-object v2, Lcom/squareup/print/StarMicronicsTcpScout;->SEARCH_BROADCAST_PACKET:[B

    array-length v3, v2

    const/16 v4, 0x56ce

    invoke-direct {v1, v2, v3, v0, v4}, Ljava/net/DatagramPacket;-><init>([BILjava/net/InetAddress;I)V

    iput-object v1, p0, Lcom/squareup/print/StarMicronicsTcpScout;->pingPacket:Ljava/net/DatagramPacket;

    return-void

    :catch_1
    move-exception v2

    new-array v1, v1, [Ljava/lang/Object;

    .line 187
    invoke-virtual {v2}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    const-string v0, "Error getting broadcast address: %s"

    invoke-virtual {p0, v0, v1}, Lcom/squareup/print/StarMicronicsTcpScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 188
    invoke-direct {p0}, Lcom/squareup/print/StarMicronicsTcpScout;->closeSocket()V

    return-void
.end method

.method private removeNotRecentlySeenPrinterResponses()V
    .locals 10

    .line 241
    iget-object v0, p0, Lcom/squareup/print/StarMicronicsTcpScout;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v0

    .line 243
    iget-object v2, p0, Lcom/squareup/print/StarMicronicsTcpScout;->printerResponses:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 244
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 245
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 246
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 247
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;

    iget-wide v5, v5, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->lastSeenTime:J

    sub-long v5, v0, v5

    const-wide/32 v7, 0xc350

    cmp-long v9, v5, v7

    if-lez v9, :cond_0

    .line 251
    iget-object v7, p0, Lcom/squareup/print/StarMicronicsTcpScout;->pingResponseLogHelper:Lcom/squareup/print/PingResponseLogHelper;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;

    iget-object v8, v8, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->id:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/squareup/print/PingResponseLogHelper;->stringify(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v4, v8, v9

    const/4 v9, 0x1

    .line 253
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v8, v9

    const/4 v5, 0x2

    aput-object v7, v8, v5

    const-string v5, "Printer %s hasn\'t been seen in %d ms. Disconnecting.\nPrinter ping history: %s"

    .line 252
    invoke-static {v5, v8}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 254
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 255
    iget-object v5, p0, Lcom/squareup/print/StarMicronicsTcpScout;->printerResponses:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    iget-object v5, p0, Lcom/squareup/print/StarMicronicsTcpScout;->analytics:Lcom/squareup/analytics/Analytics;

    .line 257
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;

    iget-object v3, v3, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    invoke-static {v3, v7}, Lcom/squareup/print/PrinterEventKt;->forPrinterDisconnectPingHistory(Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;)Lcom/squareup/print/PrinterEvent;

    move-result-object v3

    .line 256
    invoke-interface {v5, v3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 258
    iget-object v3, p0, Lcom/squareup/print/StarMicronicsTcpScout;->pingResponseLogHelper:Lcom/squareup/print/PingResponseLogHelper;

    invoke-virtual {v3, v4}, Lcom/squareup/print/PingResponseLogHelper;->removePrinter(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private sendPingIfNeeded()V
    .locals 6

    .line 284
    iget-object v0, p0, Lcom/squareup/print/StarMicronicsTcpScout;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    const-string v1, "Must be called on the printer\'s background thread!"

    invoke-interface {v0, v1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->forbid(Ljava/lang/String;)V

    .line 285
    iget-object v0, p0, Lcom/squareup/print/StarMicronicsTcpScout;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/squareup/print/StarMicronicsTcpScout;->lastPingTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1388

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    .line 287
    iget-object v2, p0, Lcom/squareup/print/StarMicronicsTcpScout;->socket:Ljava/net/DatagramSocket;

    const/4 v3, 0x0

    if-nez v2, :cond_0

    new-array v0, v3, [Ljava/lang/Object;

    const-string v1, "Can\'t send ping because of null socket."

    .line 288
    invoke-virtual {p0, v1, v0}, Lcom/squareup/print/StarMicronicsTcpScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    const/4 v2, 0x1

    :try_start_0
    const-string v4, "Sending ping. (Last ping was %d ms ago.)"

    new-array v5, v2, [Ljava/lang/Object;

    .line 292
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v3

    invoke-virtual {p0, v4, v5}, Lcom/squareup/print/StarMicronicsTcpScout;->verbose(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 293
    iget-object v0, p0, Lcom/squareup/print/StarMicronicsTcpScout;->socket:Ljava/net/DatagramSocket;

    iget-object v1, p0, Lcom/squareup/print/StarMicronicsTcpScout;->pingPacket:Ljava/net/DatagramPacket;

    invoke-virtual {v0, v1}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V

    .line 294
    iget-object v0, p0, Lcom/squareup/print/StarMicronicsTcpScout;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/print/StarMicronicsTcpScout;->lastPingTime:J

    .line 295
    iput-boolean v2, p0, Lcom/squareup/print/StarMicronicsTcpScout;->expectingPingResponse:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-array v1, v2, [Ljava/lang/Object;

    .line 297
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v3

    const-string v0, "Error sending ping: %s"

    invoke-virtual {p0, v0, v1}, Lcom/squareup/print/StarMicronicsTcpScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private sendResultsNow()V
    .locals 1

    .line 316
    invoke-direct {p0}, Lcom/squareup/print/StarMicronicsTcpScout;->getResults()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/print/StarMicronicsTcpScout;->postResults(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method protected getConnectionType()Lcom/squareup/print/ConnectionType;
    .locals 1

    .line 130
    sget-object v0, Lcom/squareup/print/ConnectionType;->TCP:Lcom/squareup/print/ConnectionType;

    return-object v0
.end method

.method protected onScoutingDone()V
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/print/StarMicronicsTcpScout;->scanning:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/print/StarMicronicsTcpScout;->postScout()V

    :cond_0
    return-void
.end method

.method public registerOnBus(Lcom/squareup/badbus/BadBus;)Lio/reactivex/disposables/Disposable;
    .locals 0

    .line 111
    invoke-static {}, Lio/reactivex/disposables/Disposables;->disposed()Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method protected scout()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;"
        }
    .end annotation

    .line 134
    iget-object v0, p0, Lcom/squareup/print/StarMicronicsTcpScout;->scanning:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/print/StarMicronicsTcpScout;->getResults()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/squareup/print/StarMicronicsTcpScout;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v0

    .line 138
    invoke-direct {p0}, Lcom/squareup/print/StarMicronicsTcpScout;->openSocketIfNone()V

    .line 142
    :cond_1
    invoke-direct {p0}, Lcom/squareup/print/StarMicronicsTcpScout;->getNextPrinterResponse()Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;

    move-result-object v2

    if-nez v2, :cond_2

    .line 147
    iget-boolean v2, p0, Lcom/squareup/print/StarMicronicsTcpScout;->expectingPingResponse:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x0

    .line 148
    iput-boolean v2, p0, Lcom/squareup/print/StarMicronicsTcpScout;->expectingPingResponse:Z

    .line 149
    iget-object v2, p0, Lcom/squareup/print/StarMicronicsTcpScout;->pingResponseLogHelper:Lcom/squareup/print/PingResponseLogHelper;

    invoke-virtual {v2}, Lcom/squareup/print/PingResponseLogHelper;->pingResponseTimedOut()V

    goto :goto_0

    .line 157
    :cond_2
    iget-object v3, v2, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->networkIpAddress:Ljava/lang/String;

    iget-object v4, v2, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->printerReportedIpAddress:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    goto :goto_0

    .line 161
    :cond_3
    iget-object v3, p0, Lcom/squareup/print/StarMicronicsTcpScout;->printerResponses:Ljava/util/Map;

    iget-object v4, v2, Lcom/squareup/print/StarMicronicsTcpScout$PrinterResponse;->id:Ljava/lang/String;

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    invoke-direct {p0}, Lcom/squareup/print/StarMicronicsTcpScout;->sendResultsNow()V

    .line 163
    :cond_4
    :goto_0
    iget-object v2, p0, Lcom/squareup/print/StarMicronicsTcpScout;->scanning:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/squareup/print/StarMicronicsTcpScout;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v2}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, v0

    const-wide/16 v4, 0xbb8

    cmp-long v6, v2, v4

    if-ltz v6, :cond_1

    .line 165
    :cond_5
    invoke-direct {p0}, Lcom/squareup/print/StarMicronicsTcpScout;->removeNotRecentlySeenPrinterResponses()V

    .line 167
    invoke-direct {p0}, Lcom/squareup/print/StarMicronicsTcpScout;->getResults()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected shutdownInBackground()V
    .locals 0

    .line 198
    invoke-direct {p0}, Lcom/squareup/print/StarMicronicsTcpScout;->closeSocket()V

    return-void
.end method

.method public start()V
    .locals 2

    .line 115
    iget-object v0, p0, Lcom/squareup/print/StarMicronicsTcpScout;->scanning:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 116
    invoke-virtual {p0}, Lcom/squareup/print/StarMicronicsTcpScout;->postScout()V

    return-void
.end method

.method public stop()V
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/squareup/print/StarMicronicsTcpScout;->scanning:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 121
    invoke-virtual {p0}, Lcom/squareup/print/StarMicronicsTcpScout;->cancelPendingScouting()V

    return-void
.end method
