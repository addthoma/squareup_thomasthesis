.class public Lcom/squareup/print/TimecardsPrintingDispatcher;
.super Ljava/lang/Object;
.source "TimecardsPrintingDispatcher.java"


# instance fields
.field private final printSpooler:Lcom/squareup/print/PrintSpooler;

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private final res:Lcom/squareup/util/Res;

.field private final timecardsSummaryPayloadFactory:Lcom/squareup/print/payload/TimecardsSummaryPayloadFactory;


# direct methods
.method public constructor <init>(Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/payload/TimecardsSummaryPayloadFactory;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/print/TimecardsPrintingDispatcher;->printSpooler:Lcom/squareup/print/PrintSpooler;

    .line 23
    iput-object p2, p0, Lcom/squareup/print/TimecardsPrintingDispatcher;->printerStations:Lcom/squareup/print/PrinterStations;

    .line 24
    iput-object p3, p0, Lcom/squareup/print/TimecardsPrintingDispatcher;->timecardsSummaryPayloadFactory:Lcom/squareup/print/payload/TimecardsSummaryPayloadFactory;

    .line 25
    iput-object p4, p0, Lcom/squareup/print/TimecardsPrintingDispatcher;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method private dispatchTimecardsSummary(Lcom/squareup/print/PrintablePayload;Ljava/lang/String;Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrintablePayload;",
            "Ljava/lang/String;",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;)V"
        }
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/squareup/print/TimecardsPrintingDispatcher;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->timecards_receipt_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 48
    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrinterStation;

    .line 49
    iget-object v2, p0, Lcom/squareup/print/TimecardsPrintingDispatcher;->printSpooler:Lcom/squareup/print/PrintSpooler;

    invoke-virtual {v2, v1, p1, v0, p2}, Lcom/squareup/print/PrintSpooler;->enqueueForPrint(Lcom/squareup/print/PrintTarget;Lcom/squareup/print/PrintablePayload;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public canPrintTimecardsSummary()Z
    .locals 4

    .line 29
    iget-object v0, p0, Lcom/squareup/print/TimecardsPrintingDispatcher;->printerStations:Lcom/squareup/print/PrinterStations;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/print/PrinterStation$Role;

    sget-object v2, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v0

    return v0
.end method

.method public printTimecardsSummary(Lcom/squareup/permissions/Employee;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;)V
    .locals 2

    .line 33
    invoke-virtual {p0}, Lcom/squareup/print/TimecardsPrintingDispatcher;->canPrintTimecardsSummary()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/squareup/print/TimecardsPrintingDispatcher;->timecardsSummaryPayloadFactory:Lcom/squareup/print/payload/TimecardsSummaryPayloadFactory;

    .line 39
    invoke-virtual {v0, p1, p2}, Lcom/squareup/print/payload/TimecardsSummaryPayloadFactory;->fromEmployeeAndShiftSummary(Lcom/squareup/permissions/Employee;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;)Lcom/squareup/print/payload/TimecardsSummaryPayload;

    move-result-object p2

    .line 41
    iget-object p1, p1, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/print/TimecardsPrintingDispatcher;->printerStations:Lcom/squareup/print/PrinterStations;

    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    .line 42
    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->getEnabledStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;

    move-result-object v0

    .line 41
    invoke-direct {p0, p2, p1, v0}, Lcom/squareup/print/TimecardsPrintingDispatcher;->dispatchTimecardsSummary(Lcom/squareup/print/PrintablePayload;Ljava/lang/String;Ljava/util/Collection;)V

    return-void
.end method
