.class public Lcom/squareup/print/RegisterHardwarePrinterExecutor;
.super Ljava/lang/Object;
.source "RegisterHardwarePrinterExecutor.java"

# interfaces
.implements Lcom/squareup/print/HardwarePrinterExecutor;


# instance fields
.field final currentlyInUseHardwarePrinterIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final threadPoolExecutor:Ljava/util/concurrent/ExecutorService;


# direct methods
.method constructor <init>(Ljava/util/concurrent/ExecutorService;Lcom/squareup/thread/executor/MainThread;)V
    .locals 1

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/print/RegisterHardwarePrinterExecutor;->currentlyInUseHardwarePrinterIds:Ljava/util/Set;

    .line 25
    iput-object p1, p0, Lcom/squareup/print/RegisterHardwarePrinterExecutor;->threadPoolExecutor:Ljava/util/concurrent/ExecutorService;

    .line 26
    iput-object p2, p0, Lcom/squareup/print/RegisterHardwarePrinterExecutor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-void
.end method


# virtual methods
.method public claimHardwarePrinterAndExecute(Lcom/squareup/print/HardwarePrinter;Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 2

    .line 32
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    const-string v0, "hardwarePrinter"

    .line 33
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string/jumbo v0, "work"

    .line 34
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "afterWorkForMainThread"

    .line 35
    invoke-static {p3, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 38
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object p1

    .line 39
    iget-object v0, p0, Lcom/squareup/print/RegisterHardwarePrinterExecutor;->currentlyInUseHardwarePrinterIds:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/squareup/print/RegisterHardwarePrinterExecutor;->currentlyInUseHardwarePrinterIds:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "[Print_HardwarePrinterExecutor_claimAndExecute] HardwarePrinter %s is now claimed."

    .line 46
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 49
    iget-object p1, p0, Lcom/squareup/print/RegisterHardwarePrinterExecutor;->threadPoolExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/squareup/print/-$$Lambda$RegisterHardwarePrinterExecutor$hpuPFeQV2rN0oRRZWu0UG_e9ulA;

    invoke-direct {v0, p0, p2, p3}, Lcom/squareup/print/-$$Lambda$RegisterHardwarePrinterExecutor$hpuPFeQV2rN0oRRZWu0UG_e9ulA;-><init>(Lcom/squareup/print/RegisterHardwarePrinterExecutor;Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    invoke-interface {p1, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void

    .line 40
    :cond_0
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "[Print_HardwarePrinterExecutor] Cannot claim hardware printer "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", hardware printer currently claimed!"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public isHardwarePrinterClaimed(Lcom/squareup/print/HardwarePrinter;)Z
    .locals 1

    .line 65
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    const-string v0, "hardwarePrinter"

    .line 66
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 67
    iget-object v0, p0, Lcom/squareup/print/RegisterHardwarePrinterExecutor;->currentlyInUseHardwarePrinterIds:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public synthetic lambda$claimHardwarePrinterAndExecute$0$RegisterHardwarePrinterExecutor(Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 0

    .line 50
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 51
    iget-object p1, p0, Lcom/squareup/print/RegisterHardwarePrinterExecutor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-interface {p1, p2}, Lcom/squareup/thread/executor/MainThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method public releaseHardwarePrinter(Lcom/squareup/print/HardwarePrinter;)V
    .locals 2

    .line 56
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    const-string v0, "hardwarePrinter"

    .line 57
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 58
    iget-object v0, p0, Lcom/squareup/print/RegisterHardwarePrinterExecutor;->currentlyInUseHardwarePrinterIds:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 60
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "[Print_HardwarePrinterExecutor_release] HardwarePrinter %s is now released."

    .line 59
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
