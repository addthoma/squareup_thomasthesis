.class public final Lcom/squareup/print/RealPrinterStations_Factory;
.super Ljava/lang/Object;
.source "RealPrinterStations_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/RealPrinterStations;",
        ">;"
    }
.end annotation


# instance fields
.field private final printerStationFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStationFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final stationUuidsSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final ticketAutoNumberingEnabledProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStationFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/print/RealPrinterStations_Factory;->printerStationFactoryProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/print/RealPrinterStations_Factory;->stationUuidsSettingProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/print/RealPrinterStations_Factory;->resProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/print/RealPrinterStations_Factory;->ticketAutoNumberingEnabledProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/RealPrinterStations_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStationFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;)",
            "Lcom/squareup/print/RealPrinterStations_Factory;"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/squareup/print/RealPrinterStations_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/print/RealPrinterStations_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/print/PrinterStationFactory;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/util/Res;Lcom/f2prateek/rx/preferences2/Preference;)Lcom/squareup/print/RealPrinterStations;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrinterStationFactory;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/squareup/util/Res;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/print/RealPrinterStations;"
        }
    .end annotation

    .line 51
    new-instance v0, Lcom/squareup/print/RealPrinterStations;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/print/RealPrinterStations;-><init>(Lcom/squareup/print/PrinterStationFactory;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/util/Res;Lcom/f2prateek/rx/preferences2/Preference;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/print/RealPrinterStations;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/print/RealPrinterStations_Factory;->printerStationFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/PrinterStationFactory;

    iget-object v1, p0, Lcom/squareup/print/RealPrinterStations_Factory;->stationUuidsSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v2, p0, Lcom/squareup/print/RealPrinterStations_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/print/RealPrinterStations_Factory;->ticketAutoNumberingEnabledProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/f2prateek/rx/preferences2/Preference;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/print/RealPrinterStations_Factory;->newInstance(Lcom/squareup/print/PrinterStationFactory;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/util/Res;Lcom/f2prateek/rx/preferences2/Preference;)Lcom/squareup/print/RealPrinterStations;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/print/RealPrinterStations_Factory;->get()Lcom/squareup/print/RealPrinterStations;

    move-result-object v0

    return-object v0
.end method
