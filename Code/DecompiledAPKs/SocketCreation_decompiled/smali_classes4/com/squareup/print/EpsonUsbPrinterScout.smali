.class public final Lcom/squareup/print/EpsonUsbPrinterScout;
.super Lcom/squareup/print/PrinterScout;
.source "EpsonUsbPrinterScout.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEpsonUsbPrinterScout.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EpsonUsbPrinterScout.kt\ncom/squareup/print/EpsonUsbPrinterScout\n*L\n1#1,80:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0008\u0010\u000f\u001a\u00020\u0010H\u0014J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0012H\u0002J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u000e\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u001e0\u001dH\u0014J\u0008\u0010\u001f\u001a\u00020\u0015H\u0016J\u0008\u0010 \u001a\u00020\u0015H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/print/EpsonUsbPrinterScout;",
        "Lcom/squareup/print/PrinterScout;",
        "usbManager",
        "Lcom/squareup/hardware/usb/UsbManager;",
        "epsonPrinterFactory",
        "Lcom/squareup/printer/epson/EpsonPrinter$Factory;",
        "backgroundExecutor",
        "Lcom/squareup/thread/executor/StoppableSerialExecutor;",
        "mainThread",
        "Lcom/squareup/thread/executor/MainThread;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/printer/epson/EpsonPrinter$Factory;Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/Features;)V",
        "started",
        "Ljava/util/concurrent/atomic/AtomicBoolean;",
        "getConnectionType",
        "Lcom/squareup/print/ConnectionType;",
        "getUsbTarget",
        "",
        "deviceName",
        "onUsbUpdated",
        "",
        "event",
        "Lcom/squareup/hardware/UsbDevicesChangedEvent;",
        "registerOnBus",
        "Lio/reactivex/disposables/Disposable;",
        "bus",
        "Lcom/squareup/badbus/BadBus;",
        "scout",
        "",
        "Lcom/squareup/print/HardwarePrinter;",
        "start",
        "stop",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final epsonPrinterFactory:Lcom/squareup/printer/epson/EpsonPrinter$Factory;

.field private final started:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final usbManager:Lcom/squareup/hardware/usb/UsbManager;


# direct methods
.method public constructor <init>(Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/printer/epson/EpsonPrinter$Factory;Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/Features;)V
    .locals 1

    const-string v0, "usbManager"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "epsonPrinterFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backgroundExecutor"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThread"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0, p3, p4, p5}, Lcom/squareup/print/PrinterScout;-><init>(Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/Features;)V

    iput-object p1, p0, Lcom/squareup/print/EpsonUsbPrinterScout;->usbManager:Lcom/squareup/hardware/usb/UsbManager;

    iput-object p2, p0, Lcom/squareup/print/EpsonUsbPrinterScout;->epsonPrinterFactory:Lcom/squareup/printer/epson/EpsonPrinter$Factory;

    .line 24
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/squareup/print/EpsonUsbPrinterScout;->started:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public static final synthetic access$onUsbUpdated(Lcom/squareup/print/EpsonUsbPrinterScout;Lcom/squareup/hardware/UsbDevicesChangedEvent;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1}, Lcom/squareup/print/EpsonUsbPrinterScout;->onUsbUpdated(Lcom/squareup/hardware/UsbDevicesChangedEvent;)V

    return-void
.end method

.method private final getUsbTarget(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "USB:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final onUsbUpdated(Lcom/squareup/hardware/UsbDevicesChangedEvent;)V
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/print/EpsonUsbPrinterScout;->started:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean p1, p1, Lcom/squareup/hardware/UsbDevicesChangedEvent;->attached:Z

    if-nez p1, :cond_1

    .line 76
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/print/EpsonUsbPrinterScout;->postScout()V

    :cond_1
    return-void
.end method


# virtual methods
.method protected getConnectionType()Lcom/squareup/print/ConnectionType;
    .locals 1

    .line 37
    sget-object v0, Lcom/squareup/print/ConnectionType;->USB:Lcom/squareup/print/ConnectionType;

    return-object v0
.end method

.method public registerOnBus(Lcom/squareup/badbus/BadBus;)Lio/reactivex/disposables/Disposable;
    .locals 2

    const-string v0, "bus"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    const-class v0, Lcom/squareup/hardware/UsbDevicesChangedEvent;

    invoke-virtual {p1, v0}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    .line 69
    new-instance v0, Lcom/squareup/print/EpsonUsbPrinterScout$registerOnBus$1;

    move-object v1, p0

    check-cast v1, Lcom/squareup/print/EpsonUsbPrinterScout;

    invoke-direct {v0, v1}, Lcom/squareup/print/EpsonUsbPrinterScout$registerOnBus$1;-><init>(Lcom/squareup/print/EpsonUsbPrinterScout;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    new-instance v1, Lcom/squareup/print/EpsonUsbPrinterScout$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v1, v0}, Lcom/squareup/print/EpsonUsbPrinterScout$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string v0, "bus.events(UsbDevicesCha\u2026subscribe(::onUsbUpdated)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method protected scout()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/squareup/print/EpsonUsbPrinterScout;->usbManager:Lcom/squareup/hardware/usb/UsbManager;

    invoke-interface {v0}, Lcom/squareup/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object v0

    .line 42
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/List;

    .line 44
    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_0
    const-string v2, "deviceList"

    .line 48
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/usb/UsbDevice;

    .line 49
    sget-object v4, Lcom/squareup/printer/epson/EpsonPrinters;->INSTANCE:Lcom/squareup/printer/epson/EpsonPrinters;

    invoke-virtual {v4}, Lcom/squareup/printer/epson/EpsonPrinters;->getEpsonUsbDeviceMapping()Ljava/util/Map;

    move-result-object v4

    const-string v5, "usbDevice"

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v8, v4

    check-cast v8, Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    if-eqz v8, :cond_1

    const-string v4, "deviceName"

    .line 52
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/squareup/print/EpsonUsbPrinterScout;->getUsbTarget(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 53
    invoke-virtual {v2}, Landroid/hardware/usb/UsbDevice;->getProductName()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    if-eqz v3, :cond_2

    move-object v7, v3

    goto :goto_1

    :cond_2
    move-object v7, v4

    .line 55
    :goto_1
    sget-object v9, Lcom/squareup/print/ConnectionType;->USB:Lcom/squareup/print/ConnectionType;

    const/4 v10, 0x0

    .line 56
    invoke-virtual {v2}, Landroid/hardware/usb/UsbDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    move-object v11, v2

    goto :goto_2

    :cond_3
    move-object v11, v4

    :goto_2
    const/16 v12, 0x10

    const/4 v13, 0x0

    .line 51
    new-instance v2, Lcom/squareup/printer/epson/EpsonPrinterInfo;

    move-object v5, v2

    invoke-direct/range {v5 .. v13}, Lcom/squareup/printer/epson/EpsonPrinterInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;Lcom/squareup/print/ConnectionType;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 58
    iget-object v3, p0, Lcom/squareup/print/EpsonUsbPrinterScout;->epsonPrinterFactory:Lcom/squareup/printer/epson/EpsonPrinter$Factory;

    invoke-virtual {v3, v2}, Lcom/squareup/printer/epson/EpsonPrinter$Factory;->create(Lcom/squareup/printer/epson/EpsonPrinterInfo;)Lcom/squareup/printer/epson/EpsonPrinter;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    return-object v1
.end method

.method public start()V
    .locals 2

    .line 27
    iget-object v0, p0, Lcom/squareup/print/EpsonUsbPrinterScout;->started:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 28
    invoke-virtual {p0}, Lcom/squareup/print/EpsonUsbPrinterScout;->postScout()V

    return-void
.end method

.method public stop()V
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/print/EpsonUsbPrinterScout;->started:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 33
    invoke-virtual {p0}, Lcom/squareup/print/EpsonUsbPrinterScout;->cancelPendingScouting()V

    return-void
.end method
