.class public Lcom/squareup/print/sections/RefundsSection;
.super Ljava/lang/Object;
.source "RefundsSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/sections/RefundsSection$RefundSection;
    }
.end annotation


# instance fields
.field public final successfulRefunds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/RefundsSection$RefundSection;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/RefundsSection$RefundSection;",
            ">;)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/print/sections/RefundsSection;->successfulRefunds:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_4

    .line 49
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 51
    :cond_1
    check-cast p1, Lcom/squareup/print/sections/RefundsSection;

    .line 53
    iget-object v2, p0, Lcom/squareup/print/sections/RefundsSection;->successfulRefunds:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/print/sections/RefundsSection;->successfulRefunds:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-interface {v2, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_3

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    :goto_0
    return v1

    :cond_3
    return v0

    :cond_4
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/print/sections/RefundsSection;->successfulRefunds:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 4

    .line 67
    iget-object v0, p0, Lcom/squareup/print/sections/RefundsSection;->successfulRefunds:Ljava/util/List;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 71
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 72
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->lightDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 73
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 75
    iget-object v0, p0, Lcom/squareup/print/sections/RefundsSection;->successfulRefunds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 76
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 77
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/sections/RefundsSection$RefundSection;

    .line 79
    iget-object v2, v1, Lcom/squareup/print/sections/RefundsSection$RefundSection;->reason:Ljava/lang/String;

    iget-object v3, v1, Lcom/squareup/print/sections/RefundsSection$RefundSection;->negativeRefundAmount:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/squareup/print/ThermalBitmapBuilder;->titleAndAmount(Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 81
    iget-object v2, v1, Lcom/squareup/print/sections/RefundsSection$RefundSection;->createdAtDate:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 82
    iget-object v1, v1, Lcom/squareup/print/sections/RefundsSection$RefundSection;->createdAtDate:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 85
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 86
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    :cond_3
    :goto_1
    return-void
.end method
