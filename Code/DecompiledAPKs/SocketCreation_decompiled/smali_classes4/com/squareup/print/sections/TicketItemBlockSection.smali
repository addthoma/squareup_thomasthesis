.class public Lcom/squareup/print/sections/TicketItemBlockSection;
.super Ljava/lang/Object;
.source "TicketItemBlockSection.java"


# instance fields
.field public final diningOptionName:Ljava/lang/String;

.field public final items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/TicketItemSection;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/TicketItemSection;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/print/sections/TicketItemBlockSection;->items:Ljava/util/List;

    .line 18
    iput-object p2, p0, Lcom/squareup/print/sections/TicketItemBlockSection;->diningOptionName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 57
    :cond_0
    instance-of v1, p1, Lcom/squareup/print/sections/TicketItemBlockSection;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 59
    :cond_1
    check-cast p1, Lcom/squareup/print/sections/TicketItemBlockSection;

    .line 60
    iget-object v1, p0, Lcom/squareup/print/sections/TicketItemBlockSection;->items:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/print/sections/TicketItemBlockSection;->items:Ljava/util/List;

    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/print/sections/TicketItemBlockSection;->diningOptionName:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/print/sections/TicketItemBlockSection;->diningOptionName:Ljava/lang/String;

    .line 61
    invoke-static {v1, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/print/sections/TicketItemBlockSection;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    .line 67
    iget-object v1, p0, Lcom/squareup/print/sections/TicketItemBlockSection;->diningOptionName:Ljava/lang/String;

    if-eqz v1, :cond_0

    mul-int/lit8 v0, v0, 0x1f

    .line 68
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method public maxQuantityWidth()I
    .locals 3

    .line 48
    iget-object v0, p0, Lcom/squareup/print/sections/TicketItemBlockSection;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/print/sections/TicketItemSection;

    .line 49
    iget-object v2, v2, Lcom/squareup/print/sections/TicketItemSection;->quantity:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_0

    :cond_0
    return v1
.end method

.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 2

    .line 22
    iget-object v0, p0, Lcom/squareup/print/sections/TicketItemBlockSection;->diningOptionName:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 23
    invoke-virtual {p1, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->diningOptionHeader(Ljava/lang/CharSequence;Z)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    .line 25
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 28
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 29
    iget-object v0, p0, Lcom/squareup/print/sections/TicketItemBlockSection;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/sections/TicketItemSection;

    .line 30
    invoke-virtual {v1, p1}, Lcom/squareup/print/sections/TicketItemSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    goto :goto_1

    .line 32
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method

.method public renderText(Lcom/squareup/print/text/ImpactTextBuilder;I)V
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/squareup/print/sections/TicketItemBlockSection;->diningOptionName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 37
    invoke-virtual {p1, v0}, Lcom/squareup/print/text/ImpactTextBuilder;->diningOptionHeader(Ljava/lang/String;)V

    goto :goto_0

    .line 39
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/print/text/ImpactTextBuilder;->divider()V

    .line 41
    :goto_0
    iget-object v0, p0, Lcom/squareup/print/sections/TicketItemBlockSection;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/sections/TicketItemSection;

    .line 42
    invoke-virtual {v1, p1, p2}, Lcom/squareup/print/sections/TicketItemSection;->renderText(Lcom/squareup/print/text/ImpactTextBuilder;I)V

    goto :goto_1

    :cond_1
    return-void
.end method
