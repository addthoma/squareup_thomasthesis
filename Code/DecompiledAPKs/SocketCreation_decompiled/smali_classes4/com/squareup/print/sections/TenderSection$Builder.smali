.class public Lcom/squareup/print/sections/TenderSection$Builder;
.super Ljava/lang/Object;
.source "TenderSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/sections/TenderSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field public amountActuallyTendered:Lcom/squareup/print/payload/LabelAmountPair;

.field public amountActuallyTenderedShort:Lcom/squareup/print/payload/LabelAmountPair;

.field public amountToTender:Lcom/squareup/print/payload/LabelAmountPair;

.field public buyerSelectedAccountName:Ljava/lang/String;

.field public change:Lcom/squareup/print/payload/LabelAmountPair;

.field public customerName:Ljava/lang/String;

.field public felicaTerminalId:Lcom/squareup/print/payload/LabelAmountPair;

.field public idTransactionTypeLocation:Ljava/lang/String;

.field public remainingBalance:Lcom/squareup/print/payload/LabelAmountPair;

.field public tip:Lcom/squareup/print/payload/LabelAmountPair;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/print/sections/TenderSection;
    .locals 12

    .line 67
    new-instance v11, Lcom/squareup/print/sections/TenderSection;

    iget-object v1, p0, Lcom/squareup/print/sections/TenderSection$Builder;->amountActuallyTendered:Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v2, p0, Lcom/squareup/print/sections/TenderSection$Builder;->amountActuallyTenderedShort:Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v3, p0, Lcom/squareup/print/sections/TenderSection$Builder;->amountToTender:Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v4, p0, Lcom/squareup/print/sections/TenderSection$Builder;->change:Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v5, p0, Lcom/squareup/print/sections/TenderSection$Builder;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v6, p0, Lcom/squareup/print/sections/TenderSection$Builder;->remainingBalance:Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v7, p0, Lcom/squareup/print/sections/TenderSection$Builder;->customerName:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/print/sections/TenderSection$Builder;->buyerSelectedAccountName:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/print/sections/TenderSection$Builder;->idTransactionTypeLocation:Ljava/lang/String;

    iget-object v10, p0, Lcom/squareup/print/sections/TenderSection$Builder;->felicaTerminalId:Lcom/squareup/print/payload/LabelAmountPair;

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/print/sections/TenderSection;-><init>(Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/payload/LabelAmountPair;)V

    return-object v11
.end method
