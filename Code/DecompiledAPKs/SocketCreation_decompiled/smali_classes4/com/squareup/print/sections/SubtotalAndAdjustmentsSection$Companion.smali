.class public final Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection$Companion;
.super Ljava/lang/Object;
.source "SubtotalAndAdjustmentsSection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSubtotalAndAdjustmentsSection.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SubtotalAndAdjustmentsSection.kt\ncom/squareup/print/sections/SubtotalAndAdjustmentsSection$Companion\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,178:1\n1360#2:179\n1429#2,3:180\n1360#2:183\n1429#2,3:184\n704#2:187\n777#2,2:188\n1360#2:190\n1429#2,3:191\n*E\n*S KotlinDebug\n*F\n+ 1 SubtotalAndAdjustmentsSection.kt\ncom/squareup/print/sections/SubtotalAndAdjustmentsSection$Companion\n*L\n115#1:179\n115#1,3:180\n162#1:183\n162#1,3:184\n173#1:187\n173#1,2:188\n174#1:190\n174#1,3:191\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H\u0002J.\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00062\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002JD\u0010\u0012\u001a\u00020\u00132\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u00152\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u0017\u001a\u00020\u00112\u0006\u0010\u0018\u001a\u00020\u00112\u0006\u0010\u0010\u001a\u00020\u0011H\u0007\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection$Companion;",
        "",
        "()V",
        "calculateSubtotalAmount",
        "",
        "items",
        "",
        "Lcom/squareup/checkout/CartItem;",
        "formatSurchargesWithPhase",
        "Lcom/squareup/print/payload/LabelAmountPair;",
        "formatter",
        "Lcom/squareup/print/ReceiptFormatter;",
        "order",
        "Lcom/squareup/payment/Order;",
        "phase",
        "Lcom/squareup/calc/constants/CalculationPhase;",
        "isHistorical",
        "",
        "fromOrder",
        "Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;",
        "tip",
        "Lcom/squareup/protos/common/Money;",
        "swedishRounding",
        "canPrintSurcharges",
        "shouldIncludeSurchargeDisclosures",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 98
    invoke-direct {p0}, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection$Companion;-><init>()V

    return-void
.end method

.method private final calculateSubtotalAmount(Ljava/util/List;)J
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;)J"
        }
    .end annotation

    .line 172
    check-cast p1, Ljava/lang/Iterable;

    .line 187
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 188
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 173
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 189
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 190
    new-instance p1, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 191
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 192
    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 174
    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->baseAmount()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 193
    :cond_2
    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 175
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->sumOfLong(Ljava/lang/Iterable;)J

    move-result-wide v0

    return-wide v0
.end method

.method private final formatSurchargesWithPhase(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/calc/constants/CalculationPhase;Z)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/ReceiptFormatter;",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/calc/constants/CalculationPhase;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;"
        }
    .end annotation

    .line 159
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getSurcharges()Ljava/util/List;

    move-result-object v0

    .line 160
    sget-object v1, Lcom/squareup/checkout/Surcharge;->Companion:Lcom/squareup/checkout/Surcharge$Companion;

    const-string v2, "surcharges"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0, p3}, Lcom/squareup/checkout/Surcharge$Companion;->withPhase(Ljava/util/List;Lcom/squareup/calc/constants/CalculationPhase;)Ljava/util/List;

    move-result-object p3

    .line 161
    check-cast p3, Ljava/lang/Iterable;

    .line 183
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p3, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 184
    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 185
    check-cast v1, Lcom/squareup/checkout/Surcharge;

    if-eqz p4, :cond_0

    .line 164
    invoke-virtual {v1}, Lcom/squareup/checkout/Surcharge;->getHistoricalAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    goto :goto_1

    .line 166
    :cond_0
    invoke-virtual {p2, v1}, Lcom/squareup/payment/Order;->getSurchargeBaseAmount(Lcom/squareup/checkout/Surcharge;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 167
    :goto_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/print/ReceiptFormatter;->formatSurcharge(Lcom/squareup/checkout/Surcharge;Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 186
    :cond_1
    check-cast v0, Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final fromOrder(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZZZ)Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;
    .locals 15
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move/from16 v2, p7

    const-string v3, "formatter"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "order"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/payment/Order;->getOrderAdjustments()Ljava/util/List;

    move-result-object v3

    const-string v4, "emptyList()"

    if-eqz p5, :cond_0

    .line 111
    move-object v5, p0

    check-cast v5, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection$Companion;

    sget-object v6, Lcom/squareup/calc/constants/CalculationPhase;->SURCHARGE_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    invoke-direct {v5, v0, v1, v6, v2}, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection$Companion;->formatSurchargesWithPhase(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/calc/constants/CalculationPhase;Z)Ljava/util/List;

    move-result-object v5

    goto :goto_0

    .line 113
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-static {v5, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    move-object v8, v5

    .line 114
    invoke-static {v3}, Lcom/squareup/print/sections/SectionUtils;->findAdditiveTaxes(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    const-string v6, "findAdditiveTaxes(orderAdjustments)"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Ljava/lang/Iterable;

    .line 179
    new-instance v6, Ljava/util/ArrayList;

    const/16 v7, 0xa

    invoke-static {v5, v7}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v7

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v6, Ljava/util/Collection;

    .line 180
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    .line 181
    check-cast v7, Lcom/squareup/payment/OrderAdjustment;

    .line 115
    invoke-virtual {v0, v7}, Lcom/squareup/print/ReceiptFormatter;->formatAdditiveTax(Lcom/squareup/payment/OrderAdjustment;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 182
    :cond_1
    move-object v9, v6

    check-cast v9, Ljava/util/List;

    if-eqz p5, :cond_2

    .line 117
    move-object v4, p0

    check-cast v4, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection$Companion;

    sget-object v5, Lcom/squareup/calc/constants/CalculationPhase;->SURCHARGE_TOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    invoke-direct {v4, v0, v1, v5, v2}, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection$Companion;->formatSurchargesWithPhase(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/calc/constants/CalculationPhase;Z)Ljava/util/List;

    move-result-object v2

    goto :goto_2

    .line 119
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2
    move-object v10, v2

    .line 122
    move-object v2, p0

    check-cast v2, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection$Companion;

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v4

    const-string v5, "order.items"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v4}, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection$Companion;->calculateSubtotalAmount(Ljava/util/List;)J

    move-result-wide v4

    const/4 v2, 0x1

    .line 123
    invoke-static {v3, v2, v2}, Lcom/squareup/print/sections/SectionUtils;->calculateTotalNegativeDiscount(Ljava/util/List;ZZ)J

    move-result-wide v2

    add-long/2addr v4, v2

    .line 125
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/payment/Order;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v2

    const-string v3, "order.currencyCode"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4, v5, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    if-eqz p6, :cond_3

    .line 128
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/payment/Order;->getSurcharges()Ljava/util/List;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/payment/Order;->getSeatingHandler()Lcom/squareup/payment/OrderSeatingHandler;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/payment/OrderSeatingHandler;->getEnabledSeats()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Lcom/squareup/print/ReceiptFormatter;->surchargeDisclosure(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :cond_3
    const/4 v3, 0x0

    :goto_3
    move-object v13, v3

    .line 133
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/payment/Order;->hasReturn()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 134
    invoke-virtual {v0, v2}, Lcom/squareup/print/ReceiptFormatter;->purchaseSubtotal(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object v2

    goto :goto_4

    .line 136
    :cond_4
    invoke-virtual {v0, v2}, Lcom/squareup/print/ReceiptFormatter;->subtotal(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object v2

    :goto_4
    move-object v7, v2

    .line 139
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/payment/Order;->hasReturn()Z

    move-result v14

    .line 141
    new-instance v1, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    move-object/from16 v2, p3

    .line 146
    invoke-virtual {v0, v2}, Lcom/squareup/print/ReceiptFormatter;->tipOrNullIfZero(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object v11

    move-object/from16 v2, p4

    .line 147
    invoke-virtual {v0, v2}, Lcom/squareup/print/ReceiptFormatter;->swedishRoundingOrNullIfZero(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object v12

    move-object v6, v1

    .line 141
    invoke-direct/range {v6 .. v14}, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;-><init>(Lcom/squareup/print/payload/LabelAmountPair;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Ljava/lang/String;Z)V

    return-object v1
.end method
