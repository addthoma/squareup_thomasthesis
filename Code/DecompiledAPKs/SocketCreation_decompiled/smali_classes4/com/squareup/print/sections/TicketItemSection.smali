.class public Lcom/squareup/print/sections/TicketItemSection;
.super Ljava/lang/Object;
.source "TicketItemSection.java"


# static fields
.field private static final ONE:Ljava/lang/String;


# instance fields
.field public final diningOptionName:Ljava/lang/String;

.field public final name:Ljava/lang/String;

.field public final note:Ljava/lang/String;

.field public final quantity:Ljava/lang/String;

.field public final seatsModifiersAndVariations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final timesSign:Ljava/lang/String;

.field public final unitQuantity:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    .line 51
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/print/sections/TicketItemSection;->ONE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/squareup/print/sections/TicketItemSection;->quantity:Ljava/lang/String;

    .line 56
    iput-object p2, p0, Lcom/squareup/print/sections/TicketItemSection;->unitQuantity:Ljava/lang/String;

    .line 57
    iput-object p3, p0, Lcom/squareup/print/sections/TicketItemSection;->timesSign:Ljava/lang/String;

    .line 58
    iput-object p4, p0, Lcom/squareup/print/sections/TicketItemSection;->name:Ljava/lang/String;

    .line 59
    iput-object p5, p0, Lcom/squareup/print/sections/TicketItemSection;->seatsModifiersAndVariations:Ljava/util/List;

    .line 60
    iput-object p6, p0, Lcom/squareup/print/sections/TicketItemSection;->note:Ljava/lang/String;

    .line 61
    iput-object p7, p0, Lcom/squareup/print/sections/TicketItemSection;->diningOptionName:Ljava/lang/String;

    return-void
.end method

.method public static fromOrderItem(Lcom/squareup/print/ReceiptFormatter;Ljava/util/Locale;Lcom/squareup/print/PrintableOrderItem;ZLcom/squareup/util/Res;ZLcom/squareup/checkout/DiningOption;)Lcom/squareup/print/sections/TicketItemSection;
    .locals 13

    move-object v7, p0

    move-object v8, p2

    move-object/from16 v9, p4

    const/4 v3, 0x1

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p2

    move/from16 v2, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    .line 23
    invoke-static/range {v0 .. v6}, Lcom/squareup/print/sections/SectionUtils;->getSeatsVariationsAndModifiersToPrint(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/print/PrintableOrderItem;ZZZLcom/squareup/util/Res;Z)Ljava/util/List;

    move-result-object v10

    .line 28
    invoke-interface {p2}, Lcom/squareup/print/PrintableOrderItem;->isUnitPriced()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/print/sections/TicketItemSection;->ONE:Ljava/lang/String;

    goto :goto_0

    :cond_0
    invoke-interface {p2}, Lcom/squareup/print/PrintableOrderItem;->getQuantityAsString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/squareup/print/ReceiptFormatter;->quantity(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 30
    invoke-interface {p2}, Lcom/squareup/print/PrintableOrderItem;->isUnitPriced()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 31
    invoke-interface {p2}, Lcom/squareup/print/PrintableOrderItem;->getQuantityAsString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v9}, Lcom/squareup/print/PrintableOrderItem;->quantityPrecision(Lcom/squareup/util/Res;)I

    move-result v2

    .line 32
    invoke-interface {p2, v9}, Lcom/squareup/print/PrintableOrderItem;->unitAbbreviation(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v3

    .line 31
    invoke-virtual {p0, v0, v2, v3}, Lcom/squareup/print/ReceiptFormatter;->unitQuantity(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 35
    :goto_1
    new-instance v2, Lcom/squareup/print/sections/TicketItemSection;

    invoke-virtual {p0}, Lcom/squareup/print/ReceiptFormatter;->ticketQuantityX()Ljava/lang/String;

    move-result-object v3

    .line 36
    invoke-interface {p2}, Lcom/squareup/print/PrintableOrderItem;->getItemName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/squareup/print/ReceiptFormatter;->itemNameOrCustomIfBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 37
    invoke-interface {p2}, Lcom/squareup/print/PrintableOrderItem;->getNotes()Ljava/lang/String;

    move-result-object v11

    if-nez p6, :cond_2

    goto :goto_2

    .line 40
    :cond_2
    invoke-virtual/range {p6 .. p6}, Lcom/squareup/checkout/DiningOption;->getName()Ljava/lang/String;

    move-result-object v1

    move-object v4, p1

    invoke-virtual {v1, p1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    move-object v12, v1

    move-object v5, v2

    move-object v7, v0

    move-object v8, v3

    invoke-direct/range {v5 .. v12}, Lcom/squareup/print/sections/TicketItemSection;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 78
    :cond_0
    instance-of v1, p1, Lcom/squareup/print/sections/TicketItemSection;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 80
    :cond_1
    check-cast p1, Lcom/squareup/print/sections/TicketItemSection;

    .line 81
    iget-object v1, p0, Lcom/squareup/print/sections/TicketItemSection;->quantity:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/print/sections/TicketItemSection;->quantity:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/print/sections/TicketItemSection;->timesSign:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/print/sections/TicketItemSection;->timesSign:Ljava/lang/String;

    .line 82
    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/squareup/print/sections/TicketItemSection;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/print/sections/TicketItemSection;->name:Ljava/lang/String;

    .line 83
    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/squareup/print/sections/TicketItemSection;->seatsModifiersAndVariations:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/print/sections/TicketItemSection;->seatsModifiersAndVariations:Ljava/util/List;

    .line 84
    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/squareup/print/sections/TicketItemSection;->note:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/print/sections/TicketItemSection;->note:Ljava/lang/String;

    .line 85
    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/squareup/print/sections/TicketItemSection;->diningOptionName:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/print/sections/TicketItemSection;->diningOptionName:Ljava/lang/String;

    .line 86
    invoke-static {v1, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 90
    iget-object v0, p0, Lcom/squareup/print/sections/TicketItemSection;->quantity:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 91
    iget-object v2, p0, Lcom/squareup/print/sections/TicketItemSection;->timesSign:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 92
    iget-object v2, p0, Lcom/squareup/print/sections/TicketItemSection;->name:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 93
    iget-object v2, p0, Lcom/squareup/print/sections/TicketItemSection;->seatsModifiersAndVariations:Ljava/util/List;

    if-eqz v2, :cond_3

    .line 94
    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 95
    iget-object v2, p0, Lcom/squareup/print/sections/TicketItemSection;->note:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 96
    iget-object v2, p0, Lcom/squareup/print/sections/TicketItemSection;->diningOptionName:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 7

    .line 65
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 66
    iget-object v1, p0, Lcom/squareup/print/sections/TicketItemSection;->quantity:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/print/sections/TicketItemSection;->unitQuantity:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/print/sections/TicketItemSection;->timesSign:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/print/sections/TicketItemSection;->name:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/print/sections/TicketItemSection;->seatsModifiersAndVariations:Ljava/util/List;

    iget-object v6, p0, Lcom/squareup/print/sections/TicketItemSection;->note:Ljava/lang/String;

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/print/ThermalBitmapBuilder;->ticketQuantityAndItem(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/util/List;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method

.method public renderText(Lcom/squareup/print/text/ImpactTextBuilder;I)V
    .locals 8

    .line 71
    invoke-virtual {p1}, Lcom/squareup/print/text/ImpactTextBuilder;->spacing()V

    .line 72
    iget-object v1, p0, Lcom/squareup/print/sections/TicketItemSection;->quantity:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/print/sections/TicketItemSection;->unitQuantity:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/print/sections/TicketItemSection;->timesSign:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/print/sections/TicketItemSection;->name:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/print/sections/TicketItemSection;->seatsModifiersAndVariations:Ljava/util/List;

    iget-object v7, p0, Lcom/squareup/print/sections/TicketItemSection;->note:Ljava/lang/String;

    move-object v0, p1

    move v2, p2

    invoke-virtual/range {v0 .. v7}, Lcom/squareup/print/text/ImpactTextBuilder;->ticketQuantityAndItem(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method
