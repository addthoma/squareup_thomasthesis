.class public abstract Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider$NoBarcodeReceiptInfoProviderModule;
.super Ljava/lang/Object;
.source "ReceiptInfoProvider.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "NoBarcodeReceiptInfoProviderModule"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider$NoBarcodeReceiptInfoProviderModule;",
        "",
        "()V",
        "bindReceiptInfoProvider",
        "Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;",
        "noBarcodeReceiptInfoProvider",
        "Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider$NoBarcodeReceiptInfoProvider;",
        "bindReceiptInfoProvider$print_release",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindReceiptInfoProvider$print_release(Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider$NoBarcodeReceiptInfoProvider;)Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
