.class public final Lcom/squareup/print/PrintModule_ProvideHardwarePrintersFactory;
.super Ljava/lang/Object;
.source "PrintModule_ProvideHardwarePrintersFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/HardwarePrinterTracker;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final preferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/print/PrintModule_ProvideHardwarePrintersFactory;->gsonProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/print/PrintModule_ProvideHardwarePrintersFactory;->preferencesProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/print/PrintModule_ProvideHardwarePrintersFactory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/PrintModule_ProvideHardwarePrintersFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/print/PrintModule_ProvideHardwarePrintersFactory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/print/PrintModule_ProvideHardwarePrintersFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/print/PrintModule_ProvideHardwarePrintersFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideHardwarePrinters(Lcom/google/gson/Gson;Landroid/content/SharedPreferences;Lcom/squareup/analytics/Analytics;)Lcom/squareup/print/HardwarePrinterTracker;
    .locals 0

    .line 45
    invoke-static {p0, p1, p2}, Lcom/squareup/print/PrintModule;->provideHardwarePrinters(Lcom/google/gson/Gson;Landroid/content/SharedPreferences;Lcom/squareup/analytics/Analytics;)Lcom/squareup/print/HardwarePrinterTracker;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/HardwarePrinterTracker;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/print/HardwarePrinterTracker;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvideHardwarePrintersFactory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/Gson;

    iget-object v1, p0, Lcom/squareup/print/PrintModule_ProvideHardwarePrintersFactory;->preferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/squareup/print/PrintModule_ProvideHardwarePrintersFactory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2}, Lcom/squareup/print/PrintModule_ProvideHardwarePrintersFactory;->provideHardwarePrinters(Lcom/google/gson/Gson;Landroid/content/SharedPreferences;Lcom/squareup/analytics/Analytics;)Lcom/squareup/print/HardwarePrinterTracker;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/print/PrintModule_ProvideHardwarePrintersFactory;->get()Lcom/squareup/print/HardwarePrinterTracker;

    move-result-object v0

    return-object v0
.end method
