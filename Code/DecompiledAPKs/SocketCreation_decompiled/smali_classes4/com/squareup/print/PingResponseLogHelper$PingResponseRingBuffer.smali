.class public Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;
.super Ljava/lang/Object;
.source "PingResponseLogHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/PingResponseLogHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PingResponseRingBuffer"
.end annotation


# instance fields
.field private final buffer:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/PingResponseLogHelper$PingReport;",
            ">;"
        }
    .end annotation
.end field

.field private final capacity:I

.field private index:I

.field final synthetic this$0:Lcom/squareup/print/PingResponseLogHelper;


# direct methods
.method constructor <init>(Lcom/squareup/print/PingResponseLogHelper;I)V
    .locals 1

    .line 88
    iput-object p1, p0, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;->this$0:Lcom/squareup/print/PingResponseLogHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput p2, p0, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;->capacity:I

    const/4 p1, 0x0

    .line 90
    iput p1, p0, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;->index:I

    .line 91
    new-instance p1, Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-static {p2, v0}, Ljava/util/Collections;->nCopies(ILjava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p1, p0, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;->buffer:Ljava/util/List;

    return-void
.end method


# virtual methods
.method addPingResponse(Ljava/lang/Long;)V
    .locals 6

    .line 96
    iget-object v0, p0, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;->buffer:Ljava/util/List;

    iget v1, p0, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;->index:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;->index:I

    new-instance v2, Lcom/squareup/print/PingResponseLogHelper$PingReport;

    iget-object v3, p0, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;->this$0:Lcom/squareup/print/PingResponseLogHelper;

    invoke-static {v3}, Lcom/squareup/print/PingResponseLogHelper;->access$000(Lcom/squareup/print/PingResponseLogHelper;)Lcom/squareup/util/Clock;

    move-result-object v4

    invoke-interface {v4}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-direct {v2, v3, v4, p1}, Lcom/squareup/print/PingResponseLogHelper$PingReport;-><init>(Lcom/squareup/print/PingResponseLogHelper;Ljava/lang/Long;Ljava/lang/Long;)V

    invoke-interface {v0, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 97
    iget p1, p0, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;->index:I

    iget v0, p0, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;->capacity:I

    rem-int/2addr p1, v0

    iput p1, p0, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;->index:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 103
    iget v1, p0, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;->index:I

    const/4 v2, 0x1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;->buffer:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    :cond_0
    sub-int/2addr v1, v2

    const/4 v3, 0x0

    move v4, v1

    const/4 v1, 0x0

    .line 104
    :goto_0
    iget v5, p0, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;->capacity:I

    if-ge v1, v5, :cond_3

    .line 105
    iget-object v5, p0, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;->buffer:Ljava/util/List;

    add-int/lit8 v6, v4, -0x1

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/print/PingResponseLogHelper$PingReport;

    if-eqz v4, :cond_1

    .line 107
    invoke-virtual {v4}, Lcom/squareup/print/PingResponseLogHelper$PingReport;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    if-gez v6, :cond_2

    .line 110
    iget v4, p0, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;->capacity:I

    sub-int/2addr v4, v2

    goto :goto_1

    :cond_2
    move v4, v6

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 115
    :cond_3
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    const-string v4, ", "

    invoke-static {v0, v4}, Lcom/squareup/util/Strings;->join([Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const-string v0, "[%s]"

    invoke-static {v1, v0, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
