.class public Lcom/squareup/print/PrintDispatchedEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "PrintDispatchedEvent.java"


# instance fields
.field public final numPrinters:I


# direct methods
.method private constructor <init>(Lcom/squareup/analytics/RegisterActionName;I)V
    .locals 1

    .line 53
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    iget-object p1, p1, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 54
    iput p2, p0, Lcom/squareup/print/PrintDispatchedEvent;->numPrinters:I

    return-void
.end method

.method public static forAuthSlipCopyDispatched(I)Lcom/squareup/print/PrintDispatchedEvent;
    .locals 2

    .line 37
    new-instance v0, Lcom/squareup/print/PrintDispatchedEvent;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_AUTH_SLIP_COPY:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1, p0}, Lcom/squareup/print/PrintDispatchedEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;I)V

    return-object v0
.end method

.method public static forAuthSlipDispatched(I)Lcom/squareup/print/PrintDispatchedEvent;
    .locals 2

    .line 33
    new-instance v0, Lcom/squareup/print/PrintDispatchedEvent;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_AUTH_SLIP:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1, p0}, Lcom/squareup/print/PrintDispatchedEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;I)V

    return-object v0
.end method

.method public static forGiftReceiptDispatched(I)Lcom/squareup/print/PrintDispatchedEvent;
    .locals 2

    .line 45
    new-instance v0, Lcom/squareup/print/PrintDispatchedEvent;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_GIFT_RECEIPT:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1, p0}, Lcom/squareup/print/PrintDispatchedEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;I)V

    return-object v0
.end method

.method public static forReceiptDispatched(I)Lcom/squareup/print/PrintDispatchedEvent;
    .locals 2

    .line 21
    new-instance v0, Lcom/squareup/print/PrintDispatchedEvent;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_RECEIPT:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1, p0}, Lcom/squareup/print/PrintDispatchedEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;I)V

    return-object v0
.end method

.method public static forReprintTicketDispatched(I)Lcom/squareup/print/PrintDispatchedEvent;
    .locals 2

    .line 41
    new-instance v0, Lcom/squareup/print/PrintDispatchedEvent;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->REPRINT_TICKET:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1, p0}, Lcom/squareup/print/PrintDispatchedEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;I)V

    return-object v0
.end method

.method public static forStubDispatched(I)Lcom/squareup/print/PrintDispatchedEvent;
    .locals 2

    .line 25
    new-instance v0, Lcom/squareup/print/PrintDispatchedEvent;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_STUB:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1, p0}, Lcom/squareup/print/PrintDispatchedEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;I)V

    return-object v0
.end method

.method public static forTicketDispatched(I)Lcom/squareup/print/PrintDispatchedEvent;
    .locals 2

    .line 29
    new-instance v0, Lcom/squareup/print/PrintDispatchedEvent;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_TICKET:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1, p0}, Lcom/squareup/print/PrintDispatchedEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;I)V

    return-object v0
.end method

.method public static forVoidTicketDispatched(I)Lcom/squareup/print/PrintDispatchedEvent;
    .locals 2

    .line 49
    new-instance v0, Lcom/squareup/print/PrintDispatchedEvent;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_VOID_TICKET:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1, p0}, Lcom/squareup/print/PrintDispatchedEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;I)V

    return-object v0
.end method
