.class public Lcom/squareup/print/TimecardsSummaryRenderer;
.super Ljava/lang/Object;
.source "TimecardsSummaryRenderer.java"


# instance fields
.field private final builder:Lcom/squareup/print/ThermalBitmapBuilder;


# direct methods
.method public constructor <init>(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/squareup/print/TimecardsSummaryRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method


# virtual methods
.method public final renderBitmap(Lcom/squareup/print/payload/TimecardsSummaryPayload;)Landroid/graphics/Bitmap;
    .locals 3

    .line 15
    iget-object v0, p0, Lcom/squareup/print/TimecardsSummaryRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    iget-object v1, p1, Lcom/squareup/print/payload/TimecardsSummaryPayload;->reportHeaderText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->dividerWithMinText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 16
    iget-object v0, p0, Lcom/squareup/print/TimecardsSummaryRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 17
    iget-object v0, p1, Lcom/squareup/print/payload/TimecardsSummaryPayload;->employeeSummarySection:Lcom/squareup/print/sections/EmployeeSummarySection;

    iget-object v1, p0, Lcom/squareup/print/TimecardsSummaryRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {v0, v1}, Lcom/squareup/print/sections/EmployeeSummarySection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 18
    iget-object v0, p0, Lcom/squareup/print/TimecardsSummaryRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 19
    iget-object v0, p0, Lcom/squareup/print/TimecardsSummaryRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    iget-object v1, p1, Lcom/squareup/print/payload/TimecardsSummaryPayload;->shiftHeaderText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->headlineBlock(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 20
    iget-object v0, p0, Lcom/squareup/print/TimecardsSummaryRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 22
    iget-object v0, p1, Lcom/squareup/print/payload/TimecardsSummaryPayload;->shiftSections:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/sections/ShiftSection;

    .line 23
    iget-object v2, p0, Lcom/squareup/print/TimecardsSummaryRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {v1, v2}, Lcom/squareup/print/sections/ShiftSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    goto :goto_0

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/squareup/print/TimecardsSummaryRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/ThermalBitmapBuilder;->largeSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 27
    iget-object v0, p0, Lcom/squareup/print/TimecardsSummaryRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 28
    iget-object v0, p0, Lcom/squareup/print/TimecardsSummaryRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/ThermalBitmapBuilder;->thinDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 29
    iget-object v0, p0, Lcom/squareup/print/TimecardsSummaryRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/ThermalBitmapBuilder;->largeSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 31
    iget-object p1, p1, Lcom/squareup/print/payload/TimecardsSummaryPayload;->printedAtSection:Lcom/squareup/print/sections/PrintedAtSection;

    iget-object v0, p0, Lcom/squareup/print/TimecardsSummaryRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {p1, v0}, Lcom/squareup/print/sections/PrintedAtSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 33
    iget-object p1, p0, Lcom/squareup/print/TimecardsSummaryRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->render()Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method
