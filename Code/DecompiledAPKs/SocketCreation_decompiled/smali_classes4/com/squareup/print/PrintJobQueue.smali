.class public interface abstract Lcom/squareup/print/PrintJobQueue;
.super Ljava/lang/Object;
.source "PrintJobQueue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/PrintJobQueue$JobState;
    }
.end annotation


# virtual methods
.method public abstract close()V
.end method

.method public abstract enqueueNewJob(Lcom/squareup/print/PrintJob;)V
.end method

.method public abstract reenqueueFailedJob(Lcom/squareup/print/PrintJob;)V
.end method

.method public abstract removeFailedJobs(Ljava/util/Collection;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrintJob;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract removeSuccessfulJob(Ljava/lang/String;)V
.end method

.method public abstract retrieveAllFailedPrintJobs()Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/print/PrintJob;",
            ">;"
        }
    .end annotation
.end method

.method public abstract retrieveAllPrintJobs()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintJob;",
            ">;"
        }
    .end annotation
.end method

.method public abstract retrieveHeadPrintJobPerPrintTarget()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/print/PrintJob;",
            ">;"
        }
    .end annotation
.end method

.method public abstract updateJobAsFailed(Lcom/squareup/print/PrintJob;)V
.end method
