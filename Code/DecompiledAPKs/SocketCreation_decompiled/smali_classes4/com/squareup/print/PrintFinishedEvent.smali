.class public Lcom/squareup/print/PrintFinishedEvent;
.super Lcom/squareup/print/PrinterEvent;
.source "PrintFinishedEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0010\t\n\u0002\u0008\u0007\n\u0002\u0010\u000e\n\u0002\u0008\u000e\n\u0002\u0010\u0008\n\u0002\u0008\u000f\u0008\u0016\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u0019\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0015\u0010\u000e\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\n\n\u0002\u0010\u0011\u001a\u0004\u0008\u000f\u0010\u0010R\u0013\u0010\u0012\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0013\u0010\u0016\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0015R\u0013\u0010\u0018\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0015R\u0013\u0010\u001a\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u0015R\u0013\u0010\u001c\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u0015R\u0011\u0010\u001e\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010 R\u0011\u0010!\u001a\u00020\"\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010$R\u0013\u0010%\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008&\u0010\u0015R\u0013\u0010\'\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008(\u0010\u0015R\u0011\u0010)\u001a\u00020\"\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008*\u0010$R\u0015\u0010+\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\n\n\u0002\u0010\u0011\u001a\u0004\u0008,\u0010\u0010R\u0015\u0010-\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\n\n\u0002\u0010\u0011\u001a\u0004\u0008.\u0010\u0010R\u0013\u0010/\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00080\u0010\u0015\u00a8\u00061"
    }
    d2 = {
        "Lcom/squareup/print/PrintFinishedEvent;",
        "Lcom/squareup/print/PrinterEvent;",
        "name",
        "Lcom/squareup/eventstream/v1/EventStream$Name;",
        "value",
        "Lcom/squareup/analytics/RegisterActionName;",
        "job",
        "Lcom/squareup/print/PrintJob;",
        "(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrintJob;)V",
        "connectAttemptIntervalsMs",
        "",
        "",
        "getConnectAttemptIntervalsMs",
        "()Ljava/util/List;",
        "enqueuedToConnectAttemptMs",
        "getEnqueuedToConnectAttemptMs",
        "()Ljava/lang/Long;",
        "Ljava/lang/Long;",
        "errorMessage",
        "",
        "getErrorMessage",
        "()Ljava/lang/String;",
        "jobName",
        "getJobName",
        "jobSourceIdentifier",
        "getJobSourceIdentifier",
        "jobType",
        "getJobType",
        "jobUuid",
        "getJobUuid",
        "portAcquisitionAccumulatedDurationMs",
        "getPortAcquisitionAccumulatedDurationMs",
        "()J",
        "portAcquisitionAttempts",
        "",
        "getPortAcquisitionAttempts",
        "()I",
        "printTargetId",
        "getPrintTargetId",
        "printTargetName",
        "getPrintTargetName",
        "retries",
        "getRetries",
        "sentDataToFinishedMs",
        "getSentDataToFinishedMs",
        "totalFromEnqueuedToFinishedMs",
        "getTotalFromEnqueuedToFinishedMs",
        "vendorSpecificResult",
        "getVendorSpecificResult",
        "hardware_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final connectAttemptIntervalsMs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final enqueuedToConnectAttemptMs:Ljava/lang/Long;

.field private final errorMessage:Ljava/lang/String;

.field private final jobName:Ljava/lang/String;

.field private final jobSourceIdentifier:Ljava/lang/String;

.field private final jobType:Ljava/lang/String;

.field private final jobUuid:Ljava/lang/String;

.field private final portAcquisitionAccumulatedDurationMs:J

.field private final portAcquisitionAttempts:I

.field private final printTargetId:Ljava/lang/String;

.field private final printTargetName:Ljava/lang/String;

.field private final retries:I

.field private final sentDataToFinishedMs:Ljava/lang/Long;

.field private final totalFromEnqueuedToFinishedMs:Ljava/lang/Long;

.field private final vendorSpecificResult:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrintJob;)V
    .locals 12

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "job"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    iget-object v3, p2, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    const-string/jumbo p2, "value.value"

    invoke-static {v3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getLatestPrintAttempt()Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object p2

    iget-object p2, p2, Lcom/squareup/print/PrintJob$PrintAttempt;->hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->hardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object p2

    :goto_0
    move-object v4, p2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xf8

    const/4 v11, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v11}, Lcom/squareup/print/PrinterEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 16
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getLatestPrintAttempt()Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/print/PrintJob$PrintAttempt;->printTimingData:Lcom/squareup/print/PrintTimingData;

    const/4 p2, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/print/PrintTimingData;->getEnqueuedToConnectAttemptMs()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    goto :goto_1

    :cond_1
    move-object p1, p2

    :goto_1
    iput-object p1, p0, Lcom/squareup/print/PrintFinishedEvent;->enqueuedToConnectAttemptMs:Ljava/lang/Long;

    .line 17
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getLatestPrintAttempt()Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/print/PrintJob$PrintAttempt;->printTimingData:Lcom/squareup/print/PrintTimingData;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/squareup/print/PrintTimingData;->getConnectAttemptIntervalsMs()Ljava/util/List;

    move-result-object p1

    goto :goto_2

    :cond_2
    move-object p1, p2

    :goto_2
    iput-object p1, p0, Lcom/squareup/print/PrintFinishedEvent;->connectAttemptIntervalsMs:Ljava/util/List;

    .line 18
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getLatestPrintAttempt()Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/print/PrintJob$PrintAttempt;->printTimingData:Lcom/squareup/print/PrintTimingData;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/squareup/print/PrintTimingData;->getSentDataToFinishedMs()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    goto :goto_3

    :cond_3
    move-object p1, p2

    :goto_3
    iput-object p1, p0, Lcom/squareup/print/PrintFinishedEvent;->sentDataToFinishedMs:Ljava/lang/Long;

    .line 20
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getLatestPrintAttempt()Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/print/PrintJob$PrintAttempt;->printTimingData:Lcom/squareup/print/PrintTimingData;

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/squareup/print/PrintTimingData;->getTotalFromEnqueuedToFinishedMs()J

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    :cond_4
    iput-object p2, p0, Lcom/squareup/print/PrintFinishedEvent;->totalFromEnqueuedToFinishedMs:Ljava/lang/Long;

    .line 22
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getLatestPrintAttempt()Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/print/PrintJob$PrintAttempt;->errorMessage:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/print/PrintFinishedEvent;->errorMessage:Ljava/lang/String;

    .line 23
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getLatestPrintAttempt()Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/print/PrintJob$PrintAttempt;->vendorSpecificResult:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/print/PrintFinishedEvent;->vendorSpecificResult:Ljava/lang/String;

    .line 24
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getLatestPrintAttempt()Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object p1

    iget p1, p1, Lcom/squareup/print/PrintJob$PrintAttempt;->portAcquisitionAttempts:I

    iput p1, p0, Lcom/squareup/print/PrintFinishedEvent;->portAcquisitionAttempts:I

    .line 26
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getLatestPrintAttempt()Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object p1

    iget-wide p1, p1, Lcom/squareup/print/PrintJob$PrintAttempt;->portAcquisitionAccumulatedDurationMs:J

    iput-wide p1, p0, Lcom/squareup/print/PrintFinishedEvent;->portAcquisitionAccumulatedDurationMs:J

    .line 28
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getPrintAttemptsCount()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    iput p1, p0, Lcom/squareup/print/PrintFinishedEvent;->retries:I

    .line 29
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getTargetId()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/PrintFinishedEvent;->printTargetId:Ljava/lang/String;

    .line 30
    iget-object p1, p3, Lcom/squareup/print/PrintJob;->printTargetName:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/print/PrintFinishedEvent;->printTargetName:Ljava/lang/String;

    .line 31
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/PrintFinishedEvent;->jobUuid:Ljava/lang/String;

    .line 32
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getTitle()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/PrintFinishedEvent;->jobName:Ljava/lang/String;

    .line 33
    iget-object p1, p3, Lcom/squareup/print/PrintJob;->sourceIdentifier:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/print/PrintFinishedEvent;->jobSourceIdentifier:Ljava/lang/String;

    .line 34
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getPayload()Lcom/squareup/print/PrintablePayload;

    move-result-object p1

    const-string p2, "job.payload"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/print/PrintablePayload;->getAnalyticsPrintJobType()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/PrintFinishedEvent;->jobType:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getConnectAttemptIntervalsMs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/squareup/print/PrintFinishedEvent;->connectAttemptIntervalsMs:Ljava/util/List;

    return-object v0
.end method

.method public final getEnqueuedToConnectAttemptMs()Ljava/lang/Long;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/print/PrintFinishedEvent;->enqueuedToConnectAttemptMs:Ljava/lang/Long;

    return-object v0
.end method

.method public final getErrorMessage()Ljava/lang/String;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/print/PrintFinishedEvent;->errorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final getJobName()Ljava/lang/String;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/print/PrintFinishedEvent;->jobName:Ljava/lang/String;

    return-object v0
.end method

.method public final getJobSourceIdentifier()Ljava/lang/String;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/print/PrintFinishedEvent;->jobSourceIdentifier:Ljava/lang/String;

    return-object v0
.end method

.method public final getJobType()Ljava/lang/String;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/print/PrintFinishedEvent;->jobType:Ljava/lang/String;

    return-object v0
.end method

.method public final getJobUuid()Ljava/lang/String;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/print/PrintFinishedEvent;->jobUuid:Ljava/lang/String;

    return-object v0
.end method

.method public final getPortAcquisitionAccumulatedDurationMs()J
    .locals 2

    .line 25
    iget-wide v0, p0, Lcom/squareup/print/PrintFinishedEvent;->portAcquisitionAccumulatedDurationMs:J

    return-wide v0
.end method

.method public final getPortAcquisitionAttempts()I
    .locals 1

    .line 24
    iget v0, p0, Lcom/squareup/print/PrintFinishedEvent;->portAcquisitionAttempts:I

    return v0
.end method

.method public final getPrintTargetId()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/print/PrintFinishedEvent;->printTargetId:Ljava/lang/String;

    return-object v0
.end method

.method public final getPrintTargetName()Ljava/lang/String;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/print/PrintFinishedEvent;->printTargetName:Ljava/lang/String;

    return-object v0
.end method

.method public final getRetries()I
    .locals 1

    .line 28
    iget v0, p0, Lcom/squareup/print/PrintFinishedEvent;->retries:I

    return v0
.end method

.method public final getSentDataToFinishedMs()Ljava/lang/Long;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/print/PrintFinishedEvent;->sentDataToFinishedMs:Ljava/lang/Long;

    return-object v0
.end method

.method public final getTotalFromEnqueuedToFinishedMs()Ljava/lang/Long;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/print/PrintFinishedEvent;->totalFromEnqueuedToFinishedMs:Ljava/lang/Long;

    return-object v0
.end method

.method public final getVendorSpecificResult()Ljava/lang/String;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/print/PrintFinishedEvent;->vendorSpecificResult:Ljava/lang/String;

    return-object v0
.end method
