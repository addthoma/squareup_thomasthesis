.class public final Lcom/squareup/posapp/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/posapp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final AddAmountValueButton:I = 0x7f130000

.field public static final AddOrEditNotesButton:I = 0x7f130001

.field public static final AlertDialog_AppCompat:I = 0x7f130002

.field public static final AlertDialog_AppCompat_Light:I = 0x7f130003

.field public static final AmountCell_Value:I = 0x7f130004

.field public static final Animation_AppCompat_Dialog:I = 0x7f130005

.field public static final Animation_AppCompat_DropDownUp:I = 0x7f130006

.field public static final Animation_AppCompat_Tooltip:I = 0x7f130007

.field public static final Animation_Design_BottomSheetDialog:I = 0x7f130008

.field public static final Animation_MaterialComponents_BottomSheetDialog:I = 0x7f130009

.field public static final BalanceActivityAmount:I = 0x7f13000a

.field public static final BalanceActivityAmount_CrossOut:I = 0x7f13000b

.field public static final BalanceActivityAmount_Positive:I = 0x7f13000c

.field public static final BalanceActivityDescription:I = 0x7f13000d

.field public static final BalanceActivityDescription_Pill:I = 0x7f13000e

.field public static final BalanceActivityDescription_Simple:I = 0x7f13000f

.field public static final Base_AlertDialog_AppCompat:I = 0x7f130010

.field public static final Base_AlertDialog_AppCompat_Light:I = 0x7f130011

.field public static final Base_Animation_AppCompat_Dialog:I = 0x7f130012

.field public static final Base_Animation_AppCompat_DropDownUp:I = 0x7f130013

.field public static final Base_Animation_AppCompat_Tooltip:I = 0x7f130014

.field public static final Base_CardView:I = 0x7f130015

.field public static final Base_DialogWindowTitleBackground_AppCompat:I = 0x7f130017

.field public static final Base_DialogWindowTitle_AppCompat:I = 0x7f130016

.field public static final Base_MaterialAlertDialog_MaterialComponents_Title_Icon:I = 0x7f130018

.field public static final Base_MaterialAlertDialog_MaterialComponents_Title_Panel:I = 0x7f130019

.field public static final Base_MaterialAlertDialog_MaterialComponents_Title_Text:I = 0x7f13001a

.field public static final Base_TextAppearance_AppCompat:I = 0x7f13001b

.field public static final Base_TextAppearance_AppCompat_Body1:I = 0x7f13001c

.field public static final Base_TextAppearance_AppCompat_Body2:I = 0x7f13001d

.field public static final Base_TextAppearance_AppCompat_Button:I = 0x7f13001e

.field public static final Base_TextAppearance_AppCompat_Caption:I = 0x7f13001f

.field public static final Base_TextAppearance_AppCompat_Display1:I = 0x7f130020

.field public static final Base_TextAppearance_AppCompat_Display2:I = 0x7f130021

.field public static final Base_TextAppearance_AppCompat_Display3:I = 0x7f130022

.field public static final Base_TextAppearance_AppCompat_Display4:I = 0x7f130023

.field public static final Base_TextAppearance_AppCompat_Headline:I = 0x7f130024

.field public static final Base_TextAppearance_AppCompat_Inverse:I = 0x7f130025

.field public static final Base_TextAppearance_AppCompat_Large:I = 0x7f130026

.field public static final Base_TextAppearance_AppCompat_Large_Inverse:I = 0x7f130027

.field public static final Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Large:I = 0x7f130028

.field public static final Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Small:I = 0x7f130029

.field public static final Base_TextAppearance_AppCompat_Medium:I = 0x7f13002a

.field public static final Base_TextAppearance_AppCompat_Medium_Inverse:I = 0x7f13002b

.field public static final Base_TextAppearance_AppCompat_Menu:I = 0x7f13002c

.field public static final Base_TextAppearance_AppCompat_SearchResult:I = 0x7f13002d

.field public static final Base_TextAppearance_AppCompat_SearchResult_Subtitle:I = 0x7f13002e

.field public static final Base_TextAppearance_AppCompat_SearchResult_Title:I = 0x7f13002f

.field public static final Base_TextAppearance_AppCompat_Small:I = 0x7f130030

.field public static final Base_TextAppearance_AppCompat_Small_Inverse:I = 0x7f130031

.field public static final Base_TextAppearance_AppCompat_Subhead:I = 0x7f130032

.field public static final Base_TextAppearance_AppCompat_Subhead_Inverse:I = 0x7f130033

.field public static final Base_TextAppearance_AppCompat_Title:I = 0x7f130034

.field public static final Base_TextAppearance_AppCompat_Title_Inverse:I = 0x7f130035

.field public static final Base_TextAppearance_AppCompat_Tooltip:I = 0x7f130036

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Menu:I = 0x7f130037

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle:I = 0x7f130038

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse:I = 0x7f130039

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Title:I = 0x7f13003a

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse:I = 0x7f13003b

.field public static final Base_TextAppearance_AppCompat_Widget_ActionMode_Subtitle:I = 0x7f13003c

.field public static final Base_TextAppearance_AppCompat_Widget_ActionMode_Title:I = 0x7f13003d

.field public static final Base_TextAppearance_AppCompat_Widget_Button:I = 0x7f13003e

.field public static final Base_TextAppearance_AppCompat_Widget_Button_Borderless_Colored:I = 0x7f13003f

.field public static final Base_TextAppearance_AppCompat_Widget_Button_Colored:I = 0x7f130040

.field public static final Base_TextAppearance_AppCompat_Widget_Button_Inverse:I = 0x7f130041

.field public static final Base_TextAppearance_AppCompat_Widget_DropDownItem:I = 0x7f130042

.field public static final Base_TextAppearance_AppCompat_Widget_PopupMenu_Header:I = 0x7f130043

.field public static final Base_TextAppearance_AppCompat_Widget_PopupMenu_Large:I = 0x7f130044

.field public static final Base_TextAppearance_AppCompat_Widget_PopupMenu_Small:I = 0x7f130045

.field public static final Base_TextAppearance_AppCompat_Widget_Switch:I = 0x7f130046

.field public static final Base_TextAppearance_AppCompat_Widget_TextView_SpinnerItem:I = 0x7f130047

.field public static final Base_TextAppearance_MaterialComponents_Badge:I = 0x7f130048

.field public static final Base_TextAppearance_MaterialComponents_Button:I = 0x7f130049

.field public static final Base_TextAppearance_MaterialComponents_Headline6:I = 0x7f13004a

.field public static final Base_TextAppearance_MaterialComponents_Subtitle2:I = 0x7f13004b

.field public static final Base_TextAppearance_Widget_AppCompat_ExpandedMenu_Item:I = 0x7f13004c

.field public static final Base_TextAppearance_Widget_AppCompat_Toolbar_Subtitle:I = 0x7f13004d

.field public static final Base_TextAppearance_Widget_AppCompat_Toolbar_Title:I = 0x7f13004e

.field public static final Base_ThemeOverlay_AppCompat:I = 0x7f130070

.field public static final Base_ThemeOverlay_AppCompat_ActionBar:I = 0x7f130071

.field public static final Base_ThemeOverlay_AppCompat_Dark:I = 0x7f130072

.field public static final Base_ThemeOverlay_AppCompat_Dark_ActionBar:I = 0x7f130073

.field public static final Base_ThemeOverlay_AppCompat_Dialog:I = 0x7f130074

.field public static final Base_ThemeOverlay_AppCompat_Dialog_Alert:I = 0x7f130075

.field public static final Base_ThemeOverlay_AppCompat_Light:I = 0x7f130076

.field public static final Base_ThemeOverlay_MaterialComponents_Dialog:I = 0x7f130077

.field public static final Base_ThemeOverlay_MaterialComponents_Dialog_Alert:I = 0x7f130078

.field public static final Base_ThemeOverlay_MaterialComponents_MaterialAlertDialog:I = 0x7f130079

.field public static final Base_Theme_AppCompat:I = 0x7f13004f

.field public static final Base_Theme_AppCompat_CompactMenu:I = 0x7f130050

.field public static final Base_Theme_AppCompat_Dialog:I = 0x7f130051

.field public static final Base_Theme_AppCompat_DialogWhenLarge:I = 0x7f130055

.field public static final Base_Theme_AppCompat_Dialog_Alert:I = 0x7f130052

.field public static final Base_Theme_AppCompat_Dialog_FixedSize:I = 0x7f130053

.field public static final Base_Theme_AppCompat_Dialog_MinWidth:I = 0x7f130054

.field public static final Base_Theme_AppCompat_Light:I = 0x7f130056

.field public static final Base_Theme_AppCompat_Light_DarkActionBar:I = 0x7f130057

.field public static final Base_Theme_AppCompat_Light_Dialog:I = 0x7f130058

.field public static final Base_Theme_AppCompat_Light_DialogWhenLarge:I = 0x7f13005c

.field public static final Base_Theme_AppCompat_Light_Dialog_Alert:I = 0x7f130059

.field public static final Base_Theme_AppCompat_Light_Dialog_FixedSize:I = 0x7f13005a

.field public static final Base_Theme_AppCompat_Light_Dialog_MinWidth:I = 0x7f13005b

.field public static final Base_Theme_MaterialComponents:I = 0x7f13005d

.field public static final Base_Theme_MaterialComponents_Bridge:I = 0x7f13005e

.field public static final Base_Theme_MaterialComponents_CompactMenu:I = 0x7f13005f

.field public static final Base_Theme_MaterialComponents_Dialog:I = 0x7f130060

.field public static final Base_Theme_MaterialComponents_DialogWhenLarge:I = 0x7f130065

.field public static final Base_Theme_MaterialComponents_Dialog_Alert:I = 0x7f130061

.field public static final Base_Theme_MaterialComponents_Dialog_Bridge:I = 0x7f130062

.field public static final Base_Theme_MaterialComponents_Dialog_FixedSize:I = 0x7f130063

.field public static final Base_Theme_MaterialComponents_Dialog_MinWidth:I = 0x7f130064

.field public static final Base_Theme_MaterialComponents_Light:I = 0x7f130066

.field public static final Base_Theme_MaterialComponents_Light_Bridge:I = 0x7f130067

.field public static final Base_Theme_MaterialComponents_Light_DarkActionBar:I = 0x7f130068

.field public static final Base_Theme_MaterialComponents_Light_DarkActionBar_Bridge:I = 0x7f130069

.field public static final Base_Theme_MaterialComponents_Light_Dialog:I = 0x7f13006a

.field public static final Base_Theme_MaterialComponents_Light_DialogWhenLarge:I = 0x7f13006f

.field public static final Base_Theme_MaterialComponents_Light_Dialog_Alert:I = 0x7f13006b

.field public static final Base_Theme_MaterialComponents_Light_Dialog_Bridge:I = 0x7f13006c

.field public static final Base_Theme_MaterialComponents_Light_Dialog_FixedSize:I = 0x7f13006d

.field public static final Base_Theme_MaterialComponents_Light_Dialog_MinWidth:I = 0x7f13006e

.field public static final Base_V14_ThemeOverlay_MaterialComponents_Dialog:I = 0x7f130083

.field public static final Base_V14_ThemeOverlay_MaterialComponents_Dialog_Alert:I = 0x7f130084

.field public static final Base_V14_ThemeOverlay_MaterialComponents_MaterialAlertDialog:I = 0x7f130085

.field public static final Base_V14_Theme_MaterialComponents:I = 0x7f13007a

.field public static final Base_V14_Theme_MaterialComponents_Bridge:I = 0x7f13007b

.field public static final Base_V14_Theme_MaterialComponents_Dialog:I = 0x7f13007c

.field public static final Base_V14_Theme_MaterialComponents_Dialog_Bridge:I = 0x7f13007d

.field public static final Base_V14_Theme_MaterialComponents_Light:I = 0x7f13007e

.field public static final Base_V14_Theme_MaterialComponents_Light_Bridge:I = 0x7f13007f

.field public static final Base_V14_Theme_MaterialComponents_Light_DarkActionBar_Bridge:I = 0x7f130080

.field public static final Base_V14_Theme_MaterialComponents_Light_Dialog:I = 0x7f130081

.field public static final Base_V14_Theme_MaterialComponents_Light_Dialog_Bridge:I = 0x7f130082

.field public static final Base_V21_ThemeOverlay_AppCompat_Dialog:I = 0x7f13008a

.field public static final Base_V21_Theme_AppCompat:I = 0x7f130086

.field public static final Base_V21_Theme_AppCompat_Dialog:I = 0x7f130087

.field public static final Base_V21_Theme_AppCompat_Light:I = 0x7f130088

.field public static final Base_V21_Theme_AppCompat_Light_Dialog:I = 0x7f130089

.field public static final Base_V22_Theme_AppCompat:I = 0x7f13008b

.field public static final Base_V22_Theme_AppCompat_Light:I = 0x7f13008c

.field public static final Base_V23_Theme_AppCompat:I = 0x7f13008d

.field public static final Base_V23_Theme_AppCompat_Light:I = 0x7f13008e

.field public static final Base_V26_Theme_AppCompat:I = 0x7f13008f

.field public static final Base_V26_Theme_AppCompat_Light:I = 0x7f130090

.field public static final Base_V26_Widget_AppCompat_Toolbar:I = 0x7f130091

.field public static final Base_V28_Theme_AppCompat:I = 0x7f130092

.field public static final Base_V28_Theme_AppCompat_Light:I = 0x7f130093

.field public static final Base_V7_ThemeOverlay_AppCompat_Dialog:I = 0x7f130098

.field public static final Base_V7_Theme_AppCompat:I = 0x7f130094

.field public static final Base_V7_Theme_AppCompat_Dialog:I = 0x7f130095

.field public static final Base_V7_Theme_AppCompat_Light:I = 0x7f130096

.field public static final Base_V7_Theme_AppCompat_Light_Dialog:I = 0x7f130097

.field public static final Base_V7_Widget_AppCompat_AutoCompleteTextView:I = 0x7f130099

.field public static final Base_V7_Widget_AppCompat_EditText:I = 0x7f13009a

.field public static final Base_V7_Widget_AppCompat_Toolbar:I = 0x7f13009b

.field public static final Base_Widget_AppCompat_ActionBar:I = 0x7f13009c

.field public static final Base_Widget_AppCompat_ActionBar_Solid:I = 0x7f13009d

.field public static final Base_Widget_AppCompat_ActionBar_TabBar:I = 0x7f13009e

.field public static final Base_Widget_AppCompat_ActionBar_TabText:I = 0x7f13009f

.field public static final Base_Widget_AppCompat_ActionBar_TabView:I = 0x7f1300a0

.field public static final Base_Widget_AppCompat_ActionButton:I = 0x7f1300a1

.field public static final Base_Widget_AppCompat_ActionButton_CloseMode:I = 0x7f1300a2

.field public static final Base_Widget_AppCompat_ActionButton_Overflow:I = 0x7f1300a3

.field public static final Base_Widget_AppCompat_ActionMode:I = 0x7f1300a4

.field public static final Base_Widget_AppCompat_ActivityChooserView:I = 0x7f1300a5

.field public static final Base_Widget_AppCompat_AutoCompleteTextView:I = 0x7f1300a6

.field public static final Base_Widget_AppCompat_Button:I = 0x7f1300a7

.field public static final Base_Widget_AppCompat_ButtonBar:I = 0x7f1300ad

.field public static final Base_Widget_AppCompat_ButtonBar_AlertDialog:I = 0x7f1300ae

.field public static final Base_Widget_AppCompat_Button_Borderless:I = 0x7f1300a8

.field public static final Base_Widget_AppCompat_Button_Borderless_Colored:I = 0x7f1300a9

.field public static final Base_Widget_AppCompat_Button_ButtonBar_AlertDialog:I = 0x7f1300aa

.field public static final Base_Widget_AppCompat_Button_Colored:I = 0x7f1300ab

.field public static final Base_Widget_AppCompat_Button_Small:I = 0x7f1300ac

.field public static final Base_Widget_AppCompat_CompoundButton_CheckBox:I = 0x7f1300af

.field public static final Base_Widget_AppCompat_CompoundButton_RadioButton:I = 0x7f1300b0

.field public static final Base_Widget_AppCompat_CompoundButton_Switch:I = 0x7f1300b1

.field public static final Base_Widget_AppCompat_DrawerArrowToggle:I = 0x7f1300b2

.field public static final Base_Widget_AppCompat_DrawerArrowToggle_Common:I = 0x7f1300b3

.field public static final Base_Widget_AppCompat_DropDownItem_Spinner:I = 0x7f1300b4

.field public static final Base_Widget_AppCompat_EditText:I = 0x7f1300b5

.field public static final Base_Widget_AppCompat_ImageButton:I = 0x7f1300b6

.field public static final Base_Widget_AppCompat_Light_ActionBar:I = 0x7f1300b7

.field public static final Base_Widget_AppCompat_Light_ActionBar_Solid:I = 0x7f1300b8

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabBar:I = 0x7f1300b9

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabText:I = 0x7f1300ba

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabText_Inverse:I = 0x7f1300bb

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabView:I = 0x7f1300bc

.field public static final Base_Widget_AppCompat_Light_PopupMenu:I = 0x7f1300bd

.field public static final Base_Widget_AppCompat_Light_PopupMenu_Overflow:I = 0x7f1300be

.field public static final Base_Widget_AppCompat_ListMenuView:I = 0x7f1300bf

.field public static final Base_Widget_AppCompat_ListPopupWindow:I = 0x7f1300c0

.field public static final Base_Widget_AppCompat_ListView:I = 0x7f1300c1

.field public static final Base_Widget_AppCompat_ListView_DropDown:I = 0x7f1300c2

.field public static final Base_Widget_AppCompat_ListView_Menu:I = 0x7f1300c3

.field public static final Base_Widget_AppCompat_PopupMenu:I = 0x7f1300c4

.field public static final Base_Widget_AppCompat_PopupMenu_Overflow:I = 0x7f1300c5

.field public static final Base_Widget_AppCompat_PopupWindow:I = 0x7f1300c6

.field public static final Base_Widget_AppCompat_ProgressBar:I = 0x7f1300c7

.field public static final Base_Widget_AppCompat_ProgressBar_Horizontal:I = 0x7f1300c8

.field public static final Base_Widget_AppCompat_RatingBar:I = 0x7f1300c9

.field public static final Base_Widget_AppCompat_RatingBar_Indicator:I = 0x7f1300ca

.field public static final Base_Widget_AppCompat_RatingBar_Small:I = 0x7f1300cb

.field public static final Base_Widget_AppCompat_SearchView:I = 0x7f1300cc

.field public static final Base_Widget_AppCompat_SearchView_ActionBar:I = 0x7f1300cd

.field public static final Base_Widget_AppCompat_SeekBar:I = 0x7f1300ce

.field public static final Base_Widget_AppCompat_SeekBar_Discrete:I = 0x7f1300cf

.field public static final Base_Widget_AppCompat_Spinner:I = 0x7f1300d0

.field public static final Base_Widget_AppCompat_Spinner_Underlined:I = 0x7f1300d1

.field public static final Base_Widget_AppCompat_TextView:I = 0x7f1300d2

.field public static final Base_Widget_AppCompat_TextView_SpinnerItem:I = 0x7f1300d3

.field public static final Base_Widget_AppCompat_Toolbar:I = 0x7f1300d4

.field public static final Base_Widget_AppCompat_Toolbar_Button_Navigation:I = 0x7f1300d5

.field public static final Base_Widget_Design_TabLayout:I = 0x7f1300d6

.field public static final Base_Widget_MaterialComponents_AutoCompleteTextView:I = 0x7f1300d7

.field public static final Base_Widget_MaterialComponents_CheckedTextView:I = 0x7f1300d8

.field public static final Base_Widget_MaterialComponents_Chip:I = 0x7f1300d9

.field public static final Base_Widget_MaterialComponents_PopupMenu:I = 0x7f1300da

.field public static final Base_Widget_MaterialComponents_PopupMenu_ContextMenu:I = 0x7f1300db

.field public static final Base_Widget_MaterialComponents_PopupMenu_ListPopupWindow:I = 0x7f1300dc

.field public static final Base_Widget_MaterialComponents_PopupMenu_Overflow:I = 0x7f1300dd

.field public static final Base_Widget_MaterialComponents_TextInputEditText:I = 0x7f1300de

.field public static final Base_Widget_MaterialComponents_TextInputLayout:I = 0x7f1300df

.field public static final Base_Widget_MaterialComponents_TextView:I = 0x7f1300e0

.field public static final Base_Widget_Noho_Button:I = 0x7f1300e1

.field public static final BottomSheetDialogStyle:I = 0x7f1300e2

.field public static final CalendarCell:I = 0x7f1300e3

.field public static final CalendarCell_CalendarDate:I = 0x7f1300e4

.field public static final CalendarCell_DayHeader:I = 0x7f1300e5

.field public static final CalendarTitle:I = 0x7f1300e6

.field public static final CapitalBodyText:I = 0x7f1300e7

.field public static final CapitalBodyTitle:I = 0x7f1300e8

.field public static final CapitalHeroText:I = 0x7f1300e9

.field public static final CapitalHeroTitle:I = 0x7f1300ea

.field public static final CapitalNohoMediumOversized:I = 0x7f1300eb

.field public static final CardView:I = 0x7f1300ec

.field public static final CardView_Dark:I = 0x7f1300ed

.field public static final CardView_Light:I = 0x7f1300ee

.field public static final Cardreader_Dark:I = 0x7f1300ef

.field public static final Cardreader_Light:I = 0x7f1300f0

.field public static final CustomerTextIcon:I = 0x7f1300f1

.field public static final CustomizeReportTimeValue:I = 0x7f1300f2

.field public static final DateTimePicker:I = 0x7f1300f3

.field public static final DepositsRecyclerEdges:I = 0x7f1300f4

.field public static final Display:I = 0x7f1300f5

.field public static final DisputesNohoScrollViewPaddingBySize:I = 0x7f1300f6

.field public static final ElevatedWhiteRoundedRectangle:I = 0x7f1300f7

.field public static final EmptyTheme:I = 0x7f1300f8

.field public static final FontSelection_Noho:I = 0x7f1300f9

.field public static final FullSheetCustomerInputLayout:I = 0x7f1300fa

.field public static final FullSheetCustomerInputLayout_Button:I = 0x7f1300fb

.field public static final FullSheetCustomerInputLayout_Glyph:I = 0x7f1300fc

.field public static final FullSheetCustomerInputLayout_HelpText:I = 0x7f1300fd

.field public static final FullSheetCustomerInputLayout_Main:I = 0x7f1300fe

.field public static final FullSheetCustomerInputLayout_Message:I = 0x7f1300ff

.field public static final FullSheetCustomerInputLayout_Title:I = 0x7f130100

.field public static final HSDivider:I = 0x7f130101

.field public static final Helpshift:I = 0x7f130102

.field public static final Helpshift_Style:I = 0x7f130103

.field public static final Helpshift_Style_AdminAttachmentBubble:I = 0x7f130104

.field public static final Helpshift_Style_AdminMessageDate:I = 0x7f130105

.field public static final Helpshift_Style_AdminTextMessage:I = 0x7f130106

.field public static final Helpshift_Style_AdminTextMessageContainer:I = 0x7f130107

.field public static final Helpshift_Style_AttachmentBubble:I = 0x7f130108

.field public static final Helpshift_Style_BorderlessButton:I = 0x7f130109

.field public static final Helpshift_Style_ContactUsButton:I = 0x7f13010a

.field public static final Helpshift_Style_FaqsListItem:I = 0x7f13010b

.field public static final Helpshift_Style_PickerOptionContainer:I = 0x7f13010c

.field public static final Helpshift_Style_PickerOptionText:I = 0x7f13010d

.field public static final Helpshift_Style_QuestionLoadProgress:I = 0x7f13010e

.field public static final Helpshift_Style_RatingBar:I = 0x7f13010f

.field public static final Helpshift_Style_SectionTabs_Dark:I = 0x7f130110

.field public static final Helpshift_Style_SectionTabs_Light:I = 0x7f130111

.field public static final Helpshift_Style_TextInputLayout_Error:I = 0x7f130112

.field public static final Helpshift_Style_Toolbar_Dark:I = 0x7f130113

.field public static final Helpshift_Style_Toolbar_Light:I = 0x7f130114

.field public static final Helpshift_Style_UserAttachmentBubble:I = 0x7f130115

.field public static final Helpshift_Style_UserMessageDate:I = 0x7f130116

.field public static final Helpshift_Style_UserTextMessage:I = 0x7f130117

.field public static final Helpshift_Style_UserTextMessageContainer:I = 0x7f130118

.field public static final Helpshift_Theme_Activity:I = 0x7f130119

.field public static final Helpshift_Theme_Base:I = 0x7f13011a

.field public static final Helpshift_Theme_Dark:I = 0x7f13011b

.field public static final Helpshift_Theme_Dialog:I = 0x7f13011c

.field public static final Helpshift_Theme_HighContrast:I = 0x7f13011d

.field public static final Helpshift_Theme_Light:I = 0x7f13011e

.field public static final Helpshift_Theme_Light_DarkActionBar:I = 0x7f13011f

.field public static final JediComponent:I = 0x7f130120

.field public static final JediComponent_LargeBottomPadding:I = 0x7f130121

.field public static final JediComponent_LargeVerticalPadding:I = 0x7f130122

.field public static final JediComponent_SmallVerticalPadding:I = 0x7f130123

.field public static final JediComponent_TextAppearance:I = 0x7f130124

.field public static final JediComponent_TextAppearance_Heading:I = 0x7f130125

.field public static final JediComponent_TextAppearance_Subheading:I = 0x7f130126

.field public static final JediComponent_TextAppearance_Superheading:I = 0x7f130127

.field public static final Label:I = 0x7f130128

.field public static final Label_LightGray:I = 0x7f130129

.field public static final Label_LightGray_BuyerCart:I = 0x7f13012a

.field public static final Label_White:I = 0x7f13012b

.field public static final LibraryItemListNohoRow_IconAction:I = 0x7f13012c

.field public static final LightDisplay:I = 0x7f13012d

.field public static final LightDisplay_DarkGray:I = 0x7f13012e

.field public static final LightDisplay_LightGray:I = 0x7f13012f

.field public static final LightDisplay_White:I = 0x7f130130

.field public static final LightDisplay_White_Light:I = 0x7f130131

.field public static final LightTitle:I = 0x7f130132

.field public static final LightTitle_DarkGray:I = 0x7f130133

.field public static final LightTitle_White:I = 0x7f130134

.field public static final LoyaltyReportBody2:I = 0x7f130135

.field public static final LoyaltyReportSummaryLarge:I = 0x7f130136

.field public static final MarinButtonBar:I = 0x7f130137

.field public static final MaterialAlertDialog_MaterialComponents:I = 0x7f130138

.field public static final MaterialAlertDialog_MaterialComponents_Body_Text:I = 0x7f130139

.field public static final MaterialAlertDialog_MaterialComponents_Picker_Date_Calendar:I = 0x7f13013a

.field public static final MaterialAlertDialog_MaterialComponents_Picker_Date_Spinner:I = 0x7f13013b

.field public static final MaterialAlertDialog_MaterialComponents_Title_Icon:I = 0x7f13013c

.field public static final MaterialAlertDialog_MaterialComponents_Title_Icon_CenterStacked:I = 0x7f13013d

.field public static final MaterialAlertDialog_MaterialComponents_Title_Panel:I = 0x7f13013e

.field public static final MaterialAlertDialog_MaterialComponents_Title_Panel_CenterStacked:I = 0x7f13013f

.field public static final MaterialAlertDialog_MaterialComponents_Title_Text:I = 0x7f130140

.field public static final MaterialAlertDialog_MaterialComponents_Title_Text_CenterStacked:I = 0x7f130141

.field public static final MediumTitle:I = 0x7f130142

.field public static final MediumTitle_Blue:I = 0x7f130143

.field public static final MediumTitle_DarkGray:I = 0x7f130144

.field public static final MediumTitle_DarkGray_LightGrayHint:I = 0x7f130145

.field public static final MediumTitle_LightGray:I = 0x7f130146

.field public static final MediumTitle_White:I = 0x7f130147

.field public static final MyCustomHelpshiftTheme:I = 0x7f130148

.field public static final None:I = 0x7f130149

.field public static final NotificationPreferencesItem:I = 0x7f13014a

.field public static final NotificationTab:I = 0x7f13014b

.field public static final NotificationTab_Selected:I = 0x7f13014c

.field public static final NotificationTab_Unselected:I = 0x7f13014d

.field public static final OrientationVerticalOnPhonePortrait:I = 0x7f13014e

.field public static final PaymentPromptLayout:I = 0x7f13014f

.field public static final PaymentPromptLayout_Card:I = 0x7f130150

.field public static final PaymentPromptLayout_CardSlot:I = 0x7f130151

.field public static final PaymentPromptLayout_Content:I = 0x7f130152

.field public static final PaymentPromptLayout_Content_Icon:I = 0x7f130153

.field public static final PaymentPromptLayout_Content_PaymentType:I = 0x7f130154

.field public static final PaymentPromptLayout_Content_Total:I = 0x7f130155

.field public static final PaymentPromptLayout_Content_TransactionType:I = 0x7f130156

.field public static final PaymentPromptLayout_Header:I = 0x7f130157

.field public static final PaymentPromptLayout_Header_BuyerLanguageButton:I = 0x7f130158

.field public static final PaymentPromptLayout_Header_UpButton:I = 0x7f130159

.field public static final Platform_AppCompat:I = 0x7f13015a

.field public static final Platform_AppCompat_Light:I = 0x7f13015b

.field public static final Platform_MaterialComponents:I = 0x7f13015c

.field public static final Platform_MaterialComponents_Dialog:I = 0x7f13015d

.field public static final Platform_MaterialComponents_Light:I = 0x7f13015e

.field public static final Platform_MaterialComponents_Light_Dialog:I = 0x7f13015f

.field public static final Platform_ThemeOverlay_AppCompat:I = 0x7f130160

.field public static final Platform_ThemeOverlay_AppCompat_Dark:I = 0x7f130161

.field public static final Platform_ThemeOverlay_AppCompat_Light:I = 0x7f130162

.field public static final Platform_V21_AppCompat:I = 0x7f130163

.field public static final Platform_V21_AppCompat_Light:I = 0x7f130164

.field public static final Platform_V25_AppCompat:I = 0x7f130165

.field public static final Platform_V25_AppCompat_Light:I = 0x7f130166

.field public static final Platform_Widget_AppCompat_Spinner:I = 0x7f130167

.field public static final QuickAmountsScrollViewPaddingBySize:I = 0x7f13016b

.field public static final QuickAmounts_Add:I = 0x7f130168

.field public static final QuickAmounts_Add_Icon:I = 0x7f130169

.field public static final QuickAmounts_Value:I = 0x7f13016a

.field public static final Rate_Subitle:I = 0x7f13016c

.field public static final Rate_Title:I = 0x7f13016d

.field public static final RecentActivityRowPositiveAmount:I = 0x7f13016e

.field public static final RecyclerViewPaddingBySize:I = 0x7f13016f

.field public static final RegularDisplay:I = 0x7f130170

.field public static final RegularDisplay_DarkGray:I = 0x7f130171

.field public static final RegularDisplay_LightGray:I = 0x7f130172

.field public static final RegularDisplay_White:I = 0x7f130173

.field public static final RegularTitle:I = 0x7f130174

.field public static final RegularTitle_DarkGray:I = 0x7f130175

.field public static final RegularTitle_DarkGray_LightGrayHint:I = 0x7f130176

.field public static final RegularTitle_LightGray:I = 0x7f130177

.field public static final RegularTitle_White:I = 0x7f130178

.field public static final RegularTitle_White_BuyerCart:I = 0x7f130179

.field public static final RtlOverlay_DialogWindowTitle_AppCompat:I = 0x7f13017a

.field public static final RtlOverlay_Widget_AppCompat_ActionBar_TitleItem:I = 0x7f13017b

.field public static final RtlOverlay_Widget_AppCompat_DialogTitle_Icon:I = 0x7f13017c

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem:I = 0x7f13017d

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem_InternalGroup:I = 0x7f13017e

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem_Shortcut:I = 0x7f13017f

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem_SubmenuArrow:I = 0x7f130180

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem_Text:I = 0x7f130181

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem_Title:I = 0x7f130182

.field public static final RtlOverlay_Widget_AppCompat_SearchView_MagIcon:I = 0x7f130188

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown:I = 0x7f130183

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Icon1:I = 0x7f130184

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Icon2:I = 0x7f130185

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Query:I = 0x7f130186

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Text:I = 0x7f130187

.field public static final RtlUnderlay_Widget_AppCompat_ActionButton:I = 0x7f130189

.field public static final RtlUnderlay_Widget_AppCompat_ActionButton_Overflow:I = 0x7f13018a

.field public static final SalesReportChartView:I = 0x7f13018d

.field public static final SalesReportOverviewAmount:I = 0x7f13018e

.field public static final SalesReportOverviewTableAmountCell:I = 0x7f13018f

.field public static final SalesReportOverviewTableAmountCell_EndCell:I = 0x7f130190

.field public static final SalesReportThreeColumnHeaderRow:I = 0x7f130191

.field public static final SalesReportThreeColumnLayout:I = 0x7f130192

.field public static final SalesReportThreeColumnLayout_Clickable:I = 0x7f130193

.field public static final SalesReportThreeColumnRow:I = 0x7f130194

.field public static final SalesReportThreeColumnRow_Center:I = 0x7f130195

.field public static final SalesReportThreeColumnRow_Left:I = 0x7f130196

.field public static final SalesReportThreeColumnRow_Right:I = 0x7f130197

.field public static final SalesReportTimeSelectorItem:I = 0x7f130198

.field public static final SalesReportTimeSelectorItem_Label:I = 0x7f130199

.field public static final SalesReportTwoColumnLayout:I = 0x7f13019a

.field public static final SalesReportTwoColumnLayout_Clickable:I = 0x7f13019b

.field public static final SalesReport_TextAppearance_AmountCell_Label:I = 0x7f13018b

.field public static final SalesReport_TextAppearance_Subheader:I = 0x7f13018c

.field public static final SettingsNohoScrollViewPaddingBySize:I = 0x7f13019c

.field public static final SetupGuideIntroCard:I = 0x7f13019d

.field public static final SetupGuideOrderReaderCard:I = 0x7f13019e

.field public static final ShapeAppearanceOverlay:I = 0x7f1301a4

.field public static final ShapeAppearanceOverlay_BottomLeftDifferentCornerSize:I = 0x7f1301a5

.field public static final ShapeAppearanceOverlay_BottomRightCut:I = 0x7f1301a6

.field public static final ShapeAppearanceOverlay_Cut:I = 0x7f1301a7

.field public static final ShapeAppearanceOverlay_DifferentCornerSize:I = 0x7f1301a8

.field public static final ShapeAppearanceOverlay_MaterialComponents_BottomSheet:I = 0x7f1301a9

.field public static final ShapeAppearanceOverlay_MaterialComponents_Chip:I = 0x7f1301aa

.field public static final ShapeAppearanceOverlay_MaterialComponents_ExtendedFloatingActionButton:I = 0x7f1301ab

.field public static final ShapeAppearanceOverlay_MaterialComponents_FloatingActionButton:I = 0x7f1301ac

.field public static final ShapeAppearanceOverlay_MaterialComponents_MaterialCalendar_Day:I = 0x7f1301ad

.field public static final ShapeAppearanceOverlay_MaterialComponents_MaterialCalendar_Window_Fullscreen:I = 0x7f1301ae

.field public static final ShapeAppearanceOverlay_MaterialComponents_MaterialCalendar_Year:I = 0x7f1301af

.field public static final ShapeAppearanceOverlay_MaterialComponents_TextInputLayout_FilledBox:I = 0x7f1301b0

.field public static final ShapeAppearanceOverlay_TopLeftCut:I = 0x7f1301b1

.field public static final ShapeAppearanceOverlay_TopRightDifferentCornerSize:I = 0x7f1301b2

.field public static final ShapeAppearance_MaterialComponents:I = 0x7f13019f

.field public static final ShapeAppearance_MaterialComponents_LargeComponent:I = 0x7f1301a0

.field public static final ShapeAppearance_MaterialComponents_MediumComponent:I = 0x7f1301a1

.field public static final ShapeAppearance_MaterialComponents_SmallComponent:I = 0x7f1301a2

.field public static final ShapeAppearance_MaterialComponents_Test:I = 0x7f1301a3

.field public static final SlideDownButtonRow:I = 0x7f1301b3

.field public static final SlideDownContent:I = 0x7f1301b4

.field public static final SplashScreenImage:I = 0x7f1301b5

.field public static final SplashScreenText:I = 0x7f1301b6

.field public static final SquareCardNohoScrollViewPaddingBySize:I = 0x7f1301b7

.field public static final SquareTextAppearance_TipSettings_Label_Description:I = 0x7f1301b8

.field public static final SquareTextAppearance_TipSettings_Label_Header:I = 0x7f1301b9

.field public static final SquareTextAppearance_TipSettings_Row_Label:I = 0x7f1301ba

.field public static final SquareTextAppearance_TipSettings_Row_Value:I = 0x7f1301bb

.field public static final StatusBar:I = 0x7f1301bc

.field public static final StatusBarControl:I = 0x7f1301bd

.field public static final StatusBarControl_Icon:I = 0x7f1301be

.field public static final StatusBarControl_Icon_TextView:I = 0x7f1301bf

.field public static final StatusBarControl_Left:I = 0x7f1301c0

.field public static final StatusBarControl_Left_TextView:I = 0x7f1301c1

.field public static final StatusBarControl_Right:I = 0x7f1301c2

.field public static final StatusBarControl_Right_TextView:I = 0x7f1301c3

.field public static final StatusBarGlyph:I = 0x7f1301c4

.field public static final StatusBarView:I = 0x7f1301c5

.field public static final Style_Noho_Dialog:I = 0x7f1301c6

.field public static final Style_Noho_Dialog_Message:I = 0x7f1301c7

.field public static final Style_Noho_Dialog_Title:I = 0x7f1301c8

.field public static final SwitcherButtonLayout:I = 0x7f1301c9

.field public static final SwitcherButtonTextView:I = 0x7f1301ca

.field public static final SwitcherButtonTextView_Notification:I = 0x7f1301cb

.field public static final SwitcherButtonTextView_Title:I = 0x7f1301cc

.field public static final SwitcherButtonVerticalStrip:I = 0x7f1301cd

.field public static final TestStyleWithLineHeight:I = 0x7f1301d3

.field public static final TestStyleWithLineHeightAppearance:I = 0x7f1301d4

.field public static final TestStyleWithThemeLineHeightAttribute:I = 0x7f1301d5

.field public static final TestStyleWithoutLineHeight:I = 0x7f1301d6

.field public static final TestThemeWithLineHeight:I = 0x7f1301d7

.field public static final TestThemeWithLineHeightDisabled:I = 0x7f1301d8

.field public static final Test_ShapeAppearanceOverlay_MaterialComponents_MaterialCalendar_Day:I = 0x7f1301ce

.field public static final Test_Theme_MaterialComponents_MaterialCalendar:I = 0x7f1301cf

.field public static final Test_Widget_MaterialComponents_MaterialCalendar:I = 0x7f1301d0

.field public static final Test_Widget_MaterialComponents_MaterialCalendar_Day:I = 0x7f1301d1

.field public static final Test_Widget_MaterialComponents_MaterialCalendar_Day_Selected:I = 0x7f1301d2

.field public static final TextAppearance_AppCompat:I = 0x7f1301d9

.field public static final TextAppearance_AppCompat_Body1:I = 0x7f1301da

.field public static final TextAppearance_AppCompat_Body2:I = 0x7f1301db

.field public static final TextAppearance_AppCompat_Button:I = 0x7f1301dc

.field public static final TextAppearance_AppCompat_Caption:I = 0x7f1301dd

.field public static final TextAppearance_AppCompat_Display1:I = 0x7f1301de

.field public static final TextAppearance_AppCompat_Display2:I = 0x7f1301df

.field public static final TextAppearance_AppCompat_Display3:I = 0x7f1301e0

.field public static final TextAppearance_AppCompat_Display4:I = 0x7f1301e1

.field public static final TextAppearance_AppCompat_Headline:I = 0x7f1301e2

.field public static final TextAppearance_AppCompat_Inverse:I = 0x7f1301e3

.field public static final TextAppearance_AppCompat_Large:I = 0x7f1301e4

.field public static final TextAppearance_AppCompat_Large_Inverse:I = 0x7f1301e5

.field public static final TextAppearance_AppCompat_Light_SearchResult_Subtitle:I = 0x7f1301e6

.field public static final TextAppearance_AppCompat_Light_SearchResult_Title:I = 0x7f1301e7

.field public static final TextAppearance_AppCompat_Light_Widget_PopupMenu_Large:I = 0x7f1301e8

.field public static final TextAppearance_AppCompat_Light_Widget_PopupMenu_Small:I = 0x7f1301e9

.field public static final TextAppearance_AppCompat_Medium:I = 0x7f1301ea

.field public static final TextAppearance_AppCompat_Medium_Inverse:I = 0x7f1301eb

.field public static final TextAppearance_AppCompat_Menu:I = 0x7f1301ec

.field public static final TextAppearance_AppCompat_SearchResult_Subtitle:I = 0x7f1301ed

.field public static final TextAppearance_AppCompat_SearchResult_Title:I = 0x7f1301ee

.field public static final TextAppearance_AppCompat_Small:I = 0x7f1301ef

.field public static final TextAppearance_AppCompat_Small_Inverse:I = 0x7f1301f0

.field public static final TextAppearance_AppCompat_Subhead:I = 0x7f1301f1

.field public static final TextAppearance_AppCompat_Subhead_Inverse:I = 0x7f1301f2

.field public static final TextAppearance_AppCompat_Title:I = 0x7f1301f3

.field public static final TextAppearance_AppCompat_Title_Inverse:I = 0x7f1301f4

.field public static final TextAppearance_AppCompat_Tooltip:I = 0x7f1301f5

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Menu:I = 0x7f1301f6

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Subtitle:I = 0x7f1301f7

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse:I = 0x7f1301f8

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Title:I = 0x7f1301f9

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse:I = 0x7f1301fa

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Subtitle:I = 0x7f1301fb

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Subtitle_Inverse:I = 0x7f1301fc

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Title:I = 0x7f1301fd

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Title_Inverse:I = 0x7f1301fe

.field public static final TextAppearance_AppCompat_Widget_Button:I = 0x7f1301ff

.field public static final TextAppearance_AppCompat_Widget_Button_Borderless_Colored:I = 0x7f130200

.field public static final TextAppearance_AppCompat_Widget_Button_Colored:I = 0x7f130201

.field public static final TextAppearance_AppCompat_Widget_Button_Inverse:I = 0x7f130202

.field public static final TextAppearance_AppCompat_Widget_DropDownItem:I = 0x7f130203

.field public static final TextAppearance_AppCompat_Widget_PopupMenu_Header:I = 0x7f130204

.field public static final TextAppearance_AppCompat_Widget_PopupMenu_Large:I = 0x7f130205

.field public static final TextAppearance_AppCompat_Widget_PopupMenu_Small:I = 0x7f130206

.field public static final TextAppearance_AppCompat_Widget_Switch:I = 0x7f130207

.field public static final TextAppearance_AppCompat_Widget_TextView_SpinnerItem:I = 0x7f130208

.field public static final TextAppearance_Badge_Drawer:I = 0x7f130209

.field public static final TextAppearance_Badge_Drawer_High:I = 0x7f13020a

.field public static final TextAppearance_Badge_Drawer_NotificationCenter:I = 0x7f13020b

.field public static final TextAppearance_BuyerFacing_LargeTitle:I = 0x7f13020c

.field public static final TextAppearance_BuyerFacing_SmallSubitle:I = 0x7f13020d

.field public static final TextAppearance_BuyerFacing_SmallTitle:I = 0x7f13020e

.field public static final TextAppearance_Compat_Notification:I = 0x7f13020f

.field public static final TextAppearance_Compat_Notification_Info:I = 0x7f130210

.field public static final TextAppearance_Compat_Notification_Info_Media:I = 0x7f130211

.field public static final TextAppearance_Compat_Notification_Line2:I = 0x7f130212

.field public static final TextAppearance_Compat_Notification_Line2_Media:I = 0x7f130213

.field public static final TextAppearance_Compat_Notification_Media:I = 0x7f130214

.field public static final TextAppearance_Compat_Notification_Time:I = 0x7f130215

.field public static final TextAppearance_Compat_Notification_Time_Media:I = 0x7f130216

.field public static final TextAppearance_Compat_Notification_Title:I = 0x7f130217

.field public static final TextAppearance_Compat_Notification_Title_Media:I = 0x7f130218

.field public static final TextAppearance_ConfigureItem_EditTextSuffix:I = 0x7f130219

.field public static final TextAppearance_ConfigureItem_EditTextSuffix_Hint:I = 0x7f13021a

.field public static final TextAppearance_Design_CollapsingToolbar_Expanded:I = 0x7f13021b

.field public static final TextAppearance_Design_Counter:I = 0x7f13021c

.field public static final TextAppearance_Design_Counter_Overflow:I = 0x7f13021d

.field public static final TextAppearance_Design_Error:I = 0x7f13021e

.field public static final TextAppearance_Design_HelperText:I = 0x7f13021f

.field public static final TextAppearance_Design_Hint:I = 0x7f130220

.field public static final TextAppearance_Design_Snackbar_Message:I = 0x7f130221

.field public static final TextAppearance_Design_Tab:I = 0x7f130222

.field public static final TextAppearance_Disputes_Description_Actionable:I = 0x7f130223

.field public static final TextAppearance_Disputes_Description_Lost:I = 0x7f130224

.field public static final TextAppearance_Disputes_Description_Pending:I = 0x7f130225

.field public static final TextAppearance_Disputes_Description_Won:I = 0x7f130226

.field public static final TextAppearance_Disputes_Row_Link:I = 0x7f130227

.field public static final TextAppearance_Disputes_Row_Value:I = 0x7f130228

.field public static final TextAppearance_Disputes_Row_Value_Bold:I = 0x7f130229

.field public static final TextAppearance_Invoices_V2_Button_Label:I = 0x7f13022a

.field public static final TextAppearance_Invoices_V2_PaymentSchedule_InstallmentLabel:I = 0x7f13022b

.field public static final TextAppearance_Invoices_V2_PaymentSchedule_Value:I = 0x7f13022c

.field public static final TextAppearance_Invoices_V2_Row_Description:I = 0x7f13022d

.field public static final TextAppearance_Invoices_V2_ToggleRow_Label:I = 0x7f13022e

.field public static final TextAppearance_Marin:I = 0x7f13022f

.field public static final TextAppearance_Marin_Badge_Drawer:I = 0x7f130230

.field public static final TextAppearance_Marin_Badge_Hamburger:I = 0x7f130231

.field public static final TextAppearance_Marin_Badge_Hamburger_Wide:I = 0x7f130232

.field public static final TextAppearance_Marin_Blue:I = 0x7f130233

.field public static final TextAppearance_Marin_Charge:I = 0x7f130234

.field public static final TextAppearance_Marin_Charge_White:I = 0x7f130235

.field public static final TextAppearance_Marin_Display:I = 0x7f130236

.field public static final TextAppearance_Marin_Gray:I = 0x7f130237

.field public static final TextAppearance_Marin_Gray_Landing:I = 0x7f130238

.field public static final TextAppearance_Marin_HeaderTitle:I = 0x7f130239

.field public static final TextAppearance_Marin_HeaderTitle_Blue:I = 0x7f13023a

.field public static final TextAppearance_Marin_HeaderTitle_Margin:I = 0x7f13023b

.field public static final TextAppearance_Marin_HeaderTitle_Medium:I = 0x7f13023c

.field public static final TextAppearance_Marin_HeaderTitle_MediumGray:I = 0x7f130241

.field public static final TextAppearance_Marin_HeaderTitle_Medium_Blue:I = 0x7f13023d

.field public static final TextAppearance_Marin_HeaderTitle_Medium_Centered:I = 0x7f13023e

.field public static final TextAppearance_Marin_HeaderTitle_Medium_Centered_Blue:I = 0x7f13023f

.field public static final TextAppearance_Marin_HeaderTitle_Medium_White:I = 0x7f130240

.field public static final TextAppearance_Marin_HeaderTitle_NullState:I = 0x7f130242

.field public static final TextAppearance_Marin_HeaderTitle_White:I = 0x7f130243

.field public static final TextAppearance_Marin_Headline:I = 0x7f130244

.field public static final TextAppearance_Marin_Headline_Medium:I = 0x7f130245

.field public static final TextAppearance_Marin_Headline_White:I = 0x7f130246

.field public static final TextAppearance_Marin_Headline_White_Medium:I = 0x7f130247

.field public static final TextAppearance_Marin_HelpText:I = 0x7f130248

.field public static final TextAppearance_Marin_HelpText_DarkGray:I = 0x7f130249

.field public static final TextAppearance_Marin_HelpText_Large:I = 0x7f13024a

.field public static final TextAppearance_Marin_HelpText_Large_Italic:I = 0x7f13024b

.field public static final TextAppearance_Marin_HelpText_Red:I = 0x7f13024c

.field public static final TextAppearance_Marin_HelpText_Sign:I = 0x7f13024d

.field public static final TextAppearance_Marin_HelpText_White:I = 0x7f13024e

.field public static final TextAppearance_Marin_InvoiceDetailHeader_PrimaryValue:I = 0x7f13024f

.field public static final TextAppearance_Marin_Keypad:I = 0x7f130250

.field public static final TextAppearance_Marin_Keypad_Home:I = 0x7f130251

.field public static final TextAppearance_Marin_Light:I = 0x7f130252

.field public static final TextAppearance_Marin_Medium:I = 0x7f130253

.field public static final TextAppearance_Marin_Medium_Blue:I = 0x7f130254

.field public static final TextAppearance_Marin_Medium_Centered:I = 0x7f130255

.field public static final TextAppearance_Marin_Medium_Centered_Selectable:I = 0x7f130256

.field public static final TextAppearance_Marin_Medium_Green:I = 0x7f130257

.field public static final TextAppearance_Marin_Medium_White:I = 0x7f130258

.field public static final TextAppearance_Marin_Message:I = 0x7f130259

.field public static final TextAppearance_Marin_ModalTitle:I = 0x7f13025a

.field public static final TextAppearance_Marin_ModalTitle_SingleLineByOrientation:I = 0x7f13025b

.field public static final TextAppearance_Marin_Note:I = 0x7f13025c

.field public static final TextAppearance_Marin_Placeholder:I = 0x7f13025d

.field public static final TextAppearance_Marin_Red:I = 0x7f13025e

.field public static final TextAppearance_Marin_SectionHeader:I = 0x7f13025f

.field public static final TextAppearance_Marin_SectionHeader_Light:I = 0x7f130260

.field public static final TextAppearance_Marin_SectionHeader_Medium:I = 0x7f130261

.field public static final TextAppearance_Marin_SignTitle:I = 0x7f130262

.field public static final TextAppearance_Marin_SubText:I = 0x7f130263

.field public static final TextAppearance_Marin_SubText_Centered:I = 0x7f130264

.field public static final TextAppearance_Marin_SubText_Centered_LightGray:I = 0x7f130265

.field public static final TextAppearance_Marin_SubText_Grey:I = 0x7f130266

.field public static final TextAppearance_Marin_SubText_Grey_Landing:I = 0x7f130267

.field public static final TextAppearance_Marin_SubText_Grey_Medium:I = 0x7f130268

.field public static final TextAppearance_Marin_SubText_Grey_Medium_Centered:I = 0x7f130269

.field public static final TextAppearance_Marin_SubText_Grey_WhiteBackground_Padding:I = 0x7f13026a

.field public static final TextAppearance_Marin_SubText_Grey_WhiteBackground_Padding_BottomLine:I = 0x7f13026b

.field public static final TextAppearance_Marin_SubText_NullState:I = 0x7f13026c

.field public static final TextAppearance_Marin_SubText_White:I = 0x7f13026d

.field public static final TextAppearance_Marin_Title1:I = 0x7f13026e

.field public static final TextAppearance_Marin_Title2:I = 0x7f13026f

.field public static final TextAppearance_Marin_TransparentBackground:I = 0x7f130270

.field public static final TextAppearance_Marin_TransparentBackground_Gray:I = 0x7f130271

.field public static final TextAppearance_Marin_TransparentBackground_Gray_Medium:I = 0x7f130272

.field public static final TextAppearance_Marin_TransparentBackground_White:I = 0x7f130273

.field public static final TextAppearance_Marin_TransparentBackground_White_Medium:I = 0x7f130274

.field public static final TextAppearance_Marin_White:I = 0x7f130275

.field public static final TextAppearance_Marin_White_Landing:I = 0x7f130276

.field public static final TextAppearance_Marin_White_LandingWorld:I = 0x7f130277

.field public static final TextAppearance_MaterialComponents_Badge:I = 0x7f130278

.field public static final TextAppearance_MaterialComponents_Body1:I = 0x7f130279

.field public static final TextAppearance_MaterialComponents_Body2:I = 0x7f13027a

.field public static final TextAppearance_MaterialComponents_Button:I = 0x7f13027b

.field public static final TextAppearance_MaterialComponents_Caption:I = 0x7f13027c

.field public static final TextAppearance_MaterialComponents_Chip:I = 0x7f13027d

.field public static final TextAppearance_MaterialComponents_Headline1:I = 0x7f13027e

.field public static final TextAppearance_MaterialComponents_Headline2:I = 0x7f13027f

.field public static final TextAppearance_MaterialComponents_Headline3:I = 0x7f130280

.field public static final TextAppearance_MaterialComponents_Headline4:I = 0x7f130281

.field public static final TextAppearance_MaterialComponents_Headline5:I = 0x7f130282

.field public static final TextAppearance_MaterialComponents_Headline6:I = 0x7f130283

.field public static final TextAppearance_MaterialComponents_Overline:I = 0x7f130284

.field public static final TextAppearance_MaterialComponents_Subtitle1:I = 0x7f130285

.field public static final TextAppearance_MaterialComponents_Subtitle2:I = 0x7f130286

.field public static final TextAppearance_Noho:I = 0x7f130287

.field public static final TextAppearance_Noho_Banner:I = 0x7f130288

.field public static final TextAppearance_Noho_Body:I = 0x7f130289

.field public static final TextAppearance_Noho_Body2:I = 0x7f13028a

.field public static final TextAppearance_Noho_Button:I = 0x7f13028b

.field public static final TextAppearance_Noho_Heading:I = 0x7f13028c

.field public static final TextAppearance_Noho_Heading2:I = 0x7f13028d

.field public static final TextAppearance_Noho_HelpText:I = 0x7f13028e

.field public static final TextAppearance_Noho_HelpTextLink:I = 0x7f13028f

.field public static final TextAppearance_Noho_Hint:I = 0x7f130290

.field public static final TextAppearance_Noho_Input:I = 0x7f130291

.field public static final TextAppearance_Noho_Label:I = 0x7f130292

.field public static final TextAppearance_Noho_Oversized:I = 0x7f130293

.field public static final TextAppearance_Noho_SecondaryLabel:I = 0x7f130294

.field public static final TextAppearance_Noho_Subheader:I = 0x7f130295

.field public static final TextAppearance_Noho_Subheader_AllCaps:I = 0x7f130296

.field public static final TextAppearance_Noho_TopBar:I = 0x7f130297

.field public static final TextAppearance_Noho_TopBarAction:I = 0x7f130298

.field public static final TextAppearance_Passcodes_Label_Link:I = 0x7f130299

.field public static final TextAppearance_PredefinedTicket_Row_Value:I = 0x7f13029a

.field public static final TextAppearance_SecureTouchModeSelection_Text:I = 0x7f13029b

.field public static final TextAppearance_SecureTouchModeSelection_Text_AmountDue:I = 0x7f13029c

.field public static final TextAppearance_Ticket_Row_Label:I = 0x7f13029d

.field public static final TextAppearance_Ticket_Row_SectionHeader_Description:I = 0x7f13029e

.field public static final TextAppearance_Ticket_Row_SectionHeader_Label:I = 0x7f13029f

.field public static final TextAppearance_TransactionDetails_Tender_CashierRow:I = 0x7f1302a0

.field public static final TextAppearance_TransactionDetails_Tender_CustomerRow_Initials:I = 0x7f1302a1

.field public static final TextAppearance_TransactionDetails_Tender_CustomerRow_Name:I = 0x7f1302a2

.field public static final TextAppearance_TransactionsHistory_SearchBar:I = 0x7f1302a3

.field public static final TextAppearance_TransactionsHistory_SmartLine_Subtitle:I = 0x7f1302a4

.field public static final TextAppearance_TransactionsHistory_SmartLine_Title:I = 0x7f1302a5

.field public static final TextAppearance_TransactionsHistory_SmartLine_Value:I = 0x7f1302a6

.field public static final TextAppearance_TransactionsHistory_StickyHeaders:I = 0x7f1302a7

.field public static final TextAppearance_TransactionsHistory_TopMessagePanel_Message:I = 0x7f1302a8

.field public static final TextAppearance_TransactionsHistory_TopMessagePanel_TapDisabledMessage:I = 0x7f1302a9

.field public static final TextAppearance_TransactionsHistory_TopMessagePanel_TapEnabledMessage:I = 0x7f1302aa

.field public static final TextAppearance_TransactionsHistory_TopMessagePanel_Title:I = 0x7f1302ab

.field public static final TextAppearance_Widget_AppCompat_ExpandedMenu_Item:I = 0x7f1302ac

.field public static final TextAppearance_Widget_AppCompat_Toolbar_Subtitle:I = 0x7f1302ad

.field public static final TextAppearance_Widget_AppCompat_Toolbar_Title:I = 0x7f1302ae

.field public static final TextAppearance_Widget_Noho_Edit:I = 0x7f1302af

.field public static final TextAppearance_Widget_Noho_Edit_Label:I = 0x7f1302b0

.field public static final TextAppearance_Widget_Noho_Edit_Note:I = 0x7f1302b1

.field public static final TextAppearance_Widget_Noho_InputBox_Details:I = 0x7f1302b2

.field public static final TextAppearance_Widget_Noho_InputBox_Error:I = 0x7f1302b3

.field public static final TextAppearance_Widget_Noho_Label_AllCapsHeader:I = 0x7f1302b4

.field public static final TextAppearance_Widget_Noho_Label_Base:I = 0x7f1302b5

.field public static final TextAppearance_Widget_Noho_Label_Body:I = 0x7f1302b6

.field public static final TextAppearance_Widget_Noho_Label_Body2:I = 0x7f1302b7

.field public static final TextAppearance_Widget_Noho_Label_Display:I = 0x7f1302b8

.field public static final TextAppearance_Widget_Noho_Label_Display2:I = 0x7f1302b9

.field public static final TextAppearance_Widget_Noho_Label_Heading:I = 0x7f1302ba

.field public static final TextAppearance_Widget_Noho_Label_Heading2:I = 0x7f1302bb

.field public static final TextAppearance_Widget_Noho_Label_Label:I = 0x7f1302bc

.field public static final TextAppearance_Widget_Noho_Label_Label2:I = 0x7f1302bd

.field public static final TextAppearance_Widget_Noho_Label_SubHeader:I = 0x7f1302be

.field public static final TextAppearance_Widget_Noho_Notification:I = 0x7f1302bf

.field public static final TextAppearance_Widget_Noho_Row_ActionLink:I = 0x7f1302c0

.field public static final TextAppearance_Widget_Noho_Row_AllCapsSubHeader_Label:I = 0x7f1302c1

.field public static final TextAppearance_Widget_Noho_Row_AllCapsSubHeader_Value:I = 0x7f1302c2

.field public static final TextAppearance_Widget_Noho_Row_Description:I = 0x7f1302c3

.field public static final TextAppearance_Widget_Noho_Row_Label:I = 0x7f1302c4

.field public static final TextAppearance_Widget_Noho_Row_Label_CategoryDrilldown:I = 0x7f1302c5

.field public static final TextAppearance_Widget_Noho_Row_Label_CombinationCheckableRow:I = 0x7f1302c6

.field public static final TextAppearance_Widget_Noho_Row_Label_EditItemDrilldown:I = 0x7f1302c7

.field public static final TextAppearance_Widget_Noho_Row_SubValue:I = 0x7f1302c8

.field public static final TextAppearance_Widget_Noho_Row_TextIcon:I = 0x7f1302c9

.field public static final TextAppearance_Widget_Noho_Row_Value:I = 0x7f1302ca

.field public static final TextAppearance_Widget_Noho_Row_Value_AvailableVariationRow:I = 0x7f1302cb

.field public static final TextAppearance_Widget_Noho_Row_Value_CategoryDrilldown:I = 0x7f1302cc

.field public static final TextAppearance_Widget_Noho_Row_Value_EditItemDrilldown:I = 0x7f1302cd

.field public static final TextAppearance_Widget_Noho_Row_Value_Inactive:I = 0x7f1302ce

.field public static final TextAppearance_Widget_Noho_Row_Value_OptionValueRow:I = 0x7f1302cf

.field public static final TextAppearance_Widget_Noho_Selectable_Label:I = 0x7f1302d0

.field public static final TextAppearance_Widget_Noho_Selectable_Value:I = 0x7f1302d1

.field public static final ThemeOverlay_AppCompat:I = 0x7f130344

.field public static final ThemeOverlay_AppCompat_ActionBar:I = 0x7f130345

.field public static final ThemeOverlay_AppCompat_Dark:I = 0x7f130346

.field public static final ThemeOverlay_AppCompat_Dark_ActionBar:I = 0x7f130347

.field public static final ThemeOverlay_AppCompat_DayNight:I = 0x7f130348

.field public static final ThemeOverlay_AppCompat_DayNight_ActionBar:I = 0x7f130349

.field public static final ThemeOverlay_AppCompat_Dialog:I = 0x7f13034a

.field public static final ThemeOverlay_AppCompat_Dialog_Alert:I = 0x7f13034b

.field public static final ThemeOverlay_AppCompat_Light:I = 0x7f13034c

.field public static final ThemeOverlay_Design_TextInputEditText:I = 0x7f13034d

.field public static final ThemeOverlay_MaterialComponents:I = 0x7f13034e

.field public static final ThemeOverlay_MaterialComponents_ActionBar:I = 0x7f13034f

.field public static final ThemeOverlay_MaterialComponents_ActionBar_Primary:I = 0x7f130350

.field public static final ThemeOverlay_MaterialComponents_ActionBar_Surface:I = 0x7f130351

.field public static final ThemeOverlay_MaterialComponents_AutoCompleteTextView:I = 0x7f130352

.field public static final ThemeOverlay_MaterialComponents_AutoCompleteTextView_FilledBox:I = 0x7f130353

.field public static final ThemeOverlay_MaterialComponents_AutoCompleteTextView_FilledBox_Dense:I = 0x7f130354

.field public static final ThemeOverlay_MaterialComponents_AutoCompleteTextView_OutlinedBox:I = 0x7f130355

.field public static final ThemeOverlay_MaterialComponents_AutoCompleteTextView_OutlinedBox_Dense:I = 0x7f130356

.field public static final ThemeOverlay_MaterialComponents_BottomAppBar_Primary:I = 0x7f130357

.field public static final ThemeOverlay_MaterialComponents_BottomAppBar_Surface:I = 0x7f130358

.field public static final ThemeOverlay_MaterialComponents_BottomSheetDialog:I = 0x7f130359

.field public static final ThemeOverlay_MaterialComponents_Dark:I = 0x7f13035a

.field public static final ThemeOverlay_MaterialComponents_Dark_ActionBar:I = 0x7f13035b

.field public static final ThemeOverlay_MaterialComponents_DayNight_BottomSheetDialog:I = 0x7f13035c

.field public static final ThemeOverlay_MaterialComponents_Dialog:I = 0x7f13035d

.field public static final ThemeOverlay_MaterialComponents_Dialog_Alert:I = 0x7f13035e

.field public static final ThemeOverlay_MaterialComponents_Light:I = 0x7f13035f

.field public static final ThemeOverlay_MaterialComponents_Light_BottomSheetDialog:I = 0x7f130360

.field public static final ThemeOverlay_MaterialComponents_MaterialAlertDialog:I = 0x7f130361

.field public static final ThemeOverlay_MaterialComponents_MaterialAlertDialog_Centered:I = 0x7f130362

.field public static final ThemeOverlay_MaterialComponents_MaterialAlertDialog_Picker_Date:I = 0x7f130363

.field public static final ThemeOverlay_MaterialComponents_MaterialAlertDialog_Picker_Date_Calendar:I = 0x7f130364

.field public static final ThemeOverlay_MaterialComponents_MaterialAlertDialog_Picker_Date_Header_Text:I = 0x7f130365

.field public static final ThemeOverlay_MaterialComponents_MaterialAlertDialog_Picker_Date_Header_Text_Day:I = 0x7f130366

.field public static final ThemeOverlay_MaterialComponents_MaterialAlertDialog_Picker_Date_Spinner:I = 0x7f130367

.field public static final ThemeOverlay_MaterialComponents_MaterialCalendar:I = 0x7f130368

.field public static final ThemeOverlay_MaterialComponents_MaterialCalendar_Fullscreen:I = 0x7f130369

.field public static final ThemeOverlay_MaterialComponents_TextInputEditText:I = 0x7f13036a

.field public static final ThemeOverlay_MaterialComponents_TextInputEditText_FilledBox:I = 0x7f13036b

.field public static final ThemeOverlay_MaterialComponents_TextInputEditText_FilledBox_Dense:I = 0x7f13036c

.field public static final ThemeOverlay_MaterialComponents_TextInputEditText_OutlinedBox:I = 0x7f13036d

.field public static final ThemeOverlay_MaterialComponents_TextInputEditText_OutlinedBox_Dense:I = 0x7f13036e

.field public static final ThemeOverlay_MaterialComponents_Toolbar_Primary:I = 0x7f13036f

.field public static final ThemeOverlay_MaterialComponents_Toolbar_Surface:I = 0x7f130370

.field public static final Theme_AppCompat:I = 0x7f1302d2

.field public static final Theme_AppCompat_CompactMenu:I = 0x7f1302d3

.field public static final Theme_AppCompat_DayNight:I = 0x7f1302d4

.field public static final Theme_AppCompat_DayNight_DarkActionBar:I = 0x7f1302d5

.field public static final Theme_AppCompat_DayNight_Dialog:I = 0x7f1302d6

.field public static final Theme_AppCompat_DayNight_DialogWhenLarge:I = 0x7f1302d9

.field public static final Theme_AppCompat_DayNight_Dialog_Alert:I = 0x7f1302d7

.field public static final Theme_AppCompat_DayNight_Dialog_MinWidth:I = 0x7f1302d8

.field public static final Theme_AppCompat_DayNight_NoActionBar:I = 0x7f1302da

.field public static final Theme_AppCompat_Dialog:I = 0x7f1302db

.field public static final Theme_AppCompat_DialogWhenLarge:I = 0x7f1302de

.field public static final Theme_AppCompat_Dialog_Alert:I = 0x7f1302dc

.field public static final Theme_AppCompat_Dialog_MinWidth:I = 0x7f1302dd

.field public static final Theme_AppCompat_Light:I = 0x7f1302df

.field public static final Theme_AppCompat_Light_DarkActionBar:I = 0x7f1302e0

.field public static final Theme_AppCompat_Light_Dialog:I = 0x7f1302e1

.field public static final Theme_AppCompat_Light_DialogWhenLarge:I = 0x7f1302e4

.field public static final Theme_AppCompat_Light_Dialog_Alert:I = 0x7f1302e2

.field public static final Theme_AppCompat_Light_Dialog_MinWidth:I = 0x7f1302e3

.field public static final Theme_AppCompat_Light_NoActionBar:I = 0x7f1302e5

.field public static final Theme_AppCompat_NoActionBar:I = 0x7f1302e6

.field public static final Theme_BuyerFacing:I = 0x7f1302e7

.field public static final Theme_BuyerFacing_LargeTitle:I = 0x7f1302e8

.field public static final Theme_BuyerFacing_SmallTitle:I = 0x7f1302e9

.field public static final Theme_Design:I = 0x7f1302ea

.field public static final Theme_Design_BottomSheetDialog:I = 0x7f1302eb

.field public static final Theme_Design_Light:I = 0x7f1302ec

.field public static final Theme_Design_Light_BottomSheetDialog:I = 0x7f1302ed

.field public static final Theme_Design_Light_NoActionBar:I = 0x7f1302ee

.field public static final Theme_Design_NoActionBar:I = 0x7f1302ef

.field public static final Theme_Dialog_Tooltip:I = 0x7f1302f0

.field public static final Theme_Invoices_BigNohoRow:I = 0x7f1302f1

.field public static final Theme_Invoices_SmallNohoRow:I = 0x7f1302f2

.field public static final Theme_Marin:I = 0x7f1302f3

.field public static final Theme_MarinBase:I = 0x7f130308

.field public static final Theme_Marin_ActionBar:I = 0x7f1302f4

.field public static final Theme_Marin_ActionBar_Blue:I = 0x7f1302f5

.field public static final Theme_Marin_ActionBar_BlueSecondaryButton:I = 0x7f1302f6

.field public static final Theme_Marin_ActionBar_Card:I = 0x7f1302f7

.field public static final Theme_Marin_ActionBar_Card_BlueSecondaryButton:I = 0x7f1302f8

.field public static final Theme_Marin_ActionBar_EditItem:I = 0x7f1302f9

.field public static final Theme_Marin_ActionBar_Green:I = 0x7f1302fa

.field public static final Theme_Marin_ActionBar_NoBackground:I = 0x7f1302fb

.field public static final Theme_Marin_ActionBar_NoBackground_WhiteText_DarkTranslucentSelector:I = 0x7f1302fc

.field public static final Theme_Marin_ActionBar_NoBottomBorder:I = 0x7f1302fd

.field public static final Theme_Marin_ActionBar_UltraLightGray:I = 0x7f1302fe

.field public static final Theme_Marin_ActionBar_UltraLightGray_BlueSecondaryButton:I = 0x7f1302ff

.field public static final Theme_Marin_ActionBar_UltraLightGray_NoBottomBorder:I = 0x7f130300

.field public static final Theme_Marin_ActionBar_White:I = 0x7f130301

.field public static final Theme_Marin_Inverted:I = 0x7f130302

.field public static final Theme_Marin_NoAnimation:I = 0x7f130303

.field public static final Theme_Marin_NoBackground:I = 0x7f130304

.field public static final Theme_Marin_X2:I = 0x7f130305

.field public static final Theme_Marin_X2Dialog:I = 0x7f130307

.field public static final Theme_Marin_X2_Dark:I = 0x7f130306

.field public static final Theme_MaterialComponents:I = 0x7f130309

.field public static final Theme_MaterialComponents_BottomSheetDialog:I = 0x7f13030a

.field public static final Theme_MaterialComponents_Bridge:I = 0x7f13030b

.field public static final Theme_MaterialComponents_CompactMenu:I = 0x7f13030c

.field public static final Theme_MaterialComponents_DayNight:I = 0x7f13030d

.field public static final Theme_MaterialComponents_DayNight_BottomSheetDialog:I = 0x7f13030e

.field public static final Theme_MaterialComponents_DayNight_Bridge:I = 0x7f13030f

.field public static final Theme_MaterialComponents_DayNight_DarkActionBar:I = 0x7f130310

.field public static final Theme_MaterialComponents_DayNight_DarkActionBar_Bridge:I = 0x7f130311

.field public static final Theme_MaterialComponents_DayNight_Dialog:I = 0x7f130312

.field public static final Theme_MaterialComponents_DayNight_DialogWhenLarge:I = 0x7f13031a

.field public static final Theme_MaterialComponents_DayNight_Dialog_Alert:I = 0x7f130313

.field public static final Theme_MaterialComponents_DayNight_Dialog_Alert_Bridge:I = 0x7f130314

.field public static final Theme_MaterialComponents_DayNight_Dialog_Bridge:I = 0x7f130315

.field public static final Theme_MaterialComponents_DayNight_Dialog_FixedSize:I = 0x7f130316

.field public static final Theme_MaterialComponents_DayNight_Dialog_FixedSize_Bridge:I = 0x7f130317

.field public static final Theme_MaterialComponents_DayNight_Dialog_MinWidth:I = 0x7f130318

.field public static final Theme_MaterialComponents_DayNight_Dialog_MinWidth_Bridge:I = 0x7f130319

.field public static final Theme_MaterialComponents_DayNight_NoActionBar:I = 0x7f13031b

.field public static final Theme_MaterialComponents_DayNight_NoActionBar_Bridge:I = 0x7f13031c

.field public static final Theme_MaterialComponents_Dialog:I = 0x7f13031d

.field public static final Theme_MaterialComponents_DialogWhenLarge:I = 0x7f130325

.field public static final Theme_MaterialComponents_Dialog_Alert:I = 0x7f13031e

.field public static final Theme_MaterialComponents_Dialog_Alert_Bridge:I = 0x7f13031f

.field public static final Theme_MaterialComponents_Dialog_Bridge:I = 0x7f130320

.field public static final Theme_MaterialComponents_Dialog_FixedSize:I = 0x7f130321

.field public static final Theme_MaterialComponents_Dialog_FixedSize_Bridge:I = 0x7f130322

.field public static final Theme_MaterialComponents_Dialog_MinWidth:I = 0x7f130323

.field public static final Theme_MaterialComponents_Dialog_MinWidth_Bridge:I = 0x7f130324

.field public static final Theme_MaterialComponents_Light:I = 0x7f130326

.field public static final Theme_MaterialComponents_Light_BarSize:I = 0x7f130327

.field public static final Theme_MaterialComponents_Light_BottomSheetDialog:I = 0x7f130328

.field public static final Theme_MaterialComponents_Light_Bridge:I = 0x7f130329

.field public static final Theme_MaterialComponents_Light_DarkActionBar:I = 0x7f13032a

.field public static final Theme_MaterialComponents_Light_DarkActionBar_Bridge:I = 0x7f13032b

.field public static final Theme_MaterialComponents_Light_Dialog:I = 0x7f13032c

.field public static final Theme_MaterialComponents_Light_DialogWhenLarge:I = 0x7f130334

.field public static final Theme_MaterialComponents_Light_Dialog_Alert:I = 0x7f13032d

.field public static final Theme_MaterialComponents_Light_Dialog_Alert_Bridge:I = 0x7f13032e

.field public static final Theme_MaterialComponents_Light_Dialog_Bridge:I = 0x7f13032f

.field public static final Theme_MaterialComponents_Light_Dialog_FixedSize:I = 0x7f130330

.field public static final Theme_MaterialComponents_Light_Dialog_FixedSize_Bridge:I = 0x7f130331

.field public static final Theme_MaterialComponents_Light_Dialog_MinWidth:I = 0x7f130332

.field public static final Theme_MaterialComponents_Light_Dialog_MinWidth_Bridge:I = 0x7f130333

.field public static final Theme_MaterialComponents_Light_LargeTouch:I = 0x7f130335

.field public static final Theme_MaterialComponents_Light_NoActionBar:I = 0x7f130336

.field public static final Theme_MaterialComponents_Light_NoActionBar_Bridge:I = 0x7f130337

.field public static final Theme_MaterialComponents_NoActionBar:I = 0x7f130338

.field public static final Theme_MaterialComponents_NoActionBar_Bridge:I = 0x7f130339

.field public static final Theme_Noho:I = 0x7f13033a

.field public static final Theme_Noho_DatePicker_X2:I = 0x7f13033b

.field public static final Theme_Noho_Dialog:I = 0x7f13033c

.field public static final Theme_Noho_DialogBase:I = 0x7f13033f

.field public static final Theme_Noho_Dialog_NoBackground:I = 0x7f13033d

.field public static final Theme_Noho_Dialog_NoBackground_NoDim:I = 0x7f13033e

.field public static final Theme_Register_Root:I = 0x7f130340

.field public static final Theme_Register_Root_Api:I = 0x7f130341

.field public static final Theme_SwitcherTheme:I = 0x7f130342

.field public static final Theme_SwitcherTheme_StockAndroid:I = 0x7f130343

.field public static final Title:I = 0x7f130371

.field public static final Tooltip_Dialog_Animation:I = 0x7f130372

.field public static final TransactionDescriptionItalic:I = 0x7f130373

.field public static final TransactionDescriptionNormal:I = 0x7f130374

.field public static final TutorialBar:I = 0x7f130375

.field public static final TutorialBar2:I = 0x7f130376

.field public static final TutorialTooltip:I = 0x7f130377

.field public static final TutorialTooltipArrow:I = 0x7f130378

.field public static final WarningBannerRowTitleAlertAppearance:I = 0x7f130379

.field public static final WarningBannerRowTitleAppearance:I = 0x7f13037a

.field public static final Widget_AppCompat_ActionBar:I = 0x7f13037b

.field public static final Widget_AppCompat_ActionBar_Solid:I = 0x7f13037c

.field public static final Widget_AppCompat_ActionBar_TabBar:I = 0x7f13037d

.field public static final Widget_AppCompat_ActionBar_TabText:I = 0x7f13037e

.field public static final Widget_AppCompat_ActionBar_TabView:I = 0x7f13037f

.field public static final Widget_AppCompat_ActionButton:I = 0x7f130380

.field public static final Widget_AppCompat_ActionButton_CloseMode:I = 0x7f130381

.field public static final Widget_AppCompat_ActionButton_Overflow:I = 0x7f130382

.field public static final Widget_AppCompat_ActionMode:I = 0x7f130383

.field public static final Widget_AppCompat_ActivityChooserView:I = 0x7f130384

.field public static final Widget_AppCompat_AutoCompleteTextView:I = 0x7f130385

.field public static final Widget_AppCompat_Button:I = 0x7f130386

.field public static final Widget_AppCompat_ButtonBar:I = 0x7f13038c

.field public static final Widget_AppCompat_ButtonBar_AlertDialog:I = 0x7f13038d

.field public static final Widget_AppCompat_Button_Borderless:I = 0x7f130387

.field public static final Widget_AppCompat_Button_Borderless_Colored:I = 0x7f130388

.field public static final Widget_AppCompat_Button_ButtonBar_AlertDialog:I = 0x7f130389

.field public static final Widget_AppCompat_Button_Colored:I = 0x7f13038a

.field public static final Widget_AppCompat_Button_Small:I = 0x7f13038b

.field public static final Widget_AppCompat_CompoundButton_CheckBox:I = 0x7f13038e

.field public static final Widget_AppCompat_CompoundButton_RadioButton:I = 0x7f13038f

.field public static final Widget_AppCompat_CompoundButton_Switch:I = 0x7f130390

.field public static final Widget_AppCompat_DrawerArrowToggle:I = 0x7f130391

.field public static final Widget_AppCompat_DropDownItem_Spinner:I = 0x7f130392

.field public static final Widget_AppCompat_EditText:I = 0x7f130393

.field public static final Widget_AppCompat_ImageButton:I = 0x7f130394

.field public static final Widget_AppCompat_Light_ActionBar:I = 0x7f130395

.field public static final Widget_AppCompat_Light_ActionBar_Solid:I = 0x7f130396

.field public static final Widget_AppCompat_Light_ActionBar_Solid_Inverse:I = 0x7f130397

.field public static final Widget_AppCompat_Light_ActionBar_TabBar:I = 0x7f130398

.field public static final Widget_AppCompat_Light_ActionBar_TabBar_Inverse:I = 0x7f130399

.field public static final Widget_AppCompat_Light_ActionBar_TabText:I = 0x7f13039a

.field public static final Widget_AppCompat_Light_ActionBar_TabText_Inverse:I = 0x7f13039b

.field public static final Widget_AppCompat_Light_ActionBar_TabView:I = 0x7f13039c

.field public static final Widget_AppCompat_Light_ActionBar_TabView_Inverse:I = 0x7f13039d

.field public static final Widget_AppCompat_Light_ActionButton:I = 0x7f13039e

.field public static final Widget_AppCompat_Light_ActionButton_CloseMode:I = 0x7f13039f

.field public static final Widget_AppCompat_Light_ActionButton_Overflow:I = 0x7f1303a0

.field public static final Widget_AppCompat_Light_ActionMode_Inverse:I = 0x7f1303a1

.field public static final Widget_AppCompat_Light_ActivityChooserView:I = 0x7f1303a2

.field public static final Widget_AppCompat_Light_AutoCompleteTextView:I = 0x7f1303a3

.field public static final Widget_AppCompat_Light_DropDownItem_Spinner:I = 0x7f1303a4

.field public static final Widget_AppCompat_Light_ListPopupWindow:I = 0x7f1303a5

.field public static final Widget_AppCompat_Light_ListView_DropDown:I = 0x7f1303a6

.field public static final Widget_AppCompat_Light_PopupMenu:I = 0x7f1303a7

.field public static final Widget_AppCompat_Light_PopupMenu_Overflow:I = 0x7f1303a8

.field public static final Widget_AppCompat_Light_SearchView:I = 0x7f1303a9

.field public static final Widget_AppCompat_Light_Spinner_DropDown_ActionBar:I = 0x7f1303aa

.field public static final Widget_AppCompat_ListMenuView:I = 0x7f1303ab

.field public static final Widget_AppCompat_ListPopupWindow:I = 0x7f1303ac

.field public static final Widget_AppCompat_ListView:I = 0x7f1303ad

.field public static final Widget_AppCompat_ListView_DropDown:I = 0x7f1303ae

.field public static final Widget_AppCompat_ListView_Menu:I = 0x7f1303af

.field public static final Widget_AppCompat_PopupMenu:I = 0x7f1303b0

.field public static final Widget_AppCompat_PopupMenu_Overflow:I = 0x7f1303b1

.field public static final Widget_AppCompat_PopupWindow:I = 0x7f1303b2

.field public static final Widget_AppCompat_ProgressBar:I = 0x7f1303b3

.field public static final Widget_AppCompat_ProgressBar_Horizontal:I = 0x7f1303b4

.field public static final Widget_AppCompat_RatingBar:I = 0x7f1303b5

.field public static final Widget_AppCompat_RatingBar_Indicator:I = 0x7f1303b6

.field public static final Widget_AppCompat_RatingBar_Small:I = 0x7f1303b7

.field public static final Widget_AppCompat_SearchView:I = 0x7f1303b8

.field public static final Widget_AppCompat_SearchView_ActionBar:I = 0x7f1303b9

.field public static final Widget_AppCompat_SeekBar:I = 0x7f1303ba

.field public static final Widget_AppCompat_SeekBar_Discrete:I = 0x7f1303bb

.field public static final Widget_AppCompat_Spinner:I = 0x7f1303bc

.field public static final Widget_AppCompat_Spinner_DropDown:I = 0x7f1303bd

.field public static final Widget_AppCompat_Spinner_DropDown_ActionBar:I = 0x7f1303be

.field public static final Widget_AppCompat_Spinner_Underlined:I = 0x7f1303bf

.field public static final Widget_AppCompat_TextView:I = 0x7f1303c0

.field public static final Widget_AppCompat_TextView_SpinnerItem:I = 0x7f1303c1

.field public static final Widget_AppCompat_Toolbar:I = 0x7f1303c2

.field public static final Widget_AppCompat_Toolbar_Button_Navigation:I = 0x7f1303c3

.field public static final Widget_Balance_SubHeader:I = 0x7f1303c4

.field public static final Widget_BottomSheetIndicator:I = 0x7f1303c5

.field public static final Widget_BuyerFacing_PrimaryTileButton:I = 0x7f1303c6

.field public static final Widget_BuyerFacing_PrimaryTileButton_LargeTitle:I = 0x7f1303c7

.field public static final Widget_BuyerFacing_PrimaryTileButton_SmallTitle:I = 0x7f1303c8

.field public static final Widget_BuyerFacing_SecondaryBarButton:I = 0x7f1303c9

.field public static final Widget_CardEditor:I = 0x7f1303ca

.field public static final Widget_Charge:I = 0x7f1303cb

.field public static final Widget_Compat_NotificationActionContainer:I = 0x7f1303cc

.field public static final Widget_Compat_NotificationActionText:I = 0x7f1303cd

.field public static final Widget_ConfigureItemCheckableGroup:I = 0x7f1303cf

.field public static final Widget_ConfigureItem_EditTextWithSuffix:I = 0x7f1303ce

.field public static final Widget_Crm_DropDownItem:I = 0x7f1303d0

.field public static final Widget_Crm_DropDownRootContainer:I = 0x7f1303d1

.field public static final Widget_Crm_ViewProfileSection:I = 0x7f1303d2

.field public static final Widget_DepositSpeedIcon:I = 0x7f1303d3

.field public static final Widget_DepositSpeedLayout:I = 0x7f1303d4

.field public static final Widget_Design_AppBarLayout:I = 0x7f1303d5

.field public static final Widget_Design_BottomNavigationView:I = 0x7f1303d6

.field public static final Widget_Design_BottomSheet_Modal:I = 0x7f1303d7

.field public static final Widget_Design_CollapsingToolbar:I = 0x7f1303d8

.field public static final Widget_Design_FloatingActionButton:I = 0x7f1303d9

.field public static final Widget_Design_NavigationView:I = 0x7f1303da

.field public static final Widget_Design_ScrimInsetsFrameLayout:I = 0x7f1303db

.field public static final Widget_Design_Snackbar:I = 0x7f1303dc

.field public static final Widget_Design_TabLayout:I = 0x7f1303dd

.field public static final Widget_Design_TextInputLayout:I = 0x7f1303de

.field public static final Widget_DiscountPriceEditor:I = 0x7f1303df

.field public static final Widget_EditItemItemOptionAndValueRow:I = 0x7f1303e0

.field public static final Widget_ErrorsBar:I = 0x7f1303e1

.field public static final Widget_HorizontalCheckableBar:I = 0x7f1303e2

.field public static final Widget_HorizontalRadioButton:I = 0x7f1303e3

.field public static final Widget_HorizontalRadioDarkButton:I = 0x7f1303e4

.field public static final Widget_IntentHowIcon:I = 0x7f1303e5

.field public static final Widget_IntentHowLayout:I = 0x7f1303e6

.field public static final Widget_IntentHowLayout_Responsive:I = 0x7f1303e7

.field public static final Widget_InvoiceReminder_Row_Icon:I = 0x7f1303e8

.field public static final Widget_Invoices_DatePicker:I = 0x7f1303e9

.field public static final Widget_Invoices_DatePicker_Title:I = 0x7f1303ea

.field public static final Widget_Invoices_Noho_AmountField:I = 0x7f1303eb

.field public static final Widget_Invoices_V2_BigRow:I = 0x7f1303ec

.field public static final Widget_Invoices_V2_Row_Icon:I = 0x7f1303ed

.field public static final Widget_Invoices_V2_SmallRow:I = 0x7f1303ee

.field public static final Widget_LandingButton:I = 0x7f1303ef

.field public static final Widget_LandingButton_BlueBackground:I = 0x7f1303f0

.field public static final Widget_LandingButton_BlueBackground_Darker:I = 0x7f1303f1

.field public static final Widget_LandingButton_Bordered:I = 0x7f1303f2

.field public static final Widget_LandingLogoText:I = 0x7f1303f3

.field public static final Widget_LandingLogoText_World:I = 0x7f1303f4

.field public static final Widget_Marin_ActionBar:I = 0x7f1303f5

.field public static final Widget_Marin_ActionBar_Green:I = 0x7f1303f6

.field public static final Widget_Marin_ActionBar_Navigation:I = 0x7f1303f7

.field public static final Widget_Marin_ActionBar_Navigation_Edit:I = 0x7f1303f8

.field public static final Widget_Marin_ActionBar_NoBackground:I = 0x7f1303f9

.field public static final Widget_Marin_ActionBar_NoBackground_WhiteText:I = 0x7f1303fa

.field public static final Widget_Marin_ActionBar_NoBorder:I = 0x7f1303fb

.field public static final Widget_Marin_ActionBar_UltraLightGray:I = 0x7f1303fc

.field public static final Widget_Marin_ActionBar_UltraLightGray_NoBorder:I = 0x7f1303fd

.field public static final Widget_Marin_ActionBar_White:I = 0x7f1303fe

.field public static final Widget_Marin_Applet_Header:I = 0x7f1303ff

.field public static final Widget_Marin_Applet_Header_List:I = 0x7f130400

.field public static final Widget_Marin_Applet_Header_Sidebar:I = 0x7f130401

.field public static final Widget_Marin_AutoPaddingTextView:I = 0x7f130402

.field public static final Widget_Marin_Button:I = 0x7f130403

.field public static final Widget_Marin_Button_ActionBar:I = 0x7f130404

.field public static final Widget_Marin_Button_ActionBar_BlueBackground:I = 0x7f130405

.field public static final Widget_Marin_Button_ActionBar_BlueText:I = 0x7f130406

.field public static final Widget_Marin_Button_ActionBar_BottomBorder:I = 0x7f130407

.field public static final Widget_Marin_Button_ActionBar_BottomBorder_BlueText:I = 0x7f130408

.field public static final Widget_Marin_Button_ActionBar_BottomBorder_UpButton:I = 0x7f130409

.field public static final Widget_Marin_Button_ActionBar_BottomBorder_UpButton_BlueText:I = 0x7f13040a

.field public static final Widget_Marin_Button_ActionBar_BottomBorder_UpButton_WhiteText:I = 0x7f13040b

.field public static final Widget_Marin_Button_ActionBar_BottomBorder_WhiteText:I = 0x7f13040c

.field public static final Widget_Marin_Button_ActionBar_BottomBorder_WhiteText_GreenBackground:I = 0x7f13040d

.field public static final Widget_Marin_Button_ActionBar_BottomBorder_WhiteText_GreenBackground_UpButton:I = 0x7f13040e

.field public static final Widget_Marin_Button_ActionBar_Navigation:I = 0x7f13040f

.field public static final Widget_Marin_Button_ActionBar_Navigation_Burger:I = 0x7f130410

.field public static final Widget_Marin_Button_ActionBar_Navigation_Burger_Wide:I = 0x7f130411

.field public static final Widget_Marin_Button_ActionBar_Navigation_Edit:I = 0x7f130412

.field public static final Widget_Marin_Button_ActionBar_NoBorder:I = 0x7f130413

.field public static final Widget_Marin_Button_ActionBar_UltraLightGrayBackground:I = 0x7f130414

.field public static final Widget_Marin_Button_ActionBar_UltraLightGrayBackground_BlueText:I = 0x7f130415

.field public static final Widget_Marin_Button_ActionBar_UltraLightGrayBackground_NoBorder:I = 0x7f130416

.field public static final Widget_Marin_Button_ActionBar_UpButton:I = 0x7f130417

.field public static final Widget_Marin_Button_ActionBar_UpButton_BlueText:I = 0x7f130418

.field public static final Widget_Marin_Button_ActionBar_UpButton_UltraLightGrayBackground:I = 0x7f130419

.field public static final Widget_Marin_Button_ActionBar_UpButton_UltraLightGrayBackground_NoBorder:I = 0x7f13041a

.field public static final Widget_Marin_Button_ActionBar_UpButton_WhiteText:I = 0x7f13041b

.field public static final Widget_Marin_Button_ActionBar_WhiteText:I = 0x7f13041c

.field public static final Widget_Marin_Button_ActionBar_WhiteText_BlackTransparentFity:I = 0x7f13041d

.field public static final Widget_Marin_Button_BlackTransparentFifty:I = 0x7f13041e

.field public static final Widget_Marin_Button_BlueBackground:I = 0x7f13041f

.field public static final Widget_Marin_Button_BlueBackground_Inline:I = 0x7f130420

.field public static final Widget_Marin_Button_BlueText:I = 0x7f130421

.field public static final Widget_Marin_Button_DarkTranslucent:I = 0x7f130422

.field public static final Widget_Marin_Button_Dialog:I = 0x7f130423

.field public static final Widget_Marin_Button_EditActionBar:I = 0x7f130424

.field public static final Widget_Marin_Button_EditText:I = 0x7f130425

.field public static final Widget_Marin_Button_InvertedGrayOnDisabled:I = 0x7f130426

.field public static final Widget_Marin_Button_Link:I = 0x7f130427

.field public static final Widget_Marin_Button_MediumGrayBackground:I = 0x7f130428

.field public static final Widget_Marin_Button_NoBackground:I = 0x7f130429

.field public static final Widget_Marin_Button_Primary:I = 0x7f13042a

.field public static final Widget_Marin_Button_Primary_Blue:I = 0x7f13042b

.field public static final Widget_Marin_Button_Primary_UltraLightGray:I = 0x7f13042c

.field public static final Widget_Marin_Button_RedBackground:I = 0x7f13042d

.field public static final Widget_Marin_Button_Tab:I = 0x7f13042e

.field public static final Widget_Marin_Button_Tab_ThreeColumn:I = 0x7f13042f

.field public static final Widget_Marin_Button_Tab_WithDrawableAtop:I = 0x7f130430

.field public static final Widget_Marin_Button_TutorialBar:I = 0x7f130431

.field public static final Widget_Marin_Button_TutorialClose:I = 0x7f130432

.field public static final Widget_Marin_Button_WeightRegular:I = 0x7f130433

.field public static final Widget_Marin_Button_White:I = 0x7f130434

.field public static final Widget_Marin_Button_White_BlueText:I = 0x7f130435

.field public static final Widget_Marin_Button_White_WeightRegular:I = 0x7f130436

.field public static final Widget_Marin_Charge:I = 0x7f130437

.field public static final Widget_Marin_Charge_Subtitle:I = 0x7f130438

.field public static final Widget_Marin_Charge_Title:I = 0x7f130439

.field public static final Widget_Marin_Charge_Title_NoTickets:I = 0x7f13043a

.field public static final Widget_Marin_CheckableGroup:I = 0x7f13043b

.field public static final Widget_Marin_CheckableGroup_Margin_GutterHalf:I = 0x7f13043c

.field public static final Widget_Marin_CheckableGroup_ThickBlankDivider:I = 0x7f13043d

.field public static final Widget_Marin_CheckableGroup_TwoColumnsThickDivider:I = 0x7f13043e

.field public static final Widget_Marin_CheckableTextView:I = 0x7f13043f

.field public static final Widget_Marin_ConfirmButton:I = 0x7f130440

.field public static final Widget_Marin_DatePicker:I = 0x7f130441

.field public static final Widget_Marin_Dialog_Message:I = 0x7f130442

.field public static final Widget_Marin_Dialog_Title:I = 0x7f130443

.field public static final Widget_Marin_Divider:I = 0x7f130444

.field public static final Widget_Marin_Divider_NoMargins:I = 0x7f130445

.field public static final Widget_Marin_Drawer_Row:I = 0x7f130446

.field public static final Widget_Marin_Drill:I = 0x7f130447

.field public static final Widget_Marin_DropDownMenu:I = 0x7f130448

.field public static final Widget_Marin_DropDownMenu_Container:I = 0x7f130449

.field public static final Widget_Marin_DropDownMenu_Container_Cart:I = 0x7f13044a

.field public static final Widget_Marin_DropDownMenu_Item:I = 0x7f13044b

.field public static final Widget_Marin_DropDownMenu_Item_Cart:I = 0x7f13044c

.field public static final Widget_Marin_DropDownMenu_Item_SplitTicket:I = 0x7f13044d

.field public static final Widget_Marin_EditText:I = 0x7f13044e

.field public static final Widget_Marin_EditText_TextBoxBorder:I = 0x7f13044f

.field public static final Widget_Marin_EditText_WhiteText:I = 0x7f130450

.field public static final Widget_Marin_EditText_X2:I = 0x7f130451

.field public static final Widget_Marin_EditText_X2_Dark:I = 0x7f130452

.field public static final Widget_Marin_EditText_X2_TextBox:I = 0x7f130453

.field public static final Widget_Marin_EditText_X2_TextBox_Dark:I = 0x7f130454

.field public static final Widget_Marin_Element:I = 0x7f130455

.field public static final Widget_Marin_Element_Clickable:I = 0x7f130456

.field public static final Widget_Marin_Element_HelpLine:I = 0x7f130457

.field public static final Widget_Marin_Element_HelpLine_Clickable:I = 0x7f130458

.field public static final Widget_Marin_Element_HelpLine_Clickable_Link:I = 0x7f130459

.field public static final Widget_Marin_Element_HelpText:I = 0x7f13045a

.field public static final Widget_Marin_Element_HelpText_BottomItem:I = 0x7f13045b

.field public static final Widget_Marin_Element_HelpText_Margin_GutterFull:I = 0x7f13045c

.field public static final Widget_Marin_Element_HelpText_Margin_GutterFull_BottomItem:I = 0x7f13045d

.field public static final Widget_Marin_Element_HelpText_Margin_GutterFull_NoBottomMargin:I = 0x7f13045e

.field public static final Widget_Marin_Element_HelpText_Margin_GutterFull_NoMargin:I = 0x7f13045f

.field public static final Widget_Marin_Element_HelpText_Margin_GutterHalf:I = 0x7f130460

.field public static final Widget_Marin_Element_HelpText_Margin_GutterHalf_BottomItem:I = 0x7f130461

.field public static final Widget_Marin_Element_HelpText_Margin_GutterHalf_NoBottomMargin:I = 0x7f130462

.field public static final Widget_Marin_Element_HelpText_MiniBottomMargin:I = 0x7f130463

.field public static final Widget_Marin_Element_HelpText_NoBottomMargin:I = 0x7f130464

.field public static final Widget_Marin_Element_HelpText_NoBottomMargin_MiniTopMargin:I = 0x7f130465

.field public static final Widget_Marin_Element_HelpText_NoMargin:I = 0x7f130466

.field public static final Widget_Marin_Element_HelpText_Padded_GutterHalf:I = 0x7f130467

.field public static final Widget_Marin_Element_HelpText_Padded_GutterHalf_BottomItem:I = 0x7f130468

.field public static final Widget_Marin_Element_Margin_GutterFull:I = 0x7f130469

.field public static final Widget_Marin_Element_Margin_GutterFull_BottomItem:I = 0x7f13046a

.field public static final Widget_Marin_Element_Margin_GutterFull_Clickable:I = 0x7f13046b

.field public static final Widget_Marin_Element_Margin_GutterHalf:I = 0x7f13046c

.field public static final Widget_Marin_Element_Margin_GutterHalf_BottomButton:I = 0x7f13046d

.field public static final Widget_Marin_Element_Margin_GutterHalf_Clickable:I = 0x7f13046e

.field public static final Widget_Marin_Element_Margin_GutterHalf_EditText:I = 0x7f13046f

.field public static final Widget_Marin_Element_Message:I = 0x7f130470

.field public static final Widget_Marin_Element_Message_GutterFull:I = 0x7f130471

.field public static final Widget_Marin_Element_Message_GutterFull_MultilinePadded:I = 0x7f130472

.field public static final Widget_Marin_Element_Padded_GutterFull:I = 0x7f130473

.field public static final Widget_Marin_Element_Padded_GutterFull_Clickable:I = 0x7f130474

.field public static final Widget_Marin_Element_Padded_GutterHalf:I = 0x7f130475

.field public static final Widget_Marin_Element_Padded_GutterHalf_Clickable:I = 0x7f130476

.field public static final Widget_Marin_Element_Padded_GutterHalf_Clickable_Lollipop:I = 0x7f130477

.field public static final Widget_Marin_Element_Padded_GutterHalf_Clickable_UltraLightGray:I = 0x7f130478

.field public static final Widget_Marin_Element_Padded_GutterHalf_Margin_GutterHalf:I = 0x7f130479

.field public static final Widget_Marin_Element_Padded_GutterHalf_Margin_GutterHalf_Clickable:I = 0x7f13047a

.field public static final Widget_Marin_Element_Padded_GutterHalf_Margin_GutterHalf_Clickable_Lollipop:I = 0x7f13047b

.field public static final Widget_Marin_Element_Padded_Medium_Framed_Selectable:I = 0x7f13047c

.field public static final Widget_Marin_Element_Padded_Multiline:I = 0x7f13047d

.field public static final Widget_Marin_Element_Padded_Multiline_Margin_GutterFull:I = 0x7f13047e

.field public static final Widget_Marin_Element_Padded_Multiline_Margin_GutterHalf:I = 0x7f13047f

.field public static final Widget_Marin_Element_WarningText:I = 0x7f130480

.field public static final Widget_Marin_GlyphTextView:I = 0x7f130481

.field public static final Widget_Marin_GlyphView:I = 0x7f130482

.field public static final Widget_Marin_GlyphView_Button:I = 0x7f130483

.field public static final Widget_Marin_GlyphView_Button_ActionBar:I = 0x7f130484

.field public static final Widget_Marin_GlyphView_Button_ActionBar_Green:I = 0x7f130485

.field public static final Widget_Marin_GlyphView_Button_ActionBar_UltraLightGrayBackground:I = 0x7f130486

.field public static final Widget_Marin_GlyphView_Button_Inline:I = 0x7f130487

.field public static final Widget_Marin_GlyphView_Button_Inline_BlueBackground:I = 0x7f130488

.field public static final Widget_Marin_GlyphView_Button_Inset_Blue:I = 0x7f130489

.field public static final Widget_Marin_GlyphView_Button_RedGlyph:I = 0x7f13048a

.field public static final Widget_Marin_GlyphView_DarkGrayText:I = 0x7f13048b

.field public static final Widget_Marin_GlyphView_VisibleByOrientation:I = 0x7f13048c

.field public static final Widget_Marin_HorizontalDividers:I = 0x7f13048d

.field public static final Widget_Marin_ListItem:I = 0x7f13048e

.field public static final Widget_Marin_ListItem_Applet_List:I = 0x7f13048f

.field public static final Widget_Marin_ListItem_Applet_List_GutterFull:I = 0x7f130490

.field public static final Widget_Marin_ListItem_Applet_Sidebar:I = 0x7f130491

.field public static final Widget_Marin_ListItem_Applet_Sidebar_BlueText:I = 0x7f130492

.field public static final Widget_Marin_ListItem_Cart:I = 0x7f130493

.field public static final Widget_Marin_ListItem_Cart_PortaitTablet:I = 0x7f130494

.field public static final Widget_Marin_ListItem_Checked:I = 0x7f130495

.field public static final Widget_Marin_ListItem_Checked_GutterFull:I = 0x7f130496

.field public static final Widget_Marin_ListItem_Checked_Multiple:I = 0x7f130497

.field public static final Widget_Marin_ListItem_Checked_NoPadding:I = 0x7f130498

.field public static final Widget_Marin_ListItem_Checked_UltraLightGray:I = 0x7f130499

.field public static final Widget_Marin_ListItem_Checked_WhiteBackground_TopLine:I = 0x7f13049a

.field public static final Widget_Marin_ListItem_HelpText:I = 0x7f13049b

.field public static final Widget_Marin_ListItem_HelpText_BottomItem_Clickable:I = 0x7f13049c

.field public static final Widget_Marin_ListItem_HelpText_Clickable:I = 0x7f13049d

.field public static final Widget_Marin_ListItem_HelpText_Padded:I = 0x7f13049e

.field public static final Widget_Marin_ListItem_Margin_GutterHalf:I = 0x7f13049f

.field public static final Widget_Marin_ListItem_MediumWeight:I = 0x7f1304a0

.field public static final Widget_Marin_ListItem_NoLeftPadding:I = 0x7f1304a1

.field public static final Widget_Marin_ListItem_NoPadding:I = 0x7f1304a2

.field public static final Widget_Marin_ListItem_Padding_GutterFull:I = 0x7f1304a3

.field public static final Widget_Marin_ListItem_SplitTicket:I = 0x7f1304a4

.field public static final Widget_Marin_ListItem_Thumbnail:I = 0x7f1304a5

.field public static final Widget_Marin_ListView:I = 0x7f1304a6

.field public static final Widget_Marin_ListViewBase:I = 0x7f1304b0

.field public static final Widget_Marin_ListView_GutterFull:I = 0x7f1304a7

.field public static final Widget_Marin_ListView_GutterFull_RightGutterHalf:I = 0x7f1304a8

.field public static final Widget_Marin_ListView_GutterHalf:I = 0x7f1304a9

.field public static final Widget_Marin_ListView_GutterHalf_NoTopPadding:I = 0x7f1304aa

.field public static final Widget_Marin_ListView_NoPadding:I = 0x7f1304ab

.field public static final Widget_Marin_ListView_NoPadding_NoTopPadding:I = 0x7f1304ac

.field public static final Widget_Marin_ListView_NoPadding_SelectorOnTop:I = 0x7f1304ad

.field public static final Widget_Marin_ListView_NoPadding_X2:I = 0x7f1304ae

.field public static final Widget_Marin_ListView_NoPadding_X2_LightGray:I = 0x7f1304af

.field public static final Widget_Marin_OrderTicketBar:I = 0x7f1304b1

.field public static final Widget_Marin_PreservedLayoutView:I = 0x7f1304b2

.field public static final Widget_Marin_ProgressBar:I = 0x7f1304b3

.field public static final Widget_Marin_ProgressBar_Dark:I = 0x7f1304b4

.field public static final Widget_Marin_ProgressBar_Dark_Medium:I = 0x7f1304b5

.field public static final Widget_Marin_ProgressBar_Large:I = 0x7f1304b6

.field public static final Widget_Marin_ProgressBar_Large_Dark:I = 0x7f1304b7

.field public static final Widget_Marin_ProgressBar_Large_Light:I = 0x7f1304b8

.field public static final Widget_Marin_ProgressBar_Small:I = 0x7f1304b9

.field public static final Widget_Marin_ProgressBar_Small_Dark:I = 0x7f1304ba

.field public static final Widget_Marin_ProgressBar_Small_Light:I = 0x7f1304bb

.field public static final Widget_Marin_ProgressBar_Tiny:I = 0x7f1304bc

.field public static final Widget_Marin_RadioButton:I = 0x7f1304bd

.field public static final Widget_Marin_ScrollView:I = 0x7f1304be

.field public static final Widget_Marin_ScrollView_GutterFull:I = 0x7f1304bf

.field public static final Widget_Marin_ScrollView_GutterHalf:I = 0x7f1304c0

.field public static final Widget_Marin_SeekBar:I = 0x7f1304c1

.field public static final Widget_Marin_Spinner:I = 0x7f1304c2

.field public static final Widget_Marin_Spinner_DropDown:I = 0x7f1304c3

.field public static final Widget_Marin_Switch:I = 0x7f1304c4

.field public static final Widget_Marin_TextView:I = 0x7f1304c5

.field public static final Widget_Marin_TextView_ActionBar_Navigation:I = 0x7f1304c6

.field public static final Widget_Marin_TextView_ConversationBubbleLeft:I = 0x7f1304c7

.field public static final Widget_Marin_TextView_ConversationBubbleRight:I = 0x7f1304c8

.field public static final Widget_Marin_TextView_Drawer:I = 0x7f1304c9

.field public static final Widget_Marin_TextView_DropDownItem:I = 0x7f1304ca

.field public static final Widget_Marin_TextView_SectionHeader:I = 0x7f1304cb

.field public static final Widget_Marin_TextView_SectionHeader_GutterFull:I = 0x7f1304cc

.field public static final Widget_Marin_TextView_SectionHeader_GutterFull_LargerTopPadding:I = 0x7f1304cd

.field public static final Widget_Marin_TextView_SectionHeader_GutterFull_NoTopPadding:I = 0x7f1304ce

.field public static final Widget_Marin_TextView_SectionHeader_GutterHalf:I = 0x7f1304cf

.field public static final Widget_Marin_TextView_SectionHeader_GutterHalf_Gradient:I = 0x7f1304d0

.field public static final Widget_Marin_TextView_SectionHeader_GutterHalf_LargerTopPadding:I = 0x7f1304d1

.field public static final Widget_Marin_TextView_SectionHeader_GutterHalf_NoTopPadding:I = 0x7f1304d2

.field public static final Widget_Marin_TextView_SectionHeader_InvoiceDetailHeader_Primary:I = 0x7f1304d3

.field public static final Widget_Marin_TextView_SectionHeader_InvoiceDetailHeader_Secondary:I = 0x7f1304d4

.field public static final Widget_Marin_TextView_SectionHeader_LargerTopPadding:I = 0x7f1304d5

.field public static final Widget_Marin_TextView_SectionHeader_MediumGray:I = 0x7f1304d6

.field public static final Widget_Marin_TextView_SectionHeader_NoTopPadding:I = 0x7f1304d7

.field public static final Widget_Marin_TextView_SectionSubHeader:I = 0x7f1304d8

.field public static final Widget_Marin_TextView_Spinner:I = 0x7f1304d9

.field public static final Widget_Marin_TextView_Spinner_Margin_GutterHalf:I = 0x7f1304da

.field public static final Widget_Marin_TextView_TextBoxBorder:I = 0x7f1304db

.field public static final Widget_Marin_TextView_Tooltip:I = 0x7f1304dc

.field public static final Widget_Marin_TimePicker:I = 0x7f1304dd

.field public static final Widget_Marin_TimePickerCell:I = 0x7f1304de

.field public static final Widget_Marin_TokenView:I = 0x7f1304df

.field public static final Widget_Marin_WifiNameRow:I = 0x7f1304e0

.field public static final Widget_Marin_WifiNameRow_Dark:I = 0x7f1304e1

.field public static final Widget_Marin_X2LinearLayout_Vertical:I = 0x7f1304e2

.field public static final Widget_MaterialComponents_ActionBar_Primary:I = 0x7f1304e3

.field public static final Widget_MaterialComponents_ActionBar_PrimarySurface:I = 0x7f1304e4

.field public static final Widget_MaterialComponents_ActionBar_Solid:I = 0x7f1304e5

.field public static final Widget_MaterialComponents_ActionBar_Surface:I = 0x7f1304e6

.field public static final Widget_MaterialComponents_AppBarLayout_Primary:I = 0x7f1304e7

.field public static final Widget_MaterialComponents_AppBarLayout_PrimarySurface:I = 0x7f1304e8

.field public static final Widget_MaterialComponents_AppBarLayout_Surface:I = 0x7f1304e9

.field public static final Widget_MaterialComponents_AutoCompleteTextView_FilledBox:I = 0x7f1304ea

.field public static final Widget_MaterialComponents_AutoCompleteTextView_FilledBox_Dense:I = 0x7f1304eb

.field public static final Widget_MaterialComponents_AutoCompleteTextView_OutlinedBox:I = 0x7f1304ec

.field public static final Widget_MaterialComponents_AutoCompleteTextView_OutlinedBox_Dense:I = 0x7f1304ed

.field public static final Widget_MaterialComponents_Badge:I = 0x7f1304ee

.field public static final Widget_MaterialComponents_BottomAppBar:I = 0x7f1304ef

.field public static final Widget_MaterialComponents_BottomAppBar_Colored:I = 0x7f1304f0

.field public static final Widget_MaterialComponents_BottomAppBar_PrimarySurface:I = 0x7f1304f1

.field public static final Widget_MaterialComponents_BottomNavigationView:I = 0x7f1304f2

.field public static final Widget_MaterialComponents_BottomNavigationView_Colored:I = 0x7f1304f3

.field public static final Widget_MaterialComponents_BottomNavigationView_PrimarySurface:I = 0x7f1304f4

.field public static final Widget_MaterialComponents_BottomSheet:I = 0x7f1304f5

.field public static final Widget_MaterialComponents_BottomSheet_Modal:I = 0x7f1304f6

.field public static final Widget_MaterialComponents_Button:I = 0x7f1304f7

.field public static final Widget_MaterialComponents_Button_Icon:I = 0x7f1304f8

.field public static final Widget_MaterialComponents_Button_OutlinedButton:I = 0x7f1304f9

.field public static final Widget_MaterialComponents_Button_OutlinedButton_Icon:I = 0x7f1304fa

.field public static final Widget_MaterialComponents_Button_TextButton:I = 0x7f1304fb

.field public static final Widget_MaterialComponents_Button_TextButton_Dialog:I = 0x7f1304fc

.field public static final Widget_MaterialComponents_Button_TextButton_Dialog_Flush:I = 0x7f1304fd

.field public static final Widget_MaterialComponents_Button_TextButton_Dialog_Icon:I = 0x7f1304fe

.field public static final Widget_MaterialComponents_Button_TextButton_Icon:I = 0x7f1304ff

.field public static final Widget_MaterialComponents_Button_TextButton_Snackbar:I = 0x7f130500

.field public static final Widget_MaterialComponents_Button_UnelevatedButton:I = 0x7f130501

.field public static final Widget_MaterialComponents_Button_UnelevatedButton_Icon:I = 0x7f130502

.field public static final Widget_MaterialComponents_CardView:I = 0x7f130503

.field public static final Widget_MaterialComponents_CheckedTextView:I = 0x7f130504

.field public static final Widget_MaterialComponents_ChipGroup:I = 0x7f130509

.field public static final Widget_MaterialComponents_Chip_Action:I = 0x7f130505

.field public static final Widget_MaterialComponents_Chip_Choice:I = 0x7f130506

.field public static final Widget_MaterialComponents_Chip_Entry:I = 0x7f130507

.field public static final Widget_MaterialComponents_Chip_Filter:I = 0x7f130508

.field public static final Widget_MaterialComponents_CompoundButton_CheckBox:I = 0x7f13050a

.field public static final Widget_MaterialComponents_CompoundButton_RadioButton:I = 0x7f13050b

.field public static final Widget_MaterialComponents_CompoundButton_Switch:I = 0x7f13050c

.field public static final Widget_MaterialComponents_ExtendedFloatingActionButton:I = 0x7f13050d

.field public static final Widget_MaterialComponents_ExtendedFloatingActionButton_Icon:I = 0x7f13050e

.field public static final Widget_MaterialComponents_FloatingActionButton:I = 0x7f13050f

.field public static final Widget_MaterialComponents_Light_ActionBar_Solid:I = 0x7f130510

.field public static final Widget_MaterialComponents_MaterialButtonToggleGroup:I = 0x7f130511

.field public static final Widget_MaterialComponents_MaterialCalendar:I = 0x7f130512

.field public static final Widget_MaterialComponents_MaterialCalendar_Day:I = 0x7f130513

.field public static final Widget_MaterialComponents_MaterialCalendar_DayTextView:I = 0x7f130517

.field public static final Widget_MaterialComponents_MaterialCalendar_Day_Invalid:I = 0x7f130514

.field public static final Widget_MaterialComponents_MaterialCalendar_Day_Selected:I = 0x7f130515

.field public static final Widget_MaterialComponents_MaterialCalendar_Day_Today:I = 0x7f130516

.field public static final Widget_MaterialComponents_MaterialCalendar_Fullscreen:I = 0x7f130518

.field public static final Widget_MaterialComponents_MaterialCalendar_HeaderConfirmButton:I = 0x7f130519

.field public static final Widget_MaterialComponents_MaterialCalendar_HeaderDivider:I = 0x7f13051a

.field public static final Widget_MaterialComponents_MaterialCalendar_HeaderLayout:I = 0x7f13051b

.field public static final Widget_MaterialComponents_MaterialCalendar_HeaderSelection:I = 0x7f13051c

.field public static final Widget_MaterialComponents_MaterialCalendar_HeaderSelection_Fullscreen:I = 0x7f13051d

.field public static final Widget_MaterialComponents_MaterialCalendar_HeaderTitle:I = 0x7f13051e

.field public static final Widget_MaterialComponents_MaterialCalendar_HeaderToggleButton:I = 0x7f13051f

.field public static final Widget_MaterialComponents_MaterialCalendar_Item:I = 0x7f130520

.field public static final Widget_MaterialComponents_MaterialCalendar_Year:I = 0x7f130521

.field public static final Widget_MaterialComponents_MaterialCalendar_Year_Selected:I = 0x7f130522

.field public static final Widget_MaterialComponents_MaterialCalendar_Year_Today:I = 0x7f130523

.field public static final Widget_MaterialComponents_NavigationView:I = 0x7f130524

.field public static final Widget_MaterialComponents_PopupMenu:I = 0x7f130525

.field public static final Widget_MaterialComponents_PopupMenu_ContextMenu:I = 0x7f130526

.field public static final Widget_MaterialComponents_PopupMenu_ListPopupWindow:I = 0x7f130527

.field public static final Widget_MaterialComponents_PopupMenu_Overflow:I = 0x7f130528

.field public static final Widget_MaterialComponents_Snackbar:I = 0x7f130529

.field public static final Widget_MaterialComponents_Snackbar_FullWidth:I = 0x7f13052a

.field public static final Widget_MaterialComponents_TabLayout:I = 0x7f13052b

.field public static final Widget_MaterialComponents_TabLayout_Colored:I = 0x7f13052c

.field public static final Widget_MaterialComponents_TabLayout_PrimarySurface:I = 0x7f13052d

.field public static final Widget_MaterialComponents_TextInputEditText_FilledBox:I = 0x7f13052e

.field public static final Widget_MaterialComponents_TextInputEditText_FilledBox_Dense:I = 0x7f13052f

.field public static final Widget_MaterialComponents_TextInputEditText_OutlinedBox:I = 0x7f130530

.field public static final Widget_MaterialComponents_TextInputEditText_OutlinedBox_Dense:I = 0x7f130531

.field public static final Widget_MaterialComponents_TextInputLayout_FilledBox:I = 0x7f130532

.field public static final Widget_MaterialComponents_TextInputLayout_FilledBox_Dense:I = 0x7f130533

.field public static final Widget_MaterialComponents_TextInputLayout_FilledBox_Dense_ExposedDropdownMenu:I = 0x7f130534

.field public static final Widget_MaterialComponents_TextInputLayout_FilledBox_ExposedDropdownMenu:I = 0x7f130535

.field public static final Widget_MaterialComponents_TextInputLayout_OutlinedBox:I = 0x7f130536

.field public static final Widget_MaterialComponents_TextInputLayout_OutlinedBox_Dense:I = 0x7f130537

.field public static final Widget_MaterialComponents_TextInputLayout_OutlinedBox_Dense_ExposedDropdownMenu:I = 0x7f130538

.field public static final Widget_MaterialComponents_TextInputLayout_OutlinedBox_ExposedDropdownMenu:I = 0x7f130539

.field public static final Widget_MaterialComponents_TextView:I = 0x7f13053a

.field public static final Widget_MaterialComponents_Toolbar:I = 0x7f13053b

.field public static final Widget_MaterialComponents_Toolbar_Primary:I = 0x7f13053c

.field public static final Widget_MaterialComponents_Toolbar_PrimarySurface:I = 0x7f13053d

.field public static final Widget_MaterialComponents_Toolbar_Surface:I = 0x7f13053e

.field public static final Widget_Noho_Banner:I = 0x7f13053f

.field public static final Widget_Noho_Button_BuyerFacingLanguageSelection:I = 0x7f130540

.field public static final Widget_Noho_Button_Dialog:I = 0x7f130541

.field public static final Widget_Noho_Button_Dialog_TutorialPrimary:I = 0x7f130542

.field public static final Widget_Noho_Button_Dialog_TutorialSecondary:I = 0x7f130543

.field public static final Widget_Noho_CardEditor:I = 0x7f130544

.field public static final Widget_Noho_ChargeButton:I = 0x7f130545

.field public static final Widget_Noho_ChargeButtonPaymentFlow:I = 0x7f130546

.field public static final Widget_Noho_Check_Colors:I = 0x7f130547

.field public static final Widget_Noho_Check_Colors_Disabled:I = 0x7f130548

.field public static final Widget_Noho_Check_Colors_Focused:I = 0x7f130549

.field public static final Widget_Noho_Check_Colors_Normal:I = 0x7f13054a

.field public static final Widget_Noho_Check_Colors_Pressed:I = 0x7f13054b

.field public static final Widget_Noho_CheckableRow:I = 0x7f13054c

.field public static final Widget_Noho_ConstraintLayout:I = 0x7f13054d

.field public static final Widget_Noho_DatePicker:I = 0x7f13054e

.field public static final Widget_Noho_DestructiveButton:I = 0x7f13054f

.field public static final Widget_Noho_Dropdown:I = 0x7f130550

.field public static final Widget_Noho_Dropdown_MenuItem:I = 0x7f130551

.field public static final Widget_Noho_Dropdown_Popup:I = 0x7f130552

.field public static final Widget_Noho_Dropdown_Popup_ListView:I = 0x7f130553

.field public static final Widget_Noho_Dropdown_SelectedItem:I = 0x7f130554

.field public static final Widget_Noho_Edit:I = 0x7f130555

.field public static final Widget_Noho_EditText:I = 0x7f130557

.field public static final Widget_Noho_EditTextPaymentFlow:I = 0x7f13055b

.field public static final Widget_Noho_EditText_AdjustInventoryField:I = 0x7f130558

.field public static final Widget_Noho_EditText_ItemAttributeField_FirstField:I = 0x7f130559

.field public static final Widget_Noho_EditText_ItemAttributeField_FollowingField:I = 0x7f13055a

.field public static final Widget_Noho_Edit_AdjustInventoryField:I = 0x7f130556

.field public static final Widget_Noho_HorizontalDividers:I = 0x7f13055c

.field public static final Widget_Noho_IconButton:I = 0x7f13055d

.field public static final Widget_Noho_InputBox:I = 0x7f13055e

.field public static final Widget_Noho_Label_Base:I = 0x7f13055f

.field public static final Widget_Noho_Label_Body:I = 0x7f130560

.field public static final Widget_Noho_Label_Body2:I = 0x7f130561

.field public static final Widget_Noho_Label_Display:I = 0x7f130562

.field public static final Widget_Noho_Label_Display2:I = 0x7f130563

.field public static final Widget_Noho_Label_Heading:I = 0x7f130564

.field public static final Widget_Noho_Label_Heading2:I = 0x7f130565

.field public static final Widget_Noho_Label_Label:I = 0x7f130566

.field public static final Widget_Noho_Label_Label2:I = 0x7f130567

.field public static final Widget_Noho_LinearLayout:I = 0x7f130568

.field public static final Widget_Noho_LinkButton:I = 0x7f130569

.field public static final Widget_Noho_LinkText:I = 0x7f13056a

.field public static final Widget_Noho_ListView:I = 0x7f13056b

.field public static final Widget_Noho_MessageText:I = 0x7f13056c

.field public static final Widget_Noho_MessageView:I = 0x7f13056d

.field public static final Widget_Noho_Notification:I = 0x7f13056e

.field public static final Widget_Noho_Notification_Balloon:I = 0x7f13056f

.field public static final Widget_Noho_Notification_Base:I = 0x7f130570

.field public static final Widget_Noho_Notification_Fatal:I = 0x7f130571

.field public static final Widget_Noho_Notification_Fatal_Balloon:I = 0x7f130572

.field public static final Widget_Noho_Notification_Fatal_FilterOut:I = 0x7f130573

.field public static final Widget_Noho_Notification_FilterOut:I = 0x7f130574

.field public static final Widget_Noho_Notification_HighPri:I = 0x7f130575

.field public static final Widget_Noho_Notification_HighPri_Balloon:I = 0x7f130576

.field public static final Widget_Noho_Notification_Text:I = 0x7f130577

.field public static final Widget_Noho_NumberPicker:I = 0x7f130578

.field public static final Widget_Noho_PrimaryButton:I = 0x7f130579

.field public static final Widget_Noho_PrimaryButtonLarge:I = 0x7f13057a

.field public static final Widget_Noho_Radio_Colors:I = 0x7f13057b

.field public static final Widget_Noho_Radio_Colors_Checked:I = 0x7f13057c

.field public static final Widget_Noho_Radio_Colors_CheckedPressed:I = 0x7f13057d

.field public static final Widget_Noho_Radio_Colors_Disabled:I = 0x7f13057e

.field public static final Widget_Noho_Radio_Colors_Normal:I = 0x7f13057f

.field public static final Widget_Noho_Radio_Colors_Pressed:I = 0x7f130580

.field public static final Widget_Noho_RecyclerEdges:I = 0x7f130581

.field public static final Widget_Noho_RecyclerView:I = 0x7f130582

.field public static final Widget_Noho_ResponsiveLinearLayout:I = 0x7f130583

.field public static final Widget_Noho_Row:I = 0x7f130584

.field public static final Widget_Noho_Row_Accessory:I = 0x7f130585

.field public static final Widget_Noho_Row_ActionIcon:I = 0x7f130586

.field public static final Widget_Noho_Row_Icon:I = 0x7f130587

.field public static final Widget_Noho_Row_Legacy:I = 0x7f130588

.field public static final Widget_Noho_Row_Legacy_Standard:I = 0x7f130589

.field public static final Widget_Noho_ScrollView:I = 0x7f13058a

.field public static final Widget_Noho_SecondaryButton:I = 0x7f13058b

.field public static final Widget_Noho_SecondaryButtonLarge:I = 0x7f13058c

.field public static final Widget_Noho_Selectable:I = 0x7f13058d

.field public static final Widget_Noho_Spinner:I = 0x7f13058e

.field public static final Widget_Noho_Squid_SecureKeypad:I = 0x7f13058f

.field public static final Widget_Noho_Squid_SecureKeypad_Button:I = 0x7f130590

.field public static final Widget_Noho_Squid_SecureKeypad_Button_Clear:I = 0x7f130591

.field public static final Widget_Noho_Squid_SecureKeypad_Button_Done:I = 0x7f130592

.field public static final Widget_Noho_Squid_SecureKeypad_Divider:I = 0x7f130593

.field public static final Widget_Noho_Squid_SecureKeypad_Divider_Horizontal:I = 0x7f130594

.field public static final Widget_Noho_Squid_SecureKeypad_Divider_Vertical:I = 0x7f130595

.field public static final Widget_Noho_Squid_SecureKeypad_Key:I = 0x7f130596

.field public static final Widget_Noho_Squid_SecureKeypad_Key_Icon:I = 0x7f130597

.field public static final Widget_Noho_Squid_SecureKeypad_Key_SoloText:I = 0x7f130598

.field public static final Widget_Noho_Squid_SecureKeypad_Key_Subtitle:I = 0x7f130599

.field public static final Widget_Noho_Squid_SecureKeypad_Key_Title:I = 0x7f13059a

.field public static final Widget_Noho_Squid_SecureKeypad_ModeSelection:I = 0x7f13059b

.field public static final Widget_Noho_Squid_SecureKeypad_ModeSelection_Button:I = 0x7f13059c

.field public static final Widget_Noho_Squid_SecureKeypad_ModeSelection_Button_Negative:I = 0x7f13059d

.field public static final Widget_Noho_Squid_SecureKeypad_ModeSelection_Button_Positive:I = 0x7f13059e

.field public static final Widget_Noho_Squid_SecureKeypad_ModeSelection_DisplayAmount:I = 0x7f13059f

.field public static final Widget_Noho_Squid_SecureKeypad_ModeSelection_Title:I = 0x7f1305a0

.field public static final Widget_Noho_Squid_SecureKeypad_StarGroup:I = 0x7f1305a1

.field public static final Widget_Noho_SubHeader:I = 0x7f1305a2

.field public static final Widget_Noho_Switch:I = 0x7f1305a3

.field public static final Widget_Noho_TertiaryButton:I = 0x7f1305a4

.field public static final Widget_Noho_TertiaryButtonLarge:I = 0x7f1305a5

.field public static final Widget_Noho_TextView:I = 0x7f1305a6

.field public static final Widget_Noho_TextView_Help:I = 0x7f1305a7

.field public static final Widget_Noho_TopBar:I = 0x7f1305a8

.field public static final Widget_Noho_TopBarAction:I = 0x7f1305a9

.field public static final Widget_Noho_TopBarIcon:I = 0x7f1305aa

.field public static final Widget_Noho_TopBarTitle:I = 0x7f1305ab

.field public static final Widget_Noho_TransactionsHistory_SearchBar:I = 0x7f1305ac

.field public static final Widget_OfflineBanner:I = 0x7f1305ad

.field public static final Widget_Onboarding_SubHeader:I = 0x7f1305ae

.field public static final Widget_PanEditor:I = 0x7f1305af

.field public static final Widget_PanEditor_Margin_GutterHalf:I = 0x7f1305b0

.field public static final Widget_PreservedLayoutView:I = 0x7f1305b1

.field public static final Widget_ReaderMessageBarTextView:I = 0x7f1305b2

.field public static final Widget_SalesReport:I = 0x7f1305b3

.field public static final Widget_Support_CoordinatorLayout:I = 0x7f1305b4

.field public static final Widget_TipSettings_Label_Description:I = 0x7f1305b5

.field public static final Widget_TipSettings_Label_Heading:I = 0x7f1305b6

.field public static final Widget_XableEditText_Margin_GutterFull:I = 0x7f1305b7

.field public static final Widget_XableEditText_Margin_GutterHalf:I = 0x7f1305b8

.field public static final Widget_XableEditText_Margin_GutterHalf_UltraLightGrayBackground:I = 0x7f1305b9

.field public static final Widget_XableEditText_SearchBar:I = 0x7f1305ba

.field public static final Widget_XableEditText_UltraLightGrayBackground:I = 0x7f1305bb


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 27300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
