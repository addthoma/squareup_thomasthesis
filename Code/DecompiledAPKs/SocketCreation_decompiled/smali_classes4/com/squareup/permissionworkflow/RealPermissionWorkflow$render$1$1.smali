.class final Lcom/squareup/permissionworkflow/RealPermissionWorkflow$render$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealPermissionWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/permissionworkflow/RealPermissionWorkflow$render$1;->invoke(Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorkerResult;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "",
        "Lcom/squareup/permissionworkflow/PermissionRequestOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $result:Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorkerResult;


# direct methods
.method constructor <init>(Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorkerResult;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$render$1$1;->$result:Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorkerResult;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$render$1$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 2

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$render$1$1;->$result:Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorkerResult;

    .line 38
    instance-of v1, v0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorkerResult$Success;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/squareup/permissionworkflow/PermissionRequestOutput$Success;

    check-cast v0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorkerResult$Success;

    invoke-virtual {v0}, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorkerResult$Success;->getAuthorizedEmployee()Lcom/squareup/protos/client/Employee;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/squareup/permissionworkflow/PermissionRequestOutput$Success;-><init>(Lcom/squareup/protos/client/Employee;)V

    check-cast v1, Lcom/squareup/permissionworkflow/PermissionRequestOutput;

    goto :goto_0

    .line 39
    :cond_0
    instance-of v0, v0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorkerResult$Failure;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/permissionworkflow/PermissionRequestOutput$Cancelled;->INSTANCE:Lcom/squareup/permissionworkflow/PermissionRequestOutput$Cancelled;

    move-object v1, v0

    check-cast v1, Lcom/squareup/permissionworkflow/PermissionRequestOutput;

    .line 36
    :goto_0
    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    return-void

    .line 39
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
