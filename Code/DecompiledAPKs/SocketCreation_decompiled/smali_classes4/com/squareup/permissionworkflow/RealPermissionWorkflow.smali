.class public final Lcom/squareup/permissionworkflow/RealPermissionWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "RealPermissionWorkflow.kt"

# interfaces
.implements Lcom/squareup/permissionworkflow/PermissionWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;,
        Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorkerResult;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/permissionworkflow/PermissionRequestInput;",
        "Lcom/squareup/permissionworkflow/PermissionRequestOutput;",
        "Lkotlin/Unit;",
        ">;",
        "Lcom/squareup/permissionworkflow/PermissionWorkflow;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0003\u0018\u00002\u00020\u00012\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0002:\u0002\u0010\u0011B\u0017\u0008\u0007\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ$\u0010\u000b\u001a\u00020\u00052\u0006\u0010\u000c\u001a\u00020\u00032\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00040\u000eH\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/permissionworkflow/RealPermissionWorkflow;",
        "Lcom/squareup/permissionworkflow/PermissionWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/permissionworkflow/PermissionRequestInput;",
        "Lcom/squareup/permissionworkflow/PermissionRequestOutput;",
        "",
        "permissionGatekeeper",
        "Lcom/squareup/permissions/PermissionGatekeeper;",
        "passcodeEmployeeManagement",
        "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
        "(Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/PasscodeEmployeeManagement;)V",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "PermissionWorker",
        "PermissionWorkerResult",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;


# direct methods
.method public constructor <init>(Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/PasscodeEmployeeManagement;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "permissionGatekeeper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "passcodeEmployeeManagement"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    iput-object p2, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    return-void
.end method


# virtual methods
.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Lcom/squareup/permissionworkflow/PermissionRequestInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/permissionworkflow/RealPermissionWorkflow;->render(Lcom/squareup/permissionworkflow/PermissionRequestInput;Lcom/squareup/workflow/RenderContext;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public render(Lcom/squareup/permissionworkflow/PermissionRequestInput;Lcom/squareup/workflow/RenderContext;)V
    .locals 9

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;

    iget-object v1, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v2, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;-><init>(Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissionworkflow/PermissionRequestInput;)V

    move-object v4, v0

    check-cast v4, Lcom/squareup/workflow/Worker;

    .line 34
    new-instance p1, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$render$1;

    invoke-direct {p1, p0}, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$render$1;-><init>(Lcom/squareup/permissionworkflow/RealPermissionWorkflow;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x0

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v3, p2

    .line 32
    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
