.class public final enum Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;
.super Ljava/lang/Enum;
.source "PersistentBundleTimingEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/persistentbundle/PersistentBundleTimingEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EventType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;

.field public static final enum PERSIST:Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;

.field public static final enum SERIALIZE:Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;

.field public static final enum START:Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 10
    new-instance v0, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;

    const/4 v1, 0x0

    const-string v2, "START"

    invoke-direct {v0, v2, v1}, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;->START:Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;

    new-instance v0, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;

    const/4 v2, 0x1

    const-string v3, "SERIALIZE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;->SERIALIZE:Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;

    new-instance v0, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;

    const/4 v3, 0x2

    const-string v4, "PERSIST"

    invoke-direct {v0, v4, v3}, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;->PERSIST:Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;

    .line 9
    sget-object v4, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;->START:Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;->SERIALIZE:Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;->PERSIST:Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;->$VALUES:[Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;
    .locals 1

    .line 9
    const-class v0, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;->$VALUES:[Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;

    invoke-virtual {v0}, [Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;

    return-object v0
.end method
