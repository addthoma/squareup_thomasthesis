.class public final Lcom/squareup/proto_utilities/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/proto_utilities/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final accounting_money_format_negative:I = 0x7f12003e

.field public static final catalogMeasurementUnitAbbreviation_area_imperialAcre:I = 0x7f12038a

.field public static final catalogMeasurementUnitAbbreviation_area_imperialSquareFoot:I = 0x7f12038b

.field public static final catalogMeasurementUnitAbbreviation_area_imperialSquareInch:I = 0x7f12038c

.field public static final catalogMeasurementUnitAbbreviation_area_imperialSquareMile:I = 0x7f12038d

.field public static final catalogMeasurementUnitAbbreviation_area_imperialSquareYard:I = 0x7f12038e

.field public static final catalogMeasurementUnitAbbreviation_area_metricSquareCentimeter:I = 0x7f12038f

.field public static final catalogMeasurementUnitAbbreviation_area_metricSquareKilometer:I = 0x7f120390

.field public static final catalogMeasurementUnitAbbreviation_area_metricSquareMeter:I = 0x7f120391

.field public static final catalogMeasurementUnitAbbreviation_length_imperialFoot:I = 0x7f120392

.field public static final catalogMeasurementUnitAbbreviation_length_imperialInch:I = 0x7f120393

.field public static final catalogMeasurementUnitAbbreviation_length_imperialMile:I = 0x7f120394

.field public static final catalogMeasurementUnitAbbreviation_length_imperialYard:I = 0x7f120395

.field public static final catalogMeasurementUnitAbbreviation_length_metricCentimeter:I = 0x7f120396

.field public static final catalogMeasurementUnitAbbreviation_length_metricKilometer:I = 0x7f120397

.field public static final catalogMeasurementUnitAbbreviation_length_metricMeter:I = 0x7f120398

.field public static final catalogMeasurementUnitAbbreviation_length_metricMillimeter:I = 0x7f120399

.field public static final catalogMeasurementUnitAbbreviation_time_genericDay:I = 0x7f12039a

.field public static final catalogMeasurementUnitAbbreviation_time_genericHour:I = 0x7f12039b

.field public static final catalogMeasurementUnitAbbreviation_time_genericMillisecond:I = 0x7f12039c

.field public static final catalogMeasurementUnitAbbreviation_time_genericMinute:I = 0x7f12039d

.field public static final catalogMeasurementUnitAbbreviation_time_genericSecond:I = 0x7f12039e

.field public static final catalogMeasurementUnitAbbreviation_volume_genericCup:I = 0x7f12039f

.field public static final catalogMeasurementUnitAbbreviation_volume_genericFluidOunce:I = 0x7f1203a0

.field public static final catalogMeasurementUnitAbbreviation_volume_genericGallon:I = 0x7f1203a1

.field public static final catalogMeasurementUnitAbbreviation_volume_genericPint:I = 0x7f1203a2

.field public static final catalogMeasurementUnitAbbreviation_volume_genericQuart:I = 0x7f1203a3

.field public static final catalogMeasurementUnitAbbreviation_volume_genericShot:I = 0x7f1203a4

.field public static final catalogMeasurementUnitAbbreviation_volume_imperialCubicFoot:I = 0x7f1203a5

.field public static final catalogMeasurementUnitAbbreviation_volume_imperialCubicInch:I = 0x7f1203a6

.field public static final catalogMeasurementUnitAbbreviation_volume_imperialCubicYard:I = 0x7f1203a7

.field public static final catalogMeasurementUnitAbbreviation_volume_metricLiter:I = 0x7f1203a8

.field public static final catalogMeasurementUnitAbbreviation_volume_metricMilliliter:I = 0x7f1203a9

.field public static final catalogMeasurementUnitAbbreviation_weight_imperialPound:I = 0x7f1203aa

.field public static final catalogMeasurementUnitAbbreviation_weight_imperialStone:I = 0x7f1203ab

.field public static final catalogMeasurementUnitAbbreviation_weight_imperialWeightOunce:I = 0x7f1203ac

.field public static final catalogMeasurementUnitAbbreviation_weight_metricGram:I = 0x7f1203ad

.field public static final catalogMeasurementUnitAbbreviation_weight_metricKilogram:I = 0x7f1203ae

.field public static final catalogMeasurementUnitAbbreviation_weight_metricMilligram:I = 0x7f1203af

.field public static final catalogMeasurementUnitName_area_imperialAcre:I = 0x7f1203b0

.field public static final catalogMeasurementUnitName_area_imperialSquareFoot:I = 0x7f1203b1

.field public static final catalogMeasurementUnitName_area_imperialSquareInch:I = 0x7f1203b2

.field public static final catalogMeasurementUnitName_area_imperialSquareMile:I = 0x7f1203b3

.field public static final catalogMeasurementUnitName_area_imperialSquareYard:I = 0x7f1203b4

.field public static final catalogMeasurementUnitName_area_metricSquareCentimeter:I = 0x7f1203b5

.field public static final catalogMeasurementUnitName_area_metricSquareKilometer:I = 0x7f1203b6

.field public static final catalogMeasurementUnitName_area_metricSquareMeter:I = 0x7f1203b7

.field public static final catalogMeasurementUnitName_length_imperialFoot:I = 0x7f1203b8

.field public static final catalogMeasurementUnitName_length_imperialInch:I = 0x7f1203b9

.field public static final catalogMeasurementUnitName_length_imperialMile:I = 0x7f1203ba

.field public static final catalogMeasurementUnitName_length_imperialYard:I = 0x7f1203bb

.field public static final catalogMeasurementUnitName_length_metricCentimeter:I = 0x7f1203bc

.field public static final catalogMeasurementUnitName_length_metricKilometer:I = 0x7f1203bd

.field public static final catalogMeasurementUnitName_length_metricMeter:I = 0x7f1203be

.field public static final catalogMeasurementUnitName_length_metricMillimeter:I = 0x7f1203bf

.field public static final catalogMeasurementUnitName_time_genericDay:I = 0x7f1203c0

.field public static final catalogMeasurementUnitName_time_genericHour:I = 0x7f1203c1

.field public static final catalogMeasurementUnitName_time_genericMillisecond:I = 0x7f1203c2

.field public static final catalogMeasurementUnitName_time_genericMinute:I = 0x7f1203c3

.field public static final catalogMeasurementUnitName_time_genericSecond:I = 0x7f1203c4

.field public static final catalogMeasurementUnitName_volume_genericCup:I = 0x7f1203c5

.field public static final catalogMeasurementUnitName_volume_genericFluidOunce:I = 0x7f1203c6

.field public static final catalogMeasurementUnitName_volume_genericGallon:I = 0x7f1203c7

.field public static final catalogMeasurementUnitName_volume_genericPint:I = 0x7f1203c8

.field public static final catalogMeasurementUnitName_volume_genericQuart:I = 0x7f1203c9

.field public static final catalogMeasurementUnitName_volume_genericShot:I = 0x7f1203ca

.field public static final catalogMeasurementUnitName_volume_imperialCubicFoot:I = 0x7f1203cb

.field public static final catalogMeasurementUnitName_volume_imperialCubicInch:I = 0x7f1203cc

.field public static final catalogMeasurementUnitName_volume_imperialCubicYard:I = 0x7f1203cd

.field public static final catalogMeasurementUnitName_volume_metricLiter:I = 0x7f1203ce

.field public static final catalogMeasurementUnitName_volume_metricMilliliter:I = 0x7f1203cf

.field public static final catalogMeasurementUnitName_weight_imperialPound:I = 0x7f1203d0

.field public static final catalogMeasurementUnitName_weight_imperialStone:I = 0x7f1203d1

.field public static final catalogMeasurementUnitName_weight_imperialWeightOunce:I = 0x7f1203d2

.field public static final catalogMeasurementUnitName_weight_metricGram:I = 0x7f1203d3

.field public static final catalogMeasurementUnitName_weight_metricKilogram:I = 0x7f1203d4

.field public static final catalogMeasurementUnitName_weight_metricMilligram:I = 0x7f1203d5

.field public static final combined_rate_format:I = 0x7f120441

.field public static final compact_money_format_billion:I = 0x7f120463

.field public static final compact_money_format_million:I = 0x7f120464

.field public static final compact_money_format_thousand:I = 0x7f120465

.field public static final currency_format_cents:I = 0x7f120786

.field public static final generic_unit_name:I = 0x7f120aed

.field public static final percent_character_pattern:I = 0x7f12142f

.field public static final precision_display_value_0:I = 0x7f121460

.field public static final precision_display_value_1:I = 0x7f121461

.field public static final precision_display_value_2:I = 0x7f121462

.field public static final precision_display_value_3:I = 0x7f121463

.field public static final precision_display_value_4:I = 0x7f121464

.field public static final precision_display_value_5:I = 0x7f121465

.field public static final quantity_entry_manual_with_connected_scale:I = 0x7f1214fb

.field public static final refund_reason_accidental_charge:I = 0x7f121647

.field public static final refund_reason_canceled_order:I = 0x7f121648

.field public static final refund_reason_fraudulent_charge:I = 0x7f121649

.field public static final refund_reason_returned_goods:I = 0x7f12164c

.field public static final refund_string:I = 0x7f121651


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
