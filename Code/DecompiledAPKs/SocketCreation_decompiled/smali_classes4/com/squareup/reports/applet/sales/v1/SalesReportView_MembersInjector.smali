.class public final Lcom/squareup/reports/applet/sales/v1/SalesReportView_MembersInjector;
.super Ljava/lang/Object;
.source "SalesReportView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/reports/applet/sales/v1/SalesReportView;",
        ">;"
    }
.end annotation


# instance fields
.field private final chartDateFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final perUnitFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView_MembersInjector;->chartDateFormatterProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView_MembersInjector;->perUnitFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/reports/applet/sales/v1/SalesReportView;",
            ">;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/SalesReportView_MembersInjector;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/reports/applet/sales/v1/SalesReportView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectChartDateFormatter(Lcom/squareup/reports/applet/sales/v1/SalesReportView;Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;)V
    .locals 0

    .line 58
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->chartDateFormatter:Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;

    return-void
.end method

.method public static injectDevice(Lcom/squareup/reports/applet/sales/v1/SalesReportView;Lcom/squareup/util/Device;)V
    .locals 0

    .line 63
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static injectPerUnitFormatter(Lcom/squareup/reports/applet/sales/v1/SalesReportView;Lcom/squareup/quantity/PerUnitFormatter;)V
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/reports/applet/sales/v1/SalesReportView;Ljava/lang/Object;)V
    .locals 0

    .line 52
    check-cast p1, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->presenter:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/reports/applet/sales/v1/SalesReportView;)V
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportView_MembersInjector;->injectPresenter(Lcom/squareup/reports/applet/sales/v1/SalesReportView;Ljava/lang/Object;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView_MembersInjector;->chartDateFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;

    invoke-static {p1, v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportView_MembersInjector;->injectChartDateFormatter(Lcom/squareup/reports/applet/sales/v1/SalesReportView;Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportView_MembersInjector;->injectDevice(Lcom/squareup/reports/applet/sales/v1/SalesReportView;Lcom/squareup/util/Device;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView_MembersInjector;->perUnitFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/quantity/PerUnitFormatter;

    invoke-static {p1, v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportView_MembersInjector;->injectPerUnitFormatter(Lcom/squareup/reports/applet/sales/v1/SalesReportView;Lcom/squareup/quantity/PerUnitFormatter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/reports/applet/sales/v1/SalesReportView;

    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportView_MembersInjector;->injectMembers(Lcom/squareup/reports/applet/sales/v1/SalesReportView;)V

    return-void
.end method
