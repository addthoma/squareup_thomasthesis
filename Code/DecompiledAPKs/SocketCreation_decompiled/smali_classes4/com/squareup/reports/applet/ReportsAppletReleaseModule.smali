.class public final Lcom/squareup/reports/applet/ReportsAppletReleaseModule;
.super Ljava/lang/Object;
.source "ReportsAppletReleaseComponent.kt"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/reports/applet/ReportsAppletCommonModule;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\u0008\u00c1\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/reports/applet/ReportsAppletReleaseModule;",
        "",
        "()V",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/reports/applet/ReportsAppletReleaseModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/reports/applet/ReportsAppletReleaseModule;

    invoke-direct {v0}, Lcom/squareup/reports/applet/ReportsAppletReleaseModule;-><init>()V

    sput-object v0, Lcom/squareup/reports/applet/ReportsAppletReleaseModule;->INSTANCE:Lcom/squareup/reports/applet/ReportsAppletReleaseModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
