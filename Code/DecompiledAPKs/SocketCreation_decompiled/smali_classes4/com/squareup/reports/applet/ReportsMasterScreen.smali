.class public final Lcom/squareup/reports/applet/ReportsMasterScreen;
.super Lcom/squareup/reports/applet/InReportsAppletScope;
.source "ReportsMasterScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/container/layer/Master;
    applet = Lcom/squareup/reports/applet/ReportsApplet;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/reports/applet/ReportsMasterScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/ReportsMasterScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/reports/applet/ReportsMasterScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/reports/applet/ReportsMasterScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/reports/applet/ReportsMasterScreen;

    invoke-direct {v0}, Lcom/squareup/reports/applet/ReportsMasterScreen;-><init>()V

    sput-object v0, Lcom/squareup/reports/applet/ReportsMasterScreen;->INSTANCE:Lcom/squareup/reports/applet/ReportsMasterScreen;

    .line 44
    sget-object v0, Lcom/squareup/reports/applet/ReportsMasterScreen;->INSTANCE:Lcom/squareup/reports/applet/ReportsMasterScreen;

    .line 45
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/reports/applet/ReportsMasterScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/squareup/reports/applet/InReportsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 30
    sget-object p1, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 26
    sget v0, Lcom/squareup/reports/applet/R$layout;->reporting_applet_master_view:I

    return v0
.end method
