.class public Lcom/squareup/reports/applet/drawer/DrawerHistoryView;
.super Landroid/widget/FrameLayout;
.source "DrawerHistoryView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private adapter:Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;

.field appNameFormatter:Lcom/squareup/util/AppNameFormatter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private cashManagementDisabledStateMessage:Lcom/squareup/noho/NohoMessageView;

.field private final gutterHalf:I

.field private historyListContainer:Landroid/view/View;

.field protected listView:Landroid/widget/ListView;

.field private noHistoryNullStateContainer:Landroid/view/View;

.field private noHistoryNullStateSubtitle:Lcom/squareup/widgets/MessageView;

.field presenter:Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

.field private final underSectionHeader:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    const-class p2, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Component;->inject(Lcom/squareup/reports/applet/drawer/DrawerHistoryView;)V

    .line 49
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->gutterHalf:I

    .line 51
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_gap_section_header_below:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->underSectionHeader:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/reports/applet/drawer/DrawerHistoryView;)I
    .locals 0

    .line 29
    iget p0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->gutterHalf:I

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/reports/applet/drawer/DrawerHistoryView;)I
    .locals 0

    .line 29
    iget p0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->underSectionHeader:I

    return p0
.end method


# virtual methods
.method getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    return-object v0
.end method

.method hideSpinner()V
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    invoke-virtual {v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->hide()V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$DrawerHistoryView(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 61
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 62
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->presenter:Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->onRowClicked(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onFinishInflate$1$DrawerHistoryView()V
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->presenter:Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->onProgressHidden()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->presenter:Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 90
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 56
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 57
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 58
    sget v0, Lcom/squareup/reports/applet/R$id;->drawer_history_progress:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/DelayedLoadingProgressBar;

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    .line 59
    sget v0, Lcom/squareup/reports/applet/R$id;->drawer_history_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->listView:Landroid/widget/ListView;

    .line 60
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->listView:Landroid/widget/ListView;

    new-instance v1, Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerHistoryView$dcMA8nJwK_ZhDYCF7DbGKEHV0ZQ;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerHistoryView$dcMA8nJwK_ZhDYCF7DbGKEHV0ZQ;-><init>(Lcom/squareup/reports/applet/drawer/DrawerHistoryView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 66
    sget v0, Lcom/squareup/reports/applet/R$id;->drawer_history_list_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->historyListContainer:Landroid/view/View;

    .line 68
    sget v0, Lcom/squareup/reports/applet/R$id;->cash_management_settings_disabled_state_message:I

    .line 69
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoMessageView;

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->cashManagementDisabledStateMessage:Lcom/squareup/noho/NohoMessageView;

    .line 70
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->cashManagementDisabledStateMessage:Lcom/squareup/noho/NohoMessageView;

    .line 71
    invoke-virtual {v0}, Lcom/squareup/noho/NohoMessageView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/vectoricons/R$drawable;->device_cash_drawer_80:I

    sget v2, Lcom/squareup/noho/R$dimen;->noho_message_null_state_icon_height:I

    .line 70
    invoke-static {v0, v1, v2}, Lcom/squareup/noho/ResizeDrawable;->createFromResources(Landroid/content/Context;II)Lcom/squareup/noho/ResizeDrawable;

    move-result-object v0

    .line 75
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->cashManagementDisabledStateMessage:Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoMessageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 77
    sget v0, Lcom/squareup/reports/applet/R$id;->drawer_history_null_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->noHistoryNullStateContainer:Landroid/view/View;

    .line 78
    sget v0, Lcom/squareup/reports/applet/R$id;->drawer_history_null_state_subtitle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->noHistoryNullStateSubtitle:Lcom/squareup/widgets/MessageView;

    .line 79
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->noHistoryNullStateSubtitle:Lcom/squareup/widgets/MessageView;

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    sget v2, Lcom/squareup/reports/applet/R$string;->drawer_history_null_state_subtitle:I

    .line 80
    invoke-interface {v1, v2}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 79
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    new-instance v1, Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerHistoryView$G9cmeA2sH8JRGLvavo6rR64hlnk;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerHistoryView$G9cmeA2sH8JRGLvavo6rR64hlnk;-><init>(Lcom/squareup/reports/applet/drawer/DrawerHistoryView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/DelayedLoadingProgressBar;->setCallback(Lcom/squareup/ui/DelayedLoadingProgressBar$DelayedLoadingProgressBarCallback;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    invoke-virtual {v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->show()V

    .line 85
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->presenter:Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method setCursors(Lcom/squareup/cashmanagement/CashDrawerShiftCursor;Lcom/squareup/cashmanagement/CashDrawerShiftCursor;)V
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->adapter:Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;

    if-nez v0, :cond_0

    .line 107
    new-instance v0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;-><init>(Lcom/squareup/reports/applet/drawer/DrawerHistoryView;Lcom/squareup/cashmanagement/CashDrawerShiftCursor;Lcom/squareup/cashmanagement/CashDrawerShiftCursor;)V

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->adapter:Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;

    .line 108
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->listView:Landroid/widget/ListView;

    iget-object p2, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->adapter:Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;

    invoke-virtual {p1, p2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 110
    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView$DrawerHistoryListAdapter;->setCursors(Lcom/squareup/cashmanagement/CashDrawerShiftCursor;Lcom/squareup/cashmanagement/CashDrawerShiftCursor;)V

    :goto_0
    return-void
.end method

.method public showCashManagementDisabledState()V
    .locals 2

    .line 115
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->historyListContainer:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->cashManagementDisabledStateMessage:Lcom/squareup/noho/NohoMessageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setVisibility(I)V

    return-void
.end method

.method public showList()V
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->listView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    return-void
.end method

.method public showNoHistoryNullState()V
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->historyListContainer:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 121
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->noHistoryNullStateContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
