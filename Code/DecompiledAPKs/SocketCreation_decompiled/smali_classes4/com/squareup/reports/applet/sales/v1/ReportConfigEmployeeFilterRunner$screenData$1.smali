.class final Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$screenData$1;
.super Ljava/lang/Object;
.source "ReportConfigEmployeeFilterRunner.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->screenData()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;",
        "Lrx/Observable<",
        "TU;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u000e\u0010\u0004\u001a\n \u0003*\u0004\u0018\u00010\u00050\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Observable;",
        "",
        "kotlin.jvm.PlatformType",
        "state",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$screenData$1;->this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;

    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$screenData$1;->call(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 71
    instance-of p1, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState$InitialState;

    if-eqz p1, :cond_0

    .line 73
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$screenData$1;->this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

    invoke-static {p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->access$getSCREEN_DATA_DEBOUNCE_MS$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;)J

    move-result-wide v0

    sget-object p1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$screenData$1;->this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

    invoke-static {v2}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->access$getMainThread$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;)Lrx/Scheduler;

    move-result-object v2

    invoke-static {v0, v1, p1, v2}, Lrx/Observable;->timer(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 75
    :cond_0
    invoke-static {}, Lrx/Observable;->empty()Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
