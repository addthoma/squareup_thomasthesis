.class public Lcom/squareup/reports/applet/sales/v1/SalesReportRow;
.super Ljava/lang/Object;
.source "SalesReportRow.java"


# instance fields
.field public final rowText:Ljava/lang/String;

.field private final textColorOverride:Ljava/lang/Integer;

.field public final value:Ljava/lang/String;

.field public final weight:Lcom/squareup/marketfont/MarketFont$Weight;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/marketfont/MarketFont$Weight;)V
    .locals 1

    const/4 v0, 0x0

    .line 16
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/marketfont/MarketFont$Weight;Ljava/lang/Integer;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/marketfont/MarketFont$Weight;Ljava/lang/Integer;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;->rowText:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;->value:Ljava/lang/String;

    .line 23
    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 24
    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;->textColorOverride:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/ui/account/view/LineRow;)V
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;->value:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 29
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;->rowText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow;->setTitle(Ljava/lang/CharSequence;)V

    .line 30
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow;->setValueWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 32
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;->textColorOverride:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 33
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow;->setPreservedLabelTextColor(I)V

    .line 34
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;->textColorOverride:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow;->setValueColor(I)V

    :cond_0
    return-void
.end method
