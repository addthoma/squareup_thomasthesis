.class public Lcom/squareup/reports/applet/drawer/DrawerReportView;
.super Landroid/widget/LinearLayout;
.source "DrawerReportView.java"


# instance fields
.field presenter:Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public recyclerView:Landroidx/recyclerview/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    const-class p2, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Component;->inject(Lcom/squareup/reports/applet/drawer/DrawerReportView;)V

    return-void
.end method


# virtual methods
.method public hideKeyboard()V
    .locals 0

    .line 44
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportView;->presenter:Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 40
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 26
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 28
    sget v0, Lcom/squareup/reports/applet/R$id;->recycler:I

    invoke-virtual {p0, v0}, Lcom/squareup/reports/applet/drawer/DrawerReportView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 29
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 31
    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/DrawerReportView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 32
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->setOrientation(I)V

    .line 33
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportView;->presenter:Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
