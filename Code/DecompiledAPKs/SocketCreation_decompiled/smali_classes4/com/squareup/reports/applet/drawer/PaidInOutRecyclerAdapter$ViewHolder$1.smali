.class Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder$1;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "PaidInOutRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->bindStaticTopRowContent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;

.field final synthetic val$paidInButton:Lcom/squareup/ui/ConfirmButton;

.field final synthetic val$paidOutButton:Lcom/squareup/ui/ConfirmButton;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;Lcom/squareup/ui/ConfirmButton;Lcom/squareup/ui/ConfirmButton;)V
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder$1;->this$1:Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;

    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder$1;->val$paidInButton:Lcom/squareup/ui/ConfirmButton;

    iput-object p3, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder$1;->val$paidOutButton:Lcom/squareup/ui/ConfirmButton;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .line 148
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 149
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder$1;->this$1:Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;

    invoke-static {v0}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->access$000(Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;)Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->setAmount(Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder$1;->this$1:Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder$1;->val$paidInButton:Lcom/squareup/ui/ConfirmButton;

    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder$1;->val$paidOutButton:Lcom/squareup/ui/ConfirmButton;

    invoke-static {v0, v1, v2, p1}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;->access$100(Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter$ViewHolder;Lcom/squareup/ui/ConfirmButton;Lcom/squareup/ui/ConfirmButton;Ljava/lang/String;)V

    return-void
.end method
