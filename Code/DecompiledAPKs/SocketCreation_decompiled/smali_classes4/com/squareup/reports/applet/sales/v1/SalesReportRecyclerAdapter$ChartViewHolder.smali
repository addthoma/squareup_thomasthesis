.class Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;
.super Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;
.source "SalesReportRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChartViewHolder"
.end annotation


# static fields
.field private static final AXIS_TEXT_SIZE_DP:F = 14.0f

.field private static final CHART_ANIMATION_MS:I = 0x3e8

.field private static final MIN_BAR_DP_HEIGHT:I = 0x1

.field private static final X_AXIS_LINE_WIDTH_DP:I = 0x2


# instance fields
.field private final chart:Lcom/github/mikephil/charting/charts/BarChart;

.field private final header:Landroid/widget/TextView;

.field private final highlight:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;Landroid/view/View;)V
    .locals 5

    .line 418
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    .line 419
    invoke-direct {p0, p2}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 423
    sget v0, Lcom/squareup/reports/applet/R$id;->sales_chart_header:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->header:Landroid/widget/TextView;

    .line 424
    sget v0, Lcom/squareup/reports/applet/R$id;->sales_chart_highlight:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->highlight:Landroid/widget/TextView;

    .line 425
    sget v0, Lcom/squareup/reports/applet/R$id;->sales_chart:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/github/mikephil/charting/charts/BarChart;

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    .line 427
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/BarChart;->setBackgroundColor(I)V

    .line 428
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/BarChart;->setDrawGridBackground(Z)V

    .line 429
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/charts/BarChart;->setDescription(Ljava/lang/String;)V

    .line 430
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/BarChart;->setDrawBorders(Z)V

    .line 431
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarChart;->getLegend()Lcom/github/mikephil/charting/components/Legend;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/components/Legend;->setEnabled(Z)V

    .line 432
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/charts/BarChart;->setHighlightIndicatorEnabled(Z)V

    .line 433
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/charts/BarChart;->setHighlightEnabled(Z)V

    .line 434
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/charts/BarChart;->setTouchEnabled(Z)V

    .line 435
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/BarChart;->setDragEnabled(Z)V

    .line 436
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/BarChart;->setScaleXEnabled(Z)V

    .line 437
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/BarChart;->setScaleYEnabled(Z)V

    .line 438
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/BarChart;->setPinchZoom(Z)V

    .line 439
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/BarChart;->setDoubleTapToZoomEnabled(Z)V

    .line 440
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/charts/BarChart;->setHighlightPerDragEnabled(Z)V

    .line 441
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/BarChart;->setDrawMarkerViews(Z)V

    .line 443
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    new-instance v3, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;

    invoke-direct {v3, v0, v2}, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;-><init>(Lcom/github/mikephil/charting/charts/BarChart;I)V

    invoke-virtual {v0, v3}, Lcom/github/mikephil/charting/charts/BarChart;->setRenderer(Lcom/github/mikephil/charting/renderer/DataRenderer;)V

    .line 445
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 447
    iget-object v3, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/charts/BarChart;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v3

    .line 448
    sget-object v4, Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;->BOTTOM:Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;

    invoke-virtual {v3, v4}, Lcom/github/mikephil/charting/components/XAxis;->setPosition(Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;)V

    .line 449
    invoke-virtual {v3, v1}, Lcom/github/mikephil/charting/components/XAxis;->setDrawGridLines(Z)V

    .line 450
    invoke-virtual {v3, v2}, Lcom/github/mikephil/charting/components/XAxis;->setDrawLabels(Z)V

    .line 451
    invoke-virtual {v3, v0}, Lcom/github/mikephil/charting/components/XAxis;->setTypeface(Landroid/graphics/Typeface;)V

    .line 452
    invoke-static {p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$100(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)I

    move-result v2

    invoke-virtual {v3, v2}, Lcom/github/mikephil/charting/components/XAxis;->setAxisLineColor(I)V

    const/high16 v2, 0x3fc00000    # 1.5f

    .line 453
    invoke-virtual {v3, v2}, Lcom/github/mikephil/charting/components/XAxis;->setGridLineWidth(F)V

    .line 454
    invoke-static {p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$100(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)I

    move-result v2

    invoke-virtual {v3, v2}, Lcom/github/mikephil/charting/components/XAxis;->setGridColor(I)V

    const/high16 v2, 0x40000000    # 2.0f

    .line 455
    invoke-virtual {v3, v2}, Lcom/github/mikephil/charting/components/XAxis;->setAxisLineWidth(F)V

    const/high16 v2, 0x41600000    # 14.0f

    .line 456
    invoke-virtual {v3, v2}, Lcom/github/mikephil/charting/components/XAxis;->setTextSize(F)V

    .line 457
    invoke-static {p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$200(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/github/mikephil/charting/components/XAxis;->setTextColor(I)V

    .line 458
    iget-object v3, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    new-instance v4, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder$1;

    invoke-direct {v4, p0, p1, p2}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder$1;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;Landroid/view/View;)V

    invoke-virtual {v3, v4}, Lcom/github/mikephil/charting/charts/BarChart;->setOnChartValueSelectedListener(Lcom/github/mikephil/charting/listener/OnChartValueSelectedListener;)V

    .line 482
    invoke-static {p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$000(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getMoneyValueFormatter()Lcom/github/mikephil/charting/utils/ValueFormatter;

    move-result-object p2

    .line 483
    iget-object v3, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/charts/BarChart;->getAxisLeft()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v3

    .line 484
    invoke-static {p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$200(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)I

    move-result p1

    invoke-virtual {v3, p1}, Lcom/github/mikephil/charting/components/YAxis;->setTextColor(I)V

    .line 485
    invoke-virtual {v3, p2}, Lcom/github/mikephil/charting/components/YAxis;->setValueFormatter(Lcom/github/mikephil/charting/utils/ValueFormatter;)V

    .line 486
    invoke-virtual {v3, v2}, Lcom/github/mikephil/charting/components/YAxis;->setTextSize(F)V

    .line 487
    invoke-virtual {v3, v0}, Lcom/github/mikephil/charting/components/YAxis;->setTypeface(Landroid/graphics/Typeface;)V

    .line 488
    invoke-virtual {v3, v1}, Lcom/github/mikephil/charting/components/YAxis;->setDrawAxisLine(Z)V

    .line 489
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    invoke-virtual {p1}, Lcom/github/mikephil/charting/charts/BarChart;->getAxisRight()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/github/mikephil/charting/components/YAxis;->setEnabled(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;)Landroid/widget/TextView;
    .locals 0

    .line 407
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->highlight:Landroid/widget/TextView;

    return-object p0
.end method


# virtual methods
.method public bind()V
    .locals 3

    .line 493
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    invoke-static {v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$600(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Lcom/github/mikephil/charting/data/BarData;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/charts/BarChart;->getData()Lcom/github/mikephil/charting/data/ChartData;

    move-result-object v1

    if-eq v0, v1, :cond_3

    .line 494
    sget-object v0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$1;->$SwitchMap$com$squareup$protos$beemo$api$v3$reporting$GroupByType:[I

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    invoke-static {v1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$700(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 502
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->header:Landroid/widget/TextView;

    sget v1, Lcom/squareup/reports/applet/R$string;->uppercase_sales_report_monthly_sales:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 505
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected group by type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    invoke-static {v2}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$700(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 499
    :cond_1
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->header:Landroid/widget/TextView;

    sget v1, Lcom/squareup/reports/applet/R$string;->uppercase_sales_report_daily_sales:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 496
    :cond_2
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->header:Landroid/widget/TextView;

    sget v1, Lcom/squareup/reports/applet/R$string;->uppercase_sales_report_hourly_sales:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 508
    :goto_0
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    invoke-static {v1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$600(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Lcom/github/mikephil/charting/data/BarData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/BarChart;->setData(Lcom/github/mikephil/charting/data/ChartData;)V

    .line 509
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/BarChart;->highlightTouch(Lcom/github/mikephil/charting/utils/Highlight;)V

    .line 510
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/charts/BarChart;->animateY(I)V

    .line 511
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;->chart:Lcom/github/mikephil/charting/charts/BarChart;

    invoke-virtual {v0}, Lcom/github/mikephil/charting/charts/BarChart;->resetViewPortOffsets()V

    :cond_3
    return-void
.end method
