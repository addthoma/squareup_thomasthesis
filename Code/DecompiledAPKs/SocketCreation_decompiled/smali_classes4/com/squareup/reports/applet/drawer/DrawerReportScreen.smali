.class public final Lcom/squareup/reports/applet/drawer/DrawerReportScreen;
.super Lcom/squareup/reports/applet/InReportsAppletScope;
.source "DrawerReportScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Component;,
        Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/reports/applet/drawer/DrawerReportScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final shiftId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 167
    sget-object v0, Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerReportScreen$-_kK8OdP8C9gUDhrygQjpYpfn2o;->INSTANCE:Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerReportScreen$-_kK8OdP8C9gUDhrygQjpYpfn2o;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 46
    invoke-direct {p0}, Lcom/squareup/reports/applet/InReportsAppletScope;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen;->shiftId:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/reports/applet/drawer/DrawerReportScreen;)Ljava/lang/String;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen;->shiftId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/reports/applet/drawer/DrawerReportScreen;
    .locals 1

    .line 168
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 169
    new-instance v0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/drawer/DrawerReportScreen;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 164
    iget-object p2, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen;->shiftId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 51
    const-class v0, Lcom/squareup/reports/applet/drawer/DrawerHistorySection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 173
    sget v0, Lcom/squareup/reports/applet/R$layout;->drawer_report_main_view:I

    return v0
.end method
