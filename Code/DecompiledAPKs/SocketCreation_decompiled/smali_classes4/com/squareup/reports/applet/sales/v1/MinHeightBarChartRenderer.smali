.class public Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;
.super Lcom/github/mikephil/charting/renderer/BarChartRenderer;
.source "MinHeightBarChartRenderer.java"


# instance fields
.field private final minPxHeight:F


# direct methods
.method public constructor <init>(Lcom/github/mikephil/charting/charts/BarChart;I)V
    .locals 2

    .line 20
    invoke-virtual {p1}, Lcom/github/mikephil/charting/charts/BarChart;->getAnimator()Lcom/github/mikephil/charting/animation/ChartAnimator;

    move-result-object v0

    invoke-virtual {p1}, Lcom/github/mikephil/charting/charts/BarChart;->getViewPortHandler()Lcom/github/mikephil/charting/utils/ViewPortHandler;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/github/mikephil/charting/renderer/BarChartRenderer;-><init>(Lcom/github/mikephil/charting/interfaces/BarDataProvider;Lcom/github/mikephil/charting/animation/ChartAnimator;Lcom/github/mikephil/charting/utils/ViewPortHandler;)V

    int-to-float p1, p2

    .line 21
    invoke-static {p1}, Lcom/github/mikephil/charting/utils/Utils;->convertDpToPixel(F)F

    move-result p1

    iput p1, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->minPxHeight:F

    return-void
.end method


# virtual methods
.method protected drawDataSet(Landroid/graphics/Canvas;Lcom/github/mikephil/charting/data/BarDataSet;I)V
    .locals 12

    .line 26
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/BarDataProvider;

    invoke-virtual {p2}, Lcom/github/mikephil/charting/data/BarDataSet;->getAxisDependency()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/github/mikephil/charting/interfaces/BarDataProvider;->getTransformer(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Lcom/github/mikephil/charting/utils/Transformer;

    move-result-object v0

    .line 28
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mShadowPaint:Landroid/graphics/Paint;

    invoke-virtual {p2}, Lcom/github/mikephil/charting/data/BarDataSet;->getBarShadowColor()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 30
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mAnimator:Lcom/github/mikephil/charting/animation/ChartAnimator;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/animation/ChartAnimator;->getPhaseX()F

    move-result v1

    .line 31
    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mAnimator:Lcom/github/mikephil/charting/animation/ChartAnimator;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/animation/ChartAnimator;->getPhaseY()F

    move-result v2

    .line 33
    invoke-virtual {p2}, Lcom/github/mikephil/charting/data/BarDataSet;->getYVals()Ljava/util/List;

    move-result-object v3

    .line 36
    iget-object v4, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mBarBuffers:[Lcom/github/mikephil/charting/buffer/BarBuffer;

    aget-object v4, v4, p3

    .line 37
    invoke-virtual {v4, v1, v2}, Lcom/github/mikephil/charting/buffer/BarBuffer;->setPhases(FF)V

    .line 38
    invoke-virtual {p2}, Lcom/github/mikephil/charting/data/BarDataSet;->getBarSpace()F

    move-result v1

    invoke-virtual {v4, v1}, Lcom/github/mikephil/charting/buffer/BarBuffer;->setBarSpace(F)V

    .line 39
    invoke-virtual {v4, p3}, Lcom/github/mikephil/charting/buffer/BarBuffer;->setDataSet(I)V

    .line 40
    iget-object p3, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/BarDataProvider;

    invoke-virtual {p2}, Lcom/github/mikephil/charting/data/BarDataSet;->getAxisDependency()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v1

    invoke-interface {p3, v1}, Lcom/github/mikephil/charting/interfaces/BarDataProvider;->isInverted(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Z

    move-result p3

    invoke-virtual {v4, p3}, Lcom/github/mikephil/charting/buffer/BarBuffer;->setInverted(Z)V

    .line 42
    invoke-virtual {v4, v3}, Lcom/github/mikephil/charting/buffer/BarBuffer;->feed(Ljava/util/List;)V

    .line 44
    iget-object p3, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    invoke-virtual {v0, p3}, Lcom/github/mikephil/charting/utils/Transformer;->pointValuesToPixel([F)V

    .line 47
    invoke-virtual {p2}, Lcom/github/mikephil/charting/data/BarDataSet;->getColors()Ljava/util/List;

    move-result-object p3

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p3

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-le p3, v1, :cond_4

    .line 49
    :goto_0
    invoke-virtual {v4}, Lcom/github/mikephil/charting/buffer/BarBuffer;->size()I

    move-result p3

    if-ge v0, p3, :cond_9

    .line 51
    iget-object p3, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    iget-object v1, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    add-int/lit8 v2, v0, 0x2

    aget v1, v1, v2

    invoke-virtual {p3, v1}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->isInBoundsLeft(F)Z

    move-result p3

    if-nez p3, :cond_0

    goto :goto_1

    .line 53
    :cond_0
    iget-object p3, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    iget-object v1, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    aget v1, v1, v0

    invoke-virtual {p3, v1}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->isInBoundsRight(F)Z

    move-result p3

    if-nez p3, :cond_1

    goto/16 :goto_4

    .line 55
    :cond_1
    iget-object p3, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/BarDataProvider;

    invoke-interface {p3}, Lcom/github/mikephil/charting/interfaces/BarDataProvider;->isDrawBarShadowEnabled()Z

    move-result p3

    if-eqz p3, :cond_2

    .line 56
    iget-object p3, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    aget v6, p3, v0

    iget-object p3, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    invoke-virtual {p3}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->contentTop()F

    move-result v7

    iget-object p3, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    aget v8, p3, v2

    iget-object p3, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    .line 57
    invoke-virtual {p3}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->contentBottom()F

    move-result v9

    iget-object v10, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mShadowPaint:Landroid/graphics/Paint;

    move-object v5, p1

    .line 56
    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 62
    :cond_2
    iget-object p3, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    div-int/lit8 v1, v0, 0x4

    invoke-virtual {p2, v1}, Lcom/github/mikephil/charting/data/BarDataSet;->getColor(I)I

    move-result v1

    invoke-virtual {p3, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 65
    iget-object p3, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    add-int/lit8 v1, v0, 0x3

    aget p3, p3, v1

    iget-object v3, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    add-int/lit8 v5, v0, 0x1

    aget v3, v3, v5

    sub-float/2addr p3, v3

    iget v3, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->minPxHeight:F

    cmpg-float p3, p3, v3

    if-gtz p3, :cond_3

    .line 66
    iget-object p3, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    iget-object v3, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    aget v3, v3, v1

    iget v6, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->minPxHeight:F

    sub-float/2addr v3, v6

    aput v3, p3, v5

    .line 69
    :cond_3
    iget-object p3, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    aget v7, p3, v0

    iget-object p3, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    aget v8, p3, v5

    iget-object p3, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    aget v9, p3, v2

    iget-object p3, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    aget v10, p3, v1

    iget-object v11, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    move-object v6, p1

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :goto_1
    add-int/lit8 v0, v0, 0x4

    goto/16 :goto_0

    .line 74
    :cond_4
    iget-object p3, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual {p2}, Lcom/github/mikephil/charting/data/BarDataSet;->getColor()I

    move-result p2

    invoke-virtual {p3, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 76
    :goto_2
    invoke-virtual {v4}, Lcom/github/mikephil/charting/buffer/BarBuffer;->size()I

    move-result p2

    if-ge v0, p2, :cond_9

    .line 78
    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    iget-object p3, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    add-int/lit8 v1, v0, 0x2

    aget p3, p3, v1

    invoke-virtual {p2, p3}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->isInBoundsLeft(F)Z

    move-result p2

    if-nez p2, :cond_5

    goto :goto_3

    .line 80
    :cond_5
    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    iget-object p3, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    aget p3, p3, v0

    invoke-virtual {p2, p3}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->isInBoundsRight(F)Z

    move-result p2

    if-nez p2, :cond_6

    goto :goto_4

    .line 82
    :cond_6
    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/BarDataProvider;

    invoke-interface {p2}, Lcom/github/mikephil/charting/interfaces/BarDataProvider;->isDrawBarShadowEnabled()Z

    move-result p2

    if-eqz p2, :cond_7

    .line 83
    iget-object p2, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    aget v6, p2, v0

    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    invoke-virtual {p2}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->contentTop()F

    move-result v7

    iget-object p2, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    aget v8, p2, v1

    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    .line 84
    invoke-virtual {p2}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->contentBottom()F

    move-result v9

    iget-object v10, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mShadowPaint:Landroid/graphics/Paint;

    move-object v5, p1

    .line 83
    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 88
    :cond_7
    iget-object p2, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    add-int/lit8 p3, v0, 0x3

    aget p2, p2, p3

    iget-object v2, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    add-int/lit8 v3, v0, 0x1

    aget v2, v2, v3

    sub-float/2addr p2, v2

    iget v2, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->minPxHeight:F

    cmpg-float p2, p2, v2

    if-gtz p2, :cond_8

    .line 89
    iget-object p2, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    iget-object v2, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    aget v2, v2, p3

    iget v5, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->minPxHeight:F

    sub-float/2addr v2, v5

    aput v2, p2, v3

    .line 92
    :cond_8
    iget-object p2, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    aget v6, p2, v0

    iget-object p2, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    aget v7, p2, v3

    iget-object p2, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    aget v8, p2, v1

    iget-object p2, v4, Lcom/github/mikephil/charting/buffer/BarBuffer;->buffer:[F

    aget v9, p2, p3

    iget-object v10, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    move-object v5, p1

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :goto_3
    add-int/lit8 v0, v0, 0x4

    goto :goto_2

    :cond_9
    :goto_4
    return-void
.end method

.method protected prepareBarHighlight(FFFFLcom/github/mikephil/charting/utils/Transformer;)V
    .locals 0

    .line 100
    invoke-super/range {p0 .. p5}, Lcom/github/mikephil/charting/renderer/BarChartRenderer;->prepareBarHighlight(FFFFLcom/github/mikephil/charting/utils/Transformer;)V

    .line 101
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mBarRect:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result p1

    iget p2, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->minPxHeight:F

    cmpg-float p1, p1, p2

    if-gez p1, :cond_0

    .line 102
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mBarRect:Landroid/graphics/RectF;

    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->mBarRect:Landroid/graphics/RectF;

    iget p2, p2, Landroid/graphics/RectF;->bottom:F

    iget p3, p0, Lcom/squareup/reports/applet/sales/v1/MinHeightBarChartRenderer;->minPxHeight:F

    sub-float/2addr p2, p3

    iput p2, p1, Landroid/graphics/RectF;->top:F

    :cond_0
    return-void
.end method
