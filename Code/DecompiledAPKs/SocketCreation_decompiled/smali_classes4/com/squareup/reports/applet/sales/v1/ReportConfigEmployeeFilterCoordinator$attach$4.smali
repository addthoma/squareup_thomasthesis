.class final Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$attach$4;
.super Ljava/lang/Object;
.source "ReportConfigEmployeeFilterCoordinator.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "screenData",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $adapter:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter;

.field final synthetic this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$attach$4;->this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;

    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$attach$4;->$adapter:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;)V
    .locals 4

    .line 65
    invoke-virtual {p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;->getShowLoading()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$attach$4;->this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;

    invoke-static {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->access$getViewAnimator$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;)Lcom/squareup/widgets/SquareViewAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$attach$4;->this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;

    invoke-static {v1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->access$getProgressBar$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    goto :goto_0

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$attach$4;->this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;

    invoke-static {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->access$getViewAnimator$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;)Lcom/squareup/widgets/SquareViewAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$attach$4;->this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;

    invoke-static {v1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->access$getEmployeeFilterView$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 70
    :goto_0
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$attach$4;->this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;

    invoke-static {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->access$getAllEmployeesButton$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;)Lcom/squareup/widgets/list/ToggleButtonRow;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;->getAllEmployeesButtonChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 71
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$attach$4;->this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;

    invoke-static {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->access$getFilterByEmployeesButton$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;)Lcom/squareup/widgets/list/ToggleButtonRow;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;->getFilterByEmployeesButtonChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 72
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$attach$4;->this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;

    invoke-static {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->access$getEmployeeListHeader$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;)Lcom/squareup/marketfont/MarketTextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;->getEmployeeListHeaderVisible()Z

    move-result v1

    const/4 v2, 0x0

    const/16 v3, 0x8

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    const/16 v1, 0x8

    :goto_1
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 73
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$attach$4;->this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;

    invoke-static {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;->access$getEmployeeList$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;)Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;->getEmployeeListVisible()Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_2

    :cond_2
    const/16 v2, 0x8

    :goto_2
    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V

    .line 75
    instance-of v0, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$FilterByEmployee;

    if-eqz v0, :cond_3

    .line 76
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$attach$4;->$adapter:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter;

    check-cast p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$FilterByEmployee;

    invoke-virtual {v0, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewAdapter;->setEmployees(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$FilterByEmployee;)V

    :cond_3
    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$attach$4;->call(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;)V

    return-void
.end method
