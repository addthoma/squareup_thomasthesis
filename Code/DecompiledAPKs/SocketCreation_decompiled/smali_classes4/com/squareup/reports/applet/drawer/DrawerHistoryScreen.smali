.class public final Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen;
.super Lcom/squareup/reports/applet/InReportsAppletScope;
.source "DrawerHistoryScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Component;,
        Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen;

    invoke-direct {v0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen;-><init>()V

    sput-object v0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen;->INSTANCE:Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen;

    .line 144
    sget-object v0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen;->INSTANCE:Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/squareup/reports/applet/InReportsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 41
    const-class v0, Lcom/squareup/reports/applet/drawer/DrawerHistorySection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 147
    sget v0, Lcom/squareup/reports/applet/R$layout;->drawer_history_view:I

    return v0
.end method
