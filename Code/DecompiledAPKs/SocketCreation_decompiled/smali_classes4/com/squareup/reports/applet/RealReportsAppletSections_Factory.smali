.class public final Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;
.super Ljava/lang/Object;
.source "RealReportsAppletSections_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/reports/applet/RealReportsAppletSections;",
        ">;"
    }
.end annotation


# instance fields
.field private final cashDrawerShiftManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
            ">;"
        }
    .end annotation
.end field

.field private final currentDrawerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;",
            ">;"
        }
    .end annotation
.end field

.field private final disputesSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/disputes/DisputesSection;",
            ">;"
        }
    .end annotation
.end field

.field private final drawerHistoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/DrawerHistorySection;",
            ">;"
        }
    .end annotation
.end field

.field private final handlesDisputesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/api/HandlesDisputes;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyReportSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final salesReportSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/SalesReportSection;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/DrawerHistorySection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/SalesReportSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/disputes/DisputesSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/api/HandlesDisputes;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;->currentDrawerProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p2, p0, Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;->drawerHistoryProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p3, p0, Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;->salesReportSectionProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p4, p0, Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;->loyaltyReportSectionProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p5, p0, Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;->cashDrawerShiftManagerProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p6, p0, Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;->disputesSectionProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p7, p0, Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;->handlesDisputesProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p8, p0, Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/DrawerHistorySection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/SalesReportSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/disputes/DisputesSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/api/HandlesDisputes;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;"
        }
    .end annotation

    .line 70
    new-instance v9, Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;Lcom/squareup/reports/applet/drawer/DrawerHistorySection;Lcom/squareup/reports/applet/sales/SalesReportSection;Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/reports/applet/disputes/DisputesSection;Lcom/squareup/disputes/api/HandlesDisputes;Lcom/squareup/util/Res;)Lcom/squareup/reports/applet/RealReportsAppletSections;
    .locals 10

    .line 78
    new-instance v9, Lcom/squareup/reports/applet/RealReportsAppletSections;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/reports/applet/RealReportsAppletSections;-><init>(Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;Lcom/squareup/reports/applet/drawer/DrawerHistorySection;Lcom/squareup/reports/applet/sales/SalesReportSection;Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/reports/applet/disputes/DisputesSection;Lcom/squareup/disputes/api/HandlesDisputes;Lcom/squareup/util/Res;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/reports/applet/RealReportsAppletSections;
    .locals 9

    .line 59
    iget-object v0, p0, Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;->currentDrawerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;

    iget-object v0, p0, Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;->drawerHistoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/reports/applet/drawer/DrawerHistorySection;

    iget-object v0, p0, Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;->salesReportSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/reports/applet/sales/SalesReportSection;

    iget-object v0, p0, Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;->loyaltyReportSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;

    iget-object v0, p0, Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;->cashDrawerShiftManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    iget-object v0, p0, Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;->disputesSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/reports/applet/disputes/DisputesSection;

    iget-object v0, p0, Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;->handlesDisputesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/disputes/api/HandlesDisputes;

    iget-object v0, p0, Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/util/Res;

    invoke-static/range {v1 .. v8}, Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;->newInstance(Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;Lcom/squareup/reports/applet/drawer/DrawerHistorySection;Lcom/squareup/reports/applet/sales/SalesReportSection;Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/reports/applet/disputes/DisputesSection;Lcom/squareup/disputes/api/HandlesDisputes;Lcom/squareup/util/Res;)Lcom/squareup/reports/applet/RealReportsAppletSections;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/reports/applet/RealReportsAppletSections_Factory;->get()Lcom/squareup/reports/applet/RealReportsAppletSections;

    move-result-object v0

    return-object v0
.end method
