.class public final Lcom/squareup/reports/applet/ReportsDeepLinkHandler;
.super Ljava/lang/Object;
.source "ReportsDeepLinkHandler.kt"

# interfaces
.implements Lcom/squareup/deeplinks/DeepLinkHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/ReportsDeepLinkHandler$ReportsHistoryFactory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReportsDeepLinkHandler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReportsDeepLinkHandler.kt\ncom/squareup/reports/applet/ReportsDeepLinkHandler\n*L\n1#1,105:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u000fB\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/reports/applet/ReportsDeepLinkHandler;",
        "Lcom/squareup/deeplinks/DeepLinkHandler;",
        "disputesSection",
        "Lcom/squareup/reports/applet/disputes/DisputesSection;",
        "salesReportSection",
        "Lcom/squareup/reports/applet/sales/SalesReportSection;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "salesReportDetailLevelHolder",
        "Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;",
        "(Lcom/squareup/reports/applet/disputes/DisputesSection;Lcom/squareup/reports/applet/sales/SalesReportSection;Lcom/squareup/settings/server/Features;Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;)V",
        "handleExternal",
        "Lcom/squareup/deeplinks/DeepLinkResult;",
        "uri",
        "Landroid/net/Uri;",
        "ReportsHistoryFactory",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final disputesSection:Lcom/squareup/reports/applet/disputes/DisputesSection;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final salesReportDetailLevelHolder:Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;

.field private final salesReportSection:Lcom/squareup/reports/applet/sales/SalesReportSection;


# direct methods
.method public constructor <init>(Lcom/squareup/reports/applet/disputes/DisputesSection;Lcom/squareup/reports/applet/sales/SalesReportSection;Lcom/squareup/settings/server/Features;Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "disputesSection"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "salesReportSection"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "salesReportDetailLevelHolder"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/reports/applet/ReportsDeepLinkHandler;->disputesSection:Lcom/squareup/reports/applet/disputes/DisputesSection;

    iput-object p2, p0, Lcom/squareup/reports/applet/ReportsDeepLinkHandler;->salesReportSection:Lcom/squareup/reports/applet/sales/SalesReportSection;

    iput-object p3, p0, Lcom/squareup/reports/applet/ReportsDeepLinkHandler;->features:Lcom/squareup/settings/server/Features;

    iput-object p4, p0, Lcom/squareup/reports/applet/ReportsDeepLinkHandler;->salesReportDetailLevelHolder:Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;

    return-void
.end method


# virtual methods
.method public handleExternal(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 3

    const-string v0, "uri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-static {p1}, Lcom/squareup/reports/applet/ReportsDeepLinkHandlerKt;->access$isReports$p(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    goto/16 :goto_2

    .line 39
    :cond_0
    invoke-static {p1}, Lcom/squareup/reports/applet/ReportsDeepLinkHandlerKt;->access$getNavigateToTopReports$p(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    new-instance v0, Lcom/squareup/reports/applet/ReportsDeepLinkHandler$ReportsHistoryFactory;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/reports/applet/ReportsDeepLinkHandler$ReportsHistoryFactory;-><init>(Lcom/squareup/container/ContainerTreeKey;)V

    check-cast v0, Lcom/squareup/ui/main/HistoryFactory;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    goto/16 :goto_2

    .line 41
    :cond_1
    invoke-static {p1}, Lcom/squareup/reports/applet/ReportsDeepLinkHandlerKt;->access$getNavigateToDisputes$p(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsDeepLinkHandler;->disputesSection:Lcom/squareup/reports/applet/disputes/DisputesSection;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/disputes/DisputesSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 42
    new-instance v0, Lcom/squareup/deeplinks/DeepLinkResult;

    new-instance v1, Lcom/squareup/reports/applet/ReportsDeepLinkHandler$ReportsHistoryFactory;

    new-instance v2, Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;

    invoke-static {p1}, Lcom/squareup/reports/applet/ReportsDeepLinkHandlerKt;->access$getDisputePaymentToken$p(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, p1}, Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;-><init>(Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/container/ContainerTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/reports/applet/ReportsDeepLinkHandler$ReportsHistoryFactory;-><init>(Lcom/squareup/container/ContainerTreeKey;)V

    check-cast v1, Lcom/squareup/ui/main/HistoryFactory;

    invoke-direct {v0, v1}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    :goto_0
    move-object p1, v0

    goto :goto_2

    .line 44
    :cond_2
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->INACCESSIBLE:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    goto :goto_2

    .line 47
    :cond_3
    invoke-static {p1}, Lcom/squareup/reports/applet/ReportsDeepLinkHandlerKt;->access$getNavigateToSales$p(Landroid/net/Uri;)Z

    move-result p1

    if-eqz p1, :cond_6

    iget-object p1, p0, Lcom/squareup/reports/applet/ReportsDeepLinkHandler;->salesReportSection:Lcom/squareup/reports/applet/sales/SalesReportSection;

    invoke-virtual {p1}, Lcom/squareup/reports/applet/sales/SalesReportSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 48
    iget-object p1, p0, Lcom/squareup/reports/applet/ReportsDeepLinkHandler;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->SALES_REPORT_V2:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 52
    iget-object p1, p0, Lcom/squareup/reports/applet/ReportsDeepLinkHandler;->salesReportDetailLevelHolder:Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;

    invoke-interface {p1}, Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;->revokePermissionForPinnedEmployee()V

    .line 53
    new-instance p1, Lcom/squareup/reports/applet/sales/v2/SalesReportBootstrapScreen;

    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsDeepLinkHandler;->salesReportDetailLevelHolder:Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;

    invoke-interface {v0}, Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;->getDetailLevel()Lcom/squareup/api/salesreport/DetailLevel;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/reports/applet/sales/v2/SalesReportBootstrapScreen;-><init>(Lcom/squareup/api/salesreport/DetailLevel;)V

    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    goto :goto_1

    .line 55
    :cond_4
    sget-object p1, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen;

    const-string v0, "SalesReportScreen.INSTANCE"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    .line 58
    :goto_1
    new-instance v0, Lcom/squareup/deeplinks/DeepLinkResult;

    new-instance v1, Lcom/squareup/reports/applet/ReportsDeepLinkHandler$ReportsHistoryFactory;

    invoke-direct {v1, p1}, Lcom/squareup/reports/applet/ReportsDeepLinkHandler$ReportsHistoryFactory;-><init>(Lcom/squareup/container/ContainerTreeKey;)V

    check-cast v1, Lcom/squareup/ui/main/HistoryFactory;

    invoke-direct {v0, v1}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    goto :goto_0

    .line 60
    :cond_5
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->INACCESSIBLE:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    goto :goto_2

    .line 62
    :cond_6
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    :goto_2
    return-object p1
.end method
