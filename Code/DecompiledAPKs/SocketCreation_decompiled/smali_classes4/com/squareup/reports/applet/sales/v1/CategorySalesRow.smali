.class public Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;
.super Ljava/lang/Object;
.source "CategorySalesRow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;
    }
.end annotation


# instance fields
.field public final formattedMeasurementUnit:Ljava/lang/String;

.field public final formattedMoney:Ljava/lang/String;

.field public final formattedName:Ljava/lang/String;

.field private opened:Z

.field public final quantity:Ljava/math/BigDecimal;

.field public final rowType:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;


# direct methods
.method public constructor <init>(Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->rowType:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    .line 29
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->formattedName:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->formattedMoney:Ljava/lang/String;

    .line 31
    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->quantity:Ljava/math/BigDecimal;

    .line 32
    iput-object p5, p0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->formattedMeasurementUnit:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public isOpened()Z
    .locals 1

    .line 36
    iget-boolean v0, p0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->opened:Z

    return v0
.end method

.method public setOpened(Z)V
    .locals 0

    .line 40
    iput-boolean p1, p0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->opened:Z

    return-void
.end method
