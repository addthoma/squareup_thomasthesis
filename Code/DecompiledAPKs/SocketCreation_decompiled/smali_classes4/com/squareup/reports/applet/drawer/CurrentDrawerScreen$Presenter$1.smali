.class Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "CurrentDrawerScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->endDrawerClicked()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;)V
    .locals 0

    .line 273
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter$1;->this$0:Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 2

    .line 275
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter$1;->this$0:Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->access$000(Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;)Lcom/squareup/permissions/EmployeeManagement;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter$1;->getAuthorizedEmployeeToken()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/permissions/EmployeeManagement;->getEmployeeByToken(Ljava/lang/String;)Lcom/squareup/permissions/Employee;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 276
    sget-object v1, Lcom/squareup/permissions/Permission;->VIEW_EXPECTED_IN_CASH_DRAWER:Lcom/squareup/permissions/Permission;

    .line 277
    invoke-static {v1}, Lkotlin/collections/SetsKt;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/Employee;->hasAnyPermission(Ljava/util/Set;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 278
    :goto_0
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter$1;->this$0:Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;

    invoke-static {v1, v0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->access$100(Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;Z)V

    return-void
.end method
