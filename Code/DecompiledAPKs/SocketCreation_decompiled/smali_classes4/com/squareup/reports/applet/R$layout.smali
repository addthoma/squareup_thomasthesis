.class public final Lcom/squareup/reports/applet/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final add_paid_in_out_view:I = 0x7f0d004b

.field public static final cash_drawer_details:I = 0x7f0d00d0

.field public static final cash_management_disabled_state:I = 0x7f0d00d2

.field public static final cash_management_settings_card_view:I = 0x7f0d00d4

.field public static final current_drawer_in_progress_state:I = 0x7f0d01a4

.field public static final current_drawer_loading_state:I = 0x7f0d01a5

.field public static final current_drawer_start_state:I = 0x7f0d01a6

.field public static final current_drawer_view:I = 0x7f0d01a7

.field public static final drawer_email_card_view:I = 0x7f0d01e7

.field public static final drawer_history_cash_drawer_details:I = 0x7f0d01e8

.field public static final drawer_history_null_state:I = 0x7f0d01e9

.field public static final drawer_history_paid_in_out_title:I = 0x7f0d01ea

.field public static final drawer_history_row:I = 0x7f0d01eb

.field public static final drawer_history_view:I = 0x7f0d01ec

.field public static final drawer_report_main_view:I = 0x7f0d01ed

.field public static final email_sheet_view:I = 0x7f0d0246

.field public static final end_current_drawer_card_view:I = 0x7f0d0259

.field public static final paid_in_out_event_row:I = 0x7f0d041b

.field public static final paid_in_out_main_view:I = 0x7f0d041c

.field public static final paid_in_out_total_row:I = 0x7f0d041d

.field public static final report_config_card_view:I = 0x7f0d047d

.field public static final report_config_employee_filter_card_view:I = 0x7f0d047e

.field public static final report_config_employees_row:I = 0x7f0d047f

.field public static final report_email_card_view:I = 0x7f0d0480

.field public static final reporting_applet_master_view:I = 0x7f0d0481

.field public static final reports_applet_list_row:I = 0x7f0d0482

.field public static final sales_report_category_row:I = 0x7f0d0492

.field public static final sales_report_category_sales_header:I = 0x7f0d0493

.field public static final sales_report_chart:I = 0x7f0d0494

.field public static final sales_report_customize_details:I = 0x7f0d0496

.field public static final sales_report_discounts_header:I = 0x7f0d0497

.field public static final sales_report_discounts_row:I = 0x7f0d0498

.field public static final sales_report_divider:I = 0x7f0d0499

.field public static final sales_report_divider_large_top:I = 0x7f0d049a

.field public static final sales_report_item_row:I = 0x7f0d049c

.field public static final sales_report_item_variation_row:I = 0x7f0d049d

.field public static final sales_report_overview:I = 0x7f0d049e

.field public static final sales_report_sales_summary_header:I = 0x7f0d04a3

.field public static final sales_report_sales_summary_payments_table_header:I = 0x7f0d04a4

.field public static final sales_report_sales_summary_row:I = 0x7f0d04a5

.field public static final sales_report_sales_summary_sales_table_header:I = 0x7f0d04a6

.field public static final sales_report_view_v1:I = 0x7f0d04b1

.field public static final section_title_row:I = 0x7f0d04b7


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
