.class Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter$OverviewAmounts;
.super Ljava/lang/Object;
.source "SalesReportScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "OverviewAmounts"
.end annotation


# instance fields
.field final formattedAverageSalesMoney:Ljava/lang/String;

.field final formattedCoverCount:Ljava/lang/String;

.field final formattedGrossSalesMoney:Ljava/lang/String;

.field final formattedSalesCount:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 440
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 441
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter$OverviewAmounts;->formattedGrossSalesMoney:Ljava/lang/String;

    .line 442
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter$OverviewAmounts;->formattedSalesCount:Ljava/lang/String;

    .line 443
    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter$OverviewAmounts;->formattedAverageSalesMoney:Ljava/lang/String;

    .line 444
    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter$OverviewAmounts;->formattedCoverCount:Ljava/lang/String;

    return-void
.end method
