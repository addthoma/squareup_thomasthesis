.class public Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;
.super Ljava/lang/Object;
.source "SalesSummaryReport.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;
    }
.end annotation


# instance fields
.field public final categoryRows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;",
            ">;"
        }
    .end annotation
.end field

.field public final chartData:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;

.field public final config:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

.field public final discountRows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;",
            ">;"
        }
    .end annotation
.end field

.field public final discounts:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

.field public final hasTransactions:Z

.field public final paymentMethods:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

.field public final paymentsRows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/SalesReportRow;",
            ">;"
        }
    .end annotation
.end field

.field public final salesRows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/SalesReportRow;",
            ">;"
        }
    .end annotation
.end field

.field public final salesSummary:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;


# direct methods
.method private constructor <init>(Lcom/squareup/reports/applet/sales/v1/ReportConfig;ZLcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            "Z",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/SalesReportRow;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/SalesReportRow;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;",
            ">;",
            "Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;",
            ")V"
        }
    .end annotation

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->config:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    .line 65
    iput-boolean p2, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->hasTransactions:Z

    .line 66
    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->salesSummary:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    .line 67
    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->discounts:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    .line 68
    iput-object p5, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->paymentMethods:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    .line 69
    iput-object p6, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->salesRows:Ljava/util/List;

    .line 70
    iput-object p7, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->paymentsRows:Ljava/util/List;

    .line 71
    iput-object p8, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->discountRows:Ljava/util/List;

    .line 72
    iput-object p9, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->categoryRows:Ljava/util/List;

    .line 73
    iput-object p10, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->chartData:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;

    return-void
.end method

.method public static noTransactions(Lcom/squareup/reports/applet/sales/v1/ReportConfig;)Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;
    .locals 12

    .line 19
    new-instance v11, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v0, v11

    move-object v1, p0

    invoke-direct/range {v0 .. v10}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfig;ZLcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;)V

    return-object v11
.end method

.method public static withTransactions(Lcom/squareup/reports/applet/sales/v1/ReportConfig;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;)Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/SalesReportRow;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/SalesReportRow;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;",
            ">;",
            "Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;",
            ")",
            "Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;"
        }
    .end annotation

    .line 27
    new-instance v11, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    const/4 v2, 0x1

    move-object v0, v11

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v0 .. v10}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfig;ZLcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;)V

    return-object v11
.end method
