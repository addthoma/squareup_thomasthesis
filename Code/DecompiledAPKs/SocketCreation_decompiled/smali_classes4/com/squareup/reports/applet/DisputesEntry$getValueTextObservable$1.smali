.class final Lcom/squareup/reports/applet/DisputesEntry$getValueTextObservable$1;
.super Ljava/lang/Object;
.source "ReportsAppletSections.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/DisputesEntry;->getValueTextObservable()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "showNotification",
        "",
        "apply",
        "(Ljava/lang/Boolean;)Ljava/lang/String;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/reports/applet/DisputesEntry;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/DisputesEntry;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/reports/applet/DisputesEntry$getValueTextObservable$1;->this$0:Lcom/squareup/reports/applet/DisputesEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 58
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/DisputesEntry$getValueTextObservable$1;->apply(Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Ljava/lang/Boolean;)Ljava/lang/String;
    .locals 1

    const-string v0, "showNotification"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 68
    iget-object p1, p0, Lcom/squareup/reports/applet/DisputesEntry$getValueTextObservable$1;->this$0:Lcom/squareup/reports/applet/DisputesEntry;

    invoke-static {p1}, Lcom/squareup/reports/applet/DisputesEntry;->access$getRes$p(Lcom/squareup/reports/applet/DisputesEntry;)Lcom/squareup/util/Res;

    move-result-object p1

    sget v0, Lcom/squareup/reports/applet/R$string;->active_disputes_badge:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    return-object p1
.end method
