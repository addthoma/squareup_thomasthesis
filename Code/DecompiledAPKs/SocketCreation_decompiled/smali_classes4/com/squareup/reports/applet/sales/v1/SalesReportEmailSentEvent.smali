.class public Lcom/squareup/reports/applet/sales/v1/SalesReportEmailSentEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "SalesReportEmailSentEvent.java"


# instance fields
.field public final changed_from_default:Z

.field public final using_account_email:Z


# direct methods
.method public constructor <init>(ZZ)V
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->REPORTS_EMAIL_SALES_REPORT_SENT:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 17
    iput-boolean p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportEmailSentEvent;->using_account_email:Z

    .line 18
    iput-boolean p2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportEmailSentEvent;->changed_from_default:Z

    return-void
.end method
