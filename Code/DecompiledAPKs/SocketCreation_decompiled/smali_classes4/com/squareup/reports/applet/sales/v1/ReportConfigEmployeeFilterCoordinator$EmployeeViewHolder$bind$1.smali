.class final Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder$bind$1;
.super Ljava/lang/Object;
.source "ReportConfigEmployeeFilterCoordinator.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;->bind(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $employee:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;

.field final synthetic this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder$bind$1;->this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;

    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder$bind$1;->$employee:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    .line 139
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder$bind$1;->this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;

    invoke-static {p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;->access$getRunner$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder;)Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$EmployeeViewHolder$bind$1;->$employee:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;->getToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->onClickEmployee(Ljava/lang/String;)V

    return-void
.end method
