.class public final Lcom/squareup/reports/applet/ReportsAppletCommonModule_Companion_ProvideReportConfigFactory;
.super Ljava/lang/Object;
.source "ReportsAppletCommonModule_Companion_ProvideReportConfigFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/ReportsAppletCommonModule_Companion_ProvideReportConfigFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lrx/subjects/BehaviorSubject<",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/reports/applet/ReportsAppletCommonModule_Companion_ProvideReportConfigFactory;
    .locals 1

    .line 24
    invoke-static {}, Lcom/squareup/reports/applet/ReportsAppletCommonModule_Companion_ProvideReportConfigFactory$InstanceHolder;->access$000()Lcom/squareup/reports/applet/ReportsAppletCommonModule_Companion_ProvideReportConfigFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideReportConfig()Lrx/subjects/BehaviorSubject;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ">;"
        }
    .end annotation

    .line 28
    sget-object v0, Lcom/squareup/reports/applet/ReportsAppletCommonModule;->Companion:Lcom/squareup/reports/applet/ReportsAppletCommonModule$Companion;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/ReportsAppletCommonModule$Companion;->provideReportConfig()Lrx/subjects/BehaviorSubject;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/subjects/BehaviorSubject;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/reports/applet/ReportsAppletCommonModule_Companion_ProvideReportConfigFactory;->get()Lrx/subjects/BehaviorSubject;

    move-result-object v0

    return-object v0
.end method

.method public get()Lrx/subjects/BehaviorSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ">;"
        }
    .end annotation

    .line 20
    invoke-static {}, Lcom/squareup/reports/applet/ReportsAppletCommonModule_Companion_ProvideReportConfigFactory;->provideReportConfig()Lrx/subjects/BehaviorSubject;

    move-result-object v0

    return-object v0
.end method
