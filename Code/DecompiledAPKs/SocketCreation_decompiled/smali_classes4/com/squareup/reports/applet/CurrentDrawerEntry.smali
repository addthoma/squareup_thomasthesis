.class public final Lcom/squareup/reports/applet/CurrentDrawerEntry;
.super Lcom/squareup/applet/AppletSectionsListEntry;
.source "ReportsAppletSections.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\n\u0010\t\u001a\u0004\u0018\u00010\nH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/reports/applet/CurrentDrawerEntry;",
        "Lcom/squareup/applet/AppletSectionsListEntry;",
        "section",
        "Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;",
        "res",
        "Lcom/squareup/util/Res;",
        "cashDrawerShiftManager",
        "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
        "(Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;Lcom/squareup/util/Res;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;)V",
        "getValueText",
        "",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;


# direct methods
.method public constructor <init>(Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;Lcom/squareup/util/Res;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;)V
    .locals 1

    const-string v0, "section"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cashDrawerShiftManager"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    check-cast p1, Lcom/squareup/applet/AppletSection;

    sget v0, Lcom/squareup/reports/applet/R$string;->reports_current_drawer:I

    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/applet/AppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V

    iput-object p3, p0, Lcom/squareup/reports/applet/CurrentDrawerEntry;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    return-void
.end method


# virtual methods
.method public getValueText()Ljava/lang/String;
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/squareup/reports/applet/CurrentDrawerEntry;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    invoke-interface {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;->cashManagementEnabledAndIsOpenShift()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/squareup/reports/applet/CurrentDrawerEntry;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/reports/applet/R$string;->current_drawer_open:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method
