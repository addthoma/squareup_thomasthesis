.class public final Lcom/squareup/reports/applet/ReportsDeepLinkHandler$ReportsHistoryFactory;
.super Ljava/lang/Object;
.source "ReportsDeepLinkHandler.kt"

# interfaces
.implements Lcom/squareup/ui/main/HistoryFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/ReportsDeepLinkHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReportsHistoryFactory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReportsDeepLinkHandler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReportsDeepLinkHandler.kt\ncom/squareup/reports/applet/ReportsDeepLinkHandler$ReportsHistoryFactory\n*L\n1#1,105:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\u0008\u0001\u0018\u00002\u00020\u0001B\u0011\u0008\u0000\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004J\u001a\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u0008H\u0016J\u0010\u0010\u000c\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\rH\u0016R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/reports/applet/ReportsDeepLinkHandler$ReportsHistoryFactory;",
        "Lcom/squareup/ui/main/HistoryFactory;",
        "screen",
        "Lcom/squareup/container/ContainerTreeKey;",
        "(Lcom/squareup/container/ContainerTreeKey;)V",
        "getScreen$reports_applet_release",
        "()Lcom/squareup/container/ContainerTreeKey;",
        "createHistory",
        "Lflow/History;",
        "home",
        "Lcom/squareup/ui/main/Home;",
        "currentHistory",
        "getPermissions",
        "",
        "Lcom/squareup/permissions/Permission;",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final screen:Lcom/squareup/container/ContainerTreeKey;


# direct methods
.method public constructor <init>(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/reports/applet/ReportsDeepLinkHandler$ReportsHistoryFactory;->screen:Lcom/squareup/container/ContainerTreeKey;

    return-void
.end method


# virtual methods
.method public createHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;
    .locals 1

    const-string v0, "home"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-static {p1, p2}, Lcom/squareup/ui/main/HomeKt;->buildUpon(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History$Builder;

    move-result-object p1

    .line 78
    sget-object p2, Lcom/squareup/reports/applet/ReportsMasterScreen;->INSTANCE:Lcom/squareup/reports/applet/ReportsMasterScreen;

    invoke-virtual {p1, p2}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    move-result-object p1

    .line 80
    iget-object p2, p0, Lcom/squareup/reports/applet/ReportsDeepLinkHandler$ReportsHistoryFactory;->screen:Lcom/squareup/container/ContainerTreeKey;

    if-eqz p2, :cond_0

    invoke-virtual {p1, p2}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    move-result-object p1

    .line 82
    :cond_0
    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    const-string p2, "home.buildUpon(currentHi\u2026       }\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 84
    sget-object v0, Lcom/squareup/permissions/Permission;->SUMMARIES_AND_REPORTS:Lcom/squareup/permissions/Permission;

    invoke-static {v0}, Lkotlin/collections/SetsKt;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final getScreen$reports_applet_release()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsDeepLinkHandler$ReportsHistoryFactory;->screen:Lcom/squareup/container/ContainerTreeKey;

    return-object v0
.end method
