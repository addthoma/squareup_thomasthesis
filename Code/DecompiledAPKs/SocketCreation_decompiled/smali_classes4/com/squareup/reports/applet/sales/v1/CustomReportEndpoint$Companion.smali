.class public final Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$Companion;
.super Ljava/lang/Object;
.source "CustomReportEndpoint.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0006\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u0006\u0010\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082T\u00a2\u0006\u0002\n\u0000R\u001c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\n\u0010\u0002R\u001c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u000c\u0010\u0002R\u0016\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$Companion;",
        "",
        "()V",
        "DISCOUNTS_GROUP_BY_TYPES",
        "",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
        "DISCOUNTS_GROUP_BY_TYPES$annotations",
        "MAX_DAYS_FOR_DAILY_SALES",
        "",
        "PAYMENT_METHOD_GROUP_BY_TYPES",
        "PAYMENT_METHOD_GROUP_BY_TYPES$annotations",
        "SALES_SUMMARY_GROUP_BY_TYPES",
        "SALES_SUMMARY_GROUP_BY_TYPES$annotations",
        "SALES_SUMMARY_GROUP_BY_TYPES_MEASUREMENT_UNIT",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 663
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 663
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$Companion;-><init>()V

    return-void
.end method

.method public static synthetic DISCOUNTS_GROUP_BY_TYPES$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic PAYMENT_METHOD_GROUP_BY_TYPES$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic SALES_SUMMARY_GROUP_BY_TYPES$annotations()V
    .locals 0

    return-void
.end method
