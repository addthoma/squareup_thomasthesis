.class public final Lcom/squareup/reports/applet/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action_amount:I = 0x7f0a0134

.field public static final action_description:I = 0x7f0a0142

.field public static final actual_in_drawer:I = 0x7f0a0162

.field public static final actual_in_drawer_container:I = 0x7f0a0163

.field public static final actual_in_drawer_edit_text:I = 0x7f0a0164

.field public static final actual_in_drawer_hint:I = 0x7f0a0165

.field public static final all_employees:I = 0x7f0a01af

.field public static final amount:I = 0x7f0a01b7

.field public static final applet_sections_list:I = 0x7f0a01c7

.field public static final cash_drawer_details:I = 0x7f0a02e8

.field public static final cash_management_settings_disabled_state_message:I = 0x7f0a02ed

.field public static final cash_refunds:I = 0x7f0a02f0

.field public static final cash_sales:I = 0x7f0a02f1

.field public static final chevron:I = 0x7f0a0329

.field public static final config_all_day:I = 0x7f0a0375

.field public static final config_device_filter:I = 0x7f0a0377

.field public static final config_employee_filter:I = 0x7f0a0379

.field public static final config_end_time_row:I = 0x7f0a037b

.field public static final config_item_details:I = 0x7f0a037c

.field public static final config_start_time_row:I = 0x7f0a037d

.field public static final content_frame:I = 0x7f0a03a6

.field public static final current_drawer_progress_bar:I = 0x7f0a051c

.field public static final current_drawer_start_drawer_button:I = 0x7f0a051d

.field public static final current_drawer_starting_cash:I = 0x7f0a051e

.field public static final date_picker:I = 0x7f0a0548

.field public static final date_time:I = 0x7f0a0555

.field public static final description:I = 0x7f0a0589

.field public static final difference:I = 0x7f0a05b6

.field public static final drawer_date:I = 0x7f0a05fc

.field public static final drawer_description:I = 0x7f0a05fd

.field public static final drawer_details_container:I = 0x7f0a05fe

.field public static final drawer_history_list:I = 0x7f0a05ff

.field public static final drawer_history_list_container:I = 0x7f0a0600

.field public static final drawer_history_null_container:I = 0x7f0a0601

.field public static final drawer_history_null_state_subtitle:I = 0x7f0a0602

.field public static final drawer_history_progress:I = 0x7f0a0603

.field public static final email_message:I = 0x7f0a06c2

.field public static final email_recipient:I = 0x7f0a06c5

.field public static final employee:I = 0x7f0a06d2

.field public static final employee_list_header:I = 0x7f0a06da

.field public static final employee_recycler:I = 0x7f0a06e9

.field public static final end_drawer_button:I = 0x7f0a0709

.field public static final end_drawer_card_end_button:I = 0x7f0a070a

.field public static final end_drawer_details:I = 0x7f0a070b

.field public static final end_drawer_message:I = 0x7f0a070c

.field public static final end_drawer_message_container:I = 0x7f0a070d

.field public static final end_of_drawer:I = 0x7f0a070e

.field public static final expected_in_drawer:I = 0x7f0a0734

.field public static final filter_by_employee:I = 0x7f0a0754

.field public static final glyph:I = 0x7f0a07ae

.field public static final in_progress_drawer_container:I = 0x7f0a0829

.field public static final loading_state_container:I = 0x7f0a0956

.field public static final paid_in_button:I = 0x7f0a0b8a

.field public static final paid_in_out:I = 0x7f0a0b8b

.field public static final paid_in_out_row:I = 0x7f0a0b8c

.field public static final paid_in_out_total_row:I = 0x7f0a0b8d

.field public static final paid_out_button:I = 0x7f0a0b8e

.field public static final recycler:I = 0x7f0a0d1f

.field public static final report_config_employee_filter_group:I = 0x7f0a0d62

.field public static final report_config_employee_filter_progress_bar:I = 0x7f0a0d63

.field public static final report_config_employee_filter_view:I = 0x7f0a0d64

.field public static final report_config_employee_filter_view_animator:I = 0x7f0a0d65

.field public static final report_config_employee_row:I = 0x7f0a0d66

.field public static final report_config_sheet_view:I = 0x7f0a0d67

.field public static final report_content_frame:I = 0x7f0a0d68

.field public static final sales_category_caret:I = 0x7f0a0da4

.field public static final sales_category_name:I = 0x7f0a0da5

.field public static final sales_category_price:I = 0x7f0a0da6

.field public static final sales_category_quantity:I = 0x7f0a0da7

.field public static final sales_category_subrow_name:I = 0x7f0a0da8

.field public static final sales_category_subrow_price:I = 0x7f0a0da9

.field public static final sales_category_subrow_quantity:I = 0x7f0a0daa

.field public static final sales_chart:I = 0x7f0a0dab

.field public static final sales_chart_header:I = 0x7f0a0dac

.field public static final sales_chart_highlight:I = 0x7f0a0dad

.field public static final sales_discount_name:I = 0x7f0a0db4

.field public static final sales_discount_price:I = 0x7f0a0db5

.field public static final sales_discount_quantity:I = 0x7f0a0db6

.field public static final sales_report_container:I = 0x7f0a0dbb

.field public static final sales_report_customize_date_range:I = 0x7f0a0dbc

.field public static final sales_report_error_state:I = 0x7f0a0dbf

.field public static final sales_report_no_transactions_for_time_period:I = 0x7f0a0dc2

.field public static final sales_report_overview_average_sales:I = 0x7f0a0dc3

.field public static final sales_report_overview_cover_count:I = 0x7f0a0dc4

.field public static final sales_report_overview_cover_count_holder:I = 0x7f0a0dc5

.field public static final sales_report_overview_cover_count_subtext:I = 0x7f0a0dc6

.field public static final sales_report_overview_gross_sales:I = 0x7f0a0dc7

.field public static final sales_report_overview_sales_count:I = 0x7f0a0dc8

.field public static final sales_report_progress_bar:I = 0x7f0a0dd0

.field public static final sales_report_recycler_view:I = 0x7f0a0dd1

.field public static final sales_report_row:I = 0x7f0a0dd2

.field public static final save_button:I = 0x7f0a0e00

.field public static final send_email_button:I = 0x7f0a0e66

.field public static final send_email_container:I = 0x7f0a0e67

.field public static final shift_description:I = 0x7f0a0e7a

.field public static final start_drawer_container:I = 0x7f0a0f2d

.field public static final start_of_drawer:I = 0x7f0a0f2f

.field public static final starting_cash:I = 0x7f0a0f31


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
