.class Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter$2;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "ReportsAppletSectionsListPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->handleSalesReportSection(Ljava/util/Set;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

.field final synthetic val$pos:I


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;I)V
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter$2;->this$0:Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

    iput p2, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter$2;->val$pos:I

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter$2;->this$0:Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

    invoke-static {v0}, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->access$500(Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;)Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter$2;->getAuthorizedEmployeeToken()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;->grantPermissionToPinnedEmployee(Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter$2;->this$0:Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

    iget v1, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter$2;->val$pos:I

    invoke-static {v0, v1}, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->access$601(Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;I)V

    return-void
.end method
