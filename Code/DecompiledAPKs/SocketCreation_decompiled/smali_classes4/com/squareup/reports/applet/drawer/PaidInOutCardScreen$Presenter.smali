.class Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen$Presenter;
.super Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;
.source "PaidInOutCardScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private flow:Lflow/Flow;

.field private res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/money/PriceLocaleHelper;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/cashdrawer/CashDrawerTracker;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/cashdrawer/CashDrawerTracker;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 58
    invoke-direct/range {p0 .. p8}, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;-><init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/money/PriceLocaleHelper;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/cashdrawer/CashDrawerTracker;)V

    .line 60
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen$Presenter;->flow:Lflow/Flow;

    .line 61
    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen$Presenter;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public synthetic lambda$onLoad$0$PaidInOutCardScreen$Presenter()V
    .locals 2

    .line 69
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/PaidInOutView;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/PaidInOutView;->hideKeyboard()V

    .line 70
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen$Presenter;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 65
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 66
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/reports/applet/R$string;->paid_in_out:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonTextX(Ljava/lang/CharSequence;)V

    .line 67
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonEnabled(Z)V

    .line 68
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v0, Lcom/squareup/reports/applet/drawer/-$$Lambda$PaidInOutCardScreen$Presenter$Y0VMfkK1bLNQdQmbzua9Fj2qQSk;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/drawer/-$$Lambda$PaidInOutCardScreen$Presenter$Y0VMfkK1bLNQdQmbzua9Fj2qQSk;-><init>(Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    return-void
.end method
