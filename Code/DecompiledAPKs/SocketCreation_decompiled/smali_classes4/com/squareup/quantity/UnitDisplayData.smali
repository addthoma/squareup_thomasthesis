.class public final Lcom/squareup/quantity/UnitDisplayData;
.super Ljava/lang/Object;
.source "UnitDisplayData.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/quantity/UnitDisplayData$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u000b\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0086\u0008\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001aB%\u0008\u0000\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0006H\u00c6\u0003J\'\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\u000e\u0010\u0014\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u0016J\t\u0010\u0017\u001a\u00020\u0006H\u00d6\u0001J\u0006\u0010\u0018\u001a\u00020\u0012J\t\u0010\u0019\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u000b\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/quantity/UnitDisplayData;",
        "",
        "unitName",
        "",
        "unitAbbreviation",
        "quantityPrecision",
        "",
        "(Ljava/lang/String;Ljava/lang/String;I)V",
        "getQuantityPrecision",
        "()I",
        "getUnitAbbreviation",
        "()Ljava/lang/String;",
        "getUnitName",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "getQuantityPrecisionHint",
        "res",
        "Lcom/squareup/util/Res;",
        "hashCode",
        "isEmpty",
        "toString",
        "Companion",
        "proto-utilities_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/quantity/UnitDisplayData$Companion;


# instance fields
.field private final quantityPrecision:I

.field private final unitAbbreviation:Ljava/lang/String;

.field private final unitName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/quantity/UnitDisplayData$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/quantity/UnitDisplayData$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/quantity/UnitDisplayData;->Companion:Lcom/squareup/quantity/UnitDisplayData$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/squareup/quantity/UnitDisplayData;-><init>(Ljava/lang/String;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    const-string v0, "unitName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "unitAbbreviation"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/quantity/UnitDisplayData;->unitName:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/quantity/UnitDisplayData;->unitAbbreviation:Ljava/lang/String;

    iput p3, p0, Lcom/squareup/quantity/UnitDisplayData;->quantityPrecision:I

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p5, p4, 0x1

    const-string v0, ""

    if-eqz p5, :cond_0

    move-object p1, v0

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    move-object p2, v0

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    const/4 p3, 0x0

    .line 37
    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/quantity/UnitDisplayData;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/quantity/UnitDisplayData;Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)Lcom/squareup/quantity/UnitDisplayData;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/quantity/UnitDisplayData;->unitName:Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/quantity/UnitDisplayData;->unitAbbreviation:Ljava/lang/String;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/squareup/quantity/UnitDisplayData;->quantityPrecision:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/quantity/UnitDisplayData;->copy(Ljava/lang/String;Ljava/lang/String;I)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object p0

    return-object p0
.end method

.method public static final empty()Lcom/squareup/quantity/UnitDisplayData;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/quantity/UnitDisplayData;->Companion:Lcom/squareup/quantity/UnitDisplayData$Companion;

    invoke-virtual {v0}, Lcom/squareup/quantity/UnitDisplayData$Companion;->empty()Lcom/squareup/quantity/UnitDisplayData;

    move-result-object v0

    return-object v0
.end method

.method public static final fromCatalogMeasurementUnit(Lcom/squareup/util/Res;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Lcom/squareup/quantity/UnitDisplayData;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/quantity/UnitDisplayData;->Companion:Lcom/squareup/quantity/UnitDisplayData$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/quantity/UnitDisplayData$Companion;->fromCatalogMeasurementUnit(Lcom/squareup/util/Res;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object p0

    return-object p0
.end method

.method public static final fromItemization(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/quantity/UnitDisplayData;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/quantity/UnitDisplayData;->Companion:Lcom/squareup/quantity/UnitDisplayData$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/quantity/UnitDisplayData$Companion;->fromItemization(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object p0

    return-object p0
.end method

.method public static final fromQuantityUnit(Lcom/squareup/util/Res;Lcom/squareup/orders/model/Order$QuantityUnit;)Lcom/squareup/quantity/UnitDisplayData;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/quantity/UnitDisplayData;->Companion:Lcom/squareup/quantity/UnitDisplayData$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/quantity/UnitDisplayData$Companion;->fromQuantityUnit(Lcom/squareup/util/Res;Lcom/squareup/orders/model/Order$QuantityUnit;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object p0

    return-object p0
.end method

.method public static final of(Ljava/lang/String;Ljava/lang/String;I)Lcom/squareup/quantity/UnitDisplayData;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/quantity/UnitDisplayData;->Companion:Lcom/squareup/quantity/UnitDisplayData$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/squareup/quantity/UnitDisplayData$Companion;->of(Ljava/lang/String;Ljava/lang/String;I)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/quantity/UnitDisplayData;->unitName:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/quantity/UnitDisplayData;->unitAbbreviation:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/quantity/UnitDisplayData;->quantityPrecision:I

    return v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;I)Lcom/squareup/quantity/UnitDisplayData;
    .locals 1

    const-string v0, "unitName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "unitAbbreviation"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/quantity/UnitDisplayData;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/quantity/UnitDisplayData;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/quantity/UnitDisplayData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/quantity/UnitDisplayData;

    iget-object v0, p0, Lcom/squareup/quantity/UnitDisplayData;->unitName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/quantity/UnitDisplayData;->unitName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/quantity/UnitDisplayData;->unitAbbreviation:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/quantity/UnitDisplayData;->unitAbbreviation:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/quantity/UnitDisplayData;->quantityPrecision:I

    iget p1, p1, Lcom/squareup/quantity/UnitDisplayData;->quantityPrecision:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getQuantityPrecision()I
    .locals 1

    .line 37
    iget v0, p0, Lcom/squareup/quantity/UnitDisplayData;->quantityPrecision:I

    return v0
.end method

.method public final getQuantityPrecisionHint(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 2

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iget v0, p0, Lcom/squareup/quantity/UnitDisplayData;->quantityPrecision:I

    if-eqz v0, :cond_5

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 48
    sget v0, Lcom/squareup/proto_utilities/R$string;->precision_display_value_5:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 49
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "invalid precision set"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 47
    :cond_1
    sget v0, Lcom/squareup/proto_utilities/R$string;->precision_display_value_4:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 46
    :cond_2
    sget v0, Lcom/squareup/proto_utilities/R$string;->precision_display_value_3:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 45
    :cond_3
    sget v0, Lcom/squareup/proto_utilities/R$string;->precision_display_value_2:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 44
    :cond_4
    sget v0, Lcom/squareup/proto_utilities/R$string;->precision_display_value_1:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 43
    :cond_5
    sget v0, Lcom/squareup/proto_utilities/R$string;->precision_display_value_0:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public final getUnitAbbreviation()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/quantity/UnitDisplayData;->unitAbbreviation:Ljava/lang/String;

    return-object v0
.end method

.method public final getUnitName()Ljava/lang/String;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/quantity/UnitDisplayData;->unitName:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/quantity/UnitDisplayData;->unitName:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/quantity/UnitDisplayData;->unitAbbreviation:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/quantity/UnitDisplayData;->quantityPrecision:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final isEmpty()Z
    .locals 2

    .line 40
    move-object v0, p0

    check-cast v0, Lcom/squareup/quantity/UnitDisplayData;

    sget-object v1, Lcom/squareup/quantity/UnitDisplayData;->Companion:Lcom/squareup/quantity/UnitDisplayData$Companion;

    invoke-virtual {v1}, Lcom/squareup/quantity/UnitDisplayData$Companion;->empty()Lcom/squareup/quantity/UnitDisplayData;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UnitDisplayData(unitName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quantity/UnitDisplayData;->unitName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", unitAbbreviation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quantity/UnitDisplayData;->unitAbbreviation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", quantityPrecision="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/quantity/UnitDisplayData;->quantityPrecision:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
