.class Lcom/squareup/persistent/PersistentFile$1;
.super Ljava/lang/Object;
.source "PersistentFile.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/persistent/PersistentFile;->get(Lcom/squareup/persistent/AsyncCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/persistent/PersistentFile;

.field final synthetic val$callBackOnMainThread:Z

.field final synthetic val$callback:Lcom/squareup/persistent/AsyncCallback;


# direct methods
.method constructor <init>(Lcom/squareup/persistent/PersistentFile;Lcom/squareup/persistent/AsyncCallback;Z)V
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/persistent/PersistentFile$1;->this$0:Lcom/squareup/persistent/PersistentFile;

    iput-object p2, p0, Lcom/squareup/persistent/PersistentFile$1;->val$callback:Lcom/squareup/persistent/AsyncCallback;

    iput-boolean p3, p0, Lcom/squareup/persistent/PersistentFile$1;->val$callBackOnMainThread:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 148
    :try_start_0
    iget-object v0, p0, Lcom/squareup/persistent/PersistentFile$1;->this$0:Lcom/squareup/persistent/PersistentFile;

    invoke-static {v0}, Lcom/squareup/persistent/PersistentFile;->access$000(Lcom/squareup/persistent/PersistentFile;)Ljava/lang/Object;

    move-result-object v0

    .line 149
    iget-object v1, p0, Lcom/squareup/persistent/PersistentFile$1;->this$0:Lcom/squareup/persistent/PersistentFile;

    iget-object v1, v1, Lcom/squareup/persistent/PersistentFile;->value:Ljava/lang/Object;

    if-nez v1, :cond_0

    .line 150
    iget-object v1, p0, Lcom/squareup/persistent/PersistentFile$1;->this$0:Lcom/squareup/persistent/PersistentFile;

    iput-object v0, v1, Lcom/squareup/persistent/PersistentFile;->value:Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const-string v0, "Somebody set a value before our loadFromFile returned. Use their value instead of the one from the file (%s)."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 152
    iget-object v3, p0, Lcom/squareup/persistent/PersistentFile$1;->this$0:Lcom/squareup/persistent/PersistentFile;

    iget-object v3, v3, Lcom/squareup/persistent/PersistentFile;->file:Ljava/io/File;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 156
    :goto_0
    iget-object v0, p0, Lcom/squareup/persistent/PersistentFile$1;->this$0:Lcom/squareup/persistent/PersistentFile;

    iget-object v1, p0, Lcom/squareup/persistent/PersistentFile$1;->val$callback:Lcom/squareup/persistent/AsyncCallback;

    iget-boolean v2, p0, Lcom/squareup/persistent/PersistentFile$1;->val$callBackOnMainThread:Z

    iget-object v3, p0, Lcom/squareup/persistent/PersistentFile$1;->this$0:Lcom/squareup/persistent/PersistentFile;

    iget-object v3, v3, Lcom/squareup/persistent/PersistentFile;->value:Ljava/lang/Object;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/persistent/PersistentFile;->access$100(Lcom/squareup/persistent/PersistentFile;Lcom/squareup/persistent/AsyncCallback;ZLjava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 160
    iget-object v1, p0, Lcom/squareup/persistent/PersistentFile$1;->this$0:Lcom/squareup/persistent/PersistentFile;

    iget-object v2, p0, Lcom/squareup/persistent/PersistentFile$1;->val$callback:Lcom/squareup/persistent/AsyncCallback;

    iget-boolean v3, p0, Lcom/squareup/persistent/PersistentFile$1;->val$callBackOnMainThread:Z

    invoke-static {v1, v2, v3, v0}, Lcom/squareup/persistent/PersistentFile;->access$200(Lcom/squareup/persistent/PersistentFile;Lcom/squareup/persistent/AsyncCallback;ZLjava/lang/Throwable;)V

    :goto_1
    return-void

    :catch_1
    move-exception v0

    .line 158
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to read value"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
