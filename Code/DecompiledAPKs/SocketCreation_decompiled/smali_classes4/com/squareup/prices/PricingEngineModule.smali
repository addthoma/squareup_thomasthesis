.class public abstract Lcom/squareup/prices/PricingEngineModule;
.super Ljava/lang/Object;
.source "PricingEngineModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static provideItemizationDetailsLoader()Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;

    invoke-direct {v0}, Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;-><init>()V

    return-object v0
.end method

.method public static providePricingEngine(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;)Lcom/squareup/shared/pricing/engine/PricingEngine;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 18
    new-instance v0, Lcom/squareup/prices/PricingEngineModule$1;

    invoke-direct {v0, p1}, Lcom/squareup/prices/PricingEngineModule$1;-><init>(Lcom/squareup/util/Clock;)V

    .line 28
    new-instance p1, Lcom/squareup/prices/RealPricingAnalytics;

    invoke-direct {p1, p0}, Lcom/squareup/prices/RealPricingAnalytics;-><init>(Lcom/squareup/analytics/Analytics;)V

    .line 30
    invoke-static {v0, p1}, Lcom/squareup/shared/pricing/engine/PricingEngineImpl;->newPricingEngine(Lcom/squareup/shared/catalog/logging/Clock;Lcom/squareup/shared/pricing/engine/analytics/PricingAnalytics;)Lcom/squareup/shared/pricing/engine/PricingEngineImpl;

    move-result-object p0

    return-object p0
.end method
