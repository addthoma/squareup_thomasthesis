.class Lcom/squareup/prices/RuleSearchEvent;
.super Lcom/squareup/eventstream/v2/AppEvent;
.source "RuleSearchEvent.java"


# static fields
.field private static final RULE_SEARCH_NAME:Ljava/lang/String; = "pricing_rule_search"


# instance fields
.field private final pricing_rule_search_automatic:Z

.field private final pricing_rule_search_count_amount_discount_rules_applied:I

.field private final pricing_rule_search_count_category_rules_applied:I

.field private final pricing_rule_search_count_combo_rules_applied:I

.field private final pricing_rule_search_count_complex_rules_applied:I

.field private final pricing_rule_search_count_item_rules_applied:I

.field private final pricing_rule_search_count_itemizations_in_cart:I

.field private final pricing_rule_search_count_percent_discount_rules_applied:I

.field private final pricing_rule_search_count_quantity_in_cart:I

.field private final pricing_rule_search_count_recurring_rules_applied:I

.field private final pricing_rule_search_count_rules_applied:I

.field private final pricing_rule_search_count_rules_loaded:I

.field private final pricing_rule_search_count_time_restricted_rules_applied:I

.field private final pricing_rule_search_count_volume_rules_applied:I

.field private final pricing_rule_search_largest_rival_set:I

.field private final pricing_rule_search_loader_duration:J

.field private final pricing_rule_search_passcode_used:Z

.field private final pricing_rule_search_rule_evaluations:J

.field private final pricing_rule_search_search_duration:J

.field private final pricing_rule_search_total_amount_discounted_cents:Lcom/squareup/protos/common/Money;

.field private final pricing_rule_search_total_duration:J


# direct methods
.method constructor <init>(Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;)V
    .locals 3

    const-string v0, "pricing_rule_search"

    .line 130
    invoke-direct {p0, v0}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    .line 131
    iget-wide v0, p1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->searchDuration:J

    iput-wide v0, p0, Lcom/squareup/prices/RuleSearchEvent;->pricing_rule_search_search_duration:J

    .line 132
    iget-wide v0, p1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->loaderDuration:J

    iput-wide v0, p0, Lcom/squareup/prices/RuleSearchEvent;->pricing_rule_search_loader_duration:J

    .line 133
    iget-wide v0, p1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->totalDuration:J

    iput-wide v0, p0, Lcom/squareup/prices/RuleSearchEvent;->pricing_rule_search_total_duration:J

    .line 134
    iget v0, p1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countRulesLoaded:I

    iput v0, p0, Lcom/squareup/prices/RuleSearchEvent;->pricing_rule_search_count_rules_loaded:I

    .line 135
    iget v0, p1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->largestRivalSet:I

    iput v0, p0, Lcom/squareup/prices/RuleSearchEvent;->pricing_rule_search_largest_rival_set:I

    .line 136
    iget-wide v0, p1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->ruleEvaluations:J

    iput-wide v0, p0, Lcom/squareup/prices/RuleSearchEvent;->pricing_rule_search_rule_evaluations:J

    .line 137
    iget v0, p1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countItemizationsInCart:I

    iput v0, p0, Lcom/squareup/prices/RuleSearchEvent;->pricing_rule_search_count_itemizations_in_cart:I

    .line 138
    iget v0, p1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countQuantityInCart:I

    iput v0, p0, Lcom/squareup/prices/RuleSearchEvent;->pricing_rule_search_count_quantity_in_cart:I

    .line 139
    iget v0, p1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countRulesApplied:I

    iput v0, p0, Lcom/squareup/prices/RuleSearchEvent;->pricing_rule_search_count_rules_applied:I

    .line 140
    iget v0, p1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countPercentDiscountRulesApplied:I

    iput v0, p0, Lcom/squareup/prices/RuleSearchEvent;->pricing_rule_search_count_percent_discount_rules_applied:I

    .line 142
    iget v0, p1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countAmountDiscountRulesApplied:I

    iput v0, p0, Lcom/squareup/prices/RuleSearchEvent;->pricing_rule_search_count_amount_discount_rules_applied:I

    .line 144
    iget-object v0, p1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->totalAmountDiscounted:Lcom/squareup/shared/pricing/models/MonetaryAmount;

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/models/MonetaryAmount;->getAmount()Ljava/lang/Long;

    move-result-object v0

    .line 145
    iget-object v1, p1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->totalAmountDiscounted:Lcom/squareup/shared/pricing/models/MonetaryAmount;

    .line 147
    invoke-virtual {v1}, Lcom/squareup/shared/pricing/models/MonetaryAmount;->getCurrency()Ljava/util/Currency;

    move-result-object v1

    .line 148
    invoke-virtual {v1}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v1

    .line 145
    invoke-static {v1}, Lcom/squareup/protos/common/CurrencyCode;->valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v1

    .line 149
    new-instance v2, Lcom/squareup/protos/common/Money;

    invoke-direct {v2, v0, v1}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 150
    iput-object v2, p0, Lcom/squareup/prices/RuleSearchEvent;->pricing_rule_search_total_amount_discounted_cents:Lcom/squareup/protos/common/Money;

    .line 151
    iget v0, p1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countCategoryRulesApplied:I

    iput v0, p0, Lcom/squareup/prices/RuleSearchEvent;->pricing_rule_search_count_category_rules_applied:I

    .line 152
    iget v0, p1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countItemRulesApplied:I

    iput v0, p0, Lcom/squareup/prices/RuleSearchEvent;->pricing_rule_search_count_item_rules_applied:I

    .line 153
    iget v0, p1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countVolumeRulesApplied:I

    iput v0, p0, Lcom/squareup/prices/RuleSearchEvent;->pricing_rule_search_count_volume_rules_applied:I

    .line 154
    iget v0, p1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countComboRulesApplied:I

    iput v0, p0, Lcom/squareup/prices/RuleSearchEvent;->pricing_rule_search_count_combo_rules_applied:I

    .line 155
    iget v0, p1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countTimeRestrictedRulesApplied:I

    iput v0, p0, Lcom/squareup/prices/RuleSearchEvent;->pricing_rule_search_count_time_restricted_rules_applied:I

    .line 157
    iget v0, p1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countRecurringRulesApplied:I

    iput v0, p0, Lcom/squareup/prices/RuleSearchEvent;->pricing_rule_search_count_recurring_rules_applied:I

    .line 158
    iget v0, p1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countComplexRulesApplied:I

    iput v0, p0, Lcom/squareup/prices/RuleSearchEvent;->pricing_rule_search_count_complex_rules_applied:I

    .line 159
    iget-boolean v0, p1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->automatic:Z

    iput-boolean v0, p0, Lcom/squareup/prices/RuleSearchEvent;->pricing_rule_search_automatic:Z

    .line 160
    iget-boolean p1, p1, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->passcodeUsed:Z

    iput-boolean p1, p0, Lcom/squareup/prices/RuleSearchEvent;->pricing_rule_search_passcode_used:Z

    return-void
.end method
