.class public final Lcom/squareup/prices/PricingEngineModule_ProvideItemizationDetailsLoaderFactory;
.super Ljava/lang/Object;
.source "PricingEngineModule_ProvideItemizationDetailsLoaderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/prices/PricingEngineModule_ProvideItemizationDetailsLoaderFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/prices/PricingEngineModule_ProvideItemizationDetailsLoaderFactory;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/prices/PricingEngineModule_ProvideItemizationDetailsLoaderFactory$InstanceHolder;->access$000()Lcom/squareup/prices/PricingEngineModule_ProvideItemizationDetailsLoaderFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideItemizationDetailsLoader()Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;
    .locals 2

    .line 23
    invoke-static {}, Lcom/squareup/prices/PricingEngineModule;->provideItemizationDetailsLoader()Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;
    .locals 1

    .line 15
    invoke-static {}, Lcom/squareup/prices/PricingEngineModule_ProvideItemizationDetailsLoaderFactory;->provideItemizationDetailsLoader()Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/prices/PricingEngineModule_ProvideItemizationDetailsLoaderFactory;->get()Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;

    move-result-object v0

    return-object v0
.end method
