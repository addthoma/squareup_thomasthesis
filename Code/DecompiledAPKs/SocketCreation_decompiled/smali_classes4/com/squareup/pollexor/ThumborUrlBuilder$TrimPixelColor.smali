.class public final enum Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;
.super Ljava/lang/Enum;
.source "ThumborUrlBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/pollexor/ThumborUrlBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TrimPixelColor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;

.field public static final enum BOTTOM_RIGHT:Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;

.field public static final enum TOP_LEFT:Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;


# instance fields
.field final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 68
    new-instance v0, Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;

    const/4 v1, 0x0

    const-string v2, "TOP_LEFT"

    const-string v3, "top-left"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;->TOP_LEFT:Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;

    new-instance v0, Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;

    const/4 v2, 0x1

    const-string v3, "BOTTOM_RIGHT"

    const-string v4, "bottom-right"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;->BOTTOM_RIGHT:Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;

    .line 67
    sget-object v3, Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;->TOP_LEFT:Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;->BOTTOM_RIGHT:Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;->$VALUES:[Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 72
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 73
    iput-object p3, p0, Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;
    .locals 1

    .line 67
    const-class v0, Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;

    return-object p0
.end method

.method public static values()[Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;
    .locals 1

    .line 67
    sget-object v0, Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;->$VALUES:[Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;

    invoke-virtual {v0}, [Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;

    return-object v0
.end method
