.class public final enum Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;
.super Ljava/lang/Enum;
.source "ThumborUrlBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/pollexor/ThumborUrlBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HorizontalAlign"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;

.field public static final enum CENTER:Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;

.field public static final enum LEFT:Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;

.field public static final enum RIGHT:Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;


# instance fields
.field final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 46
    new-instance v0, Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;

    const/4 v1, 0x0

    const-string v2, "LEFT"

    const-string v3, "left"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;->LEFT:Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;

    new-instance v0, Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;

    const/4 v2, 0x1

    const-string v3, "CENTER"

    const-string v4, "center"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;->CENTER:Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;

    new-instance v0, Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;

    const/4 v3, 0x2

    const-string v4, "RIGHT"

    const-string v5, "right"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;->RIGHT:Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;

    .line 45
    sget-object v4, Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;->LEFT:Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;->CENTER:Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;->RIGHT:Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;->$VALUES:[Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 51
    iput-object p3, p0, Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;
    .locals 1

    .line 45
    const-class v0, Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;

    return-object p0
.end method

.method public static values()[Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;
    .locals 1

    .line 45
    sget-object v0, Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;->$VALUES:[Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;

    invoke-virtual {v0}, [Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;

    return-object v0
.end method
