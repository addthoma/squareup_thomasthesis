.class public final Lcom/squareup/pollexor/Thumbor;
.super Ljava/lang/Object;
.source "Thumbor.java"


# instance fields
.field private final host:Ljava/lang/String;

.field private final key:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_1

    .line 34
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "/"

    .line 37
    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 38
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 40
    :cond_0
    iput-object p1, p0, Lcom/squareup/pollexor/Thumbor;->host:Ljava/lang/String;

    .line 41
    iput-object p2, p0, Lcom/squareup/pollexor/Thumbor;->key:Ljava/lang/String;

    return-void

    .line 35
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Host must not be blank."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static create(Ljava/lang/String;)Lcom/squareup/pollexor/Thumbor;
    .locals 2

    .line 15
    new-instance v0, Lcom/squareup/pollexor/Thumbor;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/pollexor/Thumbor;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static create(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/pollexor/Thumbor;
    .locals 1

    if-eqz p1, :cond_0

    .line 24
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    new-instance v0, Lcom/squareup/pollexor/Thumbor;

    invoke-direct {v0, p0, p1}, Lcom/squareup/pollexor/Thumbor;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 25
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Key must not be blank."

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public buildImage(Ljava/lang/String;)Lcom/squareup/pollexor/ThumborUrlBuilder;
    .locals 3

    if-eqz p1, :cond_0

    .line 54
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    new-instance v0, Lcom/squareup/pollexor/ThumborUrlBuilder;

    iget-object v1, p0, Lcom/squareup/pollexor/Thumbor;->host:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/pollexor/Thumbor;->key:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/pollexor/ThumborUrlBuilder;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 55
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Invalid image."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/pollexor/Thumbor;->host:Ljava/lang/String;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/pollexor/Thumbor;->key:Ljava/lang/String;

    return-object v0
.end method
