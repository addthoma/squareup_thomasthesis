.class public final Lcom/squareup/util/VectorFriendly;
.super Ljava/lang/Object;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/VectorFriendly\n+ 2 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n+ 3 Views.kt\ncom/squareup/util/Views\n*L\n1#1,1322:1\n347#2,7:1323\n1228#3,11:1330\n*E\n*S KotlinDebug\n*F\n+ 1 Views.kt\ncom/squareup/util/VectorFriendly\n*L\n1280#1,7:1323\n1308#1,11:1330\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\tH\u0002J\u000e\u0010\u000c\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\tR\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u001a\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/util/VectorFriendly;",
        "",
        "()V",
        "VECTOR_CAPABLE_VERSION",
        "",
        "getVECTOR_CAPABLE_VERSION",
        "()I",
        "cache",
        "Ljava/util/WeakHashMap;",
        "Landroid/content/Context;",
        "createGoodContext",
        "original",
        "ensureContext",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/util/VectorFriendly;

# The value of this static final field might be set in the static constructor
.field private static final VECTOR_CAPABLE_VERSION:I = 0x15

.field private static final cache:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap<",
            "Landroid/content/Context;",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1264
    new-instance v0, Lcom/squareup/util/VectorFriendly;

    invoke-direct {v0}, Lcom/squareup/util/VectorFriendly;-><init>()V

    sput-object v0, Lcom/squareup/util/VectorFriendly;->INSTANCE:Lcom/squareup/util/VectorFriendly;

    const/16 v0, 0x15

    .line 1266
    sput v0, Lcom/squareup/util/VectorFriendly;->VECTOR_CAPABLE_VERSION:I

    .line 1316
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lcom/squareup/util/VectorFriendly;->cache:Ljava/util/WeakHashMap;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 1264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final createGoodContext(Landroid/content/Context;)Landroid/content/Context;
    .locals 4

    .line 1302
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    instance-of v0, v0, Landroidx/appcompat/widget/VectorEnabledTintResources;

    if-eqz v0, :cond_0

    return-object p1

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1330
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1332
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v2, Lcom/squareup/util/VectorFriendly;->INSTANCE:Lcom/squareup/util/VectorFriendly;

    invoke-virtual {v2}, Lcom/squareup/util/VectorFriendly;->getVECTOR_CAPABLE_VERSION()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1333
    invoke-static {}, Landroidx/appcompat/app/AppCompatDelegate;->isCompatVectorFromResourcesEnabled()Z

    move-result v0

    .line 1334
    invoke-static {v1}, Landroidx/appcompat/app/AppCompatDelegate;->setCompatVectorFromResourcesEnabled(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    :goto_0
    const/4 v1, 0x0

    .line 1309
    :try_start_0
    new-instance v2, Landroid/content/ContextWrapper;

    invoke-direct {v2, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 1310
    new-instance p1, Landroidx/appcompat/widget/AppCompatCheckBox;

    check-cast v2, Landroid/content/Context;

    invoke-direct {p1, v2}, Landroidx/appcompat/widget/AppCompatCheckBox;-><init>(Landroid/content/Context;)V

    .line 1311
    invoke-virtual {p1}, Landroidx/appcompat/widget/AppCompatCheckBox;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v2, "ccb.context"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1339
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v3, Lcom/squareup/util/VectorFriendly;->INSTANCE:Lcom/squareup/util/VectorFriendly;

    invoke-virtual {v3}, Lcom/squareup/util/VectorFriendly;->getVECTOR_CAPABLE_VERSION()I

    move-result v3

    if-ge v2, v3, :cond_2

    if-nez v0, :cond_2

    .line 1340
    invoke-static {v1}, Landroidx/appcompat/app/AppCompatDelegate;->setCompatVectorFromResourcesEnabled(Z)V

    :cond_2
    const-string v0, "vectorFriendly {\n      v\u2026)\n      ccb.context\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :catchall_0
    move-exception p1

    .line 1339
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v3, Lcom/squareup/util/VectorFriendly;->INSTANCE:Lcom/squareup/util/VectorFriendly;

    invoke-virtual {v3}, Lcom/squareup/util/VectorFriendly;->getVECTOR_CAPABLE_VERSION()I

    move-result v3

    if-ge v2, v3, :cond_3

    if-nez v0, :cond_3

    .line 1340
    invoke-static {v1}, Landroidx/appcompat/app/AppCompatDelegate;->setCompatVectorFromResourcesEnabled(Z)V

    :cond_3
    throw p1
.end method


# virtual methods
.method public final ensureContext(Landroid/content/Context;)Landroid/content/Context;
    .locals 2

    const-string v0, "original"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1274
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sget v1, Lcom/squareup/util/VectorFriendly;->VECTOR_CAPABLE_VERSION:I

    if-lt v0, v1, :cond_0

    return-object p1

    :cond_0
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1278
    invoke-static {v1, v0, v1}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1280
    sget-object v0, Lcom/squareup/util/VectorFriendly;->cache:Ljava/util/WeakHashMap;

    check-cast v0, Ljava/util/Map;

    .line 1323
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1281
    sget-object v1, Lcom/squareup/util/VectorFriendly;->INSTANCE:Lcom/squareup/util/VectorFriendly;

    invoke-direct {v1, p1}, Lcom/squareup/util/VectorFriendly;->createGoodContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v1

    .line 1326
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    const-string p1, "cache.getOrPut(original)\u2026odContext(original)\n    }"

    .line 1324
    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/content/Context;

    return-object v1
.end method

.method public final getVECTOR_CAPABLE_VERSION()I
    .locals 1

    .line 1266
    sget v0, Lcom/squareup/util/VectorFriendly;->VECTOR_CAPABLE_VERSION:I

    return v0
.end method
