.class public final Lcom/squareup/util/Views;
.super Ljava/lang/Object;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views\n+ 2 Strings.kt\nkotlin/text/StringsKt__StringsKt\n+ 3 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n*L\n1#1,1322:1\n1154#1,2:1359\n45#2:1323\n17#2,22:1324\n10554#3,2:1346\n1203#3,2:1348\n3847#3,9:1350\n*E\n*S KotlinDebug\n*F\n+ 1 Views.kt\ncom/squareup/util/Views\n*L\n624#1:1323\n624#1,22:1324\n633#1,2:1346\n636#1,2:1348\n1116#1,9:1350\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00ba\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0011\n\u0002\u0008\u0016\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0010\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0015\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\r\n\u0002\u0008\u000f\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u001a0\u00109\u001a\u00020:2\u0006\u0010;\u001a\u00020\u00142\u0008\u0010<\u001a\u0004\u0018\u00010=2\u0006\u0010>\u001a\u00020\u00142\u0006\u0010?\u001a\u00020=2\u0006\u0010@\u001a\u00020A\u001a\u001e\u00109\u001a\u00020:2\u0006\u0010;\u001a\u00020\u00142\u0006\u0010>\u001a\u00020\u00142\u0006\u0010@\u001a\u00020A\u001a\u000e\u0010B\u001a\u00020:2\u0006\u0010C\u001a\u00020\u000f\u001a\u0016\u0010D\u001a\u00020:2\u0006\u0010E\u001a\u00020\u00142\u0006\u0010F\u001a\u00020\u0014\u001a\u0006\u0010G\u001a\u00020/\u001a\"\u0010H\u001a\u00020I2\u0006\u0010J\u001a\u00020K2\u0008\u0008\u0001\u0010L\u001a\u00020/2\u0008\u0008\u0001\u0010M\u001a\u00020/\u001a!\u0010N\u001a\u0004\u0018\u00010#2\u0012\u0010O\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020#0P\"\u00020#\u00a2\u0006\u0002\u0010Q\u001a\'\u0010R\u001a\u0002HS\"\u0008\u0008\u0000\u0010S*\u00020\u00142\u0008\u0008\u0001\u0010T\u001a\u00020/2\u0006\u0010U\u001a\u00020\u0019\u00a2\u0006\u0002\u0010V\u001a1\u0010R\u001a\u0002HS\"\u0008\u0008\u0000\u0010S*\u00020\u00142\u0008\u0008\u0001\u0010T\u001a\u00020/2\u0006\u0010U\u001a\u00020\u00192\u0008\u0008\u0002\u0010W\u001a\u00020\u000f\u00a2\u0006\u0002\u0010X\u001a\u0016\u0010Y\u001a\u00020\u000f2\u0006\u0010Z\u001a\u00020\u00142\u0006\u0010[\u001a\u00020\u0014\u001a\u001f\u0010\"\u001a\u00020\u000f2\u0012\u0010\\\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020#0P\"\u00020#\u00a2\u0006\u0002\u0010]\u001a)\u0010^\u001a\u0004\u0018\u0001HS\"\u0008\u0008\u0000\u0010S*\u00020\u00142\u0006\u0010_\u001a\u00020\u00142\u0008\u0008\u0001\u0010`\u001a\u00020/\u00a2\u0006\u0002\u0010a\u001a\u0006\u0010\u000e\u001a\u00020\u000f\u001a\u000e\u0010b\u001a\u00020:2\u0006\u0010_\u001a\u00020\u0014\u001a\u001a\u0010c\u001a\u0002062\u0006\u0010d\u001a\u0002062\u0008\u0008\u0002\u0010e\u001a\u00020\u000fH\u0000\u001a\u0010\u0010f\u001a\u0004\u0018\u00010g2\u0006\u0010_\u001a\u00020\u0014\u001a$\u0010h\u001a\u0002HS\"\u0004\u0008\u0000\u0010S2\u000e\u0008\u0004\u0010i\u001a\u0008\u0012\u0004\u0012\u0002HS0jH\u0086\u0008\u00a2\u0006\u0002\u0010k\u001a\u001a\u0010l\u001a\u00020:2\u0006\u0010m\u001a\u00020n2\u0008\u0008\u0002\u0010o\u001a\u00020AH\u0007\u001a\u001c\u0010p\u001a\u00020q*\u00020K2\u0006\u0010r\u001a\u00020s2\u0008\u0008\u0001\u0010t\u001a\u00020/\u001a\u001c\u0010p\u001a\u00020q*\u00020K2\u0006\u0010r\u001a\u00020u2\u0008\u0008\u0001\u0010t\u001a\u00020/\u001a\"\u0010v\u001a\u00020w*\u00020\u00142\u0006\u0010x\u001a\u00020\u00012\u0006\u0010y\u001a\u00020\u00012\u0006\u0010@\u001a\u00020/\u001a\u0012\u0010z\u001a\u00020/*\u00020\u00012\u0006\u0010{\u001a\u00020/\u001a\u0016\u0010|\u001a\u00020:*\u0004\u0018\u00010}2\u0008\u0010_\u001a\u0004\u0018\u00010\u0014\u001a\u0012\u0010~\u001a\u00020:*\u00020\u00142\u0006\u0010\u007f\u001a\u00020/\u001a\u001b\u0010~\u001a\u00020:*\u00020\u00142\u0007\u0010\u0080\u0001\u001a\u00020/2\u0006\u0010\u007f\u001a\u00020/\u001a8\u0010\u0081\u0001\u001a\u00020:*\u00020\u00142\t\u0008\u0002\u0010\u0080\u0001\u001a\u00020/2\u0006\u0010\u007f\u001a\u00020/2\u0016\u0010\u0082\u0001\u001a\u0011\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020:\u0018\u00010\u0083\u0001H\u0007\u001a\u0013\u0010\u0084\u0001\u001a\u00020:*\u00020\u00142\u0006\u0010\u007f\u001a\u00020/\u001a\u0013\u0010\u0085\u0001\u001a\u00020:*\u00020\u00142\u0006\u0010\u007f\u001a\u00020/\u001a%\u0010\u0086\u0001\u001a\u0002HS\"\u0008\u0008\u0000\u0010S*\u00020\u0014*\u00020\u00132\u0008\u0008\u0001\u0010`\u001a\u00020/\u00a2\u0006\u0003\u0010\u0087\u0001\u001a$\u0010\u0086\u0001\u001a\u0002HS\"\u0008\u0008\u0000\u0010S*\u00020\u0014*\u00020\u00142\u0008\u0008\u0001\u0010`\u001a\u00020/\u00a2\u0006\u0002\u0010a\u001a\u0014\u0010\u0088\u0001\u001a\u00020/*\u00020\u00142\u0007\u0010\u0089\u0001\u001a\u00020\u0019\u001a\u001d\u0010\u008a\u0001\u001a\u00020:*\u00020\u00142\u0007\u0010\u0089\u0001\u001a\u00020\u00192\u0007\u0010\u008b\u0001\u001a\u00020\'\u001a\u000c\u0010\u008c\u0001\u001a\u00030\u008d\u0001*\u00020n\u001a\u0016\u0010\u008e\u0001\u001a\u00030\u008f\u0001*\u00020n2\u0008\u0008\u0001\u0010`\u001a\u00020/\u001a\u0017\u0010\u008e\u0001\u001a\u00020u*\u00020K2\u0008\u0008\u0001\u0010t\u001a\u00020/H\u0002\u001a/\u0010\u008e\u0001\u001a\u0004\u0018\u00010u*\u00030\u0090\u00012\u0006\u0010m\u001a\u00020n2\t\u0008\u0001\u0010\u0091\u0001\u001a\u00020/2\t\u0008\u0002\u0010\u0092\u0001\u001a\u00020/H\u0086\u0008\u001aM\u0010\u0093\u0001\u001a\u0005\u0018\u0001H\u0094\u0001\"\u0011\u0008\u0000\u0010\u0094\u0001*\n\u0012\u0005\u0012\u0003H\u0094\u00010\u0095\u0001*\u00030\u0090\u00012\u0007\u0010\u0096\u0001\u001a\u00020/2\u000e\u0010\u0097\u0001\u001a\t\u0012\u0005\u0012\u0003H\u0094\u00010P2\n\u0010\u0098\u0001\u001a\u0005\u0018\u0001H\u0094\u0001\u00a2\u0006\u0003\u0010\u0099\u0001\u001aN\u0010\u0093\u0001\u001a\u0005\u0018\u0001H\u0094\u0001\"\u0011\u0008\u0000\u0010\u0094\u0001*\n\u0012\u0005\u0012\u0003H\u0094\u00010\u0095\u0001*\u00030\u0090\u00012\u0007\u0010\u0096\u0001\u001a\u00020/2\u000f\u0010\u0097\u0001\u001a\n\u0012\u0005\u0012\u0003H\u0094\u00010\u009a\u00012\n\u0010\u0098\u0001\u001a\u0005\u0018\u0001H\u0094\u0001\u00a2\u0006\u0003\u0010\u009b\u0001\u001a\u0015\u0010\u009c\u0001\u001a\u00020\u0001*\u00020K2\u0008\u0008\u0001\u0010M\u001a\u00020/\u001a\u000b\u0010\u009d\u0001\u001a\u00020/*\u00020\u0014\u001a\u000e\u0010\u009e\u0001\u001a\u00030\u009f\u0001*\u00020\u0014H\u0002\u001a\u0014\u0010\u00a0\u0001\u001a\u00020/*\u00020\u00142\u0007\u0010\u0089\u0001\u001a\u00020\u0019\u001a\u0014\u0010\u00a1\u0001\u001a\u00020/*\u00020\u00142\u0007\u0010\u0089\u0001\u001a\u00020\u0019\u001a\u0014\u0010\u00a2\u0001\u001a\u00020/*\u00020\u00142\u0007\u0010\u0089\u0001\u001a\u00020\u0019\u001a\u000b\u0010\u00a3\u0001\u001a\u00020/*\u00020\u0014\u001a\u000e\u0010\u00a4\u0001\u001a\u00020\u000f*\u00030\u00a5\u0001H\u0002\u001a\u000b\u0010\u00a6\u0001\u001a\u00020:*\u00020\u0014\u001a\u0014\u0010R\u001a\u00020\u0014*\u00020n2\u0008\u0008\u0001\u0010T\u001a\u00020/\u001a\u000e\u0010\u00a7\u0001\u001a\u00020\u000f*\u00030\u00a5\u0001H\u0002\u001a\u000b\u0010\u00a8\u0001\u001a\u00020\u000f*\u00020n\u001a\u001d\u0010&\u001a\u00020\'*\u00020\u00142\u0008\u0010\u00a9\u0001\u001a\u00030\u00aa\u00012\u0007\u0010\u008b\u0001\u001a\u00020\'\u001a\u001d\u0010*\u001a\u00020\'*\u00020\u00142\u0008\u0010\u00a9\u0001\u001a\u00030\u00aa\u00012\u0007\u0010\u008b\u0001\u001a\u00020\'\u001a\u0015\u0010\u00ab\u0001\u001a\u00020:*\u00020\u00142\u0008\u0010\u00ac\u0001\u001a\u00030\u00ad\u0001\u001a\r\u0010\u00ae\u0001\u001a\u00020:*\u00020\u0014H\u0002\u001a\r\u0010\u00af\u0001\u001a\u00020:*\u0004\u0018\u00010\u0014\u001a<\u0010\u00b0\u0001\u001a\u00020:\"\u0008\u0008\u0000\u0010S*\u00020\u0014*\u0002HS2\u001c\u0008\u0004\u0010\u00ac\u0001\u001a\u0015\u0012\u0004\u0012\u0002HS\u0012\u0004\u0012\u00020:0\u0083\u0001\u00a2\u0006\u0003\u0008\u00b1\u0001H\u0086\u0008\u00a2\u0006\u0003\u0010\u00b2\u0001\u001aA\u0010\u00b0\u0001\u001a\u00020:\"\r\u0008\u0000\u0010S*\u0007\u0012\u0002\u0008\u00030\u00b3\u0001*\u0002HS2\u001c\u0008\u0004\u0010\u00ac\u0001\u001a\u0015\u0012\u0004\u0012\u0002HS\u0012\u0004\u0012\u00020:0\u0083\u0001\u00a2\u0006\u0003\u0008\u00b1\u0001H\u0086\u0008\u00a2\u0006\u0003\u0010\u00b4\u0001\u001a\u0019\u0010\u00b5\u0001\u001a\u00020:*\u00020\u00142\u000c\u0010i\u001a\u0008\u0012\u0004\u0012\u00020:0j\u001a\u0015\u0010\u00b5\u0001\u001a\u00020:*\u00020\u00142\u0008\u0010\u00b6\u0001\u001a\u00030\u00b7\u0001\u001a\u000b\u0010\u00b8\u0001\u001a\u00020:*\u00020\u0014\u001a(\u0010\u00b9\u0001\u001a\u00020\u000f*\u00020\u00142\u0007\u0010\u00ba\u0001\u001a\u00020\u00012\u0007\u0010\u00bb\u0001\u001a\u00020\u00012\u0007\u0010\u00bc\u0001\u001a\u00020\u0001H\u0002\u001a\u001d\u0010\u00bd\u0001\u001a\u00020\u000f*\u00020\u00142\u0007\u0010\u00be\u0001\u001a\u00020\u00012\u0007\u0010\u00bf\u0001\u001a\u00020\u0001\u001a\r\u0010\u00c0\u0001\u001a\u00030\u00c1\u0001*\u00030\u00c2\u0001\u001a\u000b\u0010\u00c3\u0001\u001a\u00020:*\u00020\u0014\u001a\u0016\u0010\u00c4\u0001\u001a\u00020:*\u00020\u00142\t\u0008\u0001\u0010\u00c5\u0001\u001a\u00020/\u001a\u000b\u0010\u00c6\u0001\u001a\u00020:*\u00020\u0014\u001a\u000b\u0010\u00c7\u0001\u001a\u00020:*\u00020\u0014\u001a\u0017\u0010\u00c8\u0001\u001a\u00020:*\u00020#2\n\u0010\u00c9\u0001\u001a\u0005\u0018\u00010\u00ca\u0001\u001a\u0016\u0010\u00c8\u0001\u001a\u00020:*\u00020#2\t\u0008\u0001\u0010\u00cb\u0001\u001a\u00020/\u001a\u000b\u0010\u00cc\u0001\u001a\u00020:*\u00020\u0014\u001a\u0014\u0010\u00cd\u0001\u001a\u00020:*\u00020\u00142\u0007\u0010\u00ce\u0001\u001a\u00020\u000f\u001a\u0014\u0010\u00cf\u0001\u001a\u00020:*\u00020\u00142\u0007\u0010\u00ce\u0001\u001a\u00020\u000f\u001a\u0018\u0010\u00d0\u0001\u001a\u00020:*\u00020\u00142\t\u0008\u0002\u0010\u00d1\u0001\u001a\u00020\u000fH\u0007\u001a\u0013\u0010\u00d2\u0001\u001a\u00020:*\u00020\u00142\u0006\u0010\u007f\u001a\u00020/\u001a \u0010\u00d3\u0001\u001a\u00020:*\u00030\u0090\u00012\t\u0008\u0001\u0010\u00d4\u0001\u001a\u00020/2\u0007\u0010\u00d5\u0001\u001a\u00020#\u001a\u001a\u0010\u00d6\u0001\u001a\u0004\u0018\u00010s*\u00020\u00142\t\u0008\u0002\u0010\u00d7\u0001\u001a\u00020\u000fH\u0007\u001a\r\u0010h\u001a\u00020n*\u00020nH\u0086\u0008\u001a\u0015\u0010\u00d8\u0001\u001a\u00020:*\u00020\u00142\u0008\u0010\u00d9\u0001\u001a\u00030\u00da\u0001\u001a\u0015\u0010\u00db\u0001\u001a\u00020:*\u00020\u00142\u0008\u0010\u00d9\u0001\u001a\u00030\u00da\u0001\u001a \u0010\u00db\u0001\u001a\u00020:*\u00020\u00142\u0008\u0010\u00d9\u0001\u001a\u00030\u00da\u00012\u0007\u0010\u00dc\u0001\u001a\u00020\u000fH\u0002\"\u0016\u0010\u0000\u001a\u00020\u00018\u0000X\u0081T\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u0002\u0010\u0003\"\u0016\u0010\u0004\u001a\u00020\u00018\u0000X\u0081T\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u0005\u0010\u0003\"\u0016\u0010\u0006\u001a\n \u0008*\u0004\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u0016\u0010\t\u001a\n \u0008*\u0004\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u0016\u0010\n\u001a\u00020\u00018\u0000X\u0081T\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u000b\u0010\u0003\"\u0016\u0010\u000c\u001a\u00020\u00018\u0000X\u0081T\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\r\u0010\u0003\"\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u0015\u0010\u0012\u001a\u00020\u0013*\u00020\u00148F\u00a2\u0006\u0006\u001a\u0004\u0008\u0015\u0010\u0016\"%\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0018*\u00020\u00198\u00c6\u0002X\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u001a\u0010\u001b\u001a\u0004\u0008\u001c\u0010\u001d\"\u0015\u0010\u001e\u001a\u00020\u000f*\u00020\u00148F\u00a2\u0006\u0006\u001a\u0004\u0008\u001e\u0010\u001f\"\u0015\u0010 \u001a\u00020\u000f*\u00020\u00148F\u00a2\u0006\u0006\u001a\u0004\u0008 \u0010\u001f\"\u0015\u0010!\u001a\u00020\u000f*\u00020\u00148F\u00a2\u0006\u0006\u001a\u0004\u0008!\u0010\u001f\"\u0015\u0010\"\u001a\u00020\u000f*\u00020#8F\u00a2\u0006\u0006\u001a\u0004\u0008\"\u0010$\"\u0015\u0010%\u001a\u00020\u000f*\u00020\u00148F\u00a2\u0006\u0006\u001a\u0004\u0008%\u0010\u001f\"\u0015\u0010&\u001a\u00020\'*\u00020\u00148F\u00a2\u0006\u0006\u001a\u0004\u0008(\u0010)\"\u0015\u0010*\u001a\u00020\'*\u00020\u00148F\u00a2\u0006\u0006\u001a\u0004\u0008+\u0010)\"\u0015\u0010,\u001a\u00020\'*\u00020\u00148F\u00a2\u0006\u0006\u001a\u0004\u0008-\u0010)\"(\u00100\u001a\u00020/*\u00020#2\u0006\u0010.\u001a\u00020/8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u00081\u00102\"\u0004\u00083\u00104\"\u0015\u00105\u001a\u000206*\u00020#8F\u00a2\u0006\u0006\u001a\u0004\u00087\u00108\u00a8\u0006\u00dd\u0001"
    }
    d2 = {
        "ALMOST_OPAQUE",
        "",
        "ALMOST_OPAQUE$annotations",
        "()V",
        "ALMOST_TRANSPARENT",
        "ALMOST_TRANSPARENT$annotations",
        "COLLAPSE_WHITESPACE",
        "Ljava/util/regex/Pattern;",
        "kotlin.jvm.PlatformType",
        "COLLAPSE_WHITESPACE_IGNORE_NEWLINE",
        "OPAQUE",
        "OPAQUE$annotations",
        "TRANSPARENT",
        "TRANSPARENT$annotations",
        "noAnimationsForInstrumentation",
        "",
        "sNextGeneratedId",
        "Ljava/util/concurrent/atomic/AtomicInteger;",
        "activity",
        "Landroid/app/Activity;",
        "Landroid/view/View;",
        "getActivity",
        "(Landroid/view/View;)Landroid/app/Activity;",
        "children",
        "Lkotlin/sequences/Sequence;",
        "Landroid/view/ViewGroup;",
        "children$annotations",
        "(Landroid/view/ViewGroup;)V",
        "getChildren",
        "(Landroid/view/ViewGroup;)Lkotlin/sequences/Sequence;",
        "isAttachedToWindowCompat",
        "(Landroid/view/View;)Z",
        "isGone",
        "isInvisible",
        "isSet",
        "Landroid/widget/TextView;",
        "(Landroid/widget/TextView;)Z",
        "isVisible",
        "locationOfViewInWindow",
        "Landroid/graphics/Rect;",
        "getLocationOfViewInWindow",
        "(Landroid/view/View;)Landroid/graphics/Rect;",
        "locationOfViewOnScreen",
        "getLocationOfViewOnScreen",
        "locationOfViewOnScreenWithoutPadding",
        "getLocationOfViewOnScreenWithoutPadding",
        "limit",
        "",
        "maxLength",
        "getMaxLength",
        "(Landroid/widget/TextView;)I",
        "setMaxLength",
        "(Landroid/widget/TextView;I)V",
        "value",
        "",
        "getValue",
        "(Landroid/widget/TextView;)Ljava/lang/String;",
        "crossFade",
        "",
        "from",
        "fromListener",
        "Landroid/animation/Animator$AnimatorListener;",
        "to",
        "toListener",
        "duration",
        "",
        "disableAnimationsForInstrumentation",
        "enabled",
        "expandTouchArea",
        "bigView",
        "smallView",
        "generateViewId",
        "get9PatchSized",
        "Landroid/graphics/drawable/NinePatchDrawable;",
        "res",
        "Landroid/content/res/Resources;",
        "ninePatchId",
        "dimen",
        "getEmptyView",
        "views",
        "",
        "([Landroid/widget/TextView;)Landroid/widget/TextView;",
        "inflate",
        "T",
        "layoutResId",
        "viewGroup",
        "(ILandroid/view/ViewGroup;)Landroid/view/View;",
        "attachToRoot",
        "(ILandroid/view/ViewGroup;Z)Landroid/view/View;",
        "isDescendant",
        "parent",
        "child",
        "texts",
        "([Landroid/widget/TextView;)Z",
        "maybeFindById",
        "view",
        "id",
        "(Landroid/view/View;I)Landroid/view/View;",
        "restartInput",
        "strip",
        "s",
        "ignoreNewLines",
        "tryCopyToBitmapDrawable",
        "Landroid/graphics/drawable/BitmapDrawable;",
        "vectorFriendly",
        "block",
        "Lkotlin/Function0;",
        "(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;",
        "vibrate",
        "context",
        "Landroid/content/Context;",
        "milliseconds",
        "createOverlaidDrawable",
        "Landroid/graphics/drawable/LayerDrawable;",
        "source",
        "Landroid/graphics/Bitmap;",
        "overlayId",
        "Landroid/graphics/drawable/Drawable;",
        "doAlpha",
        "Landroid/view/animation/AlphaAnimation;",
        "fromAlpha",
        "toAlpha",
        "dpToPxRounded",
        "densityDpi",
        "endOnDetach",
        "Landroid/animation/Animator;",
        "fadeIn",
        "durationMs",
        "startDelayMs",
        "fadeOut",
        "endAction",
        "Lkotlin/Function1;",
        "fadeOutToGone",
        "fadeOutToInvisible",
        "findById",
        "(Landroid/app/Activity;I)Landroid/view/View;",
        "getBottomRelativeToHost",
        "host",
        "getBoundsRelativeToHost",
        "rect",
        "getDisplaySize",
        "Landroid/graphics/Point;",
        "getDrawableCompat",
        "Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;",
        "Landroid/content/res/TypedArray;",
        "index",
        "defaultId",
        "getEnum",
        "E",
        "",
        "styleableIndex",
        "values",
        "defaultValue",
        "(Landroid/content/res/TypedArray;I[Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/lang/Enum;",
        "",
        "(Landroid/content/res/TypedArray;ILjava/util/List;Ljava/lang/Enum;)Ljava/lang/Enum;",
        "getFloatCompat",
        "getHorizontalMargins",
        "getInputMethodManager",
        "Landroid/view/inputmethod/InputMethodManager;",
        "getLeftRelativeToHost",
        "getRightRelativeToHost",
        "getTopRelativeToHost",
        "getVerticalMargins",
        "hasHardKeyboard",
        "Landroid/content/res/Configuration;",
        "hideSoftKeyboard",
        "isHardKeyboardInUse",
        "isPortrait",
        "location",
        "",
        "notifyAttachAndDetachOnce",
        "listener",
        "Landroid/view/View$OnAttachStateChangeListener;",
        "nullViewDrawable",
        "nullViewDrawablesRecursive",
        "onClickDebounced",
        "Lkotlin/ExtensionFunctionType;",
        "(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V",
        "Landroid/widget/AdapterView;",
        "(Landroid/widget/AdapterView;Lkotlin/jvm/functions/Function1;)V",
        "onDetach",
        "runnable",
        "Ljava/lang/Runnable;",
        "performClickOnUp",
        "pointInView",
        "localX",
        "localY",
        "slop",
        "pointInViewRaw",
        "rawX",
        "rawY",
        "requireText",
        "Landroid/text/Editable;",
        "Landroid/widget/EditText;",
        "scrollToVisible",
        "setBackgroundResourceCompat",
        "backgroundId",
        "setGone",
        "setInvisible",
        "setTextAndVisibility",
        "text",
        "",
        "stringRes",
        "setVisible",
        "setVisibleOrGone",
        "visible",
        "setVisibleOrInvisible",
        "showSoftKeyboard",
        "force",
        "slideInFromLeft",
        "styleTextView",
        "resId",
        "textView",
        "tryCopyToBitmap",
        "resetViewPressedState",
        "waitForHeightMeasure",
        "callback",
        "Lcom/squareup/util/OnMeasuredCallback;",
        "waitForMeasure",
        "heightOnly",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ALMOST_OPAQUE:F = 0.999f

.field public static final ALMOST_TRANSPARENT:F = 0.001f

.field private static final COLLAPSE_WHITESPACE:Ljava/util/regex/Pattern;

.field private static final COLLAPSE_WHITESPACE_IGNORE_NEWLINE:Ljava/util/regex/Pattern;

.field public static final OPAQUE:F = 1.0f

.field public static final TRANSPARENT:F

.field private static noAnimationsForInstrumentation:Z

.field private static final sNextGeneratedId:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 75
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/squareup/util/Views;->sNextGeneratedId:Ljava/util/concurrent/atomic/AtomicInteger;

    const-string v0, "\\s+"

    .line 77
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/squareup/util/Views;->COLLAPSE_WHITESPACE:Ljava/util/regex/Pattern;

    const-string v0, "[^\\S\\r\\n]+"

    .line 78
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/squareup/util/Views;->COLLAPSE_WHITESPACE_IGNORE_NEWLINE:Ljava/util/regex/Pattern;

    return-void
.end method

.method public static synthetic ALMOST_OPAQUE$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic ALMOST_TRANSPARENT$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic OPAQUE$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic TRANSPARENT$annotations()V
    .locals 0

    return-void
.end method

.method public static final synthetic access$pointInView(Landroid/view/View;FFF)Z
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/util/Views;->pointInView(Landroid/view/View;FFF)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;Z)V

    return-void
.end method

.method public static synthetic children$annotations(Landroid/view/ViewGroup;)V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Use ViewGroup.children property from AndroidX KTX instead."
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = "children"
            imports = {
                "androidx.core.view.children"
            }
        .end subannotation
    .end annotation

    return-void
.end method

.method public static final createOverlaidDrawable(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/drawable/LayerDrawable;
    .locals 1

    const-string v0, "$this$createOverlaidDrawable"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, p0, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-static {p0, v0, p2}, Lcom/squareup/util/Views;->createOverlaidDrawable(Landroid/content/res/Resources;Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/LayerDrawable;

    move-result-object p0

    return-object p0
.end method

.method public static final createOverlaidDrawable(Landroid/content/res/Resources;Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/LayerDrawable;
    .locals 2

    const-string v0, "$this$createOverlaidDrawable"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    .line 125
    new-instance p2, Landroid/graphics/drawable/LayerDrawable;

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p0, v0, p1

    invoke-direct {p2, v0}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    return-object p2
.end method

.method public static final crossFade(Landroid/view/View;Landroid/animation/Animator$AnimatorListener;Landroid/view/View;Landroid/animation/Animator$AnimatorListener;J)V
    .locals 2

    const-string v0, "from"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "to"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "toListener"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 327
    invoke-virtual {p2, v0}, Landroid/view/View;->setAlpha(F)V

    .line 328
    invoke-static {p2}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 330
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p2

    const/high16 v1, 0x3f800000    # 1.0f

    .line 331
    invoke-virtual {p2, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p2

    .line 332
    invoke-virtual {p2, p4, p5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p2

    .line 333
    invoke-virtual {p2, p3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 334
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    .line 335
    invoke-virtual {p0, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    .line 336
    invoke-virtual {p0, p4, p5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    .line 337
    invoke-virtual {p0, p1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method public static final crossFade(Landroid/view/View;Landroid/view/View;J)V
    .locals 7

    const-string v0, "from"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "to"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 313
    new-instance v0, Lcom/squareup/util/Views$crossFade$1;

    invoke-direct {v0, p0}, Lcom/squareup/util/Views$crossFade$1;-><init>(Landroid/view/View;)V

    move-object v4, v0

    check-cast v4, Landroid/animation/Animator$AnimatorListener;

    const/4 v2, 0x0

    move-object v1, p0

    move-object v3, p1

    move-wide v5, p2

    invoke-static/range {v1 .. v6}, Lcom/squareup/util/Views;->crossFade(Landroid/view/View;Landroid/animation/Animator$AnimatorListener;Landroid/view/View;Landroid/animation/Animator$AnimatorListener;J)V

    return-void
.end method

.method public static final disableAnimationsForInstrumentation(Z)V
    .locals 0

    .line 92
    sput-boolean p0, Lcom/squareup/util/Views;->noAnimationsForInstrumentation:Z

    return-void
.end method

.method public static final doAlpha(Landroid/view/View;FFI)Landroid/view/animation/AlphaAnimation;
    .locals 1

    const-string v0, "$this$doAlpha"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 300
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p1, p2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    int-to-long p1, p3

    .line 301
    invoke-virtual {v0, p1, p2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    const/4 p1, 0x1

    .line 302
    invoke-virtual {v0, p1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 303
    move-object p1, v0

    check-cast p1, Landroid/view/animation/Animation;

    invoke-virtual {p0, p1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-object v0
.end method

.method public static final dpToPxRounded(FI)I
    .locals 1

    int-to-float p1, p1

    const/16 v0, 0xa0

    int-to-float v0, v0

    div-float/2addr p1, v0

    mul-float p0, p0, p1

    const/high16 p1, 0x3f000000    # 0.5f

    add-float/2addr p0, p1

    float-to-int p0, p0

    return p0
.end method

.method public static final endOnDetach(Landroid/animation/Animator;Landroid/view/View;)V
    .locals 2

    if-eqz p0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 746
    :cond_0
    new-instance v0, Lcom/squareup/util/Views$endOnDetach$attachListener$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/util/Views$endOnDetach$attachListener$1;-><init>(Landroid/animation/Animator;Landroid/view/View;)V

    .line 754
    move-object v1, v0

    check-cast v1, Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {p1, v1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 756
    new-instance v1, Lcom/squareup/util/Views$endOnDetach$1;

    invoke-direct {v1, p1, v0}, Lcom/squareup/util/Views$endOnDetach$1;-><init>(Landroid/view/View;Lcom/squareup/util/Views$endOnDetach$attachListener$1;)V

    check-cast v1, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {p0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public static final expandTouchArea(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    const-string v0, "bigView"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "smallView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 603
    new-instance v0, Lcom/squareup/util/Views$expandTouchArea$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/util/Views$expandTouchArea$1;-><init>(Landroid/view/View;Landroid/view/View;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public static final fadeIn(Landroid/view/View;I)V
    .locals 1

    const-string v0, "$this$fadeIn"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 838
    invoke-static {p0, v0, p1}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;II)V

    return-void
.end method

.method public static final fadeIn(Landroid/view/View;II)V
    .locals 2

    const-string v0, "$this$fadeIn"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 845
    invoke-static {p0}, Lcom/squareup/util/Views;->isVisible(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x3a83126f    # 0.001f

    .line 847
    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 848
    invoke-static {p0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 850
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getAlpha()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 851
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    .line 852
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    check-cast v0, Landroid/animation/TimeInterpolator;

    invoke-virtual {p0, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    .line 853
    invoke-virtual {p0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    int-to-long v0, p1

    .line 854
    invoke-virtual {p0, v0, v1}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    int-to-long p1, p2

    .line 855
    invoke-virtual {p0, p1, p2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    .line 856
    invoke-virtual {p0}, Landroid/view/ViewPropertyAnimator;->start()V

    :cond_1
    return-void
.end method

.method public static final fadeOut(Landroid/view/View;IILkotlin/jvm/functions/Function1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "II",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$fadeOut"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 880
    invoke-virtual {p0}, Landroid/view/View;->getAlpha()F

    move-result v0

    const/4 v1, 0x0

    cmpg-float v2, v0, v1

    if-eqz v2, :cond_2

    .line 881
    invoke-static {p0}, Lcom/squareup/util/Views;->isVisible(Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v2

    if-nez v0, :cond_1

    const v0, 0x3f7fbe77    # 0.999f

    .line 886
    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 888
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 889
    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    check-cast v2, Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 890
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v1, p1

    .line 891
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    int-to-long v0, p2

    .line 892
    invoke-virtual {p1, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 893
    new-instance p2, Lcom/squareup/util/Views$fadeOut$1;

    invoke-direct {p2, p0, p3}, Lcom/squareup/util/Views$fadeOut$1;-><init>(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    check-cast p2, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    .line 907
    invoke-virtual {p0}, Landroid/view/ViewPropertyAnimator;->start()V

    :cond_2
    :goto_0
    return-void
.end method

.method public static final fadeOut(Landroid/view/View;ILkotlin/jvm/functions/Function1;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "I",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/squareup/util/Views;->fadeOut$default(Landroid/view/View;IILkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static synthetic fadeOut$default(Landroid/view/View;IILkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x1

    if-eqz p4, :cond_0

    const/4 p1, 0x0

    .line 876
    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/util/Views;->fadeOut(Landroid/view/View;IILkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static final fadeOutToGone(Landroid/view/View;I)V
    .locals 7

    const-string v0, "$this$fadeOutToGone"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 865
    sget-object v0, Lcom/squareup/util/Views$fadeOutToGone$1;->INSTANCE:Lcom/squareup/util/Views$fadeOutToGone$1;

    move-object v4, v0

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p0

    move v3, p1

    invoke-static/range {v1 .. v6}, Lcom/squareup/util/Views;->fadeOut$default(Landroid/view/View;IILkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final fadeOutToInvisible(Landroid/view/View;I)V
    .locals 7

    const-string v0, "$this$fadeOutToInvisible"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 861
    sget-object v0, Lcom/squareup/util/Views$fadeOutToInvisible$1;->INSTANCE:Lcom/squareup/util/Views$fadeOutToInvisible$1;

    move-object v4, v0

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p0

    move v3, p1

    invoke-static/range {v1 .. v6}, Lcom/squareup/util/Views;->fadeOut$default(Landroid/view/View;IILkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final findById(Landroid/app/Activity;I)Landroid/view/View;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/app/Activity;",
            "I)TT;"
        }
    .end annotation

    const-string v0, "$this$findById"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 249
    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 252
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object p0

    .line 253
    new-instance p1, Ljava/lang/NullPointerException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot find view "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static final findById(Landroid/view/View;I)Landroid/view/View;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/View;",
            "I)TT;"
        }
    .end annotation

    const-string v0, "$this$findById"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 230
    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 233
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object p0

    .line 234
    new-instance p1, Ljava/lang/NullPointerException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot find view "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static final generateViewId()I
    .locals 3

    .line 105
    :cond_0
    sget-object v0, Lcom/squareup/util/Views;->sNextGeneratedId:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    add-int/lit8 v1, v0, 0x1

    const v2, 0xffffff

    if-le v1, v2, :cond_1

    const/4 v1, 0x1

    .line 109
    :cond_1
    sget-object v2, Lcom/squareup/util/Views;->sNextGeneratedId:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v1

    if-eqz v1, :cond_0

    return v0
.end method

.method public static final get9PatchSized(Landroid/content/res/Resources;II)Landroid/graphics/drawable/NinePatchDrawable;
    .locals 1

    const-string v0, "res"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 185
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    if-eqz p0, :cond_0

    check-cast p0, Landroid/graphics/drawable/NinePatchDrawable;

    .line 186
    new-instance p1, Landroid/graphics/Rect;

    const/4 v0, 0x0

    invoke-direct {p1, v0, v0, p2, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 187
    invoke-virtual {p0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(Landroid/graphics/Rect;)V

    return-object p0

    .line 185
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type android.graphics.drawable.NinePatchDrawable"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final getActivity(Landroid/view/View;)Landroid/app/Activity;
    .locals 1

    const-string v0, "$this$activity"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 535
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/util/Contexts;->getActivity(Landroid/content/Context;)Landroid/app/Activity;

    move-result-object p0

    if-nez p0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    return-object p0
.end method

.method public static final getBottomRelativeToHost(Landroid/view/View;Landroid/view/ViewGroup;)I
    .locals 3

    const-string v0, "$this$getBottomRelativeToHost"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "host"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 1022
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Landroid/view/View;->getTranslationY()F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    add-int/2addr v0, v1

    .line 1023
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p0

    if-eqz p0, :cond_1

    check-cast p0, Landroid/view/View;

    if-ne p1, p0, :cond_0

    return v0

    :cond_1
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type android.view.View"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final getBoundsRelativeToHost(Landroid/view/View;Landroid/view/ViewGroup;Landroid/graphics/Rect;)V
    .locals 5

    const-string v0, "$this$getBoundsRelativeToHost"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "host"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rect"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1040
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    .line 1041
    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    .line 1039
    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1044
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    .line 1045
    invoke-virtual {p0}, Landroid/view/View;->getTranslationX()F

    move-result v1

    .line 1047
    iget v2, p2, Landroid/graphics/Rect;->left:I

    float-to-int v1, v1

    add-int/2addr v2, v1

    iput v2, p2, Landroid/graphics/Rect;->left:I

    .line 1048
    iget v2, p2, Landroid/graphics/Rect;->top:I

    float-to-int v0, v0

    add-int/2addr v2, v0

    iput v2, p2, Landroid/graphics/Rect;->top:I

    .line 1049
    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v0

    iput v2, p2, Landroid/graphics/Rect;->bottom:I

    .line 1050
    iget v0, p2, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->right:I

    .line 1052
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p0

    if-eqz p0, :cond_1

    check-cast p0, Landroid/view/View;

    if-ne p1, p0, :cond_0

    return-void

    :cond_1
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type android.view.View"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final getChildren(Landroid/view/ViewGroup;)Lkotlin/sequences/Sequence;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")",
            "Lkotlin/sequences/Sequence<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$children"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1163
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v1, v0}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v0

    new-instance v1, Lcom/squareup/util/Views$children$1;

    invoke-direct {v1, p0}, Lcom/squareup/util/Views$children$1;-><init>(Landroid/view/ViewGroup;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/sequences/SequencesKt;->map(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p0

    return-object p0
.end method

.method public static final getDisplaySize(Landroid/content/Context;)Landroid/graphics/Point;
    .locals 1

    const-string v0, "$this$getDisplaySize"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "window"

    .line 341
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_0

    check-cast p0, Landroid/view/WindowManager;

    .line 342
    invoke-interface {p0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p0

    .line 343
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 344
    invoke-virtual {p0, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    return-object v0

    .line 341
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.WindowManager"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static final getDrawableCompat(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;
    .locals 0

    .line 1146
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    const-string p1, "getDrawable(overlayId)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getDrawableCompat(Landroid/content/res/TypedArray;Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;
    .locals 1

    const-string v0, "$this$getDrawableCompat"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1154
    invoke-virtual {p0, p2, p3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 1155
    :cond_0
    invoke-static {p1, p0}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static final getDrawableCompat(Landroid/content/Context;I)Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;
    .locals 1

    const-string v0, "$this$getDrawableCompat"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 710
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p0

    invoke-static {v0, p1, p0}, Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;->create(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;

    move-result-object p0

    if-nez p0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string p1, "VectorDrawableCompat.cre\u2026e(resources, id, theme)!!"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static synthetic getDrawableCompat$default(Landroid/content/res/TypedArray;Landroid/content/Context;IIILjava/lang/Object;)Landroid/graphics/drawable/Drawable;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    :cond_0
    const-string p4, "$this$getDrawableCompat"

    .line 1152
    invoke-static {p0, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "context"

    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1359
    invoke-virtual {p0, p2, p3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p0

    if-nez p0, :cond_1

    const/4 p0, 0x0

    goto :goto_0

    .line 1360
    :cond_1
    invoke-static {p1, p0}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static final varargs getEmptyView([Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 4

    const-string v0, "views"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1348
    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v2, p0, v1

    .line 636
    invoke-static {v2}, Lcom/squareup/util/Views;->isSet(Landroid/widget/TextView;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_1
    return-object v2
.end method

.method public static final getEnum(Landroid/content/res/TypedArray;ILjava/util/List;Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Enum<",
            "TE;>;>(",
            "Landroid/content/res/TypedArray;",
            "I",
            "Ljava/util/List<",
            "+TE;>;TE;)TE;"
        }
    .end annotation

    const-string v0, "$this$getEnum"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "values"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, -0x1

    .line 274
    invoke-virtual {p0, p1, v0}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result p0

    if-gez p0, :cond_0

    goto :goto_0

    .line 277
    :cond_0
    invoke-interface {p2, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    move-object p3, p0

    check-cast p3, Ljava/lang/Enum;

    :goto_0
    return-object p3
.end method

.method public static final getEnum(Landroid/content/res/TypedArray;I[Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Enum<",
            "TE;>;>(",
            "Landroid/content/res/TypedArray;",
            "I[TE;TE;)TE;"
        }
    .end annotation

    const-string v0, "$this$getEnum"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "values"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, -0x1

    .line 263
    invoke-virtual {p0, p1, v0}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result p0

    if-gez p0, :cond_0

    goto :goto_0

    .line 266
    :cond_0
    aget-object p3, p2, p0

    :goto_0
    return-object p3
.end method

.method public static final getFloatCompat(Landroid/content/res/Resources;I)F
    .locals 2

    const-string v0, "$this$getFloatCompat"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 716
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1d

    if-lt v0, v1, :cond_0

    .line 717
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getFloat(I)F

    move-result p0

    return p0

    .line 719
    :cond_0
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    invoke-virtual {v0}, Landroid/util/TypedValue;->getFloat()F

    move-result p0

    return p0
.end method

.method public static final getHorizontalMargins(Landroid/view/View;)I
    .locals 1

    const-string v0, "$this$getHorizontalMargins"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 585
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p0

    if-eqz p0, :cond_0

    check-cast p0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 586
    invoke-virtual {p0}, Landroid/view/ViewGroup$MarginLayoutParams;->getMarginStart()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/ViewGroup$MarginLayoutParams;->getMarginEnd()I

    move-result p0

    add-int/2addr v0, p0

    return v0

    .line 585
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static final getInputMethodManager(Landroid/view/View;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1

    .line 464
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_0

    check-cast p0, Landroid/view/inputmethod/InputMethodManager;

    return-object p0

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.inputmethod.InputMethodManager"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final getLeftRelativeToHost(Landroid/view/View;Landroid/view/ViewGroup;)I
    .locals 3

    const-string v0, "$this$getLeftRelativeToHost"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "host"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 992
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Landroid/view/View;->getTranslationX()F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    add-int/2addr v0, v1

    .line 993
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p0

    if-eqz p0, :cond_1

    check-cast p0, Landroid/view/View;

    if-ne p1, p0, :cond_0

    return v0

    :cond_1
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type android.view.View"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final getLocationOfViewInWindow(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 2

    const-string v0, "$this$locationOfViewInWindow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 955
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-static {p0, v0, v1}, Lcom/squareup/util/Views;->locationOfViewInWindow(Landroid/view/View;[ILandroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object p0

    return-object p0
.end method

.method public static final getLocationOfViewOnScreen(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 2

    const-string v0, "$this$locationOfViewOnScreen"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 931
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-static {p0, v0, v1}, Lcom/squareup/util/Views;->locationOfViewOnScreen(Landroid/view/View;[ILandroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object p0

    return-object p0
.end method

.method public static final getLocationOfViewOnScreenWithoutPadding(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 3

    const-string v0, "$this$locationOfViewOnScreenWithoutPadding"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 980
    invoke-static {p0}, Lcom/squareup/util/Views;->getLocationOfViewOnScreen(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 981
    iget v1, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 982
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 983
    iget v1, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 984
    iget v1, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result p0

    sub-int/2addr v1, p0

    iput v1, v0, Landroid/graphics/Rect;->right:I

    return-object v0
.end method

.method public static final getMaxLength(Landroid/widget/TextView;)I
    .locals 5

    const-string v0, "$this$maxLength"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1116
    invoke-virtual {p0}, Landroid/widget/TextView;->getFilters()[Landroid/text/InputFilter;

    move-result-object p0

    const-string v0, "filters"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1350
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 1357
    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, p0, v2

    instance-of v4, v3, Landroid/text/InputFilter$LengthFilter;

    if-eqz v4, :cond_0

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1358
    :cond_1
    check-cast v0, Ljava/util/List;

    .line 1116
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/text/InputFilter$LengthFilter;

    if-eqz p0, :cond_2

    invoke-virtual {p0}, Landroid/text/InputFilter$LengthFilter;->getMax()I

    move-result p0

    goto :goto_1

    :cond_2
    const/4 p0, -0x1

    :goto_1
    return p0
.end method

.method public static final getRightRelativeToHost(Landroid/view/View;Landroid/view/ViewGroup;)I
    .locals 3

    const-string v0, "$this$getRightRelativeToHost"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "host"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 1002
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Landroid/view/View;->getTranslationX()F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    add-int/2addr v0, v1

    .line 1003
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p0

    if-eqz p0, :cond_1

    check-cast p0, Landroid/view/View;

    if-ne p1, p0, :cond_0

    return v0

    :cond_1
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type android.view.View"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final getTopRelativeToHost(Landroid/view/View;Landroid/view/ViewGroup;)I
    .locals 3

    const-string v0, "$this$getTopRelativeToHost"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "host"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 1012
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Landroid/view/View;->getTranslationY()F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    add-int/2addr v0, v1

    .line 1013
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p0

    if-eqz p0, :cond_1

    check-cast p0, Landroid/view/View;

    if-ne p1, p0, :cond_0

    return v0

    :cond_1
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type android.view.View"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final getValue(Landroid/widget/TextView;)Ljava/lang/String;
    .locals 2

    const-string v0, "$this$value"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 618
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/widget/TextView;->getMaxLines()I

    move-result p0

    const/4 v1, 0x1

    if-le p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->strip(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final getVerticalMargins(Landroid/view/View;)I
    .locals 1

    const-string v0, "$this$getVerticalMargins"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 579
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p0

    if-eqz p0, :cond_0

    check-cast p0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 580
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget p0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, p0

    return v0

    .line 579
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static final hasHardKeyboard(Landroid/content/res/Configuration;)Z
    .locals 1

    .line 469
    iget p0, p0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static final hideSoftKeyboard(Landroid/view/View;)V
    .locals 2

    const-string v0, "$this$hideSoftKeyboard"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 349
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 350
    invoke-virtual {p0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object p0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    return-void

    .line 349
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.inputmethod.InputMethodManager"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I",
            "Landroid/view/ViewGroup;",
            ")TT;"
        }
    .end annotation

    const-string v0, "viewGroup"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 680
    invoke-static {p0, p1, v0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I",
            "Landroid/view/ViewGroup;",
            "Z)TT;"
        }
    .end annotation

    const-string v0, "viewGroup"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 693
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 694
    invoke-virtual {v0, p0, p1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type T"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final inflate(Landroid/content/Context;I)Landroid/view/View;
    .locals 1

    const-string v0, "$this$inflate"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 704
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p0

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p0

    const-string p1, "LayoutInflater.from(this\u2026nflate(layoutResId, null)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static synthetic inflate$default(ILandroid/view/ViewGroup;ZILjava/lang/Object;)Landroid/view/View;
    .locals 0

    and-int/lit8 p3, p3, 0x4

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 691
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final isAttachedToWindowCompat(Landroid/view/View;)Z
    .locals 1

    const-string v0, "$this$isAttachedToWindowCompat"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1095
    invoke-static {p0}, Landroidx/core/view/ViewCompat;->isAttachedToWindow(Landroid/view/View;)Z

    move-result p0

    return p0
.end method

.method public static final isDescendant(Landroid/view/View;Landroid/view/View;)Z
    .locals 1

    const-string v0, "parent"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "child"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 195
    instance-of v0, p0, Landroid/view/ViewParent;

    if-eqz v0, :cond_1

    .line 196
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    :goto_0
    if-eqz p1, :cond_1

    if-ne p1, p0, :cond_0

    const/4 p0, 0x1

    return p0

    .line 201
    :cond_0
    invoke-interface {p1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method public static final isGone(Landroid/view/View;)Z
    .locals 1

    const-string v0, "$this$isGone"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 794
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result p0

    const/16 v0, 0x8

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static final isHardKeyboardInUse(Landroid/content/res/Configuration;)Z
    .locals 1

    .line 474
    iget p0, p0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static final isInvisible(Landroid/view/View;)Z
    .locals 1

    const-string v0, "$this$isInvisible"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 789
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result p0

    const/4 v0, 0x4

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final isPortrait(Landroid/content/Context;)Z
    .locals 1

    const-string v0, "$this$isPortrait"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 608
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const-string v0, "resources"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p0

    .line 609
    iget p0, p0, Landroid/content/res/Configuration;->orientation:I

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static final isSet(Landroid/widget/TextView;)Z
    .locals 1

    const-string v0, "$this$isSet"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 630
    invoke-static {p0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    return p0
.end method

.method public static final varargs isSet([Landroid/widget/TextView;)Z
    .locals 4

    const-string v0, "texts"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1346
    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    aget-object v3, p0, v2

    .line 633
    invoke-static {v3}, Lcom/squareup/util/Views;->isSet(Landroid/widget/TextView;)Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    :goto_1
    return v1
.end method

.method public static final isVisible(Landroid/view/View;)Z
    .locals 1

    const-string v0, "$this$isVisible"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 784
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final locationOfViewInWindow(Landroid/view/View;[ILandroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 3

    const-string v0, "$this$locationOfViewInWindow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rect"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 965
    invoke-virtual {p0, p1}, Landroid/view/View;->getLocationInWindow([I)V

    const/4 v0, 0x0

    .line 967
    aget v1, p1, v0

    iput v1, p2, Landroid/graphics/Rect;->left:I

    const/4 v1, 0x1

    .line 968
    aget v2, p1, v1

    iput v2, p2, Landroid/graphics/Rect;->top:I

    .line 969
    aget v0, p1, v0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v0, v2

    iput v0, p2, Landroid/graphics/Rect;->right:I

    .line 970
    aget p1, p1, v1

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p0

    add-int/2addr p1, p0

    iput p1, p2, Landroid/graphics/Rect;->bottom:I

    return-object p2
.end method

.method public static final locationOfViewOnScreen(Landroid/view/View;[ILandroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 3

    const-string v0, "$this$locationOfViewOnScreen"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rect"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 941
    invoke-virtual {p0, p1}, Landroid/view/View;->getLocationOnScreen([I)V

    const/4 v0, 0x0

    .line 943
    aget v1, p1, v0

    iput v1, p2, Landroid/graphics/Rect;->left:I

    const/4 v1, 0x1

    .line 944
    aget v2, p1, v1

    iput v2, p2, Landroid/graphics/Rect;->top:I

    .line 945
    aget v0, p1, v0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v0, v2

    iput v0, p2, Landroid/graphics/Rect;->right:I

    .line 946
    aget p1, p1, v1

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p0

    add-int/2addr p1, p0

    iput p1, p2, Landroid/graphics/Rect;->bottom:I

    return-object p2
.end method

.method public static final maybeFindById(Landroid/view/View;I)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/View;",
            "I)TT;"
        }
    .end annotation

    const-string v0, "view"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 218
    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final noAnimationsForInstrumentation()Z
    .locals 1

    .line 89
    sget-boolean v0, Lcom/squareup/util/Views;->noAnimationsForInstrumentation:Z

    return v0
.end method

.method public static final notifyAttachAndDetachOnce(Landroid/view/View;Landroid/view/View$OnAttachStateChangeListener;)V
    .locals 1

    const-string v0, "$this$notifyAttachAndDetachOnce"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1081
    invoke-static {p0}, Lcom/squareup/util/Views;->isAttachedToWindowCompat(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1083
    invoke-interface {p1, p0}, Landroid/view/View$OnAttachStateChangeListener;->onViewAttachedToWindow(Landroid/view/View;)V

    .line 1084
    new-instance v0, Lcom/squareup/util/Views$notifyAttachAndDetachOnce$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/util/Views$notifyAttachAndDetachOnce$1;-><init>(Landroid/view/View;Landroid/view/View$OnAttachStateChangeListener;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->onDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    .line 1087
    :cond_0
    invoke-virtual {p0, p1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 1088
    new-instance v0, Lcom/squareup/util/Views$notifyAttachAndDetachOnce$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/util/Views$notifyAttachAndDetachOnce$2;-><init>(Landroid/view/View;Landroid/view/View$OnAttachStateChangeListener;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->onDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    :goto_0
    return-void
.end method

.method private static final nullViewDrawable(Landroid/view/View;)V
    .locals 2

    .line 1140
    instance-of v0, p0, Landroid/widget/ImageView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object v0, v1

    goto :goto_0

    :cond_0
    move-object v0, p0

    :goto_0
    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1141
    :cond_1
    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public static final nullViewDrawablesRecursive(Landroid/view/View;)V
    .locals 5

    if-eqz p0, :cond_1

    .line 654
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 655
    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, 0x0

    .line 658
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_0

    .line 660
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const-string v4, "viewGroup.getChildAt(i)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 661
    invoke-static {v3}, Lcom/squareup/util/Views;->nullViewDrawablesRecursive(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 665
    :cond_0
    invoke-static {p0}, Lcom/squareup/util/Views;->nullViewDrawable(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method public static final onClickDebounced(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(TT;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$onClickDebounced"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1103
    new-instance v0, Lcom/squareup/util/Views$onClickDebounced$1;

    invoke-direct {v0, p1}, Lcom/squareup/util/Views$onClickDebounced$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public static final onClickDebounced(Landroid/widget/AdapterView;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/AdapterView<",
            "*>;>(TT;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$onClickDebounced"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1126
    new-instance v0, Lcom/squareup/util/Views$onClickDebounced$2;

    invoke-direct {v0, p1}, Lcom/squareup/util/Views$onClickDebounced$2;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {p0, v0}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method public static final onDetach(Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 1

    const-string v0, "$this$onDetach"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "runnable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1064
    new-instance v0, Lcom/squareup/util/Views$onDetach$1;

    invoke-direct {v0, p1}, Lcom/squareup/util/Views$onDetach$1;-><init>(Ljava/lang/Runnable;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->onDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public static final onDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$onDetach"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1070
    new-instance v0, Lcom/squareup/util/Views$onDetach$2;

    invoke-direct {v0, p1}, Lcom/squareup/util/Views$onDetach$2;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {p0, v0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    return-void
.end method

.method public static final performClickOnUp(Landroid/view/View;)V
    .locals 2

    const-string v0, "$this$performClickOnUp"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 369
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    const-string v1, "ViewConfiguration.get(context)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    int-to-float v0, v0

    .line 371
    new-instance v1, Lcom/squareup/util/Views$performClickOnUp$1;

    invoke-direct {v1, v0}, Lcom/squareup/util/Views$performClickOnUp$1;-><init>(F)V

    check-cast v1, Landroid/view/View$OnTouchListener;

    invoke-virtual {p0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method private static final pointInView(Landroid/view/View;FFF)Z
    .locals 2

    neg-float v0, p3

    cmpl-float v1, p1, v0

    if-ltz v1, :cond_0

    cmpl-float v0, p2, v0

    if-ltz v0, :cond_0

    .line 417
    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    add-float/2addr v0, p3

    cmpg-float p1, p1, v0

    if-gez p1, :cond_0

    .line 418
    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result p1

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result p0

    sub-int/2addr p1, p0

    int-to-float p0, p1

    add-float/2addr p0, p3

    cmpg-float p0, p2, p0

    if-gez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final pointInViewRaw(Landroid/view/View;FF)Z
    .locals 5

    const-string v0, "$this$pointInViewRaw"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 429
    invoke-virtual {p0, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    const/4 v1, 0x0

    .line 431
    aget v2, v0, v1

    int-to-float v2, v2

    const/4 v3, 0x1

    cmpg-float v2, v2, p1

    if-gtz v2, :cond_0

    .line 430
    aget v2, v0, v1

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v2, v4

    int-to-float v2, v2

    cmpg-float p1, p1, v2

    if-gez p1, :cond_0

    aget p1, v0, v3

    int-to-float p1, p1

    cmpg-float p1, p1, p2

    if-gtz p1, :cond_0

    .line 431
    aget p1, v0, v3

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p0

    add-int/2addr p1, p0

    int-to-float p0, p1

    cmpg-float p0, p2, p0

    if-gez p0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public static final requireText(Landroid/widget/EditText;)Landroid/text/Editable;
    .locals 1

    const-string v0, "$this$requireText"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1320
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p0

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "The text is not editable or hasn\'t been initialized, yet."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static final restartInput(Landroid/view/View;)V
    .locals 1

    const-string v0, "view"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 459
    invoke-static {p0}, Lcom/squareup/util/Views;->getInputMethodManager(Landroid/view/View;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    .line 460
    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    return-void
.end method

.method public static final scrollToVisible(Landroid/view/View;)V
    .locals 1

    const-string v0, "$this$scrollToVisible"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 479
    new-instance v0, Lcom/squareup/util/Views$scrollToVisible$1;

    invoke-direct {v0, p0}, Lcom/squareup/util/Views$scrollToVisible$1;-><init>(Landroid/view/View;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public static final setBackgroundResourceCompat(Landroid/view/View;I)V
    .locals 4

    const-string v0, "$this$setBackgroundResourceCompat"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 640
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    .line 641
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    .line 642
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    .line 643
    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    .line 644
    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 645
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method

.method public static final setGone(Landroid/view/View;)V
    .locals 1

    const-string v0, "$this$setGone"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0x8

    .line 814
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public static final setInvisible(Landroid/view/View;)V
    .locals 1

    const-string v0, "$this$setInvisible"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x4

    .line 807
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public static final setMaxLength(Landroid/widget/TextView;I)V
    .locals 2

    const-string v0, "$this$maxLength"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/text/InputFilter$LengthFilter;

    .line 1113
    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v1, p1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    const/4 p1, 0x0

    aput-object v1, v0, p1

    check-cast v0, [Landroid/text/InputFilter;

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    return-void
.end method

.method public static final setTextAndVisibility(Landroid/widget/TextView;I)V
    .locals 3

    const-string v0, "$this$setTextAndVisibility"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 777
    move-object v0, p0

    check-cast v0, Landroid/view/View;

    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eq p1, v1, :cond_1

    .line 778
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void
.end method

.method public static final setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 3

    const-string v0, "$this$setTextAndVisibility"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 768
    move-object v0, p0

    check-cast v0, Landroid/view/View;

    const/4 v1, 0x1

    if-eqz p1, :cond_1

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x1

    :goto_1
    xor-int/2addr v1, v2

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 769
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static final setVisible(Landroid/view/View;)V
    .locals 1

    const-string v0, "$this$setVisible"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 800
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public static final setVisibleOrGone(Landroid/view/View;Z)V
    .locals 1

    const-string v0, "$this$setVisibleOrGone"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 822
    invoke-static {p0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    :goto_0
    return-void
.end method

.method public static final setVisibleOrInvisible(Landroid/view/View;Z)V
    .locals 1

    const-string v0, "$this$setVisibleOrInvisible"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 830
    invoke-static {p0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lcom/squareup/util/Views;->setInvisible(Landroid/view/View;)V

    :goto_0
    return-void
.end method

.method public static final showSoftKeyboard(Landroid/view/View;)V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/squareup/util/Views;->showSoftKeyboard$default(Landroid/view/View;ZILjava/lang/Object;)V

    return-void
.end method

.method public static final showSoftKeyboard(Landroid/view/View;Z)V
    .locals 7

    const-string v0, "$this$showSoftKeyboard"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 441
    invoke-static {p0}, Lcom/squareup/util/Views;->getInputMethodManager(Landroid/view/View;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x2

    if-eqz p1, :cond_0

    .line 443
    invoke-virtual {v0, v2, v1}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInput(II)V

    .line 449
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v3, "context"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const-string v4, "context.resources"

    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    const-string v5, "context.resources.configuration"

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/util/Views;->hasHardKeyboard(Landroid/content/res/Configuration;)Z

    move-result p1

    .line 450
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/squareup/util/Views;->isHardKeyboardInUse(Landroid/content/res/Configuration;)Z

    move-result v3

    if-eqz p1, :cond_1

    if-nez v3, :cond_1

    .line 452
    invoke-virtual {v0, p0, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_0

    :cond_1
    if-nez p1, :cond_2

    .line 454
    invoke-virtual {v0, p0, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    :cond_2
    :goto_0
    return-void
.end method

.method public static synthetic showSoftKeyboard$default(Landroid/view/View;ZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 440
    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;Z)V

    return-void
.end method

.method public static final slideInFromLeft(Landroid/view/View;I)V
    .locals 10

    const-string v0, "$this$slideInFromLeft"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 914
    invoke-static {p0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 916
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v2, 0x1

    const/high16 v3, -0x40800000    # -1.0f

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 920
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    check-cast v1, Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    int-to-long v1, p1

    .line 921
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 919
    check-cast v0, Landroid/view/animation/Animation;

    .line 915
    invoke-virtual {p0, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public static final strip(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 7

    const-string v0, "s"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1323
    check-cast p0, Ljava/lang/CharSequence;

    .line 1325
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    const/4 v2, 0x0

    move v3, v0

    const/4 v0, 0x0

    const/4 v4, 0x0

    :goto_0
    if-gt v0, v3, :cond_5

    if-nez v4, :cond_0

    move v5, v0

    goto :goto_1

    :cond_0
    move v5, v3

    .line 1330
    :goto_1
    invoke-interface {p0, v5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    const/16 v6, 0x20

    if-gt v5, v6, :cond_1

    const/4 v5, 0x1

    goto :goto_2

    :cond_1
    const/4 v5, 0x0

    :goto_2
    if-nez v4, :cond_3

    if-nez v5, :cond_2

    const/4 v4, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    if-nez v5, :cond_4

    goto :goto_3

    :cond_4
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    :cond_5
    :goto_3
    add-int/2addr v3, v1

    .line 1345
    invoke-interface {p0, v0, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p0

    .line 1323
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, " "

    if-eqz p1, :cond_6

    .line 625
    check-cast p0, Ljava/lang/CharSequence;

    sget-object p1, Lcom/squareup/util/Views;->COLLAPSE_WHITESPACE_IGNORE_NEWLINE:Ljava/util/regex/Pattern;

    const-string v1, "COLLAPSE_WHITESPACE_IGNORE_NEWLINE"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1, v0}, Lcom/squareup/util/Strings;->replaceAll(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_4

    .line 626
    :cond_6
    check-cast p0, Ljava/lang/CharSequence;

    sget-object p1, Lcom/squareup/util/Views;->COLLAPSE_WHITESPACE:Ljava/util/regex/Pattern;

    const-string v1, "COLLAPSE_WHITESPACE"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1, v0}, Lcom/squareup/util/Strings;->replaceAll(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    :goto_4
    return-object p0
.end method

.method public static synthetic strip$default(Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;
    .locals 0

    and-int/lit8 p2, p2, 0x2

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 622
    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/util/Views;->strip(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final styleTextView(Landroid/content/res/TypedArray;ILandroid/widget/TextView;)V
    .locals 1

    const-string v0, "$this$styleTextView"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "textView"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 731
    invoke-virtual {p0, p1, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p0

    if-eqz p0, :cond_0

    .line 733
    invoke-static {p2, p0}, Landroidx/core/widget/TextViewCompat;->setTextAppearance(Landroid/widget/TextView;I)V

    :cond_0
    return-void
.end method

.method public static final tryCopyToBitmap(Landroid/view/View;)Landroid/graphics/Bitmap;
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/squareup/util/Views;->tryCopyToBitmap$default(Landroid/view/View;ZILjava/lang/Object;)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

.method public static final tryCopyToBitmap(Landroid/view/View;Z)Landroid/graphics/Bitmap;
    .locals 5

    const-string v0, "$this$tryCopyToBitmap"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 137
    invoke-virtual {p0, v0}, Landroid/view/View;->setPressed(Z)V

    :cond_0
    const/4 p1, 0x0

    .line 145
    :try_start_0
    invoke-virtual {p0}, Landroid/view/View;->buildDrawingCache()V

    const/4 v0, 0x1

    .line 148
    invoke-virtual {p0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 154
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 155
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v4, 0x0

    .line 157
    invoke-virtual {v3, v1, v4, v4, p1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 164
    :try_start_1
    invoke-virtual {p0}, Landroid/view/View;->destroyDrawingCache()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    return-object v2

    :cond_1
    :try_start_2
    invoke-virtual {p0}, Landroid/view/View;->destroyDrawingCache()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    return-object p1

    :catchall_2
    move-exception p1

    if-eqz v0, :cond_2

    :try_start_3
    invoke-virtual {p0}, Landroid/view/View;->destroyDrawingCache()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 167
    :catchall_3
    :cond_2
    throw p1

    :catch_0
    nop

    if-eqz v0, :cond_3

    .line 164
    :try_start_4
    invoke-virtual {p0}, Landroid/view/View;->destroyDrawingCache()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    :catchall_4
    :cond_3
    return-object p1
.end method

.method public static synthetic tryCopyToBitmap$default(Landroid/view/View;ZILjava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 135
    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/util/Views;->tryCopyToBitmap(Landroid/view/View;Z)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

.method public static final tryCopyToBitmapDrawable(Landroid/view/View;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 2

    const-string v0, "view"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 176
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->tryCopyToBitmap(Landroid/view/View;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 177
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-direct {v1, p0, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v1

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static final vectorFriendly(Landroid/content/Context;)Landroid/content/Context;
    .locals 1

    const-string v0, "$this$vectorFriendly"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1261
    sget-object v0, Lcom/squareup/util/VectorFriendly;->INSTANCE:Lcom/squareup/util/VectorFriendly;

    invoke-virtual {v0, p0}, Lcom/squareup/util/VectorFriendly;->ensureContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method public static final vectorFriendly(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function0<",
            "+TT;>;)TT;"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1228
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1230
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v2, Lcom/squareup/util/VectorFriendly;->INSTANCE:Lcom/squareup/util/VectorFriendly;

    invoke-virtual {v2}, Lcom/squareup/util/VectorFriendly;->getVECTOR_CAPABLE_VERSION()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1231
    invoke-static {}, Landroidx/appcompat/app/AppCompatDelegate;->isCompatVectorFromResourcesEnabled()Z

    move-result v0

    .line 1232
    invoke-static {v1}, Landroidx/appcompat/app/AppCompatDelegate;->setCompatVectorFromResourcesEnabled(Z)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const/4 v2, 0x0

    .line 1235
    :try_start_0
    invoke-interface {p0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v1}, Lkotlin/jvm/internal/InlineMarker;->finallyStart(I)V

    .line 1237
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v4, Lcom/squareup/util/VectorFriendly;->INSTANCE:Lcom/squareup/util/VectorFriendly;

    invoke-virtual {v4}, Lcom/squareup/util/VectorFriendly;->getVECTOR_CAPABLE_VERSION()I

    move-result v4

    if-ge v3, v4, :cond_1

    if-nez v0, :cond_1

    .line 1238
    invoke-static {v2}, Landroidx/appcompat/app/AppCompatDelegate;->setCompatVectorFromResourcesEnabled(Z)V

    :cond_1
    invoke-static {v1}, Lkotlin/jvm/internal/InlineMarker;->finallyEnd(I)V

    return-object p0

    :catchall_0
    move-exception p0

    .line 1240
    invoke-static {v1}, Lkotlin/jvm/internal/InlineMarker;->finallyStart(I)V

    .line 1237
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v4, Lcom/squareup/util/VectorFriendly;->INSTANCE:Lcom/squareup/util/VectorFriendly;

    invoke-virtual {v4}, Lcom/squareup/util/VectorFriendly;->getVECTOR_CAPABLE_VERSION()I

    move-result v4

    if-ge v3, v4, :cond_2

    if-nez v0, :cond_2

    .line 1238
    invoke-static {v2}, Landroidx/appcompat/app/AppCompatDelegate;->setCompatVectorFromResourcesEnabled(Z)V

    :cond_2
    invoke-static {v1}, Lkotlin/jvm/internal/InlineMarker;->finallyEnd(I)V

    throw p0
.end method

.method public static final vibrate(Landroid/content/Context;)V
    .locals 4

    const-wide/16 v0, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {p0, v0, v1, v2, v3}, Lcom/squareup/util/Views;->vibrate$default(Landroid/content/Context;JILjava/lang/Object;)V

    return-void
.end method

.method public static final vibrate(Landroid/content/Context;J)V
    .locals 1

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "vibrator"

    .line 358
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_0

    check-cast p0, Landroid/os/Vibrator;

    .line 359
    invoke-virtual {p0, p1, p2}, Landroid/os/Vibrator;->vibrate(J)V

    return-void

    .line 358
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type android.os.Vibrator"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static synthetic vibrate$default(Landroid/content/Context;JILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const-wide/16 p1, 0xfa

    .line 356
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/squareup/util/Views;->vibrate(Landroid/content/Context;J)V

    return-void
.end method

.method public static final waitForHeightMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V
    .locals 1

    const-string v0, "$this$waitForHeightMeasure"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 512
    invoke-static {p0, p1, v0}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;Z)V

    return-void
.end method

.method public static final waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V
    .locals 1

    const-string v0, "$this$waitForMeasure"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 506
    invoke-static {p0, p1, v0}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;Z)V

    return-void
.end method

.method private static final waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;Z)V
    .locals 2

    .line 518
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 519
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    if-nez p2, :cond_0

    if-lez v0, :cond_1

    :cond_0
    if-lez v1, :cond_1

    .line 522
    invoke-interface {p1, p0, v0, v1}, Lcom/squareup/util/OnMeasuredCallback;->onMeasured(Landroid/view/View;II)V

    return-void

    .line 526
    :cond_1
    new-instance v0, Lcom/squareup/util/LayoutListener;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/util/LayoutListener;-><init>(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;Z)V

    .line 527
    invoke-virtual {p0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    const-string p2, "viewTreeObserver"

    .line 528
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result p2

    if-eqz p2, :cond_2

    .line 529
    move-object p2, v0

    check-cast p2, Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {p1, p2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 530
    check-cast v0, Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {p0, v0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    :cond_2
    return-void
.end method
