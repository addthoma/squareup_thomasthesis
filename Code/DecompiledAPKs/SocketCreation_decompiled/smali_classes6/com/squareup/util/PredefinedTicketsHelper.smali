.class public Lcom/squareup/util/PredefinedTicketsHelper;
.super Ljava/lang/Object;
.source "PredefinedTicketsHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildTicketGroup(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Lcom/squareup/api/items/TicketGroup;
    .locals 2

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Cannot build TicketGroup from null LibraryEntry."

    .line 22
    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 23
    new-instance v0, Lcom/squareup/api/items/TicketGroup$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/TicketGroup$Builder;-><init>()V

    .line 24
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/TicketGroup$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/TicketGroup$Builder;

    move-result-object v0

    .line 25
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/TicketGroup$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/TicketGroup$Builder;

    move-result-object v0

    .line 26
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getOrdinal()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/api/items/TicketGroup$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/TicketGroup$Builder;

    move-result-object p0

    .line 27
    invoke-virtual {p0}, Lcom/squareup/api/items/TicketGroup$Builder;->build()Lcom/squareup/api/items/TicketGroup;

    move-result-object p0

    return-object p0
.end method

.method public static buildTicketGroupOrNull(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Lcom/squareup/api/items/TicketGroup;
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 18
    :cond_0
    invoke-static {p0}, Lcom/squareup/util/PredefinedTicketsHelper;->buildTicketGroup(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Lcom/squareup/api/items/TicketGroup;

    move-result-object p0

    return-object p0
.end method

.method public static buildTicketTemplateOrNull(Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;)Lcom/squareup/api/items/TicketTemplate;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 35
    :cond_0
    new-instance v0, Lcom/squareup/api/items/TicketTemplate$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/TicketTemplate$Builder;-><init>()V

    .line 36
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/TicketTemplate$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/TicketTemplate$Builder;

    move-result-object v0

    .line 37
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/TicketTemplate$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/TicketTemplate$Builder;

    move-result-object v0

    .line 38
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getOrdinal()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/api/items/TicketTemplate$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/TicketTemplate$Builder;

    move-result-object p0

    .line 39
    invoke-virtual {p0}, Lcom/squareup/api/items/TicketTemplate$Builder;->build()Lcom/squareup/api/items/TicketTemplate;

    move-result-object p0

    return-object p0
.end method
