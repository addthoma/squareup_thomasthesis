.class public final Lcom/squareup/util/Views$fadeOut$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/Views;->fadeOut(Landroid/view/View;IILkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001f\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0010\u0010\u0008\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "com/squareup/util/Views$fadeOut$1",
        "Landroid/animation/AnimatorListenerAdapter;",
        "canceled",
        "",
        "onAnimationCancel",
        "",
        "animator",
        "Landroid/animation/Animator;",
        "onAnimationEnd",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $endAction:Lkotlin/jvm/functions/Function1;

.field final synthetic $this_fadeOut:Landroid/view/View;

.field private canceled:Z


# direct methods
.method constructor <init>(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    .line 893
    iput-object p1, p0, Lcom/squareup/util/Views$fadeOut$1;->$this_fadeOut:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/util/Views$fadeOut$1;->$endAction:Lkotlin/jvm/functions/Function1;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1

    const-string v0, "animator"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 904
    iput-boolean p1, p0, Lcom/squareup/util/Views$fadeOut$1;->canceled:Z

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    const-string v0, "animator"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 898
    iget-boolean p1, p0, Lcom/squareup/util/Views$fadeOut$1;->canceled:Z

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/squareup/util/Views$fadeOut$1;->$this_fadeOut:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getAlpha()F

    move-result p1

    const/4 v0, 0x0

    cmpg-float p1, p1, v0

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/squareup/util/Views$fadeOut$1;->$endAction:Lkotlin/jvm/functions/Function1;

    if-eqz p1, :cond_0

    .line 899
    iget-object v0, p0, Lcom/squareup/util/Views$fadeOut$1;->$this_fadeOut:Landroid/view/View;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
