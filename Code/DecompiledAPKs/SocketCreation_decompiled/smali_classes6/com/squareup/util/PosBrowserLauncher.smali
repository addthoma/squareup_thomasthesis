.class public final Lcom/squareup/util/PosBrowserLauncher;
.super Ljava/lang/Object;
.source "PosBrowserLauncher.java"

# interfaces
.implements Lcom/squareup/util/BrowserLauncher;


# instance fields
.field private activity:Landroid/app/Activity;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dropActivity(Landroid/app/Activity;)V
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/util/PosBrowserLauncher;->activity:Landroid/app/Activity;

    if-eq v0, p1, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 p1, 0x0

    .line 23
    iput-object p1, p0, Lcom/squareup/util/PosBrowserLauncher;->activity:Landroid/app/Activity;

    :cond_1
    return-void
.end method

.method public launchBrowser(I)V
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/squareup/util/PosBrowserLauncher;->activity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "not attached to an activity."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/util/PosBrowserLauncher;->activity:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/util/RegisterIntents;->internalLaunchBrowser(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public launchBrowser(Ljava/lang/String;)V
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/util/PosBrowserLauncher;->activity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "not attached to an activity."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcom/squareup/util/PosBrowserLauncher;->activity:Landroid/app/Activity;

    invoke-static {v0, p1}, Lcom/squareup/util/RegisterIntents;->internalLaunchBrowser(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public takeActivity(Landroid/app/Activity;)V
    .locals 0

    .line 18
    iput-object p1, p0, Lcom/squareup/util/PosBrowserLauncher;->activity:Landroid/app/Activity;

    return-void
.end method
