.class public final Lcom/squareup/util/X2Build;
.super Ljava/lang/Object;
.source "X2Build.java"


# static fields
.field private static final SQUARE_MANUFACTURER_NAME:Ljava/lang/String; = "Square"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isSquareDevice()Z
    .locals 2

    .line 9
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Square"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
