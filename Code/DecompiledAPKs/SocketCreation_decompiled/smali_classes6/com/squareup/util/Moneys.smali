.class public final Lcom/squareup/util/Moneys;
.super Ljava/lang/Object;
.source "Moneys.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMoneys.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Moneys.kt\ncom/squareup/util/Moneys\n*L\n1#1,37:1\n33#1,4:38\n35#1:42\n34#1:43\n33#1,4:44\n35#1:48\n34#1:49\n*E\n*S KotlinDebug\n*F\n+ 1 Moneys.kt\ncom/squareup/util/Moneys\n*L\n14#1,4:38\n14#1:42\n14#1:43\n21#1,4:44\n21#1:48\n21#1:49\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0006\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002\"\u0016\u0010\u0000\u001a\u00020\u0001*\u00020\u00028\u00c6\u0002\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0016\u0010\u0000\u001a\u00020\u0001*\u00020\u00058\u00c6\u0002\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0006\"\u0016\u0010\u0000\u001a\u00020\u0001*\u00020\u00078\u00c6\u0002\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "USD",
        "Lcom/squareup/protos/connect/v2/common/Money;",
        "",
        "getUSD",
        "(D)Lcom/squareup/protos/connect/v2/common/Money;",
        "",
        "(I)Lcom/squareup/protos/connect/v2/common/Money;",
        "",
        "(J)Lcom/squareup/protos/connect/v2/common/Money;",
        "public"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getUSD(D)Lcom/squareup/protos/connect/v2/common/Money;
    .locals 3

    .line 33
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Money$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/Money$Builder;-><init>()V

    const/16 v1, 0x64

    int-to-double v1, v1

    mul-double p0, p0, v1

    .line 34
    invoke-static {p0, p1}, Lkotlin/math/MathKt;->roundToLong(D)J

    move-result-wide p0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/connect/v2/common/Money$Builder;->amount(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/common/Money$Builder;

    move-result-object p0

    .line 35
    sget-object p1, Lcom/squareup/protos/connect/v2/common/Currency;->USD:Lcom/squareup/protos/connect/v2/common/Currency;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/common/Money$Builder;->currency(Lcom/squareup/protos/connect/v2/common/Currency;)Lcom/squareup/protos/connect/v2/common/Money$Builder;

    move-result-object p0

    .line 36
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/Money$Builder;->build()Lcom/squareup/protos/connect/v2/common/Money;

    move-result-object p0

    const-string p1, "Money.Builder()\n      .a\u2026rency.USD)\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getUSD(I)Lcom/squareup/protos/connect/v2/common/Money;
    .locals 4

    int-to-double v0, p0

    .line 44
    new-instance p0, Lcom/squareup/protos/connect/v2/common/Money$Builder;

    invoke-direct {p0}, Lcom/squareup/protos/connect/v2/common/Money$Builder;-><init>()V

    const/16 v2, 0x64

    int-to-double v2, v2

    mul-double v0, v0, v2

    .line 49
    invoke-static {v0, v1}, Lkotlin/math/MathKt;->roundToLong(D)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/protos/connect/v2/common/Money$Builder;->amount(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/common/Money$Builder;

    move-result-object p0

    .line 48
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->USD:Lcom/squareup/protos/connect/v2/common/Currency;

    invoke-virtual {p0, v0}, Lcom/squareup/protos/connect/v2/common/Money$Builder;->currency(Lcom/squareup/protos/connect/v2/common/Currency;)Lcom/squareup/protos/connect/v2/common/Money$Builder;

    move-result-object p0

    .line 47
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/Money$Builder;->build()Lcom/squareup/protos/connect/v2/common/Money;

    move-result-object p0

    const-string v0, "Money.Builder()\n      .a\u2026rency.USD)\n      .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getUSD(J)Lcom/squareup/protos/connect/v2/common/Money;
    .locals 3

    long-to-double p0, p0

    .line 38
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Money$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/Money$Builder;-><init>()V

    const/16 v1, 0x64

    int-to-double v1, v1

    mul-double p0, p0, v1

    .line 43
    invoke-static {p0, p1}, Lkotlin/math/MathKt;->roundToLong(D)J

    move-result-wide p0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/connect/v2/common/Money$Builder;->amount(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/common/Money$Builder;

    move-result-object p0

    .line 42
    sget-object p1, Lcom/squareup/protos/connect/v2/common/Currency;->USD:Lcom/squareup/protos/connect/v2/common/Currency;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/common/Money$Builder;->currency(Lcom/squareup/protos/connect/v2/common/Currency;)Lcom/squareup/protos/connect/v2/common/Money$Builder;

    move-result-object p0

    .line 41
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/Money$Builder;->build()Lcom/squareup/protos/connect/v2/common/Money;

    move-result-object p0

    const-string p1, "Money.Builder()\n      .a\u2026rency.USD)\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
