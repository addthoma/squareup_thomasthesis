.class public final Lcom/squareup/util/RxTuples;
.super Ljava/lang/Object;
.source "RxTuples.java"


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static expandPair(Lrx/functions/Action2;)Lrx/functions/Action1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/Action2<",
            "TA;TB;>;)",
            "Lrx/functions/Action1<",
            "Lkotlin/Pair<",
            "TA;TB;>;>;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/util/-$$Lambda$RxTuples$eo5y8tLx-Yq2XtH5kcOpBkzOcAA;

    invoke-direct {v0, p0}, Lcom/squareup/util/-$$Lambda$RxTuples$eo5y8tLx-Yq2XtH5kcOpBkzOcAA;-><init>(Lrx/functions/Action2;)V

    return-object v0
.end method

.method public static expandPairForFunc(Lrx/functions/Func2;)Lrx/functions/Func1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/Func2<",
            "TA;TB;TR;>;)",
            "Lrx/functions/Func1<",
            "Lkotlin/Pair<",
            "TA;TB;>;TR;>;"
        }
    .end annotation

    .line 51
    new-instance v0, Lcom/squareup/util/-$$Lambda$RxTuples$VOWmzDf8dOZvxWYt_TG7sraxyI0;

    invoke-direct {v0, p0}, Lcom/squareup/util/-$$Lambda$RxTuples$VOWmzDf8dOZvxWYt_TG7sraxyI0;-><init>(Lrx/functions/Func2;)V

    return-object v0
.end method

.method public static expandQuartet(Lrx/functions/Action4;)Lrx/functions/Action1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            "C:",
            "Ljava/lang/Object;",
            "D:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/Action4<",
            "TA;TB;TC;TD;>;)",
            "Lrx/functions/Action1<",
            "Lcom/squareup/util/tuple/Quartet<",
            "TA;TB;TC;TD;>;>;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/util/-$$Lambda$RxTuples$TCiz4wn4Q3m7QrT_vrMwPggSQ8c;

    invoke-direct {v0, p0}, Lcom/squareup/util/-$$Lambda$RxTuples$TCiz4wn4Q3m7QrT_vrMwPggSQ8c;-><init>(Lrx/functions/Action4;)V

    return-object v0
.end method

.method public static expandQuartetForFunc(Lrx/functions/Func4;)Lrx/functions/Func1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            "C:",
            "Ljava/lang/Object;",
            "D:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/Func4<",
            "TA;TB;TC;TD;TR;>;)",
            "Lrx/functions/Func1<",
            "Lcom/squareup/util/tuple/Quartet<",
            "TA;TB;TC;TD;>;TR;>;"
        }
    .end annotation

    .line 61
    new-instance v0, Lcom/squareup/util/-$$Lambda$RxTuples$D9Zs26dPuMtRXpxnvknsPN9P4GM;

    invoke-direct {v0, p0}, Lcom/squareup/util/-$$Lambda$RxTuples$D9Zs26dPuMtRXpxnvknsPN9P4GM;-><init>(Lrx/functions/Func4;)V

    return-object v0
.end method

.method public static expandTriplet(Lrx/functions/Action3;)Lrx/functions/Action1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            "C:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/Action3<",
            "TA;TB;TC;>;)",
            "Lrx/functions/Action1<",
            "Lcom/squareup/util/tuple/Triplet<",
            "TA;TB;TC;>;>;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/util/-$$Lambda$RxTuples$A_XFoegzIV_0JDMD7HuPsIJPJnQ;

    invoke-direct {v0, p0}, Lcom/squareup/util/-$$Lambda$RxTuples$A_XFoegzIV_0JDMD7HuPsIJPJnQ;-><init>(Lrx/functions/Action3;)V

    return-object v0
.end method

.method public static expandTripletForFunc(Lrx/functions/Func3;)Lrx/functions/Func1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            "C:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/Func3<",
            "TA;TB;TC;TR;>;)",
            "Lrx/functions/Func1<",
            "Lcom/squareup/util/tuple/Triplet<",
            "TA;TB;TC;>;TR;>;"
        }
    .end annotation

    .line 56
    new-instance v0, Lcom/squareup/util/-$$Lambda$RxTuples$SE1lpERbbENmLM1NAjiFVNeCI0o;

    invoke-direct {v0, p0}, Lcom/squareup/util/-$$Lambda$RxTuples$SE1lpERbbENmLM1NAjiFVNeCI0o;-><init>(Lrx/functions/Func3;)V

    return-object v0
.end method

.method static synthetic lambda$expandPair$0(Lrx/functions/Action2;Lkotlin/Pair;)V
    .locals 1

    .line 32
    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    invoke-interface {p0, v0, p1}, Lrx/functions/Action2;->call(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$expandPairForFunc$3(Lrx/functions/Func2;Lkotlin/Pair;)Ljava/lang/Object;
    .locals 1

    .line 51
    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    invoke-interface {p0, v0, p1}, Lrx/functions/Func2;->call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$expandQuartet$2(Lrx/functions/Action4;Lcom/squareup/util/tuple/Quartet;)V
    .locals 3

    .line 41
    iget-object v0, p1, Lcom/squareup/util/tuple/Quartet;->first:Ljava/lang/Object;

    iget-object v1, p1, Lcom/squareup/util/tuple/Quartet;->second:Ljava/lang/Object;

    iget-object v2, p1, Lcom/squareup/util/tuple/Quartet;->third:Ljava/lang/Object;

    iget-object p1, p1, Lcom/squareup/util/tuple/Quartet;->fourth:Ljava/lang/Object;

    invoke-interface {p0, v0, v1, v2, p1}, Lrx/functions/Action4;->call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$expandQuartetForFunc$5(Lrx/functions/Func4;Lcom/squareup/util/tuple/Quartet;)Ljava/lang/Object;
    .locals 3

    .line 61
    iget-object v0, p1, Lcom/squareup/util/tuple/Quartet;->first:Ljava/lang/Object;

    iget-object v1, p1, Lcom/squareup/util/tuple/Quartet;->second:Ljava/lang/Object;

    iget-object v2, p1, Lcom/squareup/util/tuple/Quartet;->third:Ljava/lang/Object;

    iget-object p1, p1, Lcom/squareup/util/tuple/Quartet;->fourth:Ljava/lang/Object;

    invoke-interface {p0, v0, v1, v2, p1}, Lrx/functions/Func4;->call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$expandTriplet$1(Lrx/functions/Action3;Lcom/squareup/util/tuple/Triplet;)V
    .locals 2

    .line 36
    iget-object v0, p1, Lcom/squareup/util/tuple/Triplet;->first:Ljava/lang/Object;

    iget-object v1, p1, Lcom/squareup/util/tuple/Triplet;->second:Ljava/lang/Object;

    iget-object p1, p1, Lcom/squareup/util/tuple/Triplet;->third:Ljava/lang/Object;

    invoke-interface {p0, v0, v1, p1}, Lrx/functions/Action3;->call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$expandTripletForFunc$4(Lrx/functions/Func3;Lcom/squareup/util/tuple/Triplet;)Ljava/lang/Object;
    .locals 2

    .line 56
    iget-object v0, p1, Lcom/squareup/util/tuple/Triplet;->first:Ljava/lang/Object;

    iget-object v1, p1, Lcom/squareup/util/tuple/Triplet;->second:Ljava/lang/Object;

    iget-object p1, p1, Lcom/squareup/util/tuple/Triplet;->third:Ljava/lang/Object;

    invoke-interface {p0, v0, v1, p1}, Lrx/functions/Func3;->call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$mapSecond$10(Lrx/functions/Func1;Lkotlin/Pair;)Lkotlin/Pair;
    .locals 2

    .line 113
    new-instance v0, Lkotlin/Pair;

    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    invoke-interface {p0, p1}, Lrx/functions/Func1;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method static synthetic lambda$pairWithSecond$9(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;
    .locals 1

    .line 108
    new-instance v0, Lkotlin/Pair;

    invoke-direct {v0, p1, p0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method static synthetic lambda$toQuartetFromSingle$8(Ljava/lang/Object;Lcom/squareup/util/tuple/Triplet;)Lcom/squareup/util/tuple/Quartet;
    .locals 3

    .line 91
    new-instance v0, Lcom/squareup/util/tuple/Quartet;

    iget-object v1, p1, Lcom/squareup/util/tuple/Triplet;->first:Ljava/lang/Object;

    iget-object v2, p1, Lcom/squareup/util/tuple/Triplet;->second:Ljava/lang/Object;

    iget-object p1, p1, Lcom/squareup/util/tuple/Triplet;->third:Ljava/lang/Object;

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/squareup/util/tuple/Quartet;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method static synthetic lambda$toTripletFromPair$7(Lkotlin/Pair;Ljava/lang/Object;)Lcom/squareup/util/tuple/Triplet;
    .locals 2

    .line 81
    new-instance v0, Lcom/squareup/util/tuple/Triplet;

    invoke-virtual {p0}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p0

    invoke-direct {v0, v1, p0, p1}, Lcom/squareup/util/tuple/Triplet;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method static synthetic lambda$toTripletFromSingle$6(Ljava/lang/Object;Lkotlin/Pair;)Lcom/squareup/util/tuple/Triplet;
    .locals 2

    .line 77
    new-instance v0, Lcom/squareup/util/tuple/Triplet;

    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    invoke-direct {v0, p0, v1, p1}, Lcom/squareup/util/tuple/Triplet;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static mapSecond(Lrx/functions/Func1;)Lrx/functions/Func1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<F:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            "S2:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/Func1<",
            "TS;TS2;>;)",
            "Lrx/functions/Func1<",
            "Lkotlin/Pair<",
            "TF;TS;>;",
            "Lkotlin/Pair<",
            "TF;TS2;>;>;"
        }
    .end annotation

    .line 113
    new-instance v0, Lcom/squareup/util/-$$Lambda$RxTuples$GZ-3Q4c-9ocy6hkAy0BJNnTBzlY;

    invoke-direct {v0, p0}, Lcom/squareup/util/-$$Lambda$RxTuples$GZ-3Q4c-9ocy6hkAy0BJNnTBzlY;-><init>(Lrx/functions/Func1;)V

    return-object v0
.end method

.method public static pairWithSecond(Ljava/lang/Object;)Lrx/functions/Func1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<F:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(TS;)",
            "Lrx/functions/Func1<",
            "TF;",
            "Lkotlin/Pair<",
            "TF;TS;>;>;"
        }
    .end annotation

    .line 108
    new-instance v0, Lcom/squareup/util/-$$Lambda$RxTuples$MYDiNUZ-zhV-UjK0fKYpppDLfyk;

    invoke-direct {v0, p0}, Lcom/squareup/util/-$$Lambda$RxTuples$MYDiNUZ-zhV-UjK0fKYpppDLfyk;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static toPair()Lrx/functions/Func2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/functions/Func2<",
            "TA;TB;",
            "Lkotlin/Pair<",
            "TA;TB;>;>;"
        }
    .end annotation

    .line 67
    sget-object v0, Lcom/squareup/util/-$$Lambda$nGGwAQAGXz1gqlIFfxvT0kocKEo;->INSTANCE:Lcom/squareup/util/-$$Lambda$nGGwAQAGXz1gqlIFfxvT0kocKEo;

    return-object v0
.end method

.method public static toQuartet()Lrx/functions/Func4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            "C:",
            "Ljava/lang/Object;",
            "D:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/functions/Func4<",
            "TA;TB;TC;TD;",
            "Lcom/squareup/util/tuple/Quartet<",
            "TA;TB;TC;TD;>;>;"
        }
    .end annotation

    .line 87
    sget-object v0, Lcom/squareup/util/-$$Lambda$kK0rsxjtT49WVjycRyriRgmWOsU;->INSTANCE:Lcom/squareup/util/-$$Lambda$kK0rsxjtT49WVjycRyriRgmWOsU;

    return-object v0
.end method

.method public static toQuartetFromSingle()Lrx/functions/Func2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            "C:",
            "Ljava/lang/Object;",
            "D:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/functions/Func2<",
            "TD;",
            "Lcom/squareup/util/tuple/Triplet<",
            "TA;TB;TC;>;",
            "Lcom/squareup/util/tuple/Quartet<",
            "TD;TA;TB;TC;>;>;"
        }
    .end annotation

    .line 91
    sget-object v0, Lcom/squareup/util/-$$Lambda$RxTuples$0VcnZ8MA4HBLxPNfAdKmCEP8xc4;->INSTANCE:Lcom/squareup/util/-$$Lambda$RxTuples$0VcnZ8MA4HBLxPNfAdKmCEP8xc4;

    return-object v0
.end method

.method public static toQuintuple()Lrx/functions/Func5;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            "C:",
            "Ljava/lang/Object;",
            "D:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/functions/Func5<",
            "TA;TB;TC;TD;TE;",
            "Lcom/squareup/util/tuple/Quintuple<",
            "TA;TB;TC;TD;TE;>;>;"
        }
    .end annotation

    .line 97
    sget-object v0, Lcom/squareup/util/-$$Lambda$AZajg5T6-rT0cLKBEsKeH4CooTI;->INSTANCE:Lcom/squareup/util/-$$Lambda$AZajg5T6-rT0cLKBEsKeH4CooTI;

    return-object v0
.end method

.method public static toSextuple()Lrx/functions/Func6;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            "C:",
            "Ljava/lang/Object;",
            "D:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "F:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/functions/Func6<",
            "TA;TB;TC;TD;TE;TF;",
            "Lcom/squareup/util/tuple/Sextuple<",
            "TA;TB;TC;TD;TE;TF;>;>;"
        }
    .end annotation

    .line 104
    sget-object v0, Lcom/squareup/util/-$$Lambda$oOHQBlh59o4-MmOQn4JDH7VtmJc;->INSTANCE:Lcom/squareup/util/-$$Lambda$oOHQBlh59o4-MmOQn4JDH7VtmJc;

    return-object v0
.end method

.method public static toTriplet()Lrx/functions/Func3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            "C:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/functions/Func3<",
            "TA;TB;TC;",
            "Lcom/squareup/util/tuple/Triplet<",
            "TA;TB;TC;>;>;"
        }
    .end annotation

    .line 73
    sget-object v0, Lcom/squareup/util/-$$Lambda$RYgXLhN5Wd__04om7syi-b6s_y8;->INSTANCE:Lcom/squareup/util/-$$Lambda$RYgXLhN5Wd__04om7syi-b6s_y8;

    return-object v0
.end method

.method public static toTripletFromPair()Lrx/functions/Func2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            "C:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/functions/Func2<",
            "Lkotlin/Pair<",
            "TA;TB;>;TC;",
            "Lcom/squareup/util/tuple/Triplet<",
            "TA;TB;TC;>;>;"
        }
    .end annotation

    .line 81
    sget-object v0, Lcom/squareup/util/-$$Lambda$RxTuples$O4wLpmHEFskl-13sobbmXnmIB0A;->INSTANCE:Lcom/squareup/util/-$$Lambda$RxTuples$O4wLpmHEFskl-13sobbmXnmIB0A;

    return-object v0
.end method

.method public static toTripletFromSingle()Lrx/functions/Func2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            "C:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/functions/Func2<",
            "TC;",
            "Lkotlin/Pair<",
            "TA;TB;>;",
            "Lcom/squareup/util/tuple/Triplet<",
            "TC;TA;TB;>;>;"
        }
    .end annotation

    .line 77
    sget-object v0, Lcom/squareup/util/-$$Lambda$RxTuples$NDN6M0lFbhdQ9PqDrS5jwfJr-uU;->INSTANCE:Lcom/squareup/util/-$$Lambda$RxTuples$NDN6M0lFbhdQ9PqDrS5jwfJr-uU;

    return-object v0
.end method
