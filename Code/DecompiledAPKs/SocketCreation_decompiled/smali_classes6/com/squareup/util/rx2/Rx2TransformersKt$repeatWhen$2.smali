.class final Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;
.super Ljava/lang/Object;
.source "Rx2Transformers.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx2/Rx2TransformersKt;->repeatWhen(Lio/reactivex/Observable;Lio/reactivex/Observable;JJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "Lio/reactivex/Observable<",
        "Ljava/lang/Object;",
        ">;",
        "Lio/reactivex/ObservableSource<",
        "*>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u0001\"\u0008\u0008\u0000\u0010\u0004*\u00020\u00052\u0014\u0010\u0006\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00050\u00050\u0001H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "",
        "kotlin.jvm.PlatformType",
        "T",
        "",
        "completed",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $maxIdleTime:J

.field final synthetic $minIdleTime:J

.field final synthetic $refreshRequested:Lio/reactivex/Observable;

.field final synthetic $scheduler:Lio/reactivex/Scheduler;

.field final synthetic $timeUnit:Ljava/util/concurrent/TimeUnit;


# direct methods
.method constructor <init>(Lio/reactivex/Observable;JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;J)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;->$refreshRequested:Lio/reactivex/Observable;

    iput-wide p2, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;->$minIdleTime:J

    iput-object p4, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;->$timeUnit:Ljava/util/concurrent/TimeUnit;

    iput-object p5, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;->$scheduler:Lio/reactivex/Scheduler;

    iput-wide p6, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;->$maxIdleTime:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Object;",
            ">;)",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "completed"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 242
    new-instance v0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2$1;

    invoke-direct {v0, p0}, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2$1;-><init>(Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lio/reactivex/Observable;

    invoke-virtual {p0, p1}, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$2;->apply(Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
