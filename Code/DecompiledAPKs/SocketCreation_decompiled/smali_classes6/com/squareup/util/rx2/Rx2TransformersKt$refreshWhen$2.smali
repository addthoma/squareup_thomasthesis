.class final Lcom/squareup/util/rx2/Rx2TransformersKt$refreshWhen$2;
.super Ljava/lang/Object;
.source "Rx2Transformers.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx2/Rx2TransformersKt;->refreshWhen(Lio/reactivex/Observable;Lio/reactivex/Observable;JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0005\u0010\u0000\u001a\u0012\u0012\u000e\u0008\u0001\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u0001\"\u0008\u0008\u0000\u0010\u0004*\u00020\u00022\u0006\u0010\u0005\u001a\u0002H\u0004H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "",
        "kotlin.jvm.PlatformType",
        "T",
        "it",
        "apply",
        "(Ljava/lang/Object;)Lio/reactivex/Observable;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $minWaitTime:J

.field final synthetic $onRefresh:Lio/reactivex/Observable;

.field final synthetic $scheduler:Lio/reactivex/Scheduler;

.field final synthetic $timeUnit:Ljava/util/concurrent/TimeUnit;


# direct methods
.method constructor <init>(Lio/reactivex/Observable;JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$refreshWhen$2;->$onRefresh:Lio/reactivex/Observable;

    iput-wide p2, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$refreshWhen$2;->$minWaitTime:J

    iput-object p4, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$refreshWhen$2;->$timeUnit:Ljava/util/concurrent/TimeUnit;

    iput-object p5, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$refreshWhen$2;->$scheduler:Lio/reactivex/Scheduler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lio/reactivex/Observable<",
            "+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    iget-object p1, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$refreshWhen$2;->$onRefresh:Lio/reactivex/Observable;

    const/4 v0, 0x1

    .line 177
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    const/4 v0, 0x0

    .line 178
    invoke-virtual {p1, v0}, Lio/reactivex/observables/ConnectableObservable;->autoConnect(I)Lio/reactivex/Observable;

    move-result-object p1

    .line 179
    iget-wide v0, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$refreshWhen$2;->$minWaitTime:J

    iget-object v2, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$refreshWhen$2;->$timeUnit:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$refreshWhen$2;->$scheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0, v1, v2, v3}, Lio/reactivex/Observable;->delaySubscription(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "onRefresh\n            .r\u2026ime, timeUnit, scheduler)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    iget-wide v0, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$refreshWhen$2;->$minWaitTime:J

    iget-object v2, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$refreshWhen$2;->$timeUnit:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$refreshWhen$2;->$scheduler:Lio/reactivex/Scheduler;

    invoke-static {p1, v0, v1, v2, v3}, Lcom/squareup/util/rx2/Rx2TransformersKt;->adaptiveSample(Lio/reactivex/Observable;JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/squareup/util/rx2/Rx2TransformersKt$refreshWhen$2;->apply(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
