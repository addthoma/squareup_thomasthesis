.class public final Lcom/squareup/util/rx2/Rx2Views;
.super Ljava/lang/Object;
.source "Rx2Views.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRx2Views.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Rx2Views.kt\ncom/squareup/util/rx2/Rx2Views\n*L\n1#1,245:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u001a\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003*\u00020\u00082\u0008\u0008\u0001\u0010\u000e\u001a\u00020\u000f\u001a\u0018\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003*\u00020\u00052\u0006\u0010\u0011\u001a\u00020\u000f\u001a\u0018\u0010\u0012\u001a\u00020\u0004*\u00020\u00082\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014\u001a\n\u0010\u0016\u001a\u00020\u0004*\u00020\u0008\u001a\u001c\u0010\u0017\u001a\u00020\u0004*\u0006\u0012\u0002\u0008\u00030\u00182\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0014\u001a\u000c\u0010\u001a\u001a\u00020\u001b*\u00020\u0008H\u0002\u001a\u0010\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0003*\u00020\u0008\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u001b\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003*\u00020\u00058G\u00a2\u0006\u0006\u001a\u0004\u0008\u0002\u0010\u0006\"\u001b\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003*\u00020\u00088G\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\t\"\u001b\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0003*\u00020\u00058G\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u0006\"\u001b\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0003*\u00020\u00058G\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\u0006\u00a8\u0006\u001d"
    }
    d2 = {
        "KEYBOARD_PERCENTAGE_CUTOFF",
        "",
        "debouncedOnChanged",
        "Lio/reactivex/Observable;",
        "",
        "Landroid/widget/TextView;",
        "(Landroid/widget/TextView;)Lio/reactivex/Observable;",
        "debouncedOnClicked",
        "Landroid/view/View;",
        "(Landroid/view/View;)Lio/reactivex/Observable;",
        "debouncedShortText",
        "",
        "isBlank",
        "",
        "id",
        "",
        "debouncedOnEditorAction",
        "imeMaskAction",
        "disposeOnDetach",
        "disposableFactory",
        "Lkotlin/Function0;",
        "Lio/reactivex/disposables/Disposable;",
        "disposeOnDetachNow",
        "disposeOnMainThread",
        "Lio/reactivex/ObservableEmitter;",
        "dispose",
        "ensureAttachedDisposable",
        "Lcom/squareup/util/rx2/AttachedDisposables;",
        "onKeyboardVisibilityChanged",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final KEYBOARD_PERCENTAGE_CUTOFF:F = 0.15f


# direct methods
.method public static final debouncedOnChanged(Landroid/widget/TextView;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            ")",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$debouncedOnChanged"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 236
    new-instance v0, Lcom/squareup/util/rx2/Rx2Views$debouncedOnChanged$1;

    invoke-direct {v0, p0}, Lcom/squareup/util/rx2/Rx2Views$debouncedOnChanged$1;-><init>(Landroid/widget/TextView;)V

    check-cast v0, Lio/reactivex/ObservableOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Observable;->create(Lio/reactivex/ObservableOnSubscribe;)Lio/reactivex/Observable;

    move-result-object p0

    const-string v0, "Observable.create { emit\u2026dListener(listener) }\n  }"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$debouncedOnClicked"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    new-instance v0, Lcom/squareup/util/rx2/Rx2Views$debouncedOnClicked$1;

    invoke-direct {v0, p0}, Lcom/squareup/util/rx2/Rx2Views$debouncedOnClicked$1;-><init>(Landroid/view/View;)V

    check-cast v0, Lio/reactivex/ObservableOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Observable;->create(Lio/reactivex/ObservableOnSubscribe;)Lio/reactivex/Observable;

    move-result-object p0

    const-string v0, "Observable.create { emit\u2026ClickListener(null) }\n  }"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final debouncedOnClicked(Landroid/view/View;I)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "I)",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$debouncedOnClicked"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static final debouncedOnEditorAction(Landroid/widget/TextView;I)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            "I)",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$debouncedOnEditorAction"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    new-instance v0, Lcom/squareup/util/rx2/Rx2Views$debouncedOnEditorAction$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/util/rx2/Rx2Views$debouncedOnEditorAction$1;-><init>(Landroid/widget/TextView;I)V

    check-cast v0, Lio/reactivex/ObservableOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Observable;->create(Lio/reactivex/ObservableOnSubscribe;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "Observable.create { emit\u2026ctionListener(null) }\n  }"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final debouncedShortText(Landroid/widget/TextView;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$debouncedShortText"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 211
    invoke-static {p0}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnChanged(Landroid/widget/TextView;)Lio/reactivex/Observable;

    move-result-object v0

    .line 212
    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    .line 213
    new-instance v1, Lcom/squareup/util/rx2/Rx2Views$debouncedShortText$1;

    invoke-direct {v1, p0}, Lcom/squareup/util/rx2/Rx2Views$debouncedShortText$1;-><init>(Landroid/widget/TextView;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    .line 214
    invoke-virtual {p0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p0

    const-string v0, "debouncedOnChanged\n     \u2026  .distinctUntilChanged()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lkotlin/jvm/functions/Function0<",
            "+",
            "Lio/reactivex/disposables/Disposable;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$disposeOnDetach"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "disposableFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-static {p0}, Lcom/squareup/util/rx2/Rx2Views;->ensureAttachedDisposable(Landroid/view/View;)Lcom/squareup/util/rx2/AttachedDisposables;

    move-result-object v0

    .line 43
    invoke-static {p0}, Lcom/squareup/util/Views;->isAttachedToWindowCompat(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 45
    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/disposables/Disposable;

    .line 46
    invoke-static {p0}, Lcom/squareup/util/Views;->isAttachedToWindowCompat(Landroid/view/View;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 48
    invoke-virtual {v0}, Lcom/squareup/util/rx2/AttachedDisposables;->getDisposable()Lio/reactivex/disposables/CompositeDisposable;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    goto :goto_0

    .line 52
    :cond_0
    invoke-interface {p1}, Lio/reactivex/disposables/Disposable;->dispose()V

    goto :goto_0

    .line 56
    :cond_1
    invoke-virtual {v0, p1}, Lcom/squareup/util/rx2/AttachedDisposables;->plusAssign(Lkotlin/jvm/functions/Function0;)V

    :goto_0
    return-void
.end method

.method public static final disposeOnDetachNow(Landroid/view/View;)V
    .locals 1

    const-string v0, "$this$disposeOnDetachNow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-static {p0}, Lcom/squareup/util/rx2/Rx2Views;->ensureAttachedDisposable(Landroid/view/View;)Lcom/squareup/util/rx2/AttachedDisposables;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/util/rx2/AttachedDisposables;->clearAllDisposables()V

    return-void
.end method

.method public static final disposeOnMainThread(Lio/reactivex/ObservableEmitter;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableEmitter<",
            "*>;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$disposeOnMainThread"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dispose"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    new-instance v0, Lcom/squareup/util/rx2/Rx2Views$disposeOnMainThread$1;

    invoke-direct {v0, p1}, Lcom/squareup/util/rx2/Rx2Views$disposeOnMainThread$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lio/reactivex/disposables/Disposable;

    invoke-interface {p0, v0}, Lio/reactivex/ObservableEmitter;->setDisposable(Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method private static final ensureAttachedDisposable(Landroid/view/View;)Lcom/squareup/util/rx2/AttachedDisposables;
    .locals 2

    .line 68
    sget v0, Lcom/squareup/rx2/utilities/android/R$id;->tag_attached_disposables:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/squareup/util/rx2/AttachedDisposables;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/squareup/util/rx2/AttachedDisposables;

    if-eqz v0, :cond_1

    goto :goto_0

    .line 69
    :cond_1
    new-instance v0, Lcom/squareup/util/rx2/AttachedDisposables;

    invoke-direct {v0}, Lcom/squareup/util/rx2/AttachedDisposables;-><init>()V

    .line 70
    sget v1, Lcom/squareup/rx2/utilities/android/R$id;->tag_attached_disposables:I

    invoke-virtual {p0, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 71
    move-object v1, v0

    check-cast v1, Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {p0, v1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    :goto_0
    return-object v0
.end method

.method public static final isBlank(Landroid/widget/TextView;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$isBlank"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 222
    invoke-static {p0}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnChanged(Landroid/widget/TextView;)Lio/reactivex/Observable;

    move-result-object v0

    .line 223
    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    .line 224
    new-instance v1, Lcom/squareup/util/rx2/Rx2Views$isBlank$1;

    invoke-direct {v1, p0}, Lcom/squareup/util/rx2/Rx2Views$isBlank$1;-><init>(Landroid/widget/TextView;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    .line 225
    invoke-virtual {p0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p0

    const-string v0, "debouncedOnChanged\n     \u2026  .distinctUntilChanged()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final onKeyboardVisibilityChanged(Landroid/view/View;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$onKeyboardVisibilityChanged"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    new-instance v0, Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1;

    invoke-direct {v0, p0}, Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1;-><init>(Landroid/view/View;)V

    check-cast v0, Lio/reactivex/ObservableOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Observable;->create(Lio/reactivex/ObservableOnSubscribe;)Lio/reactivex/Observable;

    move-result-object p0

    .line 116
    invoke-virtual {p0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p0

    const-string v0, "Observable\n      .create\u2026  .distinctUntilChanged()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
