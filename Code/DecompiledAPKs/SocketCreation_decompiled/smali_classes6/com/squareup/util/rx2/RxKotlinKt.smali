.class public final Lcom/squareup/util/rx2/RxKotlinKt;
.super Ljava/lang/Object;
.source "RxKotlin.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRxKotlin.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RxKotlin.kt\ncom/squareup/util/rx2/RxKotlinKt\n*L\n1#1,1655:1\n525#1,3:1656\n549#1,4:1659\n577#1,4:1663\n*E\n*S KotlinDebug\n*F\n+ 1 RxKotlin.kt\ncom/squareup/util/rx2/RxKotlinKt\n*L\n537#1,3:1656\n564#1,4:1659\n593#1,4:1663\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u001c\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\u0007\u001a\u001a\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00010\u0001*\u0008\u0012\u0004\u0012\u00020\u00010\u0006H\u0007\u001a\u001a\u0010\u0004\u001a\n \u0005*\u0004\u0018\u00010\u00010\u0001*\u0008\u0012\u0004\u0012\u00020\u00010\u0007H\u0007\u001ad\u0010\u0008\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u000b\u0012\u0004\u0012\u0002H\u000c0\t0\u0006\"\u0008\u0008\u0000\u0010\n*\u00020\r\"\u0008\u0008\u0001\u0010\u000b*\u00020\r\"\u0008\u0008\u0002\u0010\u000c*\u00020\r*\u0008\u0012\u0004\u0012\u0002H\n0\u00062\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u0002H\u000b0\u000f2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u0002H\u000c0\u000fH\u0007\u001a\u007f\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00110\u0006\"\u0008\u0008\u0000\u0010\n*\u00020\r\"\u0008\u0008\u0001\u0010\u000b*\u00020\r\"\u0008\u0008\u0002\u0010\u000c*\u00020\r\"\u0008\u0008\u0003\u0010\u0011*\u00020\r*\u0008\u0012\u0004\u0012\u0002H\n0\u00062\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u0002H\u000b0\u000f2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u0002H\u000c0\u000f2 \u0008\u0004\u0010\u0012\u001a\u001a\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u000b\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\u00110\u0013H\u0087\u0008\u001a\u009d\u0001\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00110\u0006\"\u0008\u0008\u0000\u0010\n*\u00020\r\"\u0008\u0008\u0001\u0010\u000b*\u00020\r\"\u0008\u0008\u0002\u0010\u000c*\u00020\r\"\u0008\u0008\u0003\u0010\u0014*\u00020\r\"\u0008\u0008\u0004\u0010\u0011*\u00020\r*\u0008\u0012\u0004\u0012\u0002H\n0\u00062\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u0002H\u000b0\u000f2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u0002H\u000c0\u000f2\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u0002H\u00140\u000f2&\u0008\u0004\u0010\u0012\u001a \u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u000b\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\u0014\u0012\u0004\u0012\u0002H\u00110\u0016H\u0087\u0008\u001a\u00bb\u0001\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00110\u0006\"\u0008\u0008\u0000\u0010\n*\u00020\r\"\u0008\u0008\u0001\u0010\u000b*\u00020\r\"\u0008\u0008\u0002\u0010\u000c*\u00020\r\"\u0008\u0008\u0003\u0010\u0014*\u00020\r\"\u0008\u0008\u0004\u0010\u0017*\u00020\r\"\u0008\u0008\u0005\u0010\u0011*\u00020\r*\u0008\u0012\u0004\u0012\u0002H\n0\u00062\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u0002H\u000b0\u000f2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u0002H\u000c0\u000f2\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u0002H\u00140\u000f2\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u0002H\u00170\u000f2,\u0008\u0004\u0010\u0012\u001a&\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u000b\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\u0014\u0012\u0004\u0012\u0002H\u0017\u0012\u0004\u0012\u0002H\u00110\u0019H\u0087\u0008\u001aF\u0010\u0008\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u001b0\u001a0\u0006\"\u0008\u0008\u0000\u0010\n*\u00020\r\"\u0008\u0008\u0001\u0010\u001b*\u00020\r*\u0008\u0012\u0004\u0012\u0002H\n0\u00062\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u0002H\u001b0\u000fH\u0007\u001aa\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00110\u0006\"\u0008\u0008\u0000\u0010\n*\u00020\r\"\u0008\u0008\u0001\u0010\u001b*\u00020\r\"\u0008\u0008\u0002\u0010\u0011*\u00020\r*\u0008\u0012\u0004\u0012\u0002H\n0\u00062\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u0002H\u001b0\u000f2\u001a\u0008\u0004\u0010\u0012\u001a\u0014\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u001b\u0012\u0004\u0012\u0002H\u00110\u001dH\u0087\u0008\u001ad\u0010\u0008\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u000b\u0012\u0004\u0012\u0002H\u000c0\t0\u0007\"\u0008\u0008\u0000\u0010\n*\u00020\r\"\u0008\u0008\u0001\u0010\u000b*\u00020\r\"\u0008\u0008\u0002\u0010\u000c*\u00020\r*\u0008\u0012\u0004\u0012\u0002H\n0\u00072\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u0002H\u000b0\u001e2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u0002H\u000c0\u001eH\u0007\u001a\u007f\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00110\u0007\"\u0008\u0008\u0000\u0010\n*\u00020\r\"\u0008\u0008\u0001\u0010\u000b*\u00020\r\"\u0008\u0008\u0002\u0010\u000c*\u00020\r\"\u0008\u0008\u0003\u0010\u0011*\u00020\r*\u0008\u0012\u0004\u0012\u0002H\n0\u00072\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u0002H\u000b0\u001e2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u0002H\u000c0\u001e2 \u0008\u0004\u0010\u0012\u001a\u001a\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u000b\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\u00110\u0013H\u0087\u0008\u001a\u0082\u0001\u0010\u0008\u001a \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u000b\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\u00140\u001f0\u0007\"\u0008\u0008\u0000\u0010\n*\u00020\r\"\u0008\u0008\u0001\u0010\u000b*\u00020\r\"\u0008\u0008\u0002\u0010\u000c*\u00020\r\"\u0008\u0008\u0003\u0010\u0014*\u00020\r*\u0008\u0012\u0004\u0012\u0002H\n0\u00072\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u0002H\u000b0\u001e2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u0002H\u000c0\u001e2\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u0002H\u00140\u001eH\u0007\u001a\u009d\u0001\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00110\u0007\"\u0008\u0008\u0000\u0010\n*\u00020\r\"\u0008\u0008\u0001\u0010\u000b*\u00020\r\"\u0008\u0008\u0002\u0010\u000c*\u00020\r\"\u0008\u0008\u0003\u0010\u0014*\u00020\r\"\u0008\u0008\u0004\u0010\u0011*\u00020\r*\u0008\u0012\u0004\u0012\u0002H\n0\u00072\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u0002H\u000b0\u001e2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u0002H\u000c0\u001e2\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u0002H\u00140\u001e2&\u0008\u0004\u0010\u0012\u001a \u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u000b\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\u0014\u0012\u0004\u0012\u0002H\u00110\u0016H\u0087\u0008\u001a\u00a0\u0001\u0010\u0008\u001a&\u0012\"\u0012 \u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u000b\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\u0014\u0012\u0004\u0012\u0002H\u00170 0\u0007\"\u0008\u0008\u0000\u0010\n*\u00020\r\"\u0008\u0008\u0001\u0010\u000b*\u00020\r\"\u0008\u0008\u0002\u0010\u000c*\u00020\r\"\u0008\u0008\u0003\u0010\u0014*\u00020\r\"\u0008\u0008\u0004\u0010\u0017*\u00020\r*\u0008\u0012\u0004\u0012\u0002H\n0\u00072\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u0002H\u000b0\u001e2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u0002H\u000c0\u001e2\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u0002H\u00140\u001e2\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u0002H\u00170\u001eH\u0007\u001a\u00bb\u0001\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00110\u0007\"\u0008\u0008\u0000\u0010\n*\u00020\r\"\u0008\u0008\u0001\u0010\u000b*\u00020\r\"\u0008\u0008\u0002\u0010\u000c*\u00020\r\"\u0008\u0008\u0003\u0010\u0014*\u00020\r\"\u0008\u0008\u0004\u0010\u0017*\u00020\r\"\u0008\u0008\u0005\u0010\u0011*\u00020\r*\u0008\u0012\u0004\u0012\u0002H\n0\u00072\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u0002H\u000b0\u001e2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u0002H\u000c0\u001e2\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u0002H\u00140\u001e2\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u0002H\u00170\u001e2,\u0008\u0004\u0010\u0012\u001a&\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u000b\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\u0014\u0012\u0004\u0012\u0002H\u0017\u0012\u0004\u0012\u0002H\u00110\u0019H\u0087\u0008\u001aF\u0010\u0008\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u001b0\u001a0\u0007\"\u0008\u0008\u0000\u0010\n*\u00020\r\"\u0008\u0008\u0001\u0010\u001b*\u00020\r*\u0008\u0012\u0004\u0012\u0002H\n0\u00072\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u0002H\u001b0\u001eH\u0007\u001aa\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00110\u0007\"\u0008\u0008\u0000\u0010\n*\u00020\r\"\u0008\u0008\u0001\u0010\u001b*\u00020\r\"\u0008\u0008\u0002\u0010\u0011*\u00020\r*\u0008\u0012\u0004\u0012\u0002H\n0\u00072\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u0002H\u001b0\u001e2\u001a\u0008\u0004\u0010\u0012\u001a\u0014\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u001b\u0012\u0004\u0012\u0002H\u00110\u001dH\u0087\u0008\u001aF\u0010!\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u001b0\u001a0\u0006\"\u0008\u0008\u0000\u0010\n*\u00020\r\"\u0008\u0008\u0001\u0010\u001b*\u00020\r*\u0008\u0012\u0004\u0012\u0002H\n0\u00062\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u0002H\u001b0\u000fH\u0007\u001aa\u0010!\u001a\u0008\u0012\u0004\u0012\u0002H\u00110\u0006\"\u0008\u0008\u0000\u0010\n*\u00020\r\"\u0008\u0008\u0001\u0010\u001b*\u00020\r\"\u0008\u0008\u0002\u0010\u0011*\u00020\r*\u0008\u0012\u0004\u0012\u0002H\n0\u00062\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u0002H\u001b0\u000f2\u001a\u0008\u0004\u0010\"\u001a\u0014\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u001b\u0012\u0004\u0012\u0002H\u00110\u001dH\u0087\u0008\u001aF\u0010!\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u001b0\u001a0#\"\u0008\u0008\u0000\u0010\n*\u00020\r\"\u0008\u0008\u0001\u0010\u001b*\u00020\r*\u0008\u0012\u0004\u0012\u0002H\n0#2\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u0002H\u001b0$H\u0007\u001aa\u0010!\u001a\u0008\u0012\u0004\u0012\u0002H\u00110#\"\u0008\u0008\u0000\u0010\n*\u00020\r\"\u0008\u0008\u0001\u0010\u001b*\u00020\r\"\u0008\u0008\u0002\u0010\u0011*\u00020\r*\u0008\u0012\u0004\u0012\u0002H\n0#2\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u0002H\u001b0$2\u001a\u0008\u0004\u0010\"\u001a\u0014\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u001b\u0012\u0004\u0012\u0002H\u00110\u001dH\u0087\u0008\u001aF\u0010!\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u001b0\u001a0\u0007\"\u0008\u0008\u0000\u0010\n*\u00020\r\"\u0008\u0008\u0001\u0010\u001b*\u00020\r*\u0008\u0012\u0004\u0012\u0002H\n0\u00072\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u0002H\u001b0\u001eH\u0007\u001aa\u0010!\u001a\u0008\u0012\u0004\u0012\u0002H\u00110\u0007\"\u0008\u0008\u0000\u0010\n*\u00020\r\"\u0008\u0008\u0001\u0010\u001b*\u00020\r\"\u0008\u0008\u0002\u0010\u0011*\u00020\r*\u0008\u0012\u0004\u0012\u0002H\n0\u00072\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u0002H\u001b0\u001e2\u001a\u0008\u0004\u0010\"\u001a\u0014\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u001b\u0012\u0004\u0012\u0002H\u00110\u001dH\u0087\u0008\u001aF\u0010!\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u001b0\u001a0%\"\u0008\u0008\u0000\u0010\n*\u00020\r\"\u0008\u0008\u0001\u0010\u001b*\u00020\r*\u0008\u0012\u0004\u0012\u0002H\n0%2\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u0002H\u001b0&H\u0007\u001aa\u0010!\u001a\u0008\u0012\u0004\u0012\u0002H\u00110%\"\u0008\u0008\u0000\u0010\n*\u00020\r\"\u0008\u0008\u0001\u0010\u001b*\u00020\r\"\u0008\u0008\u0002\u0010\u0011*\u00020\r*\u0008\u0012\u0004\u0012\u0002H\n0%2\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u0002H\u001b0&2\u001a\u0008\u0004\u0010\"\u001a\u0014\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u001b\u0012\u0004\u0012\u0002H\u00110\u001dH\u0087\u0008\u00a8\u0006\'"
    }
    d2 = {
        "concatAll",
        "Lio/reactivex/Completable;",
        "",
        "Lio/reactivex/CompletableSource;",
        "mergeAllCompletables",
        "kotlin.jvm.PlatformType",
        "Lio/reactivex/Flowable;",
        "Lio/reactivex/Observable;",
        "withLatestFrom",
        "Lkotlin/Triple;",
        "T",
        "T1",
        "T2",
        "",
        "o1",
        "Lorg/reactivestreams/Publisher;",
        "o2",
        "R",
        "combiner",
        "Lkotlin/Function3;",
        "T3",
        "o3",
        "Lkotlin/Function4;",
        "T4",
        "o4",
        "Lkotlin/Function5;",
        "Lkotlin/Pair;",
        "U",
        "other",
        "Lkotlin/Function2;",
        "Lio/reactivex/ObservableSource;",
        "Lcom/squareup/util/tuple/Quartet;",
        "Lcom/squareup/util/tuple/Quintuple;",
        "zipWith",
        "zipper",
        "Lio/reactivex/Maybe;",
        "Lio/reactivex/MaybeSource;",
        "Lio/reactivex/Single;",
        "Lio/reactivex/SingleSource;",
        "public"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final concatAll(Ljava/lang/Iterable;)Lio/reactivex/Completable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "+",
            "Lio/reactivex/CompletableSource;",
            ">;)",
            "Lio/reactivex/Completable;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$concatAll"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1654
    invoke-static {p0}, Lio/reactivex/Completable;->concat(Ljava/lang/Iterable;)Lio/reactivex/Completable;

    move-result-object p0

    const-string v0, "Completable.concat(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final mergeAllCompletables(Lio/reactivex/Flowable;)Lio/reactivex/Completable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Flowable<",
            "Lio/reactivex/Completable;",
            ">;)",
            "Lio/reactivex/Completable;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/BackpressureSupport;
        value = .enum Lio/reactivex/annotations/BackpressureKind;->UNBOUNDED_IN:Lio/reactivex/annotations/BackpressureKind;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$mergeAllCompletables"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1647
    sget-object v0, Lcom/squareup/util/rx2/RxKotlinKt$mergeAllCompletables$2;->INSTANCE:Lcom/squareup/util/rx2/RxKotlinKt$mergeAllCompletables$2;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p0, v0}, Lio/reactivex/Flowable;->flatMapCompletable(Lio/reactivex/functions/Function;)Lio/reactivex/Completable;

    move-result-object p0

    return-object p0
.end method

.method public static final mergeAllCompletables(Lio/reactivex/Observable;)Lio/reactivex/Completable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lio/reactivex/Completable;",
            ">;)",
            "Lio/reactivex/Completable;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$mergeAllCompletables"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1639
    sget-object v0, Lcom/squareup/util/rx2/RxKotlinKt$mergeAllCompletables$1;->INSTANCE:Lcom/squareup/util/rx2/RxKotlinKt$mergeAllCompletables$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->flatMapCompletable(Lio/reactivex/functions/Function;)Lio/reactivex/Completable;

    move-result-object p0

    return-object p0
.end method

.method public static final withLatestFrom(Lio/reactivex/Flowable;Lorg/reactivestreams/Publisher;)Lio/reactivex/Flowable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Flowable<",
            "TT;>;",
            "Lorg/reactivestreams/Publisher<",
            "TU;>;)",
            "Lio/reactivex/Flowable<",
            "Lkotlin/Pair<",
            "TT;TU;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/BackpressureSupport;
        value = .enum Lio/reactivex/annotations/BackpressureKind;->PASS_THROUGH:Lio/reactivex/annotations/BackpressureKind;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$withLatestFrom"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1171
    sget-object v0, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$10;->INSTANCE:Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$10;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_BiFunction$0;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_BiFunction$0;-><init>(Lkotlin/jvm/functions/Function2;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/functions/BiFunction;

    invoke-virtual {p0, p1, v0}, Lio/reactivex/Flowable;->withLatestFrom(Lorg/reactivestreams/Publisher;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Flowable;

    move-result-object p0

    const-string p1, "withLatestFrom(other, BiFunction(::Pair))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withLatestFrom(Lio/reactivex/Flowable;Lorg/reactivestreams/Publisher;Lkotlin/jvm/functions/Function2;)Lio/reactivex/Flowable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Flowable<",
            "TT;>;",
            "Lorg/reactivestreams/Publisher<",
            "TU;>;",
            "Lkotlin/jvm/functions/Function2<",
            "-TT;-TU;+TR;>;)",
            "Lio/reactivex/Flowable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/BackpressureSupport;
        value = .enum Lio/reactivex/annotations/BackpressureKind;->PASS_THROUGH:Lio/reactivex/annotations/BackpressureKind;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$withLatestFrom"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combiner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1164
    new-instance v0, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$9;

    invoke-direct {v0, p2}, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$9;-><init>(Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Lio/reactivex/functions/BiFunction;

    invoke-virtual {p0, p1, v0}, Lio/reactivex/Flowable;->withLatestFrom(Lorg/reactivestreams/Publisher;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Flowable;

    move-result-object p0

    const-string p1, "withLatestFrom(other, Bi\u2026 combiner.invoke(t, u) })"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withLatestFrom(Lio/reactivex/Flowable;Lorg/reactivestreams/Publisher;Lorg/reactivestreams/Publisher;)Lio/reactivex/Flowable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Flowable<",
            "TT;>;",
            "Lorg/reactivestreams/Publisher<",
            "TT1;>;",
            "Lorg/reactivestreams/Publisher<",
            "TT2;>;)",
            "Lio/reactivex/Flowable<",
            "Lkotlin/Triple<",
            "TT;TT1;TT2;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/BackpressureSupport;
        value = .enum Lio/reactivex/annotations/BackpressureKind;->PASS_THROUGH:Lio/reactivex/annotations/BackpressureKind;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$withLatestFrom"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1194
    sget-object v0, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$12;->INSTANCE:Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$12;

    check-cast v0, Lio/reactivex/functions/Function3;

    .line 1191
    invoke-virtual {p0, p1, p2, v0}, Lio/reactivex/Flowable;->withLatestFrom(Lorg/reactivestreams/Publisher;Lorg/reactivestreams/Publisher;Lio/reactivex/functions/Function3;)Lio/reactivex/Flowable;

    move-result-object p0

    const-string p1, "withLatestFrom(\n    o1,\n\u2026 -> Triple(t, t1, t2) }\n)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withLatestFrom(Lio/reactivex/Flowable;Lorg/reactivestreams/Publisher;Lorg/reactivestreams/Publisher;Lkotlin/jvm/functions/Function3;)Lio/reactivex/Flowable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Flowable<",
            "TT;>;",
            "Lorg/reactivestreams/Publisher<",
            "TT1;>;",
            "Lorg/reactivestreams/Publisher<",
            "TT2;>;",
            "Lkotlin/jvm/functions/Function3<",
            "-TT;-TT1;-TT2;+TR;>;)",
            "Lio/reactivex/Flowable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/BackpressureSupport;
        value = .enum Lio/reactivex/annotations/BackpressureKind;->PASS_THROUGH:Lio/reactivex/annotations/BackpressureKind;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$withLatestFrom"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combiner"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1183
    new-instance v0, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$11;

    invoke-direct {v0, p3}, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$11;-><init>(Lkotlin/jvm/functions/Function3;)V

    check-cast v0, Lio/reactivex/functions/Function3;

    invoke-virtual {p0, p1, p2, v0}, Lio/reactivex/Flowable;->withLatestFrom(Lorg/reactivestreams/Publisher;Lorg/reactivestreams/Publisher;Lio/reactivex/functions/Function3;)Lio/reactivex/Flowable;

    move-result-object p0

    const-string p1, "withLatestFrom(o1, o2, F\u2026iner.invoke(t, t1, t2) })"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withLatestFrom(Lio/reactivex/Flowable;Lorg/reactivestreams/Publisher;Lorg/reactivestreams/Publisher;Lorg/reactivestreams/Publisher;Lkotlin/jvm/functions/Function4;)Lio/reactivex/Flowable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Flowable<",
            "TT;>;",
            "Lorg/reactivestreams/Publisher<",
            "TT1;>;",
            "Lorg/reactivestreams/Publisher<",
            "TT2;>;",
            "Lorg/reactivestreams/Publisher<",
            "TT3;>;",
            "Lkotlin/jvm/functions/Function4<",
            "-TT;-TT1;-TT2;-TT3;+TR;>;)",
            "Lio/reactivex/Flowable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/BackpressureSupport;
        value = .enum Lio/reactivex/annotations/BackpressureKind;->PASS_THROUGH:Lio/reactivex/annotations/BackpressureKind;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$withLatestFrom"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combiner"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1212
    new-instance v0, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$13;

    invoke-direct {v0, p4}, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$13;-><init>(Lkotlin/jvm/functions/Function4;)V

    check-cast v0, Lio/reactivex/functions/Function4;

    .line 1208
    invoke-virtual {p0, p1, p2, p3, v0}, Lio/reactivex/Flowable;->withLatestFrom(Lorg/reactivestreams/Publisher;Lorg/reactivestreams/Publisher;Lorg/reactivestreams/Publisher;Lio/reactivex/functions/Function4;)Lio/reactivex/Flowable;

    move-result-object p0

    const-string p1, "withLatestFrom(\n    o1,\n\u2026invoke(t, t1, t2, t3) }\n)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withLatestFrom(Lio/reactivex/Flowable;Lorg/reactivestreams/Publisher;Lorg/reactivestreams/Publisher;Lorg/reactivestreams/Publisher;Lorg/reactivestreams/Publisher;Lkotlin/jvm/functions/Function5;)Lio/reactivex/Flowable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Flowable<",
            "TT;>;",
            "Lorg/reactivestreams/Publisher<",
            "TT1;>;",
            "Lorg/reactivestreams/Publisher<",
            "TT2;>;",
            "Lorg/reactivestreams/Publisher<",
            "TT3;>;",
            "Lorg/reactivestreams/Publisher<",
            "TT4;>;",
            "Lkotlin/jvm/functions/Function5<",
            "-TT;-TT1;-TT2;-TT3;-TT4;+TR;>;)",
            "Lio/reactivex/Flowable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/BackpressureSupport;
        value = .enum Lio/reactivex/annotations/BackpressureKind;->FULL:Lio/reactivex/annotations/BackpressureKind;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$withLatestFrom"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combiner"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1232
    new-instance v0, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$14;

    invoke-direct {v0, p5}, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$14;-><init>(Lkotlin/jvm/functions/Function5;)V

    move-object v6, v0

    check-cast v6, Lio/reactivex/functions/Function5;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 1227
    invoke-virtual/range {v1 .. v6}, Lio/reactivex/Flowable;->withLatestFrom(Lorg/reactivestreams/Publisher;Lorg/reactivestreams/Publisher;Lorg/reactivestreams/Publisher;Lorg/reactivestreams/Publisher;Lio/reactivex/functions/Function5;)Lio/reactivex/Flowable;

    move-result-object p0

    const-string p1, "withLatestFrom(\n    o1,\n\u2026ke(t, t1, t2, t3, t4) }\n)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT;>;",
            "Lio/reactivex/ObservableSource<",
            "TU;>;)",
            "Lio/reactivex/Observable<",
            "Lkotlin/Pair<",
            "TT;TU;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$withLatestFrom"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 514
    sget-object v0, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$2;->INSTANCE:Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$2;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_BiFunction$0;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_BiFunction$0;-><init>(Lkotlin/jvm/functions/Function2;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/functions/BiFunction;

    invoke-virtual {p0, p1, v0}, Lio/reactivex/Observable;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "withLatestFrom(other, BiFunction(::Pair))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT;>;",
            "Lio/reactivex/ObservableSource<",
            "TT1;>;",
            "Lio/reactivex/ObservableSource<",
            "TT2;>;)",
            "Lio/reactivex/Observable<",
            "Lkotlin/Triple<",
            "TT;TT1;TT2;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$withLatestFrom"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1657
    new-instance v0, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$$inlined$withLatestFrom$1;

    invoke-direct {v0}, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$$inlined$withLatestFrom$1;-><init>()V

    check-cast v0, Lio/reactivex/functions/Function3;

    .line 1656
    invoke-virtual {p0, p1, p2, v0}, Lio/reactivex/Observable;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function3;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "withLatestFrom(\n    o1, \u2026ner.invoke(t, t1, t2) }\n)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT;>;",
            "Lio/reactivex/ObservableSource<",
            "TT1;>;",
            "Lio/reactivex/ObservableSource<",
            "TT2;>;",
            "Lio/reactivex/ObservableSource<",
            "TT3;>;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/tuple/Quartet<",
            "TT;TT1;TT2;TT3;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$withLatestFrom"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1661
    new-instance v0, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$$inlined$withLatestFrom$2;

    invoke-direct {v0}, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$$inlined$withLatestFrom$2;-><init>()V

    check-cast v0, Lio/reactivex/functions/Function4;

    .line 1659
    invoke-virtual {p0, p1, p2, p3, v0}, Lio/reactivex/Observable;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function4;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "withLatestFrom(\n    o1, \u2026invoke(t, t1, t2, t3) }\n)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT;>;",
            "Lio/reactivex/ObservableSource<",
            "TT1;>;",
            "Lio/reactivex/ObservableSource<",
            "TT2;>;",
            "Lio/reactivex/ObservableSource<",
            "TT3;>;",
            "Lio/reactivex/ObservableSource<",
            "TT4;>;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/tuple/Quintuple<",
            "TT;TT1;TT2;TT3;TT4;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$withLatestFrom"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1665
    new-instance v0, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$$inlined$withLatestFrom$3;

    invoke-direct {v0}, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$$inlined$withLatestFrom$3;-><init>()V

    move-object v6, v0

    check-cast v6, Lio/reactivex/functions/Function5;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 1663
    invoke-virtual/range {v1 .. v6}, Lio/reactivex/Observable;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function5;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "withLatestFrom(\n    o1, \u2026ke(t, t1, t2, t3, t4) }\n)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lkotlin/jvm/functions/Function5;)Lio/reactivex/Observable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT;>;",
            "Lio/reactivex/ObservableSource<",
            "TT1;>;",
            "Lio/reactivex/ObservableSource<",
            "TT2;>;",
            "Lio/reactivex/ObservableSource<",
            "TT3;>;",
            "Lio/reactivex/ObservableSource<",
            "TT4;>;",
            "Lkotlin/jvm/functions/Function5<",
            "-TT;-TT1;-TT2;-TT3;-TT4;+TR;>;)",
            "Lio/reactivex/Observable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$withLatestFrom"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combiner"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 579
    new-instance v0, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$7;

    invoke-direct {v0, p5}, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$7;-><init>(Lkotlin/jvm/functions/Function5;)V

    move-object v6, v0

    check-cast v6, Lio/reactivex/functions/Function5;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 577
    invoke-virtual/range {v1 .. v6}, Lio/reactivex/Observable;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function5;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "withLatestFrom(\n    o1, \u2026ke(t, t1, t2, t3, t4) }\n)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lkotlin/jvm/functions/Function4;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT;>;",
            "Lio/reactivex/ObservableSource<",
            "TT1;>;",
            "Lio/reactivex/ObservableSource<",
            "TT2;>;",
            "Lio/reactivex/ObservableSource<",
            "TT3;>;",
            "Lkotlin/jvm/functions/Function4<",
            "-TT;-TT1;-TT2;-TT3;+TR;>;)",
            "Lio/reactivex/Observable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$withLatestFrom"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combiner"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 551
    new-instance v0, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$5;

    invoke-direct {v0, p4}, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$5;-><init>(Lkotlin/jvm/functions/Function4;)V

    check-cast v0, Lio/reactivex/functions/Function4;

    .line 549
    invoke-virtual {p0, p1, p2, p3, v0}, Lio/reactivex/Observable;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function4;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "withLatestFrom(\n    o1, \u2026invoke(t, t1, t2, t3) }\n)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lkotlin/jvm/functions/Function3;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT;>;",
            "Lio/reactivex/ObservableSource<",
            "TT1;>;",
            "Lio/reactivex/ObservableSource<",
            "TT2;>;",
            "Lkotlin/jvm/functions/Function3<",
            "-TT;-TT1;-TT2;+TR;>;)",
            "Lio/reactivex/Observable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$withLatestFrom"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "o2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combiner"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 526
    new-instance v0, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$3;

    invoke-direct {v0, p3}, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$3;-><init>(Lkotlin/jvm/functions/Function3;)V

    check-cast v0, Lio/reactivex/functions/Function3;

    .line 525
    invoke-virtual {p0, p1, p2, v0}, Lio/reactivex/Observable;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function3;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "withLatestFrom(\n    o1, \u2026ner.invoke(t, t1, t2) }\n)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;Lkotlin/jvm/functions/Function2;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT;>;",
            "Lio/reactivex/ObservableSource<",
            "TU;>;",
            "Lkotlin/jvm/functions/Function2<",
            "-TT;-TU;+TR;>;)",
            "Lio/reactivex/Observable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$withLatestFrom"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combiner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 505
    new-instance v0, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$1;

    invoke-direct {v0, p2}, Lcom/squareup/util/rx2/RxKotlinKt$withLatestFrom$1;-><init>(Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Lio/reactivex/functions/BiFunction;

    invoke-virtual {p0, p1, v0}, Lio/reactivex/Observable;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "withLatestFrom(other, Bi\u2026 combiner.invoke(t, u) })"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final zipWith(Lio/reactivex/Flowable;Lorg/reactivestreams/Publisher;)Lio/reactivex/Flowable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Flowable<",
            "TT;>;",
            "Lorg/reactivestreams/Publisher<",
            "TU;>;)",
            "Lio/reactivex/Flowable<",
            "Lkotlin/Pair<",
            "TT;TU;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/BackpressureSupport;
        value = .enum Lio/reactivex/annotations/BackpressureKind;->FULL:Lio/reactivex/annotations/BackpressureKind;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$zipWith"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1254
    sget-object v0, Lcom/squareup/util/rx2/RxKotlinKt$zipWith$4;->INSTANCE:Lcom/squareup/util/rx2/RxKotlinKt$zipWith$4;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_BiFunction$0;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_BiFunction$0;-><init>(Lkotlin/jvm/functions/Function2;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/functions/BiFunction;

    invoke-virtual {p0, p1, v0}, Lio/reactivex/Flowable;->zipWith(Lorg/reactivestreams/Publisher;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Flowable;

    move-result-object p0

    const-string p1, "zipWith(other, BiFunction(::Pair))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final zipWith(Lio/reactivex/Flowable;Lorg/reactivestreams/Publisher;Lkotlin/jvm/functions/Function2;)Lio/reactivex/Flowable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Flowable<",
            "TT;>;",
            "Lorg/reactivestreams/Publisher<",
            "TU;>;",
            "Lkotlin/jvm/functions/Function2<",
            "-TT;-TU;+TR;>;)",
            "Lio/reactivex/Flowable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/BackpressureSupport;
        value = .enum Lio/reactivex/annotations/BackpressureKind;->PASS_THROUGH:Lio/reactivex/annotations/BackpressureKind;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$zipWith"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "zipper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1244
    new-instance v0, Lcom/squareup/util/rx2/RxKotlinKt$zipWith$3;

    invoke-direct {v0, p2}, Lcom/squareup/util/rx2/RxKotlinKt$zipWith$3;-><init>(Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Lio/reactivex/functions/BiFunction;

    invoke-virtual {p0, p1, v0}, Lio/reactivex/Flowable;->zipWith(Lorg/reactivestreams/Publisher;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Flowable;

    move-result-object p0

    const-string p1, "zipWith(other, BiFunctio\u2026-> zipper.invoke(t, u) })"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final zipWith(Lio/reactivex/Maybe;Lio/reactivex/MaybeSource;)Lio/reactivex/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Maybe<",
            "TT;>;",
            "Lio/reactivex/MaybeSource<",
            "TU;>;)",
            "Lio/reactivex/Maybe<",
            "Lkotlin/Pair<",
            "TT;TU;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$zipWith"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1443
    sget-object v0, Lcom/squareup/util/rx2/RxKotlinKt$zipWith$6;->INSTANCE:Lcom/squareup/util/rx2/RxKotlinKt$zipWith$6;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_BiFunction$0;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_BiFunction$0;-><init>(Lkotlin/jvm/functions/Function2;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/functions/BiFunction;

    invoke-virtual {p0, p1, v0}, Lio/reactivex/Maybe;->zipWith(Lio/reactivex/MaybeSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Maybe;

    move-result-object p0

    const-string p1, "zipWith(other, BiFunction(::Pair))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final zipWith(Lio/reactivex/Maybe;Lio/reactivex/MaybeSource;Lkotlin/jvm/functions/Function2;)Lio/reactivex/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Maybe<",
            "TT;>;",
            "Lio/reactivex/MaybeSource<",
            "TU;>;",
            "Lkotlin/jvm/functions/Function2<",
            "-TT;-TU;+TR;>;)",
            "Lio/reactivex/Maybe<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$zipWith"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "zipper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1434
    new-instance v0, Lcom/squareup/util/rx2/RxKotlinKt$zipWith$5;

    invoke-direct {v0, p2}, Lcom/squareup/util/rx2/RxKotlinKt$zipWith$5;-><init>(Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Lio/reactivex/functions/BiFunction;

    invoke-virtual {p0, p1, v0}, Lio/reactivex/Maybe;->zipWith(Lio/reactivex/MaybeSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Maybe;

    move-result-object p0

    const-string p1, "zipWith(other, BiFunctio\u2026-> zipper.invoke(t, u) })"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final zipWith(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT;>;",
            "Lio/reactivex/ObservableSource<",
            "TU;>;)",
            "Lio/reactivex/Observable<",
            "Lkotlin/Pair<",
            "TT;TU;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$zipWith"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 612
    sget-object v0, Lcom/squareup/util/rx2/RxKotlinKt$zipWith$2;->INSTANCE:Lcom/squareup/util/rx2/RxKotlinKt$zipWith$2;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_BiFunction$0;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_BiFunction$0;-><init>(Lkotlin/jvm/functions/Function2;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/functions/BiFunction;

    invoke-virtual {p0, p1, v0}, Lio/reactivex/Observable;->zipWith(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "zipWith(other, BiFunction(::Pair))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final zipWith(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;Lkotlin/jvm/functions/Function2;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT;>;",
            "Lio/reactivex/ObservableSource<",
            "TU;>;",
            "Lkotlin/jvm/functions/Function2<",
            "-TT;-TU;+TR;>;)",
            "Lio/reactivex/Observable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$zipWith"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "zipper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 603
    new-instance v0, Lcom/squareup/util/rx2/RxKotlinKt$zipWith$1;

    invoke-direct {v0, p2}, Lcom/squareup/util/rx2/RxKotlinKt$zipWith$1;-><init>(Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Lio/reactivex/functions/BiFunction;

    invoke-virtual {p0, p1, v0}, Lio/reactivex/Observable;->zipWith(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "zipWith(other, BiFunctio\u2026-> zipper.invoke(t, u) })"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final zipWith(Lio/reactivex/Single;Lio/reactivex/SingleSource;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Single<",
            "TT;>;",
            "Lio/reactivex/SingleSource<",
            "TU;>;)",
            "Lio/reactivex/Single<",
            "Lkotlin/Pair<",
            "TT;TU;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$zipWith"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1632
    sget-object v0, Lcom/squareup/util/rx2/RxKotlinKt$zipWith$8;->INSTANCE:Lcom/squareup/util/rx2/RxKotlinKt$zipWith$8;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_BiFunction$0;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_BiFunction$0;-><init>(Lkotlin/jvm/functions/Function2;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/functions/BiFunction;

    invoke-virtual {p0, p1, v0}, Lio/reactivex/Single;->zipWith(Lio/reactivex/SingleSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Single;

    move-result-object p0

    const-string p1, "zipWith(other, BiFunction(::Pair))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final zipWith(Lio/reactivex/Single;Lio/reactivex/SingleSource;Lkotlin/jvm/functions/Function2;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Single<",
            "TT;>;",
            "Lio/reactivex/SingleSource<",
            "TU;>;",
            "Lkotlin/jvm/functions/Function2<",
            "-TT;-TU;+TR;>;)",
            "Lio/reactivex/Single<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "$this$zipWith"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "zipper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1626
    new-instance v0, Lcom/squareup/util/rx2/RxKotlinKt$zipWith$7;

    invoke-direct {v0, p2}, Lcom/squareup/util/rx2/RxKotlinKt$zipWith$7;-><init>(Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Lio/reactivex/functions/BiFunction;

    invoke-virtual {p0, p1, v0}, Lio/reactivex/Single;->zipWith(Lio/reactivex/SingleSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Single;

    move-result-object p0

    const-string p1, "zipWith(other, BiFunctio\u2026-> zipper.invoke(t, u) })"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
