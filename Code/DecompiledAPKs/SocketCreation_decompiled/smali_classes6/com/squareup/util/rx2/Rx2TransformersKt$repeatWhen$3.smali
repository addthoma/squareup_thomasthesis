.class final Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$3;
.super Ljava/lang/Object;
.source "Rx2Transformers.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx2/Rx2TransformersKt;->repeatWhen(Lio/reactivex/Completable;Lio/reactivex/Flowable;JJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Completable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "Lio/reactivex/Flowable<",
        "Ljava/lang/Object;",
        ">;",
        "Lorg/reactivestreams/Publisher<",
        "*>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0014\u0010\u0004\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00050\u00050\u0001H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Flowable;",
        "",
        "kotlin.jvm.PlatformType",
        "completed",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $maxIdleTime:J

.field final synthetic $minIdleTime:J

.field final synthetic $refreshRequested:Lio/reactivex/Flowable;

.field final synthetic $scheduler:Lio/reactivex/Scheduler;

.field final synthetic $timeUnit:Ljava/util/concurrent/TimeUnit;


# direct methods
.method constructor <init>(Lio/reactivex/Flowable;JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;J)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$3;->$refreshRequested:Lio/reactivex/Flowable;

    iput-wide p2, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$3;->$minIdleTime:J

    iput-object p4, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$3;->$timeUnit:Ljava/util/concurrent/TimeUnit;

    iput-object p5, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$3;->$scheduler:Lio/reactivex/Scheduler;

    iput-wide p6, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$3;->$maxIdleTime:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lio/reactivex/Flowable;)Lio/reactivex/Flowable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Flowable<",
            "Ljava/lang/Object;",
            ">;)",
            "Lio/reactivex/Flowable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "completed"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 278
    new-instance v0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$3$1;

    invoke-direct {v0, p0}, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$3$1;-><init>(Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$3;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Flowable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Flowable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lio/reactivex/Flowable;

    invoke-virtual {p0, p1}, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$3;->apply(Lio/reactivex/Flowable;)Lio/reactivex/Flowable;

    move-result-object p1

    return-object p1
.end method
