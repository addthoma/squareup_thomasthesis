.class final Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2;
.super Ljava/lang/Object;
.source "Rx2RetryStrategies.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx2/Rx2RetryStrategies;->customBackoffThenErrorFlowable(ILjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lio/reactivex/functions/Function;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "Lio/reactivex/Flowable<",
        "+",
        "Ljava/lang/Throwable;",
        ">;",
        "Lio/reactivex/Flowable<",
        "Lkotlin/Unit;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u000e\u0010\u0004\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00050\u0001H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Flowable;",
        "",
        "kotlin.jvm.PlatformType",
        "errors",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $delayFunction:Lkotlin/jvm/functions/Function1;

.field final synthetic $isRetryableError:Lkotlin/jvm/functions/Function1;

.field final synthetic $numberOfRetries:I

.field final synthetic $scheduler:Lio/reactivex/Scheduler;

.field final synthetic $unit:Ljava/util/concurrent/TimeUnit;


# direct methods
.method constructor <init>(ILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Ljava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)V
    .locals 0

    iput p1, p0, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2;->$numberOfRetries:I

    iput-object p2, p0, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2;->$isRetryableError:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2;->$delayFunction:Lkotlin/jvm/functions/Function1;

    iput-object p4, p0, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2;->$unit:Ljava/util/concurrent/TimeUnit;

    iput-object p5, p0, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2;->$scheduler:Lio/reactivex/Scheduler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lio/reactivex/Flowable;)Lio/reactivex/Flowable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Flowable<",
            "+",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lio/reactivex/Flowable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "errors"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    sget-object v0, Lcom/squareup/util/rx2/Rx2RetryStrategies;->INSTANCE:Lcom/squareup/util/rx2/Rx2RetryStrategies;

    .line 159
    iget v1, p0, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2;->$numberOfRetries:I

    iget-object v2, p0, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2;->$isRetryableError:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/util/rx2/Rx2RetryStrategies;->retryCountThenErrorFlowable(Lio/reactivex/Flowable;ILkotlin/jvm/functions/Function1;)Lio/reactivex/Flowable;

    move-result-object p1

    .line 160
    new-instance v0, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2$1;

    invoke-direct {v0, p0}, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2$1;-><init>(Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Flowable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Flowable;

    move-result-object p1

    .line 163
    sget-object v0, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2$2;->INSTANCE:Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2$2;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Flowable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Flowable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 11
    check-cast p1, Lio/reactivex/Flowable;

    invoke-virtual {p0, p1}, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2;->apply(Lio/reactivex/Flowable;)Lio/reactivex/Flowable;

    move-result-object p1

    return-object p1
.end method
