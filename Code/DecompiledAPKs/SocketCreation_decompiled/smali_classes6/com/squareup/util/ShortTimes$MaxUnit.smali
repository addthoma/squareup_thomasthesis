.class public final enum Lcom/squareup/util/ShortTimes$MaxUnit;
.super Ljava/lang/Enum;
.source "ShortTimes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/ShortTimes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MaxUnit"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/util/ShortTimes$MaxUnit;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/util/ShortTimes$MaxUnit;

.field public static final enum DAY:Lcom/squareup/util/ShortTimes$MaxUnit;

.field public static final enum HOUR:Lcom/squareup/util/ShortTimes$MaxUnit;

.field public static final enum MINUTE:Lcom/squareup/util/ShortTimes$MaxUnit;

.field public static final enum SECOND:Lcom/squareup/util/ShortTimes$MaxUnit;

.field public static final enum WEEK:Lcom/squareup/util/ShortTimes$MaxUnit;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 18
    new-instance v0, Lcom/squareup/util/ShortTimes$MaxUnit;

    const/4 v1, 0x0

    const-string v2, "SECOND"

    invoke-direct {v0, v2, v1}, Lcom/squareup/util/ShortTimes$MaxUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/util/ShortTimes$MaxUnit;->SECOND:Lcom/squareup/util/ShortTimes$MaxUnit;

    new-instance v0, Lcom/squareup/util/ShortTimes$MaxUnit;

    const/4 v2, 0x1

    const-string v3, "MINUTE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/util/ShortTimes$MaxUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/util/ShortTimes$MaxUnit;->MINUTE:Lcom/squareup/util/ShortTimes$MaxUnit;

    new-instance v0, Lcom/squareup/util/ShortTimes$MaxUnit;

    const/4 v3, 0x2

    const-string v4, "HOUR"

    invoke-direct {v0, v4, v3}, Lcom/squareup/util/ShortTimes$MaxUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/util/ShortTimes$MaxUnit;->HOUR:Lcom/squareup/util/ShortTimes$MaxUnit;

    new-instance v0, Lcom/squareup/util/ShortTimes$MaxUnit;

    const/4 v4, 0x3

    const-string v5, "DAY"

    invoke-direct {v0, v5, v4}, Lcom/squareup/util/ShortTimes$MaxUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/util/ShortTimes$MaxUnit;->DAY:Lcom/squareup/util/ShortTimes$MaxUnit;

    new-instance v0, Lcom/squareup/util/ShortTimes$MaxUnit;

    const/4 v5, 0x4

    const-string v6, "WEEK"

    invoke-direct {v0, v6, v5}, Lcom/squareup/util/ShortTimes$MaxUnit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/util/ShortTimes$MaxUnit;->WEEK:Lcom/squareup/util/ShortTimes$MaxUnit;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/util/ShortTimes$MaxUnit;

    sget-object v6, Lcom/squareup/util/ShortTimes$MaxUnit;->SECOND:Lcom/squareup/util/ShortTimes$MaxUnit;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/util/ShortTimes$MaxUnit;->MINUTE:Lcom/squareup/util/ShortTimes$MaxUnit;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/util/ShortTimes$MaxUnit;->HOUR:Lcom/squareup/util/ShortTimes$MaxUnit;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/util/ShortTimes$MaxUnit;->DAY:Lcom/squareup/util/ShortTimes$MaxUnit;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/util/ShortTimes$MaxUnit;->WEEK:Lcom/squareup/util/ShortTimes$MaxUnit;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/util/ShortTimes$MaxUnit;->$VALUES:[Lcom/squareup/util/ShortTimes$MaxUnit;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/util/ShortTimes$MaxUnit;
    .locals 1

    .line 18
    const-class v0, Lcom/squareup/util/ShortTimes$MaxUnit;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/util/ShortTimes$MaxUnit;

    return-object p0
.end method

.method public static values()[Lcom/squareup/util/ShortTimes$MaxUnit;
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/util/ShortTimes$MaxUnit;->$VALUES:[Lcom/squareup/util/ShortTimes$MaxUnit;

    invoke-virtual {v0}, [Lcom/squareup/util/ShortTimes$MaxUnit;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/util/ShortTimes$MaxUnit;

    return-object v0
.end method
