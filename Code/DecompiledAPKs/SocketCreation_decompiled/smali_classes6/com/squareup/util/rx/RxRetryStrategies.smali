.class public final Lcom/squareup/util/rx/RxRetryStrategies;
.super Ljava/lang/Object;
.source "RxRetryStrategies.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static customBackoffThenError(ILjava/util/concurrent/TimeUnit;Lrx/Scheduler;Lrx/functions/Func1;)Lrx/functions/Func1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/concurrent/TimeUnit;",
            "Lrx/Scheduler;",
            "Lrx/functions/Func1<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;)",
            "Lrx/functions/Func1<",
            "Lrx/Observable<",
            "+",
            "Ljava/lang/Throwable;",
            ">;",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    .line 55
    new-instance v0, Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$RPubeEN5IRUmzCj34gq9gr2587E;

    invoke-direct {v0, p0, p3, p1, p2}, Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$RPubeEN5IRUmzCj34gq9gr2587E;-><init>(ILrx/functions/Func1;Ljava/util/concurrent/TimeUnit;Lrx/Scheduler;)V

    return-object v0
.end method

.method public static exponentialBackoffThenError(IJLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/functions/Func1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "Ljava/util/concurrent/TimeUnit;",
            "Lrx/Scheduler;",
            ")",
            "Lrx/functions/Func1<",
            "Lrx/Observable<",
            "+",
            "Ljava/lang/Throwable;",
            ">;",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$3Qsw0HIGx9p_ylvSJf2QGb3njMQ;

    invoke-direct {v0, p1, p2}, Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$3Qsw0HIGx9p_ylvSJf2QGb3njMQ;-><init>(J)V

    invoke-static {p0, p3, p4, v0}, Lcom/squareup/util/rx/RxRetryStrategies;->customBackoffThenError(ILjava/util/concurrent/TimeUnit;Lrx/Scheduler;Lrx/functions/Func1;)Lrx/functions/Func1;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$customBackoffThenError$4(ILrx/functions/Func1;Ljava/util/concurrent/TimeUnit;Lrx/Scheduler;Lrx/Observable;)Lrx/Observable;
    .locals 0

    .line 56
    invoke-static {p0}, Lcom/squareup/util/rx/RxRetryStrategies;->retryCountThenError(I)Lrx/Observable$Transformer;

    move-result-object p0

    invoke-virtual {p4, p0}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p0

    new-instance p4, Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$_X47XWJ9PyCpwXNVVYXhcM4jLqk;

    invoke-direct {p4, p1, p2, p3}, Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$_X47XWJ9PyCpwXNVVYXhcM4jLqk;-><init>(Lrx/functions/Func1;Ljava/util/concurrent/TimeUnit;Lrx/Scheduler;)V

    .line 57
    invoke-virtual {p0, p4}, Lrx/Observable;->flatMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    sget-object p1, Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$2QjCDw93L9cvnNAgHBfmXwzvelw;->INSTANCE:Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$2QjCDw93L9cvnNAgHBfmXwzvelw;

    .line 58
    invoke-virtual {p0, p1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$exponentialBackoffThenError$0(JLjava/lang/Integer;)Ljava/lang/Long;
    .locals 4

    .line 33
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    int-to-double v0, p2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-long v0, v0

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    mul-long p0, p0, v0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$noBackoffThenError$1(JLjava/lang/Integer;)Ljava/lang/Long;
    .locals 0

    .line 50
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$2(Lrx/functions/Func1;Ljava/util/concurrent/TimeUnit;Lrx/Scheduler;Ljava/lang/Integer;)Lrx/Observable;
    .locals 2

    .line 57
    invoke-interface {p0, p3}, Lrx/functions/Func1;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1, p1, p2}, Lrx/Observable;->timer(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$3(Ljava/lang/Long;)Lkotlin/Unit;
    .locals 0

    .line 58
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p0
.end method

.method static synthetic lambda$null$5(ILjava/lang/Throwable;Ljava/lang/Integer;)Lrx/Observable;
    .locals 1

    .line 65
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gt v0, p0, :cond_0

    .line 66
    invoke-static {p2}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p0

    goto :goto_0

    .line 67
    :cond_0
    invoke-static {p1}, Lrx/Observable;->error(Ljava/lang/Throwable;)Lrx/Observable;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method static synthetic lambda$null$6(Lrx/Observable;)Lrx/Observable;
    .locals 0

    return-object p0
.end method

.method static synthetic lambda$retryCountThenError$7(ILrx/Observable;)Lrx/Observable;
    .locals 2

    add-int/lit8 v0, p0, 0x1

    const/4 v1, 0x1

    .line 64
    invoke-static {v1, v0}, Lrx/Observable;->range(II)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$DYeLIHzCWyAlMh5mEFq2LSBvqQY;

    invoke-direct {v1, p0}, Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$DYeLIHzCWyAlMh5mEFq2LSBvqQY;-><init>(I)V

    invoke-virtual {p1, v0, v1}, Lrx/Observable;->zipWith(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p0

    sget-object p1, Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$pGEwCAmJS30ZKxzb7N9u_aN0w6s;->INSTANCE:Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$pGEwCAmJS30ZKxzb7N9u_aN0w6s;

    .line 68
    invoke-virtual {p0, p1}, Lrx/Observable;->flatMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static noBackoffThenError(IJLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/functions/Func1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "Ljava/util/concurrent/TimeUnit;",
            "Lrx/Scheduler;",
            ")",
            "Lrx/functions/Func1<",
            "Lrx/Observable<",
            "+",
            "Ljava/lang/Throwable;",
            ">;",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    .line 50
    new-instance v0, Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$rNc-JgHkqaZP8iS2Bb8JGiFgtZU;

    invoke-direct {v0, p1, p2}, Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$rNc-JgHkqaZP8iS2Bb8JGiFgtZU;-><init>(J)V

    invoke-static {p0, p3, p4, v0}, Lcom/squareup/util/rx/RxRetryStrategies;->customBackoffThenError(ILjava/util/concurrent/TimeUnit;Lrx/Scheduler;Lrx/functions/Func1;)Lrx/functions/Func1;

    move-result-object p0

    return-object p0
.end method

.method static retryCountThenError(I)Lrx/Observable$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lrx/Observable$Transformer<",
            "Ljava/lang/Throwable;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 63
    new-instance v0, Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$SLX03-2lnyHnUhEkp42LocxQ1QQ;

    invoke-direct {v0, p0}, Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$SLX03-2lnyHnUhEkp42LocxQ1QQ;-><init>(I)V

    return-object v0
.end method
