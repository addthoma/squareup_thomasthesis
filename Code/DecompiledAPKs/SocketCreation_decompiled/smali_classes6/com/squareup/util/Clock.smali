.class public interface abstract Lcom/squareup/util/Clock;
.super Ljava/lang/Object;
.source "Clock.java"


# virtual methods
.method public abstract getCurrentTimeMillis()J
.end method

.method public abstract getElapsedRealtime()J
.end method

.method public abstract getElapsedRealtimeNanos()J
.end method

.method public abstract getGregorianCalenderInstance()Ljava/util/Calendar;
.end method

.method public abstract getTimeZone()Ljava/util/TimeZone;
.end method

.method public abstract getUptimeMillis()J
.end method

.method public abstract withinPast(Landroid/location/Location;J)Z
.end method
