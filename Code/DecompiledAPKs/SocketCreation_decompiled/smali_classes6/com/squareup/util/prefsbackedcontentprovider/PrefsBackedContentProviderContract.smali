.class public interface abstract Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;
.super Ljava/lang/Object;
.source "PrefsBackedContentProvider.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u001c\u0010\u0006\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00080\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;",
        "",
        "authority",
        "",
        "getAuthority",
        "()Ljava/lang/String;",
        "items",
        "",
        "Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;",
        "getItems",
        "()Ljava/util/List;",
        "android-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getAuthority()Ljava/lang/String;
.end method

.method public abstract getItems()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/util/prefsbackedcontentprovider/ContractItem<",
            "*>;>;"
        }
    .end annotation
.end method
