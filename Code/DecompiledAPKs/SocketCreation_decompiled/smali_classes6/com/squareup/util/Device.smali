.class public interface abstract Lcom/squareup/util/Device;
.super Ljava/lang/Object;
.source "Device.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/util/Device$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0018\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u001a\u0010\u0006\u001a\u00020\u00078VX\u0097\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u0008\u0010\t\u001a\u0004\u0008\u0006\u0010\nR\u001a\u0010\u000b\u001a\u00020\u00078VX\u0097\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u000c\u0010\t\u001a\u0004\u0008\u000b\u0010\nR\u001a\u0010\r\u001a\u00020\u00078VX\u0097\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u000e\u0010\t\u001a\u0004\u0008\r\u0010\nR\u001a\u0010\u000f\u001a\u00020\u00078VX\u0097\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u0010\u0010\t\u001a\u0004\u0008\u000f\u0010\nR\u001a\u0010\u0011\u001a\u00020\u00078VX\u0097\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u0012\u0010\t\u001a\u0004\u0008\u0011\u0010\nR\u001a\u0010\u0013\u001a\u00020\u00078VX\u0097\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u0014\u0010\t\u001a\u0004\u0008\u0013\u0010\nR\u001a\u0010\u0015\u001a\u00020\u00078VX\u0097\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u0016\u0010\t\u001a\u0004\u0008\u0015\u0010\nR\u001a\u0010\u0017\u001a\u00020\u00078VX\u0097\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u0018\u0010\t\u001a\u0004\u0008\u0017\u0010\nR\u001a\u0010\u0019\u001a\u00020\u00078VX\u0097\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u001a\u0010\t\u001a\u0004\u0008\u0019\u0010\nR\u001a\u0010\u001b\u001a\u00020\u00078VX\u0097\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u001c\u0010\t\u001a\u0004\u0008\u001b\u0010\nR\u001a\u0010\u001d\u001a\u00020\u00078VX\u0097\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u001e\u0010\t\u001a\u0004\u0008\u001d\u0010\nR\u0018\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00030 X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008!\u0010\"\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/util/Device;",
        "",
        "currentScreenSize",
        "Lcom/squareup/util/DeviceScreenSizeInfo;",
        "getCurrentScreenSize",
        "()Lcom/squareup/util/DeviceScreenSizeInfo;",
        "isAndroidOWithBadAspectRatio",
        "",
        "isAndroidOWithBadAspectRatio$annotations",
        "()V",
        "()Z",
        "isLandscape",
        "isLandscape$annotations",
        "isLandscapeLongTablet",
        "isLandscapeLongTablet$annotations",
        "isLongTablet",
        "isLongTablet$annotations",
        "isMasterDetail",
        "isMasterDetail$annotations",
        "isPhone",
        "isPhone$annotations",
        "isPhoneOrPortraitLessThan10Inches",
        "isPhoneOrPortraitLessThan10Inches$annotations",
        "isPortrait",
        "isPortrait$annotations",
        "isTablet",
        "isTablet$annotations",
        "isTabletAtLeast10Inches",
        "isTabletAtLeast10Inches$annotations",
        "isTabletLessThan10Inches",
        "isTabletLessThan10Inches$annotations",
        "screenSize",
        "Lio/reactivex/Observable;",
        "getScreenSize",
        "()Lio/reactivex/Observable;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;
.end method

.method public abstract getScreenSize()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/DeviceScreenSizeInfo;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isAndroidOWithBadAspectRatio()Z
.end method

.method public abstract isLandscape()Z
.end method

.method public abstract isLandscapeLongTablet()Z
.end method

.method public abstract isLongTablet()Z
.end method

.method public abstract isMasterDetail()Z
.end method

.method public abstract isPhone()Z
.end method

.method public abstract isPhoneOrPortraitLessThan10Inches()Z
.end method

.method public abstract isPortrait()Z
.end method

.method public abstract isTablet()Z
.end method

.method public abstract isTabletAtLeast10Inches()Z
.end method

.method public abstract isTabletLessThan10Inches()Z
.end method
