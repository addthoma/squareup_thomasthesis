.class public abstract Lcom/squareup/util/AndroidModule;
.super Ljava/lang/Object;
.source "AndroidModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$provideMessageQueue$0(Landroid/os/MessageQueue$IdleHandler;)V
    .locals 1

    .line 30
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    return-void
.end method

.method static provideCacheDirectory(Landroid/app/Application;)Ljava/io/File;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 38
    invoke-virtual {p0}, Landroid/app/Application;->getCacheDir()Ljava/io/File;

    move-result-object p0

    return-object p0
.end method

.method static provideClipboard()Lcom/squareup/util/Clipboard;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 95
    sget-object v0, Lcom/squareup/util/Clipboard;->RealClipboard:Lcom/squareup/util/Clipboard$RealClipboard;

    return-object v0
.end method

.method static provideContentResolver(Landroid/app/Application;)Landroid/content/ContentResolver;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 46
    invoke-virtual {p0}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    return-object p0
.end method

.method static provideExecutorService()Ljava/util/concurrent/ExecutorService;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "BackgroundExecutor"

    .line 42
    invoke-static {v0}, Lcom/squareup/thread/Threads;->backgroundThreadFactory(Ljava/lang/String;)Ljava/util/concurrent/ThreadFactory;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/thread/executor/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method

.method static provideFilesDirectory(Landroid/app/Application;)Ljava/io/File;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 34
    invoke-virtual {p0}, Landroid/app/Application;->getFilesDir()Ljava/io/File;

    move-result-object p0

    return-object p0
.end method

.method static provideMessageQueue()Lcom/squareup/util/SquareMessageQueue;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 30
    sget-object v0, Lcom/squareup/util/-$$Lambda$AndroidModule$NLQvBmbhj9VBgm5MuZmJdSWm4Ts;->INSTANCE:Lcom/squareup/util/-$$Lambda$AndroidModule$NLQvBmbhj9VBgm5MuZmJdSWm4Ts;

    return-object v0
.end method

.method static providePackageInfo(Landroid/app/Application;)Landroid/content/pm/PackageInfo;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 63
    :try_start_0
    invoke-virtual {p0}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object p0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object p0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 65
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, p0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method static providePackageManager(Landroid/app/Application;)Landroid/content/pm/PackageManager;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 50
    invoke-virtual {p0}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    return-object p0
.end method

.method static provideProcessUserHandle()Landroid/os/UserHandle;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 58
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v0

    return-object v0
.end method

.method static provideServiceBinder(Landroid/app/Application;)Lcom/squareup/util/ServiceBinder;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 91
    new-instance v0, Lcom/squareup/util/AndroidServiceBinder;

    invoke-direct {v0, p0}, Lcom/squareup/util/AndroidServiceBinder;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method static provideStorageManager(Landroid/app/Application;)Landroid/os/storage/StorageManager;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "storage"

    .line 54
    invoke-virtual {p0, v0}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/os/storage/StorageManager;

    return-object p0
.end method

.method static provideUsbManager(Landroid/app/Application;)Lcom/squareup/hardware/usb/UsbManager;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "usb"

    .line 71
    invoke-virtual {p0, v0}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/hardware/usb/UsbManager;

    .line 73
    :try_start_0
    invoke-virtual {p0}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    new-instance v0, Lcom/squareup/hardware/usb/UsbManager$RealUsbManager;

    invoke-direct {v0, p0}, Lcom/squareup/hardware/usb/UsbManager$RealUsbManager;-><init>(Landroid/hardware/usb/UsbManager;)V

    return-object v0

    .line 82
    :catch_0
    sget-object p0, Lcom/squareup/hardware/usb/UsbManager;->FAKE:Lcom/squareup/hardware/usb/UsbManager;

    return-object p0
.end method


# virtual methods
.method abstract provideExecutor(Ljava/util/concurrent/ExecutorService;)Ljava/util/concurrent/Executor;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
