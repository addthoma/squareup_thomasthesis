.class public final enum Lcom/squareup/util/DisplayRotator$NaturalOrientation;
.super Ljava/lang/Enum;
.source "DisplayRotator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/DisplayRotator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NaturalOrientation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/util/DisplayRotator$NaturalOrientation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/util/DisplayRotator$NaturalOrientation;

.field public static final enum LANDSCAPE:Lcom/squareup/util/DisplayRotator$NaturalOrientation;

.field public static final enum PORTRAIT:Lcom/squareup/util/DisplayRotator$NaturalOrientation;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 80
    new-instance v0, Lcom/squareup/util/DisplayRotator$NaturalOrientation;

    const/4 v1, 0x0

    const-string v2, "PORTRAIT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/util/DisplayRotator$NaturalOrientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/util/DisplayRotator$NaturalOrientation;->PORTRAIT:Lcom/squareup/util/DisplayRotator$NaturalOrientation;

    new-instance v0, Lcom/squareup/util/DisplayRotator$NaturalOrientation;

    const/4 v2, 0x1

    const-string v3, "LANDSCAPE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/util/DisplayRotator$NaturalOrientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/util/DisplayRotator$NaturalOrientation;->LANDSCAPE:Lcom/squareup/util/DisplayRotator$NaturalOrientation;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/util/DisplayRotator$NaturalOrientation;

    .line 79
    sget-object v3, Lcom/squareup/util/DisplayRotator$NaturalOrientation;->PORTRAIT:Lcom/squareup/util/DisplayRotator$NaturalOrientation;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/util/DisplayRotator$NaturalOrientation;->LANDSCAPE:Lcom/squareup/util/DisplayRotator$NaturalOrientation;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/util/DisplayRotator$NaturalOrientation;->$VALUES:[Lcom/squareup/util/DisplayRotator$NaturalOrientation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 79
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/util/DisplayRotator$NaturalOrientation;Lcom/squareup/util/DisplayRotator$Orientation;Lcom/squareup/util/DisplayRotator$Orientation;)I
    .locals 0

    .line 79
    invoke-direct {p0, p1, p2}, Lcom/squareup/util/DisplayRotator$NaturalOrientation;->get90DegreeRotationsBetween(Lcom/squareup/util/DisplayRotator$Orientation;Lcom/squareup/util/DisplayRotator$Orientation;)I

    move-result p0

    return p0
.end method

.method private get90DegreeRotationsBetween(Lcom/squareup/util/DisplayRotator$Orientation;Lcom/squareup/util/DisplayRotator$Orientation;)I
    .locals 1

    .line 83
    sget-object v0, Lcom/squareup/util/DisplayRotator$NaturalOrientation;->LANDSCAPE:Lcom/squareup/util/DisplayRotator$NaturalOrientation;

    if-ne p0, v0, :cond_0

    .line 84
    invoke-static {p2}, Lcom/squareup/util/DisplayRotator$Orientation;->access$000(Lcom/squareup/util/DisplayRotator$Orientation;)I

    move-result p2

    invoke-static {p1}, Lcom/squareup/util/DisplayRotator$Orientation;->access$000(Lcom/squareup/util/DisplayRotator$Orientation;)I

    move-result p1

    sub-int/2addr p2, p1

    add-int/lit8 p2, p2, 0x4

    rem-int/lit8 p2, p2, 0x4

    goto :goto_0

    .line 85
    :cond_0
    invoke-static {p2}, Lcom/squareup/util/DisplayRotator$Orientation;->access$100(Lcom/squareup/util/DisplayRotator$Orientation;)I

    move-result p2

    invoke-static {p1}, Lcom/squareup/util/DisplayRotator$Orientation;->access$100(Lcom/squareup/util/DisplayRotator$Orientation;)I

    move-result p1

    sub-int/2addr p2, p1

    add-int/lit8 p2, p2, 0x4

    rem-int/lit8 p2, p2, 0x4

    :goto_0
    return p2
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/util/DisplayRotator$NaturalOrientation;
    .locals 1

    .line 79
    const-class v0, Lcom/squareup/util/DisplayRotator$NaturalOrientation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/util/DisplayRotator$NaturalOrientation;

    return-object p0
.end method

.method public static values()[Lcom/squareup/util/DisplayRotator$NaturalOrientation;
    .locals 1

    .line 79
    sget-object v0, Lcom/squareup/util/DisplayRotator$NaturalOrientation;->$VALUES:[Lcom/squareup/util/DisplayRotator$NaturalOrientation;

    invoke-virtual {v0}, [Lcom/squareup/util/DisplayRotator$NaturalOrientation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/util/DisplayRotator$NaturalOrientation;

    return-object v0
.end method
