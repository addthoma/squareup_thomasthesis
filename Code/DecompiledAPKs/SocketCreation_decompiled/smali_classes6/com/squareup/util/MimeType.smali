.class public final enum Lcom/squareup/util/MimeType;
.super Ljava/lang/Enum;
.source "MimeType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/util/MimeType;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/util/MimeType;

.field public static final enum GIF:Lcom/squareup/util/MimeType;

.field public static final enum JPEG:Lcom/squareup/util/MimeType;

.field public static final enum PNG:Lcom/squareup/util/MimeType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 12
    new-instance v0, Lcom/squareup/util/MimeType;

    const/4 v1, 0x0

    const-string v2, "GIF"

    invoke-direct {v0, v2, v1}, Lcom/squareup/util/MimeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/util/MimeType;->GIF:Lcom/squareup/util/MimeType;

    new-instance v0, Lcom/squareup/util/MimeType;

    const/4 v2, 0x1

    const-string v3, "PNG"

    invoke-direct {v0, v3, v2}, Lcom/squareup/util/MimeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/util/MimeType;->PNG:Lcom/squareup/util/MimeType;

    new-instance v0, Lcom/squareup/util/MimeType;

    const/4 v3, 0x2

    const-string v4, "JPEG"

    invoke-direct {v0, v4, v3}, Lcom/squareup/util/MimeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/util/MimeType;->JPEG:Lcom/squareup/util/MimeType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/util/MimeType;

    .line 10
    sget-object v4, Lcom/squareup/util/MimeType;->GIF:Lcom/squareup/util/MimeType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/util/MimeType;->PNG:Lcom/squareup/util/MimeType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/util/MimeType;->JPEG:Lcom/squareup/util/MimeType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/util/MimeType;->$VALUES:[Lcom/squareup/util/MimeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/util/MimeType;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/util/MimeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/util/MimeType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/util/MimeType;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/util/MimeType;->$VALUES:[Lcom/squareup/util/MimeType;

    invoke-virtual {v0}, [Lcom/squareup/util/MimeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/util/MimeType;

    return-object v0
.end method
