.class public Lcom/squareup/wavpool/swipe/AudioEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "AudioEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0018\u0008\u0016\u0018\u00002\u00020\u0001B\u0091\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\n\u0012\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\n\u0012\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\n\u0012\n\u0008\u0002\u0010\r\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u000e\u001a\u0004\u0018\u00010\n\u0012\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u00010\n\u0012\n\u0008\u0002\u0010\u0010\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0002\u0010\u0011R\u0015\u0010\u000c\u001a\u0004\u0018\u00010\n\u00a2\u0006\n\n\u0002\u0010\u0014\u001a\u0004\u0008\u0012\u0010\u0013R\u0015\u0010\u000b\u001a\u0004\u0018\u00010\n\u00a2\u0006\n\n\u0002\u0010\u0014\u001a\u0004\u0008\u0015\u0010\u0013R\u0015\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\n\n\u0002\u0010\u0014\u001a\u0004\u0008\u0016\u0010\u0013R\u0013\u0010\r\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0015\u0010\u0010\u001a\u0004\u0018\u00010\n\u00a2\u0006\n\n\u0002\u0010\u0014\u001a\u0004\u0008\u0019\u0010\u0013R\u0015\u0010\u000f\u001a\u0004\u0018\u00010\n\u00a2\u0006\n\n\u0002\u0010\u0014\u001a\u0004\u0008\u001a\u0010\u0013R\u0015\u0010\u000e\u001a\u0004\u0018\u00010\n\u00a2\u0006\n\n\u0002\u0010\u0014\u001a\u0004\u0008\u001b\u0010\u0013R\u0015\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\n\n\u0002\u0010\u001e\u001a\u0004\u0008\u001c\u0010\u001dR\u0015\u0010\u0008\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\n\n\u0002\u0010\u001e\u001a\u0004\u0008\u001f\u0010\u001dR\u0015\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\n\n\u0002\u0010\u001e\u001a\u0004\u0008 \u0010\u001dR\u0015\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\n\n\u0002\u0010\u001e\u001a\u0004\u0008!\u0010\u001d\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/wavpool/swipe/AudioEvent;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "eventValue",
        "",
        "systemStreamActualVolume",
        "",
        "systemStreamAttemptedVolume",
        "musicStreamActualVolume",
        "musicStreamAttemptedVolume",
        "audioTrackState",
        "",
        "audioTrackRetryCount",
        "audioTrackNumSamples",
        "errorMessage",
        "initialVolume",
        "initialUnusedStreamVolume",
        "initialRingerType",
        "(Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V",
        "getAudioTrackNumSamples",
        "()Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "getAudioTrackRetryCount",
        "getAudioTrackState",
        "getErrorMessage",
        "()Ljava/lang/String;",
        "getInitialRingerType",
        "getInitialUnusedStreamVolume",
        "getInitialVolume",
        "getMusicStreamActualVolume",
        "()Ljava/lang/Float;",
        "Ljava/lang/Float;",
        "getMusicStreamAttemptedVolume",
        "getSystemStreamActualVolume",
        "getSystemStreamAttemptedVolume",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final audioTrackNumSamples:Ljava/lang/Integer;

.field private final audioTrackRetryCount:Ljava/lang/Integer;

.field private final audioTrackState:Ljava/lang/Integer;

.field private final errorMessage:Ljava/lang/String;

.field private final initialRingerType:Ljava/lang/Integer;

.field private final initialUnusedStreamVolume:Ljava/lang/Integer;

.field private final initialVolume:Ljava/lang/Integer;

.field private final musicStreamActualVolume:Ljava/lang/Float;

.field private final musicStreamAttemptedVolume:Ljava/lang/Float;

.field private final systemStreamActualVolume:Ljava/lang/Float;

.field private final systemStreamAttemptedVolume:Ljava/lang/Float;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 1

    const-string v0, "eventValue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->READER:Lcom/squareup/eventstream/v1/EventStream$Name;

    invoke-direct {p0, v0, p1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/squareup/wavpool/swipe/AudioEvent;->systemStreamActualVolume:Ljava/lang/Float;

    iput-object p3, p0, Lcom/squareup/wavpool/swipe/AudioEvent;->systemStreamAttemptedVolume:Ljava/lang/Float;

    iput-object p4, p0, Lcom/squareup/wavpool/swipe/AudioEvent;->musicStreamActualVolume:Ljava/lang/Float;

    iput-object p5, p0, Lcom/squareup/wavpool/swipe/AudioEvent;->musicStreamAttemptedVolume:Ljava/lang/Float;

    iput-object p6, p0, Lcom/squareup/wavpool/swipe/AudioEvent;->audioTrackState:Ljava/lang/Integer;

    iput-object p7, p0, Lcom/squareup/wavpool/swipe/AudioEvent;->audioTrackRetryCount:Ljava/lang/Integer;

    iput-object p8, p0, Lcom/squareup/wavpool/swipe/AudioEvent;->audioTrackNumSamples:Ljava/lang/Integer;

    iput-object p9, p0, Lcom/squareup/wavpool/swipe/AudioEvent;->errorMessage:Ljava/lang/String;

    iput-object p10, p0, Lcom/squareup/wavpool/swipe/AudioEvent;->initialVolume:Ljava/lang/Integer;

    iput-object p11, p0, Lcom/squareup/wavpool/swipe/AudioEvent;->initialUnusedStreamVolume:Ljava/lang/Integer;

    iput-object p12, p0, Lcom/squareup/wavpool/swipe/AudioEvent;->initialRingerType:Ljava/lang/Integer;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 12

    move/from16 v0, p13

    and-int/lit8 v1, v0, 0x2

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 13
    move-object v1, v2

    check-cast v1, Ljava/lang/Float;

    goto :goto_0

    :cond_0
    move-object v1, p2

    :goto_0
    and-int/lit8 v3, v0, 0x4

    if-eqz v3, :cond_1

    .line 14
    move-object v3, v2

    check-cast v3, Ljava/lang/Float;

    goto :goto_1

    :cond_1
    move-object v3, p3

    :goto_1
    and-int/lit8 v4, v0, 0x8

    if-eqz v4, :cond_2

    .line 15
    move-object v4, v2

    check-cast v4, Ljava/lang/Float;

    goto :goto_2

    :cond_2
    move-object/from16 v4, p4

    :goto_2
    and-int/lit8 v5, v0, 0x10

    if-eqz v5, :cond_3

    .line 16
    move-object v5, v2

    check-cast v5, Ljava/lang/Float;

    goto :goto_3

    :cond_3
    move-object/from16 v5, p5

    :goto_3
    and-int/lit8 v6, v0, 0x20

    if-eqz v6, :cond_4

    .line 17
    move-object v6, v2

    check-cast v6, Ljava/lang/Integer;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p6

    :goto_4
    and-int/lit8 v7, v0, 0x40

    if-eqz v7, :cond_5

    .line 18
    move-object v7, v2

    check-cast v7, Ljava/lang/Integer;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p7

    :goto_5
    and-int/lit16 v8, v0, 0x80

    if-eqz v8, :cond_6

    .line 19
    move-object v8, v2

    check-cast v8, Ljava/lang/Integer;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p8

    :goto_6
    and-int/lit16 v9, v0, 0x100

    if-eqz v9, :cond_7

    .line 20
    move-object v9, v2

    check-cast v9, Ljava/lang/String;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p9

    :goto_7
    and-int/lit16 v10, v0, 0x200

    if-eqz v10, :cond_8

    .line 21
    move-object v10, v2

    check-cast v10, Ljava/lang/Integer;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p10

    :goto_8
    and-int/lit16 v11, v0, 0x400

    if-eqz v11, :cond_9

    .line 22
    move-object v11, v2

    check-cast v11, Ljava/lang/Integer;

    goto :goto_9

    :cond_9
    move-object/from16 v11, p11

    :goto_9
    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_a

    .line 23
    move-object v0, v2

    check-cast v0, Ljava/lang/Integer;

    goto :goto_a

    :cond_a
    move-object/from16 v0, p12

    :goto_a
    move-object p2, p0

    move-object p3, p1

    move-object/from16 p4, v1

    move-object/from16 p5, v3

    move-object/from16 p6, v4

    move-object/from16 p7, v5

    move-object/from16 p8, v6

    move-object/from16 p9, v7

    move-object/from16 p10, v8

    move-object/from16 p11, v9

    move-object/from16 p12, v10

    move-object/from16 p13, v11

    move-object/from16 p14, v0

    invoke-direct/range {p2 .. p14}, Lcom/squareup/wavpool/swipe/AudioEvent;-><init>(Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-void
.end method


# virtual methods
.method public final getAudioTrackNumSamples()Ljava/lang/Integer;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioEvent;->audioTrackNumSamples:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getAudioTrackRetryCount()Ljava/lang/Integer;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioEvent;->audioTrackRetryCount:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getAudioTrackState()Ljava/lang/Integer;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioEvent;->audioTrackState:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getErrorMessage()Ljava/lang/String;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioEvent;->errorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final getInitialRingerType()Ljava/lang/Integer;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioEvent;->initialRingerType:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getInitialUnusedStreamVolume()Ljava/lang/Integer;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioEvent;->initialUnusedStreamVolume:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getInitialVolume()Ljava/lang/Integer;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioEvent;->initialVolume:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getMusicStreamActualVolume()Ljava/lang/Float;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioEvent;->musicStreamActualVolume:Ljava/lang/Float;

    return-object v0
.end method

.method public final getMusicStreamAttemptedVolume()Ljava/lang/Float;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioEvent;->musicStreamAttemptedVolume:Ljava/lang/Float;

    return-object v0
.end method

.method public final getSystemStreamActualVolume()Ljava/lang/Float;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioEvent;->systemStreamActualVolume:Ljava/lang/Float;

    return-object v0
.end method

.method public final getSystemStreamAttemptedVolume()Ljava/lang/Float;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioEvent;->systemStreamAttemptedVolume:Ljava/lang/Float;

    return-object v0
.end method
