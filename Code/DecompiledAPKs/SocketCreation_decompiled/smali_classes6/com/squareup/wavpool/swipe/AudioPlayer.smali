.class public interface abstract Lcom/squareup/wavpool/swipe/AudioPlayer;
.super Ljava/lang/Object;
.source "AudioPlayer.java"


# virtual methods
.method public abstract enableTransmission()V
.end method

.method public abstract forwardToReader(Z[S)V
.end method

.method public abstract notifyTransmissionComplete()V
.end method

.method public abstract reset()V
.end method

.method public abstract stopSendingToReader()V
.end method
