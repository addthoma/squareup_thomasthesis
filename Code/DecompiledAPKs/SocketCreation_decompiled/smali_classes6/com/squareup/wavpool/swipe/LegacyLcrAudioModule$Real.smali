.class public abstract Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule$Real;
.super Ljava/lang/Object;
.source "LegacyLcrAudioModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Real"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideAudioBackendInterface(Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;)Lcom/squareup/wavpool/swipe/AudioBackendBridge;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/wavpool/swipe/AudioBackendBridge$AudioBackendNativeBridge;

    invoke-direct {v0, p0}, Lcom/squareup/wavpool/swipe/AudioBackendBridge$AudioBackendNativeBridge;-><init>(Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;)V

    return-object v0
.end method

.method static provideCardReaderInterface(Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;)Lcom/squareup/wavpool/swipe/CardReaderBridge;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/wavpool/swipe/CardReaderBridge$CardReaderNativeBridge;

    invoke-direct {v0, p0}, Lcom/squareup/wavpool/swipe/CardReaderBridge$CardReaderNativeBridge;-><init>(Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;)V

    return-object v0
.end method


# virtual methods
.method abstract provideR4Decoder(Lcom/squareup/wavpool/swipe/AudioBackendLegacy;)Lcom/squareup/squarewave/m1/R4Decoder;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
