.class Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;
.super Ljava/lang/Object;
.source "AndroidAudioPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Tones"
.end annotation


# instance fields
.field private audioTrack:Landroid/media/AudioTrack;

.field private final loop:Z

.field private final samples:[S

.field final synthetic this$0:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;


# direct methods
.method constructor <init>(Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;Z[S)V
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->this$0:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    iput-boolean p2, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->loop:Z

    .line 191
    iput-object p3, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->samples:[S

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;)Z
    .locals 0

    .line 184
    iget-boolean p0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->loop:Z

    return p0
.end method


# virtual methods
.method public play()V
    .locals 4

    .line 195
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->this$0:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;

    monitor-enter v0

    .line 199
    :try_start_0
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->samples:[S

    array-length v1, v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "Cannot play a zero-length sample."

    invoke-static {v1, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 200
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->this$0:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;

    iget-boolean v2, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->loop:Z

    iget-object v3, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->samples:[S

    invoke-static {v1, v2, v3}, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->access$100(Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;Z[S)Landroid/media/AudioTrack;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->audioTrack:Landroid/media/AudioTrack;

    .line 202
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->audioTrack:Landroid/media/AudioTrack;

    if-nez v1, :cond_1

    .line 205
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->this$0:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;

    invoke-virtual {v1}, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->notifyTransmissionComplete()V

    goto :goto_1

    .line 207
    :cond_1
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v1}, Landroid/media/AudioTrack;->play()V

    .line 209
    :goto_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public stop()V
    .locals 3

    .line 213
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->this$0:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;

    monitor-enter v0

    .line 215
    :try_start_0
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->audioTrack:Landroid/media/AudioTrack;

    if-nez v1, :cond_0

    monitor-exit v0

    return-void

    .line 216
    :cond_0
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v1}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    monitor-exit v0

    return-void

    .line 218
    :cond_1
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v1}, Landroid/media/AudioTrack;->getState()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 219
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v1}, Landroid/media/AudioTrack;->stop()V

    .line 222
    :cond_2
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->this$0:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;

    invoke-virtual {v1}, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->notifyTransmissionComplete()V

    .line 223
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v1}, Landroid/media/AudioTrack;->release()V

    const/4 v1, 0x0

    .line 224
    iput-object v1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$Tones;->audioTrack:Landroid/media/AudioTrack;

    .line 225
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
