.class public final Lcom/squareup/wavpool/swipe/HeadsetConnectionState;
.super Ljava/lang/Object;
.source "HeadsetConnectionState.kt"

# interfaces
.implements Lcom/squareup/wavpool/swipe/HeadsetConnection;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005J\t\u0010\u0006\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u001d\u0010\u0008\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\u00032\u0008\u0010\n\u001a\u0004\u0018\u00010\u000bH\u00d6\u0003J\t\u0010\u000c\u001a\u00020\rH\u00d6\u0001J\u0008\u0010\u000e\u001a\u00020\u0003H\u0016J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
        "Lcom/squareup/wavpool/swipe/HeadsetConnection;",
        "connected",
        "",
        "hasMicInput",
        "(ZZ)V",
        "component1",
        "component2",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "isReaderConnected",
        "toString",
        "",
        "headset_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public final connected:Z

.field public final hasMicInput:Z


# direct methods
.method public constructor <init>(ZZ)V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->connected:Z

    iput-boolean p2, p0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->hasMicInput:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/wavpool/swipe/HeadsetConnectionState;ZZILjava/lang/Object;)Lcom/squareup/wavpool/swipe/HeadsetConnectionState;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-boolean p1, p0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->connected:Z

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-boolean p2, p0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->hasMicInput:Z

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->copy(ZZ)Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->connected:Z

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->hasMicInput:Z

    return v0
.end method

.method public final copy(ZZ)Lcom/squareup/wavpool/swipe/HeadsetConnectionState;
    .locals 1

    new-instance v0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    invoke-direct {v0, p1, p2}, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;-><init>(ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    iget-boolean v0, p0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->connected:Z

    iget-boolean v1, p1, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->connected:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->hasMicInput:Z

    iget-boolean p1, p1, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->hasMicInput:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->connected:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->hasMicInput:Z

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public isReaderConnected()Z
    .locals 1

    .line 8
    iget-boolean v0, p0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->connected:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->hasMicInput:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HeadsetConnectionState(connected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->connected:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", hasMicInput="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->hasMicInput:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
