.class public Lcom/squareup/wavpool/swipe/InputSampleRateFinder;
.super Ljava/lang/Object;
.source "InputSampleRateFinder.java"


# static fields
.field public static final CANNOT_FIND_SAMPLE_RATE:I = -0x1

.field private static final INPUT_SAMPLE_RATES:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x4

    new-array v0, v0, [I

    .line 14
    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/wavpool/swipe/InputSampleRateFinder;->INPUT_SAMPLE_RATES:[I

    return-void

    nop

    :array_0
    .array-data 4
        0xac44
        0x5622
        0x2b11
        0x1f40
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static find(I)I
    .locals 4

    if-lez p0, :cond_0

    return p0

    .line 25
    :cond_0
    invoke-static {}, Lcom/squareup/wavpool/swipe/InputSampleRateFinder;->scanSampleRates()I

    move-result p0

    const/4 v0, -0x1

    if-ne p0, v0, :cond_1

    .line 28
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "getMinBufferSize() failed for all input sample rates."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 29
    :cond_1
    sget-object v0, Lcom/squareup/wavpool/swipe/InputSampleRateFinder;->INPUT_SAMPLE_RATES:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    if-ge p0, v0, :cond_2

    .line 30
    new-instance v0, Ljava/lang/RuntimeException;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    .line 31
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    const-string v1, "Using reduced input sample rate: %d"

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 30
    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    :cond_2
    :goto_0
    return p0
.end method

.method private static scanSampleRates()I
    .locals 8

    .line 38
    sget-object v0, Lcom/squareup/wavpool/swipe/InputSampleRateFinder;->INPUT_SAMPLE_RATES:[I

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    const/4 v4, -0x1

    if-ge v3, v1, :cond_1

    aget v5, v0, v3

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    .line 39
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    const-string v7, "Trying input sample rate %d"

    invoke-static {v7, v6}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/16 v6, 0x10

    const/4 v7, 0x2

    .line 43
    :try_start_0
    invoke-static {v5, v6, v7}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v6

    const-string v7, "Exception encountered while searching for supported input sample rate."

    .line 47
    invoke-static {v6, v7}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    :goto_1
    if-lez v4, :cond_0

    return v5

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return v4
.end method
