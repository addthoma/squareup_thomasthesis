.class public abstract Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule;
.super Ljava/lang/Object;
.source "LegacyLcrAudioModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule$Real;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideAudioBackend(Ljavax/inject/Provider;Ljava/lang/Integer;Lcom/squareup/wavpool/swipe/AudioPlayer;Ljava/util/concurrent/ExecutorService;Ljavax/inject/Provider;Lcom/squareup/squarewave/EventDataListener;Lcom/squareup/wavpool/swipe/AudioBackendBridge;Lcom/squareup/wavpool/swipe/CardReaderBridge;)Lcom/squareup/wavpool/swipe/AudioBackendLegacy;
    .locals 10
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Integer;",
            "Lcom/squareup/wavpool/swipe/AudioPlayer;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/squarewave/EventDataListener;",
            "Lcom/squareup/wavpool/swipe/AudioBackendBridge;",
            "Lcom/squareup/wavpool/swipe/CardReaderBridge;",
            ")",
            "Lcom/squareup/wavpool/swipe/AudioBackendLegacy;"
        }
    .end annotation

    .line 53
    new-instance v9, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;-><init>(Ljavax/inject/Provider;Ljava/lang/Integer;Lcom/squareup/wavpool/swipe/AudioPlayer;Ljava/util/concurrent/ExecutorService;Ljavax/inject/Provider;Lcom/squareup/squarewave/EventDataListener;Lcom/squareup/wavpool/swipe/AudioBackendBridge;Lcom/squareup/wavpool/swipe/CardReaderBridge;)V

    return-object v9
.end method

.method static provideAudioBackendSession(Lcom/squareup/wavpool/swipe/AudioBackendLegacy;)Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 62
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->getSession()Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;

    move-result-object p0

    return-object p0
.end method

.method static provideReaderTypeProvider(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/wavpool/swipe/ReaderTypeProvider;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 67
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/wavpool/swipe/-$$Lambda$SLIvNnzAcM20z0ymOhpnWptNtDk;

    invoke-direct {v0, p0}, Lcom/squareup/wavpool/swipe/-$$Lambda$SLIvNnzAcM20z0ymOhpnWptNtDk;-><init>(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-object v0
.end method


# virtual methods
.method abstract provideLcrBackend(Lcom/squareup/wavpool/swipe/AudioBackendLegacy;)Lcom/squareup/cardreader/LcrBackend;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideSampleProcessor(Lcom/squareup/wavpool/swipe/AudioBackendLegacy;)Lcom/squareup/squarewave/gum/SampleProcessor;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
