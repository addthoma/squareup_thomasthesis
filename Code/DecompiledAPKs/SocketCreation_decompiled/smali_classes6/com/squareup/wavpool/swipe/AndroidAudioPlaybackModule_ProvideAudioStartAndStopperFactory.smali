.class public final Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;
.super Ljava/lang/Object;
.source "AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/wavpool/swipe/AudioStartAndStopper;",
        ">;"
    }
.end annotation


# instance fields
.field private final audioManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/media/AudioManager;",
            ">;"
        }
    .end annotation
.end field

.field private final audioVolumeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderInfoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final headsetProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;"
        }
    .end annotation
.end field

.field private final isRunningProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final r6CardReaderAwakenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/R6CardReaderAwakener;",
            ">;"
        }
    .end annotation
.end field

.field private final recorderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Recorder;",
            ">;"
        }
    .end annotation
.end field

.field private final resumerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/LcrBackend;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/media/AudioManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Recorder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/LcrBackend;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/R6CardReaderAwakener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Float;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;)V"
        }
    .end annotation

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->busProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p2, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->isRunningProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p3, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->audioManagerProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p4, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p5, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->headsetProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p6, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->recorderProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p7, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->libraryLoaderProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p8, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->resumerProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p9, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->r6CardReaderAwakenerProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p10, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->audioVolumeProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p11, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/media/AudioManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Recorder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/LcrBackend;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/R6CardReaderAwakener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Float;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;"
        }
    .end annotation

    .line 80
    new-instance v12, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static provideAudioStartAndStopper(Lcom/squareup/badbus/BadBus;Ljavax/inject/Provider;Landroid/media/AudioManager;Lcom/squareup/cardreader/CardReaderInfo;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/loader/LibraryLoader;Ljavax/inject/Provider;Lcom/squareup/cardreader/R6CardReaderAwakener;FLcom/squareup/cardreader/CardReaderListeners;)Lcom/squareup/wavpool/swipe/AudioStartAndStopper;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/badbus/BadBus;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Landroid/media/AudioManager;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Recorder;",
            ">;",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/LcrBackend;",
            ">;",
            "Lcom/squareup/cardreader/R6CardReaderAwakener;",
            "F",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ")",
            "Lcom/squareup/wavpool/swipe/AudioStartAndStopper;"
        }
    .end annotation

    .line 89
    invoke-static/range {p0 .. p10}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule;->provideAudioStartAndStopper(Lcom/squareup/badbus/BadBus;Ljavax/inject/Provider;Landroid/media/AudioManager;Lcom/squareup/cardreader/CardReaderInfo;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/loader/LibraryLoader;Ljavax/inject/Provider;Lcom/squareup/cardreader/R6CardReaderAwakener;FLcom/squareup/cardreader/CardReaderListeners;)Lcom/squareup/wavpool/swipe/AudioStartAndStopper;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/wavpool/swipe/AudioStartAndStopper;
    .locals 12

    .line 69
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/badbus/BadBus;

    iget-object v2, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->isRunningProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->audioManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/media/AudioManager;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/cardreader/CardReaderInfo;

    iget-object v5, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->headsetProvider:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->recorderProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->libraryLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/cardreader/loader/LibraryLoader;

    iget-object v8, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->resumerProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->r6CardReaderAwakenerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/cardreader/R6CardReaderAwakener;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->audioVolumeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v10

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/cardreader/CardReaderListeners;

    invoke-static/range {v1 .. v11}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->provideAudioStartAndStopper(Lcom/squareup/badbus/BadBus;Ljavax/inject/Provider;Landroid/media/AudioManager;Lcom/squareup/cardreader/CardReaderInfo;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/loader/LibraryLoader;Ljavax/inject/Provider;Lcom/squareup/cardreader/R6CardReaderAwakener;FLcom/squareup/cardreader/CardReaderListeners;)Lcom/squareup/wavpool/swipe/AudioStartAndStopper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->get()Lcom/squareup/wavpool/swipe/AudioStartAndStopper;

    move-result-object v0

    return-object v0
.end method
