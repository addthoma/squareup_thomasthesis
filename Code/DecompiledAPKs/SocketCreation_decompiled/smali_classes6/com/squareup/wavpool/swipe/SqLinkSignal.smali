.class public final Lcom/squareup/wavpool/swipe/SqLinkSignal;
.super Ljava/lang/Object;
.source "SqLinkSignal.java"


# static fields
.field private static final DO_NOT_IGNORE:Z = false

.field private static final IGNORE:Z = true

.field public static final MAX_SLOW_DURATION_MS:I = 0x75d


# instance fields
.field private final decoded:Z

.field private final linkType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

.field private final packetType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

.field private final readerCounter:J

.field private final readerId:Ljava/lang/String;

.field private final timestamp:J


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/decode/DemodResult;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;J)V
    .locals 1

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-wide p4, p0, Lcom/squareup/wavpool/swipe/SqLinkSignal;->timestamp:J

    .line 56
    invoke-virtual {p1}, Lcom/squareup/squarewave/decode/DemodResult;->isSuccessfulDemod()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/wavpool/swipe/SqLinkSignal;->decoded:Z

    .line 58
    iget-boolean p1, p0, Lcom/squareup/wavpool/swipe/SqLinkSignal;->decoded:Z

    const-wide/16 p4, 0x0

    if-eqz p1, :cond_1

    .line 59
    iget-object p1, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    iput-object p1, p0, Lcom/squareup/wavpool/swipe/SqLinkSignal;->packetType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    .line 60
    iget-object p1, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->reader_hardware_id:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/wavpool/swipe/SqLinkSignal;->readerId:Ljava/lang/String;

    .line 61
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/SqLinkSignal;->packetType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->R4_CARD_DATA:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    if-ne p1, v0, :cond_0

    .line 62
    iget-object p1, p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->r4_packet:Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->r4_card_data:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->counter:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/squareup/wavpool/swipe/SqLinkSignal;->readerCounter:J

    goto :goto_0

    .line 64
    :cond_0
    iput-wide p4, p0, Lcom/squareup/wavpool/swipe/SqLinkSignal;->readerCounter:J

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 67
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/SqLinkSignal;->packetType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    .line 68
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/SqLinkSignal;->readerId:Ljava/lang/String;

    .line 69
    iput-wide p4, p0, Lcom/squareup/wavpool/swipe/SqLinkSignal;->readerCounter:J

    .line 71
    :goto_0
    iput-object p3, p0, Lcom/squareup/wavpool/swipe/SqLinkSignal;->linkType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 72
    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_FAST:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    if-eq p3, p1, :cond_3

    sget-object p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_SLOW:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    if-ne p3, p1, :cond_2

    goto :goto_1

    .line 73
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Not an SqLink signal: "

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_1
    return-void
.end method


# virtual methods
.method public shouldBeIgnored(Lcom/squareup/wavpool/swipe/SqLinkSignal;)Z
    .locals 7

    .line 81
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SqLinkSignal;->linkType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_FAST:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    .line 83
    iget-boolean p1, p0, Lcom/squareup/wavpool/swipe/SqLinkSignal;->decoded:Z

    xor-int/2addr p1, v2

    return p1

    :cond_0
    const/4 v0, 0x0

    if-nez p1, :cond_1

    return v0

    .line 93
    :cond_1
    iget-boolean v1, p1, Lcom/squareup/wavpool/swipe/SqLinkSignal;->decoded:Z

    if-nez v1, :cond_2

    return v0

    .line 98
    :cond_2
    iget-object v1, p1, Lcom/squareup/wavpool/swipe/SqLinkSignal;->linkType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    sget-object v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_SLOW:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    if-ne v1, v3, :cond_3

    return v0

    .line 105
    :cond_3
    iget-boolean v1, p0, Lcom/squareup/wavpool/swipe/SqLinkSignal;->decoded:Z

    if-eqz v1, :cond_7

    .line 108
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/SqLinkSignal;->packetType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    iget-object v3, p1, Lcom/squareup/wavpool/swipe/SqLinkSignal;->packetType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    invoke-virtual {v1, v3}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    return v0

    .line 112
    :cond_4
    iget-wide v3, p0, Lcom/squareup/wavpool/swipe/SqLinkSignal;->readerCounter:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_5

    iget-wide v5, p1, Lcom/squareup/wavpool/swipe/SqLinkSignal;->readerCounter:J

    cmp-long v1, v3, v5

    if-eqz v1, :cond_5

    return v0

    .line 115
    :cond_5
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/SqLinkSignal;->readerId:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object p1, p1, Lcom/squareup/wavpool/swipe/SqLinkSignal;->readerId:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_6

    return v0

    :cond_6
    return v2

    .line 122
    :cond_7
    iget-wide v3, p0, Lcom/squareup/wavpool/swipe/SqLinkSignal;->timestamp:J

    iget-wide v5, p1, Lcom/squareup/wavpool/swipe/SqLinkSignal;->timestamp:J

    sub-long/2addr v3, v5

    const-wide/16 v5, 0x75d

    cmp-long p1, v3, v5

    if-gez p1, :cond_8

    return v2

    :cond_8
    return v0
.end method
