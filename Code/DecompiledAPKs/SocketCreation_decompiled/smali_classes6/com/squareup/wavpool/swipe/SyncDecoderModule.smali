.class public abstract Lcom/squareup/wavpool/swipe/SyncDecoderModule;
.super Ljava/lang/Object;
.source "SyncDecoderModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/wavpool/swipe/DecoderModule;,
        Lcom/squareup/wavpool/swipe/DecoderModule$Prod;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideDecoderExecutor()Ljava/util/concurrent/ExecutorService;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 10
    new-instance v0, Lcom/squareup/wavpool/swipe/SyncExecutorService;

    invoke-direct {v0}, Lcom/squareup/wavpool/swipe/SyncExecutorService;-><init>()V

    return-object v0
.end method
