.class public Lcom/squareup/user/NotificationPopup;
.super Lcom/squareup/flowlegacy/DialogPopup;
.source "NotificationPopup.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/flowlegacy/DialogPopup<",
        "Lcom/squareup/server/account/protos/Notification;",
        "Lcom/squareup/server/account/protos/Notification$Button;",
        ">;"
    }
.end annotation


# static fields
.field public static final CANCELED:Lcom/squareup/server/account/protos/Notification$Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15
    new-instance v0, Lcom/squareup/server/account/protos/Notification$Button$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/Notification$Button$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/server/account/protos/Notification$Button$Builder;->build()Lcom/squareup/server/account/protos/Notification$Button;

    move-result-object v0

    sput-object v0, Lcom/squareup/user/NotificationPopup;->CANCELED:Lcom/squareup/server/account/protos/Notification$Button;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/squareup/flowlegacy/DialogPopup;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private static hasIndex(Ljava/util/List;I)Z
    .locals 0

    .line 53
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    if-ge p1, p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$createDialog$0(Lcom/squareup/mortar/PopupPresenter;Lcom/squareup/server/account/protos/Notification$Button;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 33
    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$createDialog$1(Lcom/squareup/mortar/PopupPresenter;Lcom/squareup/server/account/protos/Notification$Button;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 38
    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$createDialog$2(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 46
    sget-object p1, Lcom/squareup/user/NotificationPopup;->CANCELED:Lcom/squareup/server/account/protos/Notification$Button;

    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected createDialog(Lcom/squareup/server/account/protos/Notification;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/AlertDialog;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/account/protos/Notification;",
            "Z",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/server/account/protos/Notification;",
            "Lcom/squareup/server/account/protos/Notification$Button;",
            ">;)",
            "Landroid/app/AlertDialog;"
        }
    .end annotation

    .line 24
    new-instance p2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/squareup/user/NotificationPopup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 26
    iget-object v0, p1, Lcom/squareup/server/account/protos/Notification;->title:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Notification;->message:Ljava/lang/String;

    .line 27
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 29
    invoke-static {p1}, Lcom/squareup/server/account/Notifications;->getButtons(Lcom/squareup/server/account/protos/Notification;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    .line 30
    invoke-static {v0, v1}, Lcom/squareup/user/NotificationPopup;->hasIndex(Ljava/util/List;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 31
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/server/account/protos/Notification$Button;

    .line 32
    iget-object v3, v2, Lcom/squareup/server/account/protos/Notification$Button;->label:Ljava/lang/String;

    new-instance v4, Lcom/squareup/user/-$$Lambda$NotificationPopup$aeqdGAeFIF2L33DfK5LXEqxlzUs;

    invoke-direct {v4, p3, v2}, Lcom/squareup/user/-$$Lambda$NotificationPopup$aeqdGAeFIF2L33DfK5LXEqxlzUs;-><init>(Lcom/squareup/mortar/PopupPresenter;Lcom/squareup/server/account/protos/Notification$Button;)V

    invoke-virtual {p2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :cond_0
    const/4 v2, 0x1

    .line 35
    invoke-static {v0, v2}, Lcom/squareup/user/NotificationPopup;->hasIndex(Ljava/util/List;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 36
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/Notification$Button;

    .line 37
    iget-object v2, v0, Lcom/squareup/server/account/protos/Notification$Button;->label:Ljava/lang/String;

    new-instance v3, Lcom/squareup/user/-$$Lambda$NotificationPopup$1q-gYnYrZ1pD2s31m6nSGoyFpaU;

    invoke-direct {v3, p3, v0}, Lcom/squareup/user/-$$Lambda$NotificationPopup$1q-gYnYrZ1pD2s31m6nSGoyFpaU;-><init>(Lcom/squareup/mortar/PopupPresenter;Lcom/squareup/server/account/protos/Notification$Button;)V

    invoke-virtual {p2, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 42
    :cond_1
    invoke-virtual {p2, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 44
    iget-object p1, p1, Lcom/squareup/server/account/protos/Notification;->lock_out:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_2

    .line 45
    sget p1, Lcom/squareup/common/strings/R$string;->dismiss:I

    new-instance v0, Lcom/squareup/user/-$$Lambda$NotificationPopup$YEiWRO-5xpwJIIn0sH6V0vVu24g;

    invoke-direct {v0, p3}, Lcom/squareup/user/-$$Lambda$NotificationPopup$YEiWRO-5xpwJIIn0sH6V0vVu24g;-><init>(Lcom/squareup/mortar/PopupPresenter;)V

    invoke-virtual {p2, p1, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 49
    :cond_2
    invoke-virtual {p2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic createDialog(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/server/account/protos/Notification;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/user/NotificationPopup;->createDialog(Lcom/squareup/server/account/protos/Notification;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
