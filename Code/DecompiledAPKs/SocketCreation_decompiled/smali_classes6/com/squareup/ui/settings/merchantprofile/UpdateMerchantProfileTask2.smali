.class public Lcom/squareup/ui/settings/merchantprofile/UpdateMerchantProfileTask2;
.super Lcom/squareup/queue/RpcThreadTask;
.source "UpdateMerchantProfileTask2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/merchantprofile/UpdateMerchantProfileTask2$Component;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/RpcThreadTask<",
        "Lcom/squareup/server/SimpleResponse;",
        "Lcom/squareup/ui/settings/merchantprofile/UpdateMerchantProfileTask2$Component;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field transient merchantProfileUpdater:Lcom/squareup/merchantprofile/MerchantProfileUpdater;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field final snapshot:Lcom/squareup/merchantprofile/MerchantProfileSnapshot;


# direct methods
.method public constructor <init>(Lcom/squareup/merchantprofile/MerchantProfileSnapshot;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/squareup/queue/RpcThreadTask;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/UpdateMerchantProfileTask2;->snapshot:Lcom/squareup/merchantprofile/MerchantProfileSnapshot;

    return-void
.end method


# virtual methods
.method protected callOnRpcThread()Lcom/squareup/server/SimpleResponse;
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/UpdateMerchantProfileTask2;->merchantProfileUpdater:Lcom/squareup/merchantprofile/MerchantProfileUpdater;

    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/UpdateMerchantProfileTask2;->snapshot:Lcom/squareup/merchantprofile/MerchantProfileSnapshot;

    invoke-interface {v0, v1}, Lcom/squareup/merchantprofile/MerchantProfileUpdater;->updateProfile(Lcom/squareup/merchantprofile/MerchantProfileSnapshot;)Lio/reactivex/Single;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    .line 39
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    return-object v0
.end method

.method protected bridge synthetic callOnRpcThread()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/UpdateMerchantProfileTask2;->callOnRpcThread()Lcom/squareup/server/SimpleResponse;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_4

    .line 49
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 51
    :cond_1
    check-cast p1, Lcom/squareup/ui/settings/merchantprofile/UpdateMerchantProfileTask2;

    .line 53
    iget-object v2, p0, Lcom/squareup/ui/settings/merchantprofile/UpdateMerchantProfileTask2;->snapshot:Lcom/squareup/merchantprofile/MerchantProfileSnapshot;

    iget-object p1, p1, Lcom/squareup/ui/settings/merchantprofile/UpdateMerchantProfileTask2;->snapshot:Lcom/squareup/merchantprofile/MerchantProfileSnapshot;

    if-eqz v2, :cond_2

    invoke-virtual {v2, p1}, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_3

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    :goto_0
    return v1

    :cond_3
    return v0

    :cond_4
    :goto_1
    return v1
.end method

.method protected handleResponseOnMainThread(Lcom/squareup/server/SimpleResponse;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    return-object p1
.end method

.method protected bridge synthetic handleResponseOnMainThread(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/server/SimpleResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/merchantprofile/UpdateMerchantProfileTask2;->handleResponseOnMainThread(Lcom/squareup/server/SimpleResponse;)Lcom/squareup/server/SimpleResponse;

    move-result-object p1

    return-object p1
.end method

.method public hashCode()I
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/UpdateMerchantProfileTask2;->snapshot:Lcom/squareup/merchantprofile/MerchantProfileSnapshot;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public inject(Lcom/squareup/ui/settings/merchantprofile/UpdateMerchantProfileTask2$Component;)V
    .locals 0

    .line 63
    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/merchantprofile/UpdateMerchantProfileTask2$Component;->inject(Lcom/squareup/ui/settings/merchantprofile/UpdateMerchantProfileTask2;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/ui/settings/merchantprofile/UpdateMerchantProfileTask2$Component;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/merchantprofile/UpdateMerchantProfileTask2;->inject(Lcom/squareup/ui/settings/merchantprofile/UpdateMerchantProfileTask2$Component;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/UpdateMerchantProfileTask2;->snapshot:Lcom/squareup/merchantprofile/MerchantProfileSnapshot;

    invoke-virtual {v0}, Lcom/squareup/merchantprofile/MerchantProfileSnapshot;->asStringWithoutPIIForLogs()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
