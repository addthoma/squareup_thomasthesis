.class public Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "BleGattConnectionEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;
    }
.end annotation


# instance fields
.field public final characteristic:Ljava/lang/String;

.field public final descriptor:Ljava/lang/String;

.field public final gattStatus:Ljava/lang/Integer;

.field public final mtu:Ljava/lang/Integer;

.field public final name:Ljava/lang/String;

.field public final newState:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;)V
    .locals 3

    .line 24
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->READER:Lcom/squareup/eventstream/v1/EventStream$Name;

    iget-object v1, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->name:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 25
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent;->name:Ljava/lang/String;

    .line 26
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->characteristic:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent;->characteristic:Ljava/lang/String;

    .line 27
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->descriptor:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent;->descriptor:Ljava/lang/String;

    .line 28
    iget v0, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->newState:I

    const/4 v1, 0x0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    move-object v0, v1

    goto :goto_0

    :cond_0
    iget v0, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->newState:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent;->newState:Ljava/lang/Integer;

    .line 29
    iget v0, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->mtu:I

    if-ne v0, v2, :cond_1

    move-object v0, v1

    goto :goto_1

    :cond_1
    iget v0, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->mtu:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent;->mtu:Ljava/lang/Integer;

    .line 30
    iget v0, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->gattStatus:I

    if-ne v0, v2, :cond_2

    goto :goto_2

    :cond_2
    iget p1, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->gattStatus:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_2
    iput-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent;->gattStatus:Ljava/lang/Integer;

    return-void
.end method
