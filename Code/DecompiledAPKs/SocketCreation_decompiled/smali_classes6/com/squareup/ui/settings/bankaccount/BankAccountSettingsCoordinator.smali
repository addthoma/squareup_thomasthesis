.class public final Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "BankAccountSettingsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBankAccountSettingsCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BankAccountSettingsCoordinator.kt\ncom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator\n*L\n1#1,206:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0082\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\r\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0010\u0010\u001f\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0010\u0010 \u001a\u00020\u001c2\u0006\u0010!\u001a\u00020\"H\u0002J\u0010\u0010#\u001a\u00020\u001c2\u0006\u0010$\u001a\u00020%H\u0002J:\u0010&\u001a\u00020\u001c2\u0006\u0010\u000e\u001a\u00020\'2\u0008\u0010\u0015\u001a\u0004\u0018\u00010\'2\u0006\u0010(\u001a\u00020\"2\u0006\u0010)\u001a\u00020\"2\u0006\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020+H\u0002J\u0008\u0010-\u001a\u00020\u001cH\u0002J\u0008\u0010.\u001a\u00020\u001cH\u0002J\u0010\u0010/\u001a\u00020\u001c2\u0006\u00100\u001a\u000201H\u0002J\u001a\u00102\u001a\u00020\u001c2\u0006\u00103\u001a\u00020\u000f2\u0008\u00104\u001a\u0004\u0018\u00010\'H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00065"
    }
    d2 = {
        "Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "scopeRunner",
        "Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;",
        "(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "cancelVerificationButton",
        "Landroid/widget/Button;",
        "changeBankAccountButton",
        "context",
        "Landroid/content/Context;",
        "depositOptionsHint",
        "Lcom/squareup/widgets/MessageView;",
        "firstBankAccount",
        "Lcom/squareup/banklinking/widgets/BankAccountView;",
        "linkedBankAccountSection",
        "Landroid/view/ViewGroup;",
        "res",
        "Landroid/content/res/Resources;",
        "resendEmailButton",
        "secondBankAccount",
        "spinner",
        "Landroid/widget/ProgressBar;",
        "verificationHint",
        "warningMessage",
        "Lcom/squareup/noho/NohoMessageView;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "configureActionBar",
        "hasUpButton",
        "",
        "onCouldNotLoad",
        "failureMessage",
        "Lcom/squareup/receiving/FailureMessage;",
        "onHasBankAccount",
        "Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;",
        "canBeCanceled",
        "canResendEmail",
        "verificationHintText",
        "",
        "depositOptionsHintText",
        "onLoading",
        "onNoBankAccount",
        "onScreenData",
        "screenData",
        "Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;",
        "setupBankAccount",
        "bankAccountView",
        "bankAccount",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private cancelVerificationButton:Landroid/widget/Button;

.field private changeBankAccountButton:Landroid/widget/Button;

.field private context:Landroid/content/Context;

.field private depositOptionsHint:Lcom/squareup/widgets/MessageView;

.field private firstBankAccount:Lcom/squareup/banklinking/widgets/BankAccountView;

.field private linkedBankAccountSection:Landroid/view/ViewGroup;

.field private res:Landroid/content/res/Resources;

.field private resendEmailButton:Landroid/widget/Button;

.field private final scopeRunner:Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;

.field private secondBankAccount:Lcom/squareup/banklinking/widgets/BankAccountView;

.field private spinner:Landroid/widget/ProgressBar;

.field private verificationHint:Lcom/squareup/widgets/MessageView;

.field private warningMessage:Lcom/squareup/noho/NohoMessageView;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "scopeRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;

    return-void
.end method

.method public static final synthetic access$getScopeRunner$p(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;)Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;

    return-object p0
.end method

.method public static final synthetic access$onScreenData(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->onScreenData(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 193
    sget-object v0, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 194
    sget v0, Lcom/squareup/settingsapplet/R$id;->spinner:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->spinner:Landroid/widget/ProgressBar;

    .line 195
    sget v0, Lcom/squareup/settingsapplet/R$id;->warning_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoMessageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->warningMessage:Lcom/squareup/noho/NohoMessageView;

    .line 196
    sget v0, Lcom/squareup/settingsapplet/R$id;->linked_bank_account_section:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->linkedBankAccountSection:Landroid/view/ViewGroup;

    .line 197
    sget v0, Lcom/squareup/settingsapplet/R$id;->first_bank_account:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/banklinking/widgets/BankAccountView;

    iput-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->firstBankAccount:Lcom/squareup/banklinking/widgets/BankAccountView;

    .line 198
    sget v0, Lcom/squareup/settingsapplet/R$id;->second_bank_account:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/banklinking/widgets/BankAccountView;

    iput-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->secondBankAccount:Lcom/squareup/banklinking/widgets/BankAccountView;

    .line 199
    sget v0, Lcom/squareup/settingsapplet/R$id;->resend_email_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->resendEmailButton:Landroid/widget/Button;

    .line 200
    sget v0, Lcom/squareup/settingsapplet/R$id;->change_bank_account_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->changeBankAccountButton:Landroid/widget/Button;

    .line 201
    sget v0, Lcom/squareup/settingsapplet/R$id;->cancel_verification_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->cancelVerificationButton:Landroid/widget/Button;

    .line 202
    sget v0, Lcom/squareup/settingsapplet/R$id;->verification_hint:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->verificationHint:Lcom/squareup/widgets/MessageView;

    .line 203
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_options_hint:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->depositOptionsHint:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private final configureActionBar(Z)V
    .locals 4

    .line 100
    new-instance v0, Lcom/squareup/resources/ResourceString;

    sget v1, Lcom/squareup/registerlib/R$string;->bank_account_title:I

    invoke-direct {v0, v1}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    .line 108
    iget-object v1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v1, :cond_0

    const-string v2, "actionBar"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 101
    :cond_0
    new-instance v2, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 103
    check-cast v0, Lcom/squareup/resources/TextModel;

    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    if-eqz p1, :cond_1

    .line 105
    sget-object p1, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$configureActionBar$1$1;

    iget-object v3, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;

    invoke-direct {v0, v3}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$configureActionBar$1$1;-><init>(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v2, p1, v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 108
    :cond_1
    invoke-virtual {v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final onCouldNotLoad(Lcom/squareup/receiving/FailureMessage;)V
    .locals 3

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->spinner:Landroid/widget/ProgressBar;

    if-nez v0, :cond_0

    const-string v1, "spinner"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->linkedBankAccountSection:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    const-string v2, "linkedBankAccountSection"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->warningMessage:Lcom/squareup/noho/NohoMessageView;

    const-string v1, "warningMessage"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoMessageView;->setTitle(Ljava/lang/CharSequence;)V

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->warningMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoMessageView;->setMessage(Ljava/lang/CharSequence;)V

    .line 182
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getRetryable()Z

    move-result p1

    if-eqz p1, :cond_7

    .line 183
    iget-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->warningMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    sget v0, Lcom/squareup/common/strings/R$string;->retry:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonText(I)V

    .line 184
    iget-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->warningMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    new-instance v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$onCouldNotLoad$1;

    iget-object v2, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;

    invoke-direct {v0, v2}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$onCouldNotLoad$1;-><init>(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    new-instance v2, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$sam$java_lang_Runnable$0;

    invoke-direct {v2, v0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    const-string v2, "debounceRunnable(scopeRunner::refresh)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 185
    iget-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->warningMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p1}, Lcom/squareup/noho/NohoMessageView;->showSecondaryButton()V

    goto :goto_0

    .line 187
    :cond_7
    iget-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->warningMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {p1}, Lcom/squareup/noho/NohoMessageView;->hideSecondaryButton()V

    .line 189
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->warningMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_9

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setVisibility(I)V

    return-void
.end method

.method private final onHasBankAccount(Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;ZZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 2

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->firstBankAccount:Lcom/squareup/banklinking/widgets/BankAccountView;

    if-nez v0, :cond_0

    const-string v1, "firstBankAccount"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->setupBankAccount(Lcom/squareup/banklinking/widgets/BankAccountView;Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;)V

    .line 137
    iget-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->secondBankAccount:Lcom/squareup/banklinking/widgets/BankAccountView;

    if-nez p1, :cond_1

    const-string v0, "secondBankAccount"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->setupBankAccount(Lcom/squareup/banklinking/widgets/BankAccountView;Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;)V

    .line 138
    iget-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->spinner:Landroid/widget/ProgressBar;

    if-nez p1, :cond_2

    const-string p2, "spinner"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 139
    iget-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->warningMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_3

    const-string v0, "warningMessage"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoMessageView;->setVisibility(I)V

    .line 140
    iget-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->resendEmailButton:Landroid/widget/Button;

    if-nez p1, :cond_4

    const-string p2, "resendEmailButton"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast p1, Landroid/view/View;

    invoke-static {p1, p4}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 141
    iget-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->cancelVerificationButton:Landroid/widget/Button;

    if-nez p1, :cond_5

    const-string p2, "cancelVerificationButton"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast p1, Landroid/view/View;

    invoke-static {p1, p3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 142
    iget-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->changeBankAccountButton:Landroid/widget/Button;

    const-string p2, "changeBankAccountButton"

    if-nez p1, :cond_6

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast p1, Landroid/view/View;

    xor-int/lit8 p3, p3, 0x1

    invoke-static {p1, p3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 143
    iget-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->changeBankAccountButton:Landroid/widget/Button;

    if-nez p1, :cond_7

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 144
    :cond_7
    new-instance p2, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$onHasBankAccount$1;

    invoke-direct {p2, p0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$onHasBankAccount$1;-><init>(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-static {p2}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    check-cast p2, Landroid/view/View$OnClickListener;

    .line 143
    invoke-virtual {p1, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    iget-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->verificationHint:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_8

    const-string p2, "verificationHint"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {p1, p5}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->depositOptionsHint:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_9

    const-string p2, "depositOptionsHint"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {p1, p6}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    iget-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->linkedBankAccountSection:Landroid/view/ViewGroup;

    if-nez p1, :cond_a

    const-string p2, "linkedBankAccountSection"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method private final onLoading()V
    .locals 3

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->warningMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_0

    const-string v1, "warningMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setVisibility(I)V

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->linkedBankAccountSection:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    const-string v2, "linkedBankAccountSection"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->spinner:Landroid/widget/ProgressBar;

    if-nez v0, :cond_2

    const-string v1, "spinner"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method private final onNoBankAccount()V
    .locals 4

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->spinner:Landroid/widget/ProgressBar;

    if-nez v0, :cond_0

    const-string v1, "spinner"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->linkedBankAccountSection:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    const-string v2, "linkedBankAccountSection"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->warningMessage:Lcom/squareup/noho/NohoMessageView;

    const-string v1, "warningMessage"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoMessageView;->setTitle(I)V

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->warningMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget v2, Lcom/squareup/settingsapplet/R$string;->add_bank_account_message_title:I

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoMessageView;->setMessage(I)V

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->warningMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    sget v2, Lcom/squareup/onboarding/common/R$string;->add_bank_account:I

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonText(I)V

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->warningMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 124
    :cond_5
    new-instance v2, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$onNoBankAccount$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$onNoBankAccount$1;-><init>(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v2

    const-string v3, "debounce { scopeRunner.addBankAccount() }"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->warningMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setVisibility(I)V

    return-void
.end method

.method private final onScreenData(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;)V
    .locals 7

    .line 82
    invoke-virtual {p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->getHasUpButton()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->configureActionBar(Z)V

    .line 84
    invoke-virtual {p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->getState()Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 95
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->getFailureMessage()Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    if-nez p1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->onCouldNotLoad(Lcom/squareup/receiving/FailureMessage;)V

    goto :goto_0

    .line 88
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->getFirstBankAccount()Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;

    move-result-object v1

    if-nez v1, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 89
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->getSecondBankAccount()Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;

    move-result-object v2

    .line 90
    invoke-virtual {p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->getCanBeCanceled()Z

    move-result v3

    .line 91
    invoke-virtual {p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->getCanResendEmail()Z

    move-result v4

    .line 92
    invoke-virtual {p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->getVerificationHintText()Ljava/lang/CharSequence;

    move-result-object v5

    .line 93
    invoke-virtual {p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->getDepositOptionsHintText()Ljava/lang/CharSequence;

    move-result-object v6

    move-object v0, p0

    .line 87
    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->onHasBankAccount(Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;ZZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 86
    :cond_4
    invoke-direct {p0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->onNoBankAccount()V

    goto :goto_0

    .line 85
    :cond_5
    invoke-direct {p0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->onLoading()V

    :goto_0
    return-void
.end method

.method private final setupBankAccount(Lcom/squareup/banklinking/widgets/BankAccountView;Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;)V
    .locals 2

    if-nez p2, :cond_0

    const/16 p2, 0x8

    .line 155
    invoke-virtual {p1, p2}, Lcom/squareup/banklinking/widgets/BankAccountView;->setVisibility(I)V

    return-void

    .line 159
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/banklinking/widgets/BankAccountView;->setTitle(Ljava/lang/CharSequence;)V

    .line 160
    invoke-virtual {p2}, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->getAccountType()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/banklinking/widgets/BankAccountView;->setAccountType(Ljava/lang/CharSequence;)V

    .line 161
    invoke-virtual {p2}, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->getAccountHolderName()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/banklinking/widgets/BankAccountView;->setAccountHolderName(Ljava/lang/CharSequence;)V

    .line 163
    invoke-virtual {p2}, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->getPrimaryInstitutionNumberName()Ljava/lang/CharSequence;

    move-result-object v0

    .line 164
    invoke-virtual {p2}, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->getPrimaryInstitutionNumber()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 162
    invoke-virtual {p1, v0, v1}, Lcom/squareup/banklinking/widgets/BankAccountView;->setPrimaryInstitutionalNumber(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 167
    invoke-virtual {p2}, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->getSecondaryInstitutionNumberName()Ljava/lang/CharSequence;

    move-result-object v0

    .line 168
    invoke-virtual {p2}, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->getSecondaryInstitutionNumber()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 166
    invoke-virtual {p1, v0, v1}, Lcom/squareup/banklinking/widgets/BankAccountView;->setSecondaryInstitutionalNumber(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 170
    invoke-virtual {p2}, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->getAccountNumber()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/banklinking/widgets/BankAccountView;->setAccountNumber(Ljava/lang/CharSequence;)V

    .line 171
    invoke-virtual {p2}, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->getVerificationStatus()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/banklinking/widgets/BankAccountView;->setVerificationStatus(Ljava/lang/CharSequence;)V

    .line 172
    invoke-virtual {p2}, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->getEstimatedCompletionDate()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/banklinking/widgets/BankAccountView;->setEstimatedCompletionDate(Ljava/lang/CharSequence;)V

    const/4 p2, 0x0

    .line 173
    invoke-virtual {p1, p2}, Lcom/squareup/banklinking/widgets/BankAccountView;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 57
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    const-string v1, "Path.get(view.context)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;

    .line 58
    invoke-virtual {v0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;->getConfirmationToken()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 59
    iget-object v1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;->getConfirmationToken()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v1, v2}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->confirmBankAccount(Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 60
    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;->setConfirmationToken(Ljava/lang/String;)V

    .line 63
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->bindViews(Landroid/view/View;)V

    .line 65
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "view.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->context:Landroid/content/Context;

    .line 66
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "view.resources"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->res:Landroid/content/res/Resources;

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->warningMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_2

    const-string v1, "warningMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget-object v1, Lcom/squareup/noho/NohoButtonType;->SECONDARY:Lcom/squareup/noho/NohoButtonType;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonType(Lcom/squareup/noho/NohoButtonType;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->resendEmailButton:Landroid/widget/Button;

    if-nez v0, :cond_3

    const-string v1, "resendEmailButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 70
    :cond_3
    new-instance v1, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$attach$1;

    iget-object v2, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$attach$1;-><init>(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    new-instance v2, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$sam$java_lang_Runnable$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    check-cast v1, Landroid/view/View$OnClickListener;

    .line 69
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->cancelVerificationButton:Landroid/widget/Button;

    if-nez v0, :cond_4

    const-string v1, "cancelVerificationButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 73
    :cond_4
    new-instance v1, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$attach$2;

    iget-object v2, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$attach$2;-><init>(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    new-instance v2, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$sam$java_lang_Runnable$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    check-cast v1, Landroid/view/View$OnClickListener;

    .line 72
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->bankAccountSettingsScreenData()Lio/reactivex/Observable;

    move-result-object v0

    .line 77
    new-instance v1, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$attach$3;

    move-object v2, p0

    check-cast v2, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$attach$3;-><init>(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "scopeRunner.bankAccountS\u2026subscribe(::onScreenData)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
