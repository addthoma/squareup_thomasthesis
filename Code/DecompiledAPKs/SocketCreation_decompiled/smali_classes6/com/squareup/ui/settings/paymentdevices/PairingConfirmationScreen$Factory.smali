.class public final Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen$Factory;
.super Ljava/lang/Object;
.source "PairingConfirmationScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPairingConfirmationScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PairingConfirmationScreen.kt\ncom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen$Factory\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,80:1\n52#2:81\n*E\n*S KotlinDebug\n*F\n+ 1 PairingConfirmationScreen.kt\ncom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen$Factory\n*L\n40#1:81\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen$Factory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "()V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "cardreader-ui_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;

    .line 81
    const-class v1, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen$ParentComponent;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen$ParentComponent;

    .line 41
    invoke-interface {v1}, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen$ParentComponent;->pairingPresenter()Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    move-result-object v1

    .line 42
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 43
    sget p1, Lcom/squareup/common/strings/R$string;->ok:I

    new-instance v3, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen$Factory$create$dialog$1;

    invoke-direct {v3, v1}, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen$Factory$create$dialog$1;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)V

    check-cast v3, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, p1, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 46
    sget v2, Lcom/squareup/cardreader/ui/R$string;->pairing_confirmation_wrong_reader:I

    new-instance v3, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen$Factory$create$dialog$2;

    invoke-direct {v3, v1}, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen$Factory$create$dialog$2;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)V

    check-cast v3, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 49
    invoke-static {v0}, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;->access$getTitle$p(Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 50
    invoke-static {v0}, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;->access$getContent$p(Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 51
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setVerticalButtonOrientation()Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 52
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 53
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 55
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(dialog)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
