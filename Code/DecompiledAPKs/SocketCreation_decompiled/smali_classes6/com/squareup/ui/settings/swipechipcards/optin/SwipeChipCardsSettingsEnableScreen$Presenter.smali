.class public Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsCardPresenter;
.source "SwipeChipCardsSettingsEnableScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsCardPresenter<",
        "Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsAnalytics;

.field private final res:Lcom/squareup/util/Res;

.field private final swipeChipCardsSettings:Lcom/squareup/settings/server/SwipeChipCardsSettings;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsAnalytics;Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/settings/server/SwipeChipCardsSettings;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 54
    invoke-direct {p0, p2, p4}, Lcom/squareup/ui/settings/SettingsCardPresenter;-><init>(Lcom/squareup/util/Device;Lflow/Flow;)V

    .line 55
    iput-object p1, p0, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen$Presenter;->analytics:Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsAnalytics;

    .line 56
    iput-object p3, p0, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 57
    iput-object p5, p0, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen$Presenter;->swipeChipCardsSettings:Lcom/squareup/settings/server/SwipeChipCardsSettings;

    return-void
.end method


# virtual methods
.method public enableSwipeChipCards()V
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen$Presenter;->analytics:Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsAnalytics;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsAnalytics;->swipeChipCardsEnabled()V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen$Presenter;->swipeChipCardsSettings:Lcom/squareup/settings/server/SwipeChipCardsSettings;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/settings/server/SwipeChipCardsSettings;->setEnabled(Z)V

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen$Presenter;->screenForAssertion()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->closeCard(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->swipe_chip_cards_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 61
    const-class v0, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen;

    return-object v0
.end method
