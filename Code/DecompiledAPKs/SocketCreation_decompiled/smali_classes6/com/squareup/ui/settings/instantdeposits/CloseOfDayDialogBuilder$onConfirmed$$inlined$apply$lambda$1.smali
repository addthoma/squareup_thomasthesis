.class final Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$onConfirmed$$inlined$apply$lambda$1;
.super Ljava/lang/Object;
.source "CloseOfDayDialogBuilder.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->onConfirmed(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;I)Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "run",
        "com/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$onConfirmed$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $dayOfWeek$inlined:I

.field final synthetic $scopeRunner$inlined:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

.field final synthetic $this_apply:Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;ILcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$onConfirmed$$inlined$apply$lambda$1;->$this_apply:Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;

    iput p2, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$onConfirmed$$inlined$apply$lambda$1;->$dayOfWeek$inlined:I

    iput-object p3, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$onConfirmed$$inlined$apply$lambda$1;->$scopeRunner$inlined:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$onConfirmed$$inlined$apply$lambda$1;->$this_apply:Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;

    invoke-static {v0}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->access$getDialog$p(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$onConfirmed$$inlined$apply$lambda$1;->$this_apply:Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;

    iget v1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$onConfirmed$$inlined$apply$lambda$1;->$dayOfWeek$inlined:I

    invoke-static {v0}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->access$getDayOfWeekPicker$p(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;)Lcom/squareup/noho/NohoNumberPicker;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->access$oneBasedDayOfWeek(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;I)I

    move-result v0

    .line 86
    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$onConfirmed$$inlined$apply$lambda$1;->$this_apply:Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;

    invoke-static {v1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->access$getHourPicker$p(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;)Lcom/squareup/noho/NohoNumberPicker;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v2

    iget-object v3, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$onConfirmed$$inlined$apply$lambda$1;->$this_apply:Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;

    invoke-static {v3}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->access$getAmpmPicker$p(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;)Lcom/squareup/noho/NohoNumberPicker;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->access$twentyFourHourClockHour(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;II)I

    move-result v1

    .line 87
    iget-object v2, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$onConfirmed$$inlined$apply$lambda$1;->$scopeRunner$inlined:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    invoke-virtual {v2, v0, v1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->setCustom(II)V

    return-void
.end method
