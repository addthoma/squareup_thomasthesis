.class public final Lcom/squareup/ui/settings/RealSettingsAppletServices;
.super Ljava/lang/Object;
.source "RealSettingsAppletServices.kt"

# interfaces
.implements Lcom/squareup/ui/settings/SettingsAppletServices;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/ui/settings/RealSettingsAppletServices;",
        "Lcom/squareup/ui/settings/SettingsAppletServices;",
        "cashManagementSectionController",
        "Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;",
        "openTicketsSettingsRunner",
        "Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;",
        "refreshFeesOnEnterScope",
        "Lcom/squareup/ui/settings/taxes/tax/RefreshFeesOnEnterScope;",
        "(Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;Lcom/squareup/ui/settings/taxes/tax/RefreshFeesOnEnterScope;)V",
        "services",
        "",
        "Lmortar/Scoped;",
        "getServices",
        "()Ljava/util/Set;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cashManagementSectionController:Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;

.field private final openTicketsSettingsRunner:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;

.field private final refreshFeesOnEnterScope:Lcom/squareup/ui/settings/taxes/tax/RefreshFeesOnEnterScope;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;Lcom/squareup/ui/settings/taxes/tax/RefreshFeesOnEnterScope;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cashManagementSectionController"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "openTicketsSettingsRunner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "refreshFeesOnEnterScope"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/RealSettingsAppletServices;->cashManagementSectionController:Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;

    iput-object p2, p0, Lcom/squareup/ui/settings/RealSettingsAppletServices;->openTicketsSettingsRunner:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;

    iput-object p3, p0, Lcom/squareup/ui/settings/RealSettingsAppletServices;->refreshFeesOnEnterScope:Lcom/squareup/ui/settings/taxes/tax/RefreshFeesOnEnterScope;

    return-void
.end method


# virtual methods
.method public getServices()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x3

    new-array v0, v0, [Lmortar/Scoped;

    .line 16
    iget-object v1, p0, Lcom/squareup/ui/settings/RealSettingsAppletServices;->cashManagementSectionController:Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;

    check-cast v1, Lmortar/Scoped;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 17
    iget-object v1, p0, Lcom/squareup/ui/settings/RealSettingsAppletServices;->openTicketsSettingsRunner:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;

    check-cast v1, Lmortar/Scoped;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 18
    iget-object v1, p0, Lcom/squareup/ui/settings/RealSettingsAppletServices;->refreshFeesOnEnterScope:Lcom/squareup/ui/settings/taxes/tax/RefreshFeesOnEnterScope;

    check-cast v1, Lmortar/Scoped;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 15
    invoke-static {v0}, Lkotlin/collections/SetsKt;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
