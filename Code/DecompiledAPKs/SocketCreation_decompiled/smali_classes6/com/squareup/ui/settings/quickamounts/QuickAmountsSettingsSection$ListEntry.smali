.class public Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;
.super Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;
.source "QuickAmountsSettingsSection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListEntry"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0016\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u000e\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;",
        "Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;",
        "quickAmountsSettings",
        "Lcom/squareup/quickamounts/QuickAmountsSettings;",
        "section",
        "Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;",
        "res",
        "Lcom/squareup/util/Res;",
        "device",
        "Lcom/squareup/util/Device;",
        "(Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;)V",
        "getValueTextObservable",
        "Lio/reactivex/Observable;",
        "",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;)V
    .locals 7
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "quickAmountsSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "section"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    move-object v2, p2

    check-cast v2, Lcom/squareup/applet/AppletSection;

    sget-object p2, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->CHECKOUT_OPTIONS:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    move-object v3, p2

    check-cast v3, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;

    sget v4, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;->TITLE_ID:I

    move-object v1, p0

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;ILcom/squareup/util/Res;Lcom/squareup/util/Device;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;->quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

    return-void
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;)Lcom/squareup/util/Res;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;->res:Lcom/squareup/util/Res;

    return-object p0
.end method


# virtual methods
.method public getValueTextObservable()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;->quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

    invoke-interface {v0}, Lcom/squareup/quickamounts/QuickAmountsSettings;->amounts()Lio/reactivex/Observable;

    move-result-object v0

    .line 36
    new-instance v1, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry$getValueTextObservable$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry$getValueTextObservable$1;-><init>(Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "quickAmountsSettings.amo\u2026            }\n          }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
