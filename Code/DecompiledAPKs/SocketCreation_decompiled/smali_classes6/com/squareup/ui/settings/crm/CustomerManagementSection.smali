.class public Lcom/squareup/ui/settings/crm/CustomerManagementSection;
.super Lcom/squareup/applet/AppletSection;
.source "CustomerManagementSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/crm/CustomerManagementSection$Access;,
        Lcom/squareup/ui/settings/crm/CustomerManagementSection$ListEntry;
    }
.end annotation


# static fields
.field public static final TITLE_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    sget v0, Lcom/squareup/settingsapplet/R$string;->crm_customer_management_settings_header_label:I

    sput v0, Lcom/squareup/ui/settings/crm/CustomerManagementSection;->TITLE_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/ui/settings/crm/CustomerManagementSection$Access;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/settings/crm/CustomerManagementSection$Access;-><init>(Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/settings/server/Features;)V

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/ui/settings/crm/CustomerManagementSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 30
    sget-object v0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen;

    return-object v0
.end method
