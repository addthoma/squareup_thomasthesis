.class public final Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;
.super Ljava/lang/Object;
.source "CloseOfDayDialogBuilder.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCloseOfDayDialogBuilder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CloseOfDayDialogBuilder.kt\ncom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder\n*L\n1#1,198:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\n\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0006\u0010\u0012\u001a\u00020\u000bJ\u000e\u0010\u0013\u001a\u00020\u00002\u0006\u0010\u0014\u001a\u00020\u0015J\u0019\u0010\u0016\u001a\u00020\u00002\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0018\u00a2\u0006\u0002\u0010\u001aJ\u000e\u0010\u001b\u001a\u00020\u00002\u0006\u0010\u001b\u001a\u00020\u0015J \u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u001b\u001a\u00020\u00152\u0006\u0010\u001e\u001a\u00020\u0015H\u0002J\u0016\u0010\u001f\u001a\u00020\u00002\u0006\u0010 \u001a\u00020!2\u0006\u0010\u0013\u001a\u00020\u0015J\u0010\u0010\"\u001a\u00020\u00152\u0006\u0010#\u001a\u00020\u0015H\u0002J\u0010\u0010$\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0008\u0010%\u001a\u00020\u000fH\u0002J\u0008\u0010&\u001a\u00020\u000fH\u0002J\u0008\u0010\'\u001a\u00020\u000fH\u0002J\u0010\u0010(\u001a\u00020\u00152\u0006\u0010\u001b\u001a\u00020\u0015H\u0002J\u0010\u0010)\u001a\u00020\u00152\u0006\u0010\u001b\u001a\u00020\u0015H\u0002J\u0018\u0010*\u001a\u00020\u00152\u0006\u0010\u001b\u001a\u00020\u00152\u0006\u0010\u001e\u001a\u00020\u0015H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;",
        "",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "ampmPicker",
        "Lcom/squareup/noho/NohoNumberPicker;",
        "confirmButton",
        "Landroid/widget/Button;",
        "dayOfWeekPicker",
        "dialog",
        "Landroid/app/Dialog;",
        "dismissButton",
        "hourPicker",
        "bindViews",
        "",
        "view",
        "Landroid/view/View;",
        "build",
        "dayOfWeek",
        "day",
        "",
        "dayOfWeekRange",
        "daysOfWeek",
        "",
        "",
        "([Ljava/lang/String;)Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;",
        "hour",
        "invalidCloseOfDay",
        "",
        "ampm",
        "onConfirmed",
        "scopeRunner",
        "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;",
        "oneBasedDayOfWeek",
        "zeroBasedDayOfWeek",
        "setPickersMaxValue",
        "setupAmpmPicker",
        "setupDayOfWeekPicker",
        "setupHourPicker",
        "twelveHourClockAmpm",
        "twelveHourClockHour",
        "twentyFourHourClockHour",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private ampmPicker:Lcom/squareup/noho/NohoNumberPicker;

.field private confirmButton:Landroid/widget/Button;

.field private final context:Landroid/content/Context;

.field private dayOfWeekPicker:Lcom/squareup/noho/NohoNumberPicker;

.field private final dialog:Landroid/app/Dialog;

.field private dismissButton:Landroid/widget/Button;

.field private hourPicker:Lcom/squareup/noho/NohoNumberPicker;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->context:Landroid/content/Context;

    .line 32
    new-instance p1, Landroid/app/Dialog;

    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->context:Landroid/content/Context;

    invoke-direct {p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->dialog:Landroid/app/Dialog;

    .line 41
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->dialog:Landroid/app/Dialog;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 43
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->dialog:Landroid/app/Dialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p1

    sget v0, Lcom/squareup/settingsapplet/R$layout;->close_of_day_dialog_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0, p1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->dialog:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    const-string v0, "contentView"

    .line 47
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->bindViews(Landroid/view/View;)V

    .line 49
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->dismissButton:Landroid/widget/Button;

    if-nez p1, :cond_0

    const-string v0, "dismissButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$1;

    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->dialog:Landroid/app/Dialog;

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$1;-><init>(Landroid/app/Dialog;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    new-instance v1, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilderKt$sam$java_lang_Runnable$0;

    invoke-direct {v1, v0}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilderKt$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->setupDayOfWeekPicker()V

    .line 52
    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->setupHourPicker()V

    .line 53
    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->setupAmpmPicker()V

    return-void
.end method

.method public static final synthetic access$getAmpmPicker$p(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;)Lcom/squareup/noho/NohoNumberPicker;
    .locals 1

    .line 31
    iget-object p0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->ampmPicker:Lcom/squareup/noho/NohoNumberPicker;

    if-nez p0, :cond_0

    const-string v0, "ampmPicker"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getDayOfWeekPicker$p(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;)Lcom/squareup/noho/NohoNumberPicker;
    .locals 1

    .line 31
    iget-object p0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->dayOfWeekPicker:Lcom/squareup/noho/NohoNumberPicker;

    if-nez p0, :cond_0

    const-string v0, "dayOfWeekPicker"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getDialog$p(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;)Landroid/app/Dialog;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->dialog:Landroid/app/Dialog;

    return-object p0
.end method

.method public static final synthetic access$getHourPicker$p(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;)Lcom/squareup/noho/NohoNumberPicker;
    .locals 1

    .line 31
    iget-object p0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->hourPicker:Lcom/squareup/noho/NohoNumberPicker;

    if-nez p0, :cond_0

    const-string v0, "hourPicker"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$invalidCloseOfDay(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;III)Z
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->invalidCloseOfDay(III)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$oneBasedDayOfWeek(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;I)I
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->oneBasedDayOfWeek(I)I

    move-result p0

    return p0
.end method

.method public static final synthetic access$setAmpmPicker$p(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;Lcom/squareup/noho/NohoNumberPicker;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->ampmPicker:Lcom/squareup/noho/NohoNumberPicker;

    return-void
.end method

.method public static final synthetic access$setDayOfWeekPicker$p(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;Lcom/squareup/noho/NohoNumberPicker;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->dayOfWeekPicker:Lcom/squareup/noho/NohoNumberPicker;

    return-void
.end method

.method public static final synthetic access$setHourPicker$p(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;Lcom/squareup/noho/NohoNumberPicker;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->hourPicker:Lcom/squareup/noho/NohoNumberPicker;

    return-void
.end method

.method public static final synthetic access$setPickersMaxValue(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;I)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->setPickersMaxValue(I)V

    return-void
.end method

.method public static final synthetic access$twentyFourHourClockHour(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;II)I
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->twentyFourHourClockHour(II)I

    move-result p0

    return p0
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 191
    sget v0, Lcom/squareup/settingsapplet/R$id;->day_of_week_picker:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.day_of_week_picker)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoNumberPicker;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->dayOfWeekPicker:Lcom/squareup/noho/NohoNumberPicker;

    .line 192
    sget v0, Lcom/squareup/settingsapplet/R$id;->hour_picker:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.hour_picker)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoNumberPicker;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->hourPicker:Lcom/squareup/noho/NohoNumberPicker;

    .line 193
    sget v0, Lcom/squareup/settingsapplet/R$id;->am_pm_picker:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.am_pm_picker)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoNumberPicker;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->ampmPicker:Lcom/squareup/noho/NohoNumberPicker;

    .line 194
    sget v0, Lcom/squareup/settingsapplet/R$id;->dismiss_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.dismiss_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->dismissButton:Landroid/widget/Button;

    .line 195
    sget v0, Lcom/squareup/settingsapplet/R$id;->confirm_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "view.findViewById(R.id.confirm_button)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->confirmButton:Landroid/widget/Button;

    return-void
.end method

.method private final invalidCloseOfDay(III)Z
    .locals 0

    if-nez p1, :cond_1

    const/4 p1, 0x5

    if-gez p2, :cond_0

    goto :goto_0

    :cond_0
    if-lt p1, p2, :cond_1

    if-nez p3, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    :goto_1
    return p1
.end method

.method private final oneBasedDayOfWeek(I)I
    .locals 0

    .line 111
    rem-int/lit8 p1, p1, 0x7

    add-int/lit8 p1, p1, 0x1

    return p1
.end method

.method private final setPickersMaxValue(I)V
    .locals 2

    const-string v0, "ampmPicker"

    const-string v1, "hourPicker"

    if-nez p1, :cond_2

    .line 116
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->hourPicker:Lcom/squareup/noho/NohoNumberPicker;

    if-nez p1, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/16 v1, 0xb

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoNumberPicker;->setMaxValue(I)V

    .line 117
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->ampmPicker:Lcom/squareup/noho/NohoNumberPicker;

    if-nez p1, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoNumberPicker;->setMaxValue(I)V

    goto :goto_0

    .line 120
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->hourPicker:Lcom/squareup/noho/NohoNumberPicker;

    if-nez p1, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    const/4 v1, 0x5

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoNumberPicker;->setMaxValue(I)V

    .line 121
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->ampmPicker:Lcom/squareup/noho/NohoNumberPicker;

    if-nez p1, :cond_4

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoNumberPicker;->setMaxValue(I)V

    :goto_0
    return-void
.end method

.method private final setupAmpmPicker()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    .line 177
    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->context:Landroid/content/Context;

    sget v2, Lcom/squareup/registerlib/R$string;->am:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 178
    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->context:Landroid/content/Context;

    sget v3, Lcom/squareup/registerlib/R$string;->pm:I

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    .line 180
    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->ampmPicker:Lcom/squareup/noho/NohoNumberPicker;

    const-string v3, "ampmPicker"

    if-nez v1, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoNumberPicker;->setMinValue(I)V

    .line 181
    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->ampmPicker:Lcom/squareup/noho/NohoNumberPicker;

    if-nez v1, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v2, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$setupAmpmPicker$1;

    invoke-direct {v2, v0}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$setupAmpmPicker$1;-><init>([Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/noho/NohoNumberPicker$Formatter;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoNumberPicker;->setFormatter(Lcom/squareup/noho/NohoNumberPicker$Formatter;)V

    .line 182
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->ampmPicker:Lcom/squareup/noho/NohoNumberPicker;

    if-nez v0, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v1, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$setupAmpmPicker$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$setupAmpmPicker$2;-><init>(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;)V

    check-cast v1, Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setOnValueChangedListener(Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;)V

    return-void
.end method

.method private final setupDayOfWeekPicker()V
    .locals 3

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->dayOfWeekPicker:Lcom/squareup/noho/NohoNumberPicker;

    const-string v1, "dayOfWeekPicker"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoNumberPicker;->setMinValue(I)V

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->dayOfWeekPicker:Lcom/squareup/noho/NohoNumberPicker;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoNumberPicker;->setMaxValue(I)V

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->dayOfWeekPicker:Lcom/squareup/noho/NohoNumberPicker;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v1, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$setupDayOfWeekPicker$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$setupDayOfWeekPicker$1;-><init>(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;)V

    check-cast v1, Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setOnValueChangedListener(Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;)V

    return-void
.end method

.method private final setupHourPicker()V
    .locals 4

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->hourPicker:Lcom/squareup/noho/NohoNumberPicker;

    const-string v1, "hourPicker"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoNumberPicker;->setMinValue(I)V

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->hourPicker:Lcom/squareup/noho/NohoNumberPicker;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget-object v3, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$setupHourPicker$1;->INSTANCE:Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$setupHourPicker$1;

    check-cast v3, Lcom/squareup/noho/NohoNumberPicker$Formatter;

    invoke-virtual {v0, v3}, Lcom/squareup/noho/NohoNumberPicker;->setFormatter(Lcom/squareup/noho/NohoNumberPicker$Formatter;)V

    .line 159
    :try_start_0
    const-class v0, Landroid/widget/NumberPicker;

    const-string v3, "mInputText"

    invoke-virtual {v0, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const-string v3, "NumberPicker::class.java\u2026claredField(\"mInputText\")"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x1

    .line 160
    invoke-virtual {v0, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 161
    iget-object v3, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->hourPicker:Lcom/squareup/noho/NohoNumberPicker;

    if-nez v3, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    check-cast v0, Landroid/widget/EditText;

    new-array v2, v2, [Landroid/text/InputFilter;

    .line 162
    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    goto :goto_0

    .line 161
    :cond_3
    new-instance v0, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type android.widget.EditText"

    invoke-direct {v0, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    .line 164
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 167
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->hourPicker:Lcom/squareup/noho/NohoNumberPicker;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    new-instance v1, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$setupHourPicker$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$setupHourPicker$2;-><init>(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;)V

    check-cast v1, Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setOnValueChangedListener(Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;)V

    return-void
.end method

.method private final twelveHourClockAmpm(I)I
    .locals 0

    .line 102
    div-int/lit8 p1, p1, 0xc

    return p1
.end method

.method private final twelveHourClockHour(I)I
    .locals 0

    .line 100
    rem-int/lit8 p1, p1, 0xc

    return p1
.end method

.method private final twentyFourHourClockHour(II)I
    .locals 1

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    const/16 p2, 0xc

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    add-int/2addr p1, p2

    return p1
.end method


# virtual methods
.method public final build()Landroid/app/Dialog;
    .locals 3

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v1, "dialog.window!!"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/noho/R$dimen;->noho_dialog_width:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const/4 v2, -0x2

    .line 93
    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->dialog:Landroid/app/Dialog;

    return-object v0
.end method

.method public final dayOfWeek(I)Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;
    .locals 3

    .line 64
    move-object v0, p0

    check-cast v0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;

    const/4 v1, 0x1

    if-eqz p1, :cond_1

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    const-string v2, "Day must be either 0 (current day) or 1 (next day)."

    .line 65
    invoke-static {v1, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 69
    iget-object v1, v0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->dayOfWeekPicker:Lcom/squareup/noho/NohoNumberPicker;

    if-nez v1, :cond_2

    const-string v2, "dayOfWeekPicker"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v1, p1}, Lcom/squareup/noho/NohoNumberPicker;->setValue(I)V

    .line 70
    invoke-direct {v0, p1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->setPickersMaxValue(I)V

    return-object v0
.end method

.method public final dayOfWeekRange([Ljava/lang/String;)Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;
    .locals 3

    const-string v0, "daysOfWeek"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    move-object v0, p0

    check-cast v0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;

    .line 58
    array-length v1, p1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "Days of week must only include current day and next day."

    .line 57
    invoke-static {v1, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 61
    iget-object v1, v0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->dayOfWeekPicker:Lcom/squareup/noho/NohoNumberPicker;

    if-nez v1, :cond_1

    const-string v2, "dayOfWeekPicker"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v2, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$dayOfWeekRange$$inlined$apply$lambda$1;

    invoke-direct {v2, p1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$dayOfWeekRange$$inlined$apply$lambda$1;-><init>([Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/noho/NohoNumberPicker$Formatter;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoNumberPicker;->setFormatter(Lcom/squareup/noho/NohoNumberPicker$Formatter;)V

    return-object v0
.end method

.method public final hour(I)Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;
    .locals 3

    .line 73
    move-object v0, p0

    check-cast v0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;

    if-gez p1, :cond_0

    goto :goto_0

    :cond_0
    const/16 v1, 0x17

    if-lt v1, p1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x0

    :goto_1
    const-string v2, "Hour must be between 0 and 23."

    .line 74
    invoke-static {v1, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 75
    iget-object v1, v0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->hourPicker:Lcom/squareup/noho/NohoNumberPicker;

    if-nez v1, :cond_2

    const-string v2, "hourPicker"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-direct {v0, p1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->twelveHourClockHour(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoNumberPicker;->setValue(I)V

    .line 76
    iget-object v1, v0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->ampmPicker:Lcom/squareup/noho/NohoNumberPicker;

    if-nez v1, :cond_3

    const-string v2, "ampmPicker"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-direct {v0, p1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->twelveHourClockAmpm(I)I

    move-result p1

    invoke-virtual {v1, p1}, Lcom/squareup/noho/NohoNumberPicker;->setValue(I)V

    return-object v0
.end method

.method public final onConfirmed(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;I)Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;
    .locals 3

    const-string v0, "scopeRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    move-object v0, p0

    check-cast v0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;

    .line 83
    iget-object v1, v0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->confirmButton:Landroid/widget/Button;

    if-nez v1, :cond_0

    const-string v2, "confirmButton"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v2, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$onConfirmed$$inlined$apply$lambda$1;

    invoke-direct {v2, v0, p2, p1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$onConfirmed$$inlined$apply$lambda$1;-><init>(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;ILcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method
