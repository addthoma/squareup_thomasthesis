.class public Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;
.super Landroid/widget/LinearLayout;
.source "PairingView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView$ParentComponent;
    }
.end annotation


# instance fields
.field private adapter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;

.field cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private failurePopup:Lcom/squareup/flowlegacy/WarningPopup;

.field presenter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressPopup:Lcom/squareup/caller/ProgressPopup;

.field private readerList:Landroidx/recyclerview/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x1

    .line 34
    invoke-virtual {p0, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->setOrientation(I)V

    .line 35
    const-class p2, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView$ParentComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView$ParentComponent;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView$ParentComponent;->inject(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;)V

    return-void
.end method


# virtual methods
.method getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 72
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method notifyDataSetChanged()V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->adapter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 53
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->presenter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->takeView(Ljava/lang/Object;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->presenter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->getProgressPopup()Lcom/squareup/mortar/PopupPresenter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->progressPopup:Lcom/squareup/caller/ProgressPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->presenter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->getPairingFailurePopup()Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->failurePopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 60
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->presenter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->getPairingFailurePopup()Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->failurePopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->presenter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->getProgressPopup()Lcom/squareup/mortar/PopupPresenter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->progressPopup:Lcom/squareup/caller/ProgressPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->presenter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 39
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 40
    sget v0, Lcom/squareup/cardreader/ui/R$id;->pairing_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->readerList:Landroidx/recyclerview/widget/RecyclerView;

    .line 42
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->presenter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    iget-object v3, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->readerList:Landroidx/recyclerview/widget/RecyclerView;

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;Landroid/view/View;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->adapter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;

    .line 43
    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    .line 44
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->setOrientation(I)V

    .line 45
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->readerList:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->readerList:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->adapter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 48
    new-instance v0, Lcom/squareup/caller/ProgressPopup;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/caller/ProgressPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->progressPopup:Lcom/squareup/caller/ProgressPopup;

    .line 49
    new-instance v0, Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/flowlegacy/WarningPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->failurePopup:Lcom/squareup/flowlegacy/WarningPopup;

    return-void
.end method
