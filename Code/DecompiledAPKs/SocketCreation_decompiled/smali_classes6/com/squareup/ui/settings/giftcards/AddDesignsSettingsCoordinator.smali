.class public final Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "AddDesignsSettingsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$ImageItem;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAddDesignsSettingsCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AddDesignsSettingsCoordinator.kt\ncom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 5 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n+ 6 Views.kt\ncom/squareup/util/Views\n+ 7 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,156:1\n49#2:157\n50#2,3:163\n53#2:178\n599#3,4:158\n601#3:162\n310#4,3:166\n313#4,3:175\n35#5,6:169\n1103#6,7:179\n1360#7:186\n1429#7,3:187\n*E\n*S KotlinDebug\n*F\n+ 1 AddDesignsSettingsCoordinator.kt\ncom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator\n*L\n61#1:157\n61#1,3:163\n61#1:178\n61#1,4:158\n61#1:162\n61#1,3:166\n61#1,3:175\n61#1,6:169\n117#1,7:179\n123#1:186\n123#1,3:187\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u00010B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\'\u001a\u00020(2\u0006\u0010%\u001a\u00020&H\u0016J\u0010\u0010)\u001a\u00020(2\u0006\u0010*\u001a\u00020+H\u0002J\u0014\u0010,\u001a\u00020-*\u00020&2\u0006\u0010.\u001a\u00020/H\u0002R\u001b\u0010\t\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\r\u0010\u000e\u001a\u0004\u0008\u000b\u0010\u000cR\u001c\u0010\u000f\u001a\u0010\u0012\u000c\u0012\n \u0012*\u0004\u0018\u00010\u00110\u00110\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0016\u001a\u00020\u00178BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001a\u0010\u000e\u001a\u0004\u0008\u0018\u0010\u0019R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R#\u0010\u001b\u001a\n \u0012*\u0004\u0018\u00010\u001c0\u001c8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001f\u0010\u000e\u001a\u0004\u0008\u001d\u0010\u001eR#\u0010 \u001a\n \u0012*\u0004\u0018\u00010!0!8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008$\u0010\u000e\u001a\u0004\u0008\"\u0010#R\u000e\u0010%\u001a\u00020&X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00061"
    }
    d2 = {
        "Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Runner;",
        "picasso",
        "Lcom/squareup/picasso/Picasso;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Runner;Lcom/squareup/picasso/Picasso;Lcom/squareup/recycler/RecyclerFactory;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "getActionBar",
        "()Lcom/squareup/noho/NohoActionBar;",
        "actionBar$delegate",
        "Lkotlin/Lazy;",
        "itemClicked",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
        "kotlin.jvm.PlatformType",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$ImageItem;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "getRecyclerView",
        "()Landroidx/recyclerview/widget/RecyclerView;",
        "recyclerView$delegate",
        "uploadCustom",
        "Lcom/squareup/noho/NohoButton;",
        "getUploadCustom",
        "()Lcom/squareup/noho/NohoButton;",
        "uploadCustom$delegate",
        "uploadCustomNotAvailable",
        "Lcom/squareup/widgets/MessageView;",
        "getUploadCustomNotAvailable",
        "()Lcom/squareup/widgets/MessageView;",
        "uploadCustomNotAvailable$delegate",
        "view",
        "Landroid/view/View;",
        "attach",
        "",
        "onScreenData",
        "state",
        "Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;",
        "getBorderColor",
        "",
        "selected",
        "",
        "ImageItem",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar$delegate:Lkotlin/Lazy;

.field private final itemClicked:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;"
        }
    .end annotation
.end field

.field private final picasso:Lcom/squareup/picasso/Picasso;

.field private recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$ImageItem;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final recyclerView$delegate:Lkotlin/Lazy;

.field private final runner:Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Runner;

.field private final uploadCustom$delegate:Lkotlin/Lazy;

.field private final uploadCustomNotAvailable$delegate:Lkotlin/Lazy;

.field private view:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Runner;Lcom/squareup/picasso/Picasso;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "picasso"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Runner;

    iput-object p2, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->picasso:Lcom/squareup/picasso/Picasso;

    iput-object p3, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 39
    new-instance p1, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$actionBar$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$actionBar$2;-><init>(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->actionBar$delegate:Lkotlin/Lazy;

    .line 40
    new-instance p1, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$uploadCustom$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$uploadCustom$2;-><init>(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->uploadCustom$delegate:Lkotlin/Lazy;

    .line 43
    new-instance p1, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$uploadCustomNotAvailable$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$uploadCustomNotAvailable$2;-><init>(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->uploadCustomNotAvailable$delegate:Lkotlin/Lazy;

    .line 46
    new-instance p1, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$recyclerView$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$recyclerView$2;-><init>(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->recyclerView$delegate:Lkotlin/Lazy;

    .line 51
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<EGiftTheme>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->itemClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method public static final synthetic access$getBorderColor(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;Landroid/view/View;Z)I
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->getBorderColor(Landroid/view/View;Z)I

    move-result p0

    return p0
.end method

.method public static final synthetic access$getItemClicked$p(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->itemClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$getPicasso$p(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;)Lcom/squareup/picasso/Picasso;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->picasso:Lcom/squareup/picasso/Picasso;

    return-object p0
.end method

.method public static final synthetic access$getRecyclerView$p(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;)Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Runner;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Runner;

    return-object p0
.end method

.method public static final synthetic access$getView$p(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;)Landroid/view/View;
    .locals 1

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->view:Landroid/view/View;

    if-nez p0, :cond_0

    const-string v0, "view"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$onScreenData(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->onScreenData(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;)V

    return-void
.end method

.method public static final synthetic access$setView$p(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;Landroid/view/View;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->view:Landroid/view/View;

    return-void
.end method

.method private final getActionBar()Lcom/squareup/noho/NohoActionBar;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->actionBar$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    return-object v0
.end method

.method private final getBorderColor(Landroid/view/View;Z)I
    .locals 0

    .line 150
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    if-eqz p2, :cond_0

    .line 151
    sget p2, Lcom/squareup/giftcard/R$color;->egiftcard_design_border_color_selected:I

    goto :goto_0

    .line 152
    :cond_0
    sget p2, Lcom/squareup/giftcard/R$color;->egiftcard_design_border_color:I

    .line 150
    :goto_0
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    return p1
.end method

.method private final getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->recyclerView$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getUploadCustom()Lcom/squareup/noho/NohoButton;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->uploadCustom$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    return-object v0
.end method

.method private final getUploadCustomNotAvailable()Lcom/squareup/widgets/MessageView;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->uploadCustomNotAvailable$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    return-object v0
.end method

.method private final onScreenData(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;)V
    .locals 8

    .line 114
    invoke-virtual {p1}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->getUploadEnabled()Z

    move-result v0

    const-string v1, "uploadCustomNotAvailable"

    const-string v2, "uploadCustom"

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v0, :cond_0

    .line 115
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->getUploadCustom()Lcom/squareup/noho/NohoButton;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    invoke-static {v0, v3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 116
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->getUploadCustomNotAvailable()Lcom/squareup/widgets/MessageView;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    invoke-static {v0, v4}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 117
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->getUploadCustom()Lcom/squareup/noho/NohoButton;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 179
    new-instance v1, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$onScreenData$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$onScreenData$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 119
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->getUploadCustom()Lcom/squareup/noho/NohoButton;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    invoke-static {v0, v4}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 120
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->getUploadCustomNotAvailable()Lcom/squareup/widgets/MessageView;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    invoke-static {v0, v3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 123
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->getDisabledThemes()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 186
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 187
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 188
    check-cast v2, Lcom/squareup/protos/client/giftcards/EGiftTheme;

    .line 124
    new-instance v5, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$ImageItem;

    .line 125
    iget-object v6, v2, Lcom/squareup/protos/client/giftcards/EGiftTheme;->image_url:Ljava/lang/String;

    const-string v7, "it.image_url"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    invoke-virtual {p1}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->getThemesToBeAdded()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    .line 124
    invoke-direct {v5, v6, v2, v7}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$ImageItem;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/giftcards/EGiftTheme;Z)V

    .line 128
    invoke-interface {v1, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 189
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->recycler:Lcom/squareup/cycler/Recycler;

    if-nez v0, :cond_2

    const-string v2, "recycler"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-static {v1}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler;->setData(Lcom/squareup/cycler/DataSource;)V

    .line 140
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->getActionBar()Lcom/squareup/noho/NohoActionBar;

    move-result-object v0

    .line 134
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->getActionBar()Lcom/squareup/noho/NohoActionBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 136
    sget-object v2, Lcom/squareup/noho/NohoActionButtonStyle;->PRIMARY:Lcom/squareup/noho/NohoActionButtonStyle;

    .line 137
    new-instance v5, Lcom/squareup/util/ViewString$ResourceString;

    sget v6, Lcom/squareup/settingsapplet/R$string;->giftcards_settings_egiftcard_designs_add_action_button:I

    invoke-direct {v5, v6}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v5, Lcom/squareup/resources/TextModel;

    .line 138
    invoke-virtual {p1}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->getThemesToBeAdded()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_3

    goto :goto_2

    :cond_3
    const/4 v3, 0x0

    .line 139
    :goto_2
    new-instance v4, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$onScreenData$2;

    invoke-direct {v4, p0, p1}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$onScreenData$2;-><init>(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 135
    invoke-virtual {v1, v2, v5, v3, v4}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton(Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 140
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->view:Landroid/view/View;

    .line 59
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->getActionBar()Lcom/squareup/noho/NohoActionBar;

    move-result-object v0

    .line 56
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 57
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/settingsapplet/R$string;->giftcards_settings_egiftcard_designs_add:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 58
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$attach$1;

    invoke-direct {v3, p0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$attach$1;-><init>(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 59
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    .line 157
    sget-object v2, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 158
    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 159
    new-instance v2, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v2}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 163
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 164
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 167
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v3, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$$special$$inlined$row$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 63
    sget v3, Lcom/squareup/settingsapplet/R$layout;->egiftcard_settings_design_image:I

    .line 169
    new-instance v4, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$attach$$inlined$adopt$lambda$1;

    invoke-direct {v4, v3, p0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$attach$$inlined$adopt$lambda$1;-><init>(ILcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 167
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 166
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 86
    sget-object v0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$attach$2$2;->INSTANCE:Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$attach$2$2;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->stableId(Lkotlin/jvm/functions/Function1;)V

    .line 87
    sget-object v0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$attach$2$3;->INSTANCE:Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$attach$2$3;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->compareItemsContent(Lkotlin/jvm/functions/Function2;)V

    .line 161
    invoke-virtual {v2, v1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->recycler:Lcom/squareup/cycler/Recycler;

    .line 90
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    const/4 v1, 0x0

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Runner;->gridColumnCount()Lio/reactivex/Single;

    move-result-object v0

    .line 93
    new-instance v1, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$attach$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$attach$3;-><init>(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Single;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Runner;->addDesignsScreenData()Lio/reactivex/Observable;

    move-result-object v0

    .line 98
    new-instance v1, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$attach$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$attach$4;-><init>(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->itemClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 101
    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Runner;->addDesignsScreenData()Lio/reactivex/Observable;

    move-result-object v1

    check-cast v1, Lio/reactivex/ObservableSource;

    .line 102
    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v2

    .line 100
    invoke-virtual {v0, v1, v2}, Lcom/jakewharton/rxrelay2/PublishRelay;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "itemClicked.withLatestFr\u2026esignsScreenData>()\n    )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    new-instance v1, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$attach$5;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$attach$5;-><init>(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void

    .line 158
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
