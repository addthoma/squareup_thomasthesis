.class public interface abstract Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;
.super Ljava/lang/Object;
.source "WirelessPairingEventListener.java"


# virtual methods
.method public abstract failedPairing(Ljava/lang/String;)V
.end method

.method public abstract startedPairing(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lcom/squareup/cardreader/WirelessConnection;)V
.end method

.method public abstract succeededPairing(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method
