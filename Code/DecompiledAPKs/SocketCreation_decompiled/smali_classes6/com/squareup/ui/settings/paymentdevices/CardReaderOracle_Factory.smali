.class public final Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;
.super Ljava/lang/Object;
.source "CardReaderOracle_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
        ">;"
    }
.end annotation


# instance fields
.field private final branRemoteCardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BranRemoteCardReader;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final storedCardReadersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BranRemoteCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;->firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p3, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p4, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;->branRemoteCardReaderProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p5, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;->storedCardReadersProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p6, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p7, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;->clockProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p8, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BranRemoteCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;)",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;"
        }
    .end annotation

    .line 68
    new-instance v9, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/BranRemoteCardReader;Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/util/Clock;Lrx/Scheduler;)Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;
    .locals 10

    .line 75
    new-instance v9, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;-><init>(Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/BranRemoteCardReader;Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/util/Clock;Lrx/Scheduler;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;
    .locals 9

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/cardreader/CardReaderHub;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;->firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;->branRemoteCardReaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/cardreader/BranRemoteCardReader;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;->storedCardReadersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/util/Clock;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lrx/Scheduler;

    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;->newInstance(Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/BranRemoteCardReader;Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/util/Clock;Lrx/Scheduler;)Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle_Factory;->get()Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    move-result-object v0

    return-object v0
.end method
