.class public abstract Lcom/squareup/ui/settings/SettingsAppletEntryPoint;
.super Lcom/squareup/applet/AppletEntryPoint;
.source "SettingsAppletEntryPoint.java"


# direct methods
.method public varargs constructor <init>(Lcom/squareup/settings/LocalSetting;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/applet/AppletSection;[Lcom/squareup/applet/AppletSection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/applet/AppletSection;",
            "[",
            "Lcom/squareup/applet/AppletSection;",
            ")V"
        }
    .end annotation

    .line 17
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/applet/AppletEntryPoint;-><init>(Lcom/squareup/settings/LocalSetting;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/applet/AppletSection;[Lcom/squareup/applet/AppletSection;)V

    return-void
.end method
