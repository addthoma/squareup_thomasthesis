.class public final Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView_MembersInjector;
.super Ljava/lang/Object;
.source "MerchantProfileView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;",
        ">;"
    }
.end annotation


# instance fields
.field private final fileThreadExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneNumberScrubberProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/InsertingScrubber;",
            ">;"
        }
    .end annotation
.end field

.field private final picassoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final thumborProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pollexor/Thumbor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/InsertingScrubber;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pollexor/Thumbor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView_MembersInjector;->phoneNumberScrubberProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p3, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView_MembersInjector;->thumborProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p4, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView_MembersInjector;->picassoProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p5, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView_MembersInjector;->mainThreadProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p6, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView_MembersInjector;->fileThreadExecutorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/InsertingScrubber;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pollexor/Thumbor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;",
            ">;"
        }
    .end annotation

    .line 52
    new-instance v7, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView_MembersInjector;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static injectFileThreadExecutor(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;Ljava/util/concurrent/Executor;)V
    .locals 0
    .annotation runtime Lcom/squareup/thread/FileThread;
    .end annotation

    .line 94
    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public static injectMainThread(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;Lcom/squareup/thread/executor/MainThread;)V
    .locals 0

    .line 87
    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-void
.end method

.method public static injectPhoneNumberScrubber(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;Lcom/squareup/text/InsertingScrubber;)V
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    return-void
.end method

.method public static injectPicasso(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;Lcom/squareup/picasso/Picasso;)V
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->picasso:Lcom/squareup/picasso/Picasso;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;Ljava/lang/Object;)V
    .locals 0

    .line 66
    check-cast p1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;

    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->presenter:Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;

    return-void
.end method

.method public static injectThumbor(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;Lcom/squareup/pollexor/Thumbor;)V
    .locals 0

    .line 77
    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->thumbor:Lcom/squareup/pollexor/Thumbor;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;)V
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView_MembersInjector;->injectPresenter(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;Ljava/lang/Object;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView_MembersInjector;->phoneNumberScrubberProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/InsertingScrubber;

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView_MembersInjector;->injectPhoneNumberScrubber(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;Lcom/squareup/text/InsertingScrubber;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView_MembersInjector;->thumborProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/pollexor/Thumbor;

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView_MembersInjector;->injectThumbor(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;Lcom/squareup/pollexor/Thumbor;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView_MembersInjector;->picassoProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/picasso/Picasso;

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView_MembersInjector;->injectPicasso(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;Lcom/squareup/picasso/Picasso;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView_MembersInjector;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/executor/MainThread;

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView_MembersInjector;->injectMainThread(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;Lcom/squareup/thread/executor/MainThread;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView_MembersInjector;->fileThreadExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView_MembersInjector;->injectFileThreadExecutor(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView_MembersInjector;->injectMembers(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;)V

    return-void
.end method
