.class public final Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen;
.super Lcom/squareup/ui/settings/printerstations/station/InPrinterStationScope;
.source "HardwarePrinterSelectScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Component;,
        Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;,
        Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$HardwarePrinterSelect;,
        Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\u0008\u0007\u0018\u0000 \u00122\u00020\u00012\u00020\u00022\u00020\u0003:\u0004\u0012\u0013\u0014\u0015B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0014J\u0008\u0010\u0011\u001a\u00020\u0010H\u0016R\u0018\u0010\u0007\u001a\u0006\u0012\u0002\u0008\u00030\u00088VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen;",
        "Lcom/squareup/ui/settings/printerstations/station/InPrinterStationScope;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/container/layer/InSection;",
        "printerStationFlow",
        "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;",
        "(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;)V",
        "section",
        "Ljava/lang/Class;",
        "getSection",
        "()Ljava/lang/Class;",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "screenLayout",
        "Companion",
        "Component",
        "HardwarePrinterSelect",
        "Presenter",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen;->Companion:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Companion;

    .line 264
    sget-object v0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Companion$CREATOR$1;->INSTANCE:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Companion$CREATOR$1;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "fromParcel { parcel ->\n \u2026ader)!!\n        )\n      }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;)V
    .locals 1

    const-string v0, "printerStationFlow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/station/InPrinterStationScope;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;)V

    return-void
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 248
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/settings/printerstations/station/InPrinterStationScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen;->printerStationFlow:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 43
    const-class v0, Lcom/squareup/ui/settings/printerstations/PrinterStationsSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 253
    sget v0, Lcom/squareup/settingsapplet/R$layout;->hardware_printer_select_view:I

    return v0
.end method
