.class Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator$3;
.super Lcom/squareup/padlock/Padlock$OnKeyPressListenerAdapter;
.source "CreateOwnerPasscodeCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->showConfirmationEntryData(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;)V
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator$3;->this$0:Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;

    invoke-direct {p0}, Lcom/squareup/padlock/Padlock$OnKeyPressListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackspaceClicked()V
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator$3;->this$0:Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->access$000(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;)Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onOwnerPasscodeConfirmationBackspaceClicked()V

    return-void
.end method

.method public onDigitClicked(I)V
    .locals 1

    .line 174
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result p1

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator$3;->this$0:Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->access$000(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;)Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onOwnerPasscodeConfirmationDigitEntered(C)V

    return-void
.end method
