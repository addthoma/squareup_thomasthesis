.class public Lcom/squareup/ui/settings/paymentdevices/CardReadersSection;
.super Lcom/squareup/applet/AppletSection;
.source "CardReadersSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$Access;,
        Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$ListEntry;,
        Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$CardReaderHardwareListEntry;
    }
.end annotation


# static fields
.field public static TITLE_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    sget v0, Lcom/squareup/cardreader/ui/R$string;->square_readers:I

    sput v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersSection;->TITLE_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$Access;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$Access;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 31
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen;

    return-object v0
.end method
