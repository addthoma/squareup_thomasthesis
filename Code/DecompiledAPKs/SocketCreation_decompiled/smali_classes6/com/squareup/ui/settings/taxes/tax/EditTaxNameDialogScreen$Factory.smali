.class public Lcom/squareup/ui/settings/taxes/tax/EditTaxNameDialogScreen$Factory;
.super Ljava/lang/Object;
.source "EditTaxNameDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/taxes/tax/EditTaxNameDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 26
    const-class v0, Lcom/squareup/ui/settings/taxes/tax/TaxScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/taxes/tax/TaxScope$Component;

    .line 27
    invoke-interface {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxScope$Component;->taxScopeRunner()Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;

    move-result-object v0

    .line 28
    invoke-static {p1}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/taxes/tax/EditTaxNameDialogScreen;

    .line 30
    invoke-static {v1}, Lcom/squareup/ui/settings/taxes/tax/EditTaxNameDialogScreen;->access$000(Lcom/squareup/ui/settings/taxes/tax/EditTaxNameDialogScreen;)Lcom/squareup/register/widgets/EditTextDialogFactory;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$i27O4z3yO5Jr_gCEgQOC0Ej58TI;

    invoke-direct {v2, v0}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$i27O4z3yO5Jr_gCEgQOC0Ej58TI;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;)V

    const/4 v0, 0x0

    invoke-virtual {v1, p1, v0, v2}, Lcom/squareup/register/widgets/EditTextDialogFactory;->createEditTextDialog(Landroid/content/Context;Lcom/squareup/text/Scrubber;Lcom/squareup/register/widgets/EditTextDialogFactory$EditTextDialogListener;)Landroid/app/AlertDialog;

    move-result-object p1

    .line 29
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
