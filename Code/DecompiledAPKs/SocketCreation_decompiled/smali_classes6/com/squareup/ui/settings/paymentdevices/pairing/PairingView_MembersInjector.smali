.class public final Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView_MembersInjector;
.super Ljava/lang/Object;
.source "PairingView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderMessagesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView_MembersInjector;->cardReaderMessagesProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;",
            ">;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectCardReaderMessages(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;)V
    .locals 0

    .line 45
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;->presenter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;)V
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView_MembersInjector;->injectPresenter(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView_MembersInjector;->cardReaderMessagesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView_MembersInjector;->injectCardReaderMessages(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView_MembersInjector;->injectMembers(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView;)V

    return-void
.end method
