.class public final Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "CardReadersScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Component;,
        Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 43
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen;

    .line 194
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 45
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 49
    const-class v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 197
    sget v0, Lcom/squareup/settingsapplet/R$layout;->card_readers_screen_view:I

    return v0
.end method
