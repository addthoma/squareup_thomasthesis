.class public Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$SwipeChipCardsCheckoutEntry;
.super Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$ListEntry;
.source "SwipeChipCardsSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SwipeChipCardsCheckoutEntry"
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/settings/server/SwipeChipCardsSettings;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 36
    sget-object v5, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->CHECKOUT_OPTIONS:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$ListEntry;-><init>(Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/settings/server/SwipeChipCardsSettings;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;)V

    return-void
.end method
