.class public Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint;
.super Lcom/squareup/ui/settings/SettingsAppletEntryPoint;
.source "PosSettingsAppletEntryPoint.java"


# static fields
.field private static final LAST_SECTION_NAME_KEY:Ljava/lang/String; = "last-setting-applet-section"


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection;Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;Lcom/squareup/ui/settings/taxes/TaxesSection;Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;Lcom/squareup/ui/settings/tipping/TippingSection;Lcom/squareup/ui/settings/cashmanagement/CashManagementSection;Lcom/squareup/ui/settings/offline/OfflineSection;Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection;Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;Lcom/squareup/ui/settings/crm/CustomerManagementSection;Lcom/squareup/ui/settings/crm/EmailCollectionSection;Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection;Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection;Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsSection;Lcom/squareup/ui/settings/tiles/TileAppearanceSection;Lcom/squareup/ui/settings/passcodes/PasscodesSection;Lcom/squareup/ui/settings/timetracking/TimeTrackingSection;Lcom/squareup/ui/settings/paymentdevices/CardReadersSection;Lcom/squareup/ui/settings/printerstations/PrinterStationsSection;Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection;Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection;Lcom/squareup/ui/settings/scales/ScalesSettingsSection;Lcom/squareup/ui/settings/devicename/DeviceSection;Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsSection;Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection;Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;Lcom/squareup/ui/settings/bankaccount/BankAccountSection;Lcom/squareup/ui/settings/instantdeposits/DepositsSection;)V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 98
    new-instance v0, Lcom/squareup/settings/StringLocalSetting;

    const-string v1, "last-setting-applet-section"

    move-object v2, p1

    invoke-direct {v0, p1, v1}, Lcom/squareup/settings/StringLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    const/16 v1, 0x1e

    new-array v1, v1, [Lcom/squareup/applet/AppletSection;

    const/4 v2, 0x0

    aput-object p4, v1, v2

    const/4 v2, 0x1

    aput-object p5, v1, v2

    const/4 v2, 0x2

    aput-object p6, v1, v2

    const/4 v2, 0x3

    aput-object p7, v1, v2

    const/4 v2, 0x4

    aput-object p8, v1, v2

    const/4 v2, 0x5

    aput-object p9, v1, v2

    const/4 v2, 0x6

    aput-object p10, v1, v2

    const/4 v2, 0x7

    aput-object p11, v1, v2

    const/16 v2, 0x8

    aput-object p12, v1, v2

    const/16 v2, 0x9

    aput-object p13, v1, v2

    const/16 v2, 0xa

    aput-object p14, v1, v2

    const/16 v2, 0xb

    aput-object p15, v1, v2

    const/16 v2, 0xc

    aput-object p16, v1, v2

    const/16 v2, 0xd

    aput-object p17, v1, v2

    const/16 v2, 0xe

    aput-object p18, v1, v2

    const/16 v2, 0xf

    aput-object p19, v1, v2

    const/16 v2, 0x10

    aput-object p20, v1, v2

    const/16 v2, 0x11

    aput-object p21, v1, v2

    const/16 v2, 0x12

    aput-object p22, v1, v2

    const/16 v2, 0x13

    aput-object p23, v1, v2

    const/16 v2, 0x14

    aput-object p24, v1, v2

    const/16 v2, 0x15

    aput-object p25, v1, v2

    const/16 v2, 0x16

    aput-object p26, v1, v2

    const/16 v2, 0x17

    aput-object p27, v1, v2

    const/16 v2, 0x18

    aput-object p28, v1, v2

    const/16 v2, 0x19

    aput-object p29, v1, v2

    const/16 v2, 0x1a

    aput-object p30, v1, v2

    const/16 v2, 0x1b

    aput-object p31, v1, v2

    const/16 v2, 0x1c

    aput-object p32, v1, v2

    const/16 v2, 0x1d

    aput-object p33, v1, v2

    move-object v2, p0

    move-object v3, p2

    move-object v4, p3

    invoke-direct {p0, v0, p2, p3, v1}, Lcom/squareup/ui/settings/SettingsAppletEntryPoint;-><init>(Lcom/squareup/settings/LocalSetting;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/applet/AppletSection;[Lcom/squareup/applet/AppletSection;)V

    return-void
.end method
