.class public final Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "SharedSettingsScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/sharedsettings/SharedSettingsAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final coreParametersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/sharedsettings/SharedSettingsAnalytics;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen_Presenter_Factory;->coreParametersProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen_Presenter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/sharedsettings/SharedSettingsAnalytics;",
            ">;)",
            "Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen_Presenter_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen_Presenter_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/ui/settings/sharedsettings/SharedSettingsAnalytics;)Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/ui/settings/sharedsettings/SharedSettingsAnalytics;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen_Presenter_Factory;->coreParametersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;

    iget-object v1, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsAnalytics;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen_Presenter_Factory;->newInstance(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/ui/settings/sharedsettings/SharedSettingsAnalytics;)Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen_Presenter_Factory;->get()Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
