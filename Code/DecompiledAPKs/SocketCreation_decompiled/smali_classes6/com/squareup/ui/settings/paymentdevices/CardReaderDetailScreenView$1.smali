.class Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "CardReaderDetailScreenView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;)V
    .locals 0

    .line 68
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;

    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->presenter:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->identifyReader()V

    return-void
.end method
