.class public Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;
.super Landroid/widget/LinearLayout;
.source "CardReaderDetailScreenView.java"

# interfaces
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;
.implements Lcom/squareup/ui/HasActionBar;


# static fields
.field private static DEFAULT_GLYPH_COLOR_ID:I


# instance fields
.field private acceptsRow:Lcom/squareup/widgets/list/NameValueRow;

.field private batteryGlyph:Landroid/widget/ImageView;

.field private batteryText:Landroid/widget/TextView;

.field private connectionRow:Lcom/squareup/widgets/list/NameValueRow;

.field private firmwareRow:Lcom/squareup/widgets/list/NameValueRow;

.field private forgetButton:Lcom/squareup/ui/ConfirmButton;

.field private helpMessage:Lcom/squareup/widgets/MessageView;

.field private identifyButton:Landroid/widget/Button;

.field private nameRow:Lcom/squareup/ui/XableEditText;

.field private nameViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private serialNumberRow:Lcom/squareup/widgets/list/NameValueRow;

.field private statusAdvice:Lcom/squareup/widgets/MessageView;

.field private statusHeadline:Lcom/squareup/marketfont/MarketTextView;

.field private statusRow:Lcom/squareup/widgets/list/NameValueRow;

.field private wirelessActions:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 39
    sget v0, Lcom/squareup/marin/R$color;->marin_text_selector_list:I

    sput v0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->DEFAULT_GLYPH_COLOR_ID:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 60
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->nameViews:Ljava/util/List;

    .line 61
    const-class p2, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Component;->inject(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;)V

    return-void
.end method

.method private bindViews()V
    .locals 2

    .line 196
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_status_headline:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->statusHeadline:Lcom/squareup/marketfont/MarketTextView;

    .line 197
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_status_advice:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->statusAdvice:Lcom/squareup/widgets/MessageView;

    .line 198
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_detail_name_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->nameRow:Lcom/squareup/ui/XableEditText;

    .line 199
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_status_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/NameValueRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->statusRow:Lcom/squareup/widgets/list/NameValueRow;

    .line 200
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_battery_glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->batteryGlyph:Landroid/widget/ImageView;

    .line 201
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_battery_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->batteryText:Landroid/widget/TextView;

    .line 202
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_connection_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/NameValueRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->connectionRow:Lcom/squareup/widgets/list/NameValueRow;

    .line 203
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_accepts_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/NameValueRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->acceptsRow:Lcom/squareup/widgets/list/NameValueRow;

    .line 204
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_firmware_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/NameValueRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->firmwareRow:Lcom/squareup/widgets/list/NameValueRow;

    .line 205
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_serial_number_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/NameValueRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->serialNumberRow:Lcom/squareup/widgets/list/NameValueRow;

    .line 206
    sget v0, Lcom/squareup/cardreader/ui/R$id;->card_reader_details_help_message_row:I

    .line 207
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->helpMessage:Lcom/squareup/widgets/MessageView;

    .line 208
    sget v0, Lcom/squareup/cardreader/ui/R$id;->ble_actions:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->wirelessActions:Landroid/view/ViewGroup;

    .line 209
    sget v0, Lcom/squareup/cardreader/ui/R$id;->identify_reader_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->identifyButton:Landroid/widget/Button;

    .line 210
    sget v0, Lcom/squareup/cardreader/ui/R$id;->forget_reader_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ConfirmButton;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->forgetButton:Lcom/squareup/ui/ConfirmButton;

    .line 211
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->nameViews:Ljava/util/List;

    sget v1, Lcom/squareup/cardreader/ui/R$id;->reader_detail_name_row_title:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->nameViews:Ljava/util/List;

    sget v1, Lcom/squareup/cardreader/ui/R$id;->reader_detail_name_row:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->nameViews:Ljava/util/List;

    sget v1, Lcom/squareup/cardreader/ui/R$id;->reader_detail_name_row_helper:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->nameViews:Ljava/util/List;

    sget v1, Lcom/squareup/cardreader/ui/R$id;->reader_detail_name_row_border:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private getBatteryGlyph(Lcom/squareup/cardreader/BatteryLevel;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .line 172
    sget v0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->DEFAULT_GLYPH_COLOR_ID:I

    .line 173
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 172
    invoke-static {p1, v0, v1}, Lcom/squareup/cardreader/BatteryLevelResources;->buildBatteryDrawableNormal(Lcom/squareup/cardreader/BatteryLevel;ILandroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1
.end method

.method private setBatteryGlyph(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->batteryGlyph:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->batteryText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 185
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->batteryGlyph:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private setBatteryGlyphAndText(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;)V
    .locals 2

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->batteryGlyph:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->batteryText:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->batteryGlyph:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 192
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->batteryText:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private setBatteryText(Ljava/lang/CharSequence;)V
    .locals 2

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->batteryGlyph:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->batteryText:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->batteryText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public blankBatteryLevel(Ljava/lang/String;)V
    .locals 0

    .line 152
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->setBatteryText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 168
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public getNickName()Ljava/lang/String;
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->nameRow:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 164
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public hideStatusSection()V
    .locals 2

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->statusHeadline:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->statusAdvice:Lcom/squareup/widgets/MessageView;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$CardReaderDetailScreenView()V
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->presenter:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->forgetReader()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->presenter:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 79
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 65
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 66
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->bindViews()V

    .line 67
    sget v0, Lcom/squareup/cardreader/ui/R$id;->identify_reader_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView$1;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;)V

    .line 68
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->forgetButton:Lcom/squareup/ui/ConfirmButton;

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailScreenView$OkBBr4ArkgEJHw6nhyBSArYINEQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailScreenView$OkBBr4ArkgEJHw6nhyBSArYINEQ;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->presenter:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public setAcceptsValue(Ljava/lang/CharSequence;)V
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->acceptsRow:Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setBatteryLevel(Lcom/squareup/cardreader/BatteryLevel;)V
    .locals 0

    .line 144
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->getBatteryGlyph(Lcom/squareup/cardreader/BatteryLevel;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->setBatteryGlyph(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setBatteryLevel(Lcom/squareup/cardreader/BatteryLevel;Ljava/lang/CharSequence;)V
    .locals 0

    .line 148
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->getBatteryGlyph(Lcom/squareup/cardreader/BatteryLevel;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->setBatteryGlyphAndText(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setConnectionValue(Ljava/lang/CharSequence;)V
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->connectionRow:Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setFirmwareVersion(Ljava/lang/String;)V
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->firmwareRow:Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setHelpMessage(Ljava/lang/CharSequence;)V
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->helpMessage:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setIdentifyButtonVisibility(Z)V
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->identifyButton:Landroid/widget/Button;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->nameRow:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setNickNameEnabled(Z)V
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->nameRow:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setEnabled(Z)V

    return-void
.end method

.method public setNickNameSectionVisible(Z)V
    .locals 2

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->nameViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 111
    invoke-static {v1, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setSerialNumber(Ljava/lang/String;)V
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->serialNumberRow:Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setStatusValue(Ljava/lang/CharSequence;)V
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->statusRow:Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showStatusSection(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->statusHeadline:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->statusHeadline:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    invoke-virtual {p0, p2}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->showStatusSectionAdvice(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showStatusSectionAdvice(Ljava/lang/CharSequence;)V
    .locals 2

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->statusAdvice:Lcom/squareup/widgets/MessageView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->statusAdvice:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showWirelessActions()V
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreenView;->wirelessActions:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method
