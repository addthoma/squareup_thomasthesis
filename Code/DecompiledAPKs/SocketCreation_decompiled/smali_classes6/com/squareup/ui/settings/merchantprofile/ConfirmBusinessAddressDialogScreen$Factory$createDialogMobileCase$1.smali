.class final Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Factory$createDialogMobileCase$1;
.super Ljava/lang/Object;
.source "ConfirmBusinessAddressDialogScreen.kt"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Factory;->createDialogMobileCase(Landroid/content/Context;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;Lcom/squareup/analytics/Analytics;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "dialog",
        "Landroid/content/DialogInterface;",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 1>",
        "",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $analytics:Lcom/squareup/analytics/Analytics;

.field final synthetic $runner:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;Lcom/squareup/analytics/Analytics;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Factory$createDialogMobileCase$1;->$runner:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;

    iput-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Factory$createDialogMobileCase$1;->$analytics:Lcom/squareup/analytics/Analytics;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 111
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 112
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Factory$createDialogMobileCase$1;->$runner:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->mobileBusinessConfirmedButtonClicked()V

    .line 113
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Factory$createDialogMobileCase$1;->$analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/ui/settings/merchantprofile/SettingsBusinessAddressAnalytics;->logMobileIsCorrect(Lcom/squareup/analytics/Analytics;)V

    return-void
.end method
