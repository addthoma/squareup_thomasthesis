.class public Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkR12PairingScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "ReaderSdkR12PairingScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardOverSheetScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkR12PairingScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkR12PairingScreen$Component;,
        Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkR12PairingScreen$ParentComponent;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkR12PairingScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkR12PairingScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkR12PairingScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkR12PairingScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkR12PairingScreen;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkR12PairingScreen;

    .line 34
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkR12PairingScreen;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkR12PairingScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkR12PairingScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public screenLayout()I
    .locals 1

    .line 37
    sget v0, Lcom/squareup/cardreader/ui/R$layout;->pairing_view:I

    return v0
.end method
