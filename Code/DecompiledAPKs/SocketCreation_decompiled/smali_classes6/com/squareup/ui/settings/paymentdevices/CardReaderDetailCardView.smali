.class public Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;
.super Lcom/squareup/widgets/PairLayout;
.source "CardReaderDetailCardView.java"

# interfaces
.implements Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;


# static fields
.field private static DEFAULT_GLYPH_COLOR_ID:I


# instance fields
.field private acceptsRow:Lcom/squareup/widgets/list/NameValueRow;

.field private actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

.field private batteryGlyph:Landroid/widget/ImageView;

.field private batteryText:Landroid/widget/TextView;

.field private bleActions:Landroid/view/ViewGroup;

.field private connectionRow:Lcom/squareup/widgets/list/NameValueRow;

.field private firmwareRow:Lcom/squareup/widgets/list/NameValueRow;

.field private forgetButton:Lcom/squareup/ui/ConfirmButton;

.field private helpMessage:Lcom/squareup/widgets/MessageView;

.field private identifyButton:Landroid/widget/Button;

.field private nameRow:Lcom/squareup/ui/XableEditText;

.field private final nameViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private serialNumberRow:Lcom/squareup/widgets/list/NameValueRow;

.field private statusAdvice:Lcom/squareup/widgets/MessageView;

.field private statusHeadline:Landroid/widget/TextView;

.field private statusRow:Lcom/squareup/widgets/list/NameValueRow;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 33
    sget v0, Lcom/squareup/marin/R$color;->marin_text_selector_list:I

    sput v0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->DEFAULT_GLYPH_COLOR_ID:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/PairLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->nameViews:Ljava/util/List;

    .line 56
    const-class p2, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Component;->inject(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;)V

    return-void
.end method

.method private bindViews()V
    .locals 2

    .line 186
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    .line 187
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_status_headline:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->statusHeadline:Landroid/widget/TextView;

    .line 188
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_status_advice:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->statusAdvice:Lcom/squareup/widgets/MessageView;

    .line 189
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_detail_name_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->nameRow:Lcom/squareup/ui/XableEditText;

    .line 190
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_status_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/NameValueRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->statusRow:Lcom/squareup/widgets/list/NameValueRow;

    .line 191
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_battery_glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->batteryGlyph:Landroid/widget/ImageView;

    .line 192
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_battery_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->batteryText:Landroid/widget/TextView;

    .line 193
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_connection_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/NameValueRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->connectionRow:Lcom/squareup/widgets/list/NameValueRow;

    .line 194
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_accepts_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/NameValueRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->acceptsRow:Lcom/squareup/widgets/list/NameValueRow;

    .line 195
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_firmware_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/NameValueRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->firmwareRow:Lcom/squareup/widgets/list/NameValueRow;

    .line 196
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_serial_number_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/NameValueRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->serialNumberRow:Lcom/squareup/widgets/list/NameValueRow;

    .line 197
    sget v0, Lcom/squareup/cardreader/ui/R$id;->card_reader_details_help_message_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->helpMessage:Lcom/squareup/widgets/MessageView;

    .line 198
    sget v0, Lcom/squareup/cardreader/ui/R$id;->ble_actions:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->bleActions:Landroid/view/ViewGroup;

    .line 199
    sget v0, Lcom/squareup/cardreader/ui/R$id;->identify_reader_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->identifyButton:Landroid/widget/Button;

    .line 200
    sget v0, Lcom/squareup/cardreader/ui/R$id;->forget_reader_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ConfirmButton;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->forgetButton:Lcom/squareup/ui/ConfirmButton;

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->nameViews:Ljava/util/List;

    sget v1, Lcom/squareup/cardreader/ui/R$id;->reader_detail_name_row_title:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->nameViews:Ljava/util/List;

    sget v1, Lcom/squareup/cardreader/ui/R$id;->reader_detail_name_row:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->nameViews:Ljava/util/List;

    sget v1, Lcom/squareup/cardreader/ui/R$id;->reader_detail_name_row_helper:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->nameViews:Ljava/util/List;

    sget v1, Lcom/squareup/cardreader/ui/R$id;->reader_detail_name_row_border:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private getBatteryGlyph(Lcom/squareup/cardreader/BatteryLevel;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .line 162
    sget v0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->DEFAULT_GLYPH_COLOR_ID:I

    .line 163
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 162
    invoke-static {p1, v0, v1}, Lcom/squareup/cardreader/BatteryLevelResources;->buildBatteryDrawableNormal(Lcom/squareup/cardreader/BatteryLevel;ILandroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1
.end method

.method private setBatteryGlyph(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .line 173
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->batteryGlyph:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->batteryText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->batteryGlyph:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private setBatteryGlyphAndText(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;)V
    .locals 2

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->batteryGlyph:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->batteryText:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->batteryGlyph:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 182
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->batteryText:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private setBatteryText(Ljava/lang/CharSequence;)V
    .locals 2

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->batteryGlyph:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->batteryText:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->batteryText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public blankBatteryLevel(Ljava/lang/String;)V
    .locals 0

    .line 150
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->setBatteryText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public getNickName()Ljava/lang/String;
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->nameRow:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hideStatusSection()V
    .locals 2

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->statusHeadline:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->statusAdvice:Lcom/squareup/widgets/MessageView;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$0$CardReaderDetailCardView()V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->presenter:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->forgetReader()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 60
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onAttachedToWindow()V

    .line 61
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->bindViews()V

    .line 62
    sget v0, Lcom/squareup/cardreader/ui/R$id;->identify_reader_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView$1;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;)V

    .line 63
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->forgetButton:Lcom/squareup/ui/ConfirmButton;

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailCardView$07AkO06ggJfWUUvTUY2MFyVRN4c;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderDetailCardView$07AkO06ggJfWUUvTUY2MFyVRN4c;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->presenter:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->presenter:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 74
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public setAcceptsValue(Ljava/lang/CharSequence;)V
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->acceptsRow:Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setBatteryLevel(Lcom/squareup/cardreader/BatteryLevel;)V
    .locals 0

    .line 142
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->getBatteryGlyph(Lcom/squareup/cardreader/BatteryLevel;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->setBatteryGlyph(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setBatteryLevel(Lcom/squareup/cardreader/BatteryLevel;Ljava/lang/CharSequence;)V
    .locals 0

    .line 146
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->getBatteryGlyph(Lcom/squareup/cardreader/BatteryLevel;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->setBatteryGlyphAndText(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setConnectionValue(Ljava/lang/CharSequence;)V
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->connectionRow:Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setFirmwareVersion(Ljava/lang/String;)V
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->firmwareRow:Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setHelpMessage(Ljava/lang/CharSequence;)V
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->helpMessage:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setIdentifyButtonVisibility(Z)V
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->identifyButton:Landroid/widget/Button;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->nameRow:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setNickNameEnabled(Z)V
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->nameRow:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setEnabled(Z)V

    return-void
.end method

.method public setNickNameSectionVisible(Z)V
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->nameViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 109
    invoke-static {v1, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setSerialNumber(Ljava/lang/String;)V
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->serialNumberRow:Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setStatusValue(Ljava/lang/CharSequence;)V
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->statusRow:Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showStatusSection(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 2

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->statusHeadline:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->statusHeadline:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    invoke-virtual {p0, p2}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->showStatusSectionAdvice(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showStatusSectionAdvice(Ljava/lang/CharSequence;)V
    .locals 2

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->statusAdvice:Lcom/squareup/widgets/MessageView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->statusAdvice:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showWirelessActions()V
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;->bleActions:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method
