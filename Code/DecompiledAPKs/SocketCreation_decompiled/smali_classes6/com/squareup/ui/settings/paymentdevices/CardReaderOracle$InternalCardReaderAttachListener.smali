.class Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalCardReaderAttachListener;
.super Ljava/lang/Object;
.source "CardReaderOracle.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalCardReaderAttachListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)V
    .locals 0

    .line 335
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalCardReaderAttachListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$1;)V
    .locals 0

    .line 335
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalCardReaderAttachListener;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)V

    return-void
.end method


# virtual methods
.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    return-void
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 3

    .line 342
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 345
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 346
    new-instance v1, Ljava/util/LinkedHashMap;

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalCardReaderAttachListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    .line 347
    invoke-static {v2}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->access$300(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    invoke-direct {v1, v2}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 348
    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 349
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalCardReaderAttachListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-static {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->access$300(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 352
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalCardReaderAttachListener;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->stopEventOverrideForReader(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method
