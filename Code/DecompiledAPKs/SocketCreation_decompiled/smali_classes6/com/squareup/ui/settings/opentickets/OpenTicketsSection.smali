.class public Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;
.super Lcom/squareup/applet/AppletSection;
.source "OpenTicketsSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/opentickets/OpenTicketsSection$Access;,
        Lcom/squareup/ui/settings/opentickets/OpenTicketsSection$ListEntry;
    }
.end annotation


# static fields
.field public static TITLE_ID:I


# instance fields
.field private final employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    sget v0, Lcom/squareup/registerlib/R$string;->open_tickets:I

    sput v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;->TITLE_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection$Access;

    invoke-direct {v0, p1, p3}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection$Access;-><init>(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/settings/server/Features;)V

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    .line 30
    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 34
    sget-object v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsScreen;

    return-object v0
.end method

.method public isRestricted()Z
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v0

    .line 44
    sget-object v1, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->EMPLOYEE_LOGIN:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    return v0

    .line 47
    :cond_0
    invoke-super {p0}, Lcom/squareup/applet/AppletSection;->isRestricted()Z

    move-result v0

    return v0
.end method
