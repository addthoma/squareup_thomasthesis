.class final enum Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;
.super Ljava/lang/Enum;
.source "OrderHubSettingsEvents.kt"

# interfaces
.implements Lcom/squareup/analytics/EventNamedAction;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;",
        ">;",
        "Lcom/squareup/analytics/EventNamedAction;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0007\u0008\u0082\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002B\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u0006\u001a\u00020\u0004H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;",
        "",
        "Lcom/squareup/analytics/EventNamedAction;",
        "value",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getName",
        "ORDER_HUB_CHANGED_NOTIFICATION_SETTINGS_ENABLED",
        "ORDER_HUB_CHANGED_NOTIFICATION_SETTINGS_FREQUENCY",
        "ORDER_HUB_CHANGED_PRINTING_SETTINGS",
        "ORDER_HUB_CHANGED_QUICK_ACTIONS_SETTINGS",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;

.field public static final enum ORDER_HUB_CHANGED_NOTIFICATION_SETTINGS_ENABLED:Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;

.field public static final enum ORDER_HUB_CHANGED_NOTIFICATION_SETTINGS_FREQUENCY:Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;

.field public static final enum ORDER_HUB_CHANGED_PRINTING_SETTINGS:Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;

.field public static final enum ORDER_HUB_CHANGED_QUICK_ACTIONS_SETTINGS:Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;

    new-instance v1, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;

    const/4 v2, 0x0

    const-string v3, "ORDER_HUB_CHANGED_NOTIFICATION_SETTINGS_ENABLED"

    const-string v4, "Order Hub: Changed Notification Settings Enabled"

    .line 13
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;->ORDER_HUB_CHANGED_NOTIFICATION_SETTINGS_ENABLED:Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;

    const/4 v2, 0x1

    const-string v3, "ORDER_HUB_CHANGED_NOTIFICATION_SETTINGS_FREQUENCY"

    const-string v4, "Order Hub: Changed Notification Settings Frequency"

    .line 16
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;->ORDER_HUB_CHANGED_NOTIFICATION_SETTINGS_FREQUENCY:Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;

    const/4 v2, 0x2

    const-string v3, "ORDER_HUB_CHANGED_PRINTING_SETTINGS"

    const-string v4, "Order Hub: Changed Printing Settings"

    .line 19
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;->ORDER_HUB_CHANGED_PRINTING_SETTINGS:Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;

    const/4 v2, 0x3

    const-string v3, "ORDER_HUB_CHANGED_QUICK_ACTIONS_SETTINGS"

    const-string v4, "Order Hub: Changed Quick Actions Settings"

    .line 20
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;->ORDER_HUB_CHANGED_QUICK_ACTIONS_SETTINGS:Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;->$VALUES:[Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;
    .locals 1

    const-class v0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;
    .locals 1

    sget-object v0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;->$VALUES:[Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;

    invoke-virtual {v0}, [Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsEvents;->value:Ljava/lang/String;

    return-object v0
.end method
