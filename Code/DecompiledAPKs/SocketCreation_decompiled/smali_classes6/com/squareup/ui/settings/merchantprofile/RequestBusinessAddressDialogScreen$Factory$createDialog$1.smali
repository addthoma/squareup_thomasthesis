.class final Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Factory$createDialog$1;
.super Ljava/lang/Object;
.source "RequestBusinessAddressDialogScreen.kt"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Factory;->createDialog(Landroid/content/Context;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$ScreenData;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "dialog",
        "Landroid/content/DialogInterface;",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 1>",
        "",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $data:Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$ScreenData;

.field final synthetic $runner:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$ScreenData;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Factory$createDialog$1;->$runner:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;

    iput-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Factory$createDialog$1;->$data:Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$ScreenData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 75
    iget-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Factory$createDialog$1;->$runner:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;

    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Factory$createDialog$1;->$data:Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$ScreenData;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$ScreenData;->getDialogType()Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Type;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->onVerifyBusinessAddressDialogContinue(Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Type;)V

    .line 76
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method
