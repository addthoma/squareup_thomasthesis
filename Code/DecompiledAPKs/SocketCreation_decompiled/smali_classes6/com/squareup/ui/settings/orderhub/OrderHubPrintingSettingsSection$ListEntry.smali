.class public Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$ListEntry;
.super Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;
.source "OrderHubPrintingSettingsSection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListEntry"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\u0008\u0016\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0008\u0010\u000b\u001a\u00020\u000cH\u0014R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$ListEntry;",
        "Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;",
        "section",
        "Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;",
        "res",
        "Lcom/squareup/util/Res;",
        "device",
        "Lcom/squareup/util/Device;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/analytics/Analytics;)V",
        "logClickEvent",
        "",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/analytics/Analytics;)V
    .locals 7
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "section"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    move-object v2, p1

    check-cast v2, Lcom/squareup/applet/AppletSection;

    sget-object p1, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->ORDERHUB:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    move-object v3, p1

    check-cast v3, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;

    sget v4, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;->TITLE_ID:I

    move-object v1, p0

    move-object v5, p2

    move-object v6, p3

    .line 31
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;ILcom/squareup/util/Res;Lcom/squareup/util/Device;)V

    iput-object p4, p0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$ListEntry;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method protected logClickEvent()V
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$ListEntry;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Settings Order Hub Printing"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method
