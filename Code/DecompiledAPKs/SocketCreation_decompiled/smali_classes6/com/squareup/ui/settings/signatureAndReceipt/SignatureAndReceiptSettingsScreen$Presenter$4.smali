.class Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter$4;
.super Lcom/squareup/mortar/PopupPresenter;
.source "SignatureAndReceiptSettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SidebarRefresher;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/register/widgets/Confirmation;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

.field final synthetic val$analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;Lcom/squareup/analytics/Analytics;)V
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter$4;->this$0:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    iput-object p2, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter$4;->val$analytics:Lcom/squareup/analytics/Analytics;

    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected onPopupResult(Ljava/lang/Boolean;)V
    .locals 2

    .line 139
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 140
    iget-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter$4;->this$0:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    invoke-static {p1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->access$600(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setAlwaysSkipSignatureChecked()V

    .line 141
    iget-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter$4;->this$0:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->saveSettings()V

    .line 142
    iget-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter$4;->this$0:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    invoke-static {p1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->access$400(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;)V

    .line 143
    iget-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter$4;->val$analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent;

    sget-object v1, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;->ALWAYS_SKIP_SIGNATURE:Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent;-><init>(Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;)V

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 137
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter$4;->onPopupResult(Ljava/lang/Boolean;)V

    return-void
.end method
