.class final Lcom/squareup/ui/settings/SettingsSectionPresenter$onEnterScope$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SettingsSectionPresenter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/SettingsSectionPresenter;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u0001\"\u000c\u0008\u0000\u0010\u0002*\u00020\u0003*\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0001H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "T",
        "Landroid/view/ViewGroup;",
        "Lcom/squareup/ui/HasActionBar;",
        "it",
        "invoke",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/SettingsSectionPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsSectionPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/SettingsSectionPresenter$onEnterScope$1;->this$0:Lcom/squareup/ui/settings/SettingsSectionPresenter;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter$onEnterScope$1;->invoke(Lkotlin/Unit;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Unit;)V
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsSectionPresenter$onEnterScope$1;->this$0:Lcom/squareup/ui/settings/SettingsSectionPresenter;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->access$setLoggingOut$p(Lcom/squareup/ui/settings/SettingsSectionPresenter;Z)V

    .line 67
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsSectionPresenter$onEnterScope$1;->this$0:Lcom/squareup/ui/settings/SettingsSectionPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->saveSettings()V

    return-void
.end method
