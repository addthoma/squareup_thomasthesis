.class public final Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;
.super Ljava/lang/Object;
.source "PosSettingsAppletEntryPoint_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint;",
        ">;"
    }
.end annotation


# instance fields
.field private final bankAccountSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/bankaccount/BankAccountSection;",
            ">;"
        }
    .end annotation
.end field

.field private final barcodeScannersSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReadersSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReadersSection;",
            ">;"
        }
    .end annotation
.end field

.field private final cashDrawerSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection;",
            ">;"
        }
    .end annotation
.end field

.field private final cashManagementSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/cashmanagement/CashManagementSection;",
            ">;"
        }
    .end annotation
.end field

.field private final customerManagementSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/crm/CustomerManagementSection;",
            ">;"
        }
    .end annotation
.end field

.field private final depositsSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositsSection;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/devicename/DeviceSection;",
            ">;"
        }
    .end annotation
.end field

.field private final emailCollectionSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/crm/EmailCollectionSection;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection;",
            ">;"
        }
    .end annotation
.end field

.field private final gatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardsSettingsSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltySectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/offline/OfflineSection;",
            ">;"
        }
    .end annotation
.end field

.field private final onlineCheckoutSettingsSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsSection;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubNotificationsSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsSection;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubPrintingSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubQuickActionSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodesSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/passcodes/PasscodesSection;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentTypesSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;",
            ">;"
        }
    .end annotation
.end field

.field private final preferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/PrinterStationsSection;",
            ">;"
        }
    .end annotation
.end field

.field private final publicProfileSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;",
            ">;"
        }
    .end annotation
.end field

.field private final quickAmountsSettingsSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;",
            ">;"
        }
    .end annotation
.end field

.field private final scalesSettingsSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/scales/ScalesSettingsSection;",
            ">;"
        }
    .end annotation
.end field

.field private final sharedSettingsSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection;",
            ">;"
        }
    .end annotation
.end field

.field private final signatureAndReceiptSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;",
            ">;"
        }
    .end annotation
.end field

.field private final swipeChipCardsSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;",
            ">;"
        }
    .end annotation
.end field

.field private final taxesSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/TaxesSection;",
            ">;"
        }
    .end annotation
.end field

.field private final tileAppearanceSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSection;",
            ">;"
        }
    .end annotation
.end field

.field private final timeTrackingSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/timetracking/TimeTrackingSection;",
            ">;"
        }
    .end annotation
.end field

.field private final tippingSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tipping/TippingSection;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/TaxesSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tipping/TippingSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/cashmanagement/CashManagementSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/offline/OfflineSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/crm/CustomerManagementSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/crm/EmailCollectionSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/passcodes/PasscodesSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/timetracking/TimeTrackingSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReadersSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/PrinterStationsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/scales/ScalesSettingsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/devicename/DeviceSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/bankaccount/BankAccountSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositsSection;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 148
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->preferencesProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 149
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->gatekeeperProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 150
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->sharedSettingsSectionProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 151
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->paymentTypesSectionProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 152
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->taxesSectionProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 153
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->signatureAndReceiptSectionProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 154
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->quickAmountsSettingsSectionProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 155
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->tippingSectionProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 156
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->cashManagementSectionProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 157
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->offlineSectionProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 158
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->employeeManagementSectionProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 159
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->openTicketsSectionProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 160
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->swipeChipCardsSectionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 161
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->customerManagementSectionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 162
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->emailCollectionSectionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 163
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->loyaltySectionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 164
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->giftCardsSettingsSectionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 165
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->onlineCheckoutSettingsSectionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 166
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->tileAppearanceSectionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 167
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->passcodesSectionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 168
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->timeTrackingSectionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 169
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->cardReadersSectionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 170
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->printerStationsSectionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 171
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->cashDrawerSectionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 172
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->barcodeScannersSectionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 173
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->scalesSettingsSectionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p27

    .line 174
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->deviceSectionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p28

    .line 175
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->orderHubNotificationsSectionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p29

    .line 176
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->orderHubPrintingSectionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p30

    .line 177
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->orderHubQuickActionSectionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p31

    .line 178
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->publicProfileSectionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p32

    .line 179
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->bankAccountSectionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p33

    .line 180
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->depositsSectionProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;
    .locals 35
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/TaxesSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tipping/TippingSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/cashmanagement/CashManagementSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/offline/OfflineSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/crm/CustomerManagementSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/crm/EmailCollectionSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/passcodes/PasscodesSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/timetracking/TimeTrackingSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReadersSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/PrinterStationsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/scales/ScalesSettingsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/devicename/DeviceSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/bankaccount/BankAccountSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositsSection;",
            ">;)",
            "Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    move-object/from16 v32, p31

    move-object/from16 v33, p32

    .line 222
    new-instance v34, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;

    move-object/from16 v0, v34

    invoke-direct/range {v0 .. v33}, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v34
.end method

.method public static newInstance(Landroid/content/SharedPreferences;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection;Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;Lcom/squareup/ui/settings/taxes/TaxesSection;Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;Lcom/squareup/ui/settings/tipping/TippingSection;Lcom/squareup/ui/settings/cashmanagement/CashManagementSection;Lcom/squareup/ui/settings/offline/OfflineSection;Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection;Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;Lcom/squareup/ui/settings/crm/CustomerManagementSection;Lcom/squareup/ui/settings/crm/EmailCollectionSection;Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection;Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection;Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsSection;Lcom/squareup/ui/settings/tiles/TileAppearanceSection;Lcom/squareup/ui/settings/passcodes/PasscodesSection;Lcom/squareup/ui/settings/timetracking/TimeTrackingSection;Lcom/squareup/ui/settings/paymentdevices/CardReadersSection;Lcom/squareup/ui/settings/printerstations/PrinterStationsSection;Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection;Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection;Lcom/squareup/ui/settings/scales/ScalesSettingsSection;Lcom/squareup/ui/settings/devicename/DeviceSection;Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsSection;Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection;Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;Lcom/squareup/ui/settings/bankaccount/BankAccountSection;Lcom/squareup/ui/settings/instantdeposits/DepositsSection;)Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint;
    .locals 35

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    move-object/from16 v32, p31

    move-object/from16 v33, p32

    .line 246
    new-instance v34, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint;

    move-object/from16 v0, v34

    invoke-direct/range {v0 .. v33}, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint;-><init>(Landroid/content/SharedPreferences;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection;Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;Lcom/squareup/ui/settings/taxes/TaxesSection;Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;Lcom/squareup/ui/settings/tipping/TippingSection;Lcom/squareup/ui/settings/cashmanagement/CashManagementSection;Lcom/squareup/ui/settings/offline/OfflineSection;Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection;Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;Lcom/squareup/ui/settings/crm/CustomerManagementSection;Lcom/squareup/ui/settings/crm/EmailCollectionSection;Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection;Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection;Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsSection;Lcom/squareup/ui/settings/tiles/TileAppearanceSection;Lcom/squareup/ui/settings/passcodes/PasscodesSection;Lcom/squareup/ui/settings/timetracking/TimeTrackingSection;Lcom/squareup/ui/settings/paymentdevices/CardReadersSection;Lcom/squareup/ui/settings/printerstations/PrinterStationsSection;Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection;Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection;Lcom/squareup/ui/settings/scales/ScalesSettingsSection;Lcom/squareup/ui/settings/devicename/DeviceSection;Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsSection;Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection;Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;Lcom/squareup/ui/settings/bankaccount/BankAccountSection;Lcom/squareup/ui/settings/instantdeposits/DepositsSection;)V

    return-object v34
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint;
    .locals 35

    move-object/from16 v0, p0

    .line 185
    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->preferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/content/SharedPreferences;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->gatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->sharedSettingsSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->paymentTypesSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->taxesSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/ui/settings/taxes/TaxesSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->signatureAndReceiptSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->quickAmountsSettingsSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->tippingSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/ui/settings/tipping/TippingSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->cashManagementSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/ui/settings/cashmanagement/CashManagementSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->offlineSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/ui/settings/offline/OfflineSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->employeeManagementSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->openTicketsSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->swipeChipCardsSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->customerManagementSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/ui/settings/crm/CustomerManagementSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->emailCollectionSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/ui/settings/crm/EmailCollectionSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->loyaltySectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->giftCardsSettingsSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->onlineCheckoutSettingsSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->tileAppearanceSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/ui/settings/tiles/TileAppearanceSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->passcodesSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/ui/settings/passcodes/PasscodesSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->timeTrackingSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/ui/settings/timetracking/TimeTrackingSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->cardReadersSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/ui/settings/paymentdevices/CardReadersSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->printerStationsSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/ui/settings/printerstations/PrinterStationsSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->cashDrawerSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->barcodeScannersSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v26, v1

    check-cast v26, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->scalesSettingsSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v27, v1

    check-cast v27, Lcom/squareup/ui/settings/scales/ScalesSettingsSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->deviceSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v28, v1

    check-cast v28, Lcom/squareup/ui/settings/devicename/DeviceSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->orderHubNotificationsSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v29, v1

    check-cast v29, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->orderHubPrintingSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v30, v1

    check-cast v30, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->orderHubQuickActionSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v31, v1

    check-cast v31, Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->publicProfileSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v32, v1

    check-cast v32, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->bankAccountSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v33, v1

    check-cast v33, Lcom/squareup/ui/settings/bankaccount/BankAccountSection;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->depositsSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v34, v1

    check-cast v34, Lcom/squareup/ui/settings/instantdeposits/DepositsSection;

    invoke-static/range {v2 .. v34}, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->newInstance(Landroid/content/SharedPreferences;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection;Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;Lcom/squareup/ui/settings/taxes/TaxesSection;Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;Lcom/squareup/ui/settings/tipping/TippingSection;Lcom/squareup/ui/settings/cashmanagement/CashManagementSection;Lcom/squareup/ui/settings/offline/OfflineSection;Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection;Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;Lcom/squareup/ui/settings/crm/CustomerManagementSection;Lcom/squareup/ui/settings/crm/EmailCollectionSection;Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection;Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection;Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsSection;Lcom/squareup/ui/settings/tiles/TileAppearanceSection;Lcom/squareup/ui/settings/passcodes/PasscodesSection;Lcom/squareup/ui/settings/timetracking/TimeTrackingSection;Lcom/squareup/ui/settings/paymentdevices/CardReadersSection;Lcom/squareup/ui/settings/printerstations/PrinterStationsSection;Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection;Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection;Lcom/squareup/ui/settings/scales/ScalesSettingsSection;Lcom/squareup/ui/settings/devicename/DeviceSection;Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsSection;Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection;Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;Lcom/squareup/ui/settings/bankaccount/BankAccountSection;Lcom/squareup/ui/settings/instantdeposits/DepositsSection;)Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 40
    invoke-virtual {p0}, Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint_Factory;->get()Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint;

    move-result-object v0

    return-object v0
.end method
