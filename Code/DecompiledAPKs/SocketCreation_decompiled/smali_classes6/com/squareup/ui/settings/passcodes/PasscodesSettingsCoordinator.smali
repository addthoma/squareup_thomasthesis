.class public Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "PasscodesSettingsCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$TeamPasscodeSwitchListener;,
        Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$PasscodesSettingsEnableSwitchListener;,
        Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$ConsumeSwitchCheckedChangeListener;
    }
.end annotation


# instance fields
.field private final PASSCODES_SETTINGS_ENABLE_SWITCH_LISTENER:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$PasscodesSettingsEnableSwitchListener;

.field private final TEAM_PASSCODE_SWITCH_LISTENER:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$TeamPasscodeSwitchListener;

.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private afterLogoutDescription:Lcom/squareup/widgets/MessageView;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final dashboardBaseUrl:Ljava/lang/String;

.field private mainScheduler:Lio/reactivex/Scheduler;

.field private passcodeSettingsEnableSwitch:Lcom/squareup/noho/NohoCheckableRow;

.field private passcodeSettingsEnableSwitchDescription:Lcom/squareup/widgets/MessageView;

.field private passcodesSettingsEnableSection:Lcom/squareup/noho/NohoLinearLayout;

.field private requirePasscodeAfterEachSaleCheck:Lcom/squareup/noho/NohoCheckableRow;

.field private requirePasscodeAfterLogoutCheck:Lcom/squareup/noho/NohoCheckableRow;

.field private requirePasscodeAfterTimeout:Lcom/squareup/noho/NohoRow;

.field private requirePasscodeSection:Lcom/squareup/noho/NohoLinearLayout;

.field private requirePasscodeWhenBackingOutOfASaleCheck:Lcom/squareup/noho/NohoCheckableRow;

.field private res:Lcom/squareup/util/Res;

.field private scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

.field private teamPasscode:Lcom/squareup/noho/NohoRow;

.field private teamPasscodeDescription:Lcom/squareup/widgets/MessageView;

.field private teamPasscodeSection:Lcom/squareup/noho/NohoLinearLayout;

.field private teamPasscodeSwitch:Lcom/squareup/noho/NohoCheckableRow;

.field private teamPermissions:Lcom/squareup/noho/NohoRow;

.field private view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;Lio/reactivex/Scheduler;Lcom/squareup/analytics/Analytics;Lcom/squareup/http/Server;)V
    .locals 1
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 70
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 62
    new-instance v0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$PasscodesSettingsEnableSwitchListener;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$PasscodesSettingsEnableSwitchListener;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->PASSCODES_SETTINGS_ENABLE_SWITCH_LISTENER:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$PasscodesSettingsEnableSwitchListener;

    .line 64
    new-instance v0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$TeamPasscodeSwitchListener;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$TeamPasscodeSwitchListener;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->TEAM_PASSCODE_SWITCH_LISTENER:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$TeamPasscodeSwitchListener;

    .line 71
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->res:Lcom/squareup/util/Res;

    .line 72
    iput-object p2, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    .line 73
    iput-object p3, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    .line 74
    iput-object p4, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    .line 75
    sget-object p2, Lcom/squareup/http/Endpoints$Server;->STAGING:Lcom/squareup/http/Endpoints$Server;

    invoke-virtual {p5}, Lcom/squareup/http/Server;->getUrl()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Lcom/squareup/http/Endpoints;->getServerFromUrl(Ljava/lang/String;)Lcom/squareup/http/Endpoints$Server;

    move-result-object p3

    if-ne p2, p3, :cond_0

    sget p2, Lcom/squareup/registerlib/R$string;->staging_base_url:I

    .line 76
    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    sget p2, Lcom/squareup/registerlib/R$string;->prod_base_url:I

    .line 77
    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->dashboardBaseUrl:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;)Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    return-object p0
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 104
    sget v0, Lcom/squareup/settingsapplet/R$id;->passcode_settings_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 107
    sget v0, Lcom/squareup/settingsapplet/R$id;->passcodes_settings_enable_switch:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->passcodeSettingsEnableSwitch:Lcom/squareup/noho/NohoCheckableRow;

    .line 108
    sget v0, Lcom/squareup/settingsapplet/R$id;->passcodes_settings_enable_switch_description:I

    .line 109
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->passcodeSettingsEnableSwitchDescription:Lcom/squareup/widgets/MessageView;

    .line 110
    sget v0, Lcom/squareup/settingsapplet/R$id;->passcodes_settings_enable_section:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->passcodesSettingsEnableSection:Lcom/squareup/noho/NohoLinearLayout;

    .line 113
    sget v0, Lcom/squareup/settingsapplet/R$id;->passcodes_settings_team_passcode_section:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->teamPasscodeSection:Lcom/squareup/noho/NohoLinearLayout;

    .line 114
    sget v0, Lcom/squareup/settingsapplet/R$id;->passcodes_settings_team_passcode_switch:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->teamPasscodeSwitch:Lcom/squareup/noho/NohoCheckableRow;

    .line 115
    sget v0, Lcom/squareup/settingsapplet/R$id;->passcodes_settings_team_permissions:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->teamPermissions:Lcom/squareup/noho/NohoRow;

    .line 116
    sget v0, Lcom/squareup/settingsapplet/R$id;->passcodes_settings_team_passcode:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->teamPasscode:Lcom/squareup/noho/NohoRow;

    .line 117
    sget v0, Lcom/squareup/settingsapplet/R$id;->passcodes_settings_team_passcode_description:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->teamPasscodeDescription:Lcom/squareup/widgets/MessageView;

    .line 120
    sget v0, Lcom/squareup/settingsapplet/R$id;->passcodes_settings_require_passcode_section:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->requirePasscodeSection:Lcom/squareup/noho/NohoLinearLayout;

    .line 121
    sget v0, Lcom/squareup/settingsapplet/R$id;->passcodes_settings_after_each_sale_check:I

    .line 122
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->requirePasscodeAfterEachSaleCheck:Lcom/squareup/noho/NohoCheckableRow;

    .line 123
    sget v0, Lcom/squareup/settingsapplet/R$id;->passcodes_settings_back_out_of_sale_check:I

    .line 124
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->requirePasscodeWhenBackingOutOfASaleCheck:Lcom/squareup/noho/NohoCheckableRow;

    .line 125
    sget v0, Lcom/squareup/settingsapplet/R$id;->passcodes_settings_after_timeout:I

    .line 126
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->requirePasscodeAfterTimeout:Lcom/squareup/noho/NohoRow;

    .line 127
    sget v0, Lcom/squareup/settingsapplet/R$id;->passcodes_settings_after_logout_check:I

    .line 128
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->requirePasscodeAfterLogoutCheck:Lcom/squareup/noho/NohoCheckableRow;

    .line 129
    sget v0, Lcom/squareup/settingsapplet/R$id;->passcodes_settings_after_logout_description:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->afterLogoutDescription:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private getActionBarConfig(Z)Lcom/squareup/noho/NohoActionBar$Config;
    .locals 3

    .line 153
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/settingsapplet/R$string;->passcode_settings_section_title:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    .line 154
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    if-eqz p1, :cond_0

    .line 156
    sget-object p1, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v1, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsCoordinator$Yv1i_IJu0CGf0oF5VpUouylwDDc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsCoordinator$Yv1i_IJu0CGf0oF5VpUouylwDDc;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;)V

    invoke-virtual {v0, p1, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_0

    .line 161
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideUpButton()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 163
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method private getEnableSwitchDescription()Ljava/lang/CharSequence;
    .locals 3

    .line 233
    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/squareup/settingsapplet/R$string;->passcode_settings_enable_switch_description:I

    const-string v2, "link_to_dashboard"

    .line 234
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/registerlib/R$string;->dashboard_account_relative_url:I

    .line 235
    invoke-direct {p0, v1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->getUrl(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(Ljava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/settingsapplet/R$string;->employee_management_passcode_description_link_text:I

    .line 236
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 237
    invoke-virtual {v0}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private getPasscodeWithSpaces(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, ""

    const-string v1, " "

    .line 256
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getTeamPasscodeDescription()Ljava/lang/CharSequence;
    .locals 3

    .line 241
    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/squareup/settingsapplet/R$string;->passcode_settings_team_passcode_description:I

    const-string v2, "link_to_dashboard"

    .line 242
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/registerlib/R$string;->dashboard_employees_relative_url:I

    .line 243
    invoke-direct {p0, v1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->getUrl(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(Ljava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/settingsapplet/R$string;->employee_management_passcode_description_link_text:I

    .line 244
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 245
    invoke-virtual {v0}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private getUrl(I)Ljava/lang/String;
    .locals 3

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/registerlib/R$string;->square_relative_url:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->dashboardBaseUrl:Ljava/lang/String;

    const-string v2, "base_url"

    .line 250
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->res:Lcom/squareup/util/Res;

    .line 251
    invoke-interface {v1, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v1, "link_relative_url"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 252
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic lambda$qs4Mh_j689CXsWrodArYs31jHf0(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->showScreenData(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;)V

    return-void
.end method

.method private showRequirePasscodeSection(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;)V
    .locals 4

    .line 210
    iget-object p1, p1, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;->passcodeSettingsState:Lcom/squareup/permissions/PasscodesSettings$State;

    .line 211
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->requirePasscodeSection:Lcom/squareup/noho/NohoLinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLinearLayout;->setVisibility(I)V

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->requirePasscodeAfterEachSaleCheck:Lcom/squareup/noho/NohoCheckableRow;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 214
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->requirePasscodeAfterEachSaleCheck:Lcom/squareup/noho/NohoCheckableRow;

    iget-boolean v2, p1, Lcom/squareup/permissions/PasscodesSettings$State;->requirePasscodeAfterEachSale:Z

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 215
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->requirePasscodeAfterEachSaleCheck:Lcom/squareup/noho/NohoCheckableRow;

    new-instance v2, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsCoordinator$Oms6raMuSYU7WSPsH_WqCKASy9M;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsCoordinator$Oms6raMuSYU7WSPsH_WqCKASy9M;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;)V

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->requirePasscodeWhenBackingOutOfASaleCheck:Lcom/squareup/noho/NohoCheckableRow;

    iget-boolean v2, p1, Lcom/squareup/permissions/PasscodesSettings$State;->requirePasscodeWhenBackingOutOfSale:Z

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 219
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->requirePasscodeAfterTimeout:Lcom/squareup/noho/NohoRow;

    iget-object v2, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->res:Lcom/squareup/util/Res;

    iget-object v3, p1, Lcom/squareup/permissions/PasscodesSettings$State;->passcodeTimeout:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    .line 220
    invoke-static {v3}, Lcom/squareup/ui/settings/passcodes/TimeoutSettingsMapper;->getDisplayStringId(Lcom/squareup/permissions/PasscodesSettings$Timeout;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 219
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 221
    iget-boolean v0, p1, Lcom/squareup/permissions/PasscodesSettings$State;->teamPasscodeEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/permissions/PasscodesSettings$State;->teamPasscode:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->requirePasscodeAfterLogoutCheck:Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->setVisibility(I)V

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->requirePasscodeAfterLogoutCheck:Lcom/squareup/noho/NohoCheckableRow;

    iget-boolean p1, p1, Lcom/squareup/permissions/PasscodesSettings$State;->requirePasscodeAfterLogout:Z

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 224
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->afterLogoutDescription:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 225
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->afterLogoutDescription:Lcom/squareup/widgets/MessageView;

    sget v0, Lcom/squareup/settingsapplet/R$string;->passcode_settings_after_logout_description:I

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setText(I)V

    goto :goto_0

    .line 227
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->requirePasscodeAfterLogoutCheck:Lcom/squareup/noho/NohoCheckableRow;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->setVisibility(I)V

    .line 228
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->afterLogoutDescription:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private showScreenData(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;)V
    .locals 2

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    iget-boolean v1, p1, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;->showBackButton:Z

    invoke-direct {p0, v1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->getActionBarConfig(Z)Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->passcodesSettingsEnableSection:Lcom/squareup/noho/NohoLinearLayout;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLinearLayout;->setVisibility(I)V

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->passcodeSettingsEnableSwitch:Lcom/squareup/noho/NohoCheckableRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->passcodeSettingsEnableSwitch:Lcom/squareup/noho/NohoCheckableRow;

    iget-object v1, p1, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;->passcodeSettingsState:Lcom/squareup/permissions/PasscodesSettings$State;

    iget-boolean v1, v1, Lcom/squareup/permissions/PasscodesSettings$State;->passcodesEnabled:Z

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->passcodeSettingsEnableSwitch:Lcom/squareup/noho/NohoCheckableRow;

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->PASSCODES_SETTINGS_ENABLE_SWITCH_LISTENER:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$PasscodesSettingsEnableSwitchListener;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->passcodeSettingsEnableSwitchDescription:Lcom/squareup/widgets/MessageView;

    invoke-direct {p0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->getEnableSwitchDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v0, p1, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;->passcodeSettingsState:Lcom/squareup/permissions/PasscodesSettings$State;

    iget-boolean v0, v0, Lcom/squareup/permissions/PasscodesSettings$State;->passcodesEnabled:Z

    if-nez v0, :cond_0

    .line 144
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->teamPasscodeSection:Lcom/squareup/noho/NohoLinearLayout;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoLinearLayout;->setVisibility(I)V

    .line 145
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->requirePasscodeSection:Lcom/squareup/noho/NohoLinearLayout;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoLinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 147
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->showTeamPasscodeSection(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;)V

    .line 148
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->showRequirePasscodeSection(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;)V

    :goto_0
    return-void
.end method

.method private showTeamPasscodeSection(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;)V
    .locals 5

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->teamPasscodeSection:Lcom/squareup/noho/NohoLinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLinearLayout;->setVisibility(I)V

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->teamPasscodeSwitch:Lcom/squareup/noho/NohoCheckableRow;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->teamPasscodeSwitch:Lcom/squareup/noho/NohoCheckableRow;

    iget-object v2, p1, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;->passcodeSettingsState:Lcom/squareup/permissions/PasscodesSettings$State;

    iget-boolean v2, v2, Lcom/squareup/permissions/PasscodesSettings$State;->teamPasscodeEnabled:Z

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->teamPasscodeSwitch:Lcom/squareup/noho/NohoCheckableRow;

    iget-object v2, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->TEAM_PASSCODE_SWITCH_LISTENER:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$TeamPasscodeSwitchListener;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 173
    iget-object v0, p1, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;->passcodeSettingsState:Lcom/squareup/permissions/PasscodesSettings$State;

    iget-boolean v0, v0, Lcom/squareup/permissions/PasscodesSettings$State;->teamPasscodeEnabled:Z

    const/16 v2, 0x8

    if-eqz v0, :cond_3

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->teamPermissions:Lcom/squareup/noho/NohoRow;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->teamPermissions:Lcom/squareup/noho/NohoRow;

    new-instance v3, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsCoordinator$QEcw3gyTxK8uoBFjEmyQ8no9tZg;

    invoke-direct {v3, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsCoordinator$QEcw3gyTxK8uoBFjEmyQ8no9tZg;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;)V

    invoke-virtual {v0, v3}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 181
    iget-object v0, p1, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;->passcodeSettingsState:Lcom/squareup/permissions/PasscodesSettings$State;

    iget-object v0, v0, Lcom/squareup/permissions/PasscodesSettings$State;->teamPasscode:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 186
    :goto_0
    iget-object v3, p1, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;->passcodeSettingsState:Lcom/squareup/permissions/PasscodesSettings$State;

    iget-boolean v3, v3, Lcom/squareup/permissions/PasscodesSettings$State;->teamPermissionRoleHasSharedPosAccess:Z

    if-nez v3, :cond_1

    if-eqz v0, :cond_1

    .line 188
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->teamPermissions:Lcom/squareup/noho/NohoRow;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->passcode_settings_team_permissions_enable_link:I

    .line 189
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 188
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 191
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->teamPasscode:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p1, v2}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    goto :goto_2

    .line 193
    :cond_1
    iget-object v2, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->teamPermissions:Lcom/squareup/noho/NohoRow;

    iget-object v3, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/settingsapplet/R$string;->passcode_settings_team_permissions_edit_link:I

    .line 194
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 193
    invoke-virtual {v2, v3}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    if-eqz v0, :cond_2

    .line 196
    iget-object p1, p1, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;->passcodeSettingsState:Lcom/squareup/permissions/PasscodesSettings$State;

    iget-object p1, p1, Lcom/squareup/permissions/PasscodesSettings$State;->teamPasscode:Ljava/lang/String;

    .line 197
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->getPasscodeWithSpaces(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/settingsapplet/R$string;->passcode_settings_create_team_passcode:I

    .line 198
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 199
    :goto_1
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->teamPasscode:Lcom/squareup/noho/NohoRow;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 200
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->teamPasscode:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    goto :goto_2

    .line 203
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->teamPermissions:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p1, v2}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 204
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->teamPasscode:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p1, v2}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 206
    :goto_2
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->teamPasscodeDescription:Lcom/squareup/widgets/MessageView;

    invoke-direct {p0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->getTeamPasscodeDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    .line 81
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->view:Landroid/view/View;

    .line 82
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 83
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->bindViews(Landroid/view/View;)V

    .line 86
    new-instance v0, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsCoordinator$36R7G9Dz32uO_wqDIZjJU8g-HCo;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsCoordinator$36R7G9Dz32uO_wqDIZjJU8g-HCo;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 91
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->teamPasscode:Lcom/squareup/noho/NohoRow;

    new-instance v0, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsCoordinator$ad-fxBlexyr8XG8j2d7-MVzAkGA;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsCoordinator$ad-fxBlexyr8XG8j2d7-MVzAkGA;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;)V

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->requirePasscodeAfterEachSaleCheck:Lcom/squareup/noho/NohoCheckableRow;

    new-instance v0, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsCoordinator$_9kn9wSvGF8Ek_Jm4a-g6Bk5zuI;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsCoordinator$_9kn9wSvGF8Ek_Jm4a-g6Bk5zuI;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;)V

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 94
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->requirePasscodeWhenBackingOutOfASaleCheck:Lcom/squareup/noho/NohoCheckableRow;

    new-instance v0, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsCoordinator$6L2osFgkoiZZE9hkucTZaLl64UY;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsCoordinator$6L2osFgkoiZZE9hkucTZaLl64UY;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;)V

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 96
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->requirePasscodeAfterTimeout:Lcom/squareup/noho/NohoRow;

    new-instance v0, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsCoordinator$d-1jRkvtB9becuCPyJuCz8iaT6w;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsCoordinator$d-1jRkvtB9becuCPyJuCz8iaT6w;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;)V

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->requirePasscodeAfterLogoutCheck:Lcom/squareup/noho/NohoCheckableRow;

    new-instance v0, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsCoordinator$NR0PdZQcOhxAEZ4yPoKPNMwyFUI;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsCoordinator$NR0PdZQcOhxAEZ4yPoKPNMwyFUI;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;)V

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 100
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->maybeShowCreateTeamPasscodeDialogScreen()V

    return-void
.end method

.method public synthetic lambda$attach$0$PasscodesSettingsCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodeSettingsScreenData()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    .line 87
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsCoordinator$qs4Mh_j689CXsWrodArYs31jHf0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsCoordinator$qs4Mh_j689CXsWrodArYs31jHf0;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;)V

    .line 88
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$1$PasscodesSettingsCoordinator(Landroid/view/View;)V
    .locals 0

    .line 91
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onTeamPasscodeClicked()V

    return-void
.end method

.method public synthetic lambda$attach$2$PasscodesSettingsCoordinator(Lcom/squareup/noho/NohoCheckableRow;Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 0

    .line 93
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onRequirePasscodeAfterEachSaleChanged(Ljava/lang/Boolean;)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attach$3$PasscodesSettingsCoordinator(Lcom/squareup/noho/NohoCheckableRow;Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 0

    .line 95
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onRequirePasscodeWhenBackingOutOfASaleChanged(Ljava/lang/Boolean;)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attach$4$PasscodesSettingsCoordinator(Landroid/view/View;)V
    .locals 0

    .line 96
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onTimeoutClicked()V

    return-void
.end method

.method public synthetic lambda$attach$5$PasscodesSettingsCoordinator(Lcom/squareup/noho/NohoCheckableRow;Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 0

    .line 98
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onRequirePasscodeAfterLogoutChanged(Ljava/lang/Boolean;)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$getActionBarConfig$6$PasscodesSettingsCoordinator()Lkotlin/Unit;
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onPasscodesSettingsBackClicked()V

    .line 158
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public synthetic lambda$showRequirePasscodeSection$8$PasscodesSettingsCoordinator(Lcom/squareup/noho/NohoCheckableRow;Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 0

    .line 216
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onRequirePasscodeAfterEachSaleChanged(Ljava/lang/Boolean;)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$showTeamPasscodeSection$7$PasscodesSettingsCoordinator(Landroid/view/View;)V
    .locals 1

    .line 176
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->view:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    sget v0, Lcom/squareup/registerlib/R$string;->dashboard_team_permissions_relative_url:I

    .line 177
    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->getUrl(I)Ljava/lang/String;

    move-result-object v0

    .line 176
    invoke-static {p1, v0}, Lcom/squareup/util/RegisterIntents;->launchBrowser(Landroid/content/Context;Ljava/lang/String;)V

    .line 178
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->forEditPermissionsLinkClicked()Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method
