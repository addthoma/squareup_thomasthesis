.class public final Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView_MembersInjector;
.super Ljava/lang/Object;
.source "MerchantProfileEditLogoView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;",
        ">;"
    }
.end annotation


# instance fields
.field private final picassoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView_MembersInjector;->picassoProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;",
            ">;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPicasso(Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;Lcom/squareup/picasso/Picasso;)V
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->picasso:Lcom/squareup/picasso/Picasso;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;Ljava/lang/Object;)V
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;

    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->presenter:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;)V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView_MembersInjector;->injectPresenter(Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;Ljava/lang/Object;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView_MembersInjector;->picassoProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/picasso/Picasso;

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView_MembersInjector;->injectPicasso(Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;Lcom/squareup/picasso/Picasso;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView_MembersInjector;->injectMembers(Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;)V

    return-void
.end method
