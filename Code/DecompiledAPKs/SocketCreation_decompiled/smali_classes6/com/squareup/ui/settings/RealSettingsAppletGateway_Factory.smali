.class public final Lcom/squareup/ui/settings/RealSettingsAppletGateway_Factory;
.super Ljava/lang/Object;
.source "RealSettingsAppletGateway_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/RealSettingsAppletGateway;",
        ">;"
    }
.end annotation


# instance fields
.field private final bankAccountSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/bankaccount/BankAccountSection;",
            ">;"
        }
    .end annotation
.end field

.field private final depositsSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositsSection;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsApplet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/bankaccount/BankAccountSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsApplet;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/settings/RealSettingsAppletGateway_Factory;->depositsSectionProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/settings/RealSettingsAppletGateway_Factory;->bankAccountSectionProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/settings/RealSettingsAppletGateway_Factory;->settingsAppletProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/RealSettingsAppletGateway_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/bankaccount/BankAccountSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsApplet;",
            ">;)",
            "Lcom/squareup/ui/settings/RealSettingsAppletGateway_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/ui/settings/RealSettingsAppletGateway_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/RealSettingsAppletGateway_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/settings/instantdeposits/DepositsSection;Lcom/squareup/ui/settings/bankaccount/BankAccountSection;Lcom/squareup/ui/settings/SettingsApplet;)Lcom/squareup/ui/settings/RealSettingsAppletGateway;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/ui/settings/RealSettingsAppletGateway;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/RealSettingsAppletGateway;-><init>(Lcom/squareup/ui/settings/instantdeposits/DepositsSection;Lcom/squareup/ui/settings/bankaccount/BankAccountSection;Lcom/squareup/ui/settings/SettingsApplet;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/RealSettingsAppletGateway;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/settings/RealSettingsAppletGateway_Factory;->depositsSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/instantdeposits/DepositsSection;

    iget-object v1, p0, Lcom/squareup/ui/settings/RealSettingsAppletGateway_Factory;->bankAccountSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/bankaccount/BankAccountSection;

    iget-object v2, p0, Lcom/squareup/ui/settings/RealSettingsAppletGateway_Factory;->settingsAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/settings/SettingsApplet;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/settings/RealSettingsAppletGateway_Factory;->newInstance(Lcom/squareup/ui/settings/instantdeposits/DepositsSection;Lcom/squareup/ui/settings/bankaccount/BankAccountSection;Lcom/squareup/ui/settings/SettingsApplet;)Lcom/squareup/ui/settings/RealSettingsAppletGateway;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/settings/RealSettingsAppletGateway_Factory;->get()Lcom/squareup/ui/settings/RealSettingsAppletGateway;

    move-result-object v0

    return-object v0
.end method
