.class public final Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;
.super Ljava/lang/Object;
.source "DepositSettingsScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008$\u0018\u00002\u00020\u0001B\u00a9\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0008\u0008\u0001\u0010\t\u001a\u00020\n\u0012\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u000e\u0012\u0006\u0010\u0010\u001a\u00020\u000e\u0012\u0006\u0010\u0011\u001a\u00020\u000e\u0012\u0006\u0010\u0012\u001a\u00020\u000e\u0012\u0006\u0010\u0013\u001a\u00020\u000e\u0012\u0006\u0010\u0014\u001a\u00020\u000e\u0012\u0006\u0010\u0015\u001a\u00020\u000e\u0012\u0006\u0010\u0016\u001a\u00020\u000e\u0012\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0002\u0010\u001aR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010 R\u0017\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\"R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010$R\u0011\u0010\u000f\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010&R\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\'\u0010&R\u0017\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008(\u0010\"R\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008)\u0010\"R\u0017\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008*\u0010\"R\u0011\u0010\u0014\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008+\u0010&R\u0011\u0010\u0016\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008,\u0010&R\u0011\u0010\u0015\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008-\u0010&R\u0011\u0010\u0011\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008.\u0010&R\u0011\u0010\u0010\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008/\u0010&R\u0011\u0010\u0012\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00080\u0010&R\u0011\u0010\u0013\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00081\u0010&\u00a8\u00062"
    }
    d2 = {
        "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;",
        "",
        "bankSettingsState",
        "Lcom/squareup/banklinking/BankAccountSettings$State;",
        "depositScheduleState",
        "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
        "linkedCardText",
        "Lcom/squareup/resources/TextModel;",
        "",
        "cardDrawable",
        "",
        "cardStatus",
        "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;",
        "instantDepositAllowed",
        "",
        "hasActivatedAccount",
        "showInstantTransfersSection",
        "showInstantTransfersInstrument",
        "showInstantTransfersSetUpButton",
        "showInstantTransfersToggle",
        "showAutomaticDeposits",
        "showDepositSpeed",
        "showDepositSchedule",
        "instantTransfersHint",
        "sameDayDepositFee",
        "depositScheduleHint",
        "(Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/depositschedule/DepositScheduleSettings$State;Lcom/squareup/resources/TextModel;ILcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;ZZZZZZZZZLcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;)V",
        "getBankSettingsState",
        "()Lcom/squareup/banklinking/BankAccountSettings$State;",
        "getCardDrawable",
        "()I",
        "getCardStatus",
        "()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;",
        "getDepositScheduleHint",
        "()Lcom/squareup/resources/TextModel;",
        "getDepositScheduleState",
        "()Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
        "getHasActivatedAccount",
        "()Z",
        "getInstantDepositAllowed",
        "getInstantTransfersHint",
        "getLinkedCardText",
        "getSameDayDepositFee",
        "getShowAutomaticDeposits",
        "getShowDepositSchedule",
        "getShowDepositSpeed",
        "getShowInstantTransfersInstrument",
        "getShowInstantTransfersSection",
        "getShowInstantTransfersSetUpButton",
        "getShowInstantTransfersToggle",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bankSettingsState:Lcom/squareup/banklinking/BankAccountSettings$State;

.field private final cardDrawable:I

.field private final cardStatus:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

.field private final depositScheduleHint:Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final depositScheduleState:Lcom/squareup/depositschedule/DepositScheduleSettings$State;

.field private final hasActivatedAccount:Z

.field private final instantDepositAllowed:Z

.field private final instantTransfersHint:Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final linkedCardText:Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final sameDayDepositFee:Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final showAutomaticDeposits:Z

.field private final showDepositSchedule:Z

.field private final showDepositSpeed:Z

.field private final showInstantTransfersInstrument:Z

.field private final showInstantTransfersSection:Z

.field private final showInstantTransfersSetUpButton:Z

.field private final showInstantTransfersToggle:Z


# direct methods
.method public constructor <init>(Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/depositschedule/DepositScheduleSettings$State;Lcom/squareup/resources/TextModel;ILcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;ZZZZZZZZZLcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;I",
            "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;",
            "ZZZZZZZZZ",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p15

    move-object/from16 v5, p16

    move-object/from16 v6, p17

    const-string v7, "bankSettingsState"

    invoke-static {p1, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "depositScheduleState"

    invoke-static {p2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "linkedCardText"

    invoke-static {p3, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "instantTransfersHint"

    invoke-static {v4, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "sameDayDepositFee"

    invoke-static {v5, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "depositScheduleHint"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->bankSettingsState:Lcom/squareup/banklinking/BankAccountSettings$State;

    iput-object v2, v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->depositScheduleState:Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    iput-object v3, v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->linkedCardText:Lcom/squareup/resources/TextModel;

    move v1, p4

    iput v1, v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->cardDrawable:I

    move-object v1, p5

    iput-object v1, v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->cardStatus:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    move v1, p6

    iput-boolean v1, v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->instantDepositAllowed:Z

    move v1, p7

    iput-boolean v1, v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->hasActivatedAccount:Z

    move/from16 v1, p8

    iput-boolean v1, v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->showInstantTransfersSection:Z

    move/from16 v1, p9

    iput-boolean v1, v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->showInstantTransfersInstrument:Z

    move/from16 v1, p10

    iput-boolean v1, v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->showInstantTransfersSetUpButton:Z

    move/from16 v1, p11

    iput-boolean v1, v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->showInstantTransfersToggle:Z

    move/from16 v1, p12

    iput-boolean v1, v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->showAutomaticDeposits:Z

    move/from16 v1, p13

    iput-boolean v1, v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->showDepositSpeed:Z

    move/from16 v1, p14

    iput-boolean v1, v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->showDepositSchedule:Z

    iput-object v4, v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->instantTransfersHint:Lcom/squareup/resources/TextModel;

    iput-object v5, v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->sameDayDepositFee:Lcom/squareup/resources/TextModel;

    iput-object v6, v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->depositScheduleHint:Lcom/squareup/resources/TextModel;

    return-void
.end method


# virtual methods
.method public final getBankSettingsState()Lcom/squareup/banklinking/BankAccountSettings$State;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->bankSettingsState:Lcom/squareup/banklinking/BankAccountSettings$State;

    return-object v0
.end method

.method public final getCardDrawable()I
    .locals 1

    .line 42
    iget v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->cardDrawable:I

    return v0
.end method

.method public final getCardStatus()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->cardStatus:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    return-object v0
.end method

.method public final getDepositScheduleHint()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->depositScheduleHint:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final getDepositScheduleState()Lcom/squareup/depositschedule/DepositScheduleSettings$State;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->depositScheduleState:Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    return-object v0
.end method

.method public final getHasActivatedAccount()Z
    .locals 1

    .line 45
    iget-boolean v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->hasActivatedAccount:Z

    return v0
.end method

.method public final getInstantDepositAllowed()Z
    .locals 1

    .line 44
    iget-boolean v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->instantDepositAllowed:Z

    return v0
.end method

.method public final getInstantTransfersHint()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->instantTransfersHint:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final getLinkedCardText()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->linkedCardText:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final getSameDayDepositFee()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->sameDayDepositFee:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final getShowAutomaticDeposits()Z
    .locals 1

    .line 50
    iget-boolean v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->showAutomaticDeposits:Z

    return v0
.end method

.method public final getShowDepositSchedule()Z
    .locals 1

    .line 52
    iget-boolean v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->showDepositSchedule:Z

    return v0
.end method

.method public final getShowDepositSpeed()Z
    .locals 1

    .line 51
    iget-boolean v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->showDepositSpeed:Z

    return v0
.end method

.method public final getShowInstantTransfersInstrument()Z
    .locals 1

    .line 47
    iget-boolean v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->showInstantTransfersInstrument:Z

    return v0
.end method

.method public final getShowInstantTransfersSection()Z
    .locals 1

    .line 46
    iget-boolean v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->showInstantTransfersSection:Z

    return v0
.end method

.method public final getShowInstantTransfersSetUpButton()Z
    .locals 1

    .line 48
    iget-boolean v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->showInstantTransfersSetUpButton:Z

    return v0
.end method

.method public final getShowInstantTransfersToggle()Z
    .locals 1

    .line 49
    iget-boolean v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;->showInstantTransfersToggle:Z

    return v0
.end method
