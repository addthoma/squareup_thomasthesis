.class Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "PrinterStationsListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;->buildAndBindButtonRow(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;)V
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter$1;->this$1:Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 120
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter$1;->this$1:Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;

    iget-object p1, p1, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;->this$0:Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;

    iget-object p1, p1, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;->presenter:Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->onCreatePrinterStationClicked()V

    return-void
.end method
