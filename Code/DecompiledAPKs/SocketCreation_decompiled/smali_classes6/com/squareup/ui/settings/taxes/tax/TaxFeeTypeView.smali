.class public Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;
.super Landroid/widget/LinearLayout;
.source "TaxFeeTypeView.java"


# instance fields
.field private container:Landroid/view/ViewGroup;

.field presenter:Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    const-class p2, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Component;->inject(Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;)V

    return-void
.end method


# virtual methods
.method createFeeTypeRow(Ljava/lang/CharSequence;)V
    .locals 3

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;->container:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 44
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-static {v1, p1, v2}, Lcom/squareup/widgets/list/ToggleButtonRow;->radioRow(Landroid/content/Context;Ljava/lang/CharSequence;I)Lcom/squareup/widgets/list/ToggleButtonRow;

    move-result-object p1

    .line 45
    sget v1, Lcom/squareup/marin/R$drawable;->marin_selector_ultra_light_gray_when_pressed:I

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setBackgroundResource(I)V

    const/4 v1, 0x1

    .line 47
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setFocusable(Z)V

    .line 48
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setClickable(Z)V

    .line 49
    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView$1;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;I)V

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;->container:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 63
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 38
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 31
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 32
    sget v0, Lcom/squareup/settingsapplet/R$id;->tax_fee_type_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;->container:Landroid/view/ViewGroup;

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method setFeeTypeRowChecked(IZ)V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;->container:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 59
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method
