.class public final Lcom/squareup/ui/settings/SettingsAppletScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "SettingsAppletScope.java"

# interfaces
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent$Responsive;
    phone = Lcom/squareup/ui/settings/SettingsAppletScope$PhoneComponent;
    tablet = Lcom/squareup/ui/settings/SettingsAppletScope$TabletComponent;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;,
        Lcom/squareup/ui/settings/SettingsAppletScope$ParentComponent;,
        Lcom/squareup/ui/settings/SettingsAppletScope$TabletComponent;,
        Lcom/squareup/ui/settings/SettingsAppletScope$PhoneComponent;,
        Lcom/squareup/ui/settings/SettingsAppletScope$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/SettingsAppletScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/SettingsAppletScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 94
    new-instance v0, Lcom/squareup/ui/settings/SettingsAppletScope;

    invoke-direct {v0}, Lcom/squareup/ui/settings/SettingsAppletScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/SettingsAppletScope;->INSTANCE:Lcom/squareup/ui/settings/SettingsAppletScope;

    .line 291
    sget-object v0, Lcom/squareup/ui/settings/SettingsAppletScope;->INSTANCE:Lcom/squareup/ui/settings/SettingsAppletScope;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/SettingsAppletScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 96
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    .line 100
    invoke-super {p0, p1}, Lcom/squareup/ui/main/InMainActivityScope;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object v0

    .line 101
    const-class v1, Lcom/squareup/ui/settings/SettingsAppletScope$ParentComponent;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/SettingsAppletScope$ParentComponent;

    .line 104
    invoke-interface {p1}, Lcom/squareup/ui/settings/SettingsAppletScope$ParentComponent;->previewSelectMethodWorkflowRunner()Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;

    move-result-object p1

    .line 105
    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 111
    sget-object p1, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 3

    .line 115
    const-class v0, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;

    .line 116
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v1

    invoke-interface {v0}, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;->emailCollectionSectionController()Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 117
    invoke-interface {v0}, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;->settingsAppletScopeRunner()Lcom/squareup/ui/settings/SettingsAppletScopeRunner;

    move-result-object v1

    invoke-virtual {p1, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 118
    invoke-interface {v0}, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;->passcodeSettingsScopeRunner()Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    move-result-object v1

    invoke-virtual {p1, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 119
    invoke-interface {v0}, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;->timeTrackingSettingsScopeRunner()Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
