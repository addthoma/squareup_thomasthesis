.class final enum Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;
.super Ljava/lang/Enum;
.source "MerchantProfileEditPhotoPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Choice"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

.field public static final enum CANCEL:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

.field public static final enum CHOOSE_PHOTO:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

.field public static final enum CLEAR_PHOTO:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

.field public static final enum TAKE_PHOTO:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 90
    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    const/4 v1, 0x0

    const-string v2, "TAKE_PHOTO"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;->TAKE_PHOTO:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    .line 91
    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    const/4 v2, 0x1

    const-string v3, "CHOOSE_PHOTO"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;->CHOOSE_PHOTO:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    .line 92
    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    const/4 v3, 0x2

    const-string v4, "CLEAR_PHOTO"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;->CLEAR_PHOTO:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    .line 93
    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    const/4 v4, 0x3

    const-string v5, "CANCEL"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;->CANCEL:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    .line 89
    sget-object v5, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;->TAKE_PHOTO:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;->CHOOSE_PHOTO:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;->CLEAR_PHOTO:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;->CANCEL:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;->$VALUES:[Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 89
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;
    .locals 1

    .line 89
    const-class v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;
    .locals 1

    .line 89
    sget-object v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;->$VALUES:[Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    invoke-virtual {v0}, [Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    return-object v0
.end method
