.class Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$FeaturedImageTarget;
.super Ljava/lang/Object;
.source "MerchantProfileView.java"

# interfaces
.implements Lcom/squareup/picasso/Target;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FeaturedImageTarget"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;)V
    .locals 0

    .line 620
    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$FeaturedImageTarget;->this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$1;)V
    .locals 0

    .line 620
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$FeaturedImageTarget;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;)V

    return-void
.end method


# virtual methods
.method public onBitmapFailed(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    return-void
.end method

.method public onBitmapLoaded(Landroid/graphics/Bitmap;Lcom/squareup/picasso/Picasso$LoadedFrom;)V
    .locals 0

    .line 623
    iget-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$FeaturedImageTarget;->this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    invoke-static {p2, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->access$500(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public onPrepareLoad(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    return-void
.end method
