.class public Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;
.super Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$BasicViewHolder;
.source "EditTicketGroupRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "TicketTemplateViewHolder"
.end annotation


# instance fields
.field editingLastTemplate:Z

.field presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

.field textWatcher:Lcom/squareup/debounce/DebouncedTextWatcher;

.field ticketTemplateBuilder:Lcom/squareup/api/items/TicketTemplate$Builder;

.field ticketTemplateRow:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)V
    .locals 1

    .line 80
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$BasicViewHolder;-><init>(Landroid/view/View;)V

    .line 81
    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    .line 82
    check-cast p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;

    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateRow:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;

    .line 83
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateRow:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->showControls()V

    .line 84
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateRow:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;

    const/4 v0, 0x1

    invoke-virtual {p1, v0, v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->setHorizontalBorders(ZZ)V

    .line 85
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateRow:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;

    new-instance v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder$1;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->setDeleteClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateRow:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;

    new-instance v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder$x0JoNFHTVJj2h9MCo5KQ0odoBDU;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder$x0JoNFHTVJj2h9MCo5KQ0odoBDU;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->setFocusChangedListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 97
    new-instance p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder$2;

    invoke-direct {p1, p0, p2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder$2;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->textWatcher:Lcom/squareup/debounce/DebouncedTextWatcher;

    return-void
.end method

.method public static inflate(Landroid/view/ViewGroup;Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;
    .locals 4

    .line 75
    new-instance v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/squareup/settingsapplet/R$layout;->ticket_template_row:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)V

    return-object v0
.end method


# virtual methods
.method public bindData(Lcom/squareup/api/items/TicketTemplate$Builder;)V
    .locals 3

    .line 125
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateBuilder:Lcom/squareup/api/items/TicketTemplate$Builder;

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->textWatcher:Lcom/squareup/debounce/DebouncedTextWatcher;

    if-eqz v0, :cond_0

    .line 127
    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateRow:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->isLastTicketTemplateState(Lcom/squareup/api/items/TicketTemplate$Builder;)Z

    move-result v0

    .line 131
    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->isTicketGroupSizeLimitReached()Z

    move-result v1

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 134
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateRow:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->setInputEnabled(Z)V

    .line 135
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateRow:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;

    const-string v0, ""

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->setContent(Ljava/lang/CharSequence;)V

    .line 136
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateRow:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getTicketGroupSizeLimitReachedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->setHint(Ljava/lang/CharSequence;)V

    .line 137
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateRow:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->hideControls()V

    goto :goto_0

    .line 139
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateRow:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->setInputEnabled(Z)V

    .line 140
    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateRow:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;

    iget-object p1, p1, Lcom/squareup/api/items/TicketTemplate$Builder;->name:Ljava/lang/String;

    invoke-virtual {v1, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->setContent(Ljava/lang/CharSequence;)V

    .line 141
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateRow:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->textWatcher:Lcom/squareup/debounce/DebouncedTextWatcher;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->addTextChangedListener(Landroid/text/TextWatcher;)V

    if-eqz v0, :cond_2

    .line 144
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateRow:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->hideControls()V

    goto :goto_0

    .line 146
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateRow:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->showControls()V

    :goto_0
    return-void
.end method

.method public canDropOver()Z
    .locals 1

    .line 121
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->getDragHandle()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getDragHandle()Landroid/view/View;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateRow:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->getDragHandle()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$new$0$EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;Landroid/view/View;Z)V
    .locals 0

    if-eqz p3, :cond_0

    .line 92
    iget-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateBuilder:Lcom/squareup/api/items/TicketTemplate$Builder;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->isLastTicketTemplateState(Lcom/squareup/api/items/TicketTemplate$Builder;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->editingLastTemplate:Z

    goto :goto_0

    .line 94
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateBuilder:Lcom/squareup/api/items/TicketTemplate$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->getAdapterPosition()I

    move-result p3

    invoke-virtual {p1, p2, p3}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->onTemplateLostFocus(Lcom/squareup/api/items/TicketTemplate$Builder;I)V

    :goto_0
    return-void
.end method
