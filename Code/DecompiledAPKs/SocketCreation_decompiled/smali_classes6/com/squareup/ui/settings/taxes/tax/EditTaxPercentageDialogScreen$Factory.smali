.class public Lcom/squareup/ui/settings/taxes/tax/EditTaxPercentageDialogScreen$Factory;
.super Ljava/lang/Object;
.source "EditTaxPercentageDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/taxes/tax/EditTaxPercentageDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 27
    invoke-static {p1}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/taxes/tax/EditTaxPercentageDialogScreen;

    .line 29
    const-class v1, Lcom/squareup/ui/settings/taxes/tax/TaxScope$Component;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/taxes/tax/TaxScope$Component;

    .line 30
    invoke-interface {v1}, Lcom/squareup/ui/settings/taxes/tax/TaxScope$Component;->taxScopeRunner()Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;

    move-result-object v1

    .line 31
    invoke-virtual {v1}, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->buildTaxPercentageScrubber()Lcom/squareup/text/PercentageScrubber;

    move-result-object v2

    .line 33
    invoke-static {v0}, Lcom/squareup/ui/settings/taxes/tax/EditTaxPercentageDialogScreen;->access$000(Lcom/squareup/ui/settings/taxes/tax/EditTaxPercentageDialogScreen;)Lcom/squareup/register/widgets/EditTextDialogFactory;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$Nv9XTQYGOFdJN9vE8008-9VwSag;

    invoke-direct {v3, v1}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$Nv9XTQYGOFdJN9vE8008-9VwSag;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;)V

    invoke-virtual {v0, p1, v2, v3}, Lcom/squareup/register/widgets/EditTextDialogFactory;->createEditTextDialog(Landroid/content/Context;Lcom/squareup/text/Scrubber;Lcom/squareup/register/widgets/EditTextDialogFactory$EditTextDialogListener;)Landroid/app/AlertDialog;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
