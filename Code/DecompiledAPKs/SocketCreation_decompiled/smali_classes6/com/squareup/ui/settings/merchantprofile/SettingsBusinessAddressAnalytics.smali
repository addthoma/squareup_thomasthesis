.class public final Lcom/squareup/ui/settings/merchantprofile/SettingsBusinessAddressAnalytics;
.super Ljava/lang/Object;
.source "SettingsBusinessAddressAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0013\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0013\u001a\u000c\u0010\u0014\u001a\u00020\u0015*\u00020\u0016H\u0000\u001a\u000c\u0010\u0017\u001a\u00020\u0015*\u00020\u0016H\u0000\u001a\u000c\u0010\u0018\u001a\u00020\u0015*\u00020\u0016H\u0000\u001a\u000c\u0010\u0019\u001a\u00020\u0015*\u00020\u0016H\u0000\u001a\u000c\u0010\u001a\u001a\u00020\u0015*\u00020\u0016H\u0000\u001a\u000c\u0010\u001b\u001a\u00020\u0015*\u00020\u0016H\u0000\u001a\u000c\u0010\u001c\u001a\u00020\u0015*\u00020\u0016H\u0000\u001a\u000c\u0010\u001d\u001a\u00020\u0015*\u00020\u0016H\u0000\u001a\u000c\u0010\u001e\u001a\u00020\u0015*\u00020\u0016H\u0000\u001a\u000c\u0010\u001f\u001a\u00020\u0015*\u00020\u0016H\u0000\u001a\u000c\u0010 \u001a\u00020\u0015*\u00020\u0016H\u0000\u001a\u000c\u0010!\u001a\u00020\u0015*\u00020\u0016H\u0000\u001a\u000c\u0010\"\u001a\u00020\u0015*\u00020\u0016H\u0000\u001a\u000c\u0010#\u001a\u00020\u0015*\u00020\u0016H\u0000\u001a\u000c\u0010$\u001a\u00020\u0015*\u00020\u0016H\u0000\u001a\u000c\u0010%\u001a\u00020\u0015*\u00020\u0016H\u0000\u001a\u000c\u0010&\u001a\u00020\u0015*\u00020\u0016H\u0000\u001a\u000c\u0010\'\u001a\u00020\u0015*\u00020\u0016H\u0000\u001a\u000c\u0010(\u001a\u00020\u0015*\u00020\u0016H\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0008\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\t\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\n\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000b\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000c\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\r\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000e\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000f\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0010\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0011\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0012\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0013\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"
    }
    d2 = {
        "SETTINGS_ADDRESS_CONFIRM_CORRECT",
        "",
        "SETTINGS_ADDRESS_NOT_CORRECT",
        "SETTINGS_ADDRESS_VIEW_FORGOT_SOMETHING",
        "SETTINGS_EDIT_ADDRESS",
        "SETTINGS_EDIT_CANCEL",
        "SETTINGS_EDIT_SAVE",
        "SETTINGS_MOBILE_CONFIRM_CORRECT",
        "SETTINGS_MOBILE_NOT_CORRECT",
        "SETTINGS_MOBILE_VIEW_FORGOT_SOMETHING",
        "SETTINGS_TOGGLE_MOBILE_OFF",
        "SETTINGS_TOGGLE_MOBILE_ON",
        "SETTINGS_VERIFY_BUSINESS_ADDRESS_CONTINUE",
        "SETTINGS_VERIFY_BUSINESS_ADDRESS_MODAL",
        "SETTINGS_VERIFY_CURRENT_LOCATION_CONTINUE",
        "SETTINGS_VERIFY_CURRENT_LOCATION_MODAL",
        "SETTINGS_VERIFY_NO_ADDRESS_CONTINUE",
        "SETTINGS_VERIFY_NO_ADDRESS_MODAL",
        "SETTINGS_VIEW_BADGE_SEEN",
        "SETTINGS_VIEW_INVALID_ADDRESS",
        "logAddressIsCorrect",
        "",
        "Lcom/squareup/analytics/Analytics;",
        "logAddressNotCorrect",
        "logAddressShowForgotSomething",
        "logCancelAddress",
        "logEditAddress",
        "logInvalidAddress",
        "logMobileBusiness",
        "logMobileIsCorrect",
        "logMobileNotCorrect",
        "logMobileShowForgotSomething",
        "logNotMobileBusiness",
        "logSaveAddress",
        "logVerifyBadgeSeen",
        "logVerifyBusinessAddressContinue",
        "logVerifyBusinessAddressModal",
        "logVerifyCurrentLocationContinue",
        "logVerifyCurrentLocationModal",
        "logVerifyNoAddressContinue",
        "logVerifyNoAddressModal",
        "settings-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final SETTINGS_ADDRESS_CONFIRM_CORRECT:Ljava/lang/String; = "Settings Business Information: Forgot Something Address Continue"

.field public static final SETTINGS_ADDRESS_NOT_CORRECT:Ljava/lang/String; = "Settings Business Information: Forgot Something Address Edit"

.field public static final SETTINGS_ADDRESS_VIEW_FORGOT_SOMETHING:Ljava/lang/String; = "Settings Business Information: Forgot Something Address Modal"

.field public static final SETTINGS_EDIT_ADDRESS:Ljava/lang/String; = "Settings Business Information: Edit Business Address"

.field public static final SETTINGS_EDIT_CANCEL:Ljava/lang/String; = "Settings Business Information: Edit Business Address Cancel"

.field public static final SETTINGS_EDIT_SAVE:Ljava/lang/String; = "Settings Business Information: Edit Business Address Save"

.field public static final SETTINGS_MOBILE_CONFIRM_CORRECT:Ljava/lang/String; = "Settings Business Information: Forgot Something Mobile Continue"

.field public static final SETTINGS_MOBILE_NOT_CORRECT:Ljava/lang/String; = "Settings Business Information: Forgot Something Mobile Edit"

.field public static final SETTINGS_MOBILE_VIEW_FORGOT_SOMETHING:Ljava/lang/String; = "Settings Business Information: Forgot Something Mobile Modal"

.field public static final SETTINGS_TOGGLE_MOBILE_OFF:Ljava/lang/String; = "Settings Business Information: Mobile Business Toggle Off"

.field public static final SETTINGS_TOGGLE_MOBILE_ON:Ljava/lang/String; = "Settings Business Information: Mobile Business Toggle On"

.field public static final SETTINGS_VERIFY_BUSINESS_ADDRESS_CONTINUE:Ljava/lang/String; = "Settings Business Information: Verify Business Address Continue"

.field public static final SETTINGS_VERIFY_BUSINESS_ADDRESS_MODAL:Ljava/lang/String; = "Settings Business Information: Verify Business Address Modal"

.field public static final SETTINGS_VERIFY_CURRENT_LOCATION_CONTINUE:Ljava/lang/String; = "Settings Business Information: Verify Current Location Continue"

.field public static final SETTINGS_VERIFY_CURRENT_LOCATION_MODAL:Ljava/lang/String; = "Settings Business Information: Verify Current Location Modal"

.field public static final SETTINGS_VERIFY_NO_ADDRESS_CONTINUE:Ljava/lang/String; = "Settings Business Information: Verify No Address Continue"

.field public static final SETTINGS_VERIFY_NO_ADDRESS_MODAL:Ljava/lang/String; = "Settings Business Information: Verify No Address Modal"

.field public static final SETTINGS_VIEW_BADGE_SEEN:Ljava/lang/String; = "Settings Business Information: Verify Badge"

.field public static final SETTINGS_VIEW_INVALID_ADDRESS:Ljava/lang/String; = "Settings Business Information: Invalid Address"


# direct methods
.method public static final logAddressIsCorrect(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logAddressIsCorrect"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    new-instance v0, Lcom/squareup/analytics/event/ClickEvent;

    const-string v1, "Settings Business Information: Forgot Something Address Continue"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public static final logAddressNotCorrect(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logAddressNotCorrect"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    new-instance v0, Lcom/squareup/analytics/event/ClickEvent;

    const-string v1, "Settings Business Information: Forgot Something Address Edit"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public static final logAddressShowForgotSomething(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logAddressShowForgotSomething"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    new-instance v0, Lcom/squareup/analytics/event/ViewEvent;

    const-string v1, "Settings Business Information: Forgot Something Address Modal"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ViewEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public static final logCancelAddress(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logCancelAddress"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    new-instance v0, Lcom/squareup/analytics/event/ClickEvent;

    const-string v1, "Settings Business Information: Edit Business Address Cancel"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public static final logEditAddress(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logEditAddress"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    new-instance v0, Lcom/squareup/analytics/event/ClickEvent;

    const-string v1, "Settings Business Information: Edit Business Address"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public static final logInvalidAddress(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logInvalidAddress"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    new-instance v0, Lcom/squareup/analytics/event/ViewEvent;

    const-string v1, "Settings Business Information: Invalid Address"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ViewEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public static final logMobileBusiness(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logMobileBusiness"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    new-instance v0, Lcom/squareup/analytics/event/ClickEvent;

    const-string v1, "Settings Business Information: Mobile Business Toggle On"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public static final logMobileIsCorrect(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logMobileIsCorrect"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    new-instance v0, Lcom/squareup/analytics/event/ClickEvent;

    const-string v1, "Settings Business Information: Forgot Something Mobile Continue"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public static final logMobileNotCorrect(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logMobileNotCorrect"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    new-instance v0, Lcom/squareup/analytics/event/ClickEvent;

    const-string v1, "Settings Business Information: Forgot Something Mobile Edit"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public static final logMobileShowForgotSomething(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logMobileShowForgotSomething"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    new-instance v0, Lcom/squareup/analytics/event/ViewEvent;

    const-string v1, "Settings Business Information: Forgot Something Mobile Modal"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ViewEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public static final logNotMobileBusiness(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logNotMobileBusiness"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    new-instance v0, Lcom/squareup/analytics/event/ClickEvent;

    const-string v1, "Settings Business Information: Mobile Business Toggle Off"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public static final logSaveAddress(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logSaveAddress"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    new-instance v0, Lcom/squareup/analytics/event/ClickEvent;

    const-string v1, "Settings Business Information: Edit Business Address Save"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public static final logVerifyBadgeSeen(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logVerifyBadgeSeen"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    new-instance v0, Lcom/squareup/analytics/event/ViewEvent;

    const-string v1, "Settings Business Information: Verify Badge"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ViewEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public static final logVerifyBusinessAddressContinue(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logVerifyBusinessAddressContinue"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    new-instance v0, Lcom/squareup/analytics/event/ClickEvent;

    const-string v1, "Settings Business Information: Verify Business Address Continue"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public static final logVerifyBusinessAddressModal(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logVerifyBusinessAddressModal"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    new-instance v0, Lcom/squareup/analytics/event/ViewEvent;

    const-string v1, "Settings Business Information: Verify Business Address Modal"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ViewEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public static final logVerifyCurrentLocationContinue(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logVerifyCurrentLocationContinue"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    new-instance v0, Lcom/squareup/analytics/event/ClickEvent;

    const-string v1, "Settings Business Information: Verify Current Location Continue"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public static final logVerifyCurrentLocationModal(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logVerifyCurrentLocationModal"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    new-instance v0, Lcom/squareup/analytics/event/ViewEvent;

    const-string v1, "Settings Business Information: Verify Current Location Modal"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ViewEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public static final logVerifyNoAddressContinue(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logVerifyNoAddressContinue"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    new-instance v0, Lcom/squareup/analytics/event/ClickEvent;

    const-string v1, "Settings Business Information: Verify No Address Continue"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public static final logVerifyNoAddressModal(Lcom/squareup/analytics/Analytics;)V
    .locals 2

    const-string v0, "$this$logVerifyNoAddressModal"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    new-instance v0, Lcom/squareup/analytics/event/ViewEvent;

    const-string v1, "Settings Business Information: Verify No Address Modal"

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ViewEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method
