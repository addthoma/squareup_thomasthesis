.class public final Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper_Factory;
.super Ljava/lang/Object;
.source "GiftCardsSettingsHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final unitTokenProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper_Factory;->unitTokenProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper_Factory;->accountSettingsProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)",
            "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;-><init>(Ljava/lang/String;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/protos/common/CurrencyCode;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper_Factory;->unitTokenProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper_Factory;->accountSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper_Factory;->newInstance(Ljava/lang/String;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper_Factory;->get()Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;

    move-result-object v0

    return-object v0
.end method
