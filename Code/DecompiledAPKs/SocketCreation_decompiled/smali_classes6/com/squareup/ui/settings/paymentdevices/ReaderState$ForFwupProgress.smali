.class public final Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;
.super Lcom/squareup/ui/settings/paymentdevices/ReaderState;
.source "ReaderState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/ReaderState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ForFwupProgress"
.end annotation


# instance fields
.field public final currentPercentComplete:I

.field public final totalPercentComplete:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0

    .line 235
    invoke-direct/range {p0 .. p8}, Lcom/squareup/ui/settings/paymentdevices/ReaderState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    iput p9, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->currentPercentComplete:I

    .line 238
    iput p10, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->totalPercentComplete:I

    return-void
.end method
