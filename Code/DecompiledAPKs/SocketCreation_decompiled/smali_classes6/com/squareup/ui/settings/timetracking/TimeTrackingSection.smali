.class public Lcom/squareup/ui/settings/timetracking/TimeTrackingSection;
.super Lcom/squareup/applet/AppletSection;
.source "TimeTrackingSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/timetracking/TimeTrackingSection$Access;,
        Lcom/squareup/ui/settings/timetracking/TimeTrackingSection$ListEntry;
    }
.end annotation


# static fields
.field public static TITLE_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    sget v0, Lcom/squareup/settingsapplet/R$string;->time_tracking_settings_section_title:I

    sput v0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSection;->TITLE_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/EmployeeManagementSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSection$Access;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/settings/timetracking/TimeTrackingSection$Access;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/EmployeeManagementSettings;)V

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/ui/settings/timetracking/TimeTrackingSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 35
    sget-object v0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen;

    return-object v0
.end method
