.class public Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "PredefinedTicketsOptInScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen$Component;,
        Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen;->INSTANCE:Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen;

    .line 76
    sget-object v0, Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen;->INSTANCE:Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen;

    .line 77
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 38
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_USE_PREDEFINED_TICKETS:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 34
    const-class v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 80
    sget v0, Lcom/squareup/settingsapplet/R$layout;->predefined_tickets_opt_in_view:I

    return v0
.end method
