.class public interface abstract Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Component;
.super Ljava/lang/Object;
.source "ConfirmBusinessAddressDialogScreen.kt"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Component;",
        "",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "merchantProfileRunner",
        "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract analytics()Lcom/squareup/analytics/Analytics;
.end method

.method public abstract merchantProfileRunner()Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;
.end method
