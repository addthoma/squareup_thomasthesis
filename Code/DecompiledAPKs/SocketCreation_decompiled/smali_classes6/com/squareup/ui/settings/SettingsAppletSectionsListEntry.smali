.class public Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;
.super Lcom/squareup/applet/AppletSectionsListEntry;
.source "SettingsAppletSectionsListEntry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;,
        Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;
    }
.end annotation


# instance fields
.field private final device:Lcom/squareup/util/Device;

.field private final grouping:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;


# direct methods
.method public constructor <init>(Lcom/squareup/applet/AppletSection;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;ILcom/squareup/util/Res;Lcom/squareup/util/Device;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p3, p4}, Lcom/squareup/applet/AppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V

    .line 43
    iput-object p2, p0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;->grouping:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;

    .line 44
    iput-object p5, p0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;->device:Lcom/squareup/util/Device;

    return-void
.end method


# virtual methods
.method public getGroupingTitle()Ljava/lang/String;
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;->grouping:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;

    invoke-interface {v1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;->getStringResId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShortValueText()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getValueText()Ljava/lang/String;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;->getValueTextForList()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 55
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;->getValueTextForSidebar()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValueTextForList()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getValueTextForSidebar()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public hasBadgeValue()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected logClickEvent()V
    .locals 0

    return-void
.end method
