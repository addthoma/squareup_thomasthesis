.class public Lcom/squareup/ui/settings/cashmanagement/CloseDrawerConfirmationDialog$Factory;
.super Ljava/lang/Object;
.source "CloseDrawerConfirmationDialog.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/cashmanagement/CloseDrawerConfirmationDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;Landroid/content/DialogInterface;I)V
    .locals 0

    const/4 p1, 0x1

    .line 39
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->closeConfirmationDialog(Z)V

    return-void
.end method

.method static synthetic lambda$create$1(Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;Landroid/content/DialogInterface;I)V
    .locals 0

    const/4 p1, 0x0

    .line 41
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->closeConfirmationDialog(Z)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 32
    const-class v0, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;

    .line 33
    invoke-interface {v0}, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;->cashManagementSectionController()Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;

    move-result-object v0

    .line 35
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/settingsapplet/R$string;->cash_management_unclosed_drawer_title:I

    .line 36
    invoke-virtual {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/settingsapplet/R$string;->cash_management_unclosed_drawer_message:I

    .line 37
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/common/strings/R$string;->ok:I

    new-instance v2, Lcom/squareup/ui/settings/cashmanagement/-$$Lambda$CloseDrawerConfirmationDialog$Factory$5jBa4MdpBOGU5KD5q-LBNtlDq0s;

    invoke-direct {v2, v0}, Lcom/squareup/ui/settings/cashmanagement/-$$Lambda$CloseDrawerConfirmationDialog$Factory$5jBa4MdpBOGU5KD5q-LBNtlDq0s;-><init>(Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;)V

    .line 38
    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/common/strings/R$string;->cancel:I

    new-instance v2, Lcom/squareup/ui/settings/cashmanagement/-$$Lambda$CloseDrawerConfirmationDialog$Factory$Gzy3i0OBr6opUDRbpXJfw_NP5Qs;

    invoke-direct {v2, v0}, Lcom/squareup/ui/settings/cashmanagement/-$$Lambda$CloseDrawerConfirmationDialog$Factory$Gzy3i0OBr6opUDRbpXJfw_NP5Qs;-><init>(Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;)V

    .line 40
    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 42
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 43
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 35
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
