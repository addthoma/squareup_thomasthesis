.class public final Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;
.super Ljava/lang/Object;
.source "BankAccountSettingsScopeRunner.kt"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/banklinking/LinkBankAccountWorkflowRunner$LinkBankAccountWorkflowResultHandler;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBankAccountSettingsScopeRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BankAccountSettingsScopeRunner.kt\ncom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner\n*L\n1#1,262:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00b4\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u0002Ba\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0008\u0008\u0001\u0010\u0017\u001a\u00020\u0018\u00a2\u0006\u0002\u0010\u0019J\u0006\u0010\u001c\u001a\u00020\u001dJ\u000c\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020 0\u001fJ\u000c\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\"0\u001fJ\u0006\u0010#\u001a\u00020\u001dJ\u0006\u0010$\u001a\u00020\u001dJ\u000e\u0010%\u001a\u00020\u001d2\u0006\u0010&\u001a\u00020\'J\u0008\u0010(\u001a\u00020)H\u0002J&\u0010*\u001a\u0004\u0018\u00010+2\u0008\u0010,\u001a\u0004\u0018\u00010-2\u0006\u0010.\u001a\u00020/2\u0008\u0008\u0001\u00100\u001a\u000201H\u0002J\u0006\u00102\u001a\u00020\u001dJ\u0008\u00103\u001a\u00020\u001dH\u0002J\u0006\u00104\u001a\u00020\u001dJ\u0006\u00105\u001a\u00020\u001dJ\u0010\u00106\u001a\u00020\u001d2\u0006\u00107\u001a\u000208H\u0002J\u0010\u00109\u001a\u00020\u001d2\u0006\u0010:\u001a\u00020;H\u0016J\u0008\u0010<\u001a\u00020\u001dH\u0016J\u0008\u0010=\u001a\u00020\u001dH\u0016J\u0006\u0010>\u001a\u00020\u001dJ\u0006\u0010?\u001a\u00020\u001dJ\u0010\u0010@\u001a\u00020\u001d2\u0006\u0010A\u001a\u00020BH\u0002J\u0010\u0010@\u001a\u00020\u001d2\u0006\u0010C\u001a\u00020DH\u0002J\u0010\u0010E\u001a\u00020\u001d2\u0006\u00107\u001a\u000208H\u0002J\u0010\u0010F\u001a\u00020\"2\u0006\u00107\u001a\u000208H\u0002J\u0008\u0010G\u001a\u00020)H\u0002R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006H"
    }
    d2 = {
        "Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;",
        "Lmortar/Scoped;",
        "Lcom/squareup/banklinking/LinkBankAccountWorkflowRunner$LinkBankAccountWorkflowResultHandler;",
        "res",
        "Lcom/squareup/util/Res;",
        "flow",
        "Lflow/Flow;",
        "device",
        "Lcom/squareup/util/Device;",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "bankAccountSettings",
        "Lcom/squareup/banklinking/BankAccountSettings;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "application",
        "Landroid/app/Application;",
        "browserLauncher",
        "Lcom/squareup/util/BrowserLauncher;",
        "bankLinkingStarter",
        "Lcom/squareup/banklinking/BankLinkingStarter;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "shortDateFormatter",
        "Ljava/text/DateFormat;",
        "(Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/CountryCode;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/settings/server/Features;Landroid/app/Application;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/banklinking/BankLinkingStarter;Lcom/squareup/analytics/Analytics;Ljava/text/DateFormat;)V",
        "subs",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "addBankAccount",
        "",
        "bankAccountCancelVerificationScreenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog$ScreenData;",
        "bankAccountSettingsScreenData",
        "Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;",
        "cancelVerification",
        "changeBankAccount",
        "confirmBankAccount",
        "confirmationToken",
        "",
        "depositOptionsHintText",
        "",
        "formatBankAccount",
        "Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;",
        "bankAccount",
        "Lcom/squareup/protos/client/bankaccount/BankAccount;",
        "showAccountType",
        "",
        "titleId",
        "",
        "goBackFromBankAccountSettingsScreen",
        "goToBankAccountPasswordCheckOrDetailScreen",
        "goToBankAccountResendEmailScreen",
        "logCancelVerificationDismissEvent",
        "onCancelVerificationOrConfirmBankAccount",
        "state",
        "Lcom/squareup/banklinking/BankAccountSettings$State;",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onLinkBankAccountResult",
        "refresh",
        "showCancelVerificationDialog",
        "showWarningDialog",
        "failureMessage",
        "Lcom/squareup/receiving/FailureMessage;",
        "ids",
        "Lcom/squareup/widgets/warning/WarningIds;",
        "toBankAccountResendEmailScreenData",
        "toBankAccountSettingsScreenData",
        "verificationHintText",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final application:Landroid/app/Application;

.field private final bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

.field private final bankLinkingStarter:Lcom/squareup/banklinking/BankLinkingStarter;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final countryCode:Lcom/squareup/CountryCode;

.field private final device:Lcom/squareup/util/Device;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final res:Lcom/squareup/util/Res;

.field private final shortDateFormatter:Ljava/text/DateFormat;

.field private final subs:Lio/reactivex/disposables/CompositeDisposable;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/CountryCode;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/settings/server/Features;Landroid/app/Application;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/banklinking/BankLinkingStarter;Lcom/squareup/analytics/Analytics;Ljava/text/DateFormat;)V
    .locals 1
    .param p11    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/ShortForm;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countryCode"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bankAccountSettings"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "application"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "browserLauncher"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bankLinkingStarter"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "shortDateFormatter"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->flow:Lflow/Flow;

    iput-object p3, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->device:Lcom/squareup/util/Device;

    iput-object p4, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->countryCode:Lcom/squareup/CountryCode;

    iput-object p5, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    iput-object p6, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->features:Lcom/squareup/settings/server/Features;

    iput-object p7, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->application:Landroid/app/Application;

    iput-object p8, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iput-object p9, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->bankLinkingStarter:Lcom/squareup/banklinking/BankLinkingStarter;

    iput-object p10, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p11, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->shortDateFormatter:Ljava/text/DateFormat;

    .line 70
    new-instance p1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    return-void
.end method

.method public static final synthetic access$getFlow$p(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;)Lflow/Flow;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->flow:Lflow/Flow;

    return-object p0
.end method

.method public static final synthetic access$onCancelVerificationOrConfirmBankAccount(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;Lcom/squareup/banklinking/BankAccountSettings$State;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->onCancelVerificationOrConfirmBankAccount(Lcom/squareup/banklinking/BankAccountSettings$State;)V

    return-void
.end method

.method public static final synthetic access$toBankAccountResendEmailScreenData(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;Lcom/squareup/banklinking/BankAccountSettings$State;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->toBankAccountResendEmailScreenData(Lcom/squareup/banklinking/BankAccountSettings$State;)V

    return-void
.end method

.method public static final synthetic access$toBankAccountSettingsScreenData(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;Lcom/squareup/banklinking/BankAccountSettings$State;)Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;
    .locals 0

    .line 55
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->toBankAccountSettingsScreenData(Lcom/squareup/banklinking/BankAccountSettings$State;)Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;

    move-result-object p0

    return-object p0
.end method

.method private final depositOptionsHintText()Ljava/lang/CharSequence;
    .locals 3

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->countryCode:Lcom/squareup/CountryCode;

    sget-object v1, Lcom/squareup/CountryCode;->JP:Lcom/squareup/CountryCode;

    if-ne v0, v1, :cond_0

    .line 235
    sget v0, Lcom/squareup/settingsapplet/R$string;->deposit_options_hint_jp:I

    goto :goto_0

    .line 237
    :cond_0
    sget v0, Lcom/squareup/settingsapplet/R$string;->deposit_options_hint:I

    .line 240
    :goto_0
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    iget-object v2, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->application:Landroid/app/Application;

    check-cast v2, Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    const-string v2, "learn_more"

    .line 241
    invoke-virtual {v1, v0, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 242
    sget v1, Lcom/squareup/registerlib/R$string;->deposit_options_url:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 243
    sget v1, Lcom/squareup/settingsapplet/R$string;->learn_more_about_deposits:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 244
    sget v1, Lcom/squareup/noho/R$color;->noho_text_button_link_enabled:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->linkColor(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 245
    invoke-virtual {v0}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "LinkSpan.Builder(applica\u2026        .asCharSequence()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final formatBankAccount(Lcom/squareup/protos/client/bankaccount/BankAccount;ZI)Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;
    .locals 8

    if-eqz p1, :cond_0

    .line 164
    new-instance v7, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;

    iget-object v1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->res:Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->shortDateFormatter:Ljava/text/DateFormat;

    iget-object v3, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->countryCode:Lcom/squareup/CountryCode;

    move-object v0, v7

    move-object v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;-><init>(Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/CountryCode;Lcom/squareup/protos/client/bankaccount/BankAccount;ZI)V

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    :goto_0
    return-object v7
.end method

.method private final goToBankAccountPasswordCheckOrDetailScreen()V
    .locals 3

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    invoke-interface {v1}, Lcom/squareup/banklinking/BankAccountSettings;->state()Lio/reactivex/Observable;

    move-result-object v1

    .line 250
    invoke-virtual {v1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v1

    .line 251
    new-instance v2, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$goToBankAccountPasswordCheckOrDetailScreen$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$goToBankAccountPasswordCheckOrDetailScreen$1;-><init>(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    const-string v2, "bankAccountSettings.stat\u2026  )\n          )\n        }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 249
    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method private final onCancelVerificationOrConfirmBankAccount(Lcom/squareup/banklinking/BankAccountSettings$State;)V
    .locals 2

    .line 216
    iget-object v0, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->cancelVerificationState:Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;

    sget-object v1, Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;->FAILURE:Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;

    if-eq v0, v1, :cond_0

    iget-object v0, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->confirmBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;

    sget-object v1, Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;->FAILURE:Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;

    if-ne v0, v1, :cond_2

    .line 218
    :cond_0
    iget-object p1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    if-nez p1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->showWarningDialog(Lcom/squareup/receiving/FailureMessage;)V

    :cond_2
    return-void
.end method

.method private final showWarningDialog(Lcom/squareup/receiving/FailureMessage;)V
    .locals 2

    .line 123
    new-instance v0, Lcom/squareup/widgets/warning/WarningStrings;

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iget-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    check-cast v0, Lcom/squareup/widgets/warning/Warning;

    invoke-direct {v1, v0}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {p1, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final showWarningDialog(Lcom/squareup/widgets/warning/WarningIds;)V
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    check-cast p1, Lcom/squareup/widgets/warning/Warning;

    invoke-direct {v1, p1}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final toBankAccountResendEmailScreenData(Lcom/squareup/banklinking/BankAccountSettings$State;)V
    .locals 3

    .line 206
    iget-object v0, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->resendEmailState:Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;

    sget-object v1, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 210
    iget-object p1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->showWarningDialog(Lcom/squareup/receiving/FailureMessage;)V

    goto :goto_0

    .line 211
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->resendEmailState:Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 208
    :cond_2
    new-instance p1, Lcom/squareup/widgets/warning/WarningIds;

    sget v0, Lcom/squareup/settingsapplet/R$string;->email_resent_title:I

    sget v1, Lcom/squareup/settingsapplet/R$string;->email_resent_body:I

    invoke-direct {p1, v0, v1}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    .line 207
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->showWarningDialog(Lcom/squareup/widgets/warning/WarningIds;)V

    :goto_0
    return-void
.end method

.method private final toBankAccountSettingsScreenData(Lcom/squareup/banklinking/BankAccountSettings$State;)Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;
    .locals 19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 169
    iget-object v2, v1, Lcom/squareup/banklinking/BankAccountSettings$State;->activeBankAccount:Lcom/squareup/protos/client/bankaccount/BankAccount;

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/squareup/banklinking/BankAccountSettings$State;->pendingBankAccount:Lcom/squareup/protos/client/bankaccount/BankAccount;

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_1

    .line 172
    new-instance v2, Lkotlin/Triple;

    .line 173
    iget-object v5, v1, Lcom/squareup/banklinking/BankAccountSettings$State;->pendingBankAccount:Lcom/squareup/protos/client/bankaccount/BankAccount;

    .line 174
    iget-object v6, v1, Lcom/squareup/banklinking/BankAccountSettings$State;->activeBankAccount:Lcom/squareup/protos/client/bankaccount/BankAccount;

    .line 175
    sget v7, Lcom/squareup/settingsapplet/R$string;->pending_bank_account_uppercase:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 172
    invoke-direct {v2, v5, v6, v7}, Lkotlin/Triple;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 178
    :cond_1
    new-instance v2, Lkotlin/Triple;

    .line 179
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/banklinking/BankAccountSettings$State;->bankAccount()Lcom/squareup/protos/client/bankaccount/BankAccount;

    move-result-object v5

    const/4 v6, 0x0

    .line 181
    sget v7, Lcom/squareup/settingsapplet/R$string;->linked_bank_account_uppercase:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 178
    invoke-direct {v2, v5, v6, v7}, Lkotlin/Triple;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 171
    :goto_1
    invoke-virtual {v2}, Lkotlin/Triple;->component1()Ljava/lang/Object;

    move-result-object v5

    .line 170
    check-cast v5, Lcom/squareup/protos/client/bankaccount/BankAccount;

    invoke-virtual {v2}, Lkotlin/Triple;->component2()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/protos/client/bankaccount/BankAccount;

    invoke-virtual {v2}, Lkotlin/Triple;->component3()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    .line 184
    iget-object v7, v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v8, Lcom/squareup/settings/server/Features$Feature;->BANK_LINKING_WITH_TWO_ACCOUNTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v7, v8}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v7

    xor-int/2addr v7, v4

    .line 185
    new-instance v18, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;

    .line 186
    iget-object v11, v1, Lcom/squareup/banklinking/BankAccountSettings$State;->latestBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    .line 187
    invoke-direct {v0, v5, v7, v2}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->formatBankAccount(Lcom/squareup/protos/client/bankaccount/BankAccount;ZI)Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;

    move-result-object v9

    .line 195
    sget v2, Lcom/squareup/settingsapplet/R$string;->active_bank_account_uppercase:I

    .line 192
    invoke-direct {v0, v6, v7, v2}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->formatBankAccount(Lcom/squareup/protos/client/bankaccount/BankAccount;ZI)Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;

    move-result-object v10

    .line 197
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/banklinking/BankAccountSettings$State;->canBeCanceled()Z

    move-result v12

    .line 198
    iget-object v2, v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v5, Lcom/squareup/settings/server/Features$Feature;->BANK_RESEND_EMAIL:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v5}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/banklinking/BankAccountSettings$State;->canResendEmail()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v13, 0x1

    goto :goto_2

    :cond_2
    const/4 v13, 0x0

    .line 199
    :goto_2
    iget-object v14, v1, Lcom/squareup/banklinking/BankAccountSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    .line 200
    iget-object v1, v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v15

    .line 201
    invoke-direct/range {p0 .. p0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->verificationHintText()Ljava/lang/CharSequence;

    move-result-object v16

    .line 202
    invoke-direct/range {p0 .. p0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->depositOptionsHintText()Ljava/lang/CharSequence;

    move-result-object v17

    move-object/from16 v8, v18

    .line 185
    invoke-direct/range {v8 .. v17}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;-><init>(Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;ZZLcom/squareup/receiving/FailureMessage;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-object v18
.end method

.method private final verificationHintText()Ljava/lang/CharSequence;
    .locals 2

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->countryCode:Lcom/squareup/CountryCode;

    sget-object v1, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 227
    sget v0, Lcom/squareup/settingsapplet/R$string;->verification_hint:I

    goto :goto_0

    .line 226
    :cond_0
    sget v0, Lcom/squareup/settingsapplet/R$string;->verification_hint_jp:I

    goto :goto_0

    .line 225
    :cond_1
    sget v0, Lcom/squareup/settingsapplet/R$string;->verification_hint_ca_au:I

    .line 230
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, v0}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final addBankAccount()V
    .locals 3

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Settings Bank Account: Add Bank Account"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->countryCode:Lcom/squareup/CountryCode;

    sget-object v1, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    if-ne v0, v1, :cond_0

    .line 139
    invoke-direct {p0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->goToBankAccountPasswordCheckOrDetailScreen()V

    goto :goto_0

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->bankLinkingStarter:Lcom/squareup/banklinking/BankLinkingStarter;

    invoke-interface {v0}, Lcom/squareup/banklinking/BankLinkingStarter;->maybeStartBankLinking()V

    :goto_0
    return-void
.end method

.method public final bankAccountCancelVerificationScreenData()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog$ScreenData;",
            ">;"
        }
    .end annotation

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    invoke-interface {v0}, Lcom/squareup/banklinking/BankAccountSettings;->state()Lio/reactivex/Observable;

    move-result-object v0

    .line 99
    sget-object v1, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$bankAccountCancelVerificationScreenData$1;->INSTANCE:Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$bankAccountCancelVerificationScreenData$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "bankAccountSettings.stat\u2026_number_suffix)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final bankAccountSettingsScreenData()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    invoke-interface {v0}, Lcom/squareup/banklinking/BankAccountSettings;->state()Lio/reactivex/Observable;

    move-result-object v0

    .line 93
    new-instance v1, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$bankAccountSettingsScreenData$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$bankAccountSettingsScreenData$1;-><init>(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunnerKt$sam$io_reactivex_functions_Function$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunnerKt$sam$io_reactivex_functions_Function$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "bankAccountSettings.stat\u2026ccountSettingsScreenData)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final cancelVerification()V
    .locals 3

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Settings Bank Account: Cancel Verification Confirm"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    invoke-interface {v1}, Lcom/squareup/banklinking/BankAccountSettings;->cancelVerification()Lio/reactivex/Single;

    move-result-object v1

    .line 112
    invoke-virtual {v1}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    const-string v2, "bankAccountSettings.canc\u2026on()\n        .subscribe()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public final changeBankAccount()V
    .locals 3

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Settings Bank Account: Edit Bank Account"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BANK_LINK_IN_APP:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    invoke-direct {p0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->goToBankAccountPasswordCheckOrDetailScreen()V

    goto :goto_0

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    .line 151
    iget-object v1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/banklinking/R$string;->link_bank_account_url:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 150
    invoke-interface {v0, v1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public final confirmBankAccount(Ljava/lang/String;)V
    .locals 2

    const-string v0, "confirmationToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    invoke-interface {v1, p1}, Lcom/squareup/banklinking/BankAccountSettings;->confirmBankAccount(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 106
    invoke-virtual {p1}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string v1, "bankAccountSettings.conf\u2026ken)\n        .subscribe()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-static {v0, p1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public final goBackFromBankAccountSettingsScreen()V
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public final goToBankAccountResendEmailScreen()V
    .locals 4

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    invoke-interface {v1}, Lcom/squareup/banklinking/BankAccountSettings;->resendVerificationEmail()Lio/reactivex/Single;

    move-result-object v1

    .line 129
    new-instance v2, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$goToBankAccountResendEmailScreen$1;

    move-object v3, p0

    check-cast v3, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;

    invoke-direct {v2, v3}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$goToBankAccountResendEmailScreen$1;-><init>(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    new-instance v3, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunnerKt$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v3, v2}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunnerKt$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v3}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    const-string v2, "bankAccountSettings.rese\u2026untResendEmailScreenData)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public final logCancelVerificationDismissEvent()V
    .locals 3

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Settings Bank Account: Cancel Verification Dismiss"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-virtual {p0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->refresh()V

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    invoke-interface {v0}, Lcom/squareup/banklinking/BankAccountSettings;->state()Lio/reactivex/Observable;

    move-result-object v0

    .line 76
    new-instance v1, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$onEnterScope$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunnerKt$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunnerKt$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "bankAccountSettings.stat\u2026tionOrConfirmBankAccount)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method

.method public onLinkBankAccountResult()V
    .locals 0

    return-void
.end method

.method public final refresh()V
    .locals 3

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    invoke-interface {v1}, Lcom/squareup/banklinking/BankAccountSettings;->refresh()Lio/reactivex/Single;

    move-result-object v1

    .line 88
    invoke-virtual {v1}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    const-string v2, "bankAccountSettings.refresh()\n        .subscribe()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public final showCancelVerificationDialog()V
    .locals 3

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Settings Bank Account: Cancel Verification"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog;->INSTANCE:Lcom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
