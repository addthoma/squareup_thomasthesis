.class public Lcom/squareup/ui/settings/taxes/tax/RefreshFeesOnEnterScope;
.super Ljava/lang/Object;
.source "RefreshFeesOnEnterScope.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final feesEditor:Lcom/squareup/settings/server/FeesEditor;

.field private final sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/FeesEditor;Lcom/squareup/ui/settings/SidebarRefresher;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/RefreshFeesOnEnterScope;->feesEditor:Lcom/squareup/settings/server/FeesEditor;

    .line 18
    iput-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/RefreshFeesOnEnterScope;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 23
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/RefreshFeesOnEnterScope;->feesEditor:Lcom/squareup/settings/server/FeesEditor;

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/RefreshFeesOnEnterScope;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$XJb37g57Ri_KHq04MKnmfoF_SsE;

    invoke-direct {v1, v0}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$XJb37g57Ri_KHq04MKnmfoF_SsE;-><init>(Lcom/squareup/ui/settings/SidebarRefresher;)V

    invoke-interface {p1, v1}, Lcom/squareup/settings/server/FeesEditor;->read(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
