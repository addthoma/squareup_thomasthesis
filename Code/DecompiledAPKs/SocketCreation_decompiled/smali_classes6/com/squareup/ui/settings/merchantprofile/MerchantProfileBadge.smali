.class public final Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge;
.super Ljava/lang/Object;
.source "MerchantProfileBadge.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u000bJ\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0008\u0010\u0010\u001a\u00020\rH\u0016J\u0008\u0010\u0011\u001a\u00020\u0012H\u0002R\u001c\u0010\u0007\u001a\u0010\u0012\u000c\u0012\n \n*\u0004\u0018\u00010\t0\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge;",
        "Lmortar/Scoped;",
        "monitor",
        "Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;",
        "publicProfileSection",
        "Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;",
        "(Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;)V",
        "badgeCount",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "Lio/reactivex/Observable;",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "sectionIsVisible",
        "",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final badgeCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final monitor:Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

.field private final publicProfileSection:Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;


# direct methods
.method public constructor <init>(Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "monitor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "publicProfileSection"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge;->monitor:Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

    iput-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge;->publicProfileSection:Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;

    const/4 p1, 0x0

    .line 21
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefault(0)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge;->badgeCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$getBadgeCount$p(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge;->badgeCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method private final sectionIsVisible()Z
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge;->publicProfileSection:Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final badgeCount()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge;->badgeCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge;->sectionIsVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge;->monitor:Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

    invoke-interface {v0}, Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;->addressRequiresValidation()Lio/reactivex/Observable;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 32
    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge$onEnterScope$1;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "monitor.addressRequiresV\u2026accept(0)\n              }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    :cond_0
    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
