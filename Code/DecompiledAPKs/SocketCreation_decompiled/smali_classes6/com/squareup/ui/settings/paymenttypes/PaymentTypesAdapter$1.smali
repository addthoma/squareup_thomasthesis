.class Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;
.super Landroidx/recyclerview/widget/ItemTouchHelper$SimpleCallback;
.source "PaymentTypesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;II)V
    .locals 0

    .line 73
    iput-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;->this$0:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;

    invoke-direct {p0, p2, p3}, Landroidx/recyclerview/widget/ItemTouchHelper$SimpleCallback;-><init>(II)V

    return-void
.end method


# virtual methods
.method public onMove(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)Z
    .locals 4

    .line 78
    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result p1

    .line 79
    invoke-virtual {p3}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result p2

    const/4 p3, 0x0

    if-eq p1, p2, :cond_4

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;->this$0:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    if-nez v0, :cond_0

    goto/16 :goto_1

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;->this$0:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 86
    iget-object v1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;->this$0:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;

    invoke-virtual {v1, p2}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .line 87
    instance-of v2, v1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    .line 89
    check-cast v1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;->this$0:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;

    invoke-static {p1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->access$100(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;)Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Listener;

    move-result-object p1

    new-instance p2, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;

    iget-object v1, v1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;->category:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-direct {p2, v1, p3}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;-><init>(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;I)V

    iget-object p3, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;->this$0:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;

    .line 91
    invoke-static {p3}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->access$000(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;)Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p3

    invoke-static {v0, p3}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderIndex(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;

    move-result-object p3

    .line 90
    invoke-interface {p1, v0, p2, p3}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Listener;->onTenderMovedToPosition(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;)V

    :goto_0
    const/4 p3, 0x1

    goto/16 :goto_1

    .line 93
    :cond_1
    instance-of v2, v1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Header;

    if-eqz v2, :cond_3

    .line 94
    check-cast v1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Header;

    if-ge p1, p2, :cond_2

    .line 97
    iget-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;->this$0:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;

    invoke-static {p1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->access$100(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;)Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Listener;

    move-result-object p1

    new-instance p2, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;

    iget-object v1, v1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Header;->nextCat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-direct {p2, v1, p3}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;-><init>(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;I)V

    iget-object p3, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;->this$0:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;

    .line 98
    invoke-static {p3}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->access$000(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;)Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p3

    invoke-static {v0, p3}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderIndex(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;

    move-result-object p3

    .line 97
    invoke-interface {p1, v0, p2, p3}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Listener;->onTenderMovedToPosition(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;)V

    goto :goto_0

    :cond_2
    if-le p1, p2, :cond_4

    .line 102
    iget-object p1, v1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Header;->prevCat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    if-eqz p1, :cond_4

    .line 103
    iget-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;->this$0:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;

    invoke-static {p1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->access$100(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;)Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Listener;

    move-result-object p1

    new-instance p2, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;

    iget-object p3, v1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Header;->prevCat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    iget-object v2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;->this$0:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;

    .line 104
    invoke-static {v2}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->access$000(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;)Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object v2

    iget-object v1, v1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Header;->prevCat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-static {v2, v1}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderForCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {p2, p3, v1}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;-><init>(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;I)V

    iget-object p3, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;->this$0:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;

    .line 105
    invoke-static {p3}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->access$000(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;)Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p3

    invoke-static {v0, p3}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderIndex(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;

    move-result-object p3

    .line 103
    invoke-interface {p1, v0, p2, p3}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Listener;->onTenderMovedToPosition(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;)V

    goto :goto_0

    .line 109
    :cond_3
    instance-of p1, v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    if-eqz p1, :cond_4

    .line 111
    check-cast v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 112
    iget-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;->this$0:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;

    invoke-static {p1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->access$100(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;)Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Listener;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;->this$0:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;

    invoke-static {p2}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->access$000(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;)Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p2

    invoke-static {v1, p2}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderIndex(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;->this$0:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;

    .line 113
    invoke-static {p3}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->access$000(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;)Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p3

    invoke-static {v0, p3}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderIndex(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;

    move-result-object p3

    .line 112
    invoke-interface {p1, v0, p2, p3}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Listener;->onTenderMovedToPosition(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;)V

    goto :goto_0

    :cond_4
    :goto_1
    return p3
.end method

.method public onSwiped(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 121
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "PaymentTypesAdapter only supports drag."

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
