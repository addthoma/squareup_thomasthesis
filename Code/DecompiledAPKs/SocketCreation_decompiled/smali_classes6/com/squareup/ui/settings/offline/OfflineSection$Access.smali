.class public final Lcom/squareup/ui/settings/offline/OfflineSection$Access;
.super Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;
.source "OfflineSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/offline/OfflineSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Access"
.end annotation


# instance fields
.field private final offlineModeToggleIsDisplayable:Lcom/squareup/payment/offline/OfflineModeCanBeOffered;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/payment/offline/OfflineModeCanBeOffered;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/permissions/Permission;

    .line 54
    sget-object v1, Lcom/squareup/permissions/Permission;->CHANGE_OFFLINE_MODE_SETTING:Lcom/squareup/permissions/Permission;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;-><init>(Lcom/squareup/settings/server/Features;[Lcom/squareup/permissions/Permission;)V

    .line 55
    iput-object p2, p0, Lcom/squareup/ui/settings/offline/OfflineSection$Access;->offlineModeToggleIsDisplayable:Lcom/squareup/payment/offline/OfflineModeCanBeOffered;

    return-void
.end method


# virtual methods
.method public determineVisibility()Z
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/settings/offline/OfflineSection$Access;->offlineModeToggleIsDisplayable:Lcom/squareup/payment/offline/OfflineModeCanBeOffered;

    invoke-interface {v0}, Lcom/squareup/payment/offline/OfflineModeCanBeOffered;->value()Z

    move-result v0

    return v0
.end method
