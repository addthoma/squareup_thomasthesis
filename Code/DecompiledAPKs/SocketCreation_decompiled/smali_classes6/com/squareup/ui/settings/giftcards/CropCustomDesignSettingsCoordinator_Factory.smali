.class public final Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator_Factory;
.super Ljava/lang/Object;
.source "CropCustomDesignSettingsCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final picassoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator_Factory;->picassoProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;)",
            "Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;Lcom/squareup/picasso/Picasso;)Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;-><init>(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;Lcom/squareup/picasso/Picasso;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;

    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator_Factory;->picassoProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/picasso/Picasso;

    invoke-static {v0, v1}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator_Factory;->newInstance(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;Lcom/squareup/picasso/Picasso;)Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator_Factory;->get()Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;

    move-result-object v0

    return-object v0
.end method
