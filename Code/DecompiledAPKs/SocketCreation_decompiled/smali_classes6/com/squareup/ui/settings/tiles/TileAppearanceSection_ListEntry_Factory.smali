.class public final Lcom/squareup/ui/settings/tiles/TileAppearanceSection_ListEntry_Factory;
.super Ljava/lang/Object;
.source "TileAppearanceSection_ListEntry_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final sectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSection;",
            ">;"
        }
    .end annotation
.end field

.field private final tileSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSection_ListEntry_Factory;->sectionProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSection_ListEntry_Factory;->resProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSection_ListEntry_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p4, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSection_ListEntry_Factory;->tileSettingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/tiles/TileAppearanceSection_ListEntry_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;)",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSection_ListEntry_Factory;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/ui/settings/tiles/TileAppearanceSection_ListEntry_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/settings/tiles/TileAppearanceSection_ListEntry_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/settings/tiles/TileAppearanceSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;
    .locals 1

    .line 48
    new-instance v0, Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;-><init>(Lcom/squareup/ui/settings/tiles/TileAppearanceSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;
    .locals 4

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSection_ListEntry_Factory;->sectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/tiles/TileAppearanceSection;

    iget-object v1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSection_ListEntry_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSection_ListEntry_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Device;

    iget-object v3, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSection_ListEntry_Factory;->tileSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/settings/tiles/TileAppearanceSection_ListEntry_Factory;->newInstance(Lcom/squareup/ui/settings/tiles/TileAppearanceSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSection_ListEntry_Factory;->get()Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;

    move-result-object v0

    return-object v0
.end method
