.class public Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsCardPresenter;
.source "StoreAndForwardSettingsEnableScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsCardPresenter<",
        "Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/StoreAndForwardAnalytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 49
    invoke-direct {p0, p2, p1}, Lcom/squareup/ui/settings/SettingsCardPresenter;-><init>(Lcom/squareup/util/Device;Lflow/Flow;)V

    .line 50
    iput-object p3, p0, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 51
    iput-object p4, p0, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 52
    iput-object p5, p0, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Presenter;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    return-void
.end method


# virtual methods
.method public enableOfflinePayments()V
    .locals 2

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Presenter;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    sget-object v1, Lcom/squareup/analytics/StoreAndForwardAnalytics$State;->ENABLED:Lcom/squareup/analytics/StoreAndForwardAnalytics$State;

    invoke-virtual {v0, v1}, Lcom/squareup/analytics/StoreAndForwardAnalytics;->logSettingState(Lcom/squareup/analytics/StoreAndForwardAnalytics$State;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStoreAndForwardSettings()Lcom/squareup/settings/server/StoreAndForwardSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/settings/server/StoreAndForwardSettings;->setEnabled(Ljava/lang/Boolean;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Presenter;->screenForAssertion()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->closeCard(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/registerlib/R$string;->allow_offline_mode:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 56
    const-class v0, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen;

    return-object v0
.end method
