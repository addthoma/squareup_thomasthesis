.class public Lcom/squareup/ui/settings/printerstations/station/ConfirmNameOptionDialogScreen$Factory;
.super Ljava/lang/Object;
.source "ConfirmNameOptionDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/printerstations/station/ConfirmNameOptionDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;Landroid/content/DialogInterface;I)V
    .locals 0

    const/4 p1, 0x1

    .line 42
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;->onDialogResponse(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic lambda$create$1(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;Landroid/content/DialogInterface;I)V
    .locals 0

    const/4 p1, 0x0

    .line 43
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;->onDialogResponse(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic lambda$create$2(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x0

    .line 45
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;->onDialogResponse(Ljava/lang/Boolean;)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 32
    invoke-static {p1}, Lcom/squareup/container/ContainerTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/ConfirmNameOptionDialogScreen;

    .line 34
    const-class v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Component;

    .line 35
    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Component;

    .line 36
    invoke-interface {v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Component;->printerStationScopeRunner()Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;

    move-result-object v1

    .line 38
    invoke-static {v0}, Lcom/squareup/ui/settings/printerstations/station/ConfirmNameOptionDialogScreen;->access$000(Lcom/squareup/ui/settings/printerstations/station/ConfirmNameOptionDialogScreen;)Lcom/squareup/register/widgets/Confirmation;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/squareup/register/widgets/Confirmation;->getStrings(Landroid/content/res/Resources;)Lcom/squareup/register/widgets/Confirmation$Strings;

    move-result-object v0

    .line 40
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object p1, v0, Lcom/squareup/register/widgets/Confirmation$Strings;->title:Ljava/lang/String;

    invoke-virtual {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    iget-object v2, v0, Lcom/squareup/register/widgets/Confirmation$Strings;->body:Ljava/lang/String;

    .line 41
    invoke-virtual {p1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    iget-object v2, v0, Lcom/squareup/register/widgets/Confirmation$Strings;->confirm:Ljava/lang/String;

    new-instance v3, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$ConfirmNameOptionDialogScreen$Factory$OFTjwZVRZS2xOSURqqguLpZTyXU;

    invoke-direct {v3, v1}, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$ConfirmNameOptionDialogScreen$Factory$OFTjwZVRZS2xOSURqqguLpZTyXU;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;)V

    .line 42
    invoke-virtual {p1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    iget-object v0, v0, Lcom/squareup/register/widgets/Confirmation$Strings;->cancel:Ljava/lang/String;

    new-instance v2, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$ConfirmNameOptionDialogScreen$Factory$4enRFFHtw411RPCNlAF5h2m9bEI;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$ConfirmNameOptionDialogScreen$Factory$4enRFFHtw411RPCNlAF5h2m9bEI;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;)V

    .line 43
    invoke-virtual {p1, v0, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x1

    .line 44
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$ConfirmNameOptionDialogScreen$Factory$_Kx-Rfs_yS5vm8t_i9W573yq5lo;

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$ConfirmNameOptionDialogScreen$Factory$_Kx-Rfs_yS5vm8t_i9W573yq5lo;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;)V

    .line 45
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 46
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 40
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
