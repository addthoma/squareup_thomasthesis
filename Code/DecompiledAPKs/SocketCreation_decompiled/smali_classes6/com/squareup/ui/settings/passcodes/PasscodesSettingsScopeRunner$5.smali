.class Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner$5;
.super Lcom/squareup/permissions/PermissionGatekeeper$EmployeePasscodeUnlockWhen;
.source "PasscodesSettingsScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onTeamPasscodeSwitchChanged(Ljava/lang/Boolean;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;Lcom/squareup/permissions/Permission;)V
    .locals 0

    .line 258
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner$5;->this$0:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-direct {p0, p2}, Lcom/squareup/permissions/PermissionGatekeeper$EmployeePasscodeUnlockWhen;-><init>(Lcom/squareup/permissions/Permission;)V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 2

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner$5;->this$0:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->access$000(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)Lcom/squareup/permissions/PasscodesSettings;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings;->enableOrDisableTeamPasscode(Ljava/lang/Boolean;)V

    return-void
.end method
