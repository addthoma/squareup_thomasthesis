.class final Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$disableTheme$1;
.super Lkotlin/jvm/internal/Lambda;
.source "GiftCardsSettingsScopeRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->disableTheme(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Lcom/squareup/protos/client/giftcards/EGiftTheme;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;",
        "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nGiftCardsSettingsScopeRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 GiftCardsSettingsScopeRunner.kt\ncom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$disableTheme$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,285:1\n704#2:286\n777#2,2:287\n*E\n*S KotlinDebug\n*F\n+ 1 GiftCardsSettingsScopeRunner.kt\ncom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$disableTheme$1\n*L\n212#1:286\n212#1,2:287\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;",
        "kotlin.jvm.PlatformType",
        "builder",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $enabledThemeTokens:Ljava/util/List;

.field final synthetic $theme:Lcom/squareup/protos/client/giftcards/EGiftTheme;


# direct methods
.method constructor <init>(Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftTheme;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$disableTheme$1;->$enabledThemeTokens:Ljava/util/List;

    iput-object p2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$disableTheme$1;->$theme:Lcom/squareup/protos/client/giftcards/EGiftTheme;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;
    .locals 5

    const-string v0, "builder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$disableTheme$1;->$enabledThemeTokens:Ljava/util/List;

    const-string v1, "enabledThemeTokens"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 286
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 287
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Ljava/lang/String;

    .line 212
    iget-object v4, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$disableTheme$1;->$theme:Lcom/squareup/protos/client/giftcards/EGiftTheme;

    iget-object v4, v4, Lcom/squareup/protos/client/giftcards/EGiftTheme;->read_only_token:Ljava/lang/String;

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 288
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 210
    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->enabled_theme_tokens(Ljava/util/List;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;

    move-result-object p1

    const-string v0, "builder.enabled_theme_to\u2026 theme.read_only_token })"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 47
    check-cast p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$disableTheme$1;->invoke(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;

    move-result-object p1

    return-object p1
.end method
