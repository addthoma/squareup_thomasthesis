.class final Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$marinHeading$$inlined$model$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "TipSettingsView.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/tipping/TipSettingsViewKt;->marinHeading(Lcom/squareup/blueprint/VerticalBlock;Lcom/squareup/resources/TextModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/mosaic/components/LabelUiModel<",
        "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u0008\u0012\u0004\u0012\u00020\u00050\u0004H\n\u00a2\u0006\u0002\u0008\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "P",
        "",
        "Lcom/squareup/mosaic/components/LabelUiModel;",
        "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;",
        "invoke",
        "com/squareup/ui/settings/tipping/TipSettingsViewKt$marinHeading$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $textModel$inlined:Lcom/squareup/resources/TextModel;


# direct methods
.method constructor <init>(Lcom/squareup/resources/TextModel;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$marinHeading$$inlined$model$lambda$1;->$textModel$inlined:Lcom/squareup/resources/TextModel;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/squareup/mosaic/components/LabelUiModel;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$marinHeading$$inlined$model$lambda$1;->invoke(Lcom/squareup/mosaic/components/LabelUiModel;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/mosaic/components/LabelUiModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/components/LabelUiModel<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 310
    sget v0, Lcom/squareup/settingsapplet/R$style;->Widget_TipSettings_Label_Heading:I

    invoke-virtual {p1, v0}, Lcom/squareup/mosaic/components/LabelUiModel;->setStyleRes(I)V

    .line 311
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$marinHeading$$inlined$model$lambda$1;->$textModel$inlined:Lcom/squareup/resources/TextModel;

    invoke-virtual {p1, v0}, Lcom/squareup/mosaic/components/LabelUiModel;->setText(Lcom/squareup/resources/TextModel;)V

    return-void
.end method
