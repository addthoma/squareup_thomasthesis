.class public Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog$Factory;
.super Ljava/lang/Object;
.source "EmailCollectionConfirmationDialog.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog$Controller;Landroid/content/DialogInterface;I)V
    .locals 0

    const/4 p1, 0x1

    .line 45
    invoke-interface {p0, p1}, Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog$Controller;->closeEmailCollectionConfirmationDialog(Z)V

    return-void
.end method

.method static synthetic lambda$create$1(Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog$Controller;Landroid/content/DialogInterface;I)V
    .locals 0

    const/4 p1, 0x0

    .line 47
    invoke-interface {p0, p1}, Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog$Controller;->closeEmailCollectionConfirmationDialog(Z)V

    return-void
.end method

.method static synthetic lambda$create$2(Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog$Controller;Landroid/content/DialogInterface;)V
    .locals 0

    .line 49
    invoke-interface {p0}, Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog$Controller;->closeEmailCollectionConfirmationDialog()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 42
    const-class v0, Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog$Component;

    invoke-interface {v0}, Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog$Component;->controller()Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog$Controller;

    move-result-object v0

    .line 43
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/settingsapplet/R$string;->crm_email_collection_settings_screen_dialog_confirmation:I

    new-instance v2, Lcom/squareup/ui/settings/crm/-$$Lambda$EmailCollectionConfirmationDialog$Factory$6J2_aOx5ZA8liBpwIieSBgkbDkc;

    invoke-direct {v2, v0}, Lcom/squareup/ui/settings/crm/-$$Lambda$EmailCollectionConfirmationDialog$Factory$6J2_aOx5ZA8liBpwIieSBgkbDkc;-><init>(Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog$Controller;)V

    .line 44
    invoke-virtual {v1, p1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/settingsapplet/R$string;->crm_email_collection_settings_screen_dialog_decline:I

    new-instance v2, Lcom/squareup/ui/settings/crm/-$$Lambda$EmailCollectionConfirmationDialog$Factory$UXZh00tFR0vAhvYIY79pNJisfcs;

    invoke-direct {v2, v0}, Lcom/squareup/ui/settings/crm/-$$Lambda$EmailCollectionConfirmationDialog$Factory$UXZh00tFR0vAhvYIY79pNJisfcs;-><init>(Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog$Controller;)V

    .line 46
    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    new-instance v1, Lcom/squareup/ui/settings/crm/-$$Lambda$EmailCollectionConfirmationDialog$Factory$5NPwWIFW4xm4EWT4a_h2WudGlYU;

    invoke-direct {v1, v0}, Lcom/squareup/ui/settings/crm/-$$Lambda$EmailCollectionConfirmationDialog$Factory$5NPwWIFW4xm4EWT4a_h2WudGlYU;-><init>(Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog$Controller;)V

    .line 48
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/settingsapplet/R$string;->crm_email_collection_settings_screen_dialog_title:I

    .line 50
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/settingsapplet/R$string;->crm_email_collection_settings_screen_dialog_message:I

    .line 51
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 52
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 43
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
