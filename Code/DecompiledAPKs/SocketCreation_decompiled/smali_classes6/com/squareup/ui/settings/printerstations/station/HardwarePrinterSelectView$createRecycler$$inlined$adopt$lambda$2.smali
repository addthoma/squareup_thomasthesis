.class final Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$2;
.super Lkotlin/jvm/internal/Lambda;
.source "HardwarePrinterSelectView.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->createRecycler()Lcom/squareup/cycler/Recycler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/cycler/StandardRowSpec$Creator<",
        "Ljava/lang/Object;",
        "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;",
        "Lcom/squareup/marketfont/MarketButton;",
        ">;",
        "Landroid/content/Context;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nHardwarePrinterSelectView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 HardwarePrinterSelectView.kt\ncom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$1$4$1\n+ 2 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$Creator\n*L\n1#1,222:1\n64#2,2:223\n*E\n*S KotlinDebug\n*F\n+ 1 HardwarePrinterSelectView.kt\ncom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$1$4$1\n*L\n140#1,2:223\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u0001*\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00022\u0006\u0010\u0006\u001a\u00020\u0007H\n\u00a2\u0006\u0002\u0008\u0008\u00a8\u0006\n"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/cycler/StandardRowSpec$Creator;",
        "",
        "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;",
        "Lcom/squareup/marketfont/MarketButton;",
        "it",
        "Landroid/content/Context;",
        "invoke",
        "com/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$1$4$1",
        "com/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$$special$$inlined$extraItem$lambda$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$2;->this$0:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/cycler/StandardRowSpec$Creator;

    check-cast p2, Landroid/content/Context;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$2;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec$Creator<",
            "Ljava/lang/Object;",
            "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;",
            "Lcom/squareup/marketfont/MarketButton;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "it"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    new-instance v0, Lcom/squareup/marketfont/MarketButton;

    invoke-direct {v0, p2}, Lcom/squareup/marketfont/MarketButton;-><init>(Landroid/content/Context;)V

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->setView(Landroid/view/View;)V

    .line 131
    new-instance p2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x1

    const/4 v1, -0x2

    invoke-direct {p2, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$2;->this$0:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 137
    invoke-virtual {p2, v0, v0, v0, v0}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 138
    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    check-cast p2, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 223
    new-instance p2, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$2$1;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$2$1;-><init>(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$2;Lcom/squareup/cycler/StandardRowSpec$Creator;)V

    check-cast p2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p1, p2}, Lcom/squareup/cycler/StandardRowSpec$Creator;->bind(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method
