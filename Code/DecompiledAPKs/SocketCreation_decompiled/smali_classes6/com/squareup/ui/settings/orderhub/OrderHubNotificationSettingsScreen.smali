.class public final Lcom/squareup/ui/settings/orderhub/OrderHubNotificationSettingsScreen;
.super Lcom/squareup/ui/settings/orderhub/InOrderHubSettingsScope;
.source "OrderHubNotificationSettingsScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/orderhub/OrderHubNotificationSettingsScreen$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubNotificationSettingsScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubNotificationSettingsScreen.kt\ncom/squareup/ui/settings/orderhub/OrderHubNotificationSettingsScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,33:1\n43#2:34\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubNotificationSettingsScreen.kt\ncom/squareup/ui/settings/orderhub/OrderHubNotificationSettingsScreen\n*L\n22#1:34\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0018\u0000 \u00102\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0001\u0010B\u0005\u00a2\u0006\u0002\u0010\u0005J\u0012\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0008\u0010\u000e\u001a\u00020\u000fH\u0016R\u0018\u0010\u0006\u001a\u0006\u0012\u0002\u0008\u00030\u00078VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/settings/orderhub/OrderHubNotificationSettingsScreen;",
        "Lcom/squareup/ui/settings/orderhub/InOrderHubSettingsScope;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "Lcom/squareup/container/layer/InSection;",
        "()V",
        "section",
        "Ljava/lang/Class;",
        "getSection",
        "()Ljava/lang/Class;",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "",
        "Companion",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/orderhub/OrderHubNotificationSettingsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/settings/orderhub/OrderHubNotificationSettingsScreen$Companion;

.field private static final INSTANCE:Lcom/squareup/ui/settings/orderhub/OrderHubNotificationSettingsScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/settings/orderhub/OrderHubNotificationSettingsScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/orderhub/OrderHubNotificationSettingsScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/settings/orderhub/OrderHubNotificationSettingsScreen;->Companion:Lcom/squareup/ui/settings/orderhub/OrderHubNotificationSettingsScreen$Companion;

    .line 27
    new-instance v0, Lcom/squareup/ui/settings/orderhub/OrderHubNotificationSettingsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/orderhub/OrderHubNotificationSettingsScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/orderhub/OrderHubNotificationSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/orderhub/OrderHubNotificationSettingsScreen;

    .line 30
    sget-object v0, Lcom/squareup/ui/settings/orderhub/OrderHubNotificationSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/orderhub/OrderHubNotificationSettingsScreen;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "PathCreator.forSingleton(INSTANCE)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/ui/settings/orderhub/OrderHubNotificationSettingsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/squareup/ui/settings/orderhub/InOrderHubSettingsScope;-><init>()V

    return-void
.end method

.method public static final synthetic access$getINSTANCE$cp()Lcom/squareup/ui/settings/orderhub/OrderHubNotificationSettingsScreen;
    .locals 1

    .line 12
    sget-object v0, Lcom/squareup/ui/settings/orderhub/OrderHubNotificationSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/orderhub/OrderHubNotificationSettingsScreen;

    return-object v0
.end method


# virtual methods
.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 17
    const-class v0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsSection;

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    const-class v0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$Component;

    .line 23
    invoke-interface {p1}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$Component;->orderHubNotificationSettingsCoordinator()Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 19
    sget v0, Lcom/squareup/settingsapplet/R$layout;->orderhub_alert_settings_view:I

    return v0
.end method
