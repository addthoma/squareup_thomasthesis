.class public final Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter_Factory;
.super Ljava/lang/Object;
.source "MerchantProfileBusinessAddressPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final monitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final stateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantprofile/MerchantProfileState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantprofile/MerchantProfileState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter_Factory;->stateProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter_Factory;->monitorProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantprofile/MerchantProfileState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;",
            ">;)",
            "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/merchantprofile/MerchantProfileState;Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;)Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;
    .locals 1

    .line 47
    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;-><init>(Lcom/squareup/merchantprofile/MerchantProfileState;Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter_Factory;->stateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/merchantprofile/MerchantProfileState;

    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter_Factory;->monitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

    iget-object v2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter_Factory;->newInstance(Lcom/squareup/merchantprofile/MerchantProfileState;Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;)Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter_Factory;->get()Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;

    move-result-object v0

    return-object v0
.end method
