.class final Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$onScreenData$1;
.super Ljava/lang/Object;
.source "DepositSettingsCoordinator.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->onScreenData(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen$ScreenData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$onScreenData$1;->this$0:Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .line 195
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$onScreenData$1;->this$0:Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->access$getStarter$p(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;)Lcom/squareup/onboarding/OnboardingStarter;

    move-result-object p1

    new-instance v0, Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;

    sget-object v1, Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;->RESTART:Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3, v2}, Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;-><init>(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {p1, v0}, Lcom/squareup/onboarding/OnboardingStarter;->startActivation(Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;)V

    return-void
.end method
