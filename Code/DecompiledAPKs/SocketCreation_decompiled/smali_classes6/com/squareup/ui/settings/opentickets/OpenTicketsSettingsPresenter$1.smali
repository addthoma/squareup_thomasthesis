.class Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$1;
.super Lcom/squareup/cogs/CatalogUpdateTask;
.source "OpenTicketsSettingsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->saveTicketGroupOrdinals(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

.field final synthetic val$updatedTicketGroups:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;Ljava/util/List;)V
    .locals 0

    .line 327
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$1;->this$0:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$1;->val$updatedTicketGroups:Ljava/util/List;

    invoke-direct {p0}, Lcom/squareup/cogs/CatalogUpdateTask;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Lcom/squareup/shared/catalog/Catalog$Local;)V
    .locals 2

    .line 329
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter$1;->val$updatedTicketGroups:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->write(Ljava/util/Collection;Ljava/util/Collection;)V

    return-void
.end method
