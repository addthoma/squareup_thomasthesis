.class public final Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "BarcodeScannersSettingsScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Component;,
        Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen;

    .line 101
    sget-object v0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen;

    .line 102
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_BARCODE_SCANNERS:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 32
    const-class v0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 105
    sget v0, Lcom/squareup/settingsapplet/R$layout;->barcode_scanners_settings_view:I

    return v0
.end method
