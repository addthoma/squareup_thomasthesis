.class public interface abstract Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen$Runner;
.super Ljava/lang/Object;
.source "GiftCardsSettingsScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u000e\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H&J\u000e\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005H&J\u0008\u0010\t\u001a\u00020\u0003H&J\u0018\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u0008H&J\u0018\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u000f\u001a\u00020\u0010H&J\u0018\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u0012\u001a\u00020\u0010H&J\u0018\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u0014\u001a\u00020\u0015H&\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen$Runner;",
        "",
        "exitSettings",
        "",
        "screenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ui/settings/giftcards/ScreenData;",
        "showBackButton",
        "",
        "showViewDesigns",
        "updateEnabledInPosSettings",
        "oldState",
        "Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;",
        "enabled",
        "updateMaxAmount",
        "newMax",
        "Lcom/squareup/protos/common/Money;",
        "updateMinAmount",
        "newMin",
        "updatePolicy",
        "newPolicy",
        "",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract exitSettings()V
.end method

.method public abstract screenData()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/giftcards/ScreenData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract showBackButton()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract showViewDesigns()V
.end method

.method public abstract updateEnabledInPosSettings(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Z)V
.end method

.method public abstract updateMaxAmount(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Lcom/squareup/protos/common/Money;)V
.end method

.method public abstract updateMinAmount(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Lcom/squareup/protos/common/Money;)V
.end method

.method public abstract updatePolicy(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Ljava/lang/String;)V
.end method
