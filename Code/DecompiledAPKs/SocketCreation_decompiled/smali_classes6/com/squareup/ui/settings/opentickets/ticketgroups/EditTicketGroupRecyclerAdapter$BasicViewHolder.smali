.class public Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$BasicViewHolder;
.super Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;
.source "EditTicketGroupRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "BasicViewHolder"
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    return-void
.end method

.method public static inflate(Landroid/view/ViewGroup;ILcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;)Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$BasicViewHolder;
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_3

    if-eq p1, v0, :cond_2

    const/4 v2, 0x2

    if-eq p1, v2, :cond_1

    const/4 v2, 0x4

    if-eq p1, v2, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 44
    :cond_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    sget v2, Lcom/squareup/settingsapplet/R$layout;->edit_ticket_group_view_delete_button:I

    invoke-virtual {p1, v2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    .line 46
    move-object p1, p0

    check-cast p1, Lcom/squareup/ui/ConfirmButton;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->onDeleteButtonInflated(Lcom/squareup/ui/ConfirmButton;)V

    goto :goto_0

    .line 40
    :cond_1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    sget p2, Lcom/squareup/settingsapplet/R$layout;->edit_ticket_group_view_custom_header:I

    invoke-virtual {p1, p2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    goto :goto_0

    .line 35
    :cond_2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    sget v2, Lcom/squareup/settingsapplet/R$layout;->edit_ticket_group_ticket_counter:I

    invoke-virtual {p1, v2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    .line 37
    invoke-virtual {p2, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->onTicketCountViewInflated(Landroid/view/View;)V

    goto :goto_0

    .line 30
    :cond_3
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    sget v2, Lcom/squareup/settingsapplet/R$layout;->edit_ticket_group_view_header:I

    invoke-virtual {p1, v2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    .line 32
    invoke-virtual {p2, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->onHeaderViewInflated(Landroid/view/View;)V

    :goto_0
    if-eqz p0, :cond_4

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    .line 49
    :goto_1
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 50
    new-instance p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$BasicViewHolder;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$BasicViewHolder;-><init>(Landroid/view/View;)V

    return-object p1
.end method


# virtual methods
.method public bindData(Lcom/squareup/api/items/TicketTemplate$Builder;)V
    .locals 0

    return-void
.end method

.method public getDragHandle()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
