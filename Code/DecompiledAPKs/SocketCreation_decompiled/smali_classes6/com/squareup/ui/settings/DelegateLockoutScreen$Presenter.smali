.class public Lcom/squareup/ui/settings/DelegateLockoutScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsSectionPresenter;
.source "DelegateLockoutScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/DelegateLockoutScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsSectionPresenter<",
        "Lcom/squareup/ui/settings/DelegateLockoutView;",
        ">;"
    }
.end annotation


# instance fields
.field private titleText:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 48
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V

    return-void
.end method


# virtual methods
.method public getActionbarText()Ljava/lang/String;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/settings/DelegateLockoutScreen$Presenter;->titleText:Ljava/lang/String;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 52
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 53
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/DelegateLockoutScreen;

    .line 54
    invoke-static {p1}, Lcom/squareup/ui/settings/DelegateLockoutScreen;->access$000(Lcom/squareup/ui/settings/DelegateLockoutScreen;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/DelegateLockoutScreen$Presenter;->titleText:Ljava/lang/String;

    return-void
.end method

.method protected saveSettings()V
    .locals 0

    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 57
    const-class v0, Lcom/squareup/ui/settings/DelegateLockoutScreen;

    return-object v0
.end method
