.class public final Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "SharedSettingsScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Component;,
        Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen;

    .line 76
    sget-object v0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen;

    .line 77
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 35
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_SHARED_SETTINGS:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 31
    const-class v0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 80
    sget v0, Lcom/squareup/settingsapplet/R$layout;->shared_settings_view:I

    return v0
.end method
