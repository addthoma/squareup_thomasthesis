.class public Lcom/squareup/ui/onboarding/MerchantCategoryView;
.super Lcom/squareup/ui/onboarding/BaseCategoryView;
.source "MerchantCategoryView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/onboarding/BaseCategoryView<",
        "Lcom/squareup/server/activation/ActivationResources$BusinessCategory;",
        ">;",
        "Lcom/squareup/workflow/ui/HandlesBack;"
    }
.end annotation


# instance fields
.field private final confirmLaterPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

.field presenter:Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/BaseCategoryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    const-class p2, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Component;->inject(Lcom/squareup/ui/onboarding/MerchantCategoryView;)V

    .line 23
    new-instance p2, Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-direct {p2, p1}, Lcom/squareup/flowlegacy/ConfirmationPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/MerchantCategoryView;->confirmLaterPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    return-void
.end method


# virtual methods
.method public bridge synthetic getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 13
    invoke-super {p0}, Lcom/squareup/ui/onboarding/BaseCategoryView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic hideSubtitle()V
    .locals 0

    .line 13
    invoke-super {p0}, Lcom/squareup/ui/onboarding/BaseCategoryView;->hideSubtitle()V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantCategoryView;->presenter:Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantCategoryView;->presenter:Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->confirmLaterPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/MerchantCategoryView;->confirmLaterPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantCategoryView;->presenter:Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 40
    invoke-super {p0}, Lcom/squareup/ui/onboarding/BaseCategoryView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 27
    invoke-super {p0}, Lcom/squareup/ui/onboarding/BaseCategoryView;->onFinishInflate()V

    .line 29
    sget v0, Lcom/squareup/onboarding/flow/R$string;->merchant_category_heading:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/MerchantCategoryView;->setTitle(I)V

    .line 30
    sget v0, Lcom/squareup/onboarding/flow/R$string;->choose_a_category:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/MerchantCategoryView;->setSubtitle(I)V

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantCategoryView;->presenter:Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantCategoryView;->presenter:Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->confirmLaterPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/MerchantCategoryView;->confirmLaterPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
