.class public final Lcom/squareup/ui/onboarding/bank/DepositOptionsBootstrapScreen$Onboarding;
.super Lcom/squareup/ui/onboarding/bank/DepositOptionsBootstrapScreen;
.source "DepositOptionsBootstrapScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsBootstrapScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Onboarding"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsBootstrapScreen$Onboarding;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsBootstrapScreen;",
        "()V",
        "getParentKey",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsBootstrapScreen$Onboarding;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsBootstrapScreen$Onboarding;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsBootstrapScreen$Onboarding;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsBootstrapScreen$Onboarding;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsBootstrapScreen$Onboarding;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsBootstrapScreen;-><init>()V

    return-void
.end method


# virtual methods
.method public getParentKey()Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope;
    .locals 3

    .line 16
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope;

    sget-object v1, Lcom/squareup/ui/onboarding/LoggedInOnboardingScope;->INSTANCE:Lcom/squareup/ui/onboarding/LoggedInOnboardingScope;

    const-string v2, "LoggedInOnboardingScope.INSTANCE"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v0, v1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsBootstrapScreen$Onboarding;->getParentKey()Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope;

    move-result-object v0

    return-object v0
.end method
