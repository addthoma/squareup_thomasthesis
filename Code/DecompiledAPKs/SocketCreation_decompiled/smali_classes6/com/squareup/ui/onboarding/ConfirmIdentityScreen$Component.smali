.class public interface abstract Lcom/squareup/ui/onboarding/ConfirmIdentityScreen$Component;
.super Ljava/lang/Object;
.source "ConfirmIdentityScreen.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/onboarding/ActivationCallModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/ConfirmIdentityScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/onboarding/ConfirmIdentityView;)V
.end method
