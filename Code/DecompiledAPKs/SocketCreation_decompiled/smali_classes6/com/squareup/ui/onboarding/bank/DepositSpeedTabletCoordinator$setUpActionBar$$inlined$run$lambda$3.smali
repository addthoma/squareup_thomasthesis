.class final Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$setUpActionBar$$inlined$run$lambda$3;
.super Ljava/lang/Object;
.source "DepositSpeedTabletCoordinator.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;->setUpActionBar(Landroid/view/View;ZLcom/squareup/workflow/legacy/WorkflowInput;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "run",
        "com/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$setUpActionBar$1$4"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $canGoBack$inlined:Z

.field final synthetic $res$inlined:Landroid/content/res/Resources;

.field final synthetic $view$inlined:Landroid/view/View;

.field final synthetic $workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;


# direct methods
.method constructor <init>(ZLandroid/view/View;Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/content/res/Resources;)V
    .locals 0

    iput-boolean p1, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$setUpActionBar$$inlined$run$lambda$3;->$canGoBack$inlined:Z

    iput-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$setUpActionBar$$inlined$run$lambda$3;->$view$inlined:Landroid/view/View;

    iput-object p3, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$setUpActionBar$$inlined$run$lambda$3;->$workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    iput-object p4, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$setUpActionBar$$inlined$run$lambda$3;->$res$inlined:Landroid/content/res/Resources;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$setUpActionBar$$inlined$run$lambda$3;->$workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object v1, Lcom/squareup/ui/onboarding/bank/LaterClicked;->INSTANCE:Lcom/squareup/ui/onboarding/bank/LaterClicked;

    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
