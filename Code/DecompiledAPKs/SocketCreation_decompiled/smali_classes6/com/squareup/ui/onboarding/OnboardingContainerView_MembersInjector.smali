.class public final Lcom/squareup/ui/onboarding/OnboardingContainerView_MembersInjector;
.super Ljava/lang/Object;
.source "OnboardingContainerView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/onboarding/OnboardingContainerView;",
        ">;"
    }
.end annotation


# instance fields
.field private final badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;"
        }
    .end annotation
.end field

.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingContainerView_MembersInjector;->containerProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/ui/onboarding/OnboardingContainerView_MembersInjector;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/ui/onboarding/OnboardingContainerView_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/onboarding/OnboardingContainerView;",
            ">;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/ui/onboarding/OnboardingContainerView_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/onboarding/OnboardingContainerView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectBadMaybeSquareDeviceCheck(Lcom/squareup/ui/onboarding/OnboardingContainerView;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingContainerView;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    return-void
.end method

.method public static injectContainer(Lcom/squareup/ui/onboarding/OnboardingContainerView;Lcom/squareup/ui/onboarding/OnboardingContainer;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingContainerView;->container:Lcom/squareup/ui/onboarding/OnboardingContainer;

    return-void
.end method

.method public static injectFeatures(Lcom/squareup/ui/onboarding/OnboardingContainerView;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 60
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingContainerView;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/onboarding/OnboardingContainerView;)V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainerView_MembersInjector;->containerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/OnboardingContainer;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingContainerView_MembersInjector;->injectContainer(Lcom/squareup/ui/onboarding/OnboardingContainerView;Lcom/squareup/ui/onboarding/OnboardingContainer;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainerView_MembersInjector;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingContainerView_MembersInjector;->injectBadMaybeSquareDeviceCheck(Lcom/squareup/ui/onboarding/OnboardingContainerView;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainerView_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingContainerView_MembersInjector;->injectFeatures(Lcom/squareup/ui/onboarding/OnboardingContainerView;Lcom/squareup/settings/server/Features;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/ui/onboarding/OnboardingContainerView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/OnboardingContainerView_MembersInjector;->injectMembers(Lcom/squareup/ui/onboarding/OnboardingContainerView;)V

    return-void
.end method
