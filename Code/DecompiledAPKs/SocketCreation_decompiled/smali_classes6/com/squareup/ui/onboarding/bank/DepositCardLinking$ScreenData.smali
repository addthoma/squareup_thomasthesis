.class public final Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;
.super Ljava/lang/Object;
.source "DepositCardLinkingScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/bank/DepositCardLinking;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;",
        "",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "showSpinner",
        "",
        "(Lcom/squareup/CountryCode;Z)V",
        "getCountryCode",
        "()Lcom/squareup/CountryCode;",
        "getShowSpinner",
        "()Z",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final countryCode:Lcom/squareup/CountryCode;

.field private final showSpinner:Z


# direct methods
.method public constructor <init>(Lcom/squareup/CountryCode;Z)V
    .locals 1

    const-string v0, "countryCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;->countryCode:Lcom/squareup/CountryCode;

    iput-boolean p2, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;->showSpinner:Z

    return-void
.end method


# virtual methods
.method public final getCountryCode()Lcom/squareup/CountryCode;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;->countryCode:Lcom/squareup/CountryCode;

    return-object v0
.end method

.method public final getShowSpinner()Z
    .locals 1

    .line 23
    iget-boolean v0, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;->showSpinner:Z

    return v0
.end method
