.class public Lcom/squareup/ui/onboarding/SilentExitScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "SilentExitScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/SilentExitScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/onboarding/SilentExitView;",
        ">;"
    }
.end annotation


# instance fields
.field private final runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 29
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/onboarding/SilentExitScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    return-void
.end method


# virtual methods
.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 34
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 35
    iget-object p1, p0, Lcom/squareup/ui/onboarding/SilentExitScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onSilentExit()V

    return-void
.end method
