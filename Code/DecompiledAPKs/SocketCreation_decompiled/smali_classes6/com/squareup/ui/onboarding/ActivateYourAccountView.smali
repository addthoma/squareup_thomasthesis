.class public Lcom/squareup/ui/onboarding/ActivateYourAccountView;
.super Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;
.source "ActivateYourAccountView.java"


# instance fields
.field presenter:Lcom/squareup/ui/onboarding/ActivateYourAccountScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    const-class p2, Lcom/squareup/ui/onboarding/ActivateYourAccountScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/ActivateYourAccountScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/onboarding/ActivateYourAccountScreen$Component;->inject(Lcom/squareup/ui/onboarding/ActivateYourAccountView;)V

    return-void
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivateYourAccountView;->presenter:Lcom/squareup/ui/onboarding/ActivateYourAccountScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/ActivateYourAccountScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 47
    invoke-super {p0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 23
    invoke-super {p0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->onFinishInflate()V

    .line 25
    sget v0, Lcom/squareup/onboarding/common/R$string;->onboarding_activate_in_app:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/ActivateYourAccountView;->setTitleText(I)V

    .line 26
    sget v0, Lcom/squareup/onboarding/common/R$string;->onboarding_activate_message:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/ActivateYourAccountView;->setMessageText(I)V

    .line 27
    sget v0, Lcom/squareup/common/strings/R$string;->continue_label:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/ActivateYourAccountView;->setTopButton(I)V

    .line 28
    sget v0, Lcom/squareup/onboarding/flow/R$string;->later:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/ActivateYourAccountView;->setBottomButton(I)V

    .line 29
    sget v0, Lcom/squareup/cardreader/vector/icons/R$drawable;->icon_audio_reader_120:I

    sget v1, Lcom/squareup/ui/onboarding/ActivateYourAccountView;->ROTATE_LEFT:I

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/onboarding/ActivateYourAccountView;->setVector(IF)V

    .line 30
    new-instance v0, Lcom/squareup/ui/onboarding/ActivateYourAccountView$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/ActivateYourAccountView$1;-><init>(Lcom/squareup/ui/onboarding/ActivateYourAccountView;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/ActivateYourAccountView;->setTopButtonOnClicked(Landroid/view/View$OnClickListener;)V

    .line 35
    new-instance v0, Lcom/squareup/ui/onboarding/ActivateYourAccountView$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/ActivateYourAccountView$2;-><init>(Lcom/squareup/ui/onboarding/ActivateYourAccountView;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/ActivateYourAccountView;->setBottomButtonOnClicked(Landroid/view/View$OnClickListener;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivateYourAccountView;->presenter:Lcom/squareup/ui/onboarding/ActivateYourAccountScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/ActivateYourAccountScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
