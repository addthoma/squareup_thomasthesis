.class public final Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "DepositCardLinkingCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0001,B-\u0008\u0002\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u000f\u001a\u00020\u000eH\u0002J\u001e\u0010\u001e\u001a\u00020\u00182\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00060 2\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0016\u0010!\u001a\u00020\u00182\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00060 H\u0002J(\u0010\"\u001a\u00020\u00182\u0016\u0010#\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00072\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u001e\u0010$\u001a\u00020\u00182\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00060 2\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0008\u0010%\u001a\u00020\u0018H\u0002J\u001e\u0010&\u001a\u00020\u00182\u0014\u0008\u0002\u0010\'\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u00180(H\u0002J \u0010)\u001a\u00020*2\u0016\u0010#\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J\u0008\u0010+\u001a\u00020\u0018H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\"\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\u0008\u0010\r\u001a\u0004\u0018\u00010\u000e@BX\u0082\u000e\u00a2\u0006\u0008\n\u0000\"\u0004\u0008\u0010\u0010\u0011R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;",
        "Lcom/squareup/ui/onboarding/bank/DepositCardLinking$Event;",
        "Lcom/squareup/ui/onboarding/bank/DepositCardLinkingScreen;",
        "glassSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "(Lio/reactivex/Observable;Lcom/squareup/register/widgets/GlassSpinner;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "value",
        "Lcom/squareup/Card;",
        "card",
        "setCard",
        "(Lcom/squareup/Card;)V",
        "cardEditor",
        "Lcom/squareup/register/widgets/card/CardEditor;",
        "subtitle",
        "Lcom/squareup/widgets/MessageView;",
        "title",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "getCardData",
        "Lcom/squareup/protos/client/bills/CardData;",
        "onAdvanced",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "onLater",
        "onScreen",
        "screen",
        "setUpActionBar",
        "setUpCardEditor",
        "setUpCardEditorListener",
        "onChargeCard",
        "Lkotlin/Function1;",
        "spinnerData",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "updateContinueButton",
        "Factory",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private card:Lcom/squareup/Card;

.field private cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;",
            "Lcom/squareup/ui/onboarding/bank/DepositCardLinking$Event;",
            ">;>;"
        }
    .end annotation
.end field

.field private subtitle:Lcom/squareup/widgets/MessageView;

.field private title:Lcom/squareup/widgets/MessageView;


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Lcom/squareup/register/widgets/GlassSpinner;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;",
            "Lcom/squareup/ui/onboarding/bank/DepositCardLinking$Event;",
            ">;>;",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ")V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lcom/squareup/register/widgets/GlassSpinner;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/register/widgets/GlassSpinner;)V

    return-void
.end method

.method public static final synthetic access$getCard$p(Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;)Lcom/squareup/Card;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->card:Lcom/squareup/Card;

    return-object p0
.end method

.method public static final synthetic access$onAdvanced(Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/view/View;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->onAdvanced(Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic access$onLater(Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->onLater(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method public static final synthetic access$onScreen(Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->onScreen(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic access$setCard$p(Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;Lcom/squareup/Card;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->setCard(Lcom/squareup/Card;)V

    return-void
.end method

.method public static final synthetic access$spinnerData(Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->spinnerData(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p0

    return-object p0
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 173
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "ActionBarView.findIn(view)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 174
    sget v0, Lcom/squareup/onboarding/flow/R$id;->title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->title:Lcom/squareup/widgets/MessageView;

    .line 175
    sget v0, Lcom/squareup/onboarding/flow/R$id;->subtitle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    .line 176
    sget v0, Lcom/squareup/onboarding/flow/R$id;->debit_card_input_editor:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/widgets/card/CardEditor;

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    return-void
.end method

.method private final getCardData(Lcom/squareup/Card;)Lcom/squareup/protos/client/bills/CardData;
    .locals 3

    .line 125
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;-><init>()V

    .line 126
    invoke-virtual {p1}, Lcom/squareup/Card;->getExpirationMonth()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->month(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;

    move-result-object v0

    .line 127
    invoke-virtual {p1}, Lcom/squareup/Card;->getExpirationYear()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->year(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;

    move-result-object v0

    .line 128
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->build()Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    move-result-object v0

    .line 129
    new-instance v1, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;-><init>()V

    .line 130
    invoke-virtual {p1}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->card_number(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    move-result-object v1

    .line 131
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->expiry(Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    move-result-object v0

    .line 132
    invoke-virtual {p1}, Lcom/squareup/Card;->getVerification()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->cvv(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    move-result-object v0

    .line 133
    invoke-virtual {p1}, Lcom/squareup/Card;->getPostalCode()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->postal_code(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    move-result-object p1

    .line 134
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->build()Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    move-result-object p1

    .line 135
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$Builder;-><init>()V

    .line 136
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->keyed_card(Lcom/squareup/protos/client/bills/CardData$KeyedCard;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    .line 137
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->build()Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    const-string v0, "CardData.Builder()\n     \u2026yedCard)\n        .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final onAdvanced(Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/onboarding/bank/DepositCardLinking$Event;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 116
    invoke-static {p2}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 117
    new-instance p2, Lcom/squareup/ui/onboarding/bank/DepositCardLinking$Event$LinkCard;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->card:Lcom/squareup/Card;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->getCardData(Lcom/squareup/Card;)Lcom/squareup/protos/client/bills/CardData;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/squareup/ui/onboarding/bank/DepositCardLinking$Event$LinkCard;-><init>(Lcom/squareup/protos/client/bills/CardData;)V

    invoke-interface {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method private final onLater(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/onboarding/bank/DepositCardLinking$Event;",
            ">;)V"
        }
    .end annotation

    .line 121
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositCardLinking$Event$LaterClicked;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositCardLinking$Event$LaterClicked;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method private final onScreen(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;",
            "Lcom/squareup/ui/onboarding/bank/DepositCardLinking$Event;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 86
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0, v0, p2}, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->setUpActionBar(Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/view/View;)V

    .line 87
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$onScreen$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$onScreen$1;-><init>(Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->setUpCardEditorListener(Lkotlin/jvm/functions/Function1;)V

    .line 88
    iget-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    const-string v0, "cardEditor"

    if-nez p2, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object p1

    const/4 v1, 0x1

    new-instance v2, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;

    invoke-direct {v2}, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;-><init>()V

    check-cast v2, Lcom/squareup/register/widgets/card/PanValidationStrategy;

    invoke-virtual {p2, p1, v1, v2}, Lcom/squareup/register/widgets/card/CardEditor;->init(Lcom/squareup/CountryCode;ZLcom/squareup/register/widgets/card/PanValidationStrategy;)V

    .line 89
    iget-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    if-nez p1, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 90
    :cond_1
    sget p2, Lcom/squareup/debitcard/R$string;->instant_deposits_link_debit_card_editor_hint:I

    .line 89
    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/card/CardEditor;->setCardInputHint(I)V

    return-void
.end method

.method private final setCard(Lcom/squareup/Card;)V
    .locals 0

    .line 52
    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->card:Lcom/squareup/Card;

    .line 53
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->updateContinueButton()V

    return-void
.end method

.method private final setUpActionBar(Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/onboarding/bank/DepositCardLinking$Event;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 98
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 99
    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->title:Lcom/squareup/widgets/MessageView;

    if-nez v1, :cond_0

    const-string v2, "title"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v2, Lcom/squareup/debitcard/R$string;->instant_deposits_link_debit_card:I

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 100
    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    if-nez v1, :cond_1

    const-string v2, "subtitle"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v2, Lcom/squareup/onboarding/flow/R$string;->link_debit_card_subtitle:I

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 109
    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v1, :cond_2

    const-string v2, "actionBar"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 102
    :cond_2
    new-instance v2, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 103
    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideUpButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 104
    sget v3, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_continue:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 105
    new-instance v3, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$setUpActionBar$1;

    invoke-direct {v3, p0, p1, p2}, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$setUpActionBar$1;-><init>(Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/view/View;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    .line 106
    iget-object v2, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->card:Lcom/squareup/Card;

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p2, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    .line 107
    sget v2, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_later:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    .line 108
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$setUpActionBar$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$setUpActionBar$2;-><init>(Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p2, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showSecondaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 109
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final setUpCardEditor()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 73
    invoke-static {p0, v0, v1, v0}, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->setUpCardEditorListener$default(Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    const-string v1, "cardEditor"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor;->requestFocus()Z

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;

    invoke-direct {v1}, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;-><init>()V

    check-cast v1, Lcom/squareup/register/widgets/card/PanValidationStrategy;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CardEditor;->setStrategy(Lcom/squareup/register/widgets/card/PanValidationStrategy;)V

    return-void
.end method

.method private final setUpCardEditorListener(Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/Card;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    if-nez v0, :cond_0

    const-string v1, "cardEditor"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$setUpCardEditorListener$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$setUpCardEditorListener$2;-><init>(Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/register/widgets/card/OnCardListener;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CardEditor;->setOnCardListener(Lcom/squareup/register/widgets/card/OnCardListener;)V

    return-void
.end method

.method static synthetic setUpCardEditorListener$default(Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 147
    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$setUpCardEditorListener$1;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$setUpCardEditorListener$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->setUpCardEditorListener(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final spinnerData(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;",
            "Lcom/squareup/ui/onboarding/bank/DepositCardLinking$Event;",
            ">;)",
            "Lcom/squareup/register/widgets/GlassSpinnerState;"
        }
    .end annotation

    .line 141
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    .line 142
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;->getShowSpinner()Z

    move-result p1

    sget v1, Lcom/squareup/debitcard/R$string;->instant_deposits_linking_debit_card:I

    .line 141
    invoke-virtual {v0, p1, v1}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showNonDebouncedSpinner(ZI)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method

.method private final updateContinueButton()V
    .locals 2

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->card:Lcom/squareup/Card;

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$attach$1;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$attach$1;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 59
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->bindViews(Landroid/view/View;)V

    .line 60
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->setUpCardEditor()V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "glassSpinner.showOrHideSpinner(view.context)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 64
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->screens:Lio/reactivex/Observable;

    .line 67
    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    new-instance v2, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$attach$2;

    move-object v3, p0

    check-cast v3, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;

    invoke-direct {v2, v3}, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$attach$2;-><init>(Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    new-instance v3, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$sam$rx_functions_Func1$0;

    invoke-direct {v3, v2}, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$sam$rx_functions_Func1$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v3, Lrx/functions/Func1;

    invoke-virtual {v1, v3}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransformRx2(Lrx/functions/Func1;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 68
    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$attach$3;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$attach$3;-><init>(Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens\n        .compose\u2026be { onScreen(it, view) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
