.class public Lcom/squareup/ui/onboarding/OnboardingFinishedScreen;
.super Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;
.source "OnboardingFinishedScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Component;,
        Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/onboarding/OnboardingFinishedScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/onboarding/OnboardingFinishedScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 19
    new-instance v0, Lcom/squareup/ui/onboarding/OnboardingFinishedScreen;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/OnboardingFinishedScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/OnboardingFinishedScreen;->INSTANCE:Lcom/squareup/ui/onboarding/OnboardingFinishedScreen;

    .line 20
    sget-object v0, Lcom/squareup/ui/onboarding/OnboardingFinishedScreen;->INSTANCE:Lcom/squareup/ui/onboarding/OnboardingFinishedScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/onboarding/OnboardingFinishedScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 26
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_END:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 63
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->onboarding_finished_view:I

    return v0
.end method
