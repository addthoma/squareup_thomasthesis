.class public Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen;
.super Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;
.source "ConfirmMagstripeAddressScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Component;,
        Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Module;,
        Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$RequestReaderResources;,
        Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;,
        Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 63
    new-instance v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen;->INSTANCE:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen;

    .line 346
    sget-object v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen;->INSTANCE:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen;

    .line 347
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 65
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 69
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_SEND_READER:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 363
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->confirm_magstripe_address_view:I

    return v0
.end method
