.class Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter$2;
.super Ljava/lang/Object;
.source "ActivationRetryScreen.java"

# interfaces
.implements Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;-><init>(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/util/Res;Lcom/squareup/server/activation/ActivationService;Lcom/squareup/caller/ProgressAndFailureRxGlue$Factory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener<",
        "Lcom/squareup/server/SimpleResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;)V
    .locals 0

    .line 65
    iput-object p1, p0, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter$2;->this$0:Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailureViewDismissed(Z)V
    .locals 0

    if-eqz p1, :cond_0

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter$2;->this$0:Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;

    invoke-static {p1}, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->access$100(Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;)V

    :cond_0
    return-void
.end method

.method public onProgressViewDismissed(Lcom/squareup/server/SimpleResponse;)V
    .locals 0

    return-void
.end method

.method public bridge synthetic onProgressViewDismissed(Ljava/lang/Object;)V
    .locals 0

    .line 65
    check-cast p1, Lcom/squareup/server/SimpleResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter$2;->onProgressViewDismissed(Lcom/squareup/server/SimpleResponse;)V

    return-void
.end method
