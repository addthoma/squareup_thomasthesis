.class final Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2;
.super Lkotlin/jvm/internal/Lambda;
.source "DepositOptionsScreenWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->doStart(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/legacy/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a$\u0012\u0004\u0012\u00020\u0002\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0003j\u0002`\u00040\u0001j\u0008\u0012\u0004\u0012\u00020\u0002`\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "toScreenStack",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "state",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $nonDialogScreen$1:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;

.field final synthetic $workflow:Lcom/squareup/workflow/rx1/Workflow;

.field final synthetic this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;Lcom/squareup/workflow/rx1/Workflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2;->this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;

    iput-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2;->$nonDialogScreen$1:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;

    iput-object p3, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2;->$workflow:Lcom/squareup/workflow/rx1/Workflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 93
    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2;->invoke(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 196
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone$ConfirmingLater;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone$ConfirmingLater;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 197
    :cond_0
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone$ConfirmingSameDayDepositFee;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone$ConfirmingSameDayDepositFee;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 198
    :cond_1
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet$ConfirmingLater;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet$ConfirmingLater;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 199
    :cond_2
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet$ConfirmingSameDayDepositFee;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet$ConfirmingSameDayDepositFee;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_0

    .line 200
    :cond_3
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$ConfirmingLater;

    if-eqz v0, :cond_4

    :goto_0
    sget-object v1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2;->$nonDialogScreen$1:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;->invoke(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2;->this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;

    invoke-static {v0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->access$messageDialogScreenData(Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/widgets/warning/Warning;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2;->$workflow:Lcom/squareup/workflow/rx1/Workflow;

    sget-object v5, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2$1;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2$1;

    check-cast v5, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v5}, Lcom/squareup/workflow/rx1/WorkflowKt;->adaptEvents(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 202
    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsConfirmationDialogScreenKt;->DepositOptionsConfirmationDialogScreen(Lcom/squareup/widgets/warning/Warning;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v5

    const/4 v6, 0x6

    const/4 v7, 0x0

    .line 200
    invoke-static/range {v1 .. v7}, Lcom/squareup/container/PosLayering$Companion;->dialogStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    goto :goto_2

    .line 214
    :cond_4
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$SelectingAccountType;

    if-eqz v0, :cond_5

    sget-object v1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 215
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2;->$nonDialogScreen$1:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;->invoke(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 216
    iget-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2;->$workflow:Lcom/squareup/workflow/rx1/Workflow;

    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2$2;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2$2;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/rx1/WorkflowKt;->adaptEvents(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-static {p1}, Lcom/squareup/ui/onboarding/bank/DepositAccountTypeDialogScreenKt;->DepositAccountTypeDialogScreen(Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v5

    const/4 v6, 0x6

    const/4 v7, 0x0

    .line 214
    invoke-static/range {v1 .. v7}, Lcom/squareup/container/PosLayering$Companion;->dialogStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    goto :goto_2

    .line 226
    :cond_5
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$HasInvalidInput;

    if-eqz v0, :cond_6

    goto :goto_1

    .line 227
    :cond_6
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;

    if-eqz v0, :cond_7

    :goto_1
    sget-object v1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 228
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2;->$nonDialogScreen$1:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;->invoke(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 230
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2;->this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;

    invoke-static {v0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->access$messageDialogScreenData(Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/widgets/warning/Warning;

    move-result-object p1

    .line 231
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2;->$workflow:Lcom/squareup/workflow/rx1/Workflow;

    sget-object v5, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2$3;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2$3;

    check-cast v5, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v5}, Lcom/squareup/workflow/rx1/WorkflowKt;->adaptEvents(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 229
    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWarningDialogScreenKt;->DepositOptionsWarningDialogScreen(Lcom/squareup/widgets/warning/Warning;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v5

    const/4 v6, 0x6

    const/4 v7, 0x0

    .line 227
    invoke-static/range {v1 .. v7}, Lcom/squareup/container/PosLayering$Companion;->dialogStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    goto :goto_2

    .line 235
    :cond_7
    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2;->$nonDialogScreen$1:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;

    invoke-virtual {v1, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;->invoke(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    :goto_2
    return-object p1
.end method
