.class public final Lcom/squareup/ui/onboarding/OnboardingErrorScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "OnboardingErrorScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final modelProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen_Presenter_Factory;->modelProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen_Presenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p4, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/OnboardingErrorScreen_Presenter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/ui/onboarding/OnboardingErrorScreen_Presenter_Factory;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen_Presenter_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/onboarding/OnboardingErrorScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/util/Res;)Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;
    .locals 1

    .line 48
    new-instance v0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;-><init>(Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/util/Res;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;
    .locals 4

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen_Presenter_Factory;->modelProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/OnboardingModel;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v2, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen_Presenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    iget-object v3, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/util/Res;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/onboarding/OnboardingErrorScreen_Presenter_Factory;->newInstance(Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/util/Res;)Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingErrorScreen_Presenter_Factory;->get()Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
