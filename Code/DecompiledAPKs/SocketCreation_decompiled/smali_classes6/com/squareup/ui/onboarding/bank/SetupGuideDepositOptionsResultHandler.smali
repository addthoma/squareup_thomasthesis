.class public final Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler;
.super Ljava/lang/Object;
.source "SetupGuideDepositOptionsResultHandler.kt"

# interfaces
.implements Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ResultHandler;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ResultHandler;",
        "flow",
        "Lflow/Flow;",
        "setupGuideIntegrationRunner",
        "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
        "(Lflow/Flow;Lcom/squareup/setupguide/SetupGuideIntegrationRunner;)V",
        "onDepositOptionsResult",
        "Lio/reactivex/Completable;",
        "result",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;",
        "parentTreeKey",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final flow:Lflow/Flow;

.field private final setupGuideIntegrationRunner:Lcom/squareup/setupguide/SetupGuideIntegrationRunner;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/setupguide/SetupGuideIntegrationRunner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "setupGuideIntegrationRunner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler;->flow:Lflow/Flow;

    iput-object p2, p0, Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler;->setupGuideIntegrationRunner:Lcom/squareup/setupguide/SetupGuideIntegrationRunner;

    return-void
.end method

.method public static final synthetic access$getFlow$p(Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler;)Lflow/Flow;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler;->flow:Lflow/Flow;

    return-object p0
.end method


# virtual methods
.method public onDepositOptionsResult(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;Lcom/squareup/ui/main/RegisterTreeKey;)Lio/reactivex/Completable;
    .locals 2

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parentTreeKey"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult$GoBack;

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler$onDepositOptionsResult$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler$onDepositOptionsResult$1;-><init>(Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler;)V

    check-cast p1, Lio/reactivex/functions/Action;

    invoke-static {p1}, Lio/reactivex/Completable;->fromAction(Lio/reactivex/functions/Action;)Lio/reactivex/Completable;

    move-result-object p1

    const-string p2, "Completable.fromAction { flow.goBack() }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 30
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult$LinkLater;

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult$LinkFailure;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult$LinkFailure;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_0
    new-instance p1, Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler$onDepositOptionsResult$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler$onDepositOptionsResult$2;-><init>(Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler;)V

    check-cast p1, Lio/reactivex/functions/Action;

    invoke-static {p1}, Lio/reactivex/Completable;->fromAction(Lio/reactivex/functions/Action;)Lio/reactivex/Completable;

    move-result-object p1

    const-string p2, "Completable.fromAction {\u2026pScreen\n        )\n      }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 35
    :cond_2
    instance-of p1, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult$LinkingComplete;

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler;->setupGuideIntegrationRunner:Lcom/squareup/setupguide/SetupGuideIntegrationRunner;

    .line 36
    sget-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->LINK_YOUR_BANK_ACCOUNT:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 37
    sget-object v1, Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler$onDepositOptionsResult$3;->INSTANCE:Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler$onDepositOptionsResult$3;

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 35
    invoke-interface {p1, v0, p2, v1}, Lcom/squareup/setupguide/SetupGuideIntegrationRunner;->handleCompletion(Lcom/squareup/protos/checklist/common/ActionItemName;Lcom/squareup/ui/main/RegisterTreeKey;Lkotlin/jvm/functions/Function0;)Lio/reactivex/Completable;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
