.class final Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$linkDebitCard$1;
.super Ljava/lang/Object;
.source "DepositOptionsReactor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->linkDebitCard(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
        "it",
        "Lcom/squareup/debitcard/DebitCardSettings$State;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$linkDebitCard$1;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/debitcard/DebitCardSettings$State;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 389
    iget-object p1, p1, Lcom/squareup/debitcard/DebitCardSettings$State;->linkCardState:Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;

    sget-object v0, Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;->SUCCESS:Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;

    if-ne p1, v0, :cond_0

    new-instance p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$SettingUpSameDayDeposit;

    .line 390
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$linkDebitCard$1;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;->getBankSuccess()Z

    move-result v0

    .line 389
    invoke-direct {p1, v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$SettingUpSameDayDeposit;-><init>(Z)V

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto :goto_0

    .line 392
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$linkDebitCard$1;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;->getBankSuccess()Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardFailure;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardFailure;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto :goto_0

    .line 393
    :cond_1
    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardFailure;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardFailure;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 81
    check-cast p1, Lcom/squareup/debitcard/DebitCardSettings$State;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$linkDebitCard$1;->apply(Lcom/squareup/debitcard/DebitCardSettings$State;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    move-result-object p1

    return-object p1
.end method
