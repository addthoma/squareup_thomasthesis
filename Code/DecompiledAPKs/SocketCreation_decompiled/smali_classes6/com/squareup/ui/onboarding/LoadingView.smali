.class public Lcom/squareup/ui/onboarding/LoadingView;
.super Landroid/widget/LinearLayout;
.source "LoadingView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field private final failurePopup:Lcom/squareup/caller/FailurePopup;

.field presenter:Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const-class p2, Lcom/squareup/ui/onboarding/LoadingScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/onboarding/LoadingScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/onboarding/LoadingScreen$Component;->inject(Lcom/squareup/ui/onboarding/LoadingView;)V

    .line 22
    new-instance p2, Lcom/squareup/caller/FailurePopup;

    invoke-direct {p2, p1}, Lcom/squareup/caller/FailurePopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/LoadingView;->failurePopup:Lcom/squareup/caller/FailurePopup;

    return-void
.end method


# virtual methods
.method public onBackPressed()Z
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/onboarding/LoadingView;->presenter:Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/onboarding/LoadingView;->presenter:Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;->serverCallPresenter:Lcom/squareup/ui/onboarding/LoadingStatusPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/LoadingView;->failurePopup:Lcom/squareup/caller/FailurePopup;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/onboarding/LoadingView;->presenter:Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 36
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 26
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/onboarding/LoadingView;->presenter:Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/onboarding/LoadingView;->presenter:Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;->serverCallPresenter:Lcom/squareup/ui/onboarding/LoadingStatusPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/LoadingView;->failurePopup:Lcom/squareup/caller/FailurePopup;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
