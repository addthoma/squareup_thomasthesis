.class Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter$1;
.super Lcom/squareup/mortar/PopupPresenter;
.source "ConfirmMagstripeAddressScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/register/widgets/Confirmation;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;)V
    .locals 0

    .line 89
    iput-object p1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter$1;->this$0:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;

    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected onPopupResult(Ljava/lang/Boolean;)V
    .locals 1

    .line 91
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 92
    iget-object p1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter$1;->this$0:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;

    invoke-static {p1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->access$000(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;)Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/onboarding/PersonalInfoFragment$ReaderOptOutTask;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/PersonalInfoFragment$ReaderOptOutTask;-><init>()V

    invoke-interface {p1, v0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    .line 93
    iget-object p1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter$1;->this$0:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;

    invoke-static {p1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->access$100(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;)Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onShippedOrSkippedMagstripeReader()V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 89
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter$1;->onPopupResult(Ljava/lang/Boolean;)V

    return-void
.end method
