.class public Lcom/squareup/ui/reader_deprecation/O1ReminderEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "O1ReminderEvent.java"


# static fields
.field public static final NEGATIVE:Ljava/lang/String; = "OK"

.field public static final POSITIVE:Ljava/lang/String; = "Order a Reader"

.field private static final VALUE:Ljava/lang/String; = "O1 Reader Deprecation Notice"


# instance fields
.field public final detail:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .line 16
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v1, "O1 Reader Deprecation Notice"

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 17
    iput-object p1, p0, Lcom/squareup/ui/reader_deprecation/O1ReminderEvent;->detail:Ljava/lang/String;

    return-void
.end method
