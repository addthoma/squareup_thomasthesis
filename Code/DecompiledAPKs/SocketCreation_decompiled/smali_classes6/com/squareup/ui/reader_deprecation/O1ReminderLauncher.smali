.class public Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;
.super Lcom/squareup/ui/main/DailyContentLauncher;
.source "O1ReminderLauncher.java"


# instance fields
.field private final flow:Lflow/Flow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;Lcom/squareup/settings/LocalSetting;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lflow/Flow;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            "Lflow/Flow;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    new-instance v0, Lcom/squareup/ui/reader_deprecation/-$$Lambda$O1ReminderLauncher$eBiTwrsQXPVBrj8hGEGtAgVfhcE;

    invoke-direct {v0, p1}, Lcom/squareup/ui/reader_deprecation/-$$Lambda$O1ReminderLauncher$eBiTwrsQXPVBrj8hGEGtAgVfhcE;-><init>(Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;)V

    invoke-direct {p0, v0, p2, p3}, Lcom/squareup/ui/main/DailyContentLauncher;-><init>(Ljavax/inject/Provider;Lcom/squareup/settings/LocalSetting;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V

    .line 20
    iput-object p4, p0, Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;->flow:Lflow/Flow;

    return-void
.end method

.method static synthetic lambda$new$0(Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;)Ljava/lang/Boolean;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;->getLastInputType()Lcom/squareup/Card$InputType;

    move-result-object p0

    sget-object v0, Lcom/squareup/Card$InputType;->O1_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method protected showContent()V
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/reader_deprecation/O1ReminderDialogScreen;

    invoke-direct {v1}, Lcom/squareup/ui/reader_deprecation/O1ReminderDialogScreen;-><init>()V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
