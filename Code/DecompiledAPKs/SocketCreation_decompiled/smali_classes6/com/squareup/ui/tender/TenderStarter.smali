.class public interface abstract Lcom/squareup/ui/tender/TenderStarter;
.super Ljava/lang/Object;
.source "TenderStarter.java"

# interfaces
.implements Lmortar/Scoped;


# virtual methods
.method public abstract advanceToNextFlow(Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;)Z
.end method

.method public abstract completeTenderAndAdvance(Z)Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract ensurePipTenderFlowIsShowing()V
.end method

.method public abstract goToSplitTender()V
.end method

.method public abstract inTenderFlow(Lcom/squareup/container/ContainerTreeKey;)Z
.end method

.method public abstract showNfcWarningScreen(Lcom/squareup/container/ContainerTreeKey;)V
.end method

.method public abstract startOrResumeTenderFlowAtPaymentPrompt()V
.end method

.method public abstract startTenderFlow()V
.end method

.method public abstract startTenderFlowForPaymentConfig(Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;)V
.end method

.method public abstract startTenderFlowWithSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
.end method

.method public abstract warnIfNfcEnabled()Z
.end method
