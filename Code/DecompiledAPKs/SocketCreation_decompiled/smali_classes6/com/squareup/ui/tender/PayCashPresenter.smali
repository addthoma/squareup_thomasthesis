.class public Lcom/squareup/ui/tender/PayCashPresenter;
.super Lcom/squareup/mortar/ContextPresenter;
.source "PayCashPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/tender/PayCashPresenter$StartEditingCashEvent;,
        Lcom/squareup/ui/tender/PayCashPresenter$CustomCashTenderClicked;,
        Lcom/squareup/ui/tender/PayCashPresenter$QuickCashEvent;,
        Lcom/squareup/ui/tender/PayCashPresenter$SharedScope;,
        Lcom/squareup/ui/tender/PayCashPresenter$View;,
        Lcom/squareup/ui/tender/PayCashPresenter$ScreenData;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/ContextPresenter<",
        "Lcom/squareup/ui/tender/PayCashPresenter$View;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final isTablet:Z

.field private final quickCashCalculator:Lcom/squareup/money/QuickCashCalculator;

.field private final res:Lcom/squareup/util/Res;

.field private final scopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

.field private final softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

.field private final tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/money/QuickCashCalculator;Lcom/squareup/util/Device;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/tenderpayment/TenderScopeRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 93
    invoke-direct {p0}, Lcom/squareup/mortar/ContextPresenter;-><init>()V

    .line 94
    iput-object p1, p0, Lcom/squareup/ui/tender/PayCashPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 95
    iput-object p3, p0, Lcom/squareup/ui/tender/PayCashPresenter;->res:Lcom/squareup/util/Res;

    .line 96
    iput-object p2, p0, Lcom/squareup/ui/tender/PayCashPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 97
    iput-object p4, p0, Lcom/squareup/ui/tender/PayCashPresenter;->quickCashCalculator:Lcom/squareup/money/QuickCashCalculator;

    .line 98
    invoke-interface {p5}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/tender/PayCashPresenter;->isTablet:Z

    .line 99
    iput-object p6, p0, Lcom/squareup/ui/tender/PayCashPresenter;->softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

    .line 100
    iput-object p7, p0, Lcom/squareup/ui/tender/PayCashPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    .line 101
    iput-object p8, p0, Lcom/squareup/ui/tender/PayCashPresenter;->tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

    .line 102
    iput-object p9, p0, Lcom/squareup/ui/tender/PayCashPresenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    .line 103
    iput-object p10, p0, Lcom/squareup/ui/tender/PayCashPresenter;->scopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    return-void
.end method

.method private getCustomAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 175
    invoke-virtual {p0}, Lcom/squareup/ui/tender/PayCashPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/PayCashPresenter$View;

    invoke-interface {v0}, Lcom/squareup/ui/tender/PayCashPresenter$View;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-nez v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getRoundedRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private isAmountEnough(Lcom/squareup/protos/common/Money;)Z
    .locals 6

    .line 183
    invoke-static {p1}, Lcom/squareup/payment/SwedishRounding;->isRequired(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getRoundedRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 185
    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object p1, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long p1, v2, v4

    if-ltz p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method private onTenderCash(Lcom/squareup/protos/common/Money;)V
    .locals 2

    .line 143
    invoke-direct {p0, p1}, Lcom/squareup/ui/tender/PayCashPresenter;->isAmountEnough(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 144
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/tender/PayCashPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/PayCashPresenter$View;

    invoke-interface {v0}, Lcom/squareup/ui/tender/PayCashPresenter$View;->hideSoftKeyboard()V

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashPresenter;->scopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/tender/PayCashPresenter;->tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

    invoke-interface {v1, p1}, Lcom/squareup/tenderpayment/TenderCompleter;->payCashTender(Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/tenderpayment/TenderScopeRunner;->advanceToNextFlow(Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;)Z

    return-void
.end method

.method private updateX2()V
    .locals 5

    .line 162
    invoke-virtual {p0}, Lcom/squareup/ui/tender/PayCashPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/PayCashPresenter$View;

    invoke-interface {v0}, Lcom/squareup/ui/tender/PayCashPresenter$View;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 164
    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    sget-object v1, Lcom/squareup/comms/protos/common/TenderType;->CASH:Lcom/squareup/comms/protos/common/TenderType;

    invoke-interface {v0, v1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->configuringTender(Lcom/squareup/comms/protos/common/TenderType;)Z

    goto :goto_0

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashPresenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, p0, Lcom/squareup/ui/tender/PayCashPresenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v1}, Lcom/squareup/payment/TenderInEdit;->requireBaseTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->promptForPayment(Lcom/squareup/payment/tender/BaseTender$Builder;)Z

    goto :goto_0

    .line 169
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->promptForPayment()Z

    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method getOrderIdentifier()Ljava/lang/String;
    .locals 1

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->orderIdentifier()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onEditAmountBegin()V
    .locals 3

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/tender/PayCashPresenter$StartEditingCashEvent;

    invoke-virtual {p0}, Lcom/squareup/ui/tender/PayCashPresenter;->getOrderIdentifier()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/squareup/ui/tender/PayCashPresenter$StartEditingCashEvent;-><init>(Lcom/squareup/ui/tender/PayCashPresenter;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 5

    .line 107
    invoke-super {p0, p1}, Lcom/squareup/mortar/ContextPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 109
    invoke-virtual {p0}, Lcom/squareup/ui/tender/PayCashPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/PayCashPresenter$View;

    invoke-interface {v0}, Lcom/squareup/ui/tender/PayCashPresenter$View;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 115
    iget-object v1, p0, Lcom/squareup/ui/tender/PayCashPresenter;->scopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    iget-object v2, p0, Lcom/squareup/ui/tender/PayCashPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/ui/tender/legacy/R$string;->pay_cash_title:I

    .line 116
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/tender/PayCashPresenter;->scopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    .line 117
    invoke-interface {v3}, Lcom/squareup/tenderpayment/TenderScopeRunner;->getFormattedTotal()Ljava/lang/CharSequence;

    move-result-object v3

    const-string v4, "amount"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x0

    .line 115
    invoke-interface {v1, v2, v3}, Lcom/squareup/tenderpayment/TenderScopeRunner;->buildTenderActionBarConfig(Ljava/lang/CharSequence;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 118
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 119
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/noho/R$drawable;->noho_white_border_bottom_light_gray_1px:I

    .line 120
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setBackground(I)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 121
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 115
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    :cond_0
    if-nez p1, :cond_1

    .line 125
    invoke-virtual {p0}, Lcom/squareup/ui/tender/PayCashPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/PayCashPresenter$View;

    invoke-interface {p1}, Lcom/squareup/ui/tender/PayCashPresenter$View;->requestInitialFocus()V

    .line 127
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/tender/PayCashPresenter;->update()V

    return-void
.end method

.method onQuickOptionClicked(Lcom/squareup/protos/common/Money;I)V
    .locals 4

    .line 138
    invoke-direct {p0, p1}, Lcom/squareup/ui/tender/PayCashPresenter;->onTenderCash(Lcom/squareup/protos/common/Money;)V

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/tender/PayCashPresenter$QuickCashEvent;

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, p2, v2, v3}, Lcom/squareup/ui/tender/PayCashPresenter$QuickCashEvent;-><init>(IJ)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method onTenderButtonClicked()V
    .locals 2

    .line 131
    invoke-direct {p0}, Lcom/squareup/ui/tender/PayCashPresenter;->getCustomAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 132
    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/PayCashPresenter;->onTenderCash(Lcom/squareup/protos/common/Money;)V

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PAYMENT_FLOW_QUICK_CASH_CUSTOM_OPTION:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/tender/PayCashPresenter$CustomCashTenderClicked;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/PayCashPresenter$CustomCashTenderClicked;-><init>(Lcom/squareup/ui/tender/PayCashPresenter;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method onWindowFocusChanged(Z)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 190
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/tender/PayCashPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->asAuthPayment()Lcom/squareup/payment/RequiresAuthorization;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 192
    invoke-interface {p1}, Lcom/squareup/payment/RequiresAuthorization;->hasRequestedAuthorization()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 193
    iget-object p1, p0, Lcom/squareup/ui/tender/PayCashPresenter;->softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

    sget-object v0, Lcom/squareup/workflow/SoftInputMode;->HIDDEN:Lcom/squareup/workflow/SoftInputMode;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/SoftInputPresenter;->setSoftInputMode(Lcom/squareup/workflow/SoftInputMode;)V

    :cond_1
    return-void
.end method

.method update()V
    .locals 6

    .line 149
    invoke-direct {p0}, Lcom/squareup/ui/tender/PayCashPresenter;->getCustomAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 151
    iget-object v1, p0, Lcom/squareup/ui/tender/PayCashPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getRoundedRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 152
    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/PayCashPresenter;->isAmountEnough(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    .line 153
    iget-object v2, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-wide v4, Lcom/squareup/money/MaxMoneyScrubber;->MAX_MONEY:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iget-object v4, v1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v2, v3, v4}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 154
    iget-boolean v3, p0, Lcom/squareup/ui/tender/PayCashPresenter;->isTablet:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/squareup/ui/tender/PayCashPresenter;->quickCashCalculator:Lcom/squareup/money/QuickCashCalculator;

    .line 155
    invoke-virtual {v3, v1}, Lcom/squareup/money/QuickCashCalculator;->buildQuickCashOptions(Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object v3

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    .line 157
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/tender/PayCashPresenter;->getView()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/ui/tender/PayCashPresenter$View;

    new-instance v5, Lcom/squareup/ui/tender/PayCashPresenter$ScreenData;

    invoke-direct {v5, v1, v0, v2, v3}, Lcom/squareup/ui/tender/PayCashPresenter$ScreenData;-><init>(Lcom/squareup/protos/common/Money;ZLcom/squareup/protos/common/Money;Ljava/util/List;)V

    invoke-interface {v4, v5}, Lcom/squareup/ui/tender/PayCashPresenter$View;->update(Lcom/squareup/ui/tender/PayCashPresenter$ScreenData;)V

    .line 158
    invoke-direct {p0}, Lcom/squareup/ui/tender/PayCashPresenter;->updateX2()V

    return-void
.end method
