.class public Lcom/squareup/ui/tender/PayCardSwipeOnlyView;
.super Landroid/widget/LinearLayout;
.source "PayCardSwipeOnlyView.java"


# instance fields
.field private actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

.field presenter:Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const-class p2, Lcom/squareup/ui/tender/PayCardSwipeOnlyScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/PayCardSwipeOnlyScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/tender/PayCardSwipeOnlyScreen$Component;->inject(Lcom/squareup/ui/tender/PayCardSwipeOnlyView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 39
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/tender/PayCardSwipeOnlyView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCardSwipeOnlyView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 24
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 25
    invoke-direct {p0}, Lcom/squareup/ui/tender/PayCardSwipeOnlyView;->bindViews()V

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCardSwipeOnlyView;->presenter:Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCardSwipeOnlyView;->presenter:Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;->dropView(Ljava/lang/Object;)V

    .line 31
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method
