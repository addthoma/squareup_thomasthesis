.class public Lcom/squareup/ui/tender/TenderScope$Module;
.super Ljava/lang/Object;
.source "TenderScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/TenderScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public provideCancelSplitTenderDialogScreen(Lcom/squareup/tenderpayment/TenderScopeRunner;)Lcom/squareup/workflow/legacy/Screen;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;",
            "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;",
            ">;"
        }
    .end annotation

    .line 60
    invoke-interface {p1}, Lcom/squareup/tenderpayment/TenderScopeRunner;->asCancelSplitTenderDialogScreen()Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method
