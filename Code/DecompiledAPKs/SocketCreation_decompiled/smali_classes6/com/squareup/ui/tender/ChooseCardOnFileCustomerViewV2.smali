.class public Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;
.super Landroid/widget/LinearLayout;
.source "ChooseCardOnFileCustomerViewV2.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

.field private final onSearchTermChanged:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private searchBox:Lcom/squareup/ui/XableEditText;

.field private searchMessage:Lcom/squareup/widgets/MessageView;

.field private searchProgressBar:Landroid/widget/ProgressBar;

.field private final shortAnimTimeMs:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->onSearchTermChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 46
    const-class p2, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Component;->inject(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)V

    .line 48
    invoke-virtual {p0}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 p2, 0x10e0000

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->shortAnimTimeMs:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->onSearchTermChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)Lcom/squareup/ui/XableEditText;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->searchBox:Lcom/squareup/ui/XableEditText;

    return-object p0
.end method

.method private bindViews()V
    .locals 1

    .line 117
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 118
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->choose_cof_customer_search_box:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->searchBox:Lcom/squareup/ui/XableEditText;

    .line 119
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->choose_cof_customer_contact_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    iput-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    .line 120
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->choose_cof_customer_search_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->searchMessage:Lcom/squareup/widgets/MessageView;

    .line 121
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->choose_cof_customer_search_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->searchProgressBar:Landroid/widget/ProgressBar;

    return-void
.end method


# virtual methods
.method contactList()Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 78
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->presenter:Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->presenter:Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->dropView(Ljava/lang/Object;)V

    .line 84
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 52
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 53
    invoke-direct {p0}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->bindViews()V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->searchBox:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2$1;-><init>(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->searchBox:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2$2;-><init>(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->searchBox:Lcom/squareup/ui/XableEditText;

    .line 74
    invoke-virtual {p0}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/crm/R$string;->crm_search_customers_hint:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 73
    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setHint(Ljava/lang/String;)V

    return-void
.end method

.method onSearchTermChanged()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->onSearchTermChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method showContactList(Z)V
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showMessage(ZLjava/lang/CharSequence;)V
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->searchMessage:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    iget-object p2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->searchMessage:Lcom/squareup/widgets/MessageView;

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showProgress(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 93
    iget-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->searchProgressBar:Landroid/widget/ProgressBar;

    iget v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 95
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->searchProgressBar:Landroid/widget/ProgressBar;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    return-void
.end method
