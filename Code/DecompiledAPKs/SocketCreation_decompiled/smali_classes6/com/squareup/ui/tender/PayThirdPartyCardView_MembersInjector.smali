.class public final Lcom/squareup/ui/tender/PayThirdPartyCardView_MembersInjector;
.super Ljava/lang/Object;
.source "PayThirdPartyCardView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/tender/PayThirdPartyCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView_MembersInjector;->tenderScopeRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/tender/PayThirdPartyCardView;",
            ">;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/ui/tender/PayThirdPartyCardView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/tender/PayThirdPartyCardView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/ui/tender/PayThirdPartyCardView;Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->presenter:Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;

    return-void
.end method

.method public static injectTenderScopeRunner(Lcom/squareup/ui/tender/PayThirdPartyCardView;Lcom/squareup/tenderpayment/TenderScopeRunner;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/tender/PayThirdPartyCardView;)V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/tender/PayThirdPartyCardView_MembersInjector;->injectPresenter(Lcom/squareup/ui/tender/PayThirdPartyCardView;Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardView_MembersInjector;->tenderScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/TenderScopeRunner;

    invoke-static {p1, v0}, Lcom/squareup/ui/tender/PayThirdPartyCardView_MembersInjector;->injectTenderScopeRunner(Lcom/squareup/ui/tender/PayThirdPartyCardView;Lcom/squareup/tenderpayment/TenderScopeRunner;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/tender/PayThirdPartyCardView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/tender/PayThirdPartyCardView_MembersInjector;->injectMembers(Lcom/squareup/ui/tender/PayThirdPartyCardView;)V

    return-void
.end method
