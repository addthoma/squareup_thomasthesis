.class public Lcom/squareup/ui/tender/ChooseCardOnFileView;
.super Landroid/widget/LinearLayout;
.source "ChooseCardOnFileView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field cardOnFileRowView:Lcom/squareup/ui/tender/ChooseCardOnFileRowView;

.field customerCardsLabel:Landroid/widget/TextView;

.field helpTextView:Lcom/squareup/widgets/MessageView;

.field presenter:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    const-class p2, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Component;->inject(Lcom/squareup/ui/tender/ChooseCardOnFileView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 75
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->choose_card_on_file_cards_label:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileView;->customerCardsLabel:Landroid/widget/TextView;

    .line 76
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->choose_card_on_file_cards:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/ChooseCardOnFileRowView;

    iput-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileView;->cardOnFileRowView:Lcom/squareup/ui/tender/ChooseCardOnFileRowView;

    .line 77
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->choose_card_on_file_help_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileView;->helpTextView:Lcom/squareup/widgets/MessageView;

    return-void
.end method


# virtual methods
.method public addCardOnFileCard(IZLcom/squareup/Card$Brand;Ljava/lang/String;Z)V
    .locals 7

    if-eqz p3, :cond_0

    .line 59
    invoke-static {p4}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 60
    iget-object v1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileView;->cardOnFileRowView:Lcom/squareup/ui/tender/ChooseCardOnFileRowView;

    move v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/squareup/ui/tender/ChooseCardOnFileRowView;->addCardRow(IZLcom/squareup/Card$Brand;Ljava/lang/String;Z)V

    goto :goto_0

    .line 62
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileView;->presenter:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->cardType()Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    move-result-object p1

    sget-object p2, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;->GIFT_CARD:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    if-ne p1, p2, :cond_1

    .line 63
    iget-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileView;->cardOnFileRowView:Lcom/squareup/ui/tender/ChooseCardOnFileRowView;

    invoke-virtual {p1}, Lcom/squareup/ui/tender/ChooseCardOnFileRowView;->addNoGiftCardsRow()V

    goto :goto_0

    .line 65
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileView;->cardOnFileRowView:Lcom/squareup/ui/tender/ChooseCardOnFileRowView;

    invoke-virtual {p1}, Lcom/squareup/ui/tender/ChooseCardOnFileRowView;->addNoCardsRow()V

    :goto_0
    return-void
.end method

.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 35
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public hideHelperText()V
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileView;->helpTextView:Lcom/squareup/widgets/MessageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 44
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileView;->presenter:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileView;->presenter:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 50
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .line 39
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 40
    invoke-direct {p0}, Lcom/squareup/ui/tender/ChooseCardOnFileView;->bindViews()V

    return-void
.end method

.method public showCustomerCardsLabel(Ljava/lang/CharSequence;)V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileView;->customerCardsLabel:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showHelpText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileView;->helpTextView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
