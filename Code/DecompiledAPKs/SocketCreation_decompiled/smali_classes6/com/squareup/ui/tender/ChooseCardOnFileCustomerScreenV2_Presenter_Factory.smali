.class public final Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;
.super Ljava/lang/Object;
.source "ChooseCardOnFileCustomerScreenV2_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final contactLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final formatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodexGroupLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodexServiceHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final x2SellerScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;)V"
        }
    .end annotation

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;->contactLoaderProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p3, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p4, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p5, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;->rolodexServiceHelperProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p6, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;->rolodexGroupLoaderProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p7, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p8, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p9, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p10, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;->formatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;)",
            "Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;"
        }
    .end annotation

    .line 80
    new-instance v11, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/crm/RolodexContactLoader;Lflow/Flow;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/analytics/Analytics;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/text/Formatter;)Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/RolodexContactLoader;",
            "Lflow/Flow;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            "Lcom/squareup/payment/TenderInEdit;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;"
        }
    .end annotation

    .line 88
    new-instance v11, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;-><init>(Lcom/squareup/crm/RolodexContactLoader;Lflow/Flow;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/analytics/Analytics;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/text/Formatter;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;
    .locals 11

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;->contactLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/crm/RolodexContactLoader;

    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;->rolodexServiceHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;->rolodexGroupLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/crm/RolodexGroupLoader;

    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/payment/TenderInEdit;

    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;->formatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/text/Formatter;

    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;->newInstance(Lcom/squareup/crm/RolodexContactLoader;Lflow/Flow;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/analytics/Analytics;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/text/Formatter;)Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2_Presenter_Factory;->get()Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;

    move-result-object v0

    return-object v0
.end method
