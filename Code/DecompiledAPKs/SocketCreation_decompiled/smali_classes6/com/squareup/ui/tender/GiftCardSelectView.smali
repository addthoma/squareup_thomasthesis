.class public Lcom/squareup/ui/tender/GiftCardSelectView;
.super Landroid/widget/LinearLayout;
.source "GiftCardSelectView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/ui/tender/GiftCardSelectPresenter$GiftCardView;


# instance fields
.field private actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

.field private cardOnFileRowView:Lcom/squareup/marin/widgets/MarinGlyphTextView;

.field private helperTextView:Lcom/squareup/widgets/MessageView;

.field presenter:Lcom/squareup/ui/tender/GiftCardSelectPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private scanGiftCardRowView:Lcom/squareup/marin/widgets/MarinGlyphTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    const-class p2, Lcom/squareup/ui/tender/GiftCardSelectScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/GiftCardSelectScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/tender/GiftCardSelectScreen$Component;->inject(Lcom/squareup/ui/tender/GiftCardSelectView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 58
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/tender/GiftCardSelectView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    .line 59
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->choose_gift_card_on_file:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphTextView;

    iput-object v0, p0, Lcom/squareup/ui/tender/GiftCardSelectView;->cardOnFileRowView:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 60
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->enter_card_number_or_swipe:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphTextView;

    iput-object v0, p0, Lcom/squareup/ui/tender/GiftCardSelectView;->scanGiftCardRowView:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 61
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->gift_card_purchase_hint:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/tender/GiftCardSelectView;->helperTextView:Lcom/squareup/widgets/MessageView;

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/tender/GiftCardSelectView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 39
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 40
    invoke-direct {p0}, Lcom/squareup/ui/tender/GiftCardSelectView;->bindViews()V

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/tender/GiftCardSelectView;->cardOnFileRowView:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    new-instance v1, Lcom/squareup/ui/tender/GiftCardSelectView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/GiftCardSelectView$1;-><init>(Lcom/squareup/ui/tender/GiftCardSelectView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/tender/GiftCardSelectView;->scanGiftCardRowView:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    new-instance v1, Lcom/squareup/ui/tender/GiftCardSelectView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/GiftCardSelectView$2;-><init>(Lcom/squareup/ui/tender/GiftCardSelectView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/tender/GiftCardSelectView;->presenter:Lcom/squareup/ui/tender/GiftCardSelectPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/tender/GiftCardSelectPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/tender/GiftCardSelectView;->presenter:Lcom/squareup/ui/tender/GiftCardSelectPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/tender/GiftCardSelectPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method public setHelperText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/tender/GiftCardSelectView;->helperTextView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
