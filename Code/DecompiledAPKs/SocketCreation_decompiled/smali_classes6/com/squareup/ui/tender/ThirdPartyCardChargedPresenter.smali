.class public Lcom/squareup/ui/tender/ThirdPartyCardChargedPresenter;
.super Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;
.source "ThirdPartyCardChargedPresenter.java"


# instance fields
.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/tenderpayment/TenderScopeRunner;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/payment/TenderInEdit;",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;-><init>(Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V

    .line 26
    iput-object p4, p0, Lcom/squareup/ui/tender/ThirdPartyCardChargedPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 27
    iput-object p6, p0, Lcom/squareup/ui/tender/ThirdPartyCardChargedPresenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    .line 28
    iput-object p5, p0, Lcom/squareup/ui/tender/ThirdPartyCardChargedPresenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    return-void
.end method


# virtual methods
.method getAmountDue()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/tender/ThirdPartyCardChargedPresenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireOtherTender()Lcom/squareup/payment/tender/OtherTender$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/OtherTender$Builder;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()Z
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/tender/ThirdPartyCardChargedPresenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearOtherTender()Lcom/squareup/payment/tender/OtherTender$Builder;

    const/4 v0, 0x0

    return v0
.end method

.method record(Lcom/squareup/protos/common/Money;)V
    .locals 3

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/tender/ThirdPartyCardChargedPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    .line 42
    iget-object v1, p0, Lcom/squareup/ui/tender/ThirdPartyCardChargedPresenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v1}, Lcom/squareup/payment/TenderInEdit;->requireOtherTender()Lcom/squareup/payment/tender/OtherTender$Builder;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v2, 0x0

    .line 44
    invoke-virtual {v0, p1, v2}, Lcom/squareup/payment/BillPayment;->setTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V

    .line 47
    :cond_0
    invoke-virtual {v0, v1}, Lcom/squareup/payment/BillPayment;->addTender(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/payment/tender/BaseTender;

    move-result-object p1

    check-cast p1, Lcom/squareup/payment/tender/OtherTender;

    .line 48
    invoke-virtual {v0, p1}, Lcom/squareup/payment/BillPayment;->shouldCallAuthOnTender(Lcom/squareup/payment/tender/BaseTender;)Z

    move-result p1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/tender/ThirdPartyCardChargedPresenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearOtherTender()Lcom/squareup/payment/tender/OtherTender$Builder;

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/tender/ThirdPartyCardChargedPresenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    invoke-interface {v0, p1}, Lcom/squareup/tenderpayment/TenderScopeRunner;->completeTenderAndAdvance(Z)Z

    return-void
.end method
