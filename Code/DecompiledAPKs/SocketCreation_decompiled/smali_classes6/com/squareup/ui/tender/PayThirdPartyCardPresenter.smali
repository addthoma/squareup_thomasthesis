.class Lcom/squareup/ui/tender/PayThirdPartyCardPresenter;
.super Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;
.source "PayThirdPartyCardPresenter.java"


# instance fields
.field private note:Ljava/lang/String;

.field private final tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

.field private final tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private type:Lcom/squareup/server/account/protos/OtherTenderType;


# direct methods
.method constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/tenderpayment/TenderScopeRunner;Lcom/squareup/tenderpayment/TenderCompleter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 29
    invoke-direct {p0, p2, p3, p4}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;-><init>(Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/tender/PayThirdPartyCardPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 31
    iput-object p5, p0, Lcom/squareup/ui/tender/PayThirdPartyCardPresenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    .line 32
    iput-object p6, p0, Lcom/squareup/ui/tender/PayThirdPartyCardPresenter;->tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

    return-void
.end method


# virtual methods
.method getAmountDue()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 36
    invoke-super {p0, p1}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 37
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/PayThirdPartyCardScreen;

    .line 38
    iget-object v0, p1, Lcom/squareup/ui/tender/PayThirdPartyCardScreen;->type:Lcom/squareup/server/account/protos/OtherTenderType;

    iput-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardPresenter;->type:Lcom/squareup/server/account/protos/OtherTenderType;

    .line 39
    iget-object p1, p1, Lcom/squareup/ui/tender/PayThirdPartyCardScreen;->note:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/tender/PayThirdPartyCardPresenter;->note:Ljava/lang/String;

    return-void
.end method

.method record(Lcom/squareup/protos/common/Money;)V
    .locals 4

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->startSingleTenderBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 50
    iget-object v1, p0, Lcom/squareup/ui/tender/PayThirdPartyCardPresenter;->tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

    iget-object v2, p0, Lcom/squareup/ui/tender/PayThirdPartyCardPresenter;->note:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/ui/tender/PayThirdPartyCardPresenter;->type:Lcom/squareup/server/account/protos/OtherTenderType;

    .line 51
    invoke-interface {v1, v2, v3, v0, p1}, Lcom/squareup/tenderpayment/TenderCompleter;->payOtherTender(Ljava/lang/String;Lcom/squareup/server/account/protos/OtherTenderType;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/tender/PayThirdPartyCardPresenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    invoke-interface {v0, p1}, Lcom/squareup/tenderpayment/TenderScopeRunner;->advanceToNextFlow(Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;)Z

    return-void
.end method
