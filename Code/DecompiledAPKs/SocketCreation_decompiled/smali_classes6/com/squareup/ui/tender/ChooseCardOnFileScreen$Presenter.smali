.class public Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "ChooseCardOnFileScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/ChooseCardOnFileScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/tender/ChooseCardOnFileView;",
        ">;"
    }
.end annotation


# static fields
.field private static final NO_CARD_INDEX:I = -0x1


# instance fields
.field private availableInstruments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;"
        }
    .end annotation
.end field

.field private cardTypesToSelect:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

.field private final cnpFeesMessageHelper:Lcom/squareup/cnp/CnpFeesMessageHelper;

.field private final expirationHelper:Lcom/squareup/card/ExpirationHelper;

.field private final flow:Lflow/Flow;

.field private forCustomerInTransaction:Z

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final res:Lcom/squareup/util/Res;

.field private final tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/payment/Transaction;Ljavax/inject/Provider;Lcom/squareup/card/ExpirationHelper;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/cnp/CnpFeesMessageHelper;Lcom/squareup/tenderpayment/TenderScopeRunner;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lflow/Flow;",
            "Lcom/squareup/payment/Transaction;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/card/ExpirationHelper;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/cnp/CnpFeesMessageHelper;",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 101
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 102
    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 103
    iput-object p2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->flow:Lflow/Flow;

    .line 104
    iput-object p3, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 105
    iput-object p4, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->localeProvider:Ljavax/inject/Provider;

    .line 106
    iput-object p5, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->expirationHelper:Lcom/squareup/card/ExpirationHelper;

    .line 107
    iput-object p6, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 108
    iput-object p7, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->cnpFeesMessageHelper:Lcom/squareup/cnp/CnpFeesMessageHelper;

    .line 109
    iput-object p8, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;)Lflow/Flow;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->flow:Lflow/Flow;

    return-object p0
.end method

.method private buildActionBar()V
    .locals 4

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->cardTypesToSelect:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    sget-object v1, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;->GIFT_CARD:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    if-ne v0, v1, :cond_0

    sget v0, Lcom/squareup/ui/tender/legacy/R$string;->gift_card_on_file_action_bar_title:I

    goto :goto_0

    :cond_0
    sget v0, Lcom/squareup/ui/tender/legacy/R$string;->pay_card_on_file_title:I

    .line 167
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    iget-object v2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 168
    invoke-interface {v2, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    .line 169
    invoke-interface {v2}, Lcom/squareup/tenderpayment/TenderScopeRunner;->getFormattedTotal()Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, "amount"

    invoke-virtual {v0, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v2, 0x0

    .line 167
    invoke-interface {v1, v0, v2}, Lcom/squareup/tenderpayment/TenderScopeRunner;->buildTenderActionBarConfig(Ljava/lang/CharSequence;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 169
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileScreen$Presenter$dWDxOoExRHxyvWiNwwaWeKbuHvs;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileScreen$Presenter$dWDxOoExRHxyvWiNwwaWeKbuHvs;-><init>(Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;)V

    .line 170
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 171
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 172
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 173
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideSecondaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 174
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 176
    invoke-virtual {p0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/tender/ChooseCardOnFileView;

    invoke-virtual {v1}, Lcom/squareup/ui/tender/ChooseCardOnFileView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private cancel()V
    .locals 2

    .line 230
    iget-boolean v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->forCustomerInTransaction:Z

    if-nez v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1, v1}, Lcom/squareup/payment/Transaction;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public static synthetic lambda$dWDxOoExRHxyvWiNwwaWeKbuHvs(Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->cancel()V

    return-void
.end method

.method private updateCustomer()V
    .locals 4

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCustomerDisplayNameOrBlank()Ljava/lang/String;

    move-result-object v0

    .line 182
    iget-object v1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->cardTypesToSelect:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    sget-object v2, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;->GIFT_CARD:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    if-ne v1, v2, :cond_0

    .line 183
    sget v1, Lcom/squareup/ui/tender/legacy/R$string;->uppercase_gift_card_on_file:I

    goto :goto_0

    .line 185
    :cond_0
    sget v1, Lcom/squareup/ui/tender/legacy/R$string;->uppercase_cards_on_file:I

    .line 187
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/tender/ChooseCardOnFileView;

    iget-object v3, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-interface {v3, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v3, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->localeProvider:Ljavax/inject/Provider;

    .line 188
    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "name"

    invoke-virtual {v1, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 189
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 187
    invoke-virtual {v2, v0}, Lcom/squareup/ui/tender/ChooseCardOnFileView;->showCustomerCardsLabel(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateCustomerCardsOnFile()V
    .locals 9

    .line 193
    invoke-virtual {p0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/ChooseCardOnFileView;

    .line 194
    iget-object v1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->availableInstruments:Ljava/util/List;

    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 199
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->availableInstruments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v8, v1, :cond_1

    .line 200
    iget-object v1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->availableInstruments:Ljava/util/List;

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    .line 202
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    .line 203
    invoke-static {v4}, Lcom/squareup/cardonfile/StoredInstrumentHelper;->formatNameAndNumber(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;)Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v3, v7

    const-string v4, "%s"

    .line 202
    invoke-static {v2, v4, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 205
    iget-object v2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->expirationHelper:Lcom/squareup/card/ExpirationHelper;

    iget-object v3, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iget-object v4, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    invoke-virtual {v2, v3, v4}, Lcom/squareup/card/ExpirationHelper;->isExpired(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;)Z

    move-result v6

    const/4 v3, 0x1

    .line 207
    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-static {v1}, Lcom/squareup/card/CardBrands;->toCardBrand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/Card$Brand;

    move-result-object v4

    move-object v1, v0

    move v2, v8

    invoke-virtual/range {v1 .. v6}, Lcom/squareup/ui/tender/ChooseCardOnFileView;->addCardOnFileCard(IZLcom/squareup/Card$Brand;Ljava/lang/String;Z)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_1
    return-void

    :cond_2
    :goto_1
    const/4 v2, -0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, v0

    .line 195
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/ui/tender/ChooseCardOnFileView;->addCardOnFileCard(IZLcom/squareup/Card$Brand;Ljava/lang/String;Z)V

    return-void
.end method

.method private updateHelpText()V
    .locals 4

    .line 213
    invoke-virtual {p0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/ChooseCardOnFileView;

    .line 214
    iget-object v1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->availableInstruments:Ljava/util/List;

    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 221
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->cardTypesToSelect:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    sget-object v2, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;->GIFT_CARD:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    if-ne v1, v2, :cond_1

    .line 222
    invoke-virtual {v0}, Lcom/squareup/ui/tender/ChooseCardOnFileView;->hideHelperText()V

    goto :goto_1

    .line 224
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->cnpFeesMessageHelper:Lcom/squareup/cnp/CnpFeesMessageHelper;

    invoke-virtual {v1}, Lcom/squareup/cnp/CnpFeesMessageHelper;->cardOnFileMessage()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/tender/ChooseCardOnFileView;->showHelpText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 215
    :cond_2
    :goto_0
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {v0}, Lcom/squareup/ui/tender/ChooseCardOnFileView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/ui/tender/legacy/R$string;->customer_no_cards_on_file_help_text:I

    const-string v3, "support_center"

    .line 216
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/ui/tender/legacy/R$string;->pay_card_card_on_file_url:I

    .line 217
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/checkout/R$string;->support_center:I

    .line 218
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 219
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    .line 215
    invoke-virtual {v0, v1}, Lcom/squareup/ui/tender/ChooseCardOnFileView;->showHelpText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void
.end method


# virtual methods
.method public cardType()Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;
    .locals 1

    .line 238
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->cardTypesToSelect:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    return-object v0
.end method

.method public onCardOnFileChargeClicked(I)V
    .locals 4

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCustomerDisplayNameOrBlank()Ljava/lang/String;

    move-result-object v0

    .line 144
    iget-object v1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->availableInstruments:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    .line 146
    iget-object v1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/tender/legacy/R$string;->card_on_file_charge_confirmation_body_with_type:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    .line 147
    invoke-interface {v2}, Lcom/squareup/tenderpayment/TenderScopeRunner;->getFormattedTotal()Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, "amount"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "customer_name"

    .line 148
    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    .line 149
    invoke-static {v1}, Lcom/squareup/cardonfile/StoredInstrumentHelper;->formatName(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "card_brand"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->last_four:Ljava/lang/String;

    const-string v2, "last_four"

    .line 150
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 151
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 152
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 154
    iget-object v1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v2, Lcom/squareup/permissions/Permission;->CARD_ON_FILE:Lcom/squareup/permissions/Permission;

    new-instance v3, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter$1;

    invoke-direct {v3, p0, p1, v0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter$1;-><init>(Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 113
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 114
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/ChooseCardOnFileScreen;

    .line 115
    invoke-static {p1}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen;->access$000(Lcom/squareup/ui/tender/ChooseCardOnFileScreen;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->forCustomerInTransaction:Z

    .line 116
    invoke-static {p1}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen;->access$100(Lcom/squareup/ui/tender/ChooseCardOnFileScreen;)Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->cardTypesToSelect:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 120
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 121
    sget-object p1, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$1;->$SwitchMap$com$squareup$ui$tender$ChooseCardOnFileScreen$CardType:[I

    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->cardTypesToSelect:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    invoke-virtual {v0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;->ordinal()I

    move-result v0

    aget p1, p1, v0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    .line 129
    iget-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getCustomerInstrumentDetails()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->availableInstruments:Ljava/util/List;

    goto :goto_0

    .line 132
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->cardTypesToSelect:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    aput-object v2, v0, v1

    const-string v1, "Card Type %s is not implemented"

    .line 133
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 126
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getCustomerNonGiftCardInstrumentDetails()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->availableInstruments:Ljava/util/List;

    goto :goto_0

    .line 123
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getCustomerGiftCardInstrumentDetails()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->availableInstruments:Ljava/util/List;

    .line 135
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->buildActionBar()V

    .line 136
    invoke-direct {p0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->updateCustomer()V

    .line 137
    invoke-direct {p0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->updateCustomerCardsOnFile()V

    .line 138
    invoke-direct {p0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->updateHelpText()V

    return-void
.end method
