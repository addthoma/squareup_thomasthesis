.class public final Lcom/squareup/ui/tender/TenderOrderTicketNameScreen;
.super Lcom/squareup/ui/tender/InTenderScope;
.source "TenderOrderTicketNameScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/tender/TenderOrderTicketNameScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/tender/TenderOrderTicketNameScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/tender/TenderOrderTicketNameScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/ui/tender/-$$Lambda$TenderOrderTicketNameScreen$0EKjH5SMuQDe96QZWWR-12Ng6r4;->INSTANCE:Lcom/squareup/ui/tender/-$$Lambda$TenderOrderTicketNameScreen$0EKjH5SMuQDe96QZWWR-12Ng6r4;

    .line 29
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/tender/TenderOrderTicketNameScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/ui/tender/TenderScope;->INSTANCE:Lcom/squareup/ui/tender/TenderScope;

    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/InTenderScope;-><init>(Lcom/squareup/ui/tender/TenderScopeTreeKey;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/tender/TenderOrderTicketNameScreen;
    .locals 0

    .line 29
    new-instance p0, Lcom/squareup/ui/tender/TenderOrderTicketNameScreen;

    invoke-direct {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNameScreen;-><init>()V

    return-object p0
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 19
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_ENTER_TICKET_BEFORE:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 32
    sget v0, Lcom/squareup/ui/tender/legacy/R$layout;->tender_order_ticket_name_view:I

    return v0
.end method
