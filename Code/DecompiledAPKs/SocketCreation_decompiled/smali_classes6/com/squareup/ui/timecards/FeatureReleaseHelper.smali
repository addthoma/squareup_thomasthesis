.class public Lcom/squareup/ui/timecards/FeatureReleaseHelper;
.super Ljava/lang/Object;
.source "FeatureReleaseHelper.java"


# static fields
.field private static final MULTIPLE_WAGES_ALPHA_FEATURES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/ui/timecards/Feature;",
            ">;"
        }
    .end annotation
.end field

.field private static final MULTIPLE_WAGES_BETA_FEATURES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/ui/timecards/Feature;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 24
    new-instance v0, Ljava/util/HashSet;

    sget-object v1, Lcom/squareup/ui/timecards/Feature;->MULTIPLE_WAGES_JOBS_SELECT:Lcom/squareup/ui/timecards/Feature;

    .line 25
    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->MULTIPLE_WAGES_ALPHA_FEATURES:Ljava/util/Set;

    .line 28
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/squareup/ui/timecards/Feature;

    sget-object v2, Lcom/squareup/ui/timecards/Feature;->MULTIPLE_WAGES_SWITCH_JOBS:Lcom/squareup/ui/timecards/Feature;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/ui/timecards/Feature;->MULTIPLE_WAGES_CLOCK_OUT_SUMMARY:Lcom/squareup/ui/timecards/Feature;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 29
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->MULTIPLE_WAGES_BETA_FEATURES:Ljava/util/Set;

    return-void
.end method

.method constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/EmployeeManagement;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->features:Lcom/squareup/settings/server/Features;

    .line 36
    iput-object p2, p0, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    return-void
.end method

.method private isMandatoryBreakCompletionEnabled()Z
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->MANDATORY_BREAK_COMPLETION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method private isMultipleWagesEnabled()Z
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->MULTIPLE_WAGES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method private isMultipleWagesInBeta()Z
    .locals 2

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->MULTIPLE_WAGES_BETA:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method private isReceiptSummaryEnabled()Z
    .locals 2

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->TIMECARDS_RECEIPT_SUMMARY:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method private isTimecardNotesEnabled()Z
    .locals 2

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->onFreeTier()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->TIMECARDS_TEAM_MEMBER_NOTES:Lcom/squareup/settings/server/Features$Feature;

    .line 76
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public isFeatureEnabled(Lcom/squareup/ui/timecards/Feature;)Z
    .locals 2

    .line 40
    sget-object v0, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->MULTIPLE_WAGES_ALPHA_FEATURES:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    invoke-direct {p0}, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->isMultipleWagesEnabled()Z

    move-result p1

    return p1

    .line 43
    :cond_0
    sget-object v0, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->MULTIPLE_WAGES_BETA_FEATURES:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 44
    invoke-direct {p0}, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->isMultipleWagesEnabled()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->isMultipleWagesInBeta()Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1

    .line 46
    :cond_2
    sget-object v0, Lcom/squareup/ui/timecards/Feature;->RECEIPT_SUMMARY:Lcom/squareup/ui/timecards/Feature;

    if-ne p1, v0, :cond_3

    .line 47
    invoke-direct {p0}, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->isReceiptSummaryEnabled()Z

    move-result p1

    return p1

    .line 49
    :cond_3
    sget-object v0, Lcom/squareup/ui/timecards/Feature;->MANDATORY_BREAK_COMPLETION:Lcom/squareup/ui/timecards/Feature;

    if-ne p1, v0, :cond_4

    .line 50
    invoke-direct {p0}, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->isMandatoryBreakCompletionEnabled()Z

    move-result p1

    return p1

    .line 52
    :cond_4
    sget-object v0, Lcom/squareup/ui/timecards/Feature;->TIMECARD_NOTES:Lcom/squareup/ui/timecards/Feature;

    if-ne p1, v0, :cond_5

    .line 53
    invoke-direct {p0}, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->isTimecardNotesEnabled()Z

    move-result p1

    return p1

    :cond_5
    return v1
.end method
