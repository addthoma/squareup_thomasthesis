.class public final Lcom/squareup/ui/timecards/ClockOutScreen$Data;
.super Lcom/squareup/ui/timecards/TimecardsScreenData;
.source "ClockOutScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/ClockOutScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Data"
.end annotation


# instance fields
.field public final hoursMinutesWorked:Lcom/squareup/ui/timecards/HoursMinutes;

.field public final printerConnected:Z

.field public final showViewNotesButton:Z

.field public final workdayShiftSummary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/HoursMinutes;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;ZZ)V
    .locals 9

    move-object v8, p0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move-object v6, p6

    .line 32
    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/timecards/TimecardsScreenData;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;)V

    move-object/from16 v0, p7

    .line 34
    iput-object v0, v8, Lcom/squareup/ui/timecards/ClockOutScreen$Data;->hoursMinutesWorked:Lcom/squareup/ui/timecards/HoursMinutes;

    move-object/from16 v0, p8

    .line 35
    iput-object v0, v8, Lcom/squareup/ui/timecards/ClockOutScreen$Data;->workdayShiftSummary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    move/from16 v0, p9

    .line 36
    iput-boolean v0, v8, Lcom/squareup/ui/timecards/ClockOutScreen$Data;->printerConnected:Z

    move/from16 v0, p10

    .line 37
    iput-boolean v0, v8, Lcom/squareup/ui/timecards/ClockOutScreen$Data;->showViewNotesButton:Z

    return-void
.end method
