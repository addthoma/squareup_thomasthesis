.class public Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;
.super Lcom/squareup/ui/timecards/TimecardsCoordinator;
.source "ClockOutConfirmationCoordinator.java"


# instance fields
.field private clockOutConfirmationButton:Lcom/squareup/marketfont/MarketButton;

.field private clockOutConfirmationScreen:Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;

.field private clockOutLinearLayout:Landroid/widget/LinearLayout;

.field private clockedInGreenIndicator:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .param p4    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 28
    invoke-direct/range {p0 .. p5}, Lcom/squareup/ui/timecards/TimecardsCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V

    return-void
.end method

.method private getHeader(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-eqz p1, :cond_1

    .line 73
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->timecard_clock_out_header_job:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "job_title"

    .line 78
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 79
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 74
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/ui/timecards/R$string;->timecard_clock_out_header:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 75
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    .line 32
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->attach(Landroid/view/View;)V

    .line 33
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;->clockOutConfirmationScreen:Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;

    .line 34
    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$ClockOutConfirmationCoordinator$5p_FpD2iTaVLYa3zfu6wGpJL8c4;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockOutConfirmationCoordinator$5p_FpD2iTaVLYa3zfu6wGpJL8c4;-><init>(Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 38
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;->clockOutConfirmationButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$ClockOutConfirmationCoordinator$_rv4OAgyEVDdrTONfp_rM565auI;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockOutConfirmationCoordinator$_rv4OAgyEVDdrTONfp_rM565auI;-><init>(Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;)V

    .line 39
    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    .line 38
    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected bindViews(Landroid/view/View;)V
    .locals 2

    .line 83
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->bindViews(Landroid/view/View;)V

    .line 84
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_clocked_in_green_indicator:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;->clockedInGreenIndicator:Landroid/view/View;

    .line 85
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_clockout_confirmation:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;->clockOutLinearLayout:Landroid/widget/LinearLayout;

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;->clockOutLinearLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 87
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_clockout_confirmation_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;->clockOutConfirmationButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method

.method protected getErrorMessage()Ljava/lang/String;
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_message:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getErrorTitle()Ljava/lang/String;
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSuccessViewEvent()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 47
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SELECT_ACTION_CLOCK_OUT:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public synthetic lambda$attach$0$ClockOutConfirmationCoordinator()Lrx/Subscription;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->clockOutConfirmationScreenData()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;->mainScheduler:Lrx/Scheduler;

    .line 35
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$-RNR0QJ_0Bs5wLA743I4ZeBjz10;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$-RNR0QJ_0Bs5wLA743I4ZeBjz10;-><init>(Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;)V

    .line 36
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$s91auTvdEhrnEQM_7cS1wQ0kh2I;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$s91auTvdEhrnEQM_7cS1wQ0kh2I;-><init>(Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;)V

    new-instance v2, Lcom/squareup/ui/timecards/-$$Lambda$bFR8TWiBtmUiehnhA-9HqizjesI;

    invoke-direct {v2, p0}, Lcom/squareup/ui/timecards/-$$Lambda$bFR8TWiBtmUiehnhA-9HqizjesI;-><init>(Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;)V

    .line 37
    invoke-virtual {v0, v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$1$ClockOutConfirmationCoordinator(Landroid/view/View;)V
    .locals 0

    .line 39
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->clockOutConfirmationClicked()V

    return-void
.end method

.method protected showBackArrowOnSuccess()Z
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;->clockOutConfirmationScreen:Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;

    iget-boolean v0, v0, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;->isFirstScreenInLayer:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method protected showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V
    .locals 4

    .line 59
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    .line 60
    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen$Data;

    .line 63
    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;->clockedInGreenIndicator:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 64
    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;->timecardHeader:Lcom/squareup/marketfont/MarketTextView;

    iget-object v3, v0, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen$Data;->jobTitle:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;->getHeader(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;->timecardHeader:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 67
    sget v1, Lcom/squareup/ui/timecards/R$id;->clockout_confirmation_notes_button:I

    invoke-virtual {p0, p1, v1}, Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;->showOrHideNotesButton(Lcom/squareup/ui/timecards/TimecardsScreenData;I)V

    .line 69
    iget-object p1, v0, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen$Data;->hoursMinutesWorkedToday:Lcom/squareup/ui/timecards/HoursMinutes;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;->setTimeWorkedTodayInSubHeader(Lcom/squareup/ui/timecards/HoursMinutes;)V

    return-void
.end method
