.class public Lcom/squareup/ui/timecards/ClockOutScreen;
.super Lcom/squareup/ui/timecards/InTimecardsScope;
.source "ClockOutScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/FullSheet;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/timecards/ClockOutScreen$Data;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/timecards/ClockOutScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/timecards/ClockOutScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/squareup/ui/timecards/ClockOutScreen;

    invoke-direct {v0}, Lcom/squareup/ui/timecards/ClockOutScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/timecards/ClockOutScreen;->INSTANCE:Lcom/squareup/ui/timecards/ClockOutScreen;

    .line 53
    sget-object v0, Lcom/squareup/ui/timecards/ClockOutScreen;->INSTANCE:Lcom/squareup/ui/timecards/ClockOutScreen;

    .line 54
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/timecards/ClockOutScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Lcom/squareup/ui/timecards/InTimecardsScope;-><init>()V

    return-void
.end method


# virtual methods
.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 49
    const-class v0, Lcom/squareup/ui/timecards/TimecardsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/timecards/TimecardsScope$Component;

    .line 50
    invoke-interface {p1}, Lcom/squareup/ui/timecards/TimecardsScope$Component;->clockOutCoordinator()Lcom/squareup/ui/timecards/ClockOutCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 45
    sget v0, Lcom/squareup/ui/timecards/R$layout;->timecards:I

    return v0
.end method
