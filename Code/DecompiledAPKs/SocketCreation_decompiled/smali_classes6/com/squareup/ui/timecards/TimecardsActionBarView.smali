.class public Lcom/squareup/ui/timecards/TimecardsActionBarView;
.super Landroid/widget/FrameLayout;
.source "TimecardsActionBarView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;
    }
.end annotation


# instance fields
.field private config:Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private primaryButton:Landroid/widget/TextView;

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private titleText:Landroid/widget/LinearLayout;

.field private titleTextPrimary:Landroid/widget/TextView;

.field private titleTextSecondary:Landroid/widget/TextView;

.field private upButtonText:Landroid/widget/TextView;

.field private upGlyph:Lcom/squareup/glyph/SquareGlyphView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    new-instance p2, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    invoke-direct {p2}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;-><init>()V

    invoke-virtual {p2}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->build()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->config:Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    .line 32
    const-class p2, Lcom/squareup/ui/timecards/TimecardsScope$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->componentInParent(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/timecards/TimecardsScope$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/timecards/TimecardsScope$Component;->inject(Lcom/squareup/ui/timecards/TimecardsActionBarView;)V

    .line 33
    sget p2, Lcom/squareup/ui/timecards/R$layout;->timecards_action_bar:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 90
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_up_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->upGlyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 91
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_up_button_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->upButtonText:Landroid/widget/TextView;

    .line 92
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_title_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->titleText:Landroid/widget/LinearLayout;

    .line 93
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_title_text_primary:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->titleTextPrimary:Landroid/widget/TextView;

    .line 94
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_title_text_secondary:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->titleTextSecondary:Landroid/widget/TextView;

    .line 95
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_action_bar_primary_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->primaryButton:Landroid/widget/TextView;

    return-void
.end method

.method private clearUpGlyph()V
    .locals 2

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->upGlyph:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private static run(Ljava/lang/Runnable;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 166
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    :cond_0
    return-void
.end method

.method private setUpGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->upGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 100
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->upGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 101
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->upGlyph:Lcom/squareup/glyph/SquareGlyphView;

    new-instance p2, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsActionBarView$6_JAfTlSZmgZ2SAdneMR0r-bUR8;

    invoke-direct {p2, p0}, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsActionBarView$6_JAfTlSZmgZ2SAdneMR0r-bUR8;-><init>(Lcom/squareup/ui/timecards/TimecardsActionBarView;)V

    invoke-static {p2}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private updatePrimaryButton()V
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->config:Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    iget-boolean v0, v0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->primaryButtonVisible:Z

    invoke-virtual {p0, v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setPrimaryButtonVisible(Z)V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->config:Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    iget-boolean v0, v0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->primaryButtonEnabled:Z

    invoke-virtual {p0, v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setPrimaryButtonEnabled(Z)V

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->config:Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    iget-object v0, v0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->primaryButtonText:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateTitleText()V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->config:Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    iget-boolean v0, v0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->titleTextVisible:Z

    invoke-virtual {p0, v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setTitleTextVisible(Z)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->config:Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    iget-object v0, v0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->titleTextPrimary:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setTitleTextPrimary(Ljava/lang/CharSequence;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->config:Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    iget-object v0, v0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->titleTextSecondary:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setTitleTextSecondary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateUpButton()V
    .locals 3

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->config:Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    iget-object v0, v0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->config:Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    iget-object v0, v0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->config:Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    iget-object v2, v2, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->upButtonContentDescription:Ljava/lang/CharSequence;

    invoke-direct {p0, v0, v2}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setUpGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 67
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->clearUpGlyph()V

    .line 69
    :goto_1
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->config:Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    iget-object v0, v0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->upButtonText:Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->config:Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    iget-object v0, v0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->upButtonText:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->upButtonText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->config:Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    iget-object v2, v2, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->upButtonText:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->upButtonText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 73
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->upButtonText:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    return-void
.end method

.method private updateView()V
    .locals 0

    .line 52
    invoke-direct {p0}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->updateVisibility()V

    .line 53
    invoke-direct {p0}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->updateUpButton()V

    .line 54
    invoke-direct {p0}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->updateTitleText()V

    .line 55
    invoke-direct {p0}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->updatePrimaryButton()V

    return-void
.end method

.method private updateVisibility()V
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->config:Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    iget-boolean v0, v0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->actionBarVisible:Z

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method


# virtual methods
.method getConfig()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->config:Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    return-object v0
.end method

.method public synthetic lambda$setPrimaryButtonText$1$TimecardsActionBarView(Landroid/view/View;)V
    .locals 0

    .line 130
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->config:Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    iget-object p1, p1, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->primaryButtonCommand:Ljava/lang/Runnable;

    invoke-static {p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->run(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$setUpGlyph$0$TimecardsActionBarView(Landroid/view/View;)V
    .locals 0

    .line 101
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->config:Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    iget-object p1, p1, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->upButtonCommand:Ljava/lang/Runnable;

    invoke-static {p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->run(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .line 46
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 47
    invoke-direct {p0}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->bindViews()V

    .line 48
    invoke-direct {p0}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->updateView()V

    return-void
.end method

.method setConfig(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;)V
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->config:Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    .line 42
    invoke-direct {p0}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->updateView()V

    return-void
.end method

.method setConfigForReminderLandingScreen(Ljava/lang/Runnable;Ljava/lang/String;)V
    .locals 2

    .line 134
    new-instance v0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;-><init>()V

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 135
    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setUpButtonGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object v0

    .line 136
    invoke-virtual {v0, p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setUpButtonCommand(Ljava/lang/Runnable;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object p1

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {p1, p2}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setUpButtonText(Ljava/lang/CharSequence;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object p2

    const/4 v0, 0x0

    .line 139
    invoke-virtual {p2, v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setTitleTextVisible(Z)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 141
    invoke-virtual {p1, v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setTitleTextVisible(Z)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object v0

    .line 142
    invoke-virtual {v0, p2}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setTitleTextPrimary(Ljava/lang/CharSequence;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    .line 144
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->build()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setConfig(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;)V

    return-void
.end method

.method setPrimaryButtonEnabled(Z)V
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->primaryButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method setPrimaryButtonText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->primaryButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->primaryButton:Landroid/widget/TextView;

    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsActionBarView$Fi60BiGNj-v6ZR6oeCzSAEXUk8M;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$TimecardsActionBarView$Fi60BiGNj-v6ZR6oeCzSAEXUk8M;-><init>(Lcom/squareup/ui/timecards/TimecardsActionBarView;)V

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method setPrimaryButtonVisible(Z)V
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->primaryButton:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setTitleTextPrimary(Ljava/lang/CharSequence;)V
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->titleTextPrimary:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setTitleTextSecondary(Ljava/lang/CharSequence;)V
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->titleTextSecondary:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setTitleTextVisible(Z)V
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->titleText:Landroid/widget/LinearLayout;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method updateConfigForSuccessScreen(Ljava/lang/Runnable;)V
    .locals 3

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->config:Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->buildUpon()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 157
    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setPrimaryButtonVisible(Z)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object v0

    .line 158
    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/timecards/R$string;->timecard_success_clock_in_out_button:I

    .line 159
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object v0

    .line 160
    invoke-virtual {v0, p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setPrimaryButtonCommand(Ljava/lang/Runnable;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object p1

    .line 161
    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->build()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    move-result-object p1

    .line 156
    invoke-virtual {p0, p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setConfig(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;)V

    return-void
.end method

.method updateConfigWithUpButtonCommand(Ljava/lang/Runnable;)V
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->config:Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    .line 149
    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->buildUpon()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object v0

    .line 150
    invoke-virtual {v0, p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->setUpButtonCommand(Ljava/lang/Runnable;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    move-result-object p1

    .line 151
    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->build()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    move-result-object p1

    .line 148
    invoke-virtual {p0, p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setConfig(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;)V

    return-void
.end method
