.class public Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;
.super Lcom/squareup/ui/timecards/TimecardsCoordinator;
.source "EmployeeJobsListModalCoordinator.java"


# instance fields
.field private jobsListLayout:Landroid/widget/LinearLayout;

.field private jobsListMessage:Lcom/squareup/marketfont/MarketTextView;

.field private jobsListScrollableView:Landroid/widget/LinearLayout;

.field private jobsListTitle:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .param p4    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 32
    invoke-direct/range {p0 .. p5}, Lcom/squareup/ui/timecards/TimecardsCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V

    return-void
.end method

.method private createButton(Landroid/view/LayoutInflater;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;)V
    .locals 3

    .line 85
    sget v0, Lcom/squareup/ui/timecards/R$layout;->employee_jobs_button:I

    iget-object v1, p0, Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;->jobsListScrollableView:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    .line 86
    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    .line 88
    iget-object v0, p2, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->job_title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 89
    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$EmployeeJobsListModalCoordinator$A97j_jNNc4gZ_jG7T07refGpeMo;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/timecards/-$$Lambda$EmployeeJobsListModalCoordinator$A97j_jNNc4gZ_jG7T07refGpeMo;-><init>(Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;)V

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    iget-object p2, p0, Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;->jobsListScrollableView:Landroid/widget/LinearLayout;

    invoke-virtual {p2, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    .line 36
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->attach(Landroid/view/View;)V

    .line 37
    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$EmployeeJobsListModalCoordinator$vzkPQ58Wb1Pg0hgK9NbFJho8c1c;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$EmployeeJobsListModalCoordinator$vzkPQ58Wb1Pg0hgK9NbFJho8c1c;-><init>(Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method protected bindViews(Landroid/view/View;)V
    .locals 2

    .line 95
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->bindViews(Landroid/view/View;)V

    .line 96
    sget v0, Lcom/squareup/ui/timecards/R$id;->employee_jobs_list_modal:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;->jobsListLayout:Landroid/widget/LinearLayout;

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;->jobsListLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 98
    sget v0, Lcom/squareup/ui/timecards/R$id;->employee_jobs_list_modal_scrollable_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;->jobsListScrollableView:Landroid/widget/LinearLayout;

    .line 99
    sget v0, Lcom/squareup/ui/timecards/R$id;->employee_jobs_list_modal_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;->jobsListTitle:Lcom/squareup/marketfont/MarketTextView;

    .line 100
    sget v0, Lcom/squareup/ui/timecards/R$id;->employee_jobs_list_modal_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;->jobsListMessage:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method protected getErrorMessage()Ljava/lang/String;
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_message:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getErrorTitle()Ljava/lang/String;
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSuccessViewEvent()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 52
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SELECT_ACTION_JOBS_LIST:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method protected isCardScreen()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public synthetic lambda$attach$0$EmployeeJobsListModalCoordinator()Lrx/Subscription;
    .locals 3

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->employeeJobsListScreenData()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;->mainScheduler:Lrx/Scheduler;

    .line 38
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$MRSo_c4pTFDyPdoPjPyx0M4o4ng;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$MRSo_c4pTFDyPdoPjPyx0M4o4ng;-><init>(Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;)V

    .line 39
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$GcUnazDMlhzXmnmMfBTDEmz-QjE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$GcUnazDMlhzXmnmMfBTDEmz-QjE;-><init>(Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;)V

    new-instance v2, Lcom/squareup/ui/timecards/-$$Lambda$boDXrvDqMhpmp9JFMwPWFH65brk;

    invoke-direct {v2, p0}, Lcom/squareup/ui/timecards/-$$Lambda$boDXrvDqMhpmp9JFMwPWFH65brk;-><init>(Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;)V

    .line 40
    invoke-virtual {v0, v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$createButton$1$EmployeeJobsListModalCoordinator(Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;Landroid/view/View;)V
    .locals 1

    .line 90
    iget-object p2, p0, Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    iget-object p1, p1, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->job_token:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-virtual {p2, p1, v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->onJobSelected(Ljava/lang/String;Z)V

    return-void
.end method

.method protected showBackArrowOnSuccess()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V
    .locals 2

    .line 64
    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/timecards/EmployeeJobsListScreen$Data;

    .line 66
    iget-object v1, v0, Lcom/squareup/ui/timecards/EmployeeJobsListScreen$Data;->jobs:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/squareup/ui/timecards/EmployeeJobsListScreen$Data;->jobs:Ljava/util/List;

    .line 67
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 72
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    .line 73
    iget-object p1, p0, Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;->currentTimeView:Lcom/squareup/marketfont/MarketTextView;

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 74
    iget-object p1, p0, Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;->jobsListTitle:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;->jobsListMessage:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 76
    iget-object p1, p0, Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;->view:Landroid/view/View;

    .line 77
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/LayoutInflater;

    .line 78
    iget-object v1, p0, Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;->jobsListScrollableView:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 79
    iget-object v0, v0, Lcom/squareup/ui/timecards/EmployeeJobsListScreen$Data;->jobs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    .line 80
    invoke-direct {p0, p1, v1}, Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;->createButton(Landroid/view/LayoutInflater;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;)V

    goto :goto_0

    :cond_1
    return-void

    .line 68
    :cond_2
    :goto_1
    invoke-virtual {p0, v0}, Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;->showError(Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    return-void
.end method
