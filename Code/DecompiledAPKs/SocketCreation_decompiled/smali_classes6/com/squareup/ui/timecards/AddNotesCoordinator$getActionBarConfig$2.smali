.class final Lcom/squareup/ui/timecards/AddNotesCoordinator$getActionBarConfig$2;
.super Lkotlin/jvm/internal/Lambda;
.source "AddNotesCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/timecards/AddNotesCoordinator;->getActionBarConfig()Lcom/squareup/noho/NohoActionBar$Config;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/timecards/AddNotesCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/timecards/AddNotesCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/timecards/AddNotesCoordinator$getActionBarConfig$2;->this$0:Lcom/squareup/ui/timecards/AddNotesCoordinator;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/AddNotesCoordinator$getActionBarConfig$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 2

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/timecards/AddNotesCoordinator$getActionBarConfig$2;->this$0:Lcom/squareup/ui/timecards/AddNotesCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/timecards/AddNotesCoordinator;->access$getScopeRunner$p(Lcom/squareup/ui/timecards/AddNotesCoordinator;)Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/AddNotesCoordinator$getActionBarConfig$2;->this$0:Lcom/squareup/ui/timecards/AddNotesCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/timecards/AddNotesCoordinator;->access$getNotes$p(Lcom/squareup/ui/timecards/AddNotesCoordinator;)Lcom/squareup/noho/NohoMessageText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoMessageText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->onNotesSaved(Ljava/lang/String;)V

    return-void
.end method
