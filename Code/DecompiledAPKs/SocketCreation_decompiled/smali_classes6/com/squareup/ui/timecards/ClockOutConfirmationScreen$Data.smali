.class public final Lcom/squareup/ui/timecards/ClockOutConfirmationScreen$Data;
.super Lcom/squareup/ui/timecards/TimecardsScreenData;
.source "ClockOutConfirmationScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Data"
.end annotation


# instance fields
.field public final hoursMinutesWorkedToday:Lcom/squareup/ui/timecards/HoursMinutes;

.field public final jobTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;Lcom/squareup/ui/timecards/HoursMinutes;Ljava/lang/String;)V
    .locals 0

    .line 32
    invoke-direct/range {p0 .. p7}, Lcom/squareup/ui/timecards/TimecardsScreenData;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;)V

    .line 34
    iput-object p8, p0, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen$Data;->hoursMinutesWorkedToday:Lcom/squareup/ui/timecards/HoursMinutes;

    .line 35
    iput-object p9, p0, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen$Data;->jobTitle:Ljava/lang/String;

    return-void
.end method
