.class public final Lcom/squareup/ui/timecards/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final employee_management_break_tracking_end_break_button_text:I = 0x7f1209df

.field public static final employee_management_break_tracking_end_break_early_button_text:I = 0x7f1209e0

.field public static final employee_management_break_tracking_end_break_late_message:I = 0x7f1209e1

.field public static final employee_management_break_tracking_end_break_message:I = 0x7f1209e2

.field public static final employee_management_break_tracking_end_break_one_min_message:I = 0x7f1209e3

.field public static final employee_management_break_tracking_end_break_progress_title:I = 0x7f1209e4

.field public static final employee_management_break_tracking_end_break_sub_one_min_message:I = 0x7f1209e5

.field public static final employee_management_break_tracking_end_break_success_message:I = 0x7f1209e6

.field public static final employee_management_break_tracking_general_error_message:I = 0x7f1209e7

.field public static final employee_management_break_tracking_general_error_title:I = 0x7f1209e8

.field public static final employee_management_break_tracking_list_button_text:I = 0x7f1209e9

.field public static final employee_management_break_tracking_list_button_text_one_min:I = 0x7f1209ea

.field public static final employee_management_break_tracking_list_title:I = 0x7f1209eb

.field public static final employee_management_break_tracking_list_title_one_break_def:I = 0x7f1209ec

.field public static final employee_management_break_tracking_modal_log_out:I = 0x7f1209ed

.field public static final employee_management_break_tracking_modal_message:I = 0x7f1209ee

.field public static final employee_management_break_tracking_modal_message_break_over:I = 0x7f1209ef

.field public static final employee_management_break_tracking_modal_message_one_min:I = 0x7f1209f0

.field public static final employee_management_break_tracking_modal_message_sub_one_min:I = 0x7f1209f1

.field public static final employee_management_break_tracking_modal_title_not_over:I = 0x7f1209f2

.field public static final employee_management_break_tracking_modal_title_over:I = 0x7f1209f3

.field public static final employee_management_break_tracking_no_internet_error_message:I = 0x7f1209f4

.field public static final employee_management_break_tracking_no_internet_error_title:I = 0x7f1209f5

.field public static final employee_management_break_tracking_sheet_title_not_over:I = 0x7f1209f6

.field public static final employee_management_break_tracking_sheet_title_over:I = 0x7f1209f7

.field public static final employee_management_break_tracking_shift_summary:I = 0x7f1209f8

.field public static final employee_management_break_tracking_start_button_text:I = 0x7f1209f9

.field public static final employee_management_break_tracking_start_error_message:I = 0x7f1209fa

.field public static final employee_management_break_tracking_start_error_title:I = 0x7f1209fb

.field public static final employee_management_break_tracking_start_progress_title:I = 0x7f1209fc

.field public static final employee_management_break_tracking_start_success_message:I = 0x7f1209fd

.field public static final employee_management_break_tracking_start_success_title:I = 0x7f1209fe

.field public static final employee_management_break_tracking_success_continue_button_text:I = 0x7f1209ff

.field public static final employee_management_break_tracking_switch_jobs_button_text:I = 0x7f120a00

.field public static final employee_management_clock_in_confirmation_message:I = 0x7f120a02

.field public static final employee_management_clock_in_confirmation_sub_header:I = 0x7f120a03

.field public static final employee_management_clock_in_out_content_description:I = 0x7f120a05

.field public static final employee_management_clock_in_progress_title:I = 0x7f120a06

.field public static final employee_management_clock_out_button_text:I = 0x7f120a07

.field public static final employee_management_clock_out_confirmation_button_text:I = 0x7f120a08

.field public static final employee_management_clock_out_confirmation_shift_summary:I = 0x7f120a09

.field public static final employee_management_clock_out_confirmation_shift_summary_job:I = 0x7f120a0a

.field public static final employee_management_clock_out_progress_title:I = 0x7f120a0b

.field public static final employee_management_clock_out_success_shift_summary:I = 0x7f120a0c

.field public static final employee_management_clock_out_success_title:I = 0x7f120a0d

.field public static final employee_management_clock_out_success_title_multiple_wages:I = 0x7f120a0e

.field public static final employee_management_jobs_list_modal_message:I = 0x7f120a18

.field public static final employee_management_jobs_list_title:I = 0x7f120a19

.field public static final employee_management_off:I = 0x7f120a1f

.field public static final employee_management_on:I = 0x7f120a20

.field public static final employee_management_timecard_hrs_fraction:I = 0x7f120a27

.field public static final employee_management_timecard_hrs_mins_zero:I = 0x7f120a28

.field public static final employee_management_timecard_hrs_plural:I = 0x7f120a29

.field public static final employee_management_timecard_hrs_singular:I = 0x7f120a2a

.field public static final employee_management_timecard_mins_plural:I = 0x7f120a2b

.field public static final employee_management_timecard_mins_singular:I = 0x7f120a2c

.field public static final timecard_add_notes:I = 0x7f121984

.field public static final timecard_add_or_edit_notes_save_button:I = 0x7f121985

.field public static final timecard_clock_in_and_continue:I = 0x7f121986

.field public static final timecard_clock_in_and_continue_glyph_title:I = 0x7f121987

.field public static final timecard_clock_in_and_continue_success_title:I = 0x7f121988

.field public static final timecard_clock_in_out_button:I = 0x7f121989

.field public static final timecard_clock_in_title:I = 0x7f12198a

.field public static final timecard_clock_out_header:I = 0x7f12198b

.field public static final timecard_clock_out_header_job:I = 0x7f12198c

.field public static final timecard_clock_out_summary_footer_total:I = 0x7f12198d

.field public static final timecard_clock_out_summary_mw_header_0_shift:I = 0x7f12198e

.field public static final timecard_clock_out_summary_mw_header_1_clocked_in:I = 0x7f12198f

.field public static final timecard_clock_out_summary_mw_header_2_clocked_out:I = 0x7f121990

.field public static final timecard_clock_out_summary_mw_header_3_paid_hours:I = 0x7f121991

.field public static final timecard_clock_out_title:I = 0x7f121992

.field public static final timecard_clocked_in_summary:I = 0x7f121993

.field public static final timecard_clocked_in_summary_job:I = 0x7f121994

.field public static final timecard_continue_to_register:I = 0x7f121995

.field public static final timecard_edit_notes:I = 0x7f121996

.field public static final timecard_error_no_internet_connection:I = 0x7f121997

.field public static final timecard_error_title:I = 0x7f121998

.field public static final timecard_message:I = 0x7f121999

.field public static final timecard_no_job_title:I = 0x7f12199a

.field public static final timecard_print_receipt_summary:I = 0x7f12199b

.field public static final timecard_punching_title:I = 0x7f12199c

.field public static final timecard_success_clock_in_out_button:I = 0x7f12199d

.field public static final timecard_total_hours_plural:I = 0x7f12199e

.field public static final timecard_total_hours_singular:I = 0x7f12199f

.field public static final timecard_total_minutes_plural:I = 0x7f1219a0

.field public static final timecard_total_minutes_singular:I = 0x7f1219a1

.field public static final timecard_total_minutes_zero:I = 0x7f1219a2

.field public static final timecard_view_notes:I = 0x7f1219a3

.field public static final timecard_view_notes_save_button:I = 0x7f1219a4


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
