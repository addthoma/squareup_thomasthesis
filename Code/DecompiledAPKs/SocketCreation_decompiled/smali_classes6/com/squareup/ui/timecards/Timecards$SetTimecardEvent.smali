.class Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;
.super Lcom/squareup/ui/timecards/Timecards$Event;
.source "Timecards.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/Timecards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SetTimecardEvent"
.end annotation


# instance fields
.field public final clockInUnitToken:Ljava/lang/String;

.field public final clockedInCurrentWorkday:Ljava/lang/Boolean;

.field public final employeeJobInfo:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

.field public final notes:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/ui/timecards/Timecards;

.field public final timecardStartTimestamp:Ljava/util/Date;

.field public final timecardToken:Ljava/lang/String;

.field public final totalSecondsClockedInForCurrentWorkday:Ljava/lang/Long;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/timecards/Timecards;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Boolean;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 0

    .line 604
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;->this$0:Lcom/squareup/ui/timecards/Timecards;

    invoke-direct {p0}, Lcom/squareup/ui/timecards/Timecards$Event;-><init>()V

    .line 605
    iput-object p2, p0, Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;->timecardToken:Ljava/lang/String;

    .line 606
    iput-object p3, p0, Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;->clockInUnitToken:Ljava/lang/String;

    .line 607
    iput-object p4, p0, Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;->timecardStartTimestamp:Ljava/util/Date;

    .line 608
    iput-object p5, p0, Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;->clockedInCurrentWorkday:Ljava/lang/Boolean;

    .line 609
    iput-object p6, p0, Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;->employeeJobInfo:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    .line 610
    iput-object p7, p0, Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;->totalSecondsClockedInForCurrentWorkday:Ljava/lang/Long;

    .line 611
    iput-object p8, p0, Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;->notes:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/timecards/Timecards;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Boolean;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/ui/timecards/Timecards$1;)V
    .locals 0

    .line 591
    invoke-direct/range {p0 .. p8}, Lcom/squareup/ui/timecards/Timecards$SetTimecardEvent;-><init>(Lcom/squareup/ui/timecards/Timecards;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Boolean;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;Ljava/lang/Long;Ljava/lang/String;)V

    return-void
.end method
