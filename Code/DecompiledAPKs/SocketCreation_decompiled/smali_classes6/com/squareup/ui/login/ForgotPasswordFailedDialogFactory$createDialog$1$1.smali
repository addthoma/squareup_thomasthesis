.class final Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$createDialog$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ForgotPasswordFailedDialogFactory.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$createDialog$1;->invoke()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nForgotPasswordFailedDialogFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ForgotPasswordFailedDialogFactory.kt\ncom/squareup/ui/login/ForgotPasswordFailedDialogFactory$createDialog$1$1\n+ 2 AuthenticatorEvent.kt\ncom/squareup/ui/login/AuthenticatorEventKt\n*L\n1#1,44:1\n200#2:45\n*E\n*S KotlinDebug\n*F\n+ 1 ForgotPasswordFailedDialogFactory.kt\ncom/squareup/ui/login/ForgotPasswordFailedDialogFactory$createDialog$1$1\n*L\n34#1:45\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$createDialog$1$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$createDialog$1$1;

    invoke-direct {v0}, Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$createDialog$1$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$createDialog$1$1;->INSTANCE:Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$createDialog$1$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$createDialog$1$1;->invoke(Lcom/squareup/workflow/legacy/Screen;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 45
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorEvent$GoBackFrom;

    const-class v1, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;

    invoke-direct {v0, v1}, Lcom/squareup/ui/login/AuthenticatorEvent$GoBackFrom;-><init>(Ljava/lang/Class;)V

    .line 34
    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
