.class public abstract Lcom/squareup/ui/login/OperationType;
.super Ljava/lang/Object;
.source "Operation.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/OperationType$None;,
        Lcom/squareup/ui/login/OperationType$ResetPassword;,
        Lcom/squareup/ui/login/OperationType$LoginWithEmail;,
        Lcom/squareup/ui/login/OperationType$LoginWithDeviceCode;,
        Lcom/squareup/ui/login/OperationType$SelectUnit;,
        Lcom/squareup/ui/login/OperationType$ResumeAutomaticUnitSelection;,
        Lcom/squareup/ui/login/OperationType$VerifyTwoFactorByEnroll;,
        Lcom/squareup/ui/login/OperationType$VerifyTwoFactorByUpgradeSession;,
        Lcom/squareup/ui/login/OperationType$EnrollTwoFactor;,
        Lcom/squareup/ui/login/OperationType$SendVerificationCode;,
        Lcom/squareup/ui/login/OperationType$VerifyLoginByCaptcha;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u000b\u0003\u0004\u0005\u0006\u0007\u0008\t\n\u000b\u000c\rB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u000b\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/ui/login/OperationType;",
        "",
        "()V",
        "EnrollTwoFactor",
        "LoginWithDeviceCode",
        "LoginWithEmail",
        "None",
        "ResetPassword",
        "ResumeAutomaticUnitSelection",
        "SelectUnit",
        "SendVerificationCode",
        "VerifyLoginByCaptcha",
        "VerifyTwoFactorByEnroll",
        "VerifyTwoFactorByUpgradeSession",
        "Lcom/squareup/ui/login/OperationType$None;",
        "Lcom/squareup/ui/login/OperationType$ResetPassword;",
        "Lcom/squareup/ui/login/OperationType$LoginWithEmail;",
        "Lcom/squareup/ui/login/OperationType$LoginWithDeviceCode;",
        "Lcom/squareup/ui/login/OperationType$SelectUnit;",
        "Lcom/squareup/ui/login/OperationType$ResumeAutomaticUnitSelection;",
        "Lcom/squareup/ui/login/OperationType$VerifyTwoFactorByEnroll;",
        "Lcom/squareup/ui/login/OperationType$VerifyTwoFactorByUpgradeSession;",
        "Lcom/squareup/ui/login/OperationType$EnrollTwoFactor;",
        "Lcom/squareup/ui/login/OperationType$SendVerificationCode;",
        "Lcom/squareup/ui/login/OperationType$VerifyLoginByCaptcha;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/squareup/ui/login/OperationType;-><init>()V

    return-void
.end method
