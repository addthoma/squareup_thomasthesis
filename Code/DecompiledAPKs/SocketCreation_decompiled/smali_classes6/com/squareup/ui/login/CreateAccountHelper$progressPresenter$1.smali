.class public final Lcom/squareup/ui/login/CreateAccountHelper$progressPresenter$1;
.super Ljava/lang/Object;
.source "CreateAccountHelper.kt"

# interfaces
.implements Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/CreateAccountHelper;-><init>(Lcom/squareup/account/PersistentAccountService;Lcom/squareup/server/accountstatus/AccountStatusService;Lcom/squareup/server/account/AuthenticationService;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener<",
        "Lcom/squareup/server/SimpleResponse;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0012\u0010\u0007\u001a\u00020\u00042\u0008\u0010\u0008\u001a\u0004\u0018\u00010\u0002H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "com/squareup/ui/login/CreateAccountHelper$progressPresenter$1",
        "Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener;",
        "Lcom/squareup/server/SimpleResponse;",
        "onFailureViewDismissed",
        "",
        "retry",
        "",
        "onProgressViewDismissed",
        "successResponse",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/login/CreateAccountHelper;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/CreateAccountHelper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 63
    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountHelper$progressPresenter$1;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailureViewDismissed(Z)V
    .locals 2

    if-eqz p1, :cond_1

    .line 68
    iget-object p1, p0, Lcom/squareup/ui/login/CreateAccountHelper$progressPresenter$1;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-static {p1}, Lcom/squareup/ui/login/CreateAccountHelper;->access$getCreateAccountDisposables$p(Lcom/squareup/ui/login/CreateAccountHelper;)Lio/reactivex/disposables/CompositeDisposable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper$progressPresenter$1;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-static {v0}, Lcom/squareup/ui/login/CreateAccountHelper;->access$getRequestBody$p(Lcom/squareup/ui/login/CreateAccountHelper;)Lcom/squareup/server/account/CreateBody;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v0, v1}, Lcom/squareup/ui/login/CreateAccountHelper;->attemptCreateAccount(Lcom/squareup/server/account/CreateBody;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    :cond_1
    return-void
.end method

.method public onProgressViewDismissed(Lcom/squareup/server/SimpleResponse;)V
    .locals 0

    return-void
.end method

.method public bridge synthetic onProgressViewDismissed(Ljava/lang/Object;)V
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/server/SimpleResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/CreateAccountHelper$progressPresenter$1;->onProgressViewDismissed(Lcom/squareup/server/SimpleResponse;)V

    return-void
.end method
