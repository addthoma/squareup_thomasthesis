.class public final Lcom/squareup/ui/login/AuthenticationServiceEndpoint$Companion;
.super Ljava/lang/Object;
.source "AuthenticationServiceEndpoint.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/AuthenticationServiceEndpoint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u0005\u0010\u0002\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/ui/login/AuthenticationServiceEndpoint$Companion;",
        "",
        "()V",
        "MIN_DELAY_MS",
        "",
        "MIN_DELAY_MS$annotations",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/ui/login/AuthenticationServiceEndpoint$Companion;

.field public static final MIN_DELAY_MS:J = 0x2eeL


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 149
    new-instance v0, Lcom/squareup/ui/login/AuthenticationServiceEndpoint$Companion;

    invoke-direct {v0}, Lcom/squareup/ui/login/AuthenticationServiceEndpoint$Companion;-><init>()V

    sput-object v0, Lcom/squareup/ui/login/AuthenticationServiceEndpoint$Companion;->$$INSTANCE:Lcom/squareup/ui/login/AuthenticationServiceEndpoint$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic MIN_DELAY_MS$annotations()V
    .locals 0

    return-void
.end method
