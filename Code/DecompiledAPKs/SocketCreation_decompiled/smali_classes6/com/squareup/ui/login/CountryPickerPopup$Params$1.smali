.class final Lcom/squareup/ui/login/CountryPickerPopup$Params$1;
.super Ljava/lang/Object;
.source "CountryPickerPopup.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/CountryPickerPopup$Params;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/ui/login/CountryPickerPopup$Params;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/login/CountryPickerPopup$Params;
    .locals 3

    .line 72
    new-instance v0, Lcom/squareup/ui/login/CountryPickerPopup$Params;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/squareup/CountryCode;->values()[Lcom/squareup/CountryCode;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    aget-object p1, v2, p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/login/CountryPickerPopup$Params;-><init>(Ljava/lang/String;Lcom/squareup/CountryCode;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 70
    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/CountryPickerPopup$Params$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/login/CountryPickerPopup$Params;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/ui/login/CountryPickerPopup$Params;
    .locals 0

    .line 76
    new-array p1, p1, [Lcom/squareup/ui/login/CountryPickerPopup$Params;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 70
    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/CountryPickerPopup$Params$1;->newArray(I)[Lcom/squareup/ui/login/CountryPickerPopup$Params;

    move-result-object p1

    return-object p1
.end method
