.class public final Lcom/squareup/ui/login/AuthenticatorState;
.super Ljava/lang/Object;
.source "AuthenticatorState.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0013\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B9\u0008\u0007\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\tH\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u000bH\u00c6\u0003J;\u0010\u001b\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000bH\u00c6\u0001J\u0013\u0010\u001c\u001a\u00020\u00032\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001J\t\u0010 \u001a\u00020!H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\rR\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "",
        "isWorldEnabled",
        "",
        "temporarySessionToken",
        "Lcom/squareup/account/SecretString;",
        "unitsAndMerchants",
        "Lcom/squareup/ui/login/UnitsAndMerchants;",
        "screens",
        "Lcom/squareup/ui/login/AuthenticatorStack;",
        "operation",
        "Lcom/squareup/ui/login/Operation;",
        "(ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;)V",
        "()Z",
        "getOperation",
        "()Lcom/squareup/ui/login/Operation;",
        "getScreens",
        "()Lcom/squareup/ui/login/AuthenticatorStack;",
        "getTemporarySessionToken",
        "()Lcom/squareup/account/SecretString;",
        "getUnitsAndMerchants",
        "()Lcom/squareup/ui/login/UnitsAndMerchants;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final isWorldEnabled:Z

.field private final operation:Lcom/squareup/ui/login/Operation;

.field private final screens:Lcom/squareup/ui/login/AuthenticatorStack;

.field private final temporarySessionToken:Lcom/squareup/account/SecretString;

.field private final unitsAndMerchants:Lcom/squareup/ui/login/UnitsAndMerchants;


# direct methods
.method public constructor <init>()V
    .locals 8

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1f

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorState;-><init>(ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 8

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1e

    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorState;-><init>(ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(ZLcom/squareup/account/SecretString;)V
    .locals 8

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1c

    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorState;-><init>(ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;)V
    .locals 8

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x18

    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorState;-><init>(ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;)V
    .locals 8

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorState;-><init>(ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;)V
    .locals 1

    const-string v0, "temporarySessionToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "unitsAndMerchants"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "operation"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/ui/login/AuthenticatorState;->isWorldEnabled:Z

    iput-object p2, p0, Lcom/squareup/ui/login/AuthenticatorState;->temporarySessionToken:Lcom/squareup/account/SecretString;

    iput-object p3, p0, Lcom/squareup/ui/login/AuthenticatorState;->unitsAndMerchants:Lcom/squareup/ui/login/UnitsAndMerchants;

    iput-object p4, p0, Lcom/squareup/ui/login/AuthenticatorState;->screens:Lcom/squareup/ui/login/AuthenticatorStack;

    iput-object p5, p0, Lcom/squareup/ui/login/AuthenticatorState;->operation:Lcom/squareup/ui/login/Operation;

    return-void
.end method

.method public synthetic constructor <init>(ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    .line 17
    sget-object p2, Lcom/squareup/account/SecretString;->Companion:Lcom/squareup/account/SecretString$Companion;

    invoke-virtual {p2}, Lcom/squareup/account/SecretString$Companion;->getEMPTY()Lcom/squareup/account/SecretString;

    move-result-object p2

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    .line 18
    sget-object p3, Lcom/squareup/ui/login/UnitsAndMerchants;->EMPTY:Lcom/squareup/ui/login/UnitsAndMerchants;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    .line 19
    sget-object p4, Lcom/squareup/ui/login/AuthenticatorStack;->EMPTY:Lcom/squareup/ui/login/AuthenticatorStack;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    .line 20
    sget-object p2, Lcom/squareup/ui/login/Operation;->Companion:Lcom/squareup/ui/login/Operation$Companion;

    invoke-virtual {p2}, Lcom/squareup/ui/login/Operation$Companion;->getNONE()Lcom/squareup/ui/login/Operation;

    move-result-object p5

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-direct/range {p2 .. p7}, Lcom/squareup/ui/login/AuthenticatorState;-><init>(ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-boolean p1, p0, Lcom/squareup/ui/login/AuthenticatorState;->isWorldEnabled:Z

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/login/AuthenticatorState;->temporarySessionToken:Lcom/squareup/account/SecretString;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/login/AuthenticatorState;->unitsAndMerchants:Lcom/squareup/ui/login/UnitsAndMerchants;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/ui/login/AuthenticatorState;->screens:Lcom/squareup/ui/login/AuthenticatorStack;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/ui/login/AuthenticatorState;->operation:Lcom/squareup/ui/login/Operation;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/ui/login/AuthenticatorState;->copy(ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/login/AuthenticatorState;->isWorldEnabled:Z

    return v0
.end method

.method public final component2()Lcom/squareup/account/SecretString;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorState;->temporarySessionToken:Lcom/squareup/account/SecretString;

    return-object v0
.end method

.method public final component3()Lcom/squareup/ui/login/UnitsAndMerchants;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorState;->unitsAndMerchants:Lcom/squareup/ui/login/UnitsAndMerchants;

    return-object v0
.end method

.method public final component4()Lcom/squareup/ui/login/AuthenticatorStack;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorState;->screens:Lcom/squareup/ui/login/AuthenticatorStack;

    return-object v0
.end method

.method public final component5()Lcom/squareup/ui/login/Operation;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorState;->operation:Lcom/squareup/ui/login/Operation;

    return-object v0
.end method

.method public final copy(ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 7

    const-string v0, "temporarySessionToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "unitsAndMerchants"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "operation"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorState;

    move-object v1, v0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/login/AuthenticatorState;-><init>(ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/login/AuthenticatorState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorState;

    iget-boolean v0, p0, Lcom/squareup/ui/login/AuthenticatorState;->isWorldEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/ui/login/AuthenticatorState;->isWorldEnabled:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorState;->temporarySessionToken:Lcom/squareup/account/SecretString;

    iget-object v1, p1, Lcom/squareup/ui/login/AuthenticatorState;->temporarySessionToken:Lcom/squareup/account/SecretString;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorState;->unitsAndMerchants:Lcom/squareup/ui/login/UnitsAndMerchants;

    iget-object v1, p1, Lcom/squareup/ui/login/AuthenticatorState;->unitsAndMerchants:Lcom/squareup/ui/login/UnitsAndMerchants;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorState;->screens:Lcom/squareup/ui/login/AuthenticatorStack;

    iget-object v1, p1, Lcom/squareup/ui/login/AuthenticatorState;->screens:Lcom/squareup/ui/login/AuthenticatorStack;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorState;->operation:Lcom/squareup/ui/login/Operation;

    iget-object p1, p1, Lcom/squareup/ui/login/AuthenticatorState;->operation:Lcom/squareup/ui/login/Operation;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getOperation()Lcom/squareup/ui/login/Operation;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorState;->operation:Lcom/squareup/ui/login/Operation;

    return-object v0
.end method

.method public final getScreens()Lcom/squareup/ui/login/AuthenticatorStack;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorState;->screens:Lcom/squareup/ui/login/AuthenticatorStack;

    return-object v0
.end method

.method public final getTemporarySessionToken()Lcom/squareup/account/SecretString;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorState;->temporarySessionToken:Lcom/squareup/account/SecretString;

    return-object v0
.end method

.method public final getUnitsAndMerchants()Lcom/squareup/ui/login/UnitsAndMerchants;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorState;->unitsAndMerchants:Lcom/squareup/ui/login/UnitsAndMerchants;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/ui/login/AuthenticatorState;->isWorldEnabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorState;->temporarySessionToken:Lcom/squareup/account/SecretString;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorState;->unitsAndMerchants:Lcom/squareup/ui/login/UnitsAndMerchants;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorState;->screens:Lcom/squareup/ui/login/AuthenticatorStack;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorState;->operation:Lcom/squareup/ui/login/Operation;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    return v0
.end method

.method public final isWorldEnabled()Z
    .locals 1

    .line 16
    iget-boolean v0, p0, Lcom/squareup/ui/login/AuthenticatorState;->isWorldEnabled:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AuthenticatorState(isWorldEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/login/AuthenticatorState;->isWorldEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", temporarySessionToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorState;->temporarySessionToken:Lcom/squareup/account/SecretString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", unitsAndMerchants="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorState;->unitsAndMerchants:Lcom/squareup/ui/login/UnitsAndMerchants;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", screens="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorState;->screens:Lcom/squareup/ui/login/AuthenticatorStack;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", operation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorState;->operation:Lcom/squareup/ui/login/Operation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
