.class public final Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinatorKt;
.super Ljava/lang/Object;
.source "EnrollGoogleAuthCodeCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "formatAuthKey",
        "",
        "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
        "authenticator-views_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$formatAuthKey(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinatorKt;->formatAuthKey(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static final formatAuthKey(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)Ljava/lang/String;
    .locals 9

    .line 99
    iget-object p0, p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    iget-object p0, p0, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;->authenticator_key_base32:Ljava/lang/String;

    const-string v0, "googleauth.authenticator_key_base32"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Ljava/lang/CharSequence;

    const/4 v0, 0x4

    invoke-static {p0, v0}, Lkotlin/text/StringsKt;->chunked(Ljava/lang/CharSequence;I)Ljava/util/List;

    move-result-object p0

    move-object v0, p0

    check-cast v0, Ljava/lang/Iterable;

    const-string p0, "    "

    move-object v1, p0

    check-cast v1, Ljava/lang/CharSequence;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x3e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
