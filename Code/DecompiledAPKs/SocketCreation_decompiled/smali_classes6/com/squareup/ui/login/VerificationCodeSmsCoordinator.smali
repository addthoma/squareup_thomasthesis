.class public final Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "VerificationCodeSmsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nVerificationCodeSmsCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 VerificationCodeSmsCoordinator.kt\ncom/squareup/ui/login/VerificationCodeSmsCoordinator\n+ 2 MarketSpan.kt\ncom/squareup/marketfont/MarketSpanKt\n*L\n1#1,201:1\n17#2,2:202\n*E\n*S KotlinDebug\n*F\n+ 1 VerificationCodeSmsCoordinator.kt\ncom/squareup/ui/login/VerificationCodeSmsCoordinator\n*L\n172#1,2:202\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001:\u0001,B/\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0018\u0010\u0006\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u00080\u0007\u00a2\u0006\u0002\u0010\u000bJ4\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\t2\u0008\u0008\u0001\u0010!\u001a\u00020\"2\u0008\u0008\u0001\u0010#\u001a\u00020\"2\u000e\u0008\u0002\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u001f0%H\u0002J\u0010\u0010&\u001a\u00020\u001f2\u0006\u0010\'\u001a\u00020(H\u0016J\u0010\u0010)\u001a\u00020\u001f2\u0006\u0010\'\u001a\u00020(H\u0002J\u0008\u0010*\u001a\u00020\u001fH\u0002J\u0006\u0010+\u001a\u00020\u001fR\u000e\u0010\u000c\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u00020\u000f8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\u000f8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0010R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0006\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0018\u001a\u00020\u00198BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001a\u0010\u001bR\u000e\u0010\u001c\u001a\u00020\u001dX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "res",
        "Lcom/squareup/util/Res;",
        "toastFactory",
        "Lcom/squareup/util/ToastFactory;",
        "screenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "(Lcom/squareup/util/Res;Lcom/squareup/util/ToastFactory;Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "isVerificationCodeValid",
        "",
        "()Z",
        "rememberThisDeviceIsChecked",
        "getRememberThisDeviceIsChecked",
        "rememberThisDeviceToggle",
        "Lcom/squareup/widgets/list/ToggleButtonRow;",
        "subtitle",
        "Lcom/squareup/widgets/MessageView;",
        "title",
        "verificationCode",
        "",
        "getVerificationCode",
        "()Ljava/lang/String;",
        "verificationCodeField",
        "Lcom/squareup/ui/XableEditText;",
        "applyResendCodeLink",
        "",
        "screen",
        "colorId",
        "",
        "textId",
        "onNextPress",
        "Lkotlin/Function0;",
        "attach",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "indicateVerificationCodeError",
        "updateEnabledState",
        "Factory",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private rememberThisDeviceToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private final res:Lcom/squareup/util/Res;

.field private final screenData:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private subtitle:Lcom/squareup/widgets/MessageView;

.field private title:Lcom/squareup/widgets/MessageView;

.field private final toastFactory:Lcom/squareup/util/ToastFactory;

.field private verificationCodeField:Lcom/squareup/ui/XableEditText;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/util/ToastFactory;Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/util/ToastFactory;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "toastFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenData"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->toastFactory:Lcom/squareup/util/ToastFactory;

    iput-object p3, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->screenData:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$applyResendCodeLink(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;IILkotlin/jvm/functions/Function0;)V
    .locals 0

    .line 39
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->applyResendCodeLink(Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;IILkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public static final synthetic access$getActionBar$p(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;)Lcom/squareup/marin/widgets/ActionBarView;
    .locals 1

    .line 39
    iget-object p0, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez p0, :cond_0

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getRememberThisDeviceIsChecked$p(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;)Z
    .locals 0

    .line 39
    invoke-direct {p0}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->getRememberThisDeviceIsChecked()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$getToastFactory$p(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;)Lcom/squareup/util/ToastFactory;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->toastFactory:Lcom/squareup/util/ToastFactory;

    return-object p0
.end method

.method public static final synthetic access$getVerificationCode$p(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;)Ljava/lang/String;
    .locals 0

    .line 39
    invoke-direct {p0}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->getVerificationCode()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getVerificationCodeField$p(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;)Lcom/squareup/ui/XableEditText;
    .locals 1

    .line 39
    iget-object p0, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->verificationCodeField:Lcom/squareup/ui/XableEditText;

    if-nez p0, :cond_0

    const-string v0, "verificationCodeField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$indicateVerificationCodeError(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;)V
    .locals 0

    .line 39
    invoke-direct {p0}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->indicateVerificationCodeError()V

    return-void
.end method

.method public static final synthetic access$isVerificationCodeValid$p(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;)Z
    .locals 0

    .line 39
    invoke-direct {p0}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->isVerificationCodeValid()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$setActionBar$p(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;Lcom/squareup/marin/widgets/ActionBarView;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    return-void
.end method

.method public static final synthetic access$setVerificationCodeField$p(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;Lcom/squareup/ui/XableEditText;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->verificationCodeField:Lcom/squareup/ui/XableEditText;

    return-void
.end method

.method private final applyResendCodeLink(Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;IILkotlin/jvm/functions/Function0;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;",
            "II",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 159
    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    iget-object v1, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    const-string v2, "subtitle"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/squareup/widgets/MessageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 160
    invoke-virtual {v0, p3}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p3

    .line 161
    new-instance v0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$applyResendCodeLink$resendCodeLinkText$1;

    iget-object v1, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, p2}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v1

    invoke-direct {v0, p0, p4, p2, v1}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$applyResendCodeLink$resendCodeLinkText$1;-><init>(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;Lkotlin/jvm/functions/Function0;II)V

    check-cast v0, Lcom/squareup/ui/LinkSpan;

    invoke-virtual {p3, v0}, Lcom/squareup/ui/LinkSpan$Builder;->clickableSpan(Lcom/squareup/ui/LinkSpan;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    .line 166
    invoke-virtual {p2}, Lcom/squareup/ui/LinkSpan$Builder;->asSpannable()Landroid/text/Spannable;

    move-result-object p2

    .line 169
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->getSmsTwoFactorDetails()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object p3

    iget-object p3, p3, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->description:Ljava/lang/String;

    if-eqz p3, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->getSmsTwoFactorDetails()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    iget-object p3, p1, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->phone:Ljava/lang/String;

    .line 174
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 171
    :cond_2
    iget-object p4, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_verification_sms_subtitle:I

    invoke-interface {p4, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p4

    const-string v0, "phoneNumber"

    .line 172
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p3, Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0}, Lcom/squareup/widgets/MessageView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "subtitle.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v2, 0x0

    .line 203
    new-instance v3, Lcom/squareup/fonts/FontSpan;

    invoke-static {v1, v2}, Lcom/squareup/marketfont/MarketFont$Weight;->resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;I)I

    move-result v1

    invoke-direct {v3, v0, v1}, Lcom/squareup/fonts/FontSpan;-><init>(Landroid/content/Context;I)V

    check-cast v3, Landroid/text/style/CharacterStyle;

    .line 172
    invoke-static {p3, v3}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    const-string v0, "phone_number"

    invoke-virtual {p4, v0, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    .line 173
    check-cast p2, Ljava/lang/CharSequence;

    const-string p4, "resend_code"

    invoke-virtual {p3, p4, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 174
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic applyResendCodeLink$default(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;IILkotlin/jvm/functions/Function0;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    .line 157
    sget-object p4, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$applyResendCodeLink$1;->INSTANCE:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$applyResendCodeLink$1;

    check-cast p4, Lkotlin/jvm/functions/Function0;

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->applyResendCodeLink(Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;IILkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 194
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 195
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->title:Lcom/squareup/widgets/MessageView;

    .line 196
    sget v0, Lcom/squareup/marin/R$id;->subtitle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    .line 197
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->verification_code_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->verificationCodeField:Lcom/squareup/ui/XableEditText;

    .line 198
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->remember_this_device:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object p1, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->rememberThisDeviceToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    return-void
.end method

.method private final getRememberThisDeviceIsChecked()Z
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->rememberThisDeviceToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-nez v0, :cond_0

    const-string v1, "rememberThisDeviceToggle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v0

    return v0
.end method

.method private final getVerificationCode()Ljava/lang/String;
    .locals 2

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->verificationCodeField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_0

    const-string v1, "verificationCodeField"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    if-eqz v0, :cond_1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    goto :goto_1

    :cond_3
    const-string v0, ""

    :goto_1
    return-object v0
.end method

.method private final indicateVerificationCodeError()V
    .locals 4

    .line 186
    new-instance v0, Lcom/squareup/register/widgets/validation/ShakeAnimation;

    invoke-direct {v0}, Lcom/squareup/register/widgets/validation/ShakeAnimation;-><init>()V

    .line 188
    new-instance v1, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;

    iget-object v2, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->verificationCodeField:Lcom/squareup/ui/XableEditText;

    const-string v3, "verificationCodeField"

    if-nez v2, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v2}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-direct {v1, v2}, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;-><init>(Landroid/widget/TextView;)V

    check-cast v1, Landroid/view/animation/Animation$AnimationListener;

    .line 187
    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/validation/ShakeAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 190
    iget-object v1, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->verificationCodeField:Lcom/squareup/ui/XableEditText;

    if-nez v1, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/animation/Animation;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/XableEditText;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private final isVerificationCodeValid()Z
    .locals 1

    .line 60
    invoke-direct {p0}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->getVerificationCode()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->bindViews(Landroid/view/View;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->title:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string v1, "title"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_verification_sms_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->verificationCodeField:Lcom/squareup/ui/XableEditText;

    const-string v1, "verificationCodeField"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v2, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$1;-><init>(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;)V

    check-cast v2, Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->verificationCodeField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->requestFocus()Z

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->rememberThisDeviceToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-nez v0, :cond_3

    const-string v1, "rememberThisDeviceToggle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 76
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_verification_remember_this_device:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 75
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setText(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->screenData:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;-><init>(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->screenData:Lio/reactivex/Observable;

    sget-object v1, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$3;->INSTANCE:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$3;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 140
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screenData.map { (screen\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 141
    new-instance v1, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$4;-><init>(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public final updateEnabledState()V
    .locals 2

    .line 182
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->isVerificationCodeValid()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method
