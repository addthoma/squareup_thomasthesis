.class final Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;
.super Lkotlin/jvm/internal/Lambda;
.source "VerificationCodeSmsCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;->this$0:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;->$view:Landroid/view/View;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 39
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;->invoke(Lcom/squareup/workflow/legacy/Screen;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component1()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component2()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 80
    new-instance v0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$1;-><init>(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 89
    iget-object v1, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;->this$0:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->access$getActionBar$p(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;)Lcom/squareup/marin/widgets/ActionBarView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    const-string v3, "actionBar.presenter"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    new-instance v3, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 85
    iget-object v4, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;->$view:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_verify:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v3

    .line 86
    new-instance v4, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$2;

    invoke-direct {v4, v0}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$2;-><init>(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$1;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    new-instance v5, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$sam$java_lang_Runnable$0;

    invoke-direct {v5, v4}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v5, Ljava/lang/Runnable;

    invoke-virtual {v3, v5}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v3

    .line 87
    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v5, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;->$view:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/squareup/common/authenticatorviews/R$string;->back:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v3, v4, v5}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v3

    .line 88
    new-instance v4, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$3;

    invoke-direct {v4, p1}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$3;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v4, Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v3

    .line 89
    invoke-virtual {v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 95
    iget-object v1, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;->this$0:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->access$getVerificationCodeField$p(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;)Lcom/squareup/ui/XableEditText;

    move-result-object v1

    new-instance v3, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$4;

    invoke-direct {v3, p0, v0}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$4;-><init>(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$1;)V

    check-cast v3, Lcom/squareup/debounce/DebouncedOnEditorActionListener;

    invoke-virtual {v1, v3}, Lcom/squareup/ui/XableEditText;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;->$view:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$5;

    invoke-direct {v1, p1}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$5;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 113
    invoke-virtual {v2}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->getResendCodeLinkEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;->this$0:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;

    .line 120
    sget v1, Lcom/squareup/marin/R$color;->marin_blue:I

    sget v3, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_resend_code:I

    .line 121
    new-instance v4, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$6;

    invoke-direct {v4, p1}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$6;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 119
    invoke-static {v0, v2, v1, v3, v4}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->access$applyResendCodeLink(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;IILkotlin/jvm/functions/Function0;)V

    goto :goto_0

    .line 130
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;->this$0:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;

    .line 131
    sget v3, Lcom/squareup/marin/R$color;->marin_light_gray:I

    .line 132
    sget v4, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_resend_code_pending:I

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    .line 130
    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->applyResendCodeLink$default(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;IILkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    .line 136
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;->this$0:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;

    invoke-virtual {p1}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->updateEnabledState()V

    return-void
.end method
