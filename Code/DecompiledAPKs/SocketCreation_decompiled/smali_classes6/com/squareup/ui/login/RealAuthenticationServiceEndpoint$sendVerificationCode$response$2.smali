.class final Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$sendVerificationCode$response$2;
.super Ljava/lang/Object;
.source "AuthenticationServiceEndpoint.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;->sendVerificationCode(Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;ZLjava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u00012\u0012\u0010\u0003\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00020\u00040\u0001H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/login/AuthenticationCallResult;",
        "Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;",
        "it",
        "Lcom/squareup/util/Optional;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$sendVerificationCode$response$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$sendVerificationCode$response$2;

    invoke-direct {v0}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$sendVerificationCode$response$2;-><init>()V

    sput-object v0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$sendVerificationCode$response$2;->INSTANCE:Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$sendVerificationCode$response$2;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/ui/login/AuthenticationCallResult;)Lcom/squareup/ui/login/AuthenticationCallResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticationCallResult<",
            "+",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;",
            ">;>;)",
            "Lcom/squareup/ui/login/AuthenticationCallResult<",
            "Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 280
    instance-of v0, p1, Lcom/squareup/ui/login/AuthenticationCallResult$Success;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/ui/login/AuthenticationCallResult$Success;

    check-cast p1, Lcom/squareup/ui/login/AuthenticationCallResult$Success;

    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticationCallResult$Success;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/util/Optional;

    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValueOrNull()Ljava/lang/Object;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/login/AuthenticationCallResult$Success;-><init>(Ljava/lang/Object;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/ui/login/AuthenticationCallResult;

    goto :goto_0

    .line 281
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    if-eqz v0, :cond_1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 169
    check-cast p1, Lcom/squareup/ui/login/AuthenticationCallResult;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$sendVerificationCode$response$2;->apply(Lcom/squareup/ui/login/AuthenticationCallResult;)Lcom/squareup/ui/login/AuthenticationCallResult;

    move-result-object p1

    return-object p1
.end method
