.class final Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$$special$$inlined$apply$lambda$2;
.super Ljava/lang/Object;
.source "VerificationCodeGoogleAuthCoordinator.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;->invoke(Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nVerificationCodeGoogleAuthCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 VerificationCodeGoogleAuthCoordinator.kt\ncom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$4$2\n+ 2 AuthenticatorEvent.kt\ncom/squareup/ui/login/AuthenticatorEventKt\n*L\n1#1,151:1\n200#2:152\n*E\n*S KotlinDebug\n*F\n+ 1 VerificationCodeGoogleAuthCoordinator.kt\ncom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$4$2\n*L\n101#1:152\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "run",
        "com/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$4$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $hasSmsTwoFactorDetails$inlined:Z

.field final synthetic $verifyCode$2$inlined:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$2;

.field final synthetic $workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

.field final synthetic this$0:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$2;Lcom/squareup/workflow/legacy/WorkflowInput;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$$special$$inlined$apply$lambda$2;->this$0:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;

    iput-object p2, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$$special$$inlined$apply$lambda$2;->$verifyCode$2$inlined:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$2;

    iput-object p3, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$$special$$inlined$apply$lambda$2;->$workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    iput-boolean p4, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$$special$$inlined$apply$lambda$2;->$hasSmsTwoFactorDetails$inlined:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$$special$$inlined$apply$lambda$2;->$workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 152
    new-instance v1, Lcom/squareup/ui/login/AuthenticatorEvent$GoBackFrom;

    const-class v2, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;

    invoke-direct {v1, v2}, Lcom/squareup/ui/login/AuthenticatorEvent$GoBackFrom;-><init>(Ljava/lang/Class;)V

    .line 101
    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
