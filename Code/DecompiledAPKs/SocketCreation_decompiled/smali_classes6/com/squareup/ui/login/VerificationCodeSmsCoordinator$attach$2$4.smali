.class public final Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$4;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "VerificationCodeSmsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;->invoke(Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000#\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J$\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0010\u0008\u001a\u0004\u0018\u00010\tH\u0016\u00a8\u0006\n"
    }
    d2 = {
        "com/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$4",
        "Lcom/squareup/debounce/DebouncedOnEditorActionListener;",
        "doOnEditorAction",
        "",
        "view",
        "Landroid/widget/TextView;",
        "actionId",
        "",
        "keyEvent",
        "Landroid/view/KeyEvent;",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $verifyCode$1:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$1;

.field final synthetic this$0:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$1;",
            ")V"
        }
    .end annotation

    .line 95
    iput-object p1, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$4;->this$0:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;

    iput-object p2, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$4;->$verifyCode$1:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$1;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x4

    if-ne p2, p1, :cond_0

    .line 101
    iget-object p1, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$4;->this$0:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;

    iget-object p1, p1, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;->this$0:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->access$isVerificationCodeValid$p(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 102
    iget-object p1, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$4;->$verifyCode$1:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$1;

    invoke-virtual {p1}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$1;->invoke()V

    const/4 p1, 0x1

    goto :goto_0

    .line 105
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2$4;->this$0:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;

    iget-object p1, p1, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$attach$2;->this$0:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->access$indicateVerificationCodeError(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;)V

    const/4 p1, 0x0

    :goto_0
    return p1
.end method
