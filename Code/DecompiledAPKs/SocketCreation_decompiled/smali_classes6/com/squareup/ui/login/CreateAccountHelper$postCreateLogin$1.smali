.class final Lcom/squareup/ui/login/CreateAccountHelper$postCreateLogin$1;
.super Ljava/lang/Object;
.source "CreateAccountHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/CreateAccountHelper;->postCreateLogin(Ljava/lang/String;Ljava/lang/String;Z)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/receiving/ReceivedResponse;",
        "Lcom/squareup/protos/register/api/LoginResponse;",
        "receivedResponse",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/login/CreateAccountHelper$postCreateLogin$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/login/CreateAccountHelper$postCreateLogin$1;

    invoke-direct {v0}, Lcom/squareup/ui/login/CreateAccountHelper$postCreateLogin$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/login/CreateAccountHelper$postCreateLogin$1;->INSTANCE:Lcom/squareup/ui/login/CreateAccountHelper$postCreateLogin$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/receiving/ReceivedResponse;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "Lcom/squareup/protos/register/api/LoginResponse;",
            ">;)",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "Lcom/squareup/protos/register/api/LoginResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "receivedResponse"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    sget-object v0, Lcom/squareup/receiving/ReceivedResponse;->Companion:Lcom/squareup/receiving/ReceivedResponse$Companion;

    sget-object v1, Lcom/squareup/ui/login/CreateAccountHelper$postCreateLogin$1$1;->INSTANCE:Lcom/squareup/ui/login/CreateAccountHelper$postCreateLogin$1$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/receiving/ReceivedResponse$Companion;->rejectIfNot(Lcom/squareup/receiving/ReceivedResponse;Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/CreateAccountHelper$postCreateLogin$1;->apply(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    return-object p1
.end method
