.class public final Lcom/squareup/ui/login/AuthenticatorBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "AuthenticatorBootstrapScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0008\u0010\r\u001a\u00020\u000eH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ui/login/AuthenticatorBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "asLandingScreen",
        "",
        "(Z)V",
        "directionOverride",
        "Lflow/Direction;",
        "getDirectionOverride",
        "()Lflow/Direction;",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "Lcom/squareup/ui/login/AuthenticatorScope;",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final asLandingScreen:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/ui/login/AuthenticatorBootstrapScreen;->asLandingScreen:Z

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;->Companion:Lcom/squareup/ui/login/AuthenticatorWorkflowRunner$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner$Companion;->get(Lmortar/MortarScope;)Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;

    move-result-object p1

    .line 30
    iget-boolean v0, p0, Lcom/squareup/ui/login/AuthenticatorBootstrapScreen;->asLandingScreen:Z

    invoke-virtual {p1, v0}, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;->start$loggedout_release(Z)V

    return-void
.end method

.method public getDirectionOverride()Lflow/Direction;
    .locals 1

    .line 24
    sget-object v0, Lflow/Direction;->REPLACE:Lflow/Direction;

    return-object v0
.end method

.method public getParentKey()Lcom/squareup/ui/login/AuthenticatorScope;
    .locals 1

    .line 26
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScope;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorScope;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorBootstrapScreen;->getParentKey()Lcom/squareup/ui/login/AuthenticatorScope;

    move-result-object v0

    return-object v0
.end method
