.class public final Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;
.super Lcom/squareup/ui/login/AuthenticatorScreen;
.source "AuthenticatorScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/AuthenticatorScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EmailPassword"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0010\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B;\u0008\u0007\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\tH\u00c6\u0003J=\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00032\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u00d6\u0003J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001J\t\u0010\u001d\u001a\u00020\u0006H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u000cR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u000c\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;",
        "Lcom/squareup/ui/login/AuthenticatorScreen;",
        "asLandingScreen",
        "",
        "supportsDeviceCode",
        "emailForPrefill",
        "",
        "isWorldEnabled",
        "password",
        "Lcom/squareup/account/SecretString;",
        "(ZZLjava/lang/String;ZLcom/squareup/account/SecretString;)V",
        "getAsLandingScreen",
        "()Z",
        "getEmailForPrefill",
        "()Ljava/lang/String;",
        "getPassword",
        "()Lcom/squareup/account/SecretString;",
        "getSupportsDeviceCode",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final asLandingScreen:Z

.field private final emailForPrefill:Ljava/lang/String;

.field private final isWorldEnabled:Z

.field private final password:Lcom/squareup/account/SecretString;

.field private final supportsDeviceCode:Z


# direct methods
.method public constructor <init>()V
    .locals 8

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1f

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;-><init>(ZZLjava/lang/String;ZLcom/squareup/account/SecretString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 8

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1e

    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;-><init>(ZZLjava/lang/String;ZLcom/squareup/account/SecretString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(ZZ)V
    .locals 8

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1c

    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;-><init>(ZZLjava/lang/String;ZLcom/squareup/account/SecretString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(ZZLjava/lang/String;)V
    .locals 8

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x18

    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;-><init>(ZZLjava/lang/String;ZLcom/squareup/account/SecretString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(ZZLjava/lang/String;Z)V
    .locals 8

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;-><init>(ZZLjava/lang/String;ZLcom/squareup/account/SecretString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(ZZLjava/lang/String;ZLcom/squareup/account/SecretString;)V
    .locals 1

    const-string v0, "password"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 49
    invoke-direct {p0, v0}, Lcom/squareup/ui/login/AuthenticatorScreen;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->asLandingScreen:Z

    iput-boolean p2, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->supportsDeviceCode:Z

    iput-object p3, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->emailForPrefill:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->isWorldEnabled:Z

    iput-object p5, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->password:Lcom/squareup/account/SecretString;

    return-void
.end method

.method public synthetic constructor <init>(ZZLjava/lang/String;ZLcom/squareup/account/SecretString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 3

    and-int/lit8 p7, p6, 0x1

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    const/4 p7, 0x0

    goto :goto_0

    :cond_0
    move p7, p1

    :goto_0
    and-int/lit8 p1, p6, 0x2

    if-eqz p1, :cond_1

    const/4 p2, 0x1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    move v1, p2

    :goto_1
    and-int/lit8 p1, p6, 0x4

    if-eqz p1, :cond_2

    const/4 p1, 0x0

    .line 46
    move-object p3, p1

    check-cast p3, Ljava/lang/String;

    :cond_2
    move-object v2, p3

    and-int/lit8 p1, p6, 0x8

    if-eqz p1, :cond_3

    goto :goto_2

    :cond_3
    move v0, p4

    :goto_2
    and-int/lit8 p1, p6, 0x10

    if-eqz p1, :cond_4

    .line 48
    sget-object p1, Lcom/squareup/account/SecretString;->Companion:Lcom/squareup/account/SecretString$Companion;

    invoke-virtual {p1}, Lcom/squareup/account/SecretString$Companion;->getEMPTY()Lcom/squareup/account/SecretString;

    move-result-object p5

    :cond_4
    move-object p6, p5

    move-object p1, p0

    move p2, p7

    move p3, v1

    move-object p4, v2

    move p5, v0

    invoke-direct/range {p1 .. p6}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;-><init>(ZZLjava/lang/String;ZLcom/squareup/account/SecretString;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;ZZLjava/lang/String;ZLcom/squareup/account/SecretString;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-boolean p1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->asLandingScreen:Z

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-boolean p2, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->supportsDeviceCode:Z

    :cond_1
    move p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->emailForPrefill:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->isWorldEnabled:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->password:Lcom/squareup/account/SecretString;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move p3, p1

    move p4, p7

    move-object p5, v0

    move p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->copy(ZZLjava/lang/String;ZLcom/squareup/account/SecretString;)Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->asLandingScreen:Z

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->supportsDeviceCode:Z

    return v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->emailForPrefill:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->isWorldEnabled:Z

    return v0
.end method

.method public final component5()Lcom/squareup/account/SecretString;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->password:Lcom/squareup/account/SecretString;

    return-object v0
.end method

.method public final copy(ZZLjava/lang/String;ZLcom/squareup/account/SecretString;)Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;
    .locals 7

    const-string v0, "password"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    move-object v1, v0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;-><init>(ZZLjava/lang/String;ZLcom/squareup/account/SecretString;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    iget-boolean v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->asLandingScreen:Z

    iget-boolean v1, p1, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->asLandingScreen:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->supportsDeviceCode:Z

    iget-boolean v1, p1, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->supportsDeviceCode:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->emailForPrefill:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->emailForPrefill:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->isWorldEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->isWorldEnabled:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->password:Lcom/squareup/account/SecretString;

    iget-object p1, p1, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->password:Lcom/squareup/account/SecretString;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAsLandingScreen()Z
    .locals 1

    .line 44
    iget-boolean v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->asLandingScreen:Z

    return v0
.end method

.method public final getEmailForPrefill()Ljava/lang/String;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->emailForPrefill:Ljava/lang/String;

    return-object v0
.end method

.method public final getPassword()Lcom/squareup/account/SecretString;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->password:Lcom/squareup/account/SecretString;

    return-object v0
.end method

.method public final getSupportsDeviceCode()Z
    .locals 1

    .line 45
    iget-boolean v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->supportsDeviceCode:Z

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget-boolean v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->asLandingScreen:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->supportsDeviceCode:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->emailForPrefill:Ljava/lang/String;

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->isWorldEnabled:Z

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_3
    move v1, v2

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->password:Lcom/squareup/account/SecretString;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :cond_4
    add-int/2addr v0, v3

    return v0
.end method

.method public final isWorldEnabled()Z
    .locals 1

    .line 47
    iget-boolean v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->isWorldEnabled:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EmailPassword(asLandingScreen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->asLandingScreen:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", supportsDeviceCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->supportsDeviceCode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", emailForPrefill="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->emailForPrefill:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", isWorldEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->isWorldEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", password="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->password:Lcom/squareup/account/SecretString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
