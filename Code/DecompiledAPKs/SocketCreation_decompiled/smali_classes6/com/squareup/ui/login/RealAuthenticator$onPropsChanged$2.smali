.class final Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealAuthenticator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/RealAuthenticator;->onPropsChanged(Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealAuthenticator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealAuthenticator.kt\ncom/squareup/ui/login/RealAuthenticator$onPropsChanged$2\n*L\n1#1,1721:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "maybeHasStateToResume",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $new:Lcom/squareup/ui/login/AuthenticatorInput;

.field final synthetic $old:Lcom/squareup/ui/login/AuthenticatorInput;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/ui/login/AuthenticatorInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$2;->$old:Lcom/squareup/ui/login/AuthenticatorInput;

    iput-object p2, p0, Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$2;->$new:Lcom/squareup/ui/login/AuthenticatorInput;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 3

    const-string v0, "$this$maybeHasStateToResume"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$2;->$old:Lcom/squareup/ui/login/AuthenticatorInput;

    iget-object v1, p0, Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$2;->$new:Lcom/squareup/ui/login/AuthenticatorInput;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$2;->$new:Lcom/squareup/ui/login/AuthenticatorInput;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorInput;->getUnitSelectionFlow()Lcom/squareup/ui/login/UnitSelectionFlow;

    move-result-object v0

    instance-of v2, v0, Lcom/squareup/ui/login/UnitSelectionFlow$Go;

    if-nez v2, :cond_1

    move-object v0, v1

    :cond_1
    check-cast v0, Lcom/squareup/ui/login/UnitSelectionFlow$Go;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/squareup/ui/login/UnitSelectionFlow$Go;->getResumeState()Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    move-object v1, v0

    :cond_2
    return-object v1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 157
    check-cast p1, Lcom/squareup/ui/login/AuthenticatorState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/RealAuthenticator$onPropsChanged$2;->invoke(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    return-object p1
.end method
