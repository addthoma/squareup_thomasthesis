.class public final Lcom/squareup/ui/login/TrustedDeviceDetailsStoreKt;
.super Ljava/lang/Object;
.source "TrustedDeviceDetailsStore.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTrustedDeviceDetailsStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TrustedDeviceDetailsStore.kt\ncom/squareup/ui/login/TrustedDeviceDetailsStoreKt\n+ 2 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n*L\n1#1,31:1\n469#2,7:32\n*E\n*S KotlinDebug\n*F\n+ 1 TrustedDeviceDetailsStore.kt\ncom/squareup/ui/login/TrustedDeviceDetailsStoreKt\n*L\n21#1,7:32\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u001a\"\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001*\u00020\u00042\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006H\u0000\u001a\u0014\u0010\u0007\u001a\u00020\u0008*\u00020\u00042\u0006\u0010\t\u001a\u00020\u0003H\u0000\u00a8\u0006\n"
    }
    d2 = {
        "removeExpiredDeviceDetails",
        "",
        "",
        "Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;",
        "Lcom/squareup/ui/login/TrustedDeviceDetailsStore;",
        "nowMillis",
        "",
        "update",
        "",
        "details",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final removeExpiredDeviceDetails(Lcom/squareup/ui/login/TrustedDeviceDetailsStore;J)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/TrustedDeviceDetailsStore;",
            "J)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$removeExpiredDeviceDetails"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-interface {p0}, Lcom/squareup/ui/login/TrustedDeviceDetailsStore;->getPersonTokenToDeviceDetails()Ljava/util/Map;

    move-result-object v0

    .line 32
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 33
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 34
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    .line 21
    iget-object v3, v3, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;->expiry:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sub-long/2addr v3, p1

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-lez v7, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_0

    .line 35
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 38
    :cond_2
    check-cast v1, Ljava/util/Map;

    .line 23
    invoke-interface {p0, v1}, Lcom/squareup/ui/login/TrustedDeviceDetailsStore;->setPersonTokenToDeviceDetails(Ljava/util/Map;)V

    return-object v1
.end method

.method public static synthetic removeExpiredDeviceDetails$default(Lcom/squareup/ui/login/TrustedDeviceDetailsStore;JILjava/lang/Object;)Ljava/util/Map;
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    .line 20
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/squareup/ui/login/TrustedDeviceDetailsStoreKt;->removeExpiredDeviceDetails(Lcom/squareup/ui/login/TrustedDeviceDetailsStore;J)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static final update(Lcom/squareup/ui/login/TrustedDeviceDetailsStore;Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;)V
    .locals 2

    const-string v0, "$this$update"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "details"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-interface {p0}, Lcom/squareup/ui/login/TrustedDeviceDetailsStore;->getPersonTokenToDeviceDetails()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;->person_token:Ljava/lang/String;

    invoke-static {v1, p1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    invoke-interface {p0, p1}, Lcom/squareup/ui/login/TrustedDeviceDetailsStore;->setPersonTokenToDeviceDetails(Ljava/util/Map;)V

    return-void
.end method
