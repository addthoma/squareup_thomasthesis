.class public final Lcom/squareup/ui/login/AuthenticatorEventKt;
.super Ljava/lang/Object;
.source "AuthenticatorEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u001b\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\n\u0008\u0000\u0010\u0002\u0018\u0001*\u00020\u0003H\u0086\u0008\u00a8\u0006\u0004"
    }
    d2 = {
        "GoBackFrom",
        "Lcom/squareup/ui/login/AuthenticatorEvent$GoBackFrom;",
        "R",
        "Lcom/squareup/ui/login/AuthenticatorScreen;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic GoBackFrom()Lcom/squareup/ui/login/AuthenticatorEvent$GoBackFrom;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Lcom/squareup/ui/login/AuthenticatorScreen;",
            ">()",
            "Lcom/squareup/ui/login/AuthenticatorEvent$GoBackFrom<",
            "TR;>;"
        }
    .end annotation

    .line 200
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorEvent$GoBackFrom;

    const/4 v1, 0x4

    const-string v2, "R"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Lcom/squareup/ui/login/AuthenticatorScreen;

    invoke-direct {v0, v1}, Lcom/squareup/ui/login/AuthenticatorEvent$GoBackFrom;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method
