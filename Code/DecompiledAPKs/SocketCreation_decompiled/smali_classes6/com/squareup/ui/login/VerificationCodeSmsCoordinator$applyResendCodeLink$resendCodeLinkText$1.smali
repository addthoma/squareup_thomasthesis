.class public final Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$applyResendCodeLink$resendCodeLinkText$1;
.super Lcom/squareup/ui/LinkSpan;
.source "VerificationCodeSmsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;->applyResendCodeLink(Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;IILkotlin/jvm/functions/Function0;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/ui/login/VerificationCodeSmsCoordinator$applyResendCodeLink$resendCodeLinkText$1",
        "Lcom/squareup/ui/LinkSpan;",
        "onClick",
        "",
        "widget",
        "Landroid/view/View;",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $colorId:I

.field final synthetic $onNextPress:Lkotlin/jvm/functions/Function0;

.field final synthetic this$0:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;Lkotlin/jvm/functions/Function0;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0;",
            "II)V"
        }
    .end annotation

    .line 161
    iput-object p1, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$applyResendCodeLink$resendCodeLinkText$1;->this$0:Lcom/squareup/ui/login/VerificationCodeSmsCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$applyResendCodeLink$resendCodeLinkText$1;->$onNextPress:Lkotlin/jvm/functions/Function0;

    iput p3, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$applyResendCodeLink$resendCodeLinkText$1;->$colorId:I

    invoke-direct {p0, p4}, Lcom/squareup/ui/LinkSpan;-><init>(I)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    const-string v0, "widget"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    iget-object p1, p0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$applyResendCodeLink$resendCodeLinkText$1;->$onNextPress:Lkotlin/jvm/functions/Function0;

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method
