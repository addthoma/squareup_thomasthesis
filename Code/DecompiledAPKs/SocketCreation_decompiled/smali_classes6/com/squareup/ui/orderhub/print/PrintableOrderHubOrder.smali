.class public final Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;
.super Ljava/lang/Object;
.source "PrintableOrderHubOrder.kt"

# interfaces
.implements Lcom/squareup/print/PrintableOrder;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPrintableOrderHubOrder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PrintableOrderHubOrder.kt\ncom/squareup/ui/orderhub/print/PrintableOrderHubOrder\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,168:1\n1360#2:169\n1429#2,3:170\n704#2:173\n777#2,2:174\n1360#2:176\n1429#2,3:177\n*E\n*S KotlinDebug\n*F\n+ 1 PrintableOrderHubOrder.kt\ncom/squareup/ui/orderhub/print/PrintableOrderHubOrder\n*L\n47#1:169\n47#1,3:170\n72#1:173\n72#1,2:174\n73#1:176\n73#1,3:177\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0012\u0010(\u001a\u0004\u0018\u00010)2\u0006\u0010*\u001a\u00020+H\u0016J\u0008\u0010,\u001a\u00020#H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\u0004\u0018\u00010\nX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\r\u001a\u00020\u000eX\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0012X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u001a\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u00128VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0017\u0010\u0015R\u0016\u0010\u0018\u001a\u0004\u0018\u00010\nX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u000cR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u001a\u001a\u0004\u0018\u00010\nX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u000cR\u001a\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0012X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u0015R\u0016\u0010\u001e\u001a\u0004\u0018\u00010\u001f8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008 \u0010!R\u001c\u0010\"\u001a\u0004\u0018\u00010#X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008$\u0010%\"\u0004\u0008&\u0010\'\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;",
        "Lcom/squareup/print/PrintableOrder;",
        "order",
        "Lcom/squareup/orders/model/Order;",
        "dateAndTimeFormatter",
        "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "(Lcom/squareup/orders/model/Order;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/time/CurrentTime;)V",
        "employeeToken",
        "",
        "getEmployeeToken",
        "()Ljava/lang/String;",
        "enabledSeatCount",
        "",
        "getEnabledSeatCount",
        "()I",
        "notVoidedLockedItems",
        "",
        "Lcom/squareup/print/PrintableOrderItem;",
        "getNotVoidedLockedItems",
        "()Ljava/util/List;",
        "notVoidedUnlockedItems",
        "getNotVoidedUnlockedItems",
        "openTicketNote",
        "getOpenTicketNote",
        "orderIdentifier",
        "getOrderIdentifier",
        "receiptNumbers",
        "getReceiptNumbers",
        "recipient",
        "Lcom/squareup/print/PrintableRecipient;",
        "getRecipient",
        "()Lcom/squareup/print/PrintableRecipient;",
        "timestampForReprint",
        "Ljava/util/Date;",
        "getTimestampForReprint",
        "()Ljava/util/Date;",
        "setTimestampForReprint",
        "(Ljava/util/Date;)V",
        "getDiningOption",
        "Lcom/squareup/checkout/DiningOption;",
        "res",
        "Lcom/squareup/util/Res;",
        "getTimestamp",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final dateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

.field private final employeeToken:Ljava/lang/String;

.field private final enabledSeatCount:I

.field private final notVoidedLockedItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketNote:Ljava/lang/String;

.field private final order:Lcom/squareup/orders/model/Order;

.field private final orderIdentifier:Ljava/lang/String;

.field private final receiptNumbers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private timestampForReprint:Ljava/util/Date;


# direct methods
.method public constructor <init>(Lcom/squareup/orders/model/Order;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/time/CurrentTime;)V
    .locals 4

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateAndTimeFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->order:Lcom/squareup/orders/model/Order;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->dateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->currentTime:Lcom/squareup/time/CurrentTime;

    .line 47
    iget-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->order:Lcom/squareup/orders/model/Order;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getAvailableLineItems(Lcom/squareup/orders/model/Order;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 169
    new-instance p2, Ljava/util/ArrayList;

    const/16 p3, 0xa

    invoke-static {p1, p3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-direct {p2, v0}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p2, Ljava/util/Collection;

    .line 170
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 171
    check-cast v0, Lcom/squareup/orders/model/Order$LineItem;

    .line 48
    new-instance v1, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;

    invoke-direct {v1, v0}, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItem;-><init>(Lcom/squareup/orders/model/Order$LineItem;)V

    .line 50
    invoke-interface {p2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 172
    :cond_0
    check-cast p2, Ljava/util/List;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->notVoidedLockedItems:Ljava/util/List;

    .line 57
    iget-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->order:Lcom/squareup/orders/model/Order;

    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getNote(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->openTicketNote:Ljava/lang/String;

    .line 59
    iget-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->order:Lcom/squareup/orders/model/Order;

    iget-object p1, p1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->orderIdentifier:Ljava/lang/String;

    .line 65
    iget-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->order:Lcom/squareup/orders/model/Order;

    iget-object p1, p1, Lcom/squareup/orders/model/Order;->tenders:Ljava/util/List;

    if-nez p1, :cond_1

    .line 66
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    goto/16 :goto_5

    .line 68
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->order:Lcom/squareup/orders/model/Order;

    iget-object p1, p1, Lcom/squareup/orders/model/Order;->tenders:Ljava/util/List;

    const-string p2, "order.tenders"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 173
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    check-cast p2, Ljava/util/Collection;

    .line 174
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/protos/connect/v2/resources/Tender;

    .line 72
    iget-object v3, v3, Lcom/squareup/protos/connect/v2/resources/Tender;->id:Ljava/lang/String;

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_2

    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 175
    :cond_4
    check-cast p2, Ljava/util/List;

    check-cast p2, Ljava/lang/Iterable;

    .line 176
    new-instance p1, Ljava/util/ArrayList;

    invoke-static {p2, p3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result p3

    invoke-direct {p1, p3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 177
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_3
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_7

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    .line 178
    check-cast p3, Lcom/squareup/protos/connect/v2/resources/Tender;

    .line 74
    iget-object v0, p3, Lcom/squareup/protos/connect/v2/resources/Tender;->id:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v3, 0x4

    if-lt v0, v3, :cond_5

    const/4 v0, 0x1

    goto :goto_4

    :cond_5
    const/4 v0, 0x0

    :goto_4
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 75
    iget-object p3, p3, Lcom/squareup/protos/connect/v2/resources/Tender;->id:Ljava/lang/String;

    const-string v0, "tender.id"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p3, :cond_6

    invoke-virtual {p3, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p3

    const-string v0, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, p3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_6
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 179
    :cond_7
    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 77
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    .line 65
    :goto_5
    iput-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->receiptNumbers:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getDiningOption(Lcom/squareup/util/Res;)Lcom/squareup/checkout/DiningOption;
    .locals 2

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->order:Lcom/squareup/orders/model/Order;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->dateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    invoke-static {v0, p1, v1}, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderKt;->diningOption(Lcom/squareup/orders/model/Order;Lcom/squareup/util/Res;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;)Lcom/squareup/checkout/DiningOption;

    move-result-object p1

    return-object p1
.end method

.method public getEmployeeToken()Ljava/lang/String;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->employeeToken:Ljava/lang/String;

    return-object v0
.end method

.method public getEnabledSeatCount()I
    .locals 1

    .line 55
    iget v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->enabledSeatCount:I

    return v0
.end method

.method public getNotVoidedLockedItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->notVoidedLockedItems:Ljava/util/List;

    return-object v0
.end method

.method public getNotVoidedUnlockedItems()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;"
        }
    .end annotation

    .line 44
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Order Hub orders do not support Unlocked items"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public getOpenTicketNote()Ljava/lang/String;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->openTicketNote:Ljava/lang/String;

    return-object v0
.end method

.method public getOrderIdentifier()Ljava/lang/String;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->orderIdentifier:Ljava/lang/String;

    return-object v0
.end method

.method public getReceiptNumbers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->receiptNumbers:Ljava/util/List;

    return-object v0
.end method

.method public getRecipient()Lcom/squareup/print/PrintableRecipient;
    .locals 3

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->order:Lcom/squareup/orders/model/Order;

    invoke-static {v0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    sget-object v2, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    goto :goto_1

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->order:Lcom/squareup/orders/model/Order;

    invoke-static {v0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getRecipient(Lcom/squareup/orders/model/Order$Fulfillment;)Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    if-eqz v0, :cond_3

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->order:Lcom/squareup/orders/model/Order;

    invoke-static {v0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getRecipient(Lcom/squareup/orders/model/Order$Fulfillment;)Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 87
    iget-object v1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->order:Lcom/squareup/orders/model/Order;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    const-string v2, "order.primaryFulfillment.type"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    new-instance v2, Lcom/squareup/ui/orderhub/print/PrintableOrderHubRecipient;

    invoke-direct {v2, v0, v1}, Lcom/squareup/ui/orderhub/print/PrintableOrderHubRecipient;-><init>(Lcom/squareup/orders/model/Order$FulfillmentRecipient;Lcom/squareup/orders/model/Order$FulfillmentType;)V

    move-object v1, v2

    goto :goto_0

    .line 86
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Required value was null."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 84
    :cond_3
    :goto_0
    check-cast v1, Lcom/squareup/print/PrintableRecipient;

    :goto_1
    return-object v1
.end method

.method public getTimestamp()Ljava/util/Date;
    .locals 3

    .line 61
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {v1}, Lcom/squareup/time/CurrentTime;->clock()Lorg/threeten/bp/Clock;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/Clock;->millis()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public getTimestampForReprint()Ljava/util/Date;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->timestampForReprint:Ljava/util/Date;

    return-object v0
.end method

.method public setTimestampForReprint(Ljava/util/Date;)V
    .locals 0

    .line 63
    iput-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrder;->timestampForReprint:Ljava/util/Date;

    return-void
.end method
