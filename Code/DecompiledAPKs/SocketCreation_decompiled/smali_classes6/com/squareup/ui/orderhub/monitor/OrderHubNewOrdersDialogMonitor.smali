.class public final Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;
.super Ljava/lang/Object;
.source "OrderHubNewOrdersDialogMonitor.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubNewOrdersDialogMonitor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubNewOrdersDialogMonitor.kt\ncom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Rx2.kt\ncom/squareup/util/rx2/Rx2Kt\n*L\n1#1,244:1\n1577#2,4:245\n1577#2,4:249\n19#3:253\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubNewOrdersDialogMonitor.kt\ncom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor\n*L\n150#1,4:245\n151#1,4:249\n190#1:253\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0088\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0001\u0018\u00002\u00020\u0001B\u0099\u0001\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000e\u0008\u0001\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u000e\u0008\u0001\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0007\u0012\u000e\u0008\u0001\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u0012\u0008\u0008\u0001\u0010\u000f\u001a\u00020\u0010\u0012\u0008\u0008\u0001\u0010\u0011\u001a\u00020\u0010\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u00a2\u0006\u0002\u0010\u001eJ\u001a\u0010)\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020-0,0+0*H\u0002J\u000e\u0010.\u001a\u0008\u0012\u0004\u0012\u00020\u00080*H\u0002J\u0010\u0010/\u001a\u00020\'2\u0006\u00100\u001a\u000201H\u0016J\u0008\u00102\u001a\u00020\'H\u0016J\u0016\u00103\u001a\u00020\'2\u000c\u00104\u001a\u0008\u0012\u0004\u0012\u00020-0,H\u0002R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u001f\u001a\u00020\u000e8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\"\u0010#\u001a\u0004\u0008 \u0010!R\u000e\u0010$\u001a\u00020\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010%\u001a\u0010\u0012\u000c\u0012\n (*\u0004\u0018\u00010\'0\'0&X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00065"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;",
        "Lmortar/Scoped;",
        "orderNotificationAudioPlayer",
        "Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;",
        "orderRepository",
        "Lcom/squareup/ordermanagerdata/OrderRepository;",
        "alertsEnabledPreference",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "alertsFrequencyPreference",
        "Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;",
        "ordersKnownBeforePreference",
        "lazyFlow",
        "Ldagger/Lazy;",
        "Lflow/Flow;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "computationScheduler",
        "appIdling",
        "Lcom/squareup/ui/main/AppIdling;",
        "posContainer",
        "Lcom/squareup/ui/main/PosContainer;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "buyerFlowStarter",
        "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
        "tenderStarter",
        "Lcom/squareup/ui/tender/TenderStarter;",
        "(Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Ldagger/Lazy;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/ui/main/AppIdling;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/ui/tender/TenderStarter;)V",
        "flow",
        "getFlow",
        "()Lflow/Flow;",
        "flow$delegate",
        "Ldagger/Lazy;",
        "hasAttemptedNotification",
        "requestIntervalRefresh",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "batchedNewUnknownOrders",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ordermanagerdata/ResultState$Success;",
        "",
        "Lcom/squareup/orders/model/Order;",
        "canShowNotification",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "showNewOrdersDialogScreen",
        "orders",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final alertsEnabledPreference:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final alertsFrequencyPreference:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;",
            ">;"
        }
    .end annotation
.end field

.field private final appIdling:Lcom/squareup/ui/main/AppIdling;

.field private final buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

.field private final computationScheduler:Lio/reactivex/Scheduler;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow$delegate:Ldagger/Lazy;

.field private hasAttemptedNotification:Z

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final orderNotificationAudioPlayer:Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;

.field private final orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

.field private final ordersKnownBeforePreference:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final posContainer:Lcom/squareup/ui/main/PosContainer;

.field private final requestIntervalRefresh:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    const-class v2, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "flow"

    const-string v4, "getFlow()Lflow/Flow;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Ldagger/Lazy;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/ui/main/AppIdling;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/ui/tender/TenderStarter;)V
    .locals 1
    .param p3    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/orderhub/settings/OrderHubAlertsEnabledPreference;
        .end annotation
    .end param
    .param p4    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/orderhub/settings/OrderHubAlertsFrequencyPreference;
        .end annotation
    .end param
    .param p5    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/orderhub/alerts/OrderHubOrdersKnownBeforePref;
        .end annotation
    .end param
    .param p7    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p8    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Computation;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Lio/reactivex/Scheduler;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/ui/main/AppIdling;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "orderNotificationAudioPlayer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderRepository"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "alertsEnabledPreference"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "alertsFrequencyPreference"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ordersKnownBeforePreference"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lazyFlow"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "computationScheduler"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appIdling"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "posContainer"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transaction"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerFlowStarter"

    invoke-static {p13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderStarter"

    invoke-static {p14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->orderNotificationAudioPlayer:Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->alertsEnabledPreference:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->alertsFrequencyPreference:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p5, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->ordersKnownBeforePreference:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p7, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p8, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->computationScheduler:Lio/reactivex/Scheduler;

    iput-object p9, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->appIdling:Lcom/squareup/ui/main/AppIdling;

    iput-object p10, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->posContainer:Lcom/squareup/ui/main/PosContainer;

    iput-object p11, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p12, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->features:Lcom/squareup/settings/server/Features;

    iput-object p13, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    iput-object p14, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    .line 84
    iput-object p6, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->flow$delegate:Ldagger/Lazy;

    .line 96
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefault(Unit)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->requestIntervalRefresh:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$getBuyerFlowStarter$p(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)Lcom/squareup/ui/buyer/BuyerFlowStarter;
    .locals 0

    .line 63
    iget-object p0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    return-object p0
.end method

.method public static final synthetic access$getComputationScheduler$p(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)Lio/reactivex/Scheduler;
    .locals 0

    .line 63
    iget-object p0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->computationScheduler:Lio/reactivex/Scheduler;

    return-object p0
.end method

.method public static final synthetic access$getHasAttemptedNotification$p(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)Z
    .locals 0

    .line 63
    iget-boolean p0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->hasAttemptedNotification:Z

    return p0
.end method

.method public static final synthetic access$getOrderRepository$p(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)Lcom/squareup/ordermanagerdata/OrderRepository;
    .locals 0

    .line 63
    iget-object p0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    return-object p0
.end method

.method public static final synthetic access$getOrdersKnownBeforePreference$p(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 0

    .line 63
    iget-object p0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->ordersKnownBeforePreference:Lcom/f2prateek/rx/preferences2/Preference;

    return-object p0
.end method

.method public static final synthetic access$getRequestIntervalRefresh$p(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 63
    iget-object p0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->requestIntervalRefresh:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getTenderStarter$p(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)Lcom/squareup/ui/tender/TenderStarter;
    .locals 0

    .line 63
    iget-object p0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    return-object p0
.end method

.method public static final synthetic access$getTransaction$p(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)Lcom/squareup/payment/Transaction;
    .locals 0

    .line 63
    iget-object p0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->transaction:Lcom/squareup/payment/Transaction;

    return-object p0
.end method

.method public static final synthetic access$setHasAttemptedNotification$p(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;Z)V
    .locals 0

    .line 63
    iput-boolean p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->hasAttemptedNotification:Z

    return-void
.end method

.method public static final synthetic access$showNewOrdersDialogScreen(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;Ljava/util/List;)V
    .locals 0

    .line 63
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->showNewOrdersDialogScreen(Ljava/util/List;)V

    return-void
.end method

.method private final batchedNewUnknownOrders()Lio/reactivex/Observable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ordermanagerdata/ResultState$Success<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;>;"
        }
    .end annotation

    .line 167
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 169
    iget-object v1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->ordersKnownBeforePreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v1}, Lcom/f2prateek/rx/preferences2/Preference;->asObservable()Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "ordersKnownBeforePreference.asObservable()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 170
    iget-object v2, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->alertsEnabledPreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v2}, Lcom/f2prateek/rx/preferences2/Preference;->asObservable()Lio/reactivex/Observable;

    move-result-object v2

    const-string v3, "alertsEnabledPreference.asObservable()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    iget-object v3, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->alertsFrequencyPreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v3}, Lcom/f2prateek/rx/preferences2/Preference;->asObservable()Lio/reactivex/Observable;

    move-result-object v3

    const-string v4, "alertsFrequencyPreference.asObservable()"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    iget-object v4, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->requestIntervalRefresh:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v4, Lio/reactivex/Observable;

    .line 168
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 174
    new-instance v1, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$batchedNewUnknownOrders$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$batchedNewUnknownOrders$1;-><init>(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observables\n        .com\u2026t }\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 253
    const-class v1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "ofType(T::class.java)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    iget-object v1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observables\n        .com\u2026.observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final canShowNotification()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->posContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {v0}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v0

    .line 206
    new-instance v1, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$canShowNotification$onBlacklistedScreen$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$canShowNotification$onBlacklistedScreen$1;-><init>(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const/4 v1, 0x0

    .line 223
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    .line 225
    iget-object v2, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->cartChanges()Lrx/Observable;

    move-result-object v2

    const-string v3, "transaction.cartChanges()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 226
    invoke-static {v2}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v2

    .line 227
    sget-object v3, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$canShowNotification$isEditingCart$1;->INSTANCE:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$canShowNotification$isEditingCart$1;

    check-cast v3, Lio/reactivex/functions/Predicate;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    .line 228
    new-instance v3, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$canShowNotification$isEditingCart$2;

    invoke-direct {v3, p0}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$canShowNotification$isEditingCart$2;-><init>(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)V

    check-cast v3, Lio/reactivex/functions/Function;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 229
    invoke-virtual {v2, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v1

    .line 231
    sget-object v2, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    const-string v3, "onBlacklistedScreen"

    .line 232
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "isEditingCart"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->appIdling:Lcom/squareup/ui/main/AppIdling;

    invoke-virtual {v3}, Lcom/squareup/ui/main/AppIdling;->onIdleChanged()Lio/reactivex/Observable;

    move-result-object v3

    invoke-virtual {v2, v0, v1, v3}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 233
    sget-object v1, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$canShowNotification$1;->INSTANCE:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$canShowNotification$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 236
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 237
    iget-object v1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observables\n        .com\u2026.observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final getFlow()Lflow/Flow;
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->flow$delegate:Ldagger/Lazy;

    sget-object v1, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/dagger/LazysKt;->getValue(Ldagger/Lazy;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    return-object v0
.end method

.method private final showNewOrdersDialogScreen(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;)V"
        }
    .end annotation

    .line 150
    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    .line 245
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    move-object v4, v0

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v5, 0x0

    goto :goto_2

    .line 247
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    const/4 v5, 0x0

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/orders/model/Order;

    .line 150
    invoke-static {v6}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getDisplayState(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    move-result-object v6

    iget-object v6, v6, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    sget-object v7, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->UPCOMING:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    if-ne v6, v7, :cond_2

    const/4 v6, 0x1

    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    :goto_1
    if-eqz v6, :cond_1

    add-int/lit8 v5, v5, 0x1

    if-gez v5, :cond_1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwCountOverflow()V

    goto :goto_0

    :cond_3
    :goto_2
    if-eqz v1, :cond_4

    .line 249
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x0

    goto :goto_5

    .line 251
    :cond_4
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_5
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/orders/model/Order;

    .line 151
    invoke-static {v4}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getDisplayState(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    move-result-object v4

    iget-object v4, v4, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    sget-object v6, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->NEW:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    if-ne v4, v6, :cond_6

    const/4 v4, 0x1

    goto :goto_4

    :cond_6
    const/4 v4, 0x0

    :goto_4
    if-eqz v4, :cond_5

    add-int/lit8 v1, v1, 0x1

    if-gez v1, :cond_5

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwCountOverflow()V

    goto :goto_3

    :cond_7
    :goto_5
    add-int v0, v5, v1

    .line 153
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-ne v0, p1, :cond_8

    goto :goto_6

    :cond_8
    const/4 v2, 0x0

    :goto_6
    const-string p1, "Trying to show new order dialog for order not in the upcoming or new display state."

    .line 152
    invoke-static {v2, p1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 156
    iput-boolean v3, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->hasAttemptedNotification:Z

    .line 157
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->getFlow()Lflow/Flow;

    move-result-object p1

    .line 158
    new-instance v0, Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen;

    invoke-direct {v0, v1, v5}, Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen;-><init>(II)V

    .line 157
    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 6

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->batchedNewUnknownOrders()Lio/reactivex/Observable;

    move-result-object v0

    .line 100
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->canShowNotification()Lio/reactivex/Observable;

    move-result-object v1

    .line 101
    iget-object v2, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->orderNotificationAudioPlayer:Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;->initialize()V

    .line 108
    iget-object v2, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_APPLET_ROLLOUT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v2

    const-string v3, "features.featureEnabled(\u2026.ORDERHUB_APPLET_ROLLOUT)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v2}, Lcom/squareup/util/rx2/Rx2TransformersKt;->gateBy(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v2

    .line 109
    sget-object v4, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$1;->INSTANCE:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$1;

    check-cast v4, Lio/reactivex/functions/Predicate;

    invoke-virtual {v2, v4}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    const-string v4, "batchedNewUnknownOrders\n\u2026t.response.isNotEmpty() }"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    iget-object v4, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->ordersKnownBeforePreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v4}, Lcom/f2prateek/rx/preferences2/Preference;->asObservable()Lio/reactivex/Observable;

    move-result-object v4

    const-string v5, "ordersKnownBeforePreference.asObservable()"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lio/reactivex/ObservableSource;

    move-object v5, v1

    check-cast v5, Lio/reactivex/ObservableSource;

    invoke-static {v2, v4, v5}, Lcom/squareup/util/rx2/RxKotlinKt;->withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v2

    .line 111
    new-instance v4, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$2;

    invoke-direct {v4, p0}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$2;-><init>(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-static {v2, p1, v4}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 130
    iget-object v2, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->features:Lcom/squareup/settings/server/Features;

    sget-object v4, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_APPLET_ROLLOUT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v4}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/squareup/util/rx2/Rx2TransformersKt;->gateBy(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v1

    .line 131
    check-cast v0, Lio/reactivex/ObservableSource;

    invoke-static {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt;->withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    .line 132
    new-instance v1, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$3;-><init>(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)V

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "canShowNotification\n    \u2026asAttemptedNotification }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    new-instance v1, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$4;-><init>(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->orderNotificationAudioPlayer:Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;->shutDown()V

    return-void
.end method
