.class public final Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;
.super Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action;
.source "SearchWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UpdateSearchResults"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001b\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u000f\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0006H\u00c6\u0003J#\u0010\u000e\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\u0018\u0010\u0017\u001a\u00020\u0018*\u000e\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u001b0\u0019H\u0016R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;",
        "Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action;",
        "newOrders",
        "",
        "Lcom/squareup/orders/model/Order;",
        "filter",
        "Lcom/squareup/ui/orderhub/master/Filter;",
        "(Ljava/util/List;Lcom/squareup/ui/orderhub/master/Filter;)V",
        "getFilter",
        "()Lcom/squareup/ui/orderhub/master/Filter;",
        "getNewOrders",
        "()Ljava/util/List;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/ui/orderhub/search/SearchState;",
        "",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final filter:Lcom/squareup/ui/orderhub/master/Filter;

.field private final newOrders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/squareup/ui/orderhub/master/Filter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;",
            "Lcom/squareup/ui/orderhub/master/Filter;",
            ")V"
        }
    .end annotation

    const-string v0, "newOrders"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "filter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 225
    invoke-direct {p0, v0}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;->newOrders:Ljava/util/List;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;->filter:Lcom/squareup/ui/orderhub/master/Filter;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;Ljava/util/List;Lcom/squareup/ui/orderhub/master/Filter;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;->newOrders:Ljava/util/List;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;->filter:Lcom/squareup/ui/orderhub/master/Filter;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;->copy(Ljava/util/List;Lcom/squareup/ui/orderhub/master/Filter;)Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/ui/orderhub/search/SearchState;",
            "*>;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 227
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/orderhub/search/SearchState;

    .line 228
    iget-object v7, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;->newOrders:Ljava/util/List;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xf

    const/4 v10, 0x0

    .line 227
    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/orderhub/search/SearchState;->copy$default(Lcom/squareup/ui/orderhub/search/SearchState;ZLjava/lang/String;Ljava/lang/String;ZZLjava/util/List;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/search/SearchState;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method

.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;->newOrders:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Lcom/squareup/ui/orderhub/master/Filter;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;->filter:Lcom/squareup/ui/orderhub/master/Filter;

    return-object v0
.end method

.method public final copy(Ljava/util/List;Lcom/squareup/ui/orderhub/master/Filter;)Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;",
            "Lcom/squareup/ui/orderhub/master/Filter;",
            ")",
            "Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;"
        }
    .end annotation

    const-string v0, "newOrders"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "filter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;-><init>(Ljava/util/List;Lcom/squareup/ui/orderhub/master/Filter;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;->newOrders:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;->newOrders:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;->filter:Lcom/squareup/ui/orderhub/master/Filter;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;->filter:Lcom/squareup/ui/orderhub/master/Filter;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getFilter()Lcom/squareup/ui/orderhub/master/Filter;
    .locals 1

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;->filter:Lcom/squareup/ui/orderhub/master/Filter;

    return-object v0
.end method

.method public final getNewOrders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;"
        }
    .end annotation

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;->newOrders:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;->newOrders:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;->filter:Lcom/squareup/ui/orderhub/master/Filter;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UpdateSearchResults(newOrders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;->newOrders:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", filter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$UpdateSearchResults;->filter:Lcom/squareup/ui/orderhub/master/Filter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
