.class public final Lcom/squareup/ui/orderhub/DetailState;
.super Ljava/lang/Object;
.source "OrderHubState.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubState.kt\ncom/squareup/ui/orderhub/DetailState\n*L\n1#1,78:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0019\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001BY\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0008\u0012\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\r\u0012\u0014\u0008\u0002\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\u000f\u00a2\u0006\u0002\u0010\u0012J\t\u0010 \u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J\t\u0010\"\u001a\u00020\u0008H\u00c6\u0003J\t\u0010#\u001a\u00020\u0008H\u00c6\u0003J\u000b\u0010$\u001a\u0004\u0018\u00010\u000bH\u00c6\u0003J\u000b\u0010%\u001a\u0004\u0018\u00010\rH\u00c6\u0003J\u0015\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\u000fH\u00c6\u0003Je\u0010\'\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\u00082\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\r2\u0014\u0008\u0002\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\u000fH\u00c6\u0001J\u0013\u0010(\u001a\u00020\u00082\u0008\u0010)\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010*\u001a\u00020+H\u00d6\u0001J\t\u0010,\u001a\u00020\u0010H\u00d6\u0001R\u0011\u0010\t\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0013\u0010\u000c\u001a\u0004\u0018\u00010\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0014R\u001d\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001dR\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001f\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/DetailState;",
        "",
        "selectedFilter",
        "Lcom/squareup/ui/orderhub/master/Filter;",
        "filteredOrders",
        "",
        "Lcom/squareup/orders/model/Order;",
        "loadingMoreOrders",
        "",
        "canLoadMoreOrders",
        "syncingError",
        "Lcom/squareup/ordermanagerdata/ResultState$Failure;",
        "lastSyncDate",
        "Lorg/threeten/bp/ZonedDateTime;",
        "orderQuickActionStatusMap",
        "",
        "",
        "Lcom/squareup/ui/orderhub/OrderQuickActionStatus;",
        "(Lcom/squareup/ui/orderhub/master/Filter;Ljava/util/List;ZZLcom/squareup/ordermanagerdata/ResultState$Failure;Lorg/threeten/bp/ZonedDateTime;Ljava/util/Map;)V",
        "getCanLoadMoreOrders",
        "()Z",
        "getFilteredOrders",
        "()Ljava/util/List;",
        "getLastSyncDate",
        "()Lorg/threeten/bp/ZonedDateTime;",
        "getLoadingMoreOrders",
        "getOrderQuickActionStatusMap",
        "()Ljava/util/Map;",
        "getSelectedFilter",
        "()Lcom/squareup/ui/orderhub/master/Filter;",
        "getSyncingError",
        "()Lcom/squareup/ordermanagerdata/ResultState$Failure;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final canLoadMoreOrders:Z

.field private final filteredOrders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;"
        }
    .end annotation
.end field

.field private final lastSyncDate:Lorg/threeten/bp/ZonedDateTime;

.field private final loadingMoreOrders:Z

.field private final orderQuickActionStatusMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/orderhub/OrderQuickActionStatus;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedFilter:Lcom/squareup/ui/orderhub/master/Filter;

.field private final syncingError:Lcom/squareup/ordermanagerdata/ResultState$Failure;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/orderhub/master/Filter;Ljava/util/List;ZZLcom/squareup/ordermanagerdata/ResultState$Failure;Lorg/threeten/bp/ZonedDateTime;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/master/Filter;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;ZZ",
            "Lcom/squareup/ordermanagerdata/ResultState$Failure;",
            "Lorg/threeten/bp/ZonedDateTime;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/orderhub/OrderQuickActionStatus;",
            ">;)V"
        }
    .end annotation

    const-string v0, "selectedFilter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "filteredOrders"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderQuickActionStatusMap"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/DetailState;->selectedFilter:Lcom/squareup/ui/orderhub/master/Filter;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/DetailState;->filteredOrders:Ljava/util/List;

    iput-boolean p3, p0, Lcom/squareup/ui/orderhub/DetailState;->loadingMoreOrders:Z

    iput-boolean p4, p0, Lcom/squareup/ui/orderhub/DetailState;->canLoadMoreOrders:Z

    iput-object p5, p0, Lcom/squareup/ui/orderhub/DetailState;->syncingError:Lcom/squareup/ordermanagerdata/ResultState$Failure;

    iput-object p6, p0, Lcom/squareup/ui/orderhub/DetailState;->lastSyncDate:Lorg/threeten/bp/ZonedDateTime;

    iput-object p7, p0, Lcom/squareup/ui/orderhub/DetailState;->orderQuickActionStatusMap:Ljava/util/Map;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/ui/orderhub/master/Filter;Ljava/util/List;ZZLcom/squareup/ordermanagerdata/ResultState$Failure;Lorg/threeten/bp/ZonedDateTime;Ljava/util/Map;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 10

    and-int/lit8 v0, p8, 0x10

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 56
    move-object v0, v1

    check-cast v0, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    move-object v7, v0

    goto :goto_0

    :cond_0
    move-object v7, p5

    :goto_0
    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_1

    .line 57
    move-object v0, v1

    check-cast v0, Lorg/threeten/bp/ZonedDateTime;

    move-object v8, v0

    goto :goto_1

    :cond_1
    move-object/from16 v8, p6

    :goto_1
    and-int/lit8 v0, p8, 0x40

    if-eqz v0, :cond_2

    .line 58
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object v0

    move-object v9, v0

    goto :goto_2

    :cond_2
    move-object/from16 v9, p7

    :goto_2
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v2 .. v9}, Lcom/squareup/ui/orderhub/DetailState;-><init>(Lcom/squareup/ui/orderhub/master/Filter;Ljava/util/List;ZZLcom/squareup/ordermanagerdata/ResultState$Failure;Lorg/threeten/bp/ZonedDateTime;Ljava/util/Map;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/master/Filter;Ljava/util/List;ZZLcom/squareup/ordermanagerdata/ResultState$Failure;Lorg/threeten/bp/ZonedDateTime;Ljava/util/Map;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/DetailState;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/orderhub/DetailState;->selectedFilter:Lcom/squareup/ui/orderhub/master/Filter;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/orderhub/DetailState;->filteredOrders:Ljava/util/List;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/squareup/ui/orderhub/DetailState;->loadingMoreOrders:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/ui/orderhub/DetailState;->canLoadMoreOrders:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/ui/orderhub/DetailState;->syncingError:Lcom/squareup/ordermanagerdata/ResultState$Failure;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/ui/orderhub/DetailState;->lastSyncDate:Lorg/threeten/bp/ZonedDateTime;

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-object p7, p0, Lcom/squareup/ui/orderhub/DetailState;->orderQuickActionStatusMap:Ljava/util/Map;

    :cond_6
    move-object v4, p7

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move p5, v0

    move p6, v1

    move-object p7, v2

    move-object p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/ui/orderhub/DetailState;->copy(Lcom/squareup/ui/orderhub/master/Filter;Ljava/util/List;ZZLcom/squareup/ordermanagerdata/ResultState$Failure;Lorg/threeten/bp/ZonedDateTime;Ljava/util/Map;)Lcom/squareup/ui/orderhub/DetailState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/ui/orderhub/master/Filter;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/DetailState;->selectedFilter:Lcom/squareup/ui/orderhub/master/Filter;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/orderhub/DetailState;->filteredOrders:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/DetailState;->loadingMoreOrders:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/DetailState;->canLoadMoreOrders:Z

    return v0
.end method

.method public final component5()Lcom/squareup/ordermanagerdata/ResultState$Failure;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/DetailState;->syncingError:Lcom/squareup/ordermanagerdata/ResultState$Failure;

    return-object v0
.end method

.method public final component6()Lorg/threeten/bp/ZonedDateTime;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/DetailState;->lastSyncDate:Lorg/threeten/bp/ZonedDateTime;

    return-object v0
.end method

.method public final component7()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/orderhub/OrderQuickActionStatus;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/orderhub/DetailState;->orderQuickActionStatusMap:Ljava/util/Map;

    return-object v0
.end method

.method public final copy(Lcom/squareup/ui/orderhub/master/Filter;Ljava/util/List;ZZLcom/squareup/ordermanagerdata/ResultState$Failure;Lorg/threeten/bp/ZonedDateTime;Ljava/util/Map;)Lcom/squareup/ui/orderhub/DetailState;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/master/Filter;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;ZZ",
            "Lcom/squareup/ordermanagerdata/ResultState$Failure;",
            "Lorg/threeten/bp/ZonedDateTime;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/orderhub/OrderQuickActionStatus;",
            ">;)",
            "Lcom/squareup/ui/orderhub/DetailState;"
        }
    .end annotation

    const-string v0, "selectedFilter"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "filteredOrders"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderQuickActionStatusMap"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/orderhub/DetailState;

    move-object v1, v0

    move v4, p3

    move v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/orderhub/DetailState;-><init>(Lcom/squareup/ui/orderhub/master/Filter;Ljava/util/List;ZZLcom/squareup/ordermanagerdata/ResultState$Failure;Lorg/threeten/bp/ZonedDateTime;Ljava/util/Map;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/orderhub/DetailState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/orderhub/DetailState;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/DetailState;->selectedFilter:Lcom/squareup/ui/orderhub/master/Filter;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/DetailState;->selectedFilter:Lcom/squareup/ui/orderhub/master/Filter;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/DetailState;->filteredOrders:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/DetailState;->filteredOrders:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/DetailState;->loadingMoreOrders:Z

    iget-boolean v1, p1, Lcom/squareup/ui/orderhub/DetailState;->loadingMoreOrders:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/DetailState;->canLoadMoreOrders:Z

    iget-boolean v1, p1, Lcom/squareup/ui/orderhub/DetailState;->canLoadMoreOrders:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/DetailState;->syncingError:Lcom/squareup/ordermanagerdata/ResultState$Failure;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/DetailState;->syncingError:Lcom/squareup/ordermanagerdata/ResultState$Failure;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/DetailState;->lastSyncDate:Lorg/threeten/bp/ZonedDateTime;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/DetailState;->lastSyncDate:Lorg/threeten/bp/ZonedDateTime;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/DetailState;->orderQuickActionStatusMap:Ljava/util/Map;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/DetailState;->orderQuickActionStatusMap:Ljava/util/Map;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCanLoadMoreOrders()Z
    .locals 1

    .line 55
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/DetailState;->canLoadMoreOrders:Z

    return v0
.end method

.method public final getFilteredOrders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;"
        }
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/orderhub/DetailState;->filteredOrders:Ljava/util/List;

    return-object v0
.end method

.method public final getLastSyncDate()Lorg/threeten/bp/ZonedDateTime;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/orderhub/DetailState;->lastSyncDate:Lorg/threeten/bp/ZonedDateTime;

    return-object v0
.end method

.method public final getLoadingMoreOrders()Z
    .locals 1

    .line 54
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/DetailState;->loadingMoreOrders:Z

    return v0
.end method

.method public final getOrderQuickActionStatusMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/orderhub/OrderQuickActionStatus;",
            ">;"
        }
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/orderhub/DetailState;->orderQuickActionStatusMap:Ljava/util/Map;

    return-object v0
.end method

.method public final getSelectedFilter()Lcom/squareup/ui/orderhub/master/Filter;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/orderhub/DetailState;->selectedFilter:Lcom/squareup/ui/orderhub/master/Filter;

    return-object v0
.end method

.method public final getSyncingError()Lcom/squareup/ordermanagerdata/ResultState$Failure;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/orderhub/DetailState;->syncingError:Lcom/squareup/ordermanagerdata/ResultState$Failure;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/ui/orderhub/DetailState;->selectedFilter:Lcom/squareup/ui/orderhub/master/Filter;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/DetailState;->filteredOrders:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/orderhub/DetailState;->loadingMoreOrders:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/orderhub/DetailState;->canLoadMoreOrders:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/DetailState;->syncingError:Lcom/squareup/ordermanagerdata/ResultState$Failure;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/DetailState;->lastSyncDate:Lorg/threeten/bp/ZonedDateTime;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_5
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/DetailState;->orderQuickActionStatusMap:Ljava/util/Map;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DetailState(selectedFilter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/DetailState;->selectedFilter:Lcom/squareup/ui/orderhub/master/Filter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", filteredOrders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/DetailState;->filteredOrders:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", loadingMoreOrders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/orderhub/DetailState;->loadingMoreOrders:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", canLoadMoreOrders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/orderhub/DetailState;->canLoadMoreOrders:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", syncingError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/DetailState;->syncingError:Lcom/squareup/ordermanagerdata/ResultState$Failure;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", lastSyncDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/DetailState;->lastSyncDate:Lorg/threeten/bp/ZonedDateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", orderQuickActionStatusMap="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/DetailState;->orderQuickActionStatusMap:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
