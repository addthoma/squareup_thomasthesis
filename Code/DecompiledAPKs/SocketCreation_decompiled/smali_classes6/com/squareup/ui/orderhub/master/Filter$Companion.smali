.class public final Lcom/squareup/ui/orderhub/master/Filter$Companion;
.super Ljava/lang/Object;
.source "Filter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/master/Filter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFilter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Filter.kt\ncom/squareup/ui/orderhub/master/Filter$Companion\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n*L\n1#1,283:1\n704#2:284\n777#2,2:285\n1313#2,3:287\n1316#2,3:297\n704#2:300\n777#2,2:301\n704#2:303\n777#2,2:304\n1288#2:306\n1313#2,3:307\n1316#2,3:317\n704#2:320\n777#2,2:321\n1288#2:323\n1313#2,3:324\n1316#2,3:334\n347#3,7:290\n347#3,7:310\n347#3,7:327\n*E\n*S KotlinDebug\n*F\n+ 1 Filter.kt\ncom/squareup/ui/orderhub/master/Filter$Companion\n*L\n181#1:284\n181#1,2:285\n182#1,3:287\n182#1,3:297\n185#1:300\n185#1,2:301\n187#1:303\n187#1,2:304\n188#1:306\n188#1,3:307\n188#1,3:317\n191#1:320\n191#1,2:321\n192#1:323\n192#1,3:324\n192#1,3:334\n182#1,7:290\n188#1,7:310\n192#1,7:327\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J \u0010\t\u001a\u0012\u0012\u0004\u0012\u00020\u00040\nj\u0008\u0012\u0004\u0012\u00020\u0004`\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0002J6\u0010\u000e\u001a\u0014\u0012\u0004\u0012\u00020\u0004\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00110\u00100\u000f2\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u00102\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u0013\u001a\u00020\u0014R\u001b\u0010\u0003\u001a\u00020\u00048FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0007\u0010\u0008\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/master/Filter$Companion;",
        "",
        "()V",
        "DEFAULT",
        "Lcom/squareup/ui/orderhub/master/Filter;",
        "getDEFAULT",
        "()Lcom/squareup/ui/orderhub/master/Filter;",
        "DEFAULT$delegate",
        "Lkotlin/Lazy;",
        "filterComparator",
        "Ljava/util/Comparator;",
        "Lkotlin/Comparator;",
        "res",
        "Lcom/squareup/util/Res;",
        "getGroupedFilters",
        "",
        "",
        "Lcom/squareup/orders/model/Order;",
        "orders",
        "supportsUpcomingOrders",
        "",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 116
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/master/Filter$Companion;-><init>()V

    return-void
.end method

.method private final filterComparator(Lcom/squareup/util/Res;)Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            ")",
            "Ljava/util/Comparator<",
            "Lcom/squareup/ui/orderhub/master/Filter;",
            ">;"
        }
    .end annotation

    .line 201
    new-instance v0, Lcom/squareup/ui/orderhub/master/Filter$Companion$filterComparator$1;

    invoke-direct {v0, p1}, Lcom/squareup/ui/orderhub/master/Filter$Companion$filterComparator$1;-><init>(Lcom/squareup/util/Res;)V

    check-cast v0, Ljava/util/Comparator;

    return-object v0
.end method


# virtual methods
.method public final getDEFAULT()Lcom/squareup/ui/orderhub/master/Filter;
    .locals 2

    invoke-static {}, Lcom/squareup/ui/orderhub/master/Filter;->access$getDEFAULT$cp()Lkotlin/Lazy;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/orderhub/master/Filter;->Companion:Lcom/squareup/ui/orderhub/master/Filter$Companion;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/orderhub/master/Filter;

    return-object v0
.end method

.method public final getGroupedFilters(Ljava/util/List;Lcom/squareup/util/Res;Z)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Z)",
            "Ljava/util/Map<",
            "Lcom/squareup/ui/orderhub/master/Filter;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation

    const-string v0, "orders"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p3, :cond_0

    const/4 p3, 0x3

    new-array p3, p3, [Lkotlin/Pair;

    .line 163
    new-instance v3, Lcom/squareup/ui/orderhub/master/Filter$Status;

    sget-object v4, Lcom/squareup/protos/client/orders/OrderGroup;->ACTIVE:Lcom/squareup/protos/client/orders/OrderGroup;

    invoke-direct {v3, v4}, Lcom/squareup/ui/orderhub/master/Filter$Status;-><init>(Lcom/squareup/protos/client/orders/OrderGroup;)V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    check-cast v4, Ljava/util/List;

    invoke-static {v3, v4}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v3

    aput-object v3, p3, v2

    .line 164
    new-instance v3, Lcom/squareup/ui/orderhub/master/Filter$Status;

    sget-object v4, Lcom/squareup/protos/client/orders/OrderGroup;->UPCOMING:Lcom/squareup/protos/client/orders/OrderGroup;

    invoke-direct {v3, v4}, Lcom/squareup/ui/orderhub/master/Filter$Status;-><init>(Lcom/squareup/protos/client/orders/OrderGroup;)V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    check-cast v4, Ljava/util/List;

    invoke-static {v3, v4}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v3

    aput-object v3, p3, v1

    .line 165
    new-instance v3, Lcom/squareup/ui/orderhub/master/Filter$Status;

    sget-object v4, Lcom/squareup/protos/client/orders/OrderGroup;->COMPLETED:Lcom/squareup/protos/client/orders/OrderGroup;

    invoke-direct {v3, v4}, Lcom/squareup/ui/orderhub/master/Filter$Status;-><init>(Lcom/squareup/protos/client/orders/OrderGroup;)V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    check-cast v4, Ljava/util/List;

    invoke-static {v3, v4}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v3

    aput-object v3, p3, v0

    .line 162
    invoke-static {p3}, Lkotlin/collections/MapsKt;->linkedMapOf([Lkotlin/Pair;)Ljava/util/LinkedHashMap;

    move-result-object p3

    goto :goto_0

    :cond_0
    new-array p3, v0, [Lkotlin/Pair;

    .line 169
    new-instance v0, Lcom/squareup/ui/orderhub/master/Filter$Status;

    sget-object v3, Lcom/squareup/protos/client/orders/OrderGroup;->ACTIVE:Lcom/squareup/protos/client/orders/OrderGroup;

    invoke-direct {v0, v3}, Lcom/squareup/ui/orderhub/master/Filter$Status;-><init>(Lcom/squareup/protos/client/orders/OrderGroup;)V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/List;

    invoke-static {v0, v3}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v0

    aput-object v0, p3, v2

    .line 170
    new-instance v0, Lcom/squareup/ui/orderhub/master/Filter$Status;

    sget-object v3, Lcom/squareup/protos/client/orders/OrderGroup;->COMPLETED:Lcom/squareup/protos/client/orders/OrderGroup;

    invoke-direct {v0, v3}, Lcom/squareup/ui/orderhub/master/Filter$Status;-><init>(Lcom/squareup/protos/client/orders/OrderGroup;)V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/List;

    invoke-static {v0, v3}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v0

    aput-object v0, p3, v1

    .line 168
    invoke-static {p3}, Lkotlin/collections/MapsKt;->linkedMapOf([Lkotlin/Pair;)Ljava/util/LinkedHashMap;

    move-result-object p3

    .line 174
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 176
    check-cast p3, Ljava/util/Map;

    invoke-static {p3}, Lkotlin/collections/MapsKt;->toMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_d

    .line 180
    :cond_1
    check-cast p1, Ljava/lang/Iterable;

    .line 284
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 285
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/squareup/orders/model/Order;

    .line 181
    invoke-static {v5}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getGroup(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object v5

    if-eqz v5, :cond_3

    const/4 v5, 0x1

    goto :goto_2

    :cond_3
    const/4 v5, 0x0

    :goto_2
    if-eqz v5, :cond_2

    invoke-interface {v0, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 286
    :cond_4
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 287
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 288
    move-object v4, v3

    check-cast v4, Lcom/squareup/orders/model/Order;

    .line 182
    invoke-static {v4}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getGroup(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object v4

    if-eqz v4, :cond_6

    new-instance v5, Lcom/squareup/ui/orderhub/master/Filter$Status;

    invoke-direct {v5, v4}, Lcom/squareup/ui/orderhub/master/Filter$Status;-><init>(Lcom/squareup/protos/client/orders/OrderGroup;)V

    .line 289
    move-object v4, p3

    check-cast v4, Ljava/util/Map;

    .line 290
    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_5

    .line 289
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 293
    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    :cond_5
    check-cast v6, Ljava/util/List;

    .line 297
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 182
    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Required value was null."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 299
    :cond_7
    check-cast p3, Ljava/util/Map;

    .line 180
    check-cast p3, Ljava/util/LinkedHashMap;

    .line 184
    move-object v0, p0

    check-cast v0, Lcom/squareup/ui/orderhub/master/Filter$Companion;

    invoke-direct {v0, p2}, Lcom/squareup/ui/orderhub/master/Filter$Companion;->filterComparator(Lcom/squareup/util/Res;)Ljava/util/Comparator;

    move-result-object p2

    .line 300
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 301
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_8
    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/squareup/orders/model/Order;

    .line 185
    invoke-static {v4}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getGroup(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object v4

    sget-object v5, Lcom/squareup/protos/client/orders/OrderGroup;->COMPLETED:Lcom/squareup/protos/client/orders/OrderGroup;

    if-eq v4, v5, :cond_9

    const/4 v4, 0x1

    goto :goto_5

    :cond_9
    const/4 v4, 0x0

    :goto_5
    if-eqz v4, :cond_8

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 302
    :cond_a
    check-cast v0, Ljava/util/List;

    .line 186
    check-cast v0, Ljava/lang/Iterable;

    .line 303
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/Collection;

    .line 304
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_b
    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/squareup/orders/model/Order;

    .line 187
    invoke-static {v5}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object v5

    iget-object v5, v5, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    if-eqz v5, :cond_c

    const/4 v5, 0x1

    goto :goto_7

    :cond_c
    const/4 v5, 0x0

    :goto_7
    if-eqz v5, :cond_b

    invoke-interface {p1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 305
    :cond_d
    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 306
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v3, Ljava/util/Map;

    .line 307
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_8
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 308
    move-object v5, v4

    check-cast v5, Lcom/squareup/orders/model/Order;

    .line 188
    invoke-static {v5}, Lcom/squareup/ui/orderhub/master/FilterKt;->getTypeFilter(Lcom/squareup/orders/model/Order;)Lcom/squareup/ui/orderhub/master/Filter$Type;

    move-result-object v5

    .line 310
    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_e

    .line 309
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 313
    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    :cond_e
    check-cast v6, Ljava/util/List;

    .line 317
    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 189
    :cond_f
    invoke-static {v3, p2}, Lkotlin/collections/MapsKt;->toSortedMap(Ljava/util/Map;Ljava/util/Comparator;)Ljava/util/SortedMap;

    move-result-object p1

    .line 320
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 321
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_10
    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_13

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/squareup/orders/model/Order;

    .line 191
    invoke-static {v5}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getChannel(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Channel;

    move-result-object v5

    if-eqz v5, :cond_11

    iget-object v5, v5, Lcom/squareup/protos/client/orders/Channel;->display_name:Ljava/lang/String;

    goto :goto_a

    :cond_11
    const/4 v5, 0x0

    :goto_a
    if-eqz v5, :cond_12

    const/4 v5, 0x1

    goto :goto_b

    :cond_12
    const/4 v5, 0x0

    :goto_b
    if-eqz v5, :cond_10

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 322
    :cond_13
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 323
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 324
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_15

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 325
    move-object v3, v2

    check-cast v3, Lcom/squareup/orders/model/Order;

    .line 192
    invoke-static {v3}, Lcom/squareup/ui/orderhub/master/FilterKt;->getSourceFilter(Lcom/squareup/orders/model/Order;)Lcom/squareup/ui/orderhub/master/Filter$Source;

    move-result-object v3

    .line 327
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_14

    .line 326
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 330
    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    :cond_14
    check-cast v4, Ljava/util/List;

    .line 334
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 193
    :cond_15
    invoke-static {v0, p2}, Lkotlin/collections/MapsKt;->toSortedMap(Ljava/util/Map;Ljava/util/Comparator;)Ljava/util/SortedMap;

    move-result-object p2

    .line 195
    check-cast p3, Ljava/util/Map;

    check-cast p1, Ljava/util/Map;

    invoke-static {p3, p1}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    check-cast p2, Ljava/util/Map;

    invoke-static {p1, p2}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    :goto_d
    return-object p1
.end method
