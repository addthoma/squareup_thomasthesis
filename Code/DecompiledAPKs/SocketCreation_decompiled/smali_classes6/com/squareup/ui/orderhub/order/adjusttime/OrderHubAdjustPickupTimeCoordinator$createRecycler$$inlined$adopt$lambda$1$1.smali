.class public final Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1;->invoke(ILcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$TimeRow;Lcom/squareup/noho/NohoCheckableRow;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 OrderHubAdjustPickupTimeCoordinator.kt\ncom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$1$1\n*L\n1#1,1322:1\n90#2,6:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0007"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release",
        "com/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$1$1$$special$$inlined$onClickDebounced$1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $timeRow$inlined:Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$TimeRow;

.field final synthetic this$0:Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1;Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$TimeRow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1$1;->this$0:Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1$1;->$timeRow$inlined:Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$TimeRow;

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 3

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1106
    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    .line 1323
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1$1;->this$0:Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->access$getScreen$p(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;)Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 1324
    new-instance v0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen$Event$AdjustPickupTime;

    .line 1325
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1$1;->$timeRow$inlined:Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$TimeRow;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$TimeRow;->getTime()Lorg/threeten/bp/ZonedDateTime;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/ZonedDateTime;->toOffsetDateTime()Lorg/threeten/bp/OffsetDateTime;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/OffsetDateTime;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "timeRow.time.toOffsetDateTime().toString()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1324
    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen$Event$AdjustPickupTime;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
