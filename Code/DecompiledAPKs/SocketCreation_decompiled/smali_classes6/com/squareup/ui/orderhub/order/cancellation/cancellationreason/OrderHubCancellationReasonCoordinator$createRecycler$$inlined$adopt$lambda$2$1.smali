.class public final Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;->invoke(ILcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$CancellationReasonRow;Lcom/squareup/noho/NohoCheckableRow;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 OrderHubCancellationReasonCoordinator.kt\ncom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$1$2\n*L\n1#1,1322:1\n126#2,5:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0007"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release",
        "com/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$1$2$$special$$inlined$onClickDebounced$1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $index$inlined:I

.field final synthetic $item$inlined:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$CancellationReasonRow;

.field final synthetic this$0:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$CancellationReasonRow;I)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2$1;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2$1;->$item$inlined:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$CancellationReasonRow;

    iput p3, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2$1;->$index$inlined:I

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1106
    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    .line 1323
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2$1;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->access$getCancellationReasonsList$p(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2$1;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;

    iget-object v0, v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->access$getCancellationReasonsList$p(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;)Lcom/squareup/cycler/Recycler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cycler/Recycler;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Lcom/squareup/cycler/Recycler;->refresh(II)V

    .line 1324
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2$1;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2$1;->$item$inlined:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$CancellationReasonRow;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$CancellationReasonRow;->getCancellationReason()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->access$setChosenReason$p(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;Lcom/squareup/ordermanagerdata/CancellationReason;)V

    .line 1325
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2$1;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->access$getCancellationReasonsList$p(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iget v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2$1;->$index$inlined:I

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Recycler;->refresh(I)V

    .line 1326
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2$1;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->access$updateActionBar(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;)V

    return-void
.end method
