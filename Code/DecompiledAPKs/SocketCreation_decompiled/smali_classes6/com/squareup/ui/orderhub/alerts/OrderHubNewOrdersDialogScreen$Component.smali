.class public interface abstract Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen$Component;
.super Ljava/lang/Object;
.source "OrderHubNewOrdersDialogScreen.kt"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0005H&J\u0008\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen$Component;",
        "",
        "orderHubAnalytics",
        "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
        "orderHubApplet",
        "Lcom/squareup/ui/orderhub/OrderHubApplet;",
        "orderNotificationAudioPlayer",
        "Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract orderHubAnalytics()Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;
.end method

.method public abstract orderHubApplet()Lcom/squareup/ui/orderhub/OrderHubApplet;
.end method

.method public abstract orderNotificationAudioPlayer()Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;
.end method
