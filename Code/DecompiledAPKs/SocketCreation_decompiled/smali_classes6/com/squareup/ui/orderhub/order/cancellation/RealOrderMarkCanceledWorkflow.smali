.class public final Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealOrderMarkCanceledWorkflow.kt"

# interfaces
.implements Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledWorkflow;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/orderhub/OrderHubAppletScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderMarkCanceledWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderMarkCanceledWorkflow.kt\ncom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 5 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,390:1\n85#2:391\n85#2:394\n85#2:397\n240#3:392\n240#3:395\n240#3:398\n276#4:393\n276#4:396\n276#4:399\n149#5,5:400\n149#5,5:405\n149#5,5:410\n*E\n*S KotlinDebug\n*F\n+ 1 RealOrderMarkCanceledWorkflow.kt\ncom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow\n*L\n131#1:391\n163#1:394\n195#1:397\n131#1:392\n163#1:395\n195#1:398\n131#1:393\n163#1:396\n195#1:399\n300#1,5:400\n307#1,5:405\n314#1,5:410\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0008\u0007\u0018\u0000 (2\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002:\u0001(B?\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u00a2\u0006\u0002\u0010\u0019JF\u0010\u001a\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u001b\u001a\u00020\u00042\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001dH\u0002JF\u0010\u001e\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u001b\u001a\u00020\u00042\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001dH\u0002J\u001a\u0010\u001f\u001a\u00020\u00042\u0006\u0010 \u001a\u00020\u00032\u0008\u0010!\u001a\u0004\u0018\u00010\"H\u0016J \u0010#\u001a\u00020\u00042\u0006\u0010$\u001a\u00020\u00032\u0006\u0010%\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u0004H\u0016JN\u0010&\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010 \u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u00042\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001dH\u0016J\u0010\u0010\'\u001a\u00020\"2\u0006\u0010\u001b\u001a\u00020\u0004H\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "orderCancellationReasonWorkflow",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonWorkflow;",
        "orderItemSelectionWorkflow",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;",
        "orderHubAnalytics",
        "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
        "orderRepository",
        "Lcom/squareup/ordermanagerdata/OrderRepository;",
        "orderHubInventoryService",
        "Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;",
        "orderHubBillLoader",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;",
        "uuidGenerator",
        "Lcom/squareup/log/UUIDGenerator;",
        "(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonWorkflow;Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;Lcom/squareup/log/UUIDGenerator;)V",
        "getCancellationReasonChildRendering",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "getItemSelectionChildRendering",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "onPropsChanged",
        "old",
        "new",
        "render",
        "snapshotState",
        "Companion",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final BILL_RETRIEVAL_WORKER_KEY:Ljava/lang/String; = "billHistory-retrieval-order"

.field public static final CANCEL_ORDER_WORKER_KEY:Ljava/lang/String; = "cancel-order"

.field public static final Companion:Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final INVENTORY_ADJUSTMENT_WORKER_KEY:Ljava/lang/String; = "inventory-adjustment-order"


# instance fields
.field private final orderCancellationReasonWorkflow:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonWorkflow;

.field private final orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

.field private final orderHubBillLoader:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

.field private final orderHubInventoryService:Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;

.field private final orderItemSelectionWorkflow:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;

.field private final orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

.field private final uuidGenerator:Lcom/squareup/log/UUIDGenerator;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->Companion:Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonWorkflow;Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;Lcom/squareup/log/UUIDGenerator;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "orderCancellationReasonWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderItemSelectionWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubAnalytics"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderRepository"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubInventoryService"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubBillLoader"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uuidGenerator"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->orderCancellationReasonWorkflow:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->orderItemSelectionWorkflow:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    iput-object p5, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->orderHubInventoryService:Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;

    iput-object p6, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->orderHubBillLoader:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    iput-object p7, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->uuidGenerator:Lcom/squareup/log/UUIDGenerator;

    return-void
.end method

.method public static final synthetic access$getOrderHubAnalytics$p(Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;)Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;
    .locals 0

    .line 83
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getUuidGenerator$p(Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;)Lcom/squareup/log/UUIDGenerator;
    .locals 0

    .line 83
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->uuidGenerator:Lcom/squareup/log/UUIDGenerator;

    return-object p0
.end method

.method private final getCancellationReasonChildRendering(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
            "-",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 323
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getCancelAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v3, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;

    invoke-direct {v3, v0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;-><init>(Lcom/squareup/protos/client/orders/Action;)V

    .line 325
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->orderCancellationReasonWorkflow:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonWorkflow;

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    const/4 v4, 0x0

    .line 327
    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$getCancellationReasonChildRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$getCancellationReasonChildRendering$1;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)V

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p2

    .line 324
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    return-object p1

    .line 323
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Required value was null."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final getItemSelectionChildRendering(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
            "-",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 355
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getSelectedLineItems()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v1, "Required value was null."

    if-eqz v0, :cond_3

    .line 357
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v3

    .line 358
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getCancelAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v4

    if-eqz v4, :cond_2

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    .line 356
    new-instance v0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Ljava/util/Map;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v6, v0

    goto :goto_2

    .line 358
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 362
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v0

    .line 363
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getCancelAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 364
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getSelectedLineItems()Ljava/util/Map;

    move-result-object v1

    .line 361
    new-instance v3, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;

    invoke-direct {v3, v0, v2, v1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Ljava/util/Map;)V

    move-object v6, v3

    .line 368
    :goto_2
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->orderItemSelectionWorkflow:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;

    move-object v5, v0

    check-cast v5, Lcom/squareup/workflow/Workflow;

    const/4 v7, 0x0

    .line 370
    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$getItemSelectionChildRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$getItemSelectionChildRendering$1;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)V

    move-object v8, v0

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const/4 v9, 0x4

    const/4 v10, 0x0

    move-object v4, p2

    .line 367
    invoke-static/range {v4 .. v10}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    return-object p1

    .line 363
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;
    .locals 0

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-static {p1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInputKt;->getInitialState(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 83
    check-cast p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->initialState(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    move-result-object p1

    return-object p1
.end method

.method public onPropsChanged(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;
    .locals 1

    const-string v0, "old"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "new"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 112
    :cond_0
    invoke-static {p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInputKt;->getInitialState(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    move-result-object p3

    :goto_0
    return-object p3
.end method

.method public bridge synthetic onPropsChanged(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 83
    check-cast p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;

    check-cast p2, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;

    check-cast p3, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->onPropsChanged(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 83
    check-cast p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;

    check-cast p2, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->render(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
            "-",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    const-string v3, "props"

    move-object/from16 v4, p1

    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "state"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "context"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getNextMarkCanceledAction()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    move-result-object v3

    sget-object v4, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->MARK_CANCELED:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    const-string v5, "Required value was null."

    const-string v6, ".order.version"

    const-string v7, ".order.id "

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    if-ne v3, v4, :cond_2

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    move-result-object v3

    if-nez v3, :cond_2

    .line 123
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getCancellationReason()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v3

    if-eqz v3, :cond_0

    const/4 v9, 0x1

    :cond_0
    invoke-static {v9}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 125
    iget-object v3, v0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v4

    iget-object v4, v4, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    const-string v9, "state.order.id"

    invoke-static {v4, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v9, Lcom/squareup/protos/client/orders/Action$Type;->CANCEL:Lcom/squareup/protos/client/orders/Action$Type;

    invoke-virtual {v3, v4, v9}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logAction$orderhub_applet_release(Ljava/lang/String;Lcom/squareup/protos/client/orders/Action$Type;)V

    .line 127
    iget-object v3, v0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    .line 128
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v4

    .line 129
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getCancellationReason()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 130
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v5

    invoke-static/range {p2 .. p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledStateKt;->getNonEmptySelectedLineItems(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)Ljava/util/Map;

    move-result-object v11

    invoke-static {v5, v11}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelectionsKt;->selectedLineItemQuantities(Lcom/squareup/orders/model/Order;Ljava/util/Map;)Ljava/util/List;

    move-result-object v5

    .line 127
    invoke-interface {v3, v4, v9, v5}, Lcom/squareup/ordermanagerdata/OrderRepository;->cancelOrder(Lcom/squareup/orders/model/Order;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/List;)Lio/reactivex/Single;

    move-result-object v3

    .line 391
    sget-object v4, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v4, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$$inlined$asWorker$1;

    invoke-direct {v4, v3, v8}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 392
    invoke-static {v4}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v3

    .line 393
    const-class v4, Lcom/squareup/ordermanagerdata/ResultState;

    sget-object v5, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v8, Lcom/squareup/orders/model/Order;

    invoke-static {v8}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v8

    invoke-virtual {v5, v8}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v5

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v4

    new-instance v5, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v5, v4, v3}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v5, Lcom/squareup/workflow/Worker;

    .line 132
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cancel-order "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 133
    new-instance v4, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$1;

    invoke-direct {v4, v1}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$1;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 126
    invoke-interface {v2, v5, v3, v4}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_3

    .line 129
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 152
    :cond_2
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getNextMarkCanceledAction()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    move-result-object v3

    sget-object v4, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->ISSUE_INVENTORY_ADJUSTMENT:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    if-ne v3, v4, :cond_7

    .line 153
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getIdempotencyKey()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    :goto_0
    invoke-static {v3}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 154
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    move-result-object v3

    if-nez v3, :cond_4

    const/4 v3, 0x1

    goto :goto_1

    :cond_4
    const/4 v3, 0x0

    :goto_1
    invoke-static {v3}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 157
    iget-object v11, v0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->orderHubInventoryService:Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;

    .line 158
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v12

    .line 159
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getBillHistory()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v13

    .line 160
    invoke-static/range {p2 .. p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledStateKt;->getNonEmptySelectedLineItems(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)Ljava/util/Map;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelectionsKt;->asLineItemQuantitiesByUid(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v14

    .line 161
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getCancellationReason()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v3

    sget-object v4, Lcom/squareup/ordermanagerdata/CancellationReason;->NOT_IN_STOCK:Lcom/squareup/ordermanagerdata/CancellationReason;

    if-eq v3, v4, :cond_5

    const/4 v15, 0x1

    goto :goto_2

    :cond_5
    const/4 v15, 0x0

    .line 162
    :goto_2
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getIdempotencyKey()Ljava/lang/String;

    move-result-object v16

    if-eqz v16, :cond_6

    .line 157
    invoke-virtual/range {v11 .. v16}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->issueInventoryAdjustmentRequest(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;ZLjava/lang/String;)Lio/reactivex/Single;

    move-result-object v3

    .line 394
    sget-object v4, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v4, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$$inlined$asWorker$2;

    invoke-direct {v4, v3, v8}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$$inlined$asWorker$2;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 395
    invoke-static {v4}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v3

    .line 396
    const-class v4, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    sget-object v5, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v8, Lcom/squareup/protos/client/BatchAdjustVariationInventoryResponse;

    invoke-static {v8}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v8

    invoke-virtual {v5, v8}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v5

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v4

    new-instance v5, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v5, v4, v3}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v5, Lcom/squareup/workflow/Worker;

    .line 164
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "inventory-adjustment-order "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 165
    new-instance v4, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$2;

    invoke-direct {v4, v1}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$2;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 156
    invoke-interface {v2, v5, v3, v4}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    goto :goto_3

    .line 162
    :cond_6
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 190
    :cond_7
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getNextMarkCanceledAction()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    move-result-object v3

    sget-object v4, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->RETRIEVE_BILL_STATE:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    if-ne v3, v4, :cond_9

    .line 191
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    move-result-object v3

    if-nez v3, :cond_8

    const/4 v9, 0x1

    :cond_8
    invoke-static {v9}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 193
    iget-object v3, v0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->orderHubBillLoader:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->clear$orderhub_applet_release()V

    .line 195
    iget-object v3, v0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->orderHubBillLoader:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getBillServerToken(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->loadFromToken$orderhub_applet_release(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v3

    .line 397
    sget-object v4, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v4, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$$inlined$asWorker$3;

    invoke-direct {v4, v3, v8}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$$inlined$asWorker$3;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 398
    invoke-static {v4}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v3

    .line 399
    const-class v4, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v4

    new-instance v5, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v5, v4, v3}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v5, Lcom/squareup/workflow/Worker;

    .line 196
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "billHistory-retrieval-order "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 197
    new-instance v4, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$3;

    invoke-direct {v4, v1}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$3;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 194
    invoke-interface {v2, v5, v3, v4}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 254
    :cond_9
    :goto_3
    new-instance v3, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$cancellationProcessingEventHandler$1;

    invoke-direct {v3, v0, v1}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$cancellationProcessingEventHandler$1;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-interface {v2, v3}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v3

    .line 284
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getNextMarkCanceledAction()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    move-result-object v4

    sget-object v5, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v4}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->ordinal()I

    move-result v4

    aget v4, v5, v4

    if-eq v4, v10, :cond_d

    const/4 v5, 0x2

    if-eq v4, v5, :cond_c

    const/4 v1, 0x3

    const-string v2, ""

    if-eq v4, v1, :cond_b

    const/4 v1, 0x4

    if-eq v4, v1, :cond_a

    .line 310
    sget-object v5, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 311
    new-instance v1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;

    .line 312
    sget-object v4, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData$ShowingCancellationSpinner;->INSTANCE:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData$ShowingCancellationSpinner;

    check-cast v4, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;

    .line 311
    invoke-direct {v1, v4, v3}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 411
    new-instance v6, Lcom/squareup/workflow/legacy/Screen;

    .line 412
    const-class v3, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 413
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 411
    invoke-direct {v6, v2, v1, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x1e

    const/4 v12, 0x0

    .line 310
    invoke-static/range {v5 .. v12}, Lcom/squareup/container/PosLayering$Companion;->fullStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_4

    .line 303
    :cond_a
    sget-object v1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 304
    new-instance v4, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;

    .line 305
    sget-object v5, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData$CancellationFailed;->INSTANCE:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData$CancellationFailed;

    check-cast v5, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;

    .line 304
    invoke-direct {v4, v5, v3}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;Lkotlin/jvm/functions/Function1;)V

    check-cast v4, Lcom/squareup/workflow/legacy/V2Screen;

    .line 406
    new-instance v3, Lcom/squareup/workflow/legacy/Screen;

    .line 407
    const-class v5, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;

    invoke-static {v5}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v5

    invoke-static {v5, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 408
    sget-object v5, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v5}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 406
    invoke-direct {v3, v2, v4, v5}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    move-object v2, v1

    .line 303
    invoke-static/range {v2 .. v9}, Lcom/squareup/container/PosLayering$Companion;->fullStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    goto :goto_4

    .line 296
    :cond_b
    sget-object v1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 297
    new-instance v4, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;

    .line 298
    sget-object v5, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData$CancellationCompleted;->INSTANCE:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData$CancellationCompleted;

    check-cast v5, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;

    .line 297
    invoke-direct {v4, v5, v3}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;Lkotlin/jvm/functions/Function1;)V

    check-cast v4, Lcom/squareup/workflow/legacy/V2Screen;

    .line 401
    new-instance v3, Lcom/squareup/workflow/legacy/Screen;

    .line 402
    const-class v5, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;

    invoke-static {v5}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v5

    invoke-static {v5, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 403
    sget-object v5, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v5}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 401
    invoke-direct {v3, v2, v4, v5}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    move-object v2, v1

    .line 296
    invoke-static/range {v2 .. v9}, Lcom/squareup/container/PosLayering$Companion;->fullStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    goto :goto_4

    .line 291
    :cond_c
    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->getCancellationReasonChildRendering(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object v1

    .line 292
    sget-object v2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v2}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/legacy/Screen;

    .line 293
    sget-object v2, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lcom/squareup/container/PosLayering$Companion;->fullStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    goto :goto_4

    .line 286
    :cond_d
    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->getItemSelectionChildRendering(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object v1

    .line 287
    sget-object v2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v2}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/legacy/Screen;

    .line 288
    sget-object v2, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lcom/squareup/container/PosLayering$Companion;->fullStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    :goto_4
    return-object v1
.end method

.method public snapshotState(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 83
    check-cast p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->snapshotState(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
