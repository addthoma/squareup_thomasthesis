.class public final Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;
.super Ljava/lang/Object;
.source "RealOrderNotificationAudioPlayer.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u0000 \t2\u00020\u0001:\u0001\tB\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0005\u001a\u00020\u0006J\u0006\u0010\u0007\u001a\u00020\u0006J\u0006\u0010\u0008\u001a\u00020\u0006R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;",
        "",
        "audioPlayer",
        "Lcom/squareup/brandaudio/BrandAudioPlayer;",
        "(Lcom/squareup/brandaudio/BrandAudioPlayer;)V",
        "initialize",
        "",
        "playOrderNotificationAudio",
        "shutDown",
        "Companion",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ORDER_NOTIFICATION:Ljava/lang/String; = "order_notification.mp3"


# instance fields
.field private final audioPlayer:Lcom/squareup/brandaudio/BrandAudioPlayer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;->Companion:Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/brandaudio/BrandAudioPlayer;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "audioPlayer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;->audioPlayer:Lcom/squareup/brandaudio/BrandAudioPlayer;

    return-void
.end method


# virtual methods
.method public final initialize()V
    .locals 2

    .line 17
    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;->audioPlayer:Lcom/squareup/brandaudio/BrandAudioPlayer;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/brandaudio/BrandAudioPlayer;->initialize(Z)V

    return-void
.end method

.method public final playOrderNotificationAudio()V
    .locals 5

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;->audioPlayer:Lcom/squareup/brandaudio/BrandAudioPlayer;

    const-string v1, "order_notification.mp3"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/brandaudio/BrandAudioPlayer$DefaultImpls;->playBrandAudioMessage$default(Lcom/squareup/brandaudio/BrandAudioPlayer;Ljava/lang/String;ZILjava/lang/Object;)V

    return-void
.end method

.method public final shutDown()V
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;->audioPlayer:Lcom/squareup/brandaudio/BrandAudioPlayer;

    invoke-interface {v0}, Lcom/squareup/brandaudio/BrandAudioPlayer;->shutDown()V

    return-void
.end method
