.class final Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow$render$eventHandler$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderCancellationReasonWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow;->render(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonResult;",
        "event",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow$render$eventHandler$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow$render$eventHandler$1;

    invoke-direct {v0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow$render$eventHandler$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow$render$eventHandler$1;->INSTANCE:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow$render$eventHandler$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen$Event$CancelItemsWithReason;

    if-eqz v0, :cond_0

    .line 73
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonResult$CancellationReasonComplete;

    check-cast p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen$Event$CancelItemsWithReason;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen$Event$CancelItemsWithReason;->getReason()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonResult$CancellationReasonComplete;-><init>(Lcom/squareup/ordermanagerdata/CancellationReason;)V

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 74
    :cond_0
    instance-of p1, p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen$Event$GoBackFromCancellationReason;

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonResult$GoBackFromCancellationSelectReason;->INSTANCE:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonResult$GoBackFromCancellationSelectReason;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow$render$eventHandler$1;->invoke(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
