.class final Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$5;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderDetailsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->render(Lcom/squareup/ui/orderhub/order/OrderDetailsInput;Lcom/squareup/ui/orderhub/order/OrderDetailsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        "it",
        "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$5;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$5;

    invoke-direct {v0}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$5;-><init>()V

    sput-object v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$5;->INSTANCE:Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$5;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$5;->invoke(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
