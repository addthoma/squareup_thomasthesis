.class final Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderMarkCanceledWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->render(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ordermanagerdata/ResultState<",
        "+",
        "Lcom/squareup/orders/model/Order;",
        ">;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
        "result",
        "Lcom/squareup/ordermanagerdata/ResultState;",
        "Lcom/squareup/orders/model/Order;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$1;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ordermanagerdata/ResultState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Lcom/squareup/orders/model/Order;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    instance-of v0, p1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 136
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$1;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    .line 137
    sget-object v6, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->SHOW_CANCELLATION_COMPLETED:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    const/4 v5, 0x0

    .line 138
    check-cast p1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/ResultState$Success;->getResponse()Ljava/lang/Object;

    move-result-object p1

    move-object v4, p1

    check-cast v4, Lcom/squareup/orders/model/Order;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0xda

    const/4 v13, 0x0

    .line 136
    invoke-static/range {v3 .. v13}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->copy$default(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    move-result-object p1

    .line 135
    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 142
    :cond_0
    instance-of v0, p1, Lcom/squareup/ordermanagerdata/ResultState$Failure$VersionMismatchError;

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledVersionMismatch;->INSTANCE:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledVersionMismatch;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 143
    :cond_1
    instance-of p1, p1, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    if-eqz p1, :cond_2

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 144
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$1;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 145
    sget-object v6, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->SHOW_CANCELLATION_FAILED:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0xdb

    const/4 v13, 0x0

    .line 144
    invoke-static/range {v3 .. v13}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->copy$default(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    move-result-object v0

    .line 143
    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 83
    check-cast p1, Lcom/squareup/ordermanagerdata/ResultState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$1;->invoke(Lcom/squareup/ordermanagerdata/ResultState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
