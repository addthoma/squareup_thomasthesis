.class public final Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner_Factory;
.super Ljava/lang/Object;
.source "OrderHubWorkflowRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/OrderHubApplet;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubRefundFlowStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/OrderHubApplet;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner_Factory;->orderHubRefundFlowStateProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner_Factory;->orderHubAppletProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/OrderHubApplet;",
            ">;)",
            "Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;Lcom/squareup/ui/orderhub/OrderHubApplet;)Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;-><init>(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;Lcom/squareup/ui/orderhub/OrderHubApplet;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/PosContainer;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner_Factory;->orderHubRefundFlowStateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;

    iget-object v2, p0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner_Factory;->orderHubAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/orderhub/OrderHubApplet;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner_Factory;->newInstance(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;Lcom/squareup/ui/orderhub/OrderHubApplet;)Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner_Factory;->get()Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;

    move-result-object v0

    return-object v0
.end method
