.class final Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$setUpActionBar$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderHubDetailCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->setUpActionBar(ZLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/applet/Applet;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "selectedApplet",
        "Lcom/squareup/applet/Applet;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $title:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$setUpActionBar$1;->this$0:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$setUpActionBar$1;->$title:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 100
    check-cast p1, Lcom/squareup/applet/Applet;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$setUpActionBar$1;->invoke(Lcom/squareup/applet/Applet;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/applet/Applet;)V
    .locals 4

    .line 665
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$setUpActionBar$1;->this$0:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->access$getActionBarNavigationHelper$p(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)Lcom/squareup/applet/ActionBarNavigationHelper;

    move-result-object v0

    .line 666
    iget-object v1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$setUpActionBar$1;->this$0:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->access$getActionBar$p(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)Lcom/squareup/marin/widgets/ActionBarView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    const-string v2, "actionBar.presenter"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$setUpActionBar$1;->$title:Ljava/lang/String;

    const-string v3, "selectedApplet"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 665
    invoke-interface {v0, v1, v2, p1}, Lcom/squareup/applet/ActionBarNavigationHelper;->makeActionBarForAppletActivationScreen(Lcom/squareup/marin/widgets/MarinActionBar;Ljava/lang/String;Lcom/squareup/applet/Applet;)V

    return-void
.end method
