.class public abstract Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope$BillHistoryModule;
.super Ljava/lang/Object;
.source "OrderHubBillHistoryScope.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BillHistoryModule"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007J\u0015\u0010\u0008\u001a\u00020\t2\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\nJ\u0015\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH!\u00a2\u0006\u0002\u0008\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope$BillHistoryModule;",
        "",
        "()V",
        "provideBillHistoryController",
        "Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Controller;",
        "scopeRunner",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;",
        "provideBillHistoryController$orderhub_applet_release",
        "provideBillHistoryRunner",
        "Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;",
        "provideBillHistoryRunner$orderhub_applet_release",
        "provideTenderWithCustomerInfoCache",
        "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;",
        "cache",
        "Lcom/squareup/ui/activity/billhistory/RealTenderWithCustomerInfoCache;",
        "provideTenderWithCustomerInfoCache$orderhub_applet_release",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract provideBillHistoryController$orderhub_applet_release(Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;)Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Controller;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract provideBillHistoryRunner$orderhub_applet_release(Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;)Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract provideTenderWithCustomerInfoCache$orderhub_applet_release(Lcom/squareup/ui/activity/billhistory/RealTenderWithCustomerInfoCache;)Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
