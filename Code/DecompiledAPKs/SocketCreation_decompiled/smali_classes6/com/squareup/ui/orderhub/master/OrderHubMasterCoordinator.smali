.class public final Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "OrderHubMasterCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubMasterCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubMasterCoordinator.kt\ncom/squareup/ui/orderhub/master/OrderHubMasterCoordinator\n*L\n1#1,124:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\'BY\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\"\u0010\u000e\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00120\u0010j\u0008\u0012\u0004\u0012\u00020\u0011`\u00130\u000f\u00a2\u0006\u0002\u0010\u0014J\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u0018H\u0016J\u0010\u0010 \u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u0018H\u0002J\u0010\u0010!\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u0018H\u0016J\u0010\u0010\"\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020\u0011H\u0002J\u001a\u0010$\u001a\u00020\u001e*\u00020\u00162\u000c\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\u001e0&H\u0002R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R*\u0010\u000e\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00120\u0010j\u0008\u0012\u0004\u0012\u00020\u0011`\u00130\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "res",
        "Lcom/squareup/util/Res;",
        "badgePresenter",
        "Lcom/squareup/applet/BadgePresenter;",
        "orderHubBackButtonConfig",
        "Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;",
        "actionBarNavigationHelper",
        "Lcom/squareup/applet/ActionBarNavigationHelper;",
        "appletSelection",
        "Lcom/squareup/applet/AppletSelection;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lcom/squareup/util/Res;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/AppletSelection;Lio/reactivex/Scheduler;Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "mainView",
        "Landroid/view/View;",
        "recyclerAdapter",
        "Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "attach",
        "",
        "view",
        "bindViews",
        "detach",
        "updateViews",
        "screen",
        "setBackButtonConfig",
        "upButtonClickListener",
        "Lkotlin/Function0;",
        "Factory",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private final actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

.field private final appletSelection:Lcom/squareup/applet/AppletSelection;

.field private final badgePresenter:Lcom/squareup/applet/BadgePresenter;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private mainView:Landroid/view/View;

.field private final orderHubBackButtonConfig:Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;

.field private recyclerAdapter:Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final res:Lcom/squareup/util/Res;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/AppletSelection;Lio/reactivex/Scheduler;Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/applet/BadgePresenter;",
            "Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            "Lcom/squareup/applet/AppletSelection;",
            "Lio/reactivex/Scheduler;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "badgePresenter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubBackButtonConfig"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "actionBarNavigationHelper"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appletSelection"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->orderHubBackButtonConfig:Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

    iput-object p5, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->appletSelection:Lcom/squareup/applet/AppletSelection;

    iput-object p6, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p7, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getActionBar$p(Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;)Lcom/squareup/marin/widgets/ActionBarView;
    .locals 1

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez p0, :cond_0

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getActionBarNavigationHelper$p(Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;)Lcom/squareup/applet/ActionBarNavigationHelper;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;)Lcom/squareup/util/Res;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$setActionBar$p(Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;Lcom/squareup/marin/widgets/ActionBarView;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    return-void
.end method

.method public static final synthetic access$updateViews(Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->updateViews(Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 4

    .line 78
    iput-object p1, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->mainView:Landroid/view/View;

    .line 79
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(com.sq\u2026s.R.id.stable_action_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v1, :cond_0

    const-string v2, "actionBar"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, v1}, Lcom/squareup/applet/BadgePresenter;->takeView(Ljava/lang/Object;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->appletSelection:Lcom/squareup/applet/AppletSelection;

    .line 82
    invoke-interface {v0}, Lcom/squareup/applet/AppletSelection;->selectedApplet()Lio/reactivex/Observable;

    move-result-object v0

    .line 83
    iget-object v1, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "appletSelection\n        \u2026.observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    new-instance v1, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$bindViews$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$bindViews$1;-><init>(Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 91
    new-instance v0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->res:Lcom/squareup/util/Res;

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;-><init>(Lcom/squareup/util/Res;)V

    iput-object v0, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->recyclerAdapter:Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;

    .line 92
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_filter_recycler_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 93
    move-object v0, p1

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    .line 94
    new-instance v1, Lcom/squareup/noho/NohoEdgeDecoration;

    iget-object v2, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->mainView:Landroid/view/View;

    if-nez v2, :cond_1

    const-string v3, "mainView"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "mainView.resources"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/squareup/noho/NohoEdgeDecoration;-><init>(Landroid/content/res/Resources;)V

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 95
    iget-object v1, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->recyclerAdapter:Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;

    const-string v2, "recyclerAdapter"

    if-nez v1, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$Adapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 96
    new-instance v1, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;

    iget-object v3, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->recyclerAdapter:Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;

    if-nez v3, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v3, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;

    invoke-direct {v1, v3}, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;-><init>(Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;)V

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    const-string v1, "view.findViewById<Recycl\u2026cyclerAdapter))\n        }"

    .line 93
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method

.method private final setBackButtonConfig(Lcom/squareup/marin/widgets/ActionBarView;Lkotlin/jvm/functions/Function0;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/marin/widgets/ActionBarView;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->orderHubBackButtonConfig:Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;

    invoke-interface {v0}, Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;->getShowBackButton()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    .line 113
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 115
    iget-object v1, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->orderHubBackButtonConfig:Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;

    invoke-interface {v1}, Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    .line 116
    iget-object v2, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderhub/applet/R$string;->orderhub_applet_name:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 114
    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    if-eqz p2, :cond_0

    .line 118
    new-instance v1, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$sam$i$java_lang_Runnable$0;

    invoke-direct {v1, p2}, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$sam$i$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    move-object p2, v1

    :cond_0
    check-cast p2, Ljava/lang/Runnable;

    invoke-virtual {v0, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    .line 119
    invoke-virtual {p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    :cond_1
    return-void
.end method

.method private final updateViews(Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;)V
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->recyclerAdapter:Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;

    if-nez v0, :cond_0

    const-string v1, "recyclerAdapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;->setData(Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->mainView:Landroid/view/View;

    if-nez v0, :cond_1

    const-string v1, "mainView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$updateViews$1;

    invoke-direct {v1, p1}, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$updateViews$1;-><init>(Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_2

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v1, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$updateViews$2;

    invoke-direct {v1, p1}, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$updateViews$2;-><init>(Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->setBackButtonConfig(Lcom/squareup/marin/widgets/ActionBarView;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->bindViews(Landroid/view/View;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->screens:Lio/reactivex/Observable;

    .line 66
    iget-object v1, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 67
    sget-object v1, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$attach$1;->INSTANCE:Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$attach$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screens\n        .observe\u2026map { it.unwrapV2Screen }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    new-instance v1, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$attach$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$attach$2;-><init>(Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    iget-object p1, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Lcom/squareup/marin/widgets/Badgeable;

    invoke-virtual {p1, v0}, Lcom/squareup/applet/BadgePresenter;->dropView(Lcom/squareup/marin/widgets/Badgeable;)V

    return-void
.end method
