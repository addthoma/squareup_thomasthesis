.class public final Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;
.super Ljava/lang/Object;
.source "OrderMarkCanceledState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u001c\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 62\u00020\u0001:\u00016B\u0081\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00120\u0008\u0002\u0010\u0008\u001a*\u0012\u0004\u0012\u00020\n\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\u000c0\tj\u0002`\r\u0018\u00010\tj\u0004\u0018\u0001`\u000e\u0012\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0010\u0012\n\u0008\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0012\u0012\n\u0008\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0014\u0012\n\u0008\u0002\u0010\u0015\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0002\u0010\u0016J\t\u0010\'\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010(\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\t\u0010)\u001a\u00020\u0007H\u00c6\u0003J1\u0010*\u001a*\u0012\u0004\u0012\u00020\n\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\u000c0\tj\u0002`\r\u0018\u00010\tj\u0004\u0018\u0001`\u000eH\u00c6\u0003J\u000b\u0010+\u001a\u0004\u0018\u00010\u0010H\u00c6\u0003J\u000b\u0010,\u001a\u0004\u0018\u00010\u0012H\u00c6\u0003J\u000b\u0010-\u001a\u0004\u0018\u00010\u0014H\u00c6\u0003J\u000b\u0010.\u001a\u0004\u0018\u00010\nH\u00c6\u0003J\u008b\u0001\u0010/\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u000720\u0008\u0002\u0010\u0008\u001a*\u0012\u0004\u0012\u00020\n\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\u000c0\tj\u0002`\r\u0018\u00010\tj\u0004\u0018\u0001`\u000e2\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00102\n\u0008\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00122\n\u0008\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00142\n\u0008\u0002\u0010\u0015\u001a\u0004\u0018\u00010\nH\u00c6\u0001J\u0013\u00100\u001a\u0002012\u0008\u00102\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u00103\u001a\u000204H\u00d6\u0001J\t\u00105\u001a\u00020\nH\u00d6\u0001R\u0013\u0010\u0013\u001a\u0004\u0018\u00010\u0014\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u0013\u0010\u0015\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010 R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\"R\u0013\u0010\u0011\u001a\u0004\u0018\u00010\u0012\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010$R9\u0010\u0008\u001a*\u0012\u0004\u0012\u00020\n\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\u000c0\tj\u0002`\r\u0018\u00010\tj\u0004\u0018\u0001`\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010&\u00a8\u00067"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
        "",
        "order",
        "Lcom/squareup/orders/model/Order;",
        "cancelAction",
        "Lcom/squareup/protos/client/orders/Action;",
        "nextMarkCanceledAction",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;",
        "selectedLineItems",
        "",
        "",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemRowIdentifier;",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemWithQuantityByIdentifier;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemSelectionsByUid;",
        "cancellationReason",
        "Lcom/squareup/ordermanagerdata/CancellationReason;",
        "orderUpdateFailureState",
        "Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;",
        "billHistory",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "idempotencyKey",
        "(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;)V",
        "getBillHistory",
        "()Lcom/squareup/billhistory/model/BillHistory;",
        "getCancelAction",
        "()Lcom/squareup/protos/client/orders/Action;",
        "getCancellationReason",
        "()Lcom/squareup/ordermanagerdata/CancellationReason;",
        "getIdempotencyKey",
        "()Ljava/lang/String;",
        "getNextMarkCanceledAction",
        "()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;",
        "getOrder",
        "()Lcom/squareup/orders/model/Order;",
        "getOrderUpdateFailureState",
        "()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;",
        "getSelectedLineItems",
        "()Ljava/util/Map;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "Companion",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CANCELLATION_REASON_NULL_ERROR:Ljava/lang/String; = "At this point the Order should have selected a Cancellation Reason"

.field public static final Companion:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion;


# instance fields
.field private final billHistory:Lcom/squareup/billhistory/model/BillHistory;

.field private final cancelAction:Lcom/squareup/protos/client/orders/Action;

.field private final cancellationReason:Lcom/squareup/ordermanagerdata/CancellationReason;

.field private final idempotencyKey:Ljava/lang/String;

.field private final nextMarkCanceledAction:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

.field private final order:Lcom/squareup/orders/model/Order;

.field private final orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

.field private final selectedLineItems:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->Companion:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Lcom/squareup/protos/client/orders/Action;",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;",
            "Lcom/squareup/ordermanagerdata/CancellationReason;",
            "Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nextMarkCanceledAction"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->order:Lcom/squareup/orders/model/Order;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->cancelAction:Lcom/squareup/protos/client/orders/Action;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->nextMarkCanceledAction:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->selectedLineItems:Ljava/util/Map;

    iput-object p5, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->cancellationReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    iput-object p6, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    iput-object p7, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->billHistory:Lcom/squareup/billhistory/model/BillHistory;

    iput-object p8, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->idempotencyKey:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 12

    move/from16 v0, p9

    and-int/lit8 v1, v0, 0x8

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 19
    move-object v1, v2

    check-cast v1, Ljava/util/Map;

    move-object v7, v1

    goto :goto_0

    :cond_0
    move-object/from16 v7, p4

    :goto_0
    and-int/lit8 v1, v0, 0x10

    if-eqz v1, :cond_1

    .line 20
    move-object v1, v2

    check-cast v1, Lcom/squareup/ordermanagerdata/CancellationReason;

    move-object v8, v1

    goto :goto_1

    :cond_1
    move-object/from16 v8, p5

    :goto_1
    and-int/lit8 v1, v0, 0x20

    if-eqz v1, :cond_2

    .line 21
    move-object v1, v2

    check-cast v1, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    move-object v9, v1

    goto :goto_2

    :cond_2
    move-object/from16 v9, p6

    :goto_2
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_3

    .line 22
    move-object v1, v2

    check-cast v1, Lcom/squareup/billhistory/model/BillHistory;

    move-object v10, v1

    goto :goto_3

    :cond_3
    move-object/from16 v10, p7

    :goto_3
    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_4

    .line 23
    move-object v0, v2

    check-cast v0, Ljava/lang/String;

    move-object v11, v0

    goto :goto_4

    :cond_4
    move-object/from16 v11, p8

    :goto_4
    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v3 .. v11}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->order:Lcom/squareup/orders/model/Order;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->cancelAction:Lcom/squareup/protos/client/orders/Action;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->nextMarkCanceledAction:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->selectedLineItems:Ljava/util/Map;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->cancellationReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    goto :goto_5

    :cond_5
    move-object v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->billHistory:Lcom/squareup/billhistory/model/BillHistory;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->idempotencyKey:Ljava/lang/String;

    goto :goto_7

    :cond_7
    move-object/from16 v1, p8

    :goto_7
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move-object p5, v6

    move-object p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->copy(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/squareup/orders/model/Order;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->order:Lcom/squareup/orders/model/Order;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/client/orders/Action;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->cancelAction:Lcom/squareup/protos/client/orders/Action;

    return-object v0
.end method

.method public final component3()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->nextMarkCanceledAction:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    return-object v0
.end method

.method public final component4()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->selectedLineItems:Ljava/util/Map;

    return-object v0
.end method

.method public final component5()Lcom/squareup/ordermanagerdata/CancellationReason;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->cancellationReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    return-object v0
.end method

.method public final component6()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    return-object v0
.end method

.method public final component7()Lcom/squareup/billhistory/model/BillHistory;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->billHistory:Lcom/squareup/billhistory/model/BillHistory;

    return-object v0
.end method

.method public final component8()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->idempotencyKey:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Lcom/squareup/protos/client/orders/Action;",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;",
            "Lcom/squareup/ordermanagerdata/CancellationReason;",
            "Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;"
        }
    .end annotation

    const-string v0, "order"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nextMarkCanceledAction"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    move-object v1, v0

    move-object v3, p2

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->order:Lcom/squareup/orders/model/Order;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->order:Lcom/squareup/orders/model/Order;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->cancelAction:Lcom/squareup/protos/client/orders/Action;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->cancelAction:Lcom/squareup/protos/client/orders/Action;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->nextMarkCanceledAction:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->nextMarkCanceledAction:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->selectedLineItems:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->selectedLineItems:Ljava/util/Map;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->cancellationReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->cancellationReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->billHistory:Lcom/squareup/billhistory/model/BillHistory;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->billHistory:Lcom/squareup/billhistory/model/BillHistory;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->idempotencyKey:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->idempotencyKey:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBillHistory()Lcom/squareup/billhistory/model/BillHistory;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->billHistory:Lcom/squareup/billhistory/model/BillHistory;

    return-object v0
.end method

.method public final getCancelAction()Lcom/squareup/protos/client/orders/Action;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->cancelAction:Lcom/squareup/protos/client/orders/Action;

    return-object v0
.end method

.method public final getCancellationReason()Lcom/squareup/ordermanagerdata/CancellationReason;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->cancellationReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    return-object v0
.end method

.method public final getIdempotencyKey()Ljava/lang/String;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->idempotencyKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getNextMarkCanceledAction()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->nextMarkCanceledAction:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    return-object v0
.end method

.method public final getOrder()Lcom/squareup/orders/model/Order;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->order:Lcom/squareup/orders/model/Order;

    return-object v0
.end method

.method public final getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    return-object v0
.end method

.method public final getSelectedLineItems()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->selectedLineItems:Ljava/util/Map;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->order:Lcom/squareup/orders/model/Order;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->cancelAction:Lcom/squareup/protos/client/orders/Action;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->nextMarkCanceledAction:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->selectedLineItems:Ljava/util/Map;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->cancellationReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->billHistory:Lcom/squareup/billhistory/model/BillHistory;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->idempotencyKey:Ljava/lang/String;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_7
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OrderMarkCanceledState(order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->order:Lcom/squareup/orders/model/Order;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cancelAction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->cancelAction:Lcom/squareup/protos/client/orders/Action;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", nextMarkCanceledAction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->nextMarkCanceledAction:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedLineItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->selectedLineItems:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cancellationReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->cancellationReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", orderUpdateFailureState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", billHistory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->billHistory:Lcom/squareup/billhistory/model/BillHistory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", idempotencyKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->idempotencyKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
