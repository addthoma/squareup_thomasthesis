.class public final Lcom/squareup/ui/orderhub/util/proto/KnownChannel$Companion;
.super Ljava/lang/Object;
.source "Channels.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/util/proto/KnownChannel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\tR\u001a\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/util/proto/KnownChannel$Companion;",
        "",
        "()V",
        "lookup",
        "",
        "",
        "Lcom/squareup/ui/orderhub/util/proto/KnownChannel;",
        "fromProtoChannel",
        "channel",
        "Lcom/squareup/protos/client/orders/Channel;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/util/proto/KnownChannel$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromProtoChannel(Lcom/squareup/protos/client/orders/Channel;)Lcom/squareup/ui/orderhub/util/proto/KnownChannel;
    .locals 2

    const-string v0, "channel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-static {}, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->access$getLookup$cp()Ljava/util/Map;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/client/orders/Channel;->display_name:Ljava/lang/String;

    const-string v1, "channel.display_name"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/ChannelsKt;->access$getNormalized$p(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->OTHER:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    :goto_0
    return-object p1
.end method
