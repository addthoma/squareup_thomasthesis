.class final Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Companion;
.super Ljava/lang/Object;
.source "OrderHubTrackingRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0082\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0017\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u000e\u0010\u0008\u001a\u00020\tX\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\tX\u0086T\u00a2\u0006\u0002\n\u0000R\u0017\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0007R\u0017\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Companion;",
        "",
        "()V",
        "CARRIERS_LIST",
        "",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier;",
        "getCARRIERS_LIST",
        "()Ljava/util/List;",
        "NO_CARRIER_CHOSEN_POSITION",
        "",
        "OTHER_CARRIER_NAME",
        "",
        "OTHER_CARRIER_POSITION",
        "TITLE_ROW",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType$TitleRow;",
        "getTITLE_ROW",
        "TITLE_ROW_DATA_SOURCE",
        "Lcom/squareup/cycler/DataSource;",
        "getTITLE_ROW_DATA_SOURCE",
        "()Lcom/squareup/cycler/DataSource;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 316
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 316
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCARRIERS_LIST()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier;",
            ">;"
        }
    .end annotation

    .line 323
    invoke-static {}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->access$getCARRIERS_LIST$cp()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getTITLE_ROW()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType$TitleRow;",
            ">;"
        }
    .end annotation

    .line 321
    invoke-static {}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->access$getTITLE_ROW$cp()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getTITLE_ROW_DATA_SOURCE()Lcom/squareup/cycler/DataSource;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/DataSource<",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType$TitleRow;",
            ">;"
        }
    .end annotation

    .line 322
    invoke-static {}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->access$getTITLE_ROW_DATA_SOURCE$cp()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    return-object v0
.end method
