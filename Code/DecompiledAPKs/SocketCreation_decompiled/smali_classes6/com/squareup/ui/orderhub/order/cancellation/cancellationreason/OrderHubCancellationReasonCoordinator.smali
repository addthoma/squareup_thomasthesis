.class public final Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "OrderHubCancellationReasonCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Factory;,
        Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType;,
        Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubCancellationReasonCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubCancellationReasonCoordinator.kt\ncom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 5 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n+ 6 RecyclerNoho.kt\ncom/squareup/noho/dsl/RecyclerNohoKt\n+ 7 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n*L\n1#1,165:1\n49#2:166\n50#2,3:172\n53#2:206\n599#3,4:167\n601#3:171\n310#4,3:175\n313#4,3:184\n328#4:192\n342#4,5:193\n344#4,4:198\n329#4:202\n35#5,6:178\n114#6,5:187\n120#6:203\n43#7,2:204\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubCancellationReasonCoordinator.kt\ncom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator\n*L\n107#1:166\n107#1,3:172\n107#1:206\n107#1,4:167\n107#1:171\n107#1,3:175\n107#1,3:184\n107#1:192\n107#1,5:193\n107#1,4:198\n107#1:202\n107#1,6:178\n107#1,5:187\n107#1:203\n107#1,2:204\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0018\u0000 %2\u00020\u0001:\u0003$%&B;\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\"\u0010\u0006\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0008j\u0008\u0012\u0004\u0012\u00020\t`\u000b0\u0007\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0018H\u0016J\u0010\u0010\u001d\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0018H\u0002J\u0010\u0010\u001e\u001a\u00020\u001b2\u0006\u0010\u001f\u001a\u00020\u0018H\u0002J\u0008\u0010 \u001a\u00020\u001bH\u0002J\u0010\u0010!\u001a\u00020\u001b2\u0006\u0010\u0019\u001a\u00020\tH\u0002J\u0014\u0010\"\u001a\u00020#*\u00020\u00152\u0006\u0010\u0002\u001a\u00020\u0003H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\u0006\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0008j\u0008\u0012\u0004\u0012\u00020\t`\u000b0\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "res",
        "Lcom/squareup/util/Res;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lcom/squareup/util/Res;Lcom/squareup/recycler/RecyclerFactory;Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "canGoBack",
        "",
        "cancellationReasonsList",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType;",
        "chosenReason",
        "Lcom/squareup/ordermanagerdata/CancellationReason;",
        "isReadOnly",
        "mainView",
        "Landroid/view/View;",
        "screen",
        "attach",
        "",
        "view",
        "bindViews",
        "createRecycler",
        "coordinatorView",
        "updateActionBar",
        "updateViews",
        "inventorySubtext",
        "",
        "CancellationReasonRowType",
        "Companion",
        "Factory",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CANCELLATION_REASON_DATA_SOURCE:Lcom/squareup/cycler/DataSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/DataSource<",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Companion;


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private canGoBack:Z

.field private cancellationReasonsList:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType;",
            ">;"
        }
    .end annotation
.end field

.field private chosenReason:Lcom/squareup/ordermanagerdata/CancellationReason;

.field private isReadOnly:Z

.field private mainView:Landroid/view/View;

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final res:Lcom/squareup/util/Res;

.field private screen:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->Companion:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$Companion;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType;

    .line 158
    sget-object v1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$TitleRow;->INSTANCE:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$TitleRow;

    check-cast v1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 159
    new-instance v1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$CancellationReasonRow;

    sget-object v2, Lcom/squareup/ordermanagerdata/CancellationReason;->NOT_IN_STOCK:Lcom/squareup/ordermanagerdata/CancellationReason;

    invoke-direct {v1, v2}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$CancellationReasonRow;-><init>(Lcom/squareup/ordermanagerdata/CancellationReason;)V

    check-cast v1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 160
    new-instance v1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$CancellationReasonRow;

    sget-object v2, Lcom/squareup/ordermanagerdata/CancellationReason;->CUSTOMER_REQUEST:Lcom/squareup/ordermanagerdata/CancellationReason;

    invoke-direct {v1, v2}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$CancellationReasonRow;-><init>(Lcom/squareup/ordermanagerdata/CancellationReason;)V

    check-cast v1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 161
    new-instance v1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$CancellationReasonRow;

    sget-object v2, Lcom/squareup/ordermanagerdata/CancellationReason;->OTHER:Lcom/squareup/ordermanagerdata/CancellationReason;

    invoke-direct {v1, v2}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType$CancellationReasonRow;-><init>(Lcom/squareup/ordermanagerdata/CancellationReason;)V

    check-cast v1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$CancellationReasonRowType;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 157
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 162
    invoke-static {v0}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->CANCELLATION_REASON_DATA_SOURCE:Lcom/squareup/cycler/DataSource;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/recycler/RecyclerFactory;Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getCANCELLATION_REASON_DATA_SOURCE$cp()Lcom/squareup/cycler/DataSource;
    .locals 1

    .line 39
    sget-object v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->CANCELLATION_REASON_DATA_SOURCE:Lcom/squareup/cycler/DataSource;

    return-object v0
.end method

.method public static final synthetic access$getCancellationReasonsList$p(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;)Lcom/squareup/cycler/Recycler;
    .locals 1

    .line 39
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->cancellationReasonsList:Lcom/squareup/cycler/Recycler;

    if-nez p0, :cond_0

    const-string v0, "cancellationReasonsList"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getChosenReason$p(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;)Lcom/squareup/ordermanagerdata/CancellationReason;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->chosenReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;)Lcom/squareup/util/Res;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getScreen$p(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;)Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen;
    .locals 1

    .line 39
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->screen:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen;

    if-nez p0, :cond_0

    const-string v0, "screen"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$inventorySubtext(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 0

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->inventorySubtext(Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setCancellationReasonsList$p(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;Lcom/squareup/cycler/Recycler;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->cancellationReasonsList:Lcom/squareup/cycler/Recycler;

    return-void
.end method

.method public static final synthetic access$setChosenReason$p(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;Lcom/squareup/ordermanagerdata/CancellationReason;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->chosenReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    return-void
.end method

.method public static final synthetic access$setScreen$p(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->screen:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen;

    return-void
.end method

.method public static final synthetic access$updateActionBar(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;)V
    .locals 0

    .line 39
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->updateActionBar()V

    return-void
.end method

.method public static final synthetic access$updateViews(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen;)V
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->updateViews(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 71
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->mainView:Landroid/view/View;

    .line 72
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(com.sq\u2026s.R.id.stable_action_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 73
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->createRecycler(Landroid/view/View;)V

    return-void
.end method

.method private final createRecycler(Landroid/view/View;)V
    .locals 5

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 108
    sget v1, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_cancelation_reason_recycler_view:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v1, "coordinatorView.findView\u2026ion_reason_recycler_view)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    .line 166
    sget-object v1, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 167
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 168
    new-instance v1, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v1}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 172
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 173
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 176
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$$special$$inlined$row$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 111
    sget v2, Lcom/squareup/orderhub/applet/R$layout;->orderhub_recycler_title_row:I

    .line 178
    new-instance v3, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$1;

    invoke-direct {v3, v2, p0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$1;-><init>(ILcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 176
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 175
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 120
    sget-object v0, Lcom/squareup/noho/CheckType$RADIO;->INSTANCE:Lcom/squareup/noho/CheckType$RADIO;

    check-cast v0, Lcom/squareup/noho/CheckType;

    .line 121
    new-instance v2, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;

    invoke-direct {v2, p0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$$inlined$adopt$lambda$2;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function3;

    .line 188
    new-instance v3, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$$special$$inlined$nohoRow$1;

    invoke-direct {v3, v0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$$special$$inlined$nohoRow$1;-><init>(Lcom/squareup/noho/CheckType;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 194
    new-instance v0, Lcom/squareup/cycler/BinderRowSpec;

    .line 198
    sget-object v4, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$$special$$inlined$nohoRow$2;->INSTANCE:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$$special$$inlined$nohoRow$2;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 194
    invoke-direct {v0, v4, v3}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 192
    invoke-virtual {v0, v2}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 197
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 193
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 204
    new-instance v0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {v0}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/16 v2, 0xa

    .line 134
    invoke-virtual {v0, v2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    .line 135
    check-cast v0, Lcom/squareup/cycler/ExtensionSpec;

    .line 204
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 170
    invoke-virtual {v1, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->cancellationReasonsList:Lcom/squareup/cycler/Recycler;

    .line 138
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->cancellationReasonsList:Lcom/squareup/cycler/Recycler;

    if-nez p1, :cond_0

    const-string v0, "cancellationReasonsList"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget-object v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$2;->INSTANCE:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$createRecycler$2;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    return-void

    .line 167
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final inventorySubtext(Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    .line 142
    sget-object v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/CancellationReason;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 144
    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 145
    :cond_1
    :goto_0
    sget p1, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_cancelation_inventory_restocked:I

    .line 144
    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 143
    :cond_2
    sget p1, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_cancelation_inventory_lost:I

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method private final updateActionBar()V
    .locals 6

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 90
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 91
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_cancelation_cancel_items_header:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 92
    iget-boolean v2, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->canGoBack:Z

    if-eqz v2, :cond_1

    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    :goto_0
    new-instance v3, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$updateActionBar$1;

    invoke-direct {v3, p0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$updateActionBar$1;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 96
    sget-object v2, Lcom/squareup/noho/NohoActionButtonStyle;->PRIMARY:Lcom/squareup/noho/NohoActionButtonStyle;

    .line 97
    new-instance v3, Lcom/squareup/util/ViewString$ResourceString;

    sget v4, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_cancelation_cancel_items_header:I

    invoke-direct {v3, v4}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v3, Lcom/squareup/resources/TextModel;

    .line 98
    iget-boolean v4, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->isReadOnly:Z

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->chosenReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    .line 99
    :goto_1
    new-instance v5, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$updateActionBar$2;

    invoke-direct {v5, p0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$updateActionBar$2;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;)V

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 95
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton(Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 102
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateViews(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen;)V
    .locals 2

    .line 79
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->screen:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen;

    .line 80
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen;->getData()Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;->isReadOnly()Z

    move-result v1

    iput-boolean v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->isReadOnly:Z

    .line 83
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;->getCanGoBack()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->canGoBack:Z

    .line 85
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->updateActionBar()V

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->mainView:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "mainView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$updateViews$1;

    invoke-direct {v1, p1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$updateViews$1;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->bindViews(Landroid/view/View;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$attach$1;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
