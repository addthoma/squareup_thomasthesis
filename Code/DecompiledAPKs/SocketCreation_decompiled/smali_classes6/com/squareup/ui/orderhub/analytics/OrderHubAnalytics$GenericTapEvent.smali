.class public final Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericTapEvent;
.super Lcom/squareup/analytics/event/v1/TapEvent;
.source "OrderHubAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GenericTapEvent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericTapEvent;",
        "Lcom/squareup/analytics/event/v1/TapEvent;",
        "tapName",
        "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;",
        "(Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;)V",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;)V
    .locals 1

    const-string v0, "tapName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    check-cast p1, Lcom/squareup/analytics/EventNamedTap;

    invoke-direct {p0, p1}, Lcom/squareup/analytics/event/v1/TapEvent;-><init>(Lcom/squareup/analytics/EventNamedTap;)V

    return-void
.end method
