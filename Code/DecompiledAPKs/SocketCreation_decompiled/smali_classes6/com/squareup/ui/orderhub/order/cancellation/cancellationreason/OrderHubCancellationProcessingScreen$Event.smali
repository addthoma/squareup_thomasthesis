.class public abstract Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event;
.super Ljava/lang/Object;
.source "OrderHubCancellationProcessingScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Event"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event$ContinueToRefund;,
        Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event$SkipRefund;,
        Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event$RetryCancellation;,
        Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event$CancelCancellation;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0004\u0003\u0004\u0005\u0006B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0004\u0007\u0008\t\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event;",
        "",
        "()V",
        "CancelCancellation",
        "ContinueToRefund",
        "RetryCancellation",
        "SkipRefund",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event$ContinueToRefund;",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event$SkipRefund;",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event$RetryCancellation;",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event$CancelCancellation;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event;-><init>()V

    return-void
.end method
