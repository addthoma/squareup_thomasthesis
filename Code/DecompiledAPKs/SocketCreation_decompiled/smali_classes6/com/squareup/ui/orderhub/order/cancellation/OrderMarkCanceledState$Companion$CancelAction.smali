.class public final enum Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;
.super Ljava/lang/Enum;
.source "OrderMarkCanceledState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CancelAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;",
        "",
        "(Ljava/lang/String;I)V",
        "ITEM_SELECTION",
        "CANCEL_REASON",
        "MARK_CANCELED",
        "ISSUE_INVENTORY_ADJUSTMENT",
        "RETRIEVE_BILL_STATE",
        "SHOW_CANCELLATION_COMPLETED",
        "SHOW_CANCELLATION_FAILED",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

.field public static final enum CANCEL_REASON:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

.field public static final enum ISSUE_INVENTORY_ADJUSTMENT:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

.field public static final enum ITEM_SELECTION:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

.field public static final enum MARK_CANCELED:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

.field public static final enum RETRIEVE_BILL_STATE:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

.field public static final enum SHOW_CANCELLATION_COMPLETED:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

.field public static final enum SHOW_CANCELLATION_FAILED:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    new-instance v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    const/4 v2, 0x0

    const-string v3, "ITEM_SELECTION"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->ITEM_SELECTION:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    const/4 v2, 0x1

    const-string v3, "CANCEL_REASON"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->CANCEL_REASON:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    const/4 v2, 0x2

    const-string v3, "MARK_CANCELED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->MARK_CANCELED:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    const/4 v2, 0x3

    const-string v3, "ISSUE_INVENTORY_ADJUSTMENT"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->ISSUE_INVENTORY_ADJUSTMENT:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    const/4 v2, 0x4

    const-string v3, "RETRIEVE_BILL_STATE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->RETRIEVE_BILL_STATE:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    const/4 v2, 0x5

    const-string v3, "SHOW_CANCELLATION_COMPLETED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->SHOW_CANCELLATION_COMPLETED:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    const/4 v2, 0x6

    const-string v3, "SHOW_CANCELLATION_FAILED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->SHOW_CANCELLATION_FAILED:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->$VALUES:[Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;
    .locals 1

    const-class v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;
    .locals 1

    sget-object v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->$VALUES:[Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    invoke-virtual {v0}, [Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    return-object v0
.end method
