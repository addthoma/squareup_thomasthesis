.class public final Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealOrderMarkCanceledWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final orderCancellationReasonWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubBillLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubInventoryServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;",
            ">;"
        }
    .end annotation
.end field

.field private final orderItemSelectionWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final orderRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final uuidGeneratorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/UUIDGenerator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/UUIDGenerator;",
            ">;)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow_Factory;->orderCancellationReasonWorkflowProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow_Factory;->orderItemSelectionWorkflowProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow_Factory;->orderHubAnalyticsProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p4, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow_Factory;->orderRepositoryProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p5, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow_Factory;->orderHubInventoryServiceProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p6, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow_Factory;->orderHubBillLoaderProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p7, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow_Factory;->uuidGeneratorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/UUIDGenerator;",
            ">;)",
            "Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow_Factory;"
        }
    .end annotation

    .line 67
    new-instance v8, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonWorkflow;Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;Lcom/squareup/log/UUIDGenerator;)Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;
    .locals 9

    .line 75
    new-instance v8, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonWorkflow;Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;Lcom/squareup/log/UUIDGenerator;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;
    .locals 8

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow_Factory;->orderCancellationReasonWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonWorkflow;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow_Factory;->orderItemSelectionWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow_Factory;->orderHubAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow_Factory;->orderRepositoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ordermanagerdata/OrderRepository;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow_Factory;->orderHubInventoryServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow_Factory;->orderHubBillLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow_Factory;->uuidGeneratorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/log/UUIDGenerator;

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow_Factory;->newInstance(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonWorkflow;Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;Lcom/squareup/log/UUIDGenerator;)Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow_Factory;->get()Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;

    move-result-object v0

    return-object v0
.end method
