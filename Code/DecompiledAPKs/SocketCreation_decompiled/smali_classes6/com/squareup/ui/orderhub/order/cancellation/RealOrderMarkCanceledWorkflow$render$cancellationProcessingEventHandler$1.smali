.class final Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$cancellationProcessingEventHandler$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderMarkCanceledWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->render(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
        "event",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

.field final synthetic this$0:Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$cancellationProcessingEventHandler$1;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$cancellationProcessingEventHandler$1;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "event"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 256
    sget-object v2, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event$ContinueToRefund;->INSTANCE:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event$ContinueToRefund;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "state.order.id"

    const/4 v4, 0x2

    const/4 v5, 0x0

    if-eqz v2, :cond_0

    .line 257
    iget-object v1, v0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$cancellationProcessingEventHandler$1;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;

    invoke-static {v1}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->access$getOrderHubAnalytics$p(Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;)Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$cancellationProcessingEventHandler$1;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logCancellationDecision$orderhub_applet_release(Ljava/lang/String;Z)V

    .line 258
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 259
    iget-object v6, v0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$cancellationProcessingEventHandler$1;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 260
    sget-object v9, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->RETRIEVE_BILL_STATE:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xfb

    const/16 v16, 0x0

    .line 259
    invoke-static/range {v6 .. v16}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->copy$default(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    move-result-object v2

    .line 258
    invoke-static {v1, v2, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_0

    .line 264
    :cond_0
    sget-object v2, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event$SkipRefund;->INSTANCE:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event$SkipRefund;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 265
    iget-object v1, v0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$cancellationProcessingEventHandler$1;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;

    invoke-static {v1}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->access$getOrderHubAnalytics$p(Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;)Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$cancellationProcessingEventHandler$1;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logCancellationDecision$orderhub_applet_release(Ljava/lang/String;Z)V

    .line 266
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 267
    iget-object v6, v0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$cancellationProcessingEventHandler$1;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 268
    sget-object v9, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->ISSUE_INVENTORY_ADJUSTMENT:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    .line 270
    iget-object v2, v0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$cancellationProcessingEventHandler$1;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;

    invoke-static {v2}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->access$getUuidGenerator$p(Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;)Lcom/squareup/log/UUIDGenerator;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/log/UUIDGenerator;->randomUUID()Ljava/lang/String;

    move-result-object v14

    const/16 v15, 0x3b

    const/16 v16, 0x0

    .line 267
    invoke-static/range {v6 .. v16}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->copy$default(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    move-result-object v2

    .line 266
    invoke-static {v1, v2, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_0

    .line 274
    :cond_1
    sget-object v2, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event$CancelCancellation;->INSTANCE:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event$CancelCancellation;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v2, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$GoBackFromMarkCanceled;->INSTANCE:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$GoBackFromMarkCanceled;

    invoke-virtual {v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_0

    .line 275
    :cond_2
    sget-object v2, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event$RetryCancellation;->INSTANCE:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event$RetryCancellation;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 276
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 277
    iget-object v6, v0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$cancellationProcessingEventHandler$1;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xdf

    const/16 v16, 0x0

    invoke-static/range {v6 .. v16}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->copy$default(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    move-result-object v2

    .line 276
    invoke-static {v1, v2, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_3
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 83
    check-cast p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$cancellationProcessingEventHandler$1;->invoke(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
