.class public final Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2;->invoke(ILcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType$CarrierRow;Lcom/squareup/noho/NohoCheckableRow;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 OrderHubTrackingRunner.kt\ncom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$carriersRecycler$1$2\n*L\n1#1,1322:1\n92#2,13:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0007"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release",
        "com/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$carriersRecycler$1$2$$special$$inlined$onClickDebounced$1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $carrierRow$inlined:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType$CarrierRow;

.field final synthetic this$0:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType$CarrierRow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2$1;->this$0:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2$1;->$carrierRow$inlined:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType$CarrierRow;

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1106
    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    .line 1323
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2$1;->this$0:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2;->this$0:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2$1;->$carrierRow$inlined:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType$CarrierRow;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType$CarrierRow;->getCarrier()Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier;->getCarrierName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->access$isNameOfOtherCarrier(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1324
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2$1;->$carrierRow$inlined:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType$CarrierRow;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType$CarrierRow;->getOnSelectedCarrier()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2$1;->this$0:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2;

    iget-object v0, v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2;->this$0:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;

    invoke-static {v0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->access$getOtherCarrier$p(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;)Lcom/squareup/noho/NohoEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1325
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2$1;->this$0:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2;->this$0:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->access$getOtherCarrier$p(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;)Lcom/squareup/noho/NohoEditText;

    move-result-object p1

    .line 1326
    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditText;->requestFocus()Z

    .line 1327
    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2$1$1;

    invoke-direct {v0, p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2$1$1;-><init>(Lcom/squareup/noho/NohoEditText;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditText;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1330
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2$1;->$carrierRow$inlined:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType$CarrierRow;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType$CarrierRow;->getOnSelectedCarrier()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2$1;->$carrierRow$inlined:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType$CarrierRow;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType$CarrierRow;->getCarrier()Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier;->getCarrierName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1331
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2$1;->this$0:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2;->this$0:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->access$getOtherCarrier$p(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;)Lcom/squareup/noho/NohoEditText;

    move-result-object p1

    .line 1332
    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2$1$2;

    invoke-direct {v0, p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2$1$2;-><init>(Lcom/squareup/noho/NohoEditText;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditText;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method
