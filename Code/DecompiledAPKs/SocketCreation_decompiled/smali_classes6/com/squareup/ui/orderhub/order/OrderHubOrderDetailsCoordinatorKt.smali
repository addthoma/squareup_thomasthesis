.class public final Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinatorKt;
.super Ljava/lang/Object;
.source "OrderHubOrderDetailsCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0003\"\u001a\u0010\u0000\u001a\u00020\u0001*\u0004\u0018\u00010\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "addressText",
        "",
        "Lcom/squareup/protos/connect/v2/resources/Address;",
        "getAddressText",
        "(Lcom/squareup/protos/connect/v2/resources/Address;)Ljava/lang/String;",
        "orderhub-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$getAddressText$p(Lcom/squareup/protos/connect/v2/resources/Address;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinatorKt;->getAddressText(Lcom/squareup/protos/connect/v2/resources/Address;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static final getAddressText(Lcom/squareup/protos/connect/v2/resources/Address;)Ljava/lang/String;
    .locals 9

    if-nez p0, :cond_0

    const-string p0, ""

    return-object p0

    .line 1076
    :cond_0
    sget-object v0, Lcom/squareup/address/Address;->Companion:Lcom/squareup/address/Address$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/address/Address$Companion;->fromConnectV2Address(Lcom/squareup/protos/connect/v2/resources/Address;)Lcom/squareup/address/Address;

    move-result-object p0

    .line 1077
    invoke-static {p0}, Lcom/squareup/address/AddressFormatter;->formatForShippingDisplay(Lcom/squareup/address/Address;)Ljava/util/List;

    move-result-object p0

    move-object v0, p0

    check-cast v0, Ljava/lang/Iterable;

    const-string p0, "\n"

    .line 1078
    move-object v1, p0

    check-cast v1, Ljava/lang/CharSequence;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x3e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
