.class final Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateTracking$1;
.super Ljava/lang/Object;
.source "OrderHubOrderDetailsCoordinator.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->updateTracking(Landroid/view/ViewGroup;ZZLcom/squareup/orders/model/Order$Fulfillment;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $fulfillment:Lcom/squareup/orders/model/Order$Fulfillment;

.field final synthetic $screen:Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;

.field final synthetic $trackingInfo:Lcom/squareup/ordermanagerdata/TrackingInfo;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;Lcom/squareup/orders/model/Order$Fulfillment;Lcom/squareup/ordermanagerdata/TrackingInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateTracking$1;->$screen:Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateTracking$1;->$fulfillment:Lcom/squareup/orders/model/Order$Fulfillment;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateTracking$1;->$trackingInfo:Lcom/squareup/ordermanagerdata/TrackingInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .line 686
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateTracking$1;->$screen:Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 687
    new-instance v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$EditTracking;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateTracking$1;->$fulfillment:Lcom/squareup/orders/model/Order$Fulfillment;

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateTracking$1;->$trackingInfo:Lcom/squareup/ordermanagerdata/TrackingInfo;

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen$Event$EditTracking;-><init>(Lcom/squareup/orders/model/Order$Fulfillment;Lcom/squareup/ordermanagerdata/TrackingInfo;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
