.class final Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$batchedNewUnknownOrders$1;
.super Ljava/lang/Object;
.source "OrderHubNewOrdersDialogMonitor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->batchedNewUnknownOrders()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a.\u0012\u0010\u0012\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00040\u00030\u0002 \u0005*\u0016\u0012\u0010\u0012\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00040\u00030\u0002\u0018\u00010\u00010\u00012\u001e\u0010\u0006\u001a\u001a\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0007H\n\u00a2\u0006\u0002\u0008\u000b"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ordermanagerdata/ResultState;",
        "",
        "Lcom/squareup/orders/model/Order;",
        "kotlin.jvm.PlatformType",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/util/tuple/Quartet;",
        "",
        "Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$batchedNewUnknownOrders$1;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/util/tuple/Quartet;)Lio/reactivex/Observable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/tuple/Quartet<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;",
            "Lkotlin/Unit;",
            ">;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;>;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Quartet;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Quartet;->component2()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Quartet;->component3()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    if-nez v0, :cond_0

    .line 178
    iget-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$batchedNewUnknownOrders$1;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->access$getOrderRepository$p(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)Lcom/squareup/ordermanagerdata/OrderRepository;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ordermanagerdata/OrderRepository;->unknownOrders()Lio/reactivex/Observable;

    move-result-object p1

    goto :goto_0

    :cond_0
    if-nez v1, :cond_1

    .line 180
    iget-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$batchedNewUnknownOrders$1;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->access$setHasAttemptedNotification$p(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;Z)V

    .line 181
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object p1

    goto :goto_0

    .line 183
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->getInterval()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_2

    iget-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$batchedNewUnknownOrders$1;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->access$getOrderRepository$p(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)Lcom/squareup/ordermanagerdata/OrderRepository;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ordermanagerdata/OrderRepository;->unknownOrders()Lio/reactivex/Observable;

    move-result-object p1

    goto :goto_0

    .line 185
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->getInterval()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->getTimeUnit()Ljava/util/concurrent/TimeUnit;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$batchedNewUnknownOrders$1;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    invoke-static {v2}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->access$getComputationScheduler$p(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-static {v0, v1, p1, v2}, Lio/reactivex/Observable;->interval(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "Observable\n             \u2026it, computationScheduler)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$batchedNewUnknownOrders$1;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    invoke-static {v0}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->access$getOrderRepository$p(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)Lcom/squareup/ordermanagerdata/OrderRepository;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/ordermanagerdata/OrderRepository;->unknownOrders()Lio/reactivex/Observable;

    move-result-object v0

    check-cast v0, Lio/reactivex/ObservableSource;

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/RxKotlinKt;->withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object p1

    .line 187
    sget-object v0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$batchedNewUnknownOrders$1$1;->INSTANCE:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$batchedNewUnknownOrders$1$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/util/tuple/Quartet;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$batchedNewUnknownOrders$1;->apply(Lcom/squareup/util/tuple/Quartet;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
