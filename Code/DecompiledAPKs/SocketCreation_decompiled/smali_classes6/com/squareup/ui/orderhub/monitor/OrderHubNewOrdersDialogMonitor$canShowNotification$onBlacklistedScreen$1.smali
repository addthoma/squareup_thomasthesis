.class final Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$canShowNotification$onBlacklistedScreen$1;
.super Ljava/lang/Object;
.source "OrderHubNewOrdersDialogMonitor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->canShowNotification()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubNewOrdersDialogMonitor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubNewOrdersDialogMonitor.kt\ncom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$canShowNotification$onBlacklistedScreen$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,244:1\n1550#2,3:245\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubNewOrdersDialogMonitor.kt\ncom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$canShowNotification$onBlacklistedScreen$1\n*L\n210#1,3:245\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "history",
        "Lflow/History;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$canShowNotification$onBlacklistedScreen$1;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lflow/History;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$canShowNotification$onBlacklistedScreen$1;->apply(Lflow/History;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Lflow/History;)Z
    .locals 6

    const-string v0, "history"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 209
    invoke-virtual {p1}, Lflow/History;->framesFromTop()Ljava/lang/Iterable;

    move-result-object p1

    const-string v0, "history.framesFromTop<ContainerTreeKey>()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 245
    instance-of v0, p1, Ljava/util/Collection;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_2

    .line 246
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    const/4 v3, 0x7

    new-array v3, v3, [Ljava/lang/Class;

    .line 212
    const-class v4, Lcom/squareup/ui/orderhub/OrderHubAppletScope;

    aput-object v4, v3, v2

    .line 213
    const-class v4, Lcom/squareup/cart/util/PartOfCartScope;

    aput-object v4, v3, v1

    const/4 v4, 0x2

    .line 214
    const-class v5, Lcom/squareup/x2/ui/tender/InPipTenderScope;

    aput-object v5, v3, v4

    const/4 v4, 0x3

    .line 215
    const-class v5, Lcom/squareup/ui/crm/flow/InCrmScope;

    aput-object v5, v3, v4

    const/4 v4, 0x4

    .line 216
    const-class v5, Lcom/squareup/x2/ui/crm/InPipCrmScope;

    aput-object v5, v3, v4

    const/4 v4, 0x5

    .line 217
    const-class v5, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;

    aput-object v5, v3, v4

    const/4 v4, 0x6

    .line 218
    const-class v5, Lcom/squareup/ui/permissions/PermissionDeniedScreen;

    aput-object v5, v3, v4

    .line 211
    invoke-virtual {v0, v3}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf([Ljava/lang/Class;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 219
    iget-object v3, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$canShowNotification$onBlacklistedScreen$1;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    invoke-static {v3}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->access$getBuyerFlowStarter$p(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)Lcom/squareup/ui/buyer/BuyerFlowStarter;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/squareup/ui/buyer/BuyerFlowStarter;->isInBuyerScope(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 220
    iget-object v3, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$canShowNotification$onBlacklistedScreen$1;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    invoke-static {v3}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->access$getTenderStarter$p(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)Lcom/squareup/ui/tender/TenderStarter;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/squareup/ui/tender/TenderStarter;->inTenderFlow(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :cond_4
    :goto_2
    return v2
.end method
