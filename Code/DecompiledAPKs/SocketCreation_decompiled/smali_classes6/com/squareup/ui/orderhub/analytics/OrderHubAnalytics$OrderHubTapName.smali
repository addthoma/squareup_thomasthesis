.class public final enum Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;
.super Ljava/lang/Enum;
.source "OrderHubAnalytics.kt"

# interfaces
.implements Lcom/squareup/analytics/EventNamedTap;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OrderHubTapName"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;",
        ">;",
        "Lcom/squareup/analytics/EventNamedTap;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u000c\u0008\u0080\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002B\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u0006\u001a\u00020\u0004H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;",
        "",
        "Lcom/squareup/analytics/EventNamedTap;",
        "value",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getName",
        "ORDER_HUB_FILTER_SELECTED_STATUS_ALL_ACTIVE",
        "ORDER_HUB_FILTER_SELECTED_STATUS_COMPLETED",
        "ORDER_HUB_FILTER_SELECTED_STATUS_UPCOMING",
        "ORDER_HUB_FILTER_SELECTED_TYPE_PICKUP",
        "ORDER_HUB_FILTER_SELECTED_TYPE_SHIPMENT",
        "ORDER_HUB_FILTER_SELECTED_TYPE_MANAGED_DELIVERY",
        "ORDER_HUB_FILTER_SELECTED_TYPE_DELIVERY",
        "ORDER_HUB_FILTER_SELECTED_TYPE_DIGITAL",
        "ORDER_HUB_FILTER_SELECTED_SOURCE",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

.field public static final enum ORDER_HUB_FILTER_SELECTED_SOURCE:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

.field public static final enum ORDER_HUB_FILTER_SELECTED_STATUS_ALL_ACTIVE:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

.field public static final enum ORDER_HUB_FILTER_SELECTED_STATUS_COMPLETED:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

.field public static final enum ORDER_HUB_FILTER_SELECTED_STATUS_UPCOMING:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

.field public static final enum ORDER_HUB_FILTER_SELECTED_TYPE_DELIVERY:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

.field public static final enum ORDER_HUB_FILTER_SELECTED_TYPE_DIGITAL:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

.field public static final enum ORDER_HUB_FILTER_SELECTED_TYPE_MANAGED_DELIVERY:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

.field public static final enum ORDER_HUB_FILTER_SELECTED_TYPE_PICKUP:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

.field public static final enum ORDER_HUB_FILTER_SELECTED_TYPE_SHIPMENT:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    const/4 v2, 0x0

    const-string v3, "ORDER_HUB_FILTER_SELECTED_STATUS_ALL_ACTIVE"

    const-string v4, "Order Hub: Filter - Status - All Active"

    .line 72
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;->ORDER_HUB_FILTER_SELECTED_STATUS_ALL_ACTIVE:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    const/4 v2, 0x1

    const-string v3, "ORDER_HUB_FILTER_SELECTED_STATUS_COMPLETED"

    const-string v4, "Order Hub: Filter - Status - Completed"

    .line 73
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;->ORDER_HUB_FILTER_SELECTED_STATUS_COMPLETED:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    const/4 v2, 0x2

    const-string v3, "ORDER_HUB_FILTER_SELECTED_STATUS_UPCOMING"

    const-string v4, "Order Hub: Filter - Status - Upcoming"

    .line 74
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;->ORDER_HUB_FILTER_SELECTED_STATUS_UPCOMING:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    const/4 v2, 0x3

    const-string v3, "ORDER_HUB_FILTER_SELECTED_TYPE_PICKUP"

    const-string v4, "Order Hub: Filter - Type - Pickup"

    .line 75
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;->ORDER_HUB_FILTER_SELECTED_TYPE_PICKUP:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    const/4 v2, 0x4

    const-string v3, "ORDER_HUB_FILTER_SELECTED_TYPE_SHIPMENT"

    const-string v4, "Order Hub: Filter - Type - Shipment"

    .line 76
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;->ORDER_HUB_FILTER_SELECTED_TYPE_SHIPMENT:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    const/4 v2, 0x5

    const-string v3, "ORDER_HUB_FILTER_SELECTED_TYPE_MANAGED_DELIVERY"

    const-string v4, "Order Hub: Filter - Type - Managed Delivery"

    .line 77
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;->ORDER_HUB_FILTER_SELECTED_TYPE_MANAGED_DELIVERY:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    const/4 v2, 0x6

    const-string v3, "ORDER_HUB_FILTER_SELECTED_TYPE_DELIVERY"

    const-string v4, "Order Hub: Filter - Type - Delivery"

    .line 78
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;->ORDER_HUB_FILTER_SELECTED_TYPE_DELIVERY:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    const/4 v2, 0x7

    const-string v3, "ORDER_HUB_FILTER_SELECTED_TYPE_DIGITAL"

    const-string v4, "Order Hub: Filter - Type - Digital"

    .line 79
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;->ORDER_HUB_FILTER_SELECTED_TYPE_DIGITAL:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    const/16 v2, 0x8

    const-string v3, "ORDER_HUB_FILTER_SELECTED_SOURCE"

    const-string v4, "Order Hub: Filter - Source"

    .line 80
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;->ORDER_HUB_FILTER_SELECTED_SOURCE:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;->$VALUES:[Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 70
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;
    .locals 1

    const-class v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;
    .locals 1

    sget-object v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;->$VALUES:[Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    invoke-virtual {v0}, [Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;->value:Ljava/lang/String;

    return-object v0
.end method
