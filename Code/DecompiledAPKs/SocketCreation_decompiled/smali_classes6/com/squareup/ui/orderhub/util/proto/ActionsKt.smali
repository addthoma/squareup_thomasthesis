.class public final Lcom/squareup/ui/orderhub/util/proto/ActionsKt;
.super Ljava/lang/Object;
.source "Actions.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0015\u0010\u0005\u001a\u00020\u0006*\u00020\u00078G\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\n"
    }
    d2 = {
        "canApplyToLineItems",
        "",
        "Lcom/squareup/protos/client/orders/Action;",
        "getCanApplyToLineItems",
        "(Lcom/squareup/protos/client/orders/Action;)Z",
        "nameRes",
        "",
        "Lcom/squareup/protos/client/orders/Action$Type;",
        "getNameRes",
        "(Lcom/squareup/protos/client/orders/Action$Type;)I",
        "orderhub-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getCanApplyToLineItems(Lcom/squareup/protos/client/orders/Action;)Z
    .locals 1

    const-string v0, "$this$canApplyToLineItems"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object p0, p0, Lcom/squareup/protos/client/orders/Action;->can_apply_to_line_items:Ljava/lang/Boolean;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final getNameRes(Lcom/squareup/protos/client/orders/Action$Type;)I
    .locals 3

    const-string v0, "$this$nameRes"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/ActionsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/Action$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 31
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported Action type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 28
    :pswitch_1
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_fulfillment_action_mark_in_progress:I

    goto :goto_0

    .line 27
    :pswitch_2
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_fulfillment_action_mark_shipped:I

    goto :goto_0

    .line 26
    :pswitch_3
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_fulfillment_action_mark_ready:I

    goto :goto_0

    .line 25
    :pswitch_4
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_fulfillment_action_mark_picked_up:I

    goto :goto_0

    .line 24
    :pswitch_5
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_fulfillment_action_complete:I

    goto :goto_0

    .line 23
    :pswitch_6
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_fulfillment_action_cancel_items:I

    goto :goto_0

    .line 22
    :pswitch_7
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_fulfillment_action_accept:I

    :goto_0
    return p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
