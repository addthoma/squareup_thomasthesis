.class public final Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;
.super Ljava/lang/Object;
.source "LineItems.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLineItems.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LineItems.kt\ncom/squareup/ui/orderhub/util/proto/LineItemsKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,162:1\n704#2:163\n777#2,2:164\n1412#2,9:166\n1642#2,2:175\n1421#2:177\n1412#2,9:178\n1642#2,2:187\n1421#2:189\n1360#2:190\n1429#2,3:191\n1587#2,3:194\n*E\n*S KotlinDebug\n*F\n+ 1 LineItems.kt\ncom/squareup/ui/orderhub/util/proto/LineItemsKt\n*L\n106#1:163\n106#1,2:164\n107#1,9:166\n107#1,2:175\n107#1:177\n156#1,9:178\n156#1,2:187\n156#1:189\n157#1:190\n157#1,3:191\n158#1,3:194\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a(\u0010\n\u001a\u0004\u0018\u00010\u000b*\u00020\u00022\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u0006H\u0000\u001a\u0012\u0010\u0011\u001a\u00020\u000b*\u00020\u00022\u0006\u0010\u000c\u001a\u00020\r\u001a$\u0010\u0012\u001a\u00020\u000b*\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u000c\u001a\u00020\rH\u0002\u001a\"\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0016*\u00020\u00022\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0000\u001a\u0014\u0010\u0017\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u0019H\u0002\u001a\u0014\u0010\u001a\u001a\u00020\u001b*\u00020\u00022\u0006\u0010\u000c\u001a\u00020\rH\u0000\u001a\u001e\u0010\u001c\u001a\u0004\u0018\u00010\u000b*\u00020\u00022\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0000\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0018\u0010\u0005\u001a\u00020\u0006*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0007\"\u0018\u0010\u0008\u001a\u00020\u0006*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\u0007\u00a8\u0006\u001d"
    }
    d2 = {
        "basePricePerQuantity",
        "Lcom/squareup/protos/common/Money;",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "getBasePricePerQuantity",
        "(Lcom/squareup/orders/model/Order$LineItem;)Lcom/squareup/protos/common/Money;",
        "isUnitPriced",
        "",
        "(Lcom/squareup/orders/model/Order$LineItem;)Z",
        "shouldShowVariationName",
        "getShouldShowVariationName",
        "basePricePerQuantityUnit",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "perUnitFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "includeQuantity",
        "displayName",
        "formatPrice",
        "money",
        "Lcom/squareup/protos/connect/v2/common/Money;",
        "modifierStrings",
        "",
        "modifiersBasePrice",
        "currency",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "unitDisplayData",
        "Lcom/squareup/quantity/UnitDisplayData;",
        "variationAndPriceString",
        "orderhub-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final basePricePerQuantityUnit(Lcom/squareup/orders/model/Order$LineItem;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Z)Ljava/lang/String;
    .locals 2

    const-string v0, "$this$basePricePerQuantityUnit"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "perUnitFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-static {p0}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->isUnitPriced(Lcom/squareup/orders/model/Order$LineItem;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 61
    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->unitDisplayData(Lcom/squareup/orders/model/Order$LineItem;Lcom/squareup/util/Res;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object p1

    .line 65
    iget-object v0, p0, Lcom/squareup/orders/model/Order$LineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    const-string v1, "base_price_money"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/money/v2/MoneysKt;->toMoneyV1(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/quantity/PerUnitFormatter;->money(Lcom/squareup/protos/common/Money;)Lcom/squareup/quantity/PerUnitFormatter;

    .line 66
    invoke-virtual {p1}, Lcom/squareup/quantity/UnitDisplayData;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/quantity/PerUnitFormatter;->moneyUnit(Ljava/lang/String;)Lcom/squareup/quantity/PerUnitFormatter;

    if-eqz p3, :cond_1

    .line 68
    iget-object p0, p0, Lcom/squareup/orders/model/Order$LineItem;->quantity:Ljava/lang/String;

    const-string p3, "quantity"

    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p3, Ljava/math/BigDecimal;

    invoke-direct {p3, p0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/quantity/UnitDisplayData;->getQuantityPrecision()I

    move-result p0

    invoke-virtual {p2, p3, p0}, Lcom/squareup/quantity/PerUnitFormatter;->quantityAndPrecision(Ljava/math/BigDecimal;I)Lcom/squareup/quantity/PerUnitFormatter;

    .line 71
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 72
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic basePricePerQuantityUnit$default(Lcom/squareup/orders/model/Order$LineItem;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZILjava/lang/Object;)Ljava/lang/String;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 55
    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->basePricePerQuantityUnit(Lcom/squareup/orders/model/Order$LineItem;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final displayName(Lcom/squareup/orders/model/Order$LineItem;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$displayName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/squareup/orders/model/Order$LineItem;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 23
    iget-object p0, p0, Lcom/squareup/orders/model/Order$LineItem;->name:Ljava/lang/String;

    const-string p1, "name"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0

    .line 26
    :cond_0
    iget-object p0, p0, Lcom/squareup/orders/model/Order$LineItem;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$ItemType;->CUSTOM_AMOUNT:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    if-ne p0, v0, :cond_1

    .line 27
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_item_name_custom_amount:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 30
    :cond_1
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_item_name_unknown:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static final formatPrice(Lcom/squareup/orders/model/Order$LineItem;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 0

    .line 143
    invoke-static {p0, p3}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->unitDisplayData(Lcom/squareup/orders/model/Order$LineItem;Lcom/squareup/util/Res;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object p0

    .line 146
    invoke-static {p1}, Lcom/squareup/money/v2/MoneysKt;->toMoneyV1(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/quantity/PerUnitFormatter;->money(Lcom/squareup/protos/common/Money;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p1

    .line 147
    invoke-virtual {p1}, Lcom/squareup/quantity/PerUnitFormatter;->inParentheses()Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p1

    .line 148
    invoke-virtual {p0}, Lcom/squareup/quantity/UnitDisplayData;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Lcom/squareup/quantity/PerUnitFormatter;->unit(Ljava/lang/String;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p0

    .line 149
    invoke-virtual {p0}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 150
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final getBasePricePerQuantity(Lcom/squareup/orders/model/Order$LineItem;)Lcom/squareup/protos/common/Money;
    .locals 3

    const-string v0, "$this$basePricePerQuantity"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/orders/model/Order$LineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    const-string v1, "base_price_money"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/money/v2/MoneysKt;->toMoneyV1(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 40
    iget-object v1, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v2, "itemBasePrice.currency_code"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, v1}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->modifiersBasePrice(Lcom/squareup/orders/model/Order$LineItem;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    .line 42
    invoke-static {v0, p0}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    const-string v0, "MoneyMath.sum(itemBasePrice, modifiersBasePrice)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getShouldShowVariationName(Lcom/squareup/orders/model/Order$LineItem;)Z
    .locals 3

    const-string v0, "$this$shouldShowVariationName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/squareup/orders/model/Order$LineItem;->variation_name:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    goto :goto_5

    .line 128
    :cond_2
    iget-object v0, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_object_id:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_4

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    :cond_4
    :goto_2
    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_6

    :cond_5
    :goto_4
    const/4 v1, 0x1

    goto :goto_5

    .line 129
    :cond_6
    iget-object v0, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_item_variation_count:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    iget-object p0, p0, Lcom/squareup/orders/model/Order$LineItem;->catalog_item_variation_count:Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->compare(II)I

    move-result p0

    if-lez p0, :cond_7

    goto :goto_4

    :cond_7
    :goto_5
    return v1
.end method

.method public static final isUnitPriced(Lcom/squareup/orders/model/Order$LineItem;)Z
    .locals 1

    const-string v0, "$this$isUnitPriced"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    iget-object p0, p0, Lcom/squareup/orders/model/Order$LineItem;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final modifierStrings(Lcom/squareup/orders/model/Order$LineItem;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order$LineItem;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$modifierStrings"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "perUnitFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/squareup/orders/model/Order$LineItem;->modifiers:Ljava/util/List;

    const-string v1, "modifiers"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 163
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 164
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/orders/model/Order$LineItem$Modifier;

    .line 106
    iget-object v3, v3, Lcom/squareup/orders/model/Order$LineItem$Modifier;->name:Ljava/lang/String;

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 165
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 175
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 174
    check-cast v2, Lcom/squareup/orders/model/Order$LineItem$Modifier;

    .line 108
    iget-object v3, v2, Lcom/squareup/orders/model/Order$LineItem$Modifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-nez v3, :cond_4

    .line 109
    iget-object v2, v2, Lcom/squareup/orders/model/Order$LineItem$Modifier;->name:Ljava/lang/String;

    goto :goto_3

    .line 110
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v2, Lcom/squareup/orders/model/Order$LineItem$Modifier;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v2, v2, Lcom/squareup/orders/model/Order$LineItem$Modifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    const-string v4, "it.base_price_money"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, v2, p2, p1}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->formatPrice(Lcom/squareup/orders/model/Order$LineItem;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_3
    if-eqz v2, :cond_3

    .line 174
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 177
    :cond_5
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private static final modifiersBasePrice(Lcom/squareup/orders/model/Order$LineItem;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;
    .locals 2

    const-wide/16 v0, 0x0

    .line 154
    invoke-static {p1, v0, v1}, Lcom/squareup/money/MoneyBuilderKt;->of(Lcom/squareup/protos/common/CurrencyCode;J)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 156
    iget-object p0, p0, Lcom/squareup/orders/model/Order$LineItem;->modifiers:Ljava/util/List;

    const-string v0, "modifiers"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Iterable;

    .line 178
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 187
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 186
    check-cast v1, Lcom/squareup/orders/model/Order$LineItem$Modifier;

    .line 156
    iget-object v1, v1, Lcom/squareup/orders/model/Order$LineItem$Modifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_0

    .line 186
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 189
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 190
    new-instance p0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p0, Ljava/util/Collection;

    .line 191
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 192
    check-cast v1, Lcom/squareup/protos/connect/v2/common/Money;

    .line 157
    invoke-static {v1}, Lcom/squareup/money/v2/MoneysKt;->toMoneyV1(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 193
    :cond_2
    check-cast p0, Ljava/util/List;

    check-cast p0, Ljava/lang/Iterable;

    .line 195
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_2
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    .line 159
    invoke-static {p1, v0}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    const-string v0, "MoneyMath.sum(acc, money)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    const-string p0, "modifiers.mapNotNull { i\u2026h.sum(acc, money)\n      }"

    .line 196
    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public static final unitDisplayData(Lcom/squareup/orders/model/Order$LineItem;Lcom/squareup/util/Res;)Lcom/squareup/quantity/UnitDisplayData;
    .locals 1

    const-string v0, "$this$unitDisplayData"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    sget-object v0, Lcom/squareup/quantity/UnitDisplayData;->Companion:Lcom/squareup/quantity/UnitDisplayData$Companion;

    iget-object p0, p0, Lcom/squareup/orders/model/Order$LineItem;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    invoke-virtual {v0, p1, p0}, Lcom/squareup/quantity/UnitDisplayData$Companion;->fromQuantityUnit(Lcom/squareup/util/Res;Lcom/squareup/orders/model/Order$QuantityUnit;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object p0

    return-object p0
.end method

.method public static final variationAndPriceString(Lcom/squareup/orders/model/Order$LineItem;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;)Ljava/lang/String;
    .locals 3

    const-string v0, "$this$variationAndPriceString"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "perUnitFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-static {p0}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->getShouldShowVariationName(Lcom/squareup/orders/model/Order$LineItem;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/squareup/orders/model/Order$LineItem;->variation_name:Ljava/lang/String;

    .line 89
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_2

    .line 90
    invoke-static {v1}, Lcom/squareup/money/v2/MoneysKt;->isZero(Lcom/squareup/protos/connect/v2/common/Money;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 93
    :cond_1
    invoke-static {p0, v1, p2, p1}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->formatPrice(Lcom/squareup/orders/model/Order$LineItem;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p0

    .line 94
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p2, 0x20

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_2
    :goto_0
    return-object v0
.end method
