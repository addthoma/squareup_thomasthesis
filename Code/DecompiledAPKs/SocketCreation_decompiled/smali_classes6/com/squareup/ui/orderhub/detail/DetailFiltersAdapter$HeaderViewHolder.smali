.class public final Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$HeaderViewHolder;
.super Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$ViewHolder;
.source "DetailFiltersAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "HeaderViewHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0008\u0080\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$HeaderViewHolder;",
        "Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$ViewHolder;",
        "itemView",
        "Landroid/view/View;",
        "(Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;Landroid/view/View;)V",
        "bind",
        "",
        "position",
        "",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    const-string v0, "itemView"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$HeaderViewHolder;->this$0:Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;

    invoke-direct {p0, p2}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public bind(I)V
    .locals 2

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$HeaderViewHolder;->itemView:Landroid/view/View;

    if-eqz v0, :cond_0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$HeaderViewHolder;->this$0:Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;

    invoke-static {v1}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;->access$getFilterList$p(Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;->getHeader()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.widget.TextView"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
