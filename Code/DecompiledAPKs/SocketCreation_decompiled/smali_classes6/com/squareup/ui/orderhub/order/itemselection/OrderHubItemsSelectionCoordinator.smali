.class public final Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "OrderHubItemSelectionCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubItemSelectionCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubItemSelectionCoordinator.kt\ncom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 5 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n+ 6 RecyclerNoho.kt\ncom/squareup/noho/dsl/RecyclerNohoKt\n+ 7 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n+ 8 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n*L\n1#1,281:1\n49#2:282\n50#2,3:288\n53#2:339\n599#3,4:283\n601#3:287\n310#4,3:291\n313#4,3:300\n328#4:308\n342#4,5:309\n344#4,4:314\n329#4:318\n328#4:325\n342#4,5:326\n344#4,4:331\n329#4:335\n35#5,6:294\n114#6,5:303\n120#6:319\n114#6,5:320\n120#6:336\n43#7,2:337\n46#8,9:340\n67#8:349\n92#8,3:350\n55#8,3:353\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubItemSelectionCoordinator.kt\ncom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator\n*L\n113#1:282\n113#1,3:288\n113#1:339\n113#1,4:283\n113#1:287\n113#1,3:291\n113#1,3:300\n113#1:308\n113#1,5:309\n113#1,4:314\n113#1:318\n113#1:325\n113#1,5:326\n113#1,4:331\n113#1:335\n113#1,6:294\n113#1,5:303\n113#1:319\n113#1,5:320\n113#1:336\n113#1,2:337\n208#1,9:340\n208#1:349\n208#1,3:350\n208#1,3:353\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001:\u00011BQ\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\"\u0010\u000b\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rj\u0008\u0012\u0004\u0012\u00020\u000e`\u00100\u000c\u00a2\u0006\u0002\u0010\u0011J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0018H\u0016J\u0010\u0010\u001d\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0018H\u0002J\u0010\u0010\u001e\u001a\u00020\u001b2\u0006\u0010\u001f\u001a\u00020\u0018H\u0002Jn\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u00160!2\u0006\u0010\"\u001a\u00020#2*\u0010$\u001a&\u0012\u0004\u0012\u00020&\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060&j\u0002`\'\u0012\u0004\u0012\u00020(0%j\u0002`)0%j\u0002`*2*\u0010+\u001a&\u0012\u0004\u0012\u00020&\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060&j\u0002`\'\u0012\u0004\u0012\u00020(0%j\u0002`)0%j\u0002`*H\u0002JD\u0010,\u001a\u00020\u001b2\u0006\u0010\"\u001a\u00020#2\u0006\u0010-\u001a\u00020.2*\u0010/\u001a&\u0012\u0004\u0012\u00020&\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060&j\u0002`\'\u0012\u0004\u0012\u00020(0%j\u0002`)0%j\u0002`*H\u0002J\u0010\u00100\u001a\u00020\u001b2\u0006\u0010\u0019\u001a\u00020\u000eH\u0002R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\u000b\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rj\u0008\u0012\u0004\u0012\u00020\u000e`\u00100\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00062"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "res",
        "Lcom/squareup/util/Res;",
        "lineItemSubtitleFormatter",
        "Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lcom/squareup/util/Res;Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/recycler/RecyclerFactory;Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "itemsList",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;",
        "mainView",
        "Landroid/view/View;",
        "screen",
        "attach",
        "",
        "view",
        "bindViews",
        "createRecycler",
        "coordinatorView",
        "lineItemSelection",
        "Lcom/squareup/cycler/DataSource;",
        "action",
        "Lcom/squareup/protos/client/orders/Action$Type;",
        "selectedLineItems",
        "",
        "",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemRowIdentifier;",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemWithQuantityByIdentifier;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemSelectionsByUid;",
        "availableLineItems",
        "updateActionBar",
        "isReadOnly",
        "",
        "enabledLineItems",
        "updateViews",
        "Factory",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private itemsList:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;",
            ">;"
        }
    .end annotation
.end field

.field private final lineItemSubtitleFormatter:Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;

.field private mainView:Landroid/view/View;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final res:Lcom/squareup/util/Res;

.field private screen:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/recycler/RecyclerFactory;Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lineItemSubtitleFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->lineItemSubtitleFormatter:Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iput-object p5, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getLineItemSubtitleFormatter$p(Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;)Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->lineItemSubtitleFormatter:Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;

    return-object p0
.end method

.method public static final synthetic access$getMoneyFormatter$p(Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;)Lcom/squareup/util/Res;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getScreen$p(Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;)Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen;
    .locals 1

    .line 49
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->screen:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen;

    if-nez p0, :cond_0

    const-string v0, "screen"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$lineItemSelection(Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;Lcom/squareup/protos/client/orders/Action$Type;Ljava/util/Map;Ljava/util/Map;)Lcom/squareup/cycler/DataSource;
    .locals 0

    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->lineItemSelection(Lcom/squareup/protos/client/orders/Action$Type;Ljava/util/Map;Ljava/util/Map;)Lcom/squareup/cycler/DataSource;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setScreen$p(Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->screen:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen;

    return-void
.end method

.method public static final synthetic access$updateViews(Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen;)V
    .locals 0

    .line 49
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->updateViews(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 83
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->mainView:Landroid/view/View;

    .line 84
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(com.sq\u2026s.R.id.stable_action_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 85
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->createRecycler(Landroid/view/View;)V

    return-void
.end method

.method private final createRecycler(Landroid/view/View;)V
    .locals 5

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 114
    sget v1, Lcom/squareup/orderhub/applet/R$id;->orderhub_item_selection_items_list:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v1, "coordinatorView.findView\u2026tem_selection_items_list)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    .line 282
    sget-object v1, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 283
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 284
    new-instance v1, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v1}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 288
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 289
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 117
    sget-object v0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemSelectionComparator;->INSTANCE:Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemSelectionComparator;

    check-cast v0, Lcom/squareup/cycler/ItemComparator;

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->setItemComparator(Lcom/squareup/cycler/ItemComparator;)V

    .line 292
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$$special$$inlined$row$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 120
    sget v2, Lcom/squareup/orderhub/applet/R$layout;->orderhub_recycler_title_row:I

    .line 294
    new-instance v3, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$createRecycler$$inlined$adopt$lambda$1;

    invoke-direct {v3, v2, p0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$createRecycler$$inlined$adopt$lambda$1;-><init>(ILcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 292
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 291
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 137
    sget-object v0, Lcom/squareup/noho/CheckType$CHECK;->INSTANCE:Lcom/squareup/noho/CheckType$CHECK;

    check-cast v0, Lcom/squareup/noho/CheckType;

    .line 138
    new-instance v2, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$createRecycler$$inlined$adopt$lambda$2;

    invoke-direct {v2, p0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$createRecycler$$inlined$adopt$lambda$2;-><init>(Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function3;

    .line 304
    new-instance v3, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$$special$$inlined$nohoRow$1;

    invoke-direct {v3, v0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$$special$$inlined$nohoRow$1;-><init>(Lcom/squareup/noho/CheckType;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 310
    new-instance v0, Lcom/squareup/cycler/BinderRowSpec;

    .line 314
    sget-object v4, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$$special$$inlined$nohoRow$2;->INSTANCE:Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$$special$$inlined$nohoRow$2;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 310
    invoke-direct {v0, v4, v3}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 308
    invoke-virtual {v0, v2}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 313
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 309
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 144
    sget-object v0, Lcom/squareup/noho/CheckType$CHECK;->INSTANCE:Lcom/squareup/noho/CheckType$CHECK;

    check-cast v0, Lcom/squareup/noho/CheckType;

    .line 145
    new-instance v2, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$createRecycler$$inlined$adopt$lambda$3;

    invoke-direct {v2, p0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$createRecycler$$inlined$adopt$lambda$3;-><init>(Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function3;

    .line 321
    new-instance v3, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$$special$$inlined$nohoRow$3;

    invoke-direct {v3, v0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$$special$$inlined$nohoRow$3;-><init>(Lcom/squareup/noho/CheckType;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 327
    new-instance v0, Lcom/squareup/cycler/BinderRowSpec;

    .line 331
    sget-object v4, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$$special$$inlined$nohoRow$4;->INSTANCE:Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$$special$$inlined$nohoRow$4;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 327
    invoke-direct {v0, v4, v3}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 325
    invoke-virtual {v0, v2}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 330
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 326
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 337
    new-instance v0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {v0}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/16 v2, 0xa

    .line 168
    invoke-virtual {v0, v2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    .line 169
    check-cast v0, Lcom/squareup/cycler/ExtensionSpec;

    .line 337
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 286
    invoke-virtual {v1, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->itemsList:Lcom/squareup/cycler/Recycler;

    .line 171
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->itemsList:Lcom/squareup/cycler/Recycler;

    if-nez p1, :cond_0

    const-string v0, "itemsList"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    const/4 v0, 0x0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    return-void

    .line 283
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final lineItemSelection(Lcom/squareup/protos/client/orders/Action$Type;Ljava/util/Map;Ljava/util/Map;)Lcom/squareup/cycler/DataSource;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/orders/Action$Type;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;)",
            "Lcom/squareup/cycler/DataSource<",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;",
            ">;"
        }
    .end annotation

    .line 205
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 206
    new-instance v1, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$TitleRow;

    invoke-direct {v1, p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$TitleRow;-><init>(Lcom/squareup/protos/client/orders/Action$Type;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    new-instance p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$SelectAll;

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v1

    invoke-interface {p3}, Ljava/util/Map;->size()I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-direct {p1, v1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$SelectAll;-><init>(Z)V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 340
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/Collection;

    .line 347
    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p3

    invoke-interface {p3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_1
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 209
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    .line 210
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    .line 349
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v4, Ljava/util/Collection;

    .line 350
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 211
    new-instance v6, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/orders/model/Order$LineItem;

    invoke-direct {v6, v7, v5, v2}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;-><init>(Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem;Z)V

    invoke-interface {v4, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 352
    :cond_1
    check-cast v4, Ljava/util/List;

    .line 212
    check-cast v4, Ljava/lang/Iterable;

    .line 353
    invoke-static {p1, v4}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_1

    .line 355
    :cond_2
    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    .line 208
    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 215
    invoke-static {v0}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object p1

    return-object p1
.end method

.method private final updateActionBar(Lcom/squareup/protos/client/orders/Action$Type;ZLjava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/orders/Action$Type;",
            "Z",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;)V"
        }
    .end annotation

    .line 179
    sget-object v0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/Action$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 181
    sget p1, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_cancelation_cancel_items_header:I

    goto :goto_0

    .line 182
    :cond_0
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unexpected action "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2

    .line 180
    :cond_1
    sget p1, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_shipment_ship_items_header:I

    .line 197
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_2

    const-string v2, "actionBar"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 185
    :cond_2
    new-instance v2, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 186
    new-instance v3, Lcom/squareup/util/ViewString$ResourceString;

    invoke-direct {v3, p1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v3, Lcom/squareup/resources/TextModel;

    invoke-virtual {v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 187
    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$updateActionBar$1;

    invoke-direct {v3, p0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$updateActionBar$1;-><init>(Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {p1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 191
    sget-object v2, Lcom/squareup/noho/NohoActionButtonStyle;->PRIMARY:Lcom/squareup/noho/NohoActionButtonStyle;

    .line 192
    new-instance v3, Lcom/squareup/util/ViewString$ResourceString;

    sget v4, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_item_selection_next:I

    invoke-direct {v3, v4}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v3, Lcom/squareup/resources/TextModel;

    if-nez p2, :cond_3

    .line 193
    invoke-interface {p3}, Ljava/util/Map;->isEmpty()Z

    move-result p2

    xor-int/2addr p2, v1

    if-eqz p2, :cond_3

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    .line 194
    :goto_1
    new-instance p2, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$updateActionBar$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$updateActionBar$2;-><init>(Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    .line 190
    invoke-virtual {p1, v2, v3, v1, p2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton(Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 197
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateViews(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen;)V
    .locals 3

    .line 91
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->screen:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen;

    .line 92
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen;->getData()Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    move-result-object v0

    .line 94
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->mainView:Landroid/view/View;

    if-nez v1, :cond_0

    const-string v2, "mainView"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v2, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$updateViews$1;

    invoke-direct {v2, p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$updateViews$1;-><init>(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-static {v1, v2}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 97
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->getAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/orders/Action;->type:Lcom/squareup/protos/client/orders/Action$Type;

    const-string v1, "screenData.action.type"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->isReadOnly()Z

    move-result v1

    .line 99
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->getSelectedLineItems()Ljava/util/Map;

    move-result-object v2

    .line 96
    invoke-direct {p0, p1, v1, v2}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->updateActionBar(Lcom/squareup/protos/client/orders/Action$Type;ZLjava/util/Map;)V

    .line 102
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->itemsList:Lcom/squareup/cycler/Recycler;

    if-nez p1, :cond_1

    const-string v1, "itemsList"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$updateViews$2;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$updateViews$2;-><init>(Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->bindViews(Landroid/view/View;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$attach$1;-><init>(Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
