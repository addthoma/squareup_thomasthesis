.class public final Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinatorKt;
.super Ljava/lang/Object;
.source "OrderHubDetailCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubDetailCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubDetailCoordinator.kt\ncom/squareup/ui/orderhub/detail/OrderHubDetailCoordinatorKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,869:1\n1360#2:870\n1429#2,3:871\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubDetailCoordinator.kt\ncom/squareup/ui/orderhub/detail/OrderHubDetailCoordinatorKt\n*L\n821#1:870\n821#1,3:871\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a8\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00080\u00062\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\nH\u0002\u001a$\u0010\u000b\u001a\u00020\u0007*\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002\u00a8\u0006\u0013"
    }
    d2 = {
        "asOrderDetailRows",
        "Lcom/squareup/cycler/DataSource;",
        "Lcom/squareup/ui/orderhub/detail/OrderDetailRow;",
        "",
        "Lcom/squareup/orders/model/Order;",
        "orderQuickActionStatusMap",
        "",
        "",
        "Lcom/squareup/ui/orderhub/OrderQuickActionStatus;",
        "filterHeader",
        "Lcom/squareup/ui/orderhub/master/Filter;",
        "getTitle",
        "Lcom/squareup/orders/model/Order$Fulfillment;",
        "res",
        "Lcom/squareup/util/Res;",
        "relativeDateAndTimeFormatter",
        "Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;",
        "now",
        "Lorg/threeten/bp/ZonedDateTime;",
        "orderhub-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$asOrderDetailRows(Ljava/util/List;Ljava/util/Map;Lcom/squareup/ui/orderhub/master/Filter;)Lcom/squareup/cycler/DataSource;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinatorKt;->asOrderDetailRows(Ljava/util/List;Ljava/util/Map;Lcom/squareup/ui/orderhub/master/Filter;)Lcom/squareup/cycler/DataSource;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getTitle(Lcom/squareup/orders/model/Order$Fulfillment;Lcom/squareup/util/Res;Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;Lorg/threeten/bp/ZonedDateTime;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinatorKt;->getTitle(Lcom/squareup/orders/model/Order$Fulfillment;Lcom/squareup/util/Res;Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;Lorg/threeten/bp/ZonedDateTime;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static final asOrderDetailRows(Ljava/util/List;Ljava/util/Map;Lcom/squareup/ui/orderhub/master/Filter;)Lcom/squareup/cycler/DataSource;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/orderhub/OrderQuickActionStatus;",
            ">;",
            "Lcom/squareup/ui/orderhub/master/Filter;",
            ")",
            "Lcom/squareup/cycler/DataSource<",
            "Lcom/squareup/ui/orderhub/detail/OrderDetailRow;",
            ">;"
        }
    .end annotation

    .line 821
    check-cast p0, Ljava/lang/Iterable;

    .line 870
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 871
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 872
    check-cast v1, Lcom/squareup/orders/model/Order;

    .line 822
    new-instance v2, Lcom/squareup/ui/orderhub/detail/OrderDetailRow$OrderRow;

    iget-object v3, v1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;

    invoke-direct {v2, v1, v3}, Lcom/squareup/ui/orderhub/detail/OrderDetailRow$OrderRow;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/ui/orderhub/OrderQuickActionStatus;)V

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 873
    :cond_0
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    .line 823
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p0

    const/4 p1, 0x0

    .line 825
    sget-object v0, Lcom/squareup/ui/orderhub/detail/OrderDetailRow$Spacer;->INSTANCE:Lcom/squareup/ui/orderhub/detail/OrderDetailRow$Spacer;

    invoke-interface {p0, p1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    if-eqz p2, :cond_1

    const/4 p1, 0x1

    .line 829
    new-instance v0, Lcom/squareup/ui/orderhub/detail/OrderDetailRow$FilterHeader;

    invoke-direct {v0, p2}, Lcom/squareup/ui/orderhub/detail/OrderDetailRow$FilterHeader;-><init>(Lcom/squareup/ui/orderhub/master/Filter;)V

    invoke-interface {p0, p1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 832
    :cond_1
    invoke-static {p0}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object p0

    return-object p0
.end method

.method static synthetic asOrderDetailRows$default(Ljava/util/List;Ljava/util/Map;Lcom/squareup/ui/orderhub/master/Filter;ILjava/lang/Object;)Lcom/squareup/cycler/DataSource;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 819
    check-cast p2, Lcom/squareup/ui/orderhub/master/Filter;

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinatorKt;->asOrderDetailRows(Ljava/util/List;Ljava/util/Map;Lcom/squareup/ui/orderhub/master/Filter;)Lcom/squareup/cycler/DataSource;

    move-result-object p0

    return-object p0
.end method

.method private static final getTitle(Lcom/squareup/orders/model/Order$Fulfillment;Lcom/squareup/util/Res;Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;Lorg/threeten/bp/ZonedDateTime;)Ljava/lang/String;
    .locals 3

    .line 849
    iget-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinatorKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_6

    const/4 v2, 0x2

    if-eq v0, v2, :cond_5

    const/4 v2, 0x3

    if-eq v0, v2, :cond_3

    const/4 v2, 0x4

    if-eq v0, v2, :cond_1

    goto :goto_0

    .line 855
    :cond_1
    iget-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->deliver_at:Ljava/lang/String;

    :cond_2
    invoke-virtual {p2, v1, p3}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->formatTitle(Ljava/lang/String;Lorg/threeten/bp/ZonedDateTime;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 853
    :cond_3
    iget-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    if-eqz v0, :cond_4

    iget-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->pickup_at:Ljava/lang/String;

    :cond_4
    invoke-virtual {p2, v1, p3}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->formatTitle(Ljava/lang/String;Lorg/threeten/bp/ZonedDateTime;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 851
    :cond_5
    iget-object p2, p0, Lcom/squareup/orders/model/Order$Fulfillment;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    if-eqz p2, :cond_8

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipping_type:Ljava/lang/String;

    goto :goto_0

    .line 850
    :cond_6
    iget-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    if-eqz v0, :cond_7

    iget-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->pickup_at:Ljava/lang/String;

    :cond_7
    invoke-virtual {p2, v1, p3}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->formatTitle(Ljava/lang/String;Lorg/threeten/bp/ZonedDateTime;)Ljava/lang/String;

    move-result-object v1

    :cond_8
    :goto_0
    if-nez v1, :cond_9

    .line 860
    invoke-static {p0}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getTypeNameShortRes(Lcom/squareup/orders/model/Order$Fulfillment;)I

    move-result p0

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 862
    :cond_9
    sget p2, Lcom/squareup/orderhub/applet/R$string;->orderhub_detail_order_title_format:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 863
    invoke-static {p0}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getTypeNameShortRes(Lcom/squareup/orders/model/Order$Fulfillment;)I

    move-result p0

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    const-string p1, "fulfillment_type"

    invoke-virtual {p2, p1, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 864
    check-cast v1, Ljava/lang/CharSequence;

    const-string p1, "info"

    invoke-virtual {p0, p1, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 865
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 866
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    :goto_1
    return-object p0
.end method
