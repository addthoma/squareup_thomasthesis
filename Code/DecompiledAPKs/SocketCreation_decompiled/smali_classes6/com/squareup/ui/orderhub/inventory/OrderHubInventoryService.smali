.class public final Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;
.super Ljava/lang/Object;
.source "OrderHubInventoryService.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubInventoryService.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubInventoryService.kt\ncom/squareup/ui/orderhub/inventory/OrderHubInventoryService\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n+ 4 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n*L\n1#1,231:1\n1265#2,12:232\n704#2:244\n777#2,2:245\n1642#2,2:247\n1412#2,9:259\n1642#2,2:268\n1421#2:270\n1143#2,4:280\n704#2:284\n777#2,2:285\n1084#2,2:287\n1158#2,4:289\n501#3:249\n486#3,6:250\n501#3:271\n486#3,6:272\n443#3:278\n389#3:279\n92#4,3:256\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubInventoryService.kt\ncom/squareup/ui/orderhub/inventory/OrderHubInventoryService\n*L\n109#1,12:232\n110#1:244\n110#1,2:245\n111#1,2:247\n195#1,9:259\n195#1,2:268\n195#1:270\n209#1,4:280\n215#1:284\n215#1,2:285\n216#1,2:287\n216#1,4:289\n139#1:249\n139#1,6:250\n208#1:271\n208#1,6:272\n209#1:278\n209#1:279\n140#1,3:256\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0082\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\"\n\u0002\u0008\u0002\n\u0002\u0010%\n\u0002\u0008\u0002\u0008\u0007\u0018\u0000 12\u00020\u0001:\u00011B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J<\u0010\u0012\u001a\u00020\u00132\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0014\u001a\u00020\n2\u0006\u0010\u0015\u001a\u00020\u000f2\u0006\u0010\u0016\u001a\u00020\u00172\u0008\u0010\u0018\u001a\u0004\u0018\u00010\n2\u0008\u0010\u0019\u001a\u0004\u0018\u00010\nH\u0002Jh\u0010\u001a\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001d0\u001c0\u001b2\u0006\u0010\u001e\u001a\u00020\u001f2\n\u0008\u0002\u0010 \u001a\u0004\u0018\u00010!2\u0016\u0010\"\u001a\u0012\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000f0#j\u0002`$2\u0016\u0010%\u001a\u0012\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020&0#j\u0002`\'2\u0006\u0010(\u001a\u00020&2\u0006\u0010)\u001a\u00020\nH\u0002JN\u0010\u001a\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001d0\u001c0\u001b2\u0006\u0010\u001e\u001a\u00020\u001f2\n\u0008\u0002\u0010 \u001a\u0004\u0018\u00010!2\u0016\u0010\"\u001a\u0012\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000f0#j\u0002`$2\u0006\u0010(\u001a\u00020&2\u0006\u0010)\u001a\u00020\nJ$\u0010*\u001a\u00020&2\u0006\u0010+\u001a\u00020\n2\u0012\u0010%\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020&0#H\u0002J\u0012\u0010,\u001a\u0008\u0012\u0004\u0012\u00020\n0-*\u00020\u001fH\u0002J\u0018\u0010.\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0#*\u00020\u001fH\u0002J0\u0010/\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000f00*\u0012\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000f0#j\u0002`$2\u0006\u0010\u001e\u001a\u00020\u001fH\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u0004\u0018\u00010\n*\u00020\u000b8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\rR\u0018\u0010\u000e\u001a\u00020\u000f*\u00020\u000b8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0011\u00a8\u00062"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;",
        "",
        "inventoryService",
        "Lcom/squareup/server/inventory/InventoryService;",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/server/inventory/InventoryService;Lcom/squareup/cogs/Cogs;Lcom/squareup/settings/server/Features;)V",
        "catalogVariationId",
        "",
        "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;",
        "getCatalogVariationId",
        "(Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;)Ljava/lang/String;",
        "refundedQuantity",
        "Ljava/math/BigDecimal;",
        "getRefundedQuantity",
        "(Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;)Ljava/math/BigDecimal;",
        "buildInventoryAdjustment",
        "Lcom/squareup/protos/client/InventoryAdjustment;",
        "unitToken",
        "quantity",
        "reason",
        "Lcom/squareup/protos/client/InventoryAdjustmentReason;",
        "refundBillServerToken",
        "billServerToken",
        "issueInventoryAdjustmentRequest",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/BatchAdjustVariationInventoryResponse;",
        "order",
        "Lcom/squareup/orders/model/Order;",
        "returnBill",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "selectedItemQuantities",
        "",
        "Lcom/squareup/ui/orderhub/inventory/LineItemQuantitiesByUid;",
        "catalogVariationTracking",
        "",
        "Lcom/squareup/ui/orderhub/inventory/InventoryTrackingByCatalogVariationId;",
        "isRestock",
        "idempotencyKey",
        "trackingInventory",
        "variationId",
        "catalogVariationIds",
        "",
        "catalogVariationIdsByLineItemUid",
        "quantitiesByCatalogId",
        "",
        "Companion",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$Companion;

.field private static final FAKE_SUCCESS:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess<",
            "Lcom/squareup/protos/client/BatchAdjustVariationInventoryResponse;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final inventoryService:Lcom/squareup/server/inventory/InventoryService;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->Companion:Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$Companion;

    .line 228
    new-instance v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    new-instance v1, Lcom/squareup/protos/client/BatchAdjustVariationInventoryResponse$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryResponse$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryResponse$Builder;->build()Lcom/squareup/protos/client/BatchAdjustVariationInventoryResponse;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->FAKE_SUCCESS:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/inventory/InventoryService;Lcom/squareup/cogs/Cogs;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "inventoryService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cogs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->inventoryService:Lcom/squareup/server/inventory/InventoryService;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->cogs:Lcom/squareup/cogs/Cogs;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static final synthetic access$catalogVariationIds(Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;Lcom/squareup/orders/model/Order;)Ljava/util/Set;
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->catalogVariationIds(Lcom/squareup/orders/model/Order;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$issueInventoryAdjustmentRequest(Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;Ljava/util/Map;ZLjava/lang/String;)Lio/reactivex/Single;
    .locals 0

    .line 35
    invoke-direct/range {p0 .. p6}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->issueInventoryAdjustmentRequest(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;Ljava/util/Map;ZLjava/lang/String;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private final buildInventoryAdjustment(Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Lcom/squareup/protos/client/InventoryAdjustmentReason;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment;
    .locals 1

    .line 174
    new-instance v0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;-><init>()V

    .line 175
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->variation_token(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    move-result-object p1

    .line 176
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    move-result-object p1

    .line 177
    invoke-virtual {p3}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->adjust_quantity_decimal(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    move-result-object p1

    .line 178
    invoke-virtual {p1, p4}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->reason(Lcom/squareup/protos/client/InventoryAdjustmentReason;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    move-result-object p1

    .line 179
    invoke-virtual {p1, p5}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->refund_bill_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    move-result-object p1

    .line 180
    invoke-virtual {p1, p6}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->bill_token(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    move-result-object p1

    .line 181
    invoke-virtual {p1}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->build()Lcom/squareup/protos/client/InventoryAdjustment;

    move-result-object p1

    const-string p2, "InventoryAdjustment.Buil\u2026erToken)\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final catalogVariationIds(Lcom/squareup/orders/model/Order;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            ")",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 195
    iget-object p1, p1, Lcom/squareup/orders/model/Order;->line_items:Ljava/util/List;

    const-string v0, "line_items"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 259
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 268
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 267
    check-cast v1, Lcom/squareup/orders/model/Order$LineItem;

    .line 195
    iget-object v1, v1, Lcom/squareup/orders/model/Order$LineItem;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 267
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 270
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 196
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object p1

    return-object p1
.end method

.method private final catalogVariationIdsByLineItemUid(Lcom/squareup/orders/model/Order;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 214
    iget-object p1, p1, Lcom/squareup/orders/model/Order;->line_items:Ljava/util/List;

    const-string v0, "line_items"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 284
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 285
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/orders/model/Order$LineItem;

    .line 215
    iget-object v2, v2, Lcom/squareup/orders/model/Order$LineItem;->catalog_object_id:Ljava/lang/String;

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 286
    :cond_2
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    const/16 p1, 0xa

    .line 287
    invoke-static {v0, p1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result p1

    invoke-static {p1}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result p1

    const/16 v1, 0x10

    invoke-static {p1, v1}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result p1

    .line 288
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1, p1}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v1, Ljava/util/Map;

    .line 289
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 290
    check-cast v0, Lcom/squareup/orders/model/Order$LineItem;

    .line 216
    iget-object v2, v0, Lcom/squareup/orders/model/Order$LineItem;->uid:Ljava/lang/String;

    iget-object v0, v0, Lcom/squareup/orders/model/Order$LineItem;->catalog_object_id:Ljava/lang/String;

    invoke-static {v2, v0}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    return-object v1
.end method

.method private final getCatalogVariationId(Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;)Ljava/lang/String;
    .locals 0

    .line 224
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;->item_variation:Lcom/squareup/api/items/ItemVariation;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/api/items/ItemVariation;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/api/items/MerchantCatalogObjectReference;->catalog_object_token:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final getRefundedQuantity(Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;)Ljava/math/BigDecimal;
    .locals 1

    .line 220
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    const-string v0, "itemization.quantity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private final issueInventoryAdjustmentRequest(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;Ljava/util/Map;ZLjava/lang/String;)Lio/reactivex/Single;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;Z",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/BatchAdjustVariationInventoryResponse;",
            ">;>;"
        }
    .end annotation

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p4

    move-object/from16 v0, p3

    .line 101
    invoke-direct {v7, v0, v8}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->quantitiesByCatalogId(Ljava/util/Map;Lcom/squareup/orders/model/Order;)Ljava/util/Map;

    move-result-object v11

    if-eqz p5, :cond_0

    .line 102
    sget-object v0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->RETURNED:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->CANCELED_SALE:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    :goto_0
    move-object v12, v0

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v13, v0

    check-cast v13, Ljava/util/List;

    if-eqz v9, :cond_1

    .line 108
    iget-object v0, v9, Lcom/squareup/billhistory/model/BillHistory;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->return_line_items:Ljava/util/List;

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_2
    check-cast v0, Ljava/lang/Iterable;

    .line 232
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 239
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 240
    check-cast v2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    .line 109
    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_itemization:Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 241
    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_3

    .line 243
    :cond_3
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 244
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 245
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/16 v16, 0x0

    const-wide/16 v5, 0x0

    const-string v4, "it"

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    .line 110
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v7, v3}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->getRefundedQuantity(Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-static {v4, v5, v6}, Lcom/squareup/util/BigDecimals;->greaterThan(Ljava/math/BigDecimal;J)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-direct {v7, v3}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->getCatalogVariationId(Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    const/4 v15, 0x1

    goto :goto_5

    :cond_5
    const/4 v15, 0x0

    :goto_5
    if-eqz v15, :cond_4

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 246
    :cond_6
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 247
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_6
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const-string v3, "order.location_id"

    if-eqz v0, :cond_b

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    .line 112
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v7, v0}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->getRefundedQuantity(Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 113
    invoke-direct {v7, v0}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->getCatalogVariationId(Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "Required value was null."

    if-eqz v1, :cond_a

    .line 117
    invoke-direct {v7, v1, v10}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->trackingInventory(Ljava/lang/String;Ljava/util/Map;)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 118
    move-object v14, v13

    check-cast v14, Ljava/util/Collection;

    .line 120
    iget-object v5, v8, Lcom/squareup/orders/model/Order;->location_id:Ljava/lang/String;

    invoke-static {v5, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v9, :cond_7

    .line 123
    iget-object v0, v9, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    const-string v3, "requireNotNull(returnBill).id"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistoryId;->getId()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    iget-object v6, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-object v3, v1

    move-object/from16 v20, v2

    move-object v2, v5

    move-object v5, v3

    move-object/from16 v3, v20

    move-object/from16 v21, v4

    move-object v4, v12

    move-object v15, v5

    const-wide/16 v8, 0x0

    move-object v5, v6

    move-object/from16 v6, v19

    .line 118
    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->buildInventoryAdjustment(Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Lcom/squareup/protos/client/InventoryAdjustmentReason;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment;

    move-result-object v0

    invoke-interface {v14, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 123
    :cond_7
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_8
    move-object v15, v1

    move-object/from16 v20, v2

    move-object/from16 v21, v4

    move-wide v8, v5

    .line 129
    :goto_7
    invoke-interface {v11, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/math/BigDecimal;

    if-eqz v0, :cond_9

    move-object/from16 v1, v20

    .line 132
    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    const-string v1, "this.subtract(other)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    invoke-interface {v11, v15, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    move-wide v5, v8

    move-object/from16 v4, v21

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    goto/16 :goto_6

    .line 113
    :cond_a
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_b
    move-wide v8, v5

    .line 249
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 250
    invoke-interface {v11}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_c
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 139
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/math/BigDecimal;

    invoke-static {v4, v8, v9}, Lcom/squareup/util/BigDecimals;->greaterThan(Ljava/math/BigDecimal;J)Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {v7, v4, v10}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->trackingInventory(Ljava/lang/String;Ljava/util/Map;)Z

    move-result v4

    if-eqz v4, :cond_d

    const/4 v4, 0x1

    goto :goto_9

    :cond_d
    const/4 v4, 0x0

    :goto_9
    if-eqz v4, :cond_c

    .line 252
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    .line 256
    :cond_e
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_a
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 257
    move-object v9, v13

    check-cast v9, Ljava/util/Collection;

    .line 142
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object/from16 v10, p1

    .line 143
    iget-object v2, v10, Lcom/squareup/orders/model/Order;->location_id:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/math/BigDecimal;

    const/4 v5, 0x0

    if-eqz p5, :cond_f

    const/4 v6, 0x0

    goto :goto_b

    .line 147
    :cond_f
    invoke-static/range {p1 .. p1}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getBillServerToken(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    :goto_b
    move-object/from16 v0, p0

    move-object v11, v3

    move-object v3, v4

    move-object v4, v12

    .line 141
    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->buildInventoryAdjustment(Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Lcom/squareup/protos/client/InventoryAdjustmentReason;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment;

    move-result-object v0

    .line 148
    invoke-interface {v9, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-object v3, v11

    goto :goto_a

    .line 258
    :cond_10
    move-object v0, v13

    check-cast v0, Ljava/util/Collection;

    .line 153
    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 154
    sget-object v0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->FAKE_SUCCESS:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "Single.just(FAKE_SUCCESS)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 157
    :cond_11
    new-instance v0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;-><init>()V

    move-object/from16 v1, p6

    .line 158
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;->idempotency_token(Ljava/lang/String;)Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;

    move-result-object v0

    .line 159
    invoke-virtual {v0, v13}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;->adjustments(Ljava/util/List;)Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;

    move-result-object v0

    .line 160
    invoke-virtual {v0}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;->build()Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;

    move-result-object v0

    .line 161
    iget-object v1, v7, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->inventoryService:Lcom/squareup/server/inventory/InventoryService;

    .line 162
    invoke-interface {v1, v0}, Lcom/squareup/server/inventory/InventoryService;->batchAdjust(Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 163
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method static synthetic issueInventoryAdjustmentRequest$default(Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;Ljava/util/Map;ZLjava/lang/String;ILjava/lang/Object;)Lio/reactivex/Single;
    .locals 7

    and-int/lit8 p7, p7, 0x2

    if-eqz p7, :cond_0

    const/4 p2, 0x0

    .line 94
    check-cast p2, Lcom/squareup/billhistory/model/BillHistory;

    :cond_0
    move-object v2, p2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->issueInventoryAdjustmentRequest(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;Ljava/util/Map;ZLjava/lang/String;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic issueInventoryAdjustmentRequest$default(Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;ZLjava/lang/String;ILjava/lang/Object;)Lio/reactivex/Single;
    .locals 6

    and-int/lit8 p6, p6, 0x2

    if-eqz p6, :cond_0

    const/4 p2, 0x0

    .line 57
    check-cast p2, Lcom/squareup/billhistory/model/BillHistory;

    :cond_0
    move-object v2, p2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->issueInventoryAdjustmentRequest(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;ZLjava/lang/String;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private final quantitiesByCatalogId(Ljava/util/Map;Lcom/squareup/orders/model/Order;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/math/BigDecimal;",
            ">;",
            "Lcom/squareup/orders/model/Order;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation

    .line 205
    invoke-direct {p0, p2}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->catalogVariationIdsByLineItemUid(Lcom/squareup/orders/model/Order;)Ljava/util/Map;

    move-result-object p2

    .line 271
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 272
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 208
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 274
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 278
    :cond_1
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    invoke-static {v1}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v1

    invoke-direct {p1, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast p1, Ljava/util/Map;

    .line 279
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 280
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 281
    check-cast v1, Ljava/util/Map$Entry;

    .line 209
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-static {p2, v2}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 279
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 210
    :cond_2
    invoke-static {p1}, Lkotlin/collections/MapsKt;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final trackingInventory(Ljava/lang/String;Ljava/util/Map;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)Z"
        }
    .end annotation

    .line 191
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    return p1
.end method


# virtual methods
.method public final issueInventoryAdjustmentRequest(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;ZLjava/lang/String;)Lio/reactivex/Single;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/math/BigDecimal;",
            ">;Z",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/BatchAdjustVariationInventoryResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedItemQuantities"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "idempotencyKey"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ADJUST_INVENTORY_AFTER_ORDER_CANCELATION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    sget-object p1, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->FAKE_SUCCESS:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.just(FAKE_SUCCESS)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->cogs:Lcom/squareup/cogs/Cogs;

    .line 72
    new-instance v1, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$1;-><init>(Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;Lcom/squareup/orders/model/Order;)V

    check-cast v1, Lcom/squareup/shared/catalog/CatalogTask;

    invoke-interface {v0, v1}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object v0

    const-string v1, "cogs\n        .asSingle {\u2026s()\n          )\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object v0

    .line 78
    new-instance v8, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$2;

    move-object v1, v8

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$2;-><init>(Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;ZLjava/lang/String;)V

    check-cast v8, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v8}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "cogs\n        .asSingle {\u2026Key\n          )\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
