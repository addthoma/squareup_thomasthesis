.class public abstract Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "SearchWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;,
        Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;",
        "Lcom/squareup/ui/orderhub/search/SearchState;",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008&\u0018\u00002\u001a\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0001:\u0002\u0007\u0008B\u0005\u00a2\u0006\u0002\u0010\u0006\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;",
        "Lcom/squareup/ui/orderhub/search/SearchState;",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;",
        "()V",
        "Props",
        "SearchRendering",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    return-void
.end method
