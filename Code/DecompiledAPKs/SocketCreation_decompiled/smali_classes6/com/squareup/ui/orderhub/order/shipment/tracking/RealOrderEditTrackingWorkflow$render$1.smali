.class final Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderEditTrackingWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow;->render(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/connectivity/InternetState;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult;",
        "it",
        "Lcom/squareup/connectivity/InternetState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$1;->$state:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/connectivity/InternetState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/connectivity/InternetState;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    sget-object v0, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 79
    :goto_0
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$1;->$state:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;

    const/4 v3, 0x0

    xor-int/lit8 v4, p1, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0xfd

    const/4 v12, 0x0

    invoke-static/range {v2 .. v12}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->copy$default(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;Lcom/squareup/orders/model/Order;ZZZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/ordermanagerdata/TrackingInfo;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;

    move-result-object p1

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 40
    check-cast p1, Lcom/squareup/connectivity/InternetState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$1;->invoke(Lcom/squareup/connectivity/InternetState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
