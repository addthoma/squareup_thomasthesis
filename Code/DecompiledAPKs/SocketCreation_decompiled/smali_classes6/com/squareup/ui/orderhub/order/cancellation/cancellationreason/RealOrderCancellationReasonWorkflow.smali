.class public final Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealOrderCancellationReasonWorkflow.kt"

# interfaces
.implements Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonWorkflow;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/orderhub/OrderHubAppletScope;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderCancellationReasonWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderCancellationReasonWorkflow.kt\ncom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 4 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,86:1\n41#2:87\n56#2,2:88\n276#3:90\n149#4,5:91\n*E\n*S KotlinDebug\n*F\n+ 1 RealOrderCancellationReasonWorkflow.kt\ncom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow\n*L\n64#1:87\n64#1,2:88\n64#1:90\n82#1,5:91\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002B\u000f\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u001a\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u00032\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0016J \u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u0004H\u0016JN\u0010\u0016\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u00042\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0018H\u0016J\u0010\u0010\u0019\u001a\u00020\u00112\u0006\u0010\u0015\u001a\u00020\u0004H\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow;",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "connectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "(Lcom/squareup/connectivity/ConnectivityMonitor;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "onPropsChanged",
        "old",
        "new",
        "state",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;


# direct methods
.method public constructor <init>(Lcom/squareup/connectivity/ConnectivityMonitor;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "connectivityMonitor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;
    .locals 0

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-static {p1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInputKt;->getInitialState(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;)Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow;->initialState(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;

    move-result-object p1

    return-object p1
.end method

.method public onPropsChanged(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;)Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;
    .locals 1

    const-string v0, "old"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "new"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 54
    :cond_0
    invoke-static {p2}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInputKt;->getInitialState(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;)Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;

    move-result-object p3

    :goto_0
    return-object p3
.end method

.method public bridge synthetic onPropsChanged(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;

    check-cast p2, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;

    check-cast p3, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow;->onPropsChanged(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;)Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;

    check-cast p2, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow;->render(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;",
            "-",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {p1}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "connectivityMonitor.internetState()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    sget-object v0, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    const-string v0, "this.toFlowable(BUFFER)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_0

    .line 89
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 90
    const-class v0, Lcom/squareup/connectivity/InternetState;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/Worker;

    const/4 v4, 0x0

    .line 65
    new-instance p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow$render$1;

    invoke-direct {p1, p2}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow$render$1;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, p3

    .line 63
    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 70
    sget-object p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow$render$eventHandler$1;->INSTANCE:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow$render$eventHandler$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, p1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 78
    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 79
    new-instance p3, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen;

    invoke-direct {p3, p2, p1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;Lkotlin/jvm/functions/Function1;)V

    check-cast p3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 92
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 93
    const-class p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonScreen;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    const-string p2, ""

    invoke-static {p1, p2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p1

    .line 94
    sget-object p2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p2

    .line 92
    invoke-direct {v1, p1, p3, p2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1e

    .line 78
    invoke-static/range {v0 .. v7}, Lcom/squareup/container/PosLayering$Companion;->fullStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 89
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/RealOrderCancellationReasonWorkflow;->snapshotState(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
