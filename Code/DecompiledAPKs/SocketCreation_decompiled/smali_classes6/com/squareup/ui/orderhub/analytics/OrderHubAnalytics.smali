.class public final Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;
.super Ljava/lang/Object;
.source "OrderHubAnalytics.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;,
        Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;,
        Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderEventWithId;,
        Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericActionEvent;,
        Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderSourceFilterSelectedEvent;,
        Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericTapEvent;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubAnalytics.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubAnalytics.kt\ncom/squareup/ui/orderhub/analytics/OrderHubAnalytics\n*L\n1#1,284:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0014\u0008\u0007\u0018\u00002\u00020\u0001:\u0006*+,-./B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001d\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0000\u00a2\u0006\u0002\u0008\u0011J\u001d\u0010\u0012\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u0014H\u0000\u00a2\u0006\u0002\u0008\u0015J\u001d\u0010\u0016\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0017\u001a\u00020\u0018H\u0000\u00a2\u0006\u0002\u0008\u0019J\u0015\u0010\u001a\u001a\u00020\u000c2\u0006\u0010\u001b\u001a\u00020\u001cH\u0000\u00a2\u0006\u0002\u0008\u001dJ\u0015\u0010\u001e\u001a\u00020\u000c2\u0006\u0010\u001f\u001a\u00020\u0014H\u0000\u00a2\u0006\u0002\u0008 J\u0015\u0010!\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0000\u00a2\u0006\u0002\u0008\"J\u001d\u0010#\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010$\u001a\u00020\u0014H\u0000\u00a2\u0006\u0002\u0008%J\u0015\u0010&\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0000\u00a2\u0006\u0002\u0008\'J\r\u0010(\u001a\u00020\u000cH\u0000\u00a2\u0006\u0002\u0008)R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u00060"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
        "",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;)V",
        "getAnalytics",
        "()Lcom/squareup/analytics/Analytics;",
        "getRes",
        "()Lcom/squareup/util/Res;",
        "logAction",
        "",
        "orderId",
        "",
        "action",
        "Lcom/squareup/protos/client/orders/Action$Type;",
        "logAction$orderhub_applet_release",
        "logCancellationDecision",
        "continueToRefund",
        "",
        "logCancellationDecision$orderhub_applet_release",
        "logEvent",
        "actionName",
        "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;",
        "logEvent$orderhub_applet_release",
        "logFilterSelection",
        "filter",
        "Lcom/squareup/ui/orderhub/master/Filter;",
        "logFilterSelection$orderhub_applet_release",
        "logNewOrderNotificationDecision",
        "viewNow",
        "logNewOrderNotificationDecision$orderhub_applet_release",
        "logOrderMarkedAsPrinted",
        "logOrderMarkedAsPrinted$orderhub_applet_release",
        "logPrintingEvent",
        "autoPrint",
        "logPrintingEvent$orderhub_applet_release",
        "logSkippedAutomaticPrinting",
        "logSkippedAutomaticPrinting$orderhub_applet_release",
        "logSkippedAutomaticPrintingInitialAttempt",
        "logSkippedAutomaticPrintingInitialAttempt$orderhub_applet_release",
        "GenericActionEvent",
        "GenericTapEvent",
        "OrderEventWithId",
        "OrderHubActionName",
        "OrderHubTapName",
        "OrderSourceFilterSelectedEvent",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public final getAnalytics()Lcom/squareup/analytics/Analytics;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    return-object v0
.end method

.method public final getRes()Lcom/squareup/util/Res;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->res:Lcom/squareup/util/Res;

    return-object v0
.end method

.method public final logAction$orderhub_applet_release(Ljava/lang/String;Lcom/squareup/protos/client/orders/Action$Type;)V
    .locals 2

    const-string v0, "orderId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "action"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 268
    sget-object v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p2}, Lcom/squareup/protos/client/orders/Action$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 280
    :pswitch_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unsupported action "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2

    .line 279
    :pswitch_1
    iget-object p2, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderEventWithId;

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_MARK_SHIPPED:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-direct {v0, p1, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderEventWithId;-><init>(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {p2, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 278
    :pswitch_2
    iget-object p2, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderEventWithId;

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_MARK_READY:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-direct {v0, p1, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderEventWithId;-><init>(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {p2, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 275
    :pswitch_3
    iget-object p2, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 276
    new-instance v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderEventWithId;

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_MARK_PICKED_UP:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-direct {v0, p1, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderEventWithId;-><init>(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 275
    invoke-interface {p2, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 272
    :pswitch_4
    iget-object p2, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 273
    new-instance v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderEventWithId;

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_MARK_IN_PROGRESS:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-direct {v0, p1, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderEventWithId;-><init>(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 272
    invoke-interface {p2, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 271
    :pswitch_5
    iget-object p2, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderEventWithId;

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_COMPLETE:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-direct {v0, p1, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderEventWithId;-><init>(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {p2, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 270
    :pswitch_6
    iget-object p2, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderEventWithId;

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_CANCEL:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-direct {v0, p1, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderEventWithId;-><init>(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {p2, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 269
    :pswitch_7
    iget-object p2, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderEventWithId;

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_ACCEPT:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-direct {v0, p1, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderEventWithId;-><init>(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {p2, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final logCancellationDecision$orderhub_applet_release(Ljava/lang/String;Z)V
    .locals 1

    const-string v0, "orderId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 204
    sget-object p2, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_CANCEL_CONTINUE_TO_REFUND:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logEvent$orderhub_applet_release(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    goto :goto_0

    .line 206
    :cond_0
    sget-object p2, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_CANCEL_SKIP_REFUND:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logEvent$orderhub_applet_release(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    :goto_0
    return-void
.end method

.method public final logEvent$orderhub_applet_release(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V
    .locals 2

    const-string v0, "orderId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "actionName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderEventWithId;

    invoke-direct {v1, p1, p2}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderEventWithId;-><init>(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public final logFilterSelection$orderhub_applet_release(Lcom/squareup/ui/orderhub/master/Filter;)V
    .locals 3

    const-string v0, "filter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 228
    instance-of v0, p1, Lcom/squareup/ui/orderhub/master/Filter$Source;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 229
    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderSourceFilterSelectedEvent;

    iget-object v2, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1, v2}, Lcom/squareup/ui/orderhub/master/Filter;->getFilterName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderSourceFilterSelectedEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 228
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto/16 :goto_0

    .line 231
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/orderhub/master/Filter$Status;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/ui/orderhub/master/Filter$Status;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/master/Filter$Status;->getOrderGroup()Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/protos/client/orders/OrderGroup;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 242
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "unsupported order group "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/master/Filter$Status;->getOrderGroup()Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 241
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 238
    :pswitch_1
    iget-object p1, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 239
    new-instance v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericTapEvent;

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;->ORDER_HUB_FILTER_SELECTED_STATUS_COMPLETED:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericTapEvent;-><init>(Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 238
    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto/16 :goto_0

    .line 235
    :pswitch_2
    iget-object p1, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 236
    new-instance v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericTapEvent;

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;->ORDER_HUB_FILTER_SELECTED_STATUS_UPCOMING:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericTapEvent;-><init>(Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 235
    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto/16 :goto_0

    .line 232
    :pswitch_3
    iget-object p1, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 233
    new-instance v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericTapEvent;

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;->ORDER_HUB_FILTER_SELECTED_STATUS_ALL_ACTIVE:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericTapEvent;-><init>(Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 232
    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto/16 :goto_0

    .line 245
    :cond_1
    instance-of v0, p1, Lcom/squareup/ui/orderhub/master/Filter$Type;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/squareup/ui/orderhub/master/Filter$Type;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/master/Filter$Type;->getType()Lcom/squareup/orders/model/Order$FulfillmentType;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 255
    :pswitch_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "unsupported fulfillment type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/master/Filter$Type;->getType()Lcom/squareup/orders/model/Order$FulfillmentType;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 254
    :pswitch_5
    iget-object p1, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericTapEvent;

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;->ORDER_HUB_FILTER_SELECTED_TYPE_SHIPMENT:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericTapEvent;-><init>(Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 253
    :pswitch_6
    iget-object p1, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericTapEvent;

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;->ORDER_HUB_FILTER_SELECTED_TYPE_PICKUP:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericTapEvent;-><init>(Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 250
    :pswitch_7
    iget-object p1, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 251
    new-instance v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericTapEvent;

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;->ORDER_HUB_FILTER_SELECTED_TYPE_MANAGED_DELIVERY:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericTapEvent;-><init>(Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 250
    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 249
    :pswitch_8
    iget-object p1, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericTapEvent;

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;->ORDER_HUB_FILTER_SELECTED_TYPE_DIGITAL:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericTapEvent;-><init>(Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 246
    :pswitch_9
    iget-object p1, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 247
    new-instance v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericTapEvent;

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;->ORDER_HUB_FILTER_SELECTED_TYPE_DELIVERY:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericTapEvent;-><init>(Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubTapName;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 246
    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :cond_2
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public final logNewOrderNotificationDecision$orderhub_applet_release(Z)V
    .locals 2

    if-eqz p1, :cond_0

    .line 215
    iget-object p1, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericActionEvent;

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_NEW_ORDER_NOTIFICATION_VIEW_NOW:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericActionEvent;-><init>(Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 217
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericActionEvent;

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_NEW_ORDER_NOTIFICATION_VIEW_LATER:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericActionEvent;-><init>(Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :goto_0
    return-void
.end method

.method public final logOrderMarkedAsPrinted$orderhub_applet_release(Ljava/lang/String;)V
    .locals 1

    const-string v0, "orderId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    sget-object v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_MARKED_ORDER_AS_PRINTED:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logEvent$orderhub_applet_release(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    return-void
.end method

.method public final logPrintingEvent$orderhub_applet_release(Ljava/lang/String;Z)V
    .locals 1

    const-string v0, "orderId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 188
    sget-object p2, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_AUTOMATIC_PRINT:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    goto :goto_0

    .line 190
    :cond_0
    sget-object p2, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_MANUAL_PRINT:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    .line 192
    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logEvent$orderhub_applet_release(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    return-void
.end method

.method public final logSkippedAutomaticPrinting$orderhub_applet_release(Ljava/lang/String;)V
    .locals 1

    const-string v0, "orderId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    sget-object v0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_SKIPPED_AUTOMATIC_PRINT:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logEvent$orderhub_applet_release(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    return-void
.end method

.method public final logSkippedAutomaticPrintingInitialAttempt$orderhub_applet_release()V
    .locals 3

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericActionEvent;

    sget-object v2, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_SKIPPED_AUTOMATIC_PRINT_INITIAL_ATTEMPT:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-direct {v1, v2}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$GenericActionEvent;-><init>(Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method
