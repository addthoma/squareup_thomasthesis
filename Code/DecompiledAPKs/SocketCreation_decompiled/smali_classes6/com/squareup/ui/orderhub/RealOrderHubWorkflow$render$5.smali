.class final Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$5;
.super Ljava/lang/Object;
.source "RealOrderHubWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->render(Lcom/squareup/ui/orderhub/OrderHubWorkflowInput;Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/ordermanagerdata/ResultState<",
        "+",
        "Ljava/util/List<",
        "+",
        "Lcom/squareup/orders/model/Order;",
        ">;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012&\u0010\u0002\u001a\"\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u0004 \u0006*\u0010\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/ordermanagerdata/ResultState;",
        "",
        "Lcom/squareup/orders/model/Order;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$5;->this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/ordermanagerdata/ResultState;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "+",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;)V"
        }
    .end annotation

    .line 218
    iget-object p1, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$5;->this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->access$getOrderRepository$p(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;)Lcom/squareup/ordermanagerdata/OrderRepository;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ordermanagerdata/OrderRepository;->markAllOrdersAsKnown()Lio/reactivex/Single;

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 77
    check-cast p1, Lcom/squareup/ordermanagerdata/ResultState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$5;->accept(Lcom/squareup/ordermanagerdata/ResultState;)V

    return-void
.end method
