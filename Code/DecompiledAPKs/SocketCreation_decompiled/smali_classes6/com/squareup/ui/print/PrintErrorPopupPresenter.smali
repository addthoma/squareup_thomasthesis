.class public Lcom/squareup/ui/print/PrintErrorPopupPresenter;
.super Lcom/squareup/mortar/PopupPresenter;
.source "PrintErrorPopupPresenter.java"

# interfaces
.implements Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;
.implements Lcom/squareup/print/HardwarePrinterTracker$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;,
        Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/ui/Showing;",
        "Ljava/lang/Void;",
        ">;",
        "Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;",
        "Lcom/squareup/print/HardwarePrinterTracker$Listener;"
    }
.end annotation


# static fields
.field private static final DELAY_UNTIL_SHOWING_MS:J = 0x3e8L


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final bucketedErrorRows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

.field final discardPrintJobsPresenter:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/register/widgets/Confirmation;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

.field private final isTablet:Z

.field private final mainContainer:Lcom/squareup/ui/main/PosContainer;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final printSpooler:Lcom/squareup/print/PrintSpooler;

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private final res:Lcom/squareup/util/Res;

.field private final router:Lcom/squareup/print/PrintTargetRouter;

.field private final toastFactory:Lcom/squareup/util/ToastFactory;

.field private final tryShowErrorRunnable:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/squareup/print/PrinterStations;Lcom/squareup/print/PrintSpooler;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/print/PrintTargetRouter;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/ToastFactory;Lcom/squareup/ui/buyer/BuyerFlowStarter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 141
    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    .line 132
    new-instance v0, Lcom/squareup/ui/print/-$$Lambda$PrintErrorPopupPresenter$XqeJQng_Klrt1K1WiU9-q-5F1zA;

    invoke-direct {v0, p0}, Lcom/squareup/ui/print/-$$Lambda$PrintErrorPopupPresenter$XqeJQng_Klrt1K1WiU9-q-5F1zA;-><init>(Lcom/squareup/ui/print/PrintErrorPopupPresenter;)V

    iput-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->tryShowErrorRunnable:Ljava/lang/Runnable;

    .line 133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    .line 142
    iput-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->printerStations:Lcom/squareup/print/PrinterStations;

    .line 143
    iput-object p2, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->printSpooler:Lcom/squareup/print/PrintSpooler;

    .line 144
    iput-object p3, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    .line 145
    iput-object p4, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    .line 146
    iput-object p5, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 147
    iput-object p6, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->router:Lcom/squareup/print/PrintTargetRouter;

    .line 148
    iput-object p7, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->res:Lcom/squareup/util/Res;

    .line 149
    invoke-interface {p8}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->isTablet:Z

    .line 150
    iput-object p9, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 151
    iput-object p10, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->toastFactory:Lcom/squareup/util/ToastFactory;

    .line 152
    iput-object p11, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    .line 154
    new-instance p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter$1;-><init>(Lcom/squareup/ui/print/PrintErrorPopupPresenter;)V

    iput-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->discardPrintJobsPresenter:Lcom/squareup/mortar/PopupPresenter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/print/PrintErrorPopupPresenter;)V
    .locals 0

    .line 59
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->purgeJobsAndCancel()V

    return-void
.end method

.method private buildAffectedStations(I)Ljava/lang/String;
    .locals 3

    .line 565
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 566
    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;

    iget-object p1, p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;->printJobs:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrintJob;

    .line 568
    iget-boolean v2, v1, Lcom/squareup/print/PrintJob;->forTestPrint:Z

    if-nez v2, :cond_0

    .line 569
    invoke-virtual {v1}, Lcom/squareup/print/PrintJob;->getTargetId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 574
    :cond_1
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x0

    return-object p1

    .line 578
    :cond_2
    new-instance p1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 579
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 580
    iget-object v2, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->printerStations:Lcom/squareup/print/PrinterStations;

    invoke-interface {v2, v1}, Lcom/squareup/print/PrinterStations;->getPrinterStationById(Ljava/lang/String;)Lcom/squareup/print/PrinterStation;

    move-result-object v1

    if-nez v1, :cond_3

    goto :goto_1

    .line 585
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/print/PrinterStation;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 587
    :cond_4
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 589
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/utilities/R$string;->list_pattern_long_nonfinal_separator:I

    .line 590
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/ListPhrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/util/ListPhrase;

    move-result-object v0

    .line 591
    invoke-virtual {v0, p1}, Lcom/squareup/util/ListPhrase;->format(Ljava/lang/Iterable;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private canShowPopupRightNow()Z
    .locals 4

    .line 407
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/BuyerFlowStarter;->isShowing()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/BuyerFlowStarter;->isInTransitionToBuyerFlow()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 413
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    const-class v3, Lcom/squareup/ui/permissions/PermissionDeniedScreen;

    invoke-interface {v0, v3}, Lcom/squareup/ui/main/PosContainer;->currentPathIncludes(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const-string v3, "PrintErrorUi_shouldShow"

    aput-object v3, v0, v2

    .line 415
    iget-object v3, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    .line 416
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const-string v1, "[%s] Not showing %d error rows because passcode visible."

    .line 415
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    .line 420
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->isPopupShowing()Z

    move-result v0

    xor-int/2addr v0, v1

    return v0

    :cond_2
    :goto_0
    new-array v0, v1, [Ljava/lang/Object;

    .line 408
    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    .line 409
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const-string v1, "[PrintErrorUi_shouldShow] Not showing %d errors rows, buyer flow visible."

    .line 408
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2
.end method

.method private fetchCurrentFailedJobsAndUpdate()V
    .locals 2

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->printSpooler:Lcom/squareup/print/PrintSpooler;

    new-instance v1, Lcom/squareup/ui/print/-$$Lambda$PrintErrorPopupPresenter$Kek_SeVQxbvSiv2_Yb_aFu1XLCQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/print/-$$Lambda$PrintErrorPopupPresenter$Kek_SeVQxbvSiv2_Yb_aFu1XLCQ;-><init>(Lcom/squareup/ui/print/PrintErrorPopupPresenter;)V

    invoke-virtual {v0, v1}, Lcom/squareup/print/PrintSpooler;->getFailedPrintJobs(Lcom/squareup/print/PrintCallback;)V

    return-void
.end method

.method private finishWhenEmpty()V
    .locals 0

    .line 540
    invoke-virtual {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->dismiss()V

    return-void
.end method

.method private forReprintTapped(Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;)Lcom/squareup/print/PrintErrorReprintEvent;
    .locals 3

    .line 615
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->res:Lcom/squareup/util/Res;

    iget-object v1, p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;->bucket:Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;

    iget-object v1, v1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->errorType:Lcom/squareup/ui/print/PrintErrorType;

    invoke-virtual {v1}, Lcom/squareup/ui/print/PrintErrorType;->getTitle()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 616
    iget-object v1, p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;->printJobs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 617
    iget-object p1, p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;->bucket:Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;

    iget-object p1, p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    .line 619
    new-instance v2, Lcom/squareup/print/PrintErrorReprintEvent;

    invoke-direct {v2, p1, v0, v1}, Lcom/squareup/print/PrintErrorReprintEvent;-><init>(Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;I)V

    return-object v2
.end method

.method private getCachedHardwareInfoForTarget(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/HardwarePrinter$HardwareInfo;
    .locals 1

    .line 600
    iget-boolean v0, p1, Lcom/squareup/print/PrintJob;->forTestPrint:Z

    if-eqz v0, :cond_0

    .line 601
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getTargetId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/print/HardwarePrinterTracker;->getCachedHardwareInfo(Ljava/lang/String;)Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object p1

    return-object p1

    .line 603
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->printerStations:Lcom/squareup/print/PrinterStations;

    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getTargetId()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/print/PrinterStations;->getPrinterStationById(Ljava/lang/String;)Lcom/squareup/print/PrinterStation;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 607
    invoke-virtual {p1}, Lcom/squareup/print/PrinterStation;->hasHardwarePrinterSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 610
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    invoke-virtual {p1}, Lcom/squareup/print/PrinterStation;->getSelectedHardwarePrinterId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/print/HardwarePrinterTracker;->getCachedHardwareInfo(Ljava/lang/String;)Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object p1

    return-object p1

    .line 608
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Station does not have a hardware printer!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 605
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Can\'t resolve the station!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private getTitlePattern(Z)Ljava/lang/String;
    .locals 1

    if-eqz p1, :cond_1

    .line 554
    iget-boolean p1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->isTablet:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/print/popup/error/impl/R$string;->kitchen_printing_error_printer_stations_jobs_tablet:I

    .line 555
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/print/popup/error/impl/R$string;->kitchen_printing_error_printer_stations_jobs_phone:I

    .line 556
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1

    .line 558
    :cond_1
    iget-boolean p1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->isTablet:Z

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/print/popup/error/impl/R$string;->kitchen_printing_error_printer_jobs_tablet:I

    .line 559
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/print/popup/error/impl/R$string;->kitchen_printing_error_printer_jobs_phone:I

    .line 560
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method private isPopupShowing()Z
    .locals 1

    .line 266
    invoke-virtual {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/mortar/Popup;

    invoke-interface {v0}, Lcom/squareup/mortar/Popup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static synthetic lambda$XqeJQng_Klrt1K1WiU9-q-5F1zA(Lcom/squareup/ui/print/PrintErrorPopupPresenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->showPopupAsSoonAsPossible()V

    return-void
.end method

.method private logViewPrintErrorPopupEvent()V
    .locals 4

    .line 367
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    .line 368
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 369
    iget-object v2, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;

    .line 370
    iget-object v3, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->res:Lcom/squareup/util/Res;

    iget-object v2, v2, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;->bucket:Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;

    iget-object v2, v2, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->errorType:Lcom/squareup/ui/print/PrintErrorType;

    invoke-virtual {v2}, Lcom/squareup/ui/print/PrintErrorType;->getTitle()I

    move-result v2

    invoke-interface {v3, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 371
    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 372
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 375
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 376
    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0}, Lcom/squareup/ui/print/PrintErrorPopupEvent;->forViewPrintErrorPopup([Ljava/lang/String;)Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private purgeJobsAndCancel()V
    .locals 6

    .line 516
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 517
    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;

    .line 518
    iget-object v2, v2, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;->printJobs:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 520
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    .line 521
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const-string v4, "[PrintErrorUi_purgeJobsAndCancel] Purging %d print jobs..."

    invoke-static {v4, v3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 522
    iget-object v3, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->printSpooler:Lcom/squareup/print/PrintSpooler;

    const-string v4, "PrintErrorPopupPresenter.purgeJobsAndCancel"

    invoke-virtual {v3, v0, v4}, Lcom/squareup/print/PrintSpooler;->purgeFailedJobs(Ljava/util/Collection;Ljava/lang/String;)V

    .line 523
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    if-lez v1, :cond_2

    if-ne v1, v2, :cond_1

    .line 527
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/popup/error/impl/R$string;->kitchen_printing_discarded_prints_one:I

    .line 528
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/print/popup/error/impl/R$string;->kitchen_printing_discarded_prints_many:I

    .line 529
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v2, "count"

    .line 530
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 531
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 532
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 533
    :goto_1
    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->toastFactory:Lcom/squareup/util/ToastFactory;

    invoke-interface {v1, v0, v5}, Lcom/squareup/util/ToastFactory;->showText(Ljava/lang/CharSequence;I)V

    .line 535
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->dismiss()V

    return-void
.end method

.method private refreshView()V
    .locals 1

    .line 549
    invoke-virtual {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/print/PrintErrorPopup;

    invoke-virtual {v0}, Lcom/squareup/ui/print/PrintErrorPopup;->getContentView()Lcom/squareup/ui/print/PrintErrorPopupView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/print/PrintErrorPopupView;->update()V

    return-void
.end method

.method private shouldTryToShowPopup()Z
    .locals 3

    .line 393
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "[PrintErrorUi_canShow] Not showing popup because there are no errors"

    .line 394
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v0

    .line 399
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->isPopupShowing()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private showErrorPopup()V
    .locals 1

    .line 544
    new-instance v0, Lcom/squareup/ui/Showing;

    invoke-direct {v0}, Lcom/squareup/ui/Showing;-><init>()V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->show(Lcom/squareup/ui/Showing;)V

    .line 545
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->refreshView()V

    return-void
.end method

.method private showPopupAsSoonAsPossible()V
    .locals 4

    .line 380
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->tryShowErrorRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 381
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->canShowPopupRightNow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 382
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->showErrorPopup()V

    goto :goto_0

    .line 384
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->tryShowErrorRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return-void
.end method


# virtual methods
.method doProcessFailedJobs(Ljava/util/LinkedHashMap;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/print/PrintJob;",
            ">;)V"
        }
    .end annotation

    .line 290
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 292
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 295
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 297
    invoke-virtual {p1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    const/4 v7, 0x2

    const/4 v8, 0x1

    if-eqz v6, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/print/PrintJob;

    .line 298
    invoke-virtual {v6}, Lcom/squareup/print/PrintJob;->getTargetId()Ljava/lang/String;

    move-result-object v9

    .line 299
    iget-object v10, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->router:Lcom/squareup/print/PrintTargetRouter;

    iget-boolean v11, v6, Lcom/squareup/print/PrintJob;->forTestPrint:Z

    .line 300
    invoke-interface {v10, v9, v11}, Lcom/squareup/print/PrintTargetRouter;->retrieveHardwarePrinterFromTarget(Ljava/lang/String;Z)Lkotlin/Pair;

    move-result-object v9

    .line 301
    invoke-virtual {v6}, Lcom/squareup/print/PrintJob;->getLatestPrintAttempt()Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object v10

    iget-object v10, v10, Lcom/squareup/print/PrintJob$PrintAttempt;->result:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    .line 305
    invoke-static {v9, v10}, Lcom/squareup/ui/print/PrintErrorType;->shouldDisplayError(Lkotlin/Pair;Lcom/squareup/print/PrintJob$PrintAttempt$Result;)Z

    move-result v9

    const-string v11, "PrintErrorUi_process"

    if-nez v9, :cond_0

    .line 306
    invoke-interface {v0, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v11, v9, v3

    .line 309
    invoke-virtual {v6}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v9, v8

    invoke-virtual {v10}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->name()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v9, v7

    const-string v6, "[%s] Print Job %s failed unresolvably with %s, caching for silent removal."

    .line 308
    invoke-static {v6, v9}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 313
    :cond_0
    invoke-direct {p0, v6}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->getCachedHardwareInfoForTarget(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v9

    if-nez v9, :cond_1

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v11, v7, v3

    .line 319
    invoke-virtual {v6}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const-string v8, "[%s] Print Job %s has no discernible hardware info andthus cannot be fixed. :(."

    .line 318
    invoke-static {v8, v7}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 320
    invoke-interface {v0, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 325
    :cond_1
    invoke-static {v10}, Lcom/squareup/ui/print/PrintErrorType;->fromResult(Lcom/squareup/print/PrintJob$PrintAttempt$Result;)Lcom/squareup/ui/print/PrintErrorType;

    move-result-object v7

    .line 326
    invoke-virtual {v6}, Lcom/squareup/print/PrintJob;->getPrintAttemptsCount()I

    move-result v8

    .line 327
    new-instance v10, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;

    invoke-direct {v10, v9, v7, v8}, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;-><init>(Lcom/squareup/print/HardwarePrinter$HardwareInfo;Lcom/squareup/ui/print/PrintErrorType;I)V

    .line 329
    invoke-virtual {v1, v10}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 330
    invoke-virtual {v1, v10}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 332
    :cond_2
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    .line 333
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 334
    invoke-virtual {v1, v10, v7}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 340
    :cond_3
    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 341
    iget-object v6, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    new-instance v9, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-direct {v9, v10, v2}, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;-><init>(Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;Ljava/util/List;)V

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    new-array v1, v7, [Ljava/lang/Object;

    .line 343
    iget-object v2, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    .line 344
    invoke-virtual {p1}, Ljava/util/LinkedHashMap;->size()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v1, v8

    const-string p1, "[PrintErrorUi_fetch] %d error rows generated from %d jobs."

    .line 343
    invoke-static {p1, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 347
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_5

    new-array p1, v8, [Ljava/lang/Object;

    .line 349
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p1, v3

    const-string v1, "[PrintErrorUi_fetch] Silently removing %d print jobs."

    .line 348
    invoke-static {v1, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 350
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "doProcessFailedJobs purging failedJobsToRemoveSilently ("

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " failures should not be displayed, "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " failures because hardware printer doesn\'t exist"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 355
    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->printSpooler:Lcom/squareup/print/PrintSpooler;

    invoke-virtual {v1, v0, p1}, Lcom/squareup/print/PrintSpooler;->purgeFailedJobs(Ljava/util/Collection;Ljava/lang/String;)V

    .line 359
    :cond_5
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->isPopupShowing()Z

    move-result p1

    if-eqz p1, :cond_6

    .line 360
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->refreshView()V

    goto :goto_2

    .line 361
    :cond_6
    iget-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_7

    .line 362
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->showPopupAsSoonAsPossible()V

    :cond_7
    :goto_2
    return-void
.end method

.method getAffectedText(I)Ljava/lang/String;
    .locals 2

    .line 475
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/utilities/R$string;->list_pattern_long_nonfinal_separator:I

    .line 476
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/ListPhrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/util/ListPhrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    .line 477
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;

    iget-object p1, p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;->printJobs:Ljava/util/List;

    sget-object v1, Lcom/squareup/ui/print/-$$Lambda$30Jx6519Q3R1eqV9EnFLXvaaF-A;->INSTANCE:Lcom/squareup/ui/print/-$$Lambda$30Jx6519Q3R1eqV9EnFLXvaaF-A;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/util/ListPhrase;->format(Ljava/lang/Iterable;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 478
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/popup/error/impl/R$string;->kitchen_printing_error_affected_prints:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "prints"

    .line 479
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 480
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 481
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getErrorRowCount()I
    .locals 1

    .line 441
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method getHelpMessage(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 2

    .line 499
    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/print/popup/error/impl/R$string;->kitchen_printing_error_title_message:I

    const-string v1, "support_center"

    .line 500
    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/print/popup/error/impl/R$string;->printer_troubleshooting:I

    .line 501
    invoke-virtual {p1, v0}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/print/popup/error/impl/R$string;->kitchen_printing_error_issue_body_support_center:I

    .line 502
    invoke-virtual {p1, v0}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 503
    invoke-virtual {p1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method getMessageText(I)Ljava/lang/CharSequence;
    .locals 2

    .line 471
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;

    iget-object p1, p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;->bucket:Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;

    iget-object p1, p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->errorType:Lcom/squareup/ui/print/PrintErrorType;

    invoke-virtual {p1}, Lcom/squareup/ui/print/PrintErrorType;->getMessage()I

    move-result p1

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getStableId(I)J
    .locals 2

    .line 508
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;

    iget-object p1, p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;->bucket:Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;

    invoke-virtual {p1}, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->hashCode()I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method getTitleText(I)Ljava/lang/String;
    .locals 5

    .line 449
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;

    iget-object v0, v0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;->bucket:Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;

    .line 450
    iget-object v1, v0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    .line 452
    invoke-virtual {v1}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->getDisplayableModelName()Ljava/lang/String;

    move-result-object v1

    .line 453
    invoke-direct {p0, p1}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->buildAffectedStations(I)Ljava/lang/String;

    move-result-object p1

    .line 454
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    .line 456
    invoke-direct {p0, v2}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->getTitlePattern(Z)Ljava/lang/String;

    move-result-object v3

    .line 457
    iget-boolean v4, v0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->hasHadAtLeastOneReprintAttempt:Z

    if-eqz v4, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/print/popup/error/impl/R$string;->kitchen_printing_error_reprint_failed:I

    .line 458
    invoke-interface {v0, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->res:Lcom/squareup/util/Res;

    iget-object v0, v0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->errorType:Lcom/squareup/ui/print/PrintErrorType;

    .line 459
    invoke-virtual {v0}, Lcom/squareup/ui/print/PrintErrorType;->getTitle()I

    move-result v0

    invoke-interface {v4, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 461
    :goto_0
    invoke-static {v3}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    const-string v4, "printer"

    .line 462
    invoke-virtual {v3, v4, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v3, "error"

    .line 463
    invoke-virtual {v1, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    if-eqz v2, :cond_1

    const-string v1, "stations"

    .line 465
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    .line 467
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$fetchCurrentFailedJobsAndUpdate$0$PrintErrorPopupPresenter(Lcom/squareup/print/PrintCallback$PrintCallbackResult;)V
    .locals 1

    .line 280
    invoke-virtual {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 283
    :cond_0
    invoke-interface {p1}, Lcom/squareup/print/PrintCallback$PrintCallbackResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/LinkedHashMap;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->doProcessFailedJobs(Ljava/util/LinkedHashMap;)V

    return-void
.end method

.method onCloseButtonClicked()V
    .locals 6

    .line 270
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->discardPrintJobsPresenter:Lcom/squareup/mortar/PopupPresenter;

    new-instance v1, Lcom/squareup/register/widgets/ConfirmationIds;

    sget v2, Lcom/squareup/print/popup/error/impl/R$string;->kitchen_printing_confirm_discard_print_jobs_title:I

    sget v3, Lcom/squareup/print/popup/error/impl/R$string;->kitchen_printing_confirm_discard_print_jobs_message:I

    sget v4, Lcom/squareup/print/popup/error/impl/R$string;->kitchen_printing_confirm_discard_print_jobs_confirm:I

    sget v5, Lcom/squareup/print/popup/error/impl/R$string;->kitchen_printing_confirm_discard_print_jobs_cancel:I

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/squareup/register/widgets/ConfirmationIds;-><init>(IIII)V

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 164
    iget-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    invoke-virtual {p1, p0}, Lcom/squareup/print/HardwarePrinterTracker;->addListener(Lcom/squareup/print/HardwarePrinterTracker$Listener;)V

    .line 165
    iget-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->printSpooler:Lcom/squareup/print/PrintSpooler;

    invoke-virtual {p1, p0}, Lcom/squareup/print/PrintSpooler;->addPrintJobStatusListener(Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;)V

    .line 166
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->fetchCurrentFailedJobsAndUpdate()V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    invoke-virtual {v0, p0}, Lcom/squareup/print/HardwarePrinterTracker;->removeListener(Lcom/squareup/print/HardwarePrinterTracker$Listener;)V

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->printSpooler:Lcom/squareup/print/PrintSpooler;

    invoke-virtual {v0, p0}, Lcom/squareup/print/PrintSpooler;->removePrintJobStatusListener(Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;)V

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->tryShowErrorRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 176
    invoke-super {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 177
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->isPopupShowing()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 178
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->refreshView()V

    goto :goto_0

    .line 179
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->shouldTryToShowPopup()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 181
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->showPopupAsSoonAsPossible()V

    :cond_1
    :goto_0
    return-void
.end method

.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 58
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->onPopupResult(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPopupResult(Ljava/lang/Void;)V
    .locals 0

    return-void
.end method

.method public onPrintJobAttempted(Lcom/squareup/print/PrintJob;)V
    .locals 9

    .line 213
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getLatestPrintAttempt()Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/print/PrintJob$PrintAttempt;->result:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    .line 214
    sget-object v1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->SUCCESS:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/4 v2, 0x3

    const/4 v3, 0x2

    const-string v4, "PrintErrorUi_update"

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-ne v0, v1, :cond_4

    .line 217
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getPrintAttemptsCount()I

    move-result v0

    if-ne v0, v6, :cond_0

    new-array v0, v3, [Ljava/lang/Object;

    aput-object v4, v0, v5

    .line 219
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v6

    const-string p1, "[%s] Print Job %s is SUCCESS after 1 attempt, ignore it."

    .line 218
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;

    .line 226
    iget-object v7, v1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;->printJobs:Ljava/util/List;

    invoke-interface {v7, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 227
    iget-object v0, v1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;->printJobs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    new-array v0, v3, [Ljava/lang/Object;

    aput-object v4, v0, v5

    .line 230
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v6

    const-string p1, "[%s] Previously FAILED Print Job %s was reported SUCCESS, removing from backing data."

    .line 228
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 231
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->isPopupShowing()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 232
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->refreshView()V

    :cond_2
    return-void

    :cond_3
    new-array v0, v2, [Ljava/lang/Object;

    aput-object v4, v0, v5

    .line 240
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getPrintAttemptsCount()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    const-string p1, "[%s] Print Job %s is SUCCESS after %d attempt, ignore it."

    .line 239
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 244
    :cond_4
    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->router:Lcom/squareup/print/PrintTargetRouter;

    .line 245
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getTargetId()Ljava/lang/String;

    move-result-object v7

    iget-boolean v8, p1, Lcom/squareup/print/PrintJob;->forTestPrint:Z

    invoke-interface {v1, v7, v8}, Lcom/squareup/print/PrintTargetRouter;->retrieveHardwarePrinterFromTarget(Ljava/lang/String;Z)Lkotlin/Pair;

    move-result-object v1

    .line 248
    invoke-static {v1, v0}, Lcom/squareup/ui/print/PrintErrorType;->shouldDisplayError(Lkotlin/Pair;Lcom/squareup/print/PrintJob$PrintAttempt$Result;)Z

    move-result v1

    if-nez v1, :cond_5

    new-array v1, v2, [Ljava/lang/Object;

    aput-object v4, v1, v5

    .line 250
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {v0}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->name()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    const-string v2, "[%s] Print Job %s failed unresolvably with %s, removing it silently."

    .line 249
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 251
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PrintErrorPopupPresenter.onPrintJobAttempted: Failed print job with result "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    invoke-virtual {v0}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " should not display error"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 255
    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->printSpooler:Lcom/squareup/print/PrintSpooler;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v1, p1, v0}, Lcom/squareup/print/PrintSpooler;->purgeFailedJobs(Ljava/util/Collection;Ljava/lang/String;)V

    return-void

    :cond_5
    new-array v1, v2, [Ljava/lang/Object;

    aput-object v4, v1, v5

    .line 261
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v6

    invoke-virtual {v0}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->name()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v3

    const-string p1, "[%s] Print Job %s failed resolvably with %s, fetching all failed jobs..."

    .line 260
    invoke-static {p1, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 262
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->fetchCurrentFailedJobsAndUpdate()V

    return-void
.end method

.method public onPrintJobEnqueued(Lcom/squareup/print/PrintJob;)V
    .locals 0

    return-void
.end method

.method onReprintClicked(I)V
    .locals 4

    .line 424
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;

    .line 425
    iget-object v1, v0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;->printJobs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/print/PrintJob;

    .line 426
    iget-object v3, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->printSpooler:Lcom/squareup/print/PrintSpooler;

    invoke-virtual {v3, v2}, Lcom/squareup/print/PrintSpooler;->enqueueForReprint(Lcom/squareup/print/PrintJob;)V

    goto :goto_0

    .line 428
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 431
    iget-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 432
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->finishWhenEmpty()V

    goto :goto_1

    .line 434
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->refreshView()V

    .line 437
    :goto_1
    iget-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-direct {p0, v0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->forReprintTapped(Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;)Lcom/squareup/print/PrintErrorReprintEvent;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 1

    .line 186
    invoke-super {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onSave(Landroid/os/Bundle;)V

    .line 187
    iget-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->tryShowErrorRunnable:Ljava/lang/Runnable;

    invoke-interface {p1, v0}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    return-void
.end method

.method public printerConnected(Lcom/squareup/print/HardwarePrinter;)V
    .locals 0

    .line 197
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->isPopupShowing()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 198
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->refreshView()V

    :cond_0
    return-void
.end method

.method public printerDisconnected(Lcom/squareup/print/HardwarePrinter;)V
    .locals 0

    .line 203
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->isPopupShowing()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 204
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->refreshView()V

    :cond_0
    return-void
.end method

.method shouldShowReprintWarning(I)Z
    .locals 1

    .line 445
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;

    iget-object p1, p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;->bucket:Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;

    iget-boolean p1, p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->hasHadAtLeastOneReprintAttempt:Z

    return p1
.end method

.method public bridge synthetic show(Landroid/os/Parcelable;)V
    .locals 0

    .line 58
    check-cast p1, Lcom/squareup/ui/Showing;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->show(Lcom/squareup/ui/Showing;)V

    return-void
.end method

.method public show(Lcom/squareup/ui/Showing;)V
    .locals 0

    .line 191
    invoke-super {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    .line 193
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->logViewPrintErrorPopupEvent()V

    return-void
.end method

.method showSpinner(I)Z
    .locals 2

    .line 485
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->bucketedErrorRows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;

    iget-object p1, p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;->bucket:Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;

    .line 486
    iget-object v0, p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->errorType:Lcom/squareup/ui/print/PrintErrorType;

    invoke-virtual {v0}, Lcom/squareup/ui/print/PrintErrorType;->canShowSpinner()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 491
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    iget-object p1, p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/print/HardwarePrinterTracker;->getHardwarePrinter(Ljava/lang/String;)Lcom/squareup/print/HardwarePrinter;

    move-result-object p1

    if-eqz p1, :cond_1

    return v1

    :cond_1
    const/4 p1, 0x1

    return p1
.end method
