.class Lcom/squareup/ui/print/PrintErrorPopupView$PrintErrorAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "PrintErrorPopupView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/print/PrintErrorPopupView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PrintErrorAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/ui/print/PrintErrorPopupView$PrinterErrorHolder;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/print/PrintErrorPopupView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/print/PrintErrorPopupView;)V
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupView$PrintErrorAdapter;->this$0:Lcom/squareup/ui/print/PrintErrorPopupView;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    const/4 p1, 0x1

    .line 103
    invoke-virtual {p0, p1}, Lcom/squareup/ui/print/PrintErrorPopupView$PrintErrorAdapter;->setHasStableIds(Z)V

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$PrintErrorAdapter;->this$0:Lcom/squareup/ui/print/PrintErrorPopupView;

    iget-object v0, v0, Lcom/squareup/ui/print/PrintErrorPopupView;->presenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->getErrorRowCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    .line 141
    invoke-virtual {p0, p1}, Lcom/squareup/ui/print/PrintErrorPopupView$PrintErrorAdapter;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$PrintErrorAdapter;->this$0:Lcom/squareup/ui/print/PrintErrorPopupView;

    iget-object v0, v0, Lcom/squareup/ui/print/PrintErrorPopupView;->presenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->getStableId(I)J

    move-result-wide v0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$PrintErrorAdapter;->this$0:Lcom/squareup/ui/print/PrintErrorPopupView;

    iget-object v0, v0, Lcom/squareup/ui/print/PrintErrorPopupView;->presenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->getErrorRowCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 100
    check-cast p1, Lcom/squareup/ui/print/PrintErrorPopupView$PrinterErrorHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/print/PrintErrorPopupView$PrintErrorAdapter;->onBindViewHolder(Lcom/squareup/ui/print/PrintErrorPopupView$PrinterErrorHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/print/PrintErrorPopupView$PrinterErrorHolder;I)V
    .locals 0

    .line 130
    invoke-virtual {p1, p2}, Lcom/squareup/ui/print/PrintErrorPopupView$PrinterErrorHolder;->bindRow(I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 100
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/print/PrintErrorPopupView$PrintErrorAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/print/PrintErrorPopupView$PrinterErrorHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/print/PrintErrorPopupView$PrinterErrorHolder;
    .locals 3

    if-nez p2, :cond_1

    .line 114
    iget-object p2, p0, Lcom/squareup/ui/print/PrintErrorPopupView$PrintErrorAdapter;->this$0:Lcom/squareup/ui/print/PrintErrorPopupView;

    invoke-virtual {p2}, Lcom/squareup/ui/print/PrintErrorPopupView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    .line 115
    sget v0, Lcom/squareup/print/popup/error/impl/R$string;->kitchen_printing_reprint:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 116
    sget v1, Lcom/squareup/print/popup/error/impl/R$string;->kitchen_printing_try_again:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 118
    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupView$PrintErrorAdapter;->this$0:Lcom/squareup/ui/print/PrintErrorPopupView;

    iget-object v1, v1, Lcom/squareup/ui/print/PrintErrorPopupView;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 119
    new-instance v1, Lcom/squareup/ui/print/ErrorRetryRowTablet;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v1, p1, v0, p2}, Lcom/squareup/ui/print/ErrorRetryRowTablet;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 121
    :cond_0
    new-instance v1, Lcom/squareup/ui/print/ErrorRetryRowPhone;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/ui/print/ErrorRetryRowPhone;-><init>(Landroid/content/Context;)V

    .line 123
    :goto_0
    new-instance p1, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;

    iget-object v2, p0, Lcom/squareup/ui/print/PrintErrorPopupView$PrintErrorAdapter;->this$0:Lcom/squareup/ui/print/PrintErrorPopupView;

    invoke-direct {p1, v2, v1, v0, p2}, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;-><init>(Lcom/squareup/ui/print/PrintErrorPopupView;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    return-object p1

    .line 125
    :cond_1
    new-instance p2, Lcom/squareup/ui/print/PrintErrorPopupView$FooterRowHolder;

    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$PrintErrorAdapter;->this$0:Lcom/squareup/ui/print/PrintErrorPopupView;

    invoke-direct {p2, v0, p1}, Lcom/squareup/ui/print/PrintErrorPopupView$FooterRowHolder;-><init>(Lcom/squareup/ui/print/PrintErrorPopupView;Landroid/view/ViewGroup;)V

    return-object p2
.end method
