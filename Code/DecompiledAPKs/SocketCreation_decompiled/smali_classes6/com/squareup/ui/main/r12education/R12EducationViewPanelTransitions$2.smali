.class Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$2;
.super Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;
.source "R12EducationViewPanelTransitions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->populateStandardPanelTransitions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)V
    .locals 0

    .line 94
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$2;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    invoke-direct {p0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;-><init>()V

    return-void
.end method


# virtual methods
.method public tweenElement(Lcom/squareup/ui/main/r12education/R12EducationView$Element;Landroid/view/View;FLandroid/view/ViewGroup;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 0

    .line 98
    sget-object p4, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$14;->$SwitchMap$com$squareup$ui$main$r12education$R12EducationView$Element:[I

    invoke-virtual {p1}, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->ordinal()I

    move-result p1

    aget p1, p4, p1

    const/4 p4, 0x1

    const/high16 p6, 0x3f400000    # 0.75f

    if-eq p1, p4, :cond_2

    const/4 p4, 0x2

    const/high16 p5, 0x3f800000    # 1.0f

    if-eq p1, p4, :cond_1

    const/4 p4, 0x3

    if-eq p1, p4, :cond_0

    .line 129
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->hide(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    const p1, 0x3f733333    # 0.95f

    .line 122
    invoke-static {}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$200()Landroid/view/animation/Interpolator;

    move-result-object p4

    invoke-static {p3, p1, p5, p4}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFFLandroid/view/animation/Interpolator;)F

    move-result p1

    .line 123
    invoke-static {p2, p1}, Lcom/squareup/ui/main/r12education/Tweens;->fadeIn(Landroid/view/View;F)V

    goto :goto_0

    .line 113
    :cond_1
    invoke-static {}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$200()Landroid/view/animation/Interpolator;

    move-result-object p1

    invoke-static {p3, p6, p5, p1}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFFLandroid/view/animation/Interpolator;)F

    move-result p1

    .line 115
    invoke-static {p2, p1}, Lcom/squareup/ui/main/r12education/Tweens;->fadeIn(Landroid/view/View;F)V

    .line 116
    invoke-static {p2, p7}, Lcom/squareup/ui/main/r12education/Tweens;->centerY(Landroid/view/View;Landroid/view/View;)V

    .line 117
    iget-object p3, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$2;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    invoke-static {p3}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$300(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;

    move-result-object p3

    iget p3, p3, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;->cableMargin:I

    invoke-static {p2, p7, p3, p1}, Lcom/squareup/ui/main/r12education/Tweens;->slideInLeftToAlignEdgeToCenter(Landroid/view/View;Landroid/view/View;IF)V

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    .line 102
    invoke-static {}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$100()Landroid/view/animation/Interpolator;

    move-result-object p4

    invoke-static {p3, p1, p6, p4}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFFLandroid/view/animation/Interpolator;)F

    move-result p1

    .line 104
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->show(Landroid/view/View;)V

    .line 105
    invoke-static {p2, p7}, Lcom/squareup/ui/main/r12education/Tweens;->centerX(Landroid/view/View;Landroid/view/View;)V

    .line 106
    invoke-static {p2, p5, p7, p1}, Lcom/squareup/ui/main/r12education/Tweens;->translateCenterY(Landroid/view/View;Landroid/view/View;Landroid/view/View;F)V

    .line 107
    invoke-static {p2, p1}, Lcom/squareup/ui/main/r12education/Tweens;->rotate90(Landroid/view/View;F)V

    :goto_0
    return-void
.end method
