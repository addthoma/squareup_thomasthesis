.class public Lcom/squareup/ui/main/SessionExpiredDialog$Runner;
.super Ljava/lang/Object;
.source "SessionExpiredDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/SessionExpiredDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Runner"
.end annotation


# instance fields
.field private final apiTransactionController:Lcom/squareup/api/ApiTransactionController;

.field private final apiTransactionState:Lcom/squareup/api/ApiTransactionState;

.field private final authenticator:Lcom/squareup/account/LegacyAuthenticator;

.field private final loggedOutStarter:Lcom/squareup/loggedout/LoggedOutStarter;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/loggedout/LoggedOutStarter;Lcom/squareup/payment/Transaction;Lcom/squareup/api/ApiTransactionState;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, Lcom/squareup/ui/main/SessionExpiredDialog$Runner;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    .line 76
    iput-object p2, p0, Lcom/squareup/ui/main/SessionExpiredDialog$Runner;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    .line 77
    iput-object p3, p0, Lcom/squareup/ui/main/SessionExpiredDialog$Runner;->loggedOutStarter:Lcom/squareup/loggedout/LoggedOutStarter;

    .line 78
    iput-object p4, p0, Lcom/squareup/ui/main/SessionExpiredDialog$Runner;->transaction:Lcom/squareup/payment/Transaction;

    .line 79
    iput-object p5, p0, Lcom/squareup/ui/main/SessionExpiredDialog$Runner;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    return-void
.end method


# virtual methods
.method dismissed(Landroid/content/Context;)V
    .locals 3

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/main/SessionExpiredDialog$Runner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/main/SessionExpiredDialog$Runner;->transaction:Lcom/squareup/payment/Transaction;

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_OTHER:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->dropPayment(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/SessionExpiredDialog$Runner;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    new-instance v1, Lcom/squareup/account/LogOutReason$SessionExpired;

    const-string v2, "RootFlow"

    invoke-direct {v1, v2}, Lcom/squareup/account/LogOutReason$SessionExpired;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/account/LegacyAuthenticator;->logOut(Lcom/squareup/account/LogOutReason;)V

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/main/SessionExpiredDialog$Runner;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionState;->isApiTransactionRequest()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/main/SessionExpiredDialog$Runner;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    invoke-static {p1}, Lcom/squareup/container/ContainerTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object p1

    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->USER_NOT_LOGGED_IN:Lcom/squareup/api/ApiErrorResult;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/api/ApiTransactionController;->handleApiTransactionCanceled(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/api/ApiErrorResult;)Z

    goto :goto_0

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/main/SessionExpiredDialog$Runner;->loggedOutStarter:Lcom/squareup/loggedout/LoggedOutStarter;

    invoke-interface {v0, p1}, Lcom/squareup/loggedout/LoggedOutStarter;->showLogin(Landroid/content/Context;)V

    :goto_0
    return-void
.end method
