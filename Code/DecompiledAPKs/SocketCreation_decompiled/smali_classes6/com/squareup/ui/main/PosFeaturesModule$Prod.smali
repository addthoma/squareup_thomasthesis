.class public abstract Lcom/squareup/ui/main/PosFeaturesModule$Prod;
.super Ljava/lang/Object;
.source "PosFeaturesModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/server/account/AuthenticationModule;,
        Lcom/squareup/analytics/BranchHelper$Module;,
        Lcom/squareup/loggedout/LoggedOutFeatureModule;,
        Lcom/squareup/ui/main/PosFeaturesModule;,
        Lcom/squareup/ui/main/PosFeaturesModule$Real;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/PosFeaturesModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Prod"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
