.class Lcom/squareup/ui/main/errors/ButtonFlowStarter$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "ButtonFlowStarter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/errors/ButtonFlowStarter;->killTenderReaderInitiated()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/errors/ButtonFlowStarter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/errors/ButtonFlowStarter;)V
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter$1;->this$0:Lcom/squareup/ui/main/errors/ButtonFlowStarter;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 3

    .line 76
    sget-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_READER_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    .line 77
    iget-object v1, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter$1;->this$0:Lcom/squareup/ui/main/errors/ButtonFlowStarter;

    invoke-static {v1}, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->access$000(Lcom/squareup/ui/main/errors/ButtonFlowStarter;)Lcom/squareup/payment/Transaction;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/squareup/payment/Transaction;->dropPaymentOrTender(ZLcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 78
    iget-object v1, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter$1;->this$0:Lcom/squareup/ui/main/errors/ButtonFlowStarter;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->goHome(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    return-void
.end method
