.class public final Lcom/squareup/ui/main/r12education/R12EducationScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "R12EducationScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/LocksOrientation;


# annotations
.annotation runtime Lcom/squareup/container/layer/FullSheet;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/main/r12education/R12EducationScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/r12education/R12EducationScreen$Component;,
        Lcom/squareup/ui/main/r12education/R12EducationScreen$ParentComponent;,
        Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/main/r12education/R12EducationScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final useUpdatingMessaging:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 176
    sget-object v0, Lcom/squareup/ui/main/r12education/-$$Lambda$R12EducationScreen$U5rFzTaBXKD2m2lIziKgRA630BU;->INSTANCE:Lcom/squareup/ui/main/r12education/-$$Lambda$R12EducationScreen$U5rFzTaBXKD2m2lIziKgRA630BU;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    .line 51
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    .line 52
    iput-boolean p1, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen;->useUpdatingMessaging:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/main/r12education/R12EducationScreen;)Z
    .locals 0

    .line 47
    iget-boolean p0, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen;->useUpdatingMessaging:Z

    return p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/main/r12education/R12EducationScreen;
    .locals 1

    .line 177
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 178
    :goto_0
    new-instance p0, Lcom/squareup/ui/main/r12education/R12EducationScreen;

    invoke-direct {p0, v0}, Lcom/squareup/ui/main/r12education/R12EducationScreen;-><init>(Z)V

    return-object p0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 173
    iget-boolean p2, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen;->useUpdatingMessaging:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public getOrientationForPhone()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;
    .locals 1

    .line 60
    sget-object v0, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->SENSOR_PORTRAIT:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-object v0
.end method

.method public getOrientationForTablet()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;
    .locals 1

    .line 64
    sget-object v0, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->SENSOR_LANDSCAPE:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 56
    sget v0, Lcom/squareup/readertutorial/R$layout;->r12_education_view:I

    return v0
.end method
