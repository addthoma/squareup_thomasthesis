.class public final Lcom/squareup/ui/main/SessionExpiredDialog_Runner_Factory;
.super Ljava/lang/Object;
.source "SessionExpiredDialog_Runner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/SessionExpiredDialog$Runner;",
        ">;"
    }
.end annotation


# instance fields
.field private final apiTransactionControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;"
        }
    .end annotation
.end field

.field private final apiTransactionStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;"
        }
    .end annotation
.end field

.field private final authenticatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;"
        }
    .end annotation
.end field

.field private final loggedOutStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loggedout/LoggedOutStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loggedout/LoggedOutStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/main/SessionExpiredDialog_Runner_Factory;->authenticatorProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/ui/main/SessionExpiredDialog_Runner_Factory;->apiTransactionControllerProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/ui/main/SessionExpiredDialog_Runner_Factory;->loggedOutStarterProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/ui/main/SessionExpiredDialog_Runner_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/ui/main/SessionExpiredDialog_Runner_Factory;->apiTransactionStateProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/SessionExpiredDialog_Runner_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loggedout/LoggedOutStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;)",
            "Lcom/squareup/ui/main/SessionExpiredDialog_Runner_Factory;"
        }
    .end annotation

    .line 54
    new-instance v6, Lcom/squareup/ui/main/SessionExpiredDialog_Runner_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/main/SessionExpiredDialog_Runner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/loggedout/LoggedOutStarter;Lcom/squareup/payment/Transaction;Lcom/squareup/api/ApiTransactionState;)Lcom/squareup/ui/main/SessionExpiredDialog$Runner;
    .locals 7

    .line 60
    new-instance v6, Lcom/squareup/ui/main/SessionExpiredDialog$Runner;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/main/SessionExpiredDialog$Runner;-><init>(Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/loggedout/LoggedOutStarter;Lcom/squareup/payment/Transaction;Lcom/squareup/api/ApiTransactionState;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/SessionExpiredDialog$Runner;
    .locals 5

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/main/SessionExpiredDialog_Runner_Factory;->authenticatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/account/LegacyAuthenticator;

    iget-object v1, p0, Lcom/squareup/ui/main/SessionExpiredDialog_Runner_Factory;->apiTransactionControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/ApiTransactionController;

    iget-object v2, p0, Lcom/squareup/ui/main/SessionExpiredDialog_Runner_Factory;->loggedOutStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/loggedout/LoggedOutStarter;

    iget-object v3, p0, Lcom/squareup/ui/main/SessionExpiredDialog_Runner_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/payment/Transaction;

    iget-object v4, p0, Lcom/squareup/ui/main/SessionExpiredDialog_Runner_Factory;->apiTransactionStateProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/ApiTransactionState;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/main/SessionExpiredDialog_Runner_Factory;->newInstance(Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/loggedout/LoggedOutStarter;Lcom/squareup/payment/Transaction;Lcom/squareup/api/ApiTransactionState;)Lcom/squareup/ui/main/SessionExpiredDialog$Runner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/main/SessionExpiredDialog_Runner_Factory;->get()Lcom/squareup/ui/main/SessionExpiredDialog$Runner;

    move-result-object v0

    return-object v0
.end method
