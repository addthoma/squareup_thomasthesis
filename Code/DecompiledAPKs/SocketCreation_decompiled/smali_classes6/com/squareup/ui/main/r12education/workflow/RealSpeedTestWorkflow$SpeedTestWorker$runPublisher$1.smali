.class final Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker$runPublisher$1;
.super Ljava/lang/Object;
.source "RealSpeedTestWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker;->runPublisher()Lorg/reactivestreams/Publisher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lretrofit2/Response;",
        "Ljava/lang/Void;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $timeBefore:J

.field final synthetic this$0:Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker;J)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker$runPublisher$1;->this$0:Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker;

    iput-wide p2, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker$runPublisher$1;->$timeBefore:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lretrofit2/Response;)J
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/Response<",
            "Ljava/lang/Void;",
            ">;)J"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker$runPublisher$1;->this$0:Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker;

    iget-object p1, p1, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker;->this$0:Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;

    invoke-static {p1}, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;->access$getClock$p(Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;)Lcom/squareup/util/Clock;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker$runPublisher$1;->$timeBefore:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .line 131
    check-cast p1, Lretrofit2/Response;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker$runPublisher$1;->apply(Lretrofit2/Response;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    return-object p1
.end method
