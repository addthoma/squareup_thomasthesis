.class public final Lcom/squareup/ui/main/errors/ConcreteWarningScreens$CardNotRead;
.super Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen;
.source "ConcreteWarningScreens.java"


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent$FromFactory;
    value = Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$ComponentFactory;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/ConcreteWarningScreens;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CardNotRead"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/main/errors/ConcreteWarningScreens$CardNotRead;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$CardNotRead;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 390
    new-instance v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$CardNotRead;

    invoke-direct {v0}, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$CardNotRead;-><init>()V

    sput-object v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$CardNotRead;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$CardNotRead;

    .line 408
    sget-object v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$CardNotRead;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$CardNotRead;

    .line 409
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$CardNotRead;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 392
    invoke-direct {p0}, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen;-><init>()V

    return-void
.end method


# virtual methods
.method protected getInitialScreenData(Lcom/squareup/util/Res;)Lcom/squareup/ui/main/errors/WarningScreenData;
    .locals 2

    .line 398
    new-instance p1, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    invoke-direct {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;-><init>()V

    const/4 v0, 0x0

    .line 399
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->defaultButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_EXCLAMATION:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 400
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/R$string;->card_not_read_message:I

    .line 401
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->messageId(I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/R$string;->card_not_read_title:I

    .line 402
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->titleId(I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 403
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->cancellable(Z)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    const-wide/16 v0, 0x1388

    .line 404
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->timeout(Ljava/lang/Long;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    .line 405
    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->build()Lcom/squareup/ui/main/errors/WarningScreenData;

    move-result-object p1

    return-object p1
.end method
