.class public Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;
.super Ljava/lang/Object;
.source "R12EducationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/r12education/R12EducationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PanelList"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$VideoAvailable;,
        Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;,
        Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;
    }
.end annotation


# instance fields
.field private final panelList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$VideoAvailable;)V
    .locals 3

    .line 312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 313
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->panelList:Ljava/util/List;

    .line 316
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$5;->$SwitchMap$com$squareup$ui$main$r12education$R12EducationView$PanelList$DuringFwup:[I

    invoke-virtual {p1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x2

    const/4 v1, 0x1

    if-eq p1, v1, :cond_1

    if-ne p1, v0, :cond_0

    .line 321
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->panelList:Ljava/util/List;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->WELCOME:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 324
    :cond_0
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    .line 318
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->panelList:Ljava/util/List;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->WELCOME_DURING_BLOCKING_FWUP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 327
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->panelList:Ljava/util/List;

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->CHARGE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 329
    sget-object p1, Lcom/squareup/ui/main/r12education/R12EducationView$5;->$SwitchMap$com$squareup$ui$main$r12education$R12EducationView$PanelList$PreferContactless:[I

    invoke-virtual {p2}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;->ordinal()I

    move-result p2

    aget p1, p1, p2

    if-eq p1, v1, :cond_4

    if-eq p1, v0, :cond_3

    const/4 p2, 0x3

    if-ne p1, p2, :cond_2

    .line 338
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->panelList:Ljava/util/List;

    sget-object p2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->DIP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 339
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->panelList:Ljava/util/List;

    sget-object p2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->TAP_PHONE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 342
    :cond_2
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    .line 334
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->panelList:Ljava/util/List;

    sget-object p2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->TAP_HAND:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 335
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->panelList:Ljava/util/List;

    sget-object p2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->DIP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 331
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->panelList:Ljava/util/List;

    sget-object p2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->DIP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 345
    :goto_1
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->panelList:Ljava/util/List;

    sget-object p2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->SWIPE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 347
    sget-object p1, Lcom/squareup/ui/main/r12education/R12EducationView$5;->$SwitchMap$com$squareup$ui$main$r12education$R12EducationView$PanelList$VideoAvailable:[I

    invoke-virtual {p3}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$VideoAvailable;->ordinal()I

    move-result p2

    aget p1, p1, p2

    if-eq p1, v1, :cond_6

    if-ne p1, v0, :cond_5

    goto :goto_2

    .line 354
    :cond_5
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    .line 349
    :cond_6
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->panelList:Ljava/util/List;

    sget-object p2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->VIDEO:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 357
    :goto_2
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->panelList:Ljava/util/List;

    sget-object p2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->START_SELLING:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method getPanelCount()I
    .locals 1

    .line 365
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->panelList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method panelAt(I)Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;
    .locals 1

    .line 361
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->panelList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    return-object p1
.end method
