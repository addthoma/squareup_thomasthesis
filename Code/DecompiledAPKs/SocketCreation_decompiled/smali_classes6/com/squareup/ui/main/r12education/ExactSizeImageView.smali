.class public Lcom/squareup/ui/main/r12education/ExactSizeImageView;
.super Landroidx/appcompat/widget/AppCompatImageView;
.source "ExactSizeImageView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    sget-object p1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/r12education/ExactSizeImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 2

    .line 21
    invoke-super {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatImageView;->onMeasure(II)V

    .line 22
    invoke-virtual {p0}, Lcom/squareup/ui/main/r12education/ExactSizeImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 23
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result p2

    .line 24
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result p1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/ui/main/r12education/ExactSizeImageView;->getMeasuredWidth()I

    move-result v0

    .line 26
    invoke-virtual {p0}, Lcom/squareup/ui/main/r12education/ExactSizeImageView;->getMeasuredHeight()I

    move-result v1

    if-lt v0, p2, :cond_0

    if-ge v1, p1, :cond_1

    .line 28
    :cond_0
    invoke-virtual {p0, p2, p1}, Lcom/squareup/ui/main/r12education/ExactSizeImageView;->setMeasuredDimension(II)V

    :cond_1
    return-void
.end method
