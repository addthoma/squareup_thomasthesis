.class public interface abstract Lcom/squareup/ui/main/PosContainer;
.super Ljava/lang/Object;
.source "PosContainer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/PosContainer$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u001e\n\u0002\u0008\u0002\u0008f\u0018\u0000 $2\u00020\u0001:\u0001$J\u0018\u0010\u0002\u001a\u00020\u00032\u000e\u0010\u0004\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00010\u0005H\'J)\u0010\u0006\u001a\u00020\u00072\u001a\u0010\u0008\u001a\u000e\u0012\n\u0008\u0001\u0012\u0006\u0012\u0002\u0008\u00030\u00050\t\"\u0006\u0012\u0002\u0008\u00030\u0005H&\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u00072\u0006\u0010\u000c\u001a\u00020\rH&J\u000e\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u000fH&J\u000e\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u000fH&J\u0010\u0010\u0012\u001a\u00020\u00072\u0006\u0010\u0013\u001a\u00020\u0014H&J\u0008\u0010\u0015\u001a\u00020\u0007H&J\u0010\u0010\u0016\u001a\u00020\u00072\u0006\u0010\u0013\u001a\u00020\u0014H&J\u0008\u0010\u0017\u001a\u00020\u0007H&J\u0016\u0010\u0018\u001a\u00020\u00072\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001aH&J\u0008\u0010\u001c\u001a\u00020\u0007H&J\u0018\u0010\u001c\u001a\u00020\u00072\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H&J\u000e\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u000fH&J\u0014\u0010\"\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001b0#0\u000fH&\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/ui/main/PosContainer;",
        "",
        "currentPathIncludes",
        "",
        "pathType",
        "Ljava/lang/Class;",
        "goBackPast",
        "",
        "screenTypes",
        "",
        "([Ljava/lang/Class;)V",
        "goBackPastWorkflow",
        "runnerServiceName",
        "",
        "hasViewNow",
        "Lio/reactivex/Observable;",
        "nextHistory",
        "Lflow/History;",
        "onActivityCreate",
        "intent",
        "Landroid/content/Intent;",
        "onActivityFinish",
        "onActivityNewIntent",
        "onBackPressed",
        "pushStack",
        "newTop",
        "",
        "Lcom/squareup/container/ContainerTreeKey;",
        "resetHistory",
        "temporaryStartHistory",
        "Lcom/squareup/ui/main/HistoryFactory;",
        "direction",
        "Lflow/Direction;",
        "topOfTraversalCompleting",
        "traversalByVisibleScreens",
        "",
        "Companion",
        "pos-container_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/main/PosContainer$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/ui/main/PosContainer$Companion;->$$INSTANCE:Lcom/squareup/ui/main/PosContainer$Companion;

    sput-object v0, Lcom/squareup/ui/main/PosContainer;->Companion:Lcom/squareup/ui/main/PosContainer$Companion;

    return-void
.end method


# virtual methods
.method public abstract currentPathIncludes(Ljava/lang/Class;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "There is no good reason to ask \"what is showing right now.\""
    .end annotation
.end method

.method public varargs abstract goBackPast([Ljava/lang/Class;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Class<",
            "*>;)V"
        }
    .end annotation
.end method

.method public abstract goBackPastWorkflow(Ljava/lang/String;)V
.end method

.method public abstract hasViewNow()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract nextHistory()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lflow/History;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onActivityCreate(Landroid/content/Intent;)V
.end method

.method public abstract onActivityFinish()V
.end method

.method public abstract onActivityNewIntent(Landroid/content/Intent;)V
.end method

.method public abstract onBackPressed()V
.end method

.method public abstract pushStack(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract resetHistory()V
.end method

.method public abstract resetHistory(Lcom/squareup/ui/main/HistoryFactory;Lflow/Direction;)V
.end method

.method public abstract topOfTraversalCompleting()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation
.end method

.method public abstract traversalByVisibleScreens()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/Collection<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;>;"
        }
    .end annotation
.end method
