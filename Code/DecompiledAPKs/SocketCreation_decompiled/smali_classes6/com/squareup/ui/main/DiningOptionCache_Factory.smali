.class public final Lcom/squareup/ui/main/DiningOptionCache_Factory;
.super Ljava/lang/Object;
.source "DiningOptionCache_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/DiningOptionCache;",
        ">;"
    }
.end annotation


# instance fields
.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final eventSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/main/DiningOptionCache_Factory;->cogsProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/main/DiningOptionCache_Factory;->eventSinkProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/DiningOptionCache_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;)",
            "Lcom/squareup/ui/main/DiningOptionCache_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/main/DiningOptionCache_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/main/DiningOptionCache_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;Lcom/squareup/badbus/BadEventSink;)Lcom/squareup/ui/main/DiningOptionCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/badbus/BadEventSink;",
            ")",
            "Lcom/squareup/ui/main/DiningOptionCache;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/ui/main/DiningOptionCache;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/main/DiningOptionCache;-><init>(Ljavax/inject/Provider;Lcom/squareup/badbus/BadEventSink;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/DiningOptionCache;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/main/DiningOptionCache_Factory;->cogsProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/ui/main/DiningOptionCache_Factory;->eventSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/badbus/BadEventSink;

    invoke-static {v0, v1}, Lcom/squareup/ui/main/DiningOptionCache_Factory;->newInstance(Ljavax/inject/Provider;Lcom/squareup/badbus/BadEventSink;)Lcom/squareup/ui/main/DiningOptionCache;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/main/DiningOptionCache_Factory;->get()Lcom/squareup/ui/main/DiningOptionCache;

    move-result-object v0

    return-object v0
.end method
