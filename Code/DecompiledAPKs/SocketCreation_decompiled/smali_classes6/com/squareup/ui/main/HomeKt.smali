.class public final Lcom/squareup/ui/main/HomeKt;
.super Ljava/lang/Object;
.source "Home.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0004\u001a\u0014\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u001a\u001c\u0010\u0005\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u001a\u0014\u0010\t\u001a\u00020\u0004*\u00020\u00022\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u001a\u0012\u0010\n\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u000b\u001a\u00020\u0004\u00a8\u0006\u000c"
    }
    d2 = {
        "buildUpon",
        "Lflow/History$Builder;",
        "Lcom/squareup/ui/main/Home;",
        "currentHistory",
        "Lflow/History;",
        "contains",
        "",
        "key",
        "",
        "getHomeHistory",
        "isHome",
        "navigationHistory",
        "pos-container_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final buildUpon(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History$Builder;
    .locals 1

    const-string v0, "$this$buildUpon"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-static {}, Lflow/History;->emptyBuilder()Lflow/History$Builder;

    move-result-object v0

    invoke-interface {p0, p1}, Lcom/squareup/ui/main/Home;->getHomeScreens(Lflow/History;)Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/util/Collection;

    invoke-virtual {v0, p0}, Lflow/History$Builder;->addAll(Ljava/util/Collection;)Lflow/History$Builder;

    move-result-object p0

    const-string p1, "History.emptyBuilder().a\u2026eScreens(currentHistory))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final contains(Lcom/squareup/ui/main/Home;Ljava/lang/Object;Lflow/History;)Z
    .locals 1

    const-string v0, "$this$contains"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-interface {p0, p2}, Lcom/squareup/ui/main/Home;->getHomeScreens(Lflow/History;)Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    invoke-static {p0, p1}, Lkotlin/collections/CollectionsKt;->contains(Ljava/lang/Iterable;Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method public static final getHomeHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;
    .locals 1

    const-string v0, "$this$getHomeHistory"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-static {p0, p1}, Lcom/squareup/ui/main/HomeKt;->buildUpon(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p0

    const-string p1, "buildUpon(currentHistory).build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final isHome(Lcom/squareup/ui/main/Home;Lflow/History;)Z
    .locals 1

    const-string v0, "$this$isHome"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "navigationHistory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-interface {p0, p1}, Lcom/squareup/ui/main/Home;->getHomeScreens(Lflow/History;)Ljava/util/List;

    move-result-object p0

    invoke-static {p1}, Lcom/squareup/container/Histories;->toList(Lflow/History;)Ljava/util/List;

    move-result-object p1

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method
