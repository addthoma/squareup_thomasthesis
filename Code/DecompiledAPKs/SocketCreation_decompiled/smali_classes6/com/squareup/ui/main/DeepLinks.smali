.class public Lcom/squareup/ui/main/DeepLinks;
.super Ljava/lang/Object;
.source "DeepLinks.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/DeepLinks$LinkEvent;
    }
.end annotation


# static fields
.field private static final APPOINTMENT_INTERNAL_HOSTS:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final BNC_LT_DEBUG_PATHS:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final BNC_LT_PATHS:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final INTERNAL_HOSTS:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SQUARE_APPOINTMENTS_DEBUG_SCHEME:Ljava/lang/String; = "square-appointments-debug"

.field private static final SQUARE_APPOINTMENTS_SCHEME:Ljava/lang/String; = "square-appointments"

.field private static final SQUARE_INVOICES_SCHEME:Ljava/lang/String; = "square-invoices"

.field private static final SQUARE_REGISTER_DEBUG_SCHEME:Ljava/lang/String; = "square-register-debug"

.field private static final SQUARE_REGISTER_SCHEME:Ljava/lang/String; = "square-register"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final handlers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/deeplinks/DeepLinkHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainer:Lcom/squareup/ui/main/PosContainer;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const-string v0, "/instant-deposit"

    const-string v1, "/r12-pairing"

    const-string v2, "/payment-tutorial"

    .line 36
    filled-new-array {v0, v1, v2}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/SquareCollections;->unmodifiableList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/main/DeepLinks;->BNC_LT_PATHS:Ljava/util/Collection;

    const-string v0, "/instant-deposit-test"

    const-string v1, "/r12-pairing-test"

    const-string v2, "/payment-tutorial-test"

    .line 41
    filled-new-array {v0, v1, v2}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/SquareCollections;->unmodifiableList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/main/DeepLinks;->BNC_LT_DEBUG_PATHS:Ljava/util/Collection;

    const-string v1, "account-freeze"

    const-string v2, "activity"

    const-string v3, "appointments"

    const-string v4, "checkout"

    const-string v5, "customers"

    const-string v6, "deposits"

    const-string v7, "estimate"

    const-string v8, "help"

    const-string v9, "invoice"

    const-string v10, "items"

    const-string v11, "orders"

    const-string v12, "reports"

    const-string v13, "root"

    const-string v14, "settings"

    const-string v15, "statusbar"

    .line 46
    filled-new-array/range {v1 .. v15}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/SquareCollections;->unmodifiableList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/main/DeepLinks;->INTERNAL_HOSTS:Ljava/util/Collection;

    const-string v0, "appointments-calendar"

    const-string v1, "appointments-clients"

    const-string v2, "appointments-online-booking"

    const-string v3, "appointments-services"

    const-string v4, "items"

    .line 63
    filled-new-array {v0, v1, v2, v3, v4}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/SquareCollections;->unmodifiableList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/main/DeepLinks;->APPOINTMENT_INTERNAL_HOSTS:Ljava/util/Collection;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/main/PosContainer;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Ljava/util/List<",
            "Lcom/squareup/deeplinks/DeepLinkHandler;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/squareup/ui/main/DeepLinks;->analytics:Lcom/squareup/analytics/Analytics;

    .line 78
    iput-object p2, p0, Lcom/squareup/ui/main/DeepLinks;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    .line 79
    iput-object p3, p0, Lcom/squareup/ui/main/DeepLinks;->handlers:Ljava/util/List;

    return-void
.end method

.method private checkHandlers(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 4

    .line 231
    iget-object v0, p0, Lcom/squareup/ui/main/DeepLinks;->handlers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/deeplinks/DeepLinkHandler;

    .line 232
    invoke-interface {v1, p1}, Lcom/squareup/deeplinks/DeepLinkHandler;->handleExternal(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object v1

    .line 233
    invoke-virtual {v1}, Lcom/squareup/deeplinks/DeepLinkResult;->getStatus()Lcom/squareup/deeplinks/DeepLinkStatus;

    move-result-object v2

    sget-object v3, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    if-eq v2, v3, :cond_0

    return-object v1

    .line 237
    :cond_1
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    return-object p1
.end method

.method private forExternal(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 8

    .line 173
    invoke-static {p1}, Lcom/squareup/ui/main/DeepLinks;->isPossiblyDeepLinkUri(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 174
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    return-object p1

    .line 176
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    .line 177
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/4 v3, 0x5

    const/4 v4, 0x4

    const/4 v5, 0x3

    const/4 v6, 0x2

    const/4 v7, 0x1

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v2, "square-alternative.app.link"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_1
    const-string v2, "root"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_2
    const-string v2, "squareup.com"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_3
    const-string v2, "square.test-app.link"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v2, "bnc.lt"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_5
    const-string v2, "square.app.link"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    :goto_0
    if-eqz v1, :cond_3

    if-eq v1, v7, :cond_3

    if-eq v1, v6, :cond_3

    if-eq v1, v5, :cond_3

    if-eq v1, v4, :cond_2

    if-eq v1, v3, :cond_2

    .line 187
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/DeepLinks;->checkHandlers(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    .line 185
    :cond_2
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/DeepLinks;->resolveExternalRoot(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    .line 182
    :cond_3
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/DeepLinks;->resolveExternalHttp(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    :sswitch_data_0
    .sparse-switch
        -0x6d54daa8 -> :sswitch_5
        -0x52893ec1 -> :sswitch_4
        -0x4348448f -> :sswitch_3
        -0xfbdfcf5 -> :sswitch_2
        0x3580e2 -> :sswitch_1
        0x1284b78 -> :sswitch_0
    .end sparse-switch
.end method

.method private forInternal(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 4

    .line 156
    invoke-static {p1}, Lcom/squareup/ui/main/DeepLinks;->isPossiblyDeepLinkUri(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 157
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    return-object p1

    .line 161
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/DeepLinks;->forExternal(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object v0

    .line 162
    invoke-virtual {v0}, Lcom/squareup/deeplinks/DeepLinkResult;->getStatus()Lcom/squareup/deeplinks/DeepLinkStatus;

    move-result-object v1

    sget-object v2, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    if-eq v1, v2, :cond_1

    return-object v0

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/main/DeepLinks;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/main/DeepLinks$LinkEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->DEEP_LINK_REJECTED:Lcom/squareup/analytics/RegisterActionName;

    .line 168
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v3, "com.squareup.deeplinks"

    invoke-direct {v1, v2, p1, v3}, Lcom/squareup/ui/main/DeepLinks$LinkEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 169
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    return-object p1
.end method

.method public static getEverythingAfterScheme(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2

    .line 209
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "://"

    .line 210
    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    return-object p0

    :cond_0
    add-int/lit8 v0, v0, 0x3

    .line 214
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static handleDeepLinkOrThrow(Lcom/squareup/ui/main/DeepLinks;Ljava/lang/String;)V
    .locals 1

    .line 83
    new-instance v0, Lcom/squareup/ui/main/-$$Lambda$DeepLinks$HmUn2wDK0yDkkn1UTm9IywgfoK4;

    invoke-direct {v0, p1}, Lcom/squareup/ui/main/-$$Lambda$DeepLinks$HmUn2wDK0yDkkn1UTm9IywgfoK4;-><init>(Ljava/lang/String;)V

    invoke-static {p0, p1, v0}, Lcom/squareup/ui/main/DeepLinks;->handleDeepLinkOrThrow(Lcom/squareup/ui/main/DeepLinks;Ljava/lang/String;Lrx/functions/Func0;)V

    return-void
.end method

.method public static handleDeepLinkOrThrow(Lcom/squareup/ui/main/DeepLinks;Ljava/lang/String;Lrx/functions/Func0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/DeepLinks;",
            "Ljava/lang/String;",
            "Lrx/functions/Func0<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 87
    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/DeepLinks;->handleInternalDeepLink(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    return-void

    .line 88
    :cond_0
    new-instance p0, Ljava/lang/RuntimeException;

    invoke-interface {p2}, Lrx/functions/Func0;->call()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static isPossiblyDeepLinkUri(Landroid/net/Uri;)Z
    .locals 8

    .line 245
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 246
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v0, :cond_7

    if-nez v1, :cond_0

    goto/16 :goto_4

    .line 250
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v3

    const/4 v4, -0x1

    const/4 v5, 0x3

    const/4 v6, 0x2

    const/4 v7, 0x1

    sparse-switch v3, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v3, "square-appointments-debug"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x6

    goto :goto_1

    :sswitch_1
    const-string v3, "https"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v3, "http"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_3
    const-string v3, "square-appointments"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_4
    const-string v3, "square-invoices"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_5
    const-string v3, "square-register-debug"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_6
    const-string v3, "square-register"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    return v2

    .line 275
    :pswitch_1
    invoke-static {v1}, Lcom/squareup/ui/main/DeepLinks;->supportedByAppointments(Ljava/lang/String;)Z

    move-result p0

    return p0

    :pswitch_2
    return v2

    .line 271
    :pswitch_3
    sget-object p0, Lcom/squareup/ui/main/DeepLinks;->INTERNAL_HOSTS:Ljava/util/Collection;

    invoke-interface {p0, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result p0

    return p0

    .line 253
    :pswitch_4
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_2

    :sswitch_7
    const-string v0, "squareup.com"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v4, 0x1

    goto :goto_2

    :sswitch_8
    const-string v0, "square.test-app.link"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v4, 0x2

    goto :goto_2

    :sswitch_9
    const-string v0, "bnc.lt"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v4, 0x3

    goto :goto_2

    :sswitch_a
    const-string v0, "square.app.link"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v4, 0x0

    :cond_2
    :goto_2
    if-eqz v4, :cond_6

    if-eq v4, v7, :cond_6

    if-eq v4, v6, :cond_5

    if-eq v4, v5, :cond_3

    return v2

    .line 262
    :cond_3
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p0

    .line 263
    sget-object v0, Lcom/squareup/ui/main/DeepLinks;->BNC_LT_PATHS:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_4

    goto :goto_3

    :cond_4
    const/4 v2, 0x1

    :cond_5
    :goto_3
    return v2

    :cond_6
    return v7

    :cond_7
    :goto_4
    return v2

    :sswitch_data_0
    .sparse-switch
        -0x6da2b6ad -> :sswitch_6
        -0x475d96a7 -> :sswitch_5
        -0x1e8cc26a -> :sswitch_4
        -0x1a55a49c -> :sswitch_3
        0x310888 -> :sswitch_2
        0x5f008eb -> :sswitch_1
        0x51a3eaaa -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        -0x6d54daa8 -> :sswitch_a
        -0x52893ec1 -> :sswitch_9
        -0x4348448f -> :sswitch_8
        -0xfbdfcf5 -> :sswitch_7
    .end sparse-switch
.end method

.method static synthetic lambda$handleDeepLinkOrThrow$0(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Can\'t handle deep link: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static normalizeSquareRegisterPath(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .line 224
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object p0

    const-string v0, "square-register"

    invoke-virtual {p0, v0}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p0

    invoke-virtual {p0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 227
    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static normalizeSquareRegisterPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 219
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/ui/main/DeepLinks;->normalizeSquareRegisterPath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private resolveExternalHttp(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 1

    .line 192
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/DeepLinks;->checkHandlers(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    .line 193
    invoke-virtual {p1}, Lcom/squareup/deeplinks/DeepLinkResult;->hasTarget()Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p1

    .line 196
    :cond_0
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->WAITING_FOR_BRANCH:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    return-object p1
.end method

.method private resolveExternalRoot(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 1

    .line 200
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/DeepLinks;->checkHandlers(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    .line 201
    invoke-virtual {p1}, Lcom/squareup/deeplinks/DeepLinkResult;->hasTarget()Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p1

    .line 204
    :cond_0
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    return-object p1
.end method

.method private static supportedByAppointments(Ljava/lang/String;)Z
    .locals 1

    .line 283
    sget-object v0, Lcom/squareup/ui/main/DeepLinks;->APPOINTMENT_INTERNAL_HOSTS:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/squareup/ui/main/DeepLinks;->INTERNAL_HOSTS:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method


# virtual methods
.method public handleExternalDeepLink(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 5

    .line 128
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/DeepLinks;->forExternal(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object v0

    .line 130
    sget-object v1, Lcom/squareup/ui/main/DeepLinks$1;->$SwitchMap$com$squareup$deeplinks$DeepLinkStatus:[I

    invoke-virtual {v0}, Lcom/squareup/deeplinks/DeepLinkResult;->getStatus()Lcom/squareup/deeplinks/DeepLinkStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/deeplinks/DeepLinkStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    const-string v3, "external"

    if-eq v1, v2, :cond_3

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    goto :goto_0

    .line 143
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unexpected result status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/squareup/deeplinks/DeepLinkResult;->getStatus()Lcom/squareup/deeplinks/DeepLinkStatus;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 135
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/main/DeepLinks;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/ui/main/DeepLinks$LinkEvent;

    sget-object v4, Lcom/squareup/analytics/RegisterActionName;->DEEP_LINK_BRANCH:Lcom/squareup/analytics/RegisterActionName;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, v4, p1, v3}, Lcom/squareup/ui/main/DeepLinks$LinkEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-object v0

    .line 146
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/main/DeepLinks;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/ui/main/DeepLinks$LinkEvent;

    sget-object v3, Lcom/squareup/analytics/RegisterActionName;->DEEP_LINK_HANDLED:Lcom/squareup/analytics/RegisterActionName;

    .line 147
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v4, "com.squareup.deeplinks"

    invoke-direct {v2, v3, p1, v4}, Lcom/squareup/ui/main/DeepLinks$LinkEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-object v0

    .line 132
    :cond_3
    iget-object v1, p0, Lcom/squareup/ui/main/DeepLinks;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/ui/main/DeepLinks$LinkEvent;

    sget-object v4, Lcom/squareup/analytics/RegisterActionName;->DEEP_LINK_REJECTED:Lcom/squareup/analytics/RegisterActionName;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, v4, p1, v3}, Lcom/squareup/ui/main/DeepLinks$LinkEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-object v0
.end method

.method handleInternalDeepLink(Landroid/net/Uri;)Z
    .locals 5

    .line 105
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/DeepLinks;->forInternal(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object v0

    .line 107
    sget-object v1, Lcom/squareup/ui/main/DeepLinks$1;->$SwitchMap$com$squareup$deeplinks$DeepLinkStatus:[I

    invoke-virtual {v0}, Lcom/squareup/deeplinks/DeepLinkResult;->getStatus()Lcom/squareup/deeplinks/DeepLinkStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/deeplinks/DeepLinkStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v3, 0x2

    if-eq v1, v3, :cond_1

    const/4 v3, 0x3

    if-eq v1, v3, :cond_1

    const/4 v3, 0x4

    if-ne v1, v3, :cond_0

    .line 115
    iget-object v1, p0, Lcom/squareup/ui/main/DeepLinks;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-virtual {v0}, Lcom/squareup/deeplinks/DeepLinkResult;->getFactory()Lcom/squareup/ui/main/HistoryFactory;

    move-result-object v0

    sget-object v3, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-interface {v1, v0, v3}, Lcom/squareup/ui/main/PosContainer;->resetHistory(Lcom/squareup/ui/main/HistoryFactory;Lflow/Direction;)V

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/main/DeepLinks;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/main/DeepLinks$LinkEvent;

    sget-object v3, Lcom/squareup/analytics/RegisterActionName;->DEEP_LINK_HANDLED:Lcom/squareup/analytics/RegisterActionName;

    .line 123
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v4, "com.squareup.deeplinks"

    invoke-direct {v1, v3, p1, v4}, Lcom/squareup/ui/main/DeepLinks$LinkEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return v2

    .line 119
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unexpected result status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/squareup/deeplinks/DeepLinkResult;->getStatus()Lcom/squareup/deeplinks/DeepLinkStatus;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public handleInternalDeepLink(Ljava/lang/String;)Z
    .locals 0

    .line 97
    invoke-static {p1}, Lcom/squareup/ui/main/DeepLinks;->normalizeSquareRegisterPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/DeepLinks;->handleInternalDeepLink(Landroid/net/Uri;)Z

    move-result p1

    return p1
.end method
