.class final Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$1;
.super Ljava/lang/Object;
.source "MainActivityContainer.kt"

# interfaces
.implements Lcom/squareup/container/DispatchStep;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/MainActivityContainer;->buildDispatchPipeline(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u000e\u0010\u0003\u001a\n \u0002*\u0004\u0018\u00010\u00040\u00042\u000e\u0010\u0005\u001a\n \u0002*\u0004\u0018\u00010\u00060\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/container/DispatchStep$Result;",
        "kotlin.jvm.PlatformType",
        "traversal",
        "Lflow/Traversal;",
        "<anonymous parameter 1>",
        "Lflow/TraversalCallback;",
        "dispatch"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/MainActivityContainer;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/MainActivityContainer;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final dispatch(Lflow/Traversal;Lflow/TraversalCallback;)Lcom/squareup/container/DispatchStep$Result;
    .locals 0

    .line 350
    iget-object p2, p0, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-static {p2}, Lcom/squareup/ui/main/MainActivityContainer;->access$getApiTransactionController$p(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/squareup/api/ApiTransactionController;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/api/ApiTransactionController;->abortOnEscapeFromApiTransaction(Lflow/Traversal;)Lcom/squareup/container/DispatchStep$Result;

    move-result-object p1

    return-object p1
.end method
