.class public Lcom/squareup/ui/main/errors/PaymentInputHandler;
.super Ljava/lang/Object;
.source "PaymentInputHandler.kt"

# interfaces
.implements Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;
.implements Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;
.implements Lcom/squareup/ui/main/SmartPaymentResultHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/errors/PaymentInputHandler$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00de\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0016\u0018\u0000 W2\u00020\u00012\u00020\u00022\u00020\u0003:\u0001WBw\u0008\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u0012\u0006\u0010\u001e\u001a\u00020\u001f\u00a2\u0006\u0002\u0010 J\u0008\u00105\u001a\u000204H\u0016J\u0008\u00106\u001a\u000204H\u0016J\u0008\u00107\u001a\u000204H\u0016J\u0010\u00108\u001a\u0002042\u0006\u00103\u001a\u000200H\u0002J\u0010\u00109\u001a\u0002042\u0006\u0010:\u001a\u00020;H\u0016J\u001e\u0010<\u001a\u0002042\u0006\u0010=\u001a\u00020>2\u000c\u0010?\u001a\u0008\u0012\u0004\u0012\u0002040@H\u0002J\u0010\u0010A\u001a\u0002042\u0006\u0010=\u001a\u00020>H\u0016J\u0010\u0010B\u001a\u0002042\u0006\u0010C\u001a\u00020DH\u0016J\u0008\u0010E\u001a\u000204H\u0016J\u0010\u0010F\u001a\u0002042\u0006\u0010G\u001a\u00020HH\u0016J\u0010\u0010I\u001a\u0002042\u0006\u0010J\u001a\u000200H\u0016J\u0008\u0010K\u001a\u000204H\u0016J\u0010\u0010L\u001a\u0002042\u0006\u0010M\u001a\u00020NH\u0016J\u0010\u0010O\u001a\u0002042\u0006\u0010P\u001a\u00020QH\u0014J\u0010\u0010R\u001a\u0002042\u0006\u0010S\u001a\u00020TH\u0016J\u0010\u0010U\u001a\u0002042\u0006\u0010G\u001a\u00020HH\u0016J\u0010\u0010V\u001a\u0002042\u0006\u0010G\u001a\u00020HH\u0016R\u000e\u0010!\u001a\u00020\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020$X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\'0&X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008(\u0010)R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010*\u001a\u0008\u0012\u0004\u0012\u00020\'0+X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008,\u0010-R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R)\u0010.\u001a\u001d\u0012\u0013\u0012\u001100\u00a2\u0006\u000c\u00081\u0012\u0008\u00082\u0012\u0004\u0008\u0008(3\u0012\u0004\u0012\u0002040/X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006X"
    }
    d2 = {
        "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
        "Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;",
        "Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;",
        "Lcom/squareup/ui/main/SmartPaymentResultHandler;",
        "nfcProcessor",
        "Lcom/squareup/ui/NfcProcessor;",
        "mainThread",
        "Lcom/squareup/thread/executor/MainThread;",
        "badBus",
        "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
        "emvDipScreenHandler",
        "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
        "smartPaymentFlowStarter",
        "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
        "readerHudManager",
        "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
        "dippedCardTracker",
        "Lcom/squareup/cardreader/DippedCardTracker;",
        "cardReaderHubUtils",
        "Lcom/squareup/cardreader/CardReaderHubUtils;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "tenderStarter",
        "Lcom/squareup/ui/tender/TenderStarter;",
        "readerIssueScreenRequestSink",
        "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
        "readerIssueNavigator",
        "Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;",
        "swipeValidator",
        "Lcom/squareup/swipe/SwipeValidator;",
        "giftCards",
        "Lcom/squareup/giftcard/GiftCards;",
        "(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/giftcard/GiftCards;)V",
        "autoFieldRestartRunnable",
        "Ljava/lang/Runnable;",
        "busSubs",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "events",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ui/main/errors/PaymentEvent;",
        "getEvents",
        "()Lio/reactivex/Observable;",
        "publishEvent",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "getPublishEvent",
        "()Lcom/jakewharton/rxrelay2/PublishRelay;",
        "readerIssueRequestHandler",
        "Lkotlin/reflect/KFunction1;",
        "Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;",
        "Lkotlin/ParameterName;",
        "name",
        "request",
        "",
        "cancelPayment",
        "cancelPaymentAndDestroy",
        "destroy",
        "handleReaderIssueScreenRequest",
        "handleSmartPaymentResult",
        "result",
        "Lcom/squareup/ui/main/SmartPaymentResult;",
        "initializeMonitoring",
        "initialDelayMillis",
        "",
        "startMonitoringAction",
        "Lkotlin/Function0;",
        "initializeWithNfcFieldAutoRestart",
        "initializeWithNfcFieldOn",
        "nfcStatusDisplay",
        "Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;",
        "initializeWithoutNfc",
        "navigateForEmvPayment",
        "cardReaderInfo",
        "Lcom/squareup/cardreader/CardReaderInfo;",
        "navigateForReaderIssueScreen",
        "screenRequest",
        "navigateForSplitTenderEmvPayment",
        "onNfcAuthorizationRequestReceived",
        "authData",
        "Lcom/squareup/ui/NfcAuthData;",
        "onSwipeFailed",
        "failedSwipe",
        "Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;",
        "onSwipeSuccess",
        "successfulSwipe",
        "Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;",
        "processEmvCardInserted",
        "processEmvCardRemoved",
        "Companion",
        "cardreader-ui_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/main/errors/PaymentInputHandler$Companion;

.field private static final NO_DELAY:I


# instance fields
.field private autoFieldRestartRunnable:Ljava/lang/Runnable;

.field private final badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

.field private final busSubs:Lio/reactivex/disposables/CompositeDisposable;

.field private final cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

.field private final dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

.field private final emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

.field private final events:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/main/errors/PaymentEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCards:Lcom/squareup/giftcard/GiftCards;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final nfcProcessor:Lcom/squareup/ui/NfcProcessor;

.field private final publishEvent:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/ui/main/errors/PaymentEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final readerHudManager:Lcom/squareup/cardreader/dipper/ReaderHudManager;

.field private final readerIssueNavigator:Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;

.field private final readerIssueRequestHandler:Lkotlin/reflect/KFunction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/reflect/KFunction<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

.field private final smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

.field private final swipeValidator:Lcom/squareup/swipe/SwipeValidator;

.field private final tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/main/errors/PaymentInputHandler$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/main/errors/PaymentInputHandler$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->Companion:Lcom/squareup/ui/main/errors/PaymentInputHandler$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/giftcard/GiftCards;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "nfcProcessor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThread"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "badBus"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emvDipScreenHandler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "smartPaymentFlowStarter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "readerHudManager"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dippedCardTracker"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReaderHubUtils"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transaction"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderStarter"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "readerIssueScreenRequestSink"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "readerIssueNavigator"

    invoke-static {p12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "swipeValidator"

    invoke-static {p13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "giftCards"

    invoke-static {p14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    iput-object p2, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iput-object p3, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    iput-object p4, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    iput-object p5, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    iput-object p6, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->readerHudManager:Lcom/squareup/cardreader/dipper/ReaderHudManager;

    iput-object p7, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    iput-object p8, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    iput-object p9, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p10, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    iput-object p11, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    iput-object p12, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->readerIssueNavigator:Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;

    iput-object p13, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->swipeValidator:Lcom/squareup/swipe/SwipeValidator;

    iput-object p14, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->giftCards:Lcom/squareup/giftcard/GiftCards;

    .line 53
    new-instance p1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->busSubs:Lio/reactivex/disposables/CompositeDisposable;

    .line 54
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<PaymentEvent>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->publishEvent:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 57
    new-instance p1, Lcom/squareup/ui/main/errors/PaymentInputHandler$readerIssueRequestHandler$1;

    move-object p2, p0

    check-cast p2, Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-direct {p1, p2}, Lcom/squareup/ui/main/errors/PaymentInputHandler$readerIssueRequestHandler$1;-><init>(Lcom/squareup/ui/main/errors/PaymentInputHandler;)V

    iput-object p1, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->readerIssueRequestHandler:Lkotlin/reflect/KFunction;

    .line 59
    sget-object p1, Lcom/squareup/ui/main/errors/PaymentInputHandler$autoFieldRestartRunnable$1;->INSTANCE:Lcom/squareup/ui/main/errors/PaymentInputHandler$autoFieldRestartRunnable$1;

    check-cast p1, Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->autoFieldRestartRunnable:Ljava/lang/Runnable;

    .line 62
    iget-object p1, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->publishEvent:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast p1, Lio/reactivex/Observable;

    iput-object p1, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->events:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getCardReaderHubUtils$p(Lcom/squareup/ui/main/errors/PaymentInputHandler;)Lcom/squareup/cardreader/CardReaderHubUtils;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    return-object p0
.end method

.method public static final synthetic access$getDippedCardTracker$p(Lcom/squareup/ui/main/errors/PaymentInputHandler;)Lcom/squareup/cardreader/DippedCardTracker;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    return-object p0
.end method

.method public static final synthetic access$getNfcProcessor$p(Lcom/squareup/ui/main/errors/PaymentInputHandler;)Lcom/squareup/ui/NfcProcessor;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    return-object p0
.end method

.method public static final synthetic access$getReaderHudManager$p(Lcom/squareup/ui/main/errors/PaymentInputHandler;)Lcom/squareup/cardreader/dipper/ReaderHudManager;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->readerHudManager:Lcom/squareup/cardreader/dipper/ReaderHudManager;

    return-object p0
.end method

.method public static final synthetic access$getTransaction$p(Lcom/squareup/ui/main/errors/PaymentInputHandler;)Lcom/squareup/payment/Transaction;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->transaction:Lcom/squareup/payment/Transaction;

    return-object p0
.end method

.method public static final synthetic access$handleReaderIssueScreenRequest(Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->handleReaderIssueScreenRequest(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    return-void
.end method

.method private final handleReaderIssueScreenRequest(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V
    .locals 2

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->publishEvent:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/ui/main/errors/ReportReaderIssue;

    invoke-direct {v1, p1}, Lcom/squareup/ui/main/errors/ReportReaderIssue;-><init>(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private final initializeMonitoring(JLkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 84
    new-instance v0, Lcom/squareup/ui/main/errors/PaymentInputHandler$initializeMonitoring$1;

    invoke-direct {v0, p0, p3}, Lcom/squareup/ui/main/errors/PaymentInputHandler$initializeMonitoring$1;-><init>(Lcom/squareup/ui/main/errors/PaymentInputHandler;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Ljava/lang/Runnable;

    iput-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->autoFieldRestartRunnable:Ljava/lang/Runnable;

    .line 93
    iget-object p3, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->autoFieldRestartRunnable:Ljava/lang/Runnable;

    invoke-interface {p3, v0, p1, p2}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    .line 95
    iget-object p1, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    move-object p2, p0

    check-cast p2, Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/NfcProcessor;->registerNfcAuthDelegate(Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;)V

    .line 96
    iget-object p1, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    move-object p2, p0

    check-cast p2, Lcom/squareup/ui/main/SmartPaymentResultHandler;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/NfcProcessor;->registerSmartPaymentResultDelegate(Lcom/squareup/ui/main/SmartPaymentResultHandler;)V

    .line 97
    invoke-virtual {p0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->initializeWithoutNfc()V

    return-void
.end method


# virtual methods
.method public cancelPayment()V
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    invoke-virtual {v0}, Lcom/squareup/ui/NfcProcessor;->stopMonitoringSoon()V

    return-void
.end method

.method public cancelPaymentAndDestroy()V
    .locals 0

    .line 111
    invoke-virtual {p0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->cancelPayment()V

    .line 112
    invoke-virtual {p0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->destroy()V

    return-void
.end method

.method public destroy()V
    .locals 2

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->autoFieldRestartRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->busSubs:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    move-object v1, p0

    check-cast v1, Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->removeEmvCardInsertRemoveProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)Z

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    move-object v1, p0

    check-cast v1, Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/NfcProcessor;->unregisterNfcAuthDelegate(Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;)V

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    move-object v1, p0

    check-cast v1, Lcom/squareup/ui/main/SmartPaymentResultHandler;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/NfcProcessor;->unregisterSmartPaymentResultDelegate(Lcom/squareup/ui/main/SmartPaymentResultHandler;)V

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    iget-object v1, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->readerIssueRequestHandler:Lkotlin/reflect/KFunction;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->unsetReaderIssueScreenRequestHandler(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public getEvents()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/main/errors/PaymentEvent;",
            ">;"
        }
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->events:Lio/reactivex/Observable;

    return-object v0
.end method

.method protected final getPublishEvent()Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/ui/main/errors/PaymentEvent;",
            ">;"
        }
    .end annotation

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->publishEvent:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public handleSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V
    .locals 2

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->publishEvent:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/ui/main/errors/TakeTapPayment;

    invoke-direct {v1, p1}, Lcom/squareup/ui/main/errors/TakeTapPayment;-><init>(Lcom/squareup/ui/main/SmartPaymentResult;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public initializeWithNfcFieldAutoRestart(J)V
    .locals 1

    .line 66
    new-instance v0, Lcom/squareup/ui/main/errors/PaymentInputHandler$initializeWithNfcFieldAutoRestart$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/errors/PaymentInputHandler$initializeWithNfcFieldAutoRestart$1;-><init>(Lcom/squareup/ui/main/errors/PaymentInputHandler;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->initializeMonitoring(JLkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public initializeWithNfcFieldOn(Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;)V
    .locals 3

    const-string v0, "nfcStatusDisplay"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    new-instance v0, Lcom/squareup/ui/main/errors/PaymentInputHandler$initializeWithNfcFieldOn$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/main/errors/PaymentInputHandler$initializeWithNfcFieldOn$1;-><init>(Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    const-wide/16 v1, 0x0

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->initializeMonitoring(JLkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public initializeWithoutNfc()V
    .locals 6

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->busSubs:Lio/reactivex/disposables/CompositeDisposable;

    const/4 v1, 0x2

    new-array v1, v1, [Lio/reactivex/disposables/Disposable;

    .line 102
    iget-object v2, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    invoke-virtual {v2}, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->successfulSwipes()Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/main/errors/PaymentInputHandler$initializeWithoutNfc$1;

    move-object v4, p0

    check-cast v4, Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-direct {v3, v4}, Lcom/squareup/ui/main/errors/PaymentInputHandler$initializeWithoutNfc$1;-><init>(Lcom/squareup/ui/main/errors/PaymentInputHandler;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    new-instance v5, Lcom/squareup/ui/main/errors/PaymentInputHandler$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v5, v3}, Lcom/squareup/ui/main/errors/PaymentInputHandler$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v5, Lio/reactivex/functions/Consumer;

    invoke-virtual {v2, v5}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 103
    iget-object v2, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    invoke-virtual {v2}, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->failedSwipes()Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/main/errors/PaymentInputHandler$initializeWithoutNfc$2;

    invoke-direct {v3, v4}, Lcom/squareup/ui/main/errors/PaymentInputHandler$initializeWithoutNfc$2;-><init>(Lcom/squareup/ui/main/errors/PaymentInputHandler;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    new-instance v4, Lcom/squareup/ui/main/errors/PaymentInputHandler$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v4, v3}, Lcom/squareup/ui/main/errors/PaymentInputHandler$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v4, Lio/reactivex/functions/Consumer;

    invoke-virtual {v2, v4}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 101
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->addAll([Lio/reactivex/disposables/Disposable;)Z

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    move-object v1, p0

    check-cast v1, Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->addEmvCardInsertRemoveProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    iget-object v1, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->readerIssueRequestHandler:Lkotlin/reflect/KFunction;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->setReaderIssueScreenRequestHandler(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public navigateForEmvPayment(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    const-string v0, "cardReaderInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->getOrContinueEmvTenderMaybeAfterErrorResult(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/ui/main/SmartPaymentResult;

    move-result-object p1

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->navigateForSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V

    return-void
.end method

.method public navigateForReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V
    .locals 1

    const-string v0, "screenRequest"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->readerIssueNavigator:Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;->navigateToReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    return-void
.end method

.method public navigateForSplitTenderEmvPayment()V
    .locals 2

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/ui/tender/TenderStarter;->completeTenderAndAdvance(Z)Z

    return-void
.end method

.method public onNfcAuthorizationRequestReceived(Lcom/squareup/ui/NfcAuthData;)V
    .locals 2

    const-string v0, "authData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    .line 187
    iget-object v1, p1, Lcom/squareup/ui/NfcAuthData;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 188
    iget-object p1, p1, Lcom/squareup/ui/NfcAuthData;->authorizationData:[B

    .line 186
    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->getContinueContactlessTenderMaybeAfterErrorResult(Lcom/squareup/cardreader/CardReaderInfo;[B)Lcom/squareup/ui/main/SmartPaymentResult;

    move-result-object p1

    const-string v0, "result"

    .line 190
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->handleSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V

    return-void
.end method

.method protected onSwipeFailed(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 2

    const-string v0, "failedSwipe"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    iget-boolean p1, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;->swipeStraight:Z

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/ui/main/errors/SwipeStraight;->INSTANCE:Lcom/squareup/ui/main/errors/SwipeStraight;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/ui/main/errors/TryAgain;->INSTANCE:Lcom/squareup/ui/main/errors/TryAgain;

    :goto_0
    check-cast p1, Lcom/squareup/ui/main/errors/PaymentError;

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->publishEvent:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/ui/main/errors/CardFailed;

    invoke-direct {v1, p1}, Lcom/squareup/ui/main/errors/CardFailed;-><init>(Lcom/squareup/ui/main/errors/PaymentError;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onSwipeSuccess(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 3

    const-string v0, "successfulSwipe"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getTenderAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    const-string v1, "transaction.requireBillPayment()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_0

    .line 149
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 152
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->swipeValidator:Lcom/squareup/swipe/SwipeValidator;

    iget-object v2, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->card:Lcom/squareup/Card;

    invoke-virtual {v1, v2, v0}, Lcom/squareup/swipe/SwipeValidator;->isInAuthRange(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->giftCards:Lcom/squareup/giftcard/GiftCards;

    .line 154
    iget-object p1, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->card:Lcom/squareup/Card;

    .line 153
    invoke-virtual {v0, p1}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 156
    sget-object p1, Lcom/squareup/ui/main/errors/PaymentOutOfRangeGiftCard;->INSTANCE:Lcom/squareup/ui/main/errors/PaymentOutOfRangeGiftCard;

    goto :goto_1

    :cond_2
    sget-object p1, Lcom/squareup/ui/main/errors/PaymentOutOfRange;->INSTANCE:Lcom/squareup/ui/main/errors/PaymentOutOfRange;

    :goto_1
    check-cast p1, Lcom/squareup/ui/main/errors/PaymentError;

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->publishEvent:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/ui/main/errors/CardFailed;

    invoke-direct {v1, p1}, Lcom/squareup/ui/main/errors/CardFailed;-><init>(Lcom/squareup/ui/main/errors/PaymentError;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void

    .line 161
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->publishEvent:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/ui/main/errors/TakeSwipePayment;

    invoke-direct {v1, p1}, Lcom/squareup/ui/main/errors/TakeSwipePayment;-><init>(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    const-string v0, "cardReaderInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->paymentIsWithinRange()Z

    move-result v0

    if-nez v0, :cond_0

    .line 172
    iget-object p1, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->publishEvent:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v0, Lcom/squareup/ui/main/errors/CardFailed;

    sget-object v1, Lcom/squareup/ui/main/errors/PaymentOutOfRange;->INSTANCE:Lcom/squareup/ui/main/errors/PaymentOutOfRange;

    check-cast v1, Lcom/squareup/ui/main/errors/PaymentError;

    invoke-direct {v0, v1}, Lcom/squareup/ui/main/errors/CardFailed;-><init>(Lcom/squareup/ui/main/errors/PaymentError;)V

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->publishEvent:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/ui/main/errors/TakeDipPayment;

    invoke-direct {v1, p1}, Lcom/squareup/ui/main/errors/TakeDipPayment;-><init>(Lcom/squareup/cardreader/CardReaderInfo;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    const-string v0, "cardReaderInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresenceRequired()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 181
    iget-object p1, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler;->autoFieldRestartRunnable:Ljava/lang/Runnable;

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :cond_0
    return-void
.end method
