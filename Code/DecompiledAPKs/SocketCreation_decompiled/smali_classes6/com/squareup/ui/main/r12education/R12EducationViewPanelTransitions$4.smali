.class Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$4;
.super Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;
.source "R12EducationViewPanelTransitions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->populateStandardPanelTransitions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)V
    .locals 0

    .line 196
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    invoke-direct {p0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;-><init>()V

    return-void
.end method


# virtual methods
.method tweenElement(Lcom/squareup/ui/main/r12education/R12EducationView$Element;Landroid/view/View;FLandroid/view/ViewGroup;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 1

    .line 200
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$14;->$SwitchMap$com$squareup$ui$main$r12education$R12EducationView$Element:[I

    invoke-virtual {p1}, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x4

    if-eq p1, v0, :cond_2

    const/4 p4, 0x5

    const/high16 p5, 0x3f800000    # 1.0f

    if-eq p1, p4, :cond_1

    const/4 p4, 0x6

    if-eq p1, p4, :cond_0

    .line 242
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->hide(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    const p1, 0x3f733333    # 0.95f

    .line 235
    invoke-static {}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$200()Landroid/view/animation/Interpolator;

    move-result-object p4

    invoke-static {p3, p1, p5, p4}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFFLandroid/view/animation/Interpolator;)F

    move-result p1

    .line 236
    invoke-static {p2, p1}, Lcom/squareup/ui/main/r12education/Tweens;->fadeIn(Landroid/view/View;F)V

    goto :goto_0

    :cond_1
    const/high16 p1, 0x3f000000    # 0.5f

    .line 225
    invoke-static {}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$200()Landroid/view/animation/Interpolator;

    move-result-object p4

    invoke-static {p3, p1, p5, p4}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFFLandroid/view/animation/Interpolator;)F

    move-result p1

    .line 227
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->show(Landroid/view/View;)V

    .line 228
    iget-object p3, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    invoke-static {p3}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$300(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;

    move-result-object p3

    iget p3, p3, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;->phoneApplePayBottomMargin:I

    neg-int p3, p3

    invoke-static {p2, p7, p3}, Lcom/squareup/ui/main/r12education/Tweens;->alignBottomEdgeToCenterY(Landroid/view/View;Landroid/view/View;I)V

    .line 229
    iget-object p3, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    .line 230
    invoke-static {p3}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$300(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;

    move-result-object p3

    iget p3, p3, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;->phoneApplePayLeftMargin:I

    neg-int p3, p3

    .line 229
    invoke-static {p2, p7, p6, p3, p1}, Lcom/squareup/ui/main/r12education/Tweens;->slideInRightToAlignEdgeToCenter(Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;IF)V

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    const p6, 0x3ea8f5c3    # 0.33f

    .line 204
    invoke-static {}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$400()Landroid/view/animation/Interpolator;

    move-result-object p7

    invoke-static {p3, p1, p6, p7}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFFLandroid/view/animation/Interpolator;)F

    move-result p1

    .line 206
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->show(Landroid/view/View;)V

    .line 207
    invoke-static {p2, p5}, Lcom/squareup/ui/main/r12education/Tweens;->centerY(Landroid/view/View;Landroid/view/View;)V

    .line 208
    iget-object p3, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    .line 209
    invoke-static {p3}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$300(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;

    move-result-object p3

    iget p3, p3, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;->chipCardMargin:I

    .line 208
    invoke-static {p2, p5, p4, p3, p1}, Lcom/squareup/ui/main/r12education/Tweens;->slideOutRightFromAlignCenter(Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;IF)V

    goto :goto_0

    :cond_3
    const p1, 0x3dcccccd    # 0.1f

    const p4, 0x3f59999a    # 0.85f

    .line 215
    invoke-static {p3, p1, p4}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFF)F

    move-result p1

    .line 217
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->show(Landroid/view/View;)V

    .line 218
    invoke-static {p2, p5, p7, p1}, Lcom/squareup/ui/main/r12education/Tweens;->translateCenterY(Landroid/view/View;Landroid/view/View;Landroid/view/View;F)V

    .line 219
    invoke-static {p2, p5, p7, p1}, Lcom/squareup/ui/main/r12education/Tweens;->translateCenterX(Landroid/view/View;Landroid/view/View;Landroid/view/View;F)V

    :goto_0
    return-void
.end method
