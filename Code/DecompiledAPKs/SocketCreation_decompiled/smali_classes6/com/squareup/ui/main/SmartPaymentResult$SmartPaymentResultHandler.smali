.class public interface abstract Lcom/squareup/ui/main/SmartPaymentResult$SmartPaymentResultHandler;
.super Ljava/lang/Object;
.source "SmartPaymentResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/SmartPaymentResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SmartPaymentResultHandler"
.end annotation


# virtual methods
.method public abstract onResult(Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowHardwarePinRequested;)V
.end method

.method public abstract onResult(Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowNavigate;)V
.end method

.method public abstract onResult(Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowPinRequested;)V
.end method

.method public abstract onResult(Lcom/squareup/ui/main/SmartPaymentResult$DoNothing;)V
.end method

.method public abstract onResult(Lcom/squareup/ui/main/SmartPaymentResult$EmvNavigateAndAuthorize;)V
.end method

.method public abstract onResult(Lcom/squareup/ui/main/SmartPaymentResult$PreparePaymentNavigate;)V
.end method
