.class public Lcom/squareup/ui/main/MainActivityContainer;
.super Lcom/squareup/container/ContainerPresenter;
.source "MainActivityContainer.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/MainActivityContainer$RedirectPipelineDecorator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/ContainerPresenter<",
        "Lcom/squareup/ui/main/MainActivityScope$View;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMainActivityContainer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MainActivityContainer.kt\ncom/squareup/ui/main/MainActivityContainer\n*L\n1#1,532:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00c8\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0014\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0017\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0093\u0001B\u00fd\u0001\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000e\u0008\u0001\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0006\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0006\u0012\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0006\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u0012\u0006\u0010\u001e\u001a\u00020\u001f\u0012\u0006\u0010 \u001a\u00020!\u0012\u0006\u0010\"\u001a\u00020#\u0012\u0006\u0010$\u001a\u00020%\u0012\u0006\u0010&\u001a\u00020\'\u0012\u0006\u0010(\u001a\u00020)\u0012\u0006\u0010*\u001a\u00020+\u0012\u000c\u0010,\u001a\u0008\u0012\u0004\u0012\u00020-0\u0006\u0012\u0006\u0010.\u001a\u00020/\u0012\u0006\u00100\u001a\u000201\u0012\u0006\u00102\u001a\u000203\u0012\u000c\u00104\u001a\u0008\u0012\u0004\u0012\u0002050\u0006\u0012\u0006\u00106\u001a\u000207\u00a2\u0006\u0002\u00108J\u0006\u0010b\u001a\u00020cJ\u0016\u0010d\u001a\u00020e2\u000c\u0010f\u001a\u0008\u0012\u0004\u0012\u00020h0gH\u0014J\u000e\u0010i\u001a\u0008\u0012\u0004\u0012\u00020k0jH\u0014J\u0014\u0010l\u001a\u00020G2\n\u0010m\u001a\u0006\u0012\u0002\u0008\u00030nH\u0003J\u0010\u0010o\u001a\u00020e2\u0006\u0010p\u001a\u00020\u0002H\u0016J\u0010\u0010q\u001a\u00020r2\u0006\u0010p\u001a\u00020\u0002H\u0014J\u0010\u0010s\u001a\u00020t2\u0006\u0010u\u001a\u00020vH\u0014J\u0012\u0010w\u001a\u00020<2\u0008\u0010x\u001a\u0004\u0018\u00010<H\u0002J\u0010\u0010y\u001a\u00020e2\u0006\u0010z\u001a\u00020{H\u0002J\u0010\u0010|\u001a\u00020e2\u0006\u0010z\u001a\u00020{H\u0002J\u0008\u0010}\u001a\u00020eH\u0016J\u0011\u0010~\u001a\u00020e2\u0007\u0010\u007f\u001a\u00030\u0080\u0001H\u0014J\t\u0010\u0081\u0001\u001a\u00020eH\u0014J\u0015\u0010\u0082\u0001\u001a\u00020e2\n\u0010\u0083\u0001\u001a\u0005\u0018\u00010\u0084\u0001H\u0016J\u0013\u0010\u0085\u0001\u001a\u00020e2\u0008\u0010\u0086\u0001\u001a\u00030\u0087\u0001H\u0002J\u0007\u0010\u0088\u0001\u001a\u00020@J\u0016\u0010\u0089\u0001\u001a\u0005\u0018\u00010\u008a\u00012\u0008\u0010\u008b\u0001\u001a\u00030\u008c\u0001H\u0002J\t\u0010\u008d\u0001\u001a\u00020eH\u0002J\u001e\u0010\u008d\u0001\u001a\u00020e2\t\u0010\u008e\u0001\u001a\u0004\u0018\u00010\\2\u0008\u0010\u008f\u0001\u001a\u00030\u0090\u0001H\u0002J\t\u0010\u0091\u0001\u001a\u00020eH\u0002J\t\u0010\u0092\u0001\u001a\u00020GH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u00106\u001a\u000207X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00089\u0010:R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020+X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010;\u001a\u00020<8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008=\u0010>R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010?\u001a\u0004\u0018\u00010@8VX\u0097\u0004\u00a2\u0006\u000c\u0012\u0004\u0008A\u0010B\u001a\u0004\u0008C\u0010DR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010E\u001a\u0010\u0012\u000c\u0012\n H*\u0004\u0018\u00010G0G0FX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020)X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010I\u001a\u00020\u00178BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008L\u0010M\u001a\u0004\u0008J\u0010KR\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010N\u001a\u00020-8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008Q\u0010M\u001a\u0004\u0008O\u0010PR\u000e\u0010$\u001a\u00020%X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010R\u001a\u0010\u0012\u000c\u0012\n H*\u0004\u0018\u00010<0<0FX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010S\u001a\u0002058BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008V\u0010M\u001a\u0004\u0008T\u0010UR\u001b\u0010W\u001a\u00020\u00078BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008Z\u0010M\u001a\u0004\u0008X\u0010YR\u000e\u0010.\u001a\u00020/X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00102\u001a\u000203X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\'X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010[\u001a\u00020\\X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010]\u001a\u0010\u0012\u000c\u0012\n H*\u0004\u0018\u00010<0<0FX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00100\u001a\u000201X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010^\u001a\u00020\t8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008a\u0010M\u001a\u0004\u0008_\u0010`\u00a8\u0006\u0094\u0001"
    }
    d2 = {
        "Lcom/squareup/ui/main/MainActivityContainer;",
        "Lcom/squareup/container/ContainerPresenter;",
        "Lcom/squareup/ui/main/MainActivityScope$View;",
        "activityBackHandler",
        "Lcom/squareup/ui/MainActivityBackHandler;",
        "lazyRegistrar",
        "Ldagger/Lazy;",
        "Lmortar/Scoped;",
        "lazyX2ScreenRunner",
        "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "emvSwipePassthroughEnabler",
        "Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "glassConfirmController",
        "Lcom/squareup/widgets/glass/GlassConfirmController;",
        "jailKeeper",
        "Lcom/squareup/jailkeeper/JailKeeper;",
        "transactionMetrics",
        "Lcom/squareup/ui/main/TransactionMetrics;",
        "lazyIntentParser",
        "Lcom/squareup/ui/main/IntentParser;",
        "lazyApplets",
        "Lcom/squareup/applet/Applets;",
        "passcodeEmployeeManagement",
        "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
        "permissionGatekeeper",
        "Lcom/squareup/permissions/PermissionGatekeeper;",
        "apiTransactionController",
        "Lcom/squareup/api/ApiTransactionController;",
        "systemPermissionsPresenter",
        "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
        "screenNavigationLogger",
        "Lcom/squareup/navigation/ScreenNavigationLogger;",
        "navigationListener",
        "Lcom/squareup/navigation/NavigationListener;",
        "softInputPresenter",
        "Lcom/squareup/ui/SoftInputPresenter;",
        "home",
        "Lcom/squareup/ui/main/Home;",
        "deepLinkHelper",
        "Lcom/squareup/analytics/DeepLinkHelper;",
        "lazyLockScreenMonitor",
        "Lcom/squareup/permissions/ui/LockScreenMonitor;",
        "rootViewSetup",
        "Lcom/squareup/rootview/RootViewSetup;",
        "unsyncedOpenTicketsSpinner",
        "Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;",
        "salesReportDetailLevelHolder",
        "Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;",
        "lazyRedirectPipelineDecorator",
        "Lcom/squareup/ui/main/MainActivityContainer$RedirectPipelineDecorator;",
        "additionalContainerLayerSetup",
        "Lcom/squareup/container/AdditionalContainerLayerSetup;",
        "(Lcom/squareup/ui/MainActivityBackHandler;Ldagger/Lazy;Ldagger/Lazy;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/settings/server/Features;Lcom/squareup/widgets/glass/GlassConfirmController;Lcom/squareup/jailkeeper/JailKeeper;Lcom/squareup/ui/main/TransactionMetrics;Ldagger/Lazy;Ldagger/Lazy;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/navigation/ScreenNavigationLogger;Lcom/squareup/navigation/NavigationListener;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/ui/main/Home;Lcom/squareup/analytics/DeepLinkHelper;Ldagger/Lazy;Lcom/squareup/rootview/RootViewSetup;Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;Ldagger/Lazy;Lcom/squareup/container/AdditionalContainerLayerSetup;)V",
        "getAdditionalContainerLayerSetup",
        "()Lcom/squareup/container/AdditionalContainerLayerSetup;",
        "defaultHistory",
        "Lflow/History;",
        "getDefaultHistory",
        "()Lflow/History;",
        "flow",
        "Lflow/Flow;",
        "flow$annotations",
        "()V",
        "getFlow",
        "()Lflow/Flow;",
        "hasView",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "intentParser",
        "getIntentParser",
        "()Lcom/squareup/ui/main/IntentParser;",
        "intentParser$delegate",
        "Ldagger/Lazy;",
        "lockScreenMonitor",
        "getLockScreenMonitor",
        "()Lcom/squareup/permissions/ui/LockScreenMonitor;",
        "lockScreenMonitor$delegate",
        "nextHistory",
        "redirectPipelineDecorator",
        "getRedirectPipelineDecorator",
        "()Lcom/squareup/ui/main/MainActivityContainer$RedirectPipelineDecorator;",
        "redirectPipelineDecorator$delegate",
        "registrar",
        "getRegistrar",
        "()Lmortar/Scoped;",
        "registrar$delegate",
        "startHistoryFactory",
        "Lcom/squareup/ui/main/HistoryFactory;",
        "traversalCompleting",
        "x2ScreenRunner",
        "getX2ScreenRunner",
        "()Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
        "x2ScreenRunner$delegate",
        "asPosContainer",
        "Lcom/squareup/ui/main/PosContainer;",
        "buildDispatchPipeline",
        "",
        "pipeline",
        "",
        "Lcom/squareup/container/DispatchStep;",
        "buildRedirectPipeline",
        "",
        "Lcom/squareup/container/RedirectStep;",
        "currentPathIncludes",
        "pathType",
        "Ljava/lang/Class;",
        "dropView",
        "view",
        "extractBundleService",
        "Lmortar/bundler/BundleService;",
        "initialDetailScreenForAnnotation",
        "Lflow/path/Path;",
        "annotation",
        "Lcom/squareup/container/layer/Master;",
        "nextHistoryToShowWhileResetting",
        "currentHistory",
        "onActivityCreate",
        "intent",
        "Landroid/content/Intent;",
        "onActivityNewIntent",
        "onBackPressed",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onLoad",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onNewBranchLink",
        "path",
        "",
        "requireFlow",
        "resetForMissingPayment",
        "Lcom/squareup/container/RedirectStep$Result;",
        "traversal",
        "Lflow/Traversal;",
        "resetHistory",
        "temporaryStartHistory",
        "direction",
        "Lflow/Direction;",
        "setStartHistoryFactoryToDefault",
        "shouldSkipJail",
        "RedirectPipelineDecorator",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final activityBackHandler:Lcom/squareup/ui/MainActivityBackHandler;

.field private final additionalContainerLayerSetup:Lcom/squareup/container/AdditionalContainerLayerSetup;

.field private final apiTransactionController:Lcom/squareup/api/ApiTransactionController;

.field private final deepLinkHelper:Lcom/squareup/analytics/DeepLinkHelper;

.field private final emvSwipePassthroughEnabler:Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final glassConfirmController:Lcom/squareup/widgets/glass/GlassConfirmController;

.field private final hasView:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final home:Lcom/squareup/ui/main/Home;

.field private final intentParser$delegate:Ldagger/Lazy;

.field private final jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

.field private final lazyApplets:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/applet/Applets;",
            ">;"
        }
    .end annotation
.end field

.field private final lockScreenMonitor$delegate:Ldagger/Lazy;

.field private final navigationListener:Lcom/squareup/navigation/NavigationListener;

.field private final nextHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lflow/History;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final redirectPipelineDecorator$delegate:Ldagger/Lazy;

.field private final registrar$delegate:Ldagger/Lazy;

.field private final rootViewSetup:Lcom/squareup/rootview/RootViewSetup;

.field private final salesReportDetailLevelHolder:Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;

.field private final screenNavigationLogger:Lcom/squareup/navigation/ScreenNavigationLogger;

.field private final softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

.field private startHistoryFactory:Lcom/squareup/ui/main/HistoryFactory;

.field private final systemPermissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

.field private final traversalCompleting:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lflow/History;",
            ">;"
        }
    .end annotation
.end field

.field private final unsyncedOpenTicketsSpinner:Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;

.field private final x2ScreenRunner$delegate:Ldagger/Lazy;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-class v0, Lcom/squareup/ui/main/MainActivityContainer;

    const/4 v1, 0x5

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/PropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "registrar"

    const-string v5, "getRegistrar()Lmortar/Scoped;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/PropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "x2ScreenRunner"

    const-string v5, "getX2ScreenRunner()Lcom/squareup/x2/MaybeX2SellerScreenRunner;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/PropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "intentParser"

    const-string v5, "getIntentParser()Lcom/squareup/ui/main/IntentParser;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/PropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "lockScreenMonitor"

    const-string v5, "getLockScreenMonitor()Lcom/squareup/permissions/ui/LockScreenMonitor;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/PropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v3, "redirectPipelineDecorator"

    const-string v4, "getRedirectPipelineDecorator()Lcom/squareup/ui/main/MainActivityContainer$RedirectPipelineDecorator;"

    invoke-direct {v2, v0, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aput-object v0, v1, v2

    sput-object v1, Lcom/squareup/ui/main/MainActivityContainer;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/MainActivityBackHandler;Ldagger/Lazy;Ldagger/Lazy;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/settings/server/Features;Lcom/squareup/widgets/glass/GlassConfirmController;Lcom/squareup/jailkeeper/JailKeeper;Lcom/squareup/ui/main/TransactionMetrics;Ldagger/Lazy;Ldagger/Lazy;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/navigation/ScreenNavigationLogger;Lcom/squareup/navigation/NavigationListener;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/ui/main/Home;Lcom/squareup/analytics/DeepLinkHelper;Ldagger/Lazy;Lcom/squareup/rootview/RootViewSetup;Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;Ldagger/Lazy;Lcom/squareup/container/AdditionalContainerLayerSetup;)V
    .locals 16
    .param p2    # Ldagger/Lazy;
        .annotation runtime Lcom/squareup/ForMainActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/MainActivityBackHandler;",
            "Ldagger/Lazy<",
            "Lmortar/Scoped;",
            ">;",
            "Ldagger/Lazy<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/widgets/glass/GlassConfirmController;",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/IntentParser;",
            ">;",
            "Ldagger/Lazy<",
            "Lcom/squareup/applet/Applets;",
            ">;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/api/ApiTransactionController;",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
            "Lcom/squareup/navigation/ScreenNavigationLogger;",
            "Lcom/squareup/navigation/NavigationListener;",
            "Lcom/squareup/ui/SoftInputPresenter;",
            "Lcom/squareup/ui/main/Home;",
            "Lcom/squareup/analytics/DeepLinkHelper;",
            "Ldagger/Lazy<",
            "Lcom/squareup/permissions/ui/LockScreenMonitor;",
            ">;",
            "Lcom/squareup/rootview/RootViewSetup;",
            "Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;",
            "Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/MainActivityContainer$RedirectPipelineDecorator;",
            ">;",
            "Lcom/squareup/container/AdditionalContainerLayerSetup;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v0, p16

    const-string v0, "activityBackHandler"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lazyRegistrar"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lazyX2ScreenRunner"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transaction"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emvSwipePassthroughEnabler"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "glassConfirmController"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jailKeeper"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionMetrics"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lazyIntentParser"

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lazyApplets"

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "passcodeEmployeeManagement"

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "permissionGatekeeper"

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiTransactionController"

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "systemPermissionsPresenter"

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenNavigationLogger"

    move-object/from16 v10, p16

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "navigationListener"

    move-object/from16 v10, p17

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "softInputPresenter"

    move-object/from16 v2, p18

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "home"

    move-object/from16 v2, p19

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deepLinkHelper"

    move-object/from16 v2, p20

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lazyLockScreenMonitor"

    move-object/from16 v2, p21

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rootViewSetup"

    move-object/from16 v2, p22

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "unsyncedOpenTicketsSpinner"

    move-object/from16 v2, p23

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "salesReportDetailLevelHolder"

    move-object/from16 v2, p24

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lazyRedirectPipelineDecorator"

    move-object/from16 v2, p25

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "additionalContainerLayerSetup"

    move-object/from16 v2, p26

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    new-instance v0, Lcom/squareup/ui/main/MainActivityContainer$1;

    invoke-direct {v0, v3, v1}, Lcom/squareup/ui/main/MainActivityContainer$1;-><init>(Ldagger/Lazy;Lcom/squareup/ui/MainActivityBackHandler;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    move-object/from16 v3, p0

    move-object/from16 v2, p16

    .line 104
    invoke-direct {v3, v0}, Lcom/squareup/container/ContainerPresenter;-><init>(Lkotlin/jvm/functions/Function1;)V

    iput-object v1, v3, Lcom/squareup/ui/main/MainActivityContainer;->activityBackHandler:Lcom/squareup/ui/MainActivityBackHandler;

    iput-object v4, v3, Lcom/squareup/ui/main/MainActivityContainer;->transaction:Lcom/squareup/payment/Transaction;

    iput-object v5, v3, Lcom/squareup/ui/main/MainActivityContainer;->emvSwipePassthroughEnabler:Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

    iput-object v6, v3, Lcom/squareup/ui/main/MainActivityContainer;->features:Lcom/squareup/settings/server/Features;

    iput-object v7, v3, Lcom/squareup/ui/main/MainActivityContainer;->glassConfirmController:Lcom/squareup/widgets/glass/GlassConfirmController;

    iput-object v8, v3, Lcom/squareup/ui/main/MainActivityContainer;->jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

    iput-object v9, v3, Lcom/squareup/ui/main/MainActivityContainer;->transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

    iput-object v11, v3, Lcom/squareup/ui/main/MainActivityContainer;->lazyApplets:Ldagger/Lazy;

    iput-object v12, v3, Lcom/squareup/ui/main/MainActivityContainer;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iput-object v13, v3, Lcom/squareup/ui/main/MainActivityContainer;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    iput-object v14, v3, Lcom/squareup/ui/main/MainActivityContainer;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    iput-object v15, v3, Lcom/squareup/ui/main/MainActivityContainer;->systemPermissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    iput-object v2, v3, Lcom/squareup/ui/main/MainActivityContainer;->screenNavigationLogger:Lcom/squareup/navigation/ScreenNavigationLogger;

    iput-object v10, v3, Lcom/squareup/ui/main/MainActivityContainer;->navigationListener:Lcom/squareup/navigation/NavigationListener;

    move-object/from16 v0, p18

    move-object/from16 v1, p19

    iput-object v0, v3, Lcom/squareup/ui/main/MainActivityContainer;->softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

    iput-object v1, v3, Lcom/squareup/ui/main/MainActivityContainer;->home:Lcom/squareup/ui/main/Home;

    move-object/from16 v0, p20

    move-object/from16 v1, p21

    iput-object v0, v3, Lcom/squareup/ui/main/MainActivityContainer;->deepLinkHelper:Lcom/squareup/analytics/DeepLinkHelper;

    move-object/from16 v0, p22

    move-object/from16 v2, p23

    iput-object v0, v3, Lcom/squareup/ui/main/MainActivityContainer;->rootViewSetup:Lcom/squareup/rootview/RootViewSetup;

    iput-object v2, v3, Lcom/squareup/ui/main/MainActivityContainer;->unsyncedOpenTicketsSpinner:Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;

    move-object/from16 v0, p24

    move-object/from16 v2, p25

    iput-object v0, v3, Lcom/squareup/ui/main/MainActivityContainer;->salesReportDetailLevelHolder:Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;

    move-object/from16 v0, p26

    iput-object v0, v3, Lcom/squareup/ui/main/MainActivityContainer;->additionalContainerLayerSetup:Lcom/squareup/container/AdditionalContainerLayerSetup;

    move-object/from16 v0, p2

    .line 113
    iput-object v0, v3, Lcom/squareup/ui/main/MainActivityContainer;->registrar$delegate:Ldagger/Lazy;

    move-object v0, v3

    move-object/from16 v3, p3

    .line 114
    iput-object v3, v0, Lcom/squareup/ui/main/MainActivityContainer;->x2ScreenRunner$delegate:Ldagger/Lazy;

    move-object/from16 v3, p10

    .line 115
    iput-object v3, v0, Lcom/squareup/ui/main/MainActivityContainer;->intentParser$delegate:Ldagger/Lazy;

    .line 116
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer;->lockScreenMonitor$delegate:Ldagger/Lazy;

    .line 117
    iput-object v2, v0, Lcom/squareup/ui/main/MainActivityContainer;->redirectPipelineDecorator$delegate:Ldagger/Lazy;

    const/4 v1, 0x0

    .line 119
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    const-string v2, "BehaviorRelay.createDefault(false)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer;->hasView:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 120
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    const-string v2, "BehaviorRelay.create<History>()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer;->nextHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 121
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer;->traversalCompleting:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$currentPathIncludes(Lcom/squareup/ui/main/MainActivityContainer;Ljava/lang/Class;)Z
    .locals 0

    .line 77
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/MainActivityContainer;->currentPathIncludes(Ljava/lang/Class;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$getApiTransactionController$p(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/squareup/api/ApiTransactionController;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/squareup/ui/main/MainActivityContainer;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    return-object p0
.end method

.method public static final synthetic access$getEmvSwipePassthroughEnabler$p(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/squareup/ui/main/MainActivityContainer;->emvSwipePassthroughEnabler:Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

    return-object p0
.end method

.method public static final synthetic access$getHasView$p(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/squareup/ui/main/MainActivityContainer;->hasView:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getNavigationListener$p(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/squareup/navigation/NavigationListener;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/squareup/ui/main/MainActivityContainer;->navigationListener:Lcom/squareup/navigation/NavigationListener;

    return-object p0
.end method

.method public static final synthetic access$getNextHistory$p(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/squareup/ui/main/MainActivityContainer;->nextHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getSalesReportDetailLevelHolder$p(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/squareup/ui/main/MainActivityContainer;->salesReportDetailLevelHolder:Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;

    return-object p0
.end method

.method public static final synthetic access$getSoftInputPresenter$p(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/squareup/ui/SoftInputPresenter;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/squareup/ui/main/MainActivityContainer;->softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

    return-object p0
.end method

.method public static final synthetic access$getStartHistoryFactory$p(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/squareup/ui/main/HistoryFactory;
    .locals 1

    .line 77
    iget-object p0, p0, Lcom/squareup/ui/main/MainActivityContainer;->startHistoryFactory:Lcom/squareup/ui/main/HistoryFactory;

    if-nez p0, :cond_0

    const-string v0, "startHistoryFactory"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getTransactionMetrics$p(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/squareup/ui/main/TransactionMetrics;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/squareup/ui/main/MainActivityContainer;->transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

    return-object p0
.end method

.method public static final synthetic access$getTraversalCompleting$p(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/squareup/ui/main/MainActivityContainer;->traversalCompleting:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getView(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/squareup/ui/main/MainActivityScope$View;
    .locals 0

    .line 77
    invoke-virtual {p0}, Lcom/squareup/ui/main/MainActivityContainer;->getView()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/MainActivityScope$View;

    return-object p0
.end method

.method public static final synthetic access$onActivityCreate(Lcom/squareup/ui/main/MainActivityContainer;Landroid/content/Intent;)V
    .locals 0

    .line 77
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/MainActivityContainer;->onActivityCreate(Landroid/content/Intent;)V

    return-void
.end method

.method public static final synthetic access$onActivityNewIntent(Lcom/squareup/ui/main/MainActivityContainer;Landroid/content/Intent;)V
    .locals 0

    .line 77
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/MainActivityContainer;->onActivityNewIntent(Landroid/content/Intent;)V

    return-void
.end method

.method public static final synthetic access$onNewBranchLink(Lcom/squareup/ui/main/MainActivityContainer;Ljava/lang/String;)V
    .locals 0

    .line 77
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/MainActivityContainer;->onNewBranchLink(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$resetForMissingPayment(Lcom/squareup/ui/main/MainActivityContainer;Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 0

    .line 77
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/MainActivityContainer;->resetForMissingPayment(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$resetHistory(Lcom/squareup/ui/main/MainActivityContainer;)V
    .locals 0

    .line 77
    invoke-direct {p0}, Lcom/squareup/ui/main/MainActivityContainer;->resetHistory()V

    return-void
.end method

.method public static final synthetic access$resetHistory(Lcom/squareup/ui/main/MainActivityContainer;Lcom/squareup/ui/main/HistoryFactory;Lflow/Direction;)V
    .locals 0

    .line 77
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/main/MainActivityContainer;->resetHistory(Lcom/squareup/ui/main/HistoryFactory;Lflow/Direction;)V

    return-void
.end method

.method public static final synthetic access$setStartHistoryFactory$p(Lcom/squareup/ui/main/MainActivityContainer;Lcom/squareup/ui/main/HistoryFactory;)V
    .locals 0

    .line 77
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivityContainer;->startHistoryFactory:Lcom/squareup/ui/main/HistoryFactory;

    return-void
.end method

.method private final currentPathIncludes(Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)Z"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = ""
    .end annotation

    .line 463
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->nextHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v0, Lflow/History;

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    .line 464
    invoke-virtual {v0, p1}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p1

    return p1
.end method

.method public static synthetic flow$annotations()V
    .locals 0

    return-void
.end method

.method private final getIntentParser()Lcom/squareup/ui/main/IntentParser;
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->intentParser$delegate:Ldagger/Lazy;

    sget-object v1, Lcom/squareup/ui/main/MainActivityContainer;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/dagger/LazysKt;->getValue(Ldagger/Lazy;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/IntentParser;

    return-object v0
.end method

.method private final getLockScreenMonitor()Lcom/squareup/permissions/ui/LockScreenMonitor;
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->lockScreenMonitor$delegate:Ldagger/Lazy;

    sget-object v1, Lcom/squareup/ui/main/MainActivityContainer;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/dagger/LazysKt;->getValue(Ldagger/Lazy;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/permissions/ui/LockScreenMonitor;

    return-object v0
.end method

.method private final getRedirectPipelineDecorator()Lcom/squareup/ui/main/MainActivityContainer$RedirectPipelineDecorator;
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->redirectPipelineDecorator$delegate:Ldagger/Lazy;

    sget-object v1, Lcom/squareup/ui/main/MainActivityContainer;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/dagger/LazysKt;->getValue(Ldagger/Lazy;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/MainActivityContainer$RedirectPipelineDecorator;

    return-object v0
.end method

.method private final getRegistrar()Lmortar/Scoped;
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->registrar$delegate:Ldagger/Lazy;

    sget-object v1, Lcom/squareup/ui/main/MainActivityContainer;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/dagger/LazysKt;->getValue(Ldagger/Lazy;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmortar/Scoped;

    return-object v0
.end method

.method private final getX2ScreenRunner()Lcom/squareup/x2/MaybeX2SellerScreenRunner;
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->x2ScreenRunner$delegate:Ldagger/Lazy;

    sget-object v1, Lcom/squareup/ui/main/MainActivityContainer;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/dagger/LazysKt;->getValue(Ldagger/Lazy;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    return-object v0
.end method

.method private final nextHistoryToShowWhileResetting(Lflow/History;)Lflow/History;
    .locals 5

    .line 252
    invoke-direct {p0}, Lcom/squareup/ui/main/MainActivityContainer;->shouldSkipJail()Z

    move-result v0

    if-nez v0, :cond_0

    .line 253
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivityContainer;->jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

    invoke-interface {p1}, Lcom/squareup/jailkeeper/JailKeeper;->getJailScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    invoke-static {p1}, Lflow/History;->single(Ljava/lang/Object;)Lflow/History;

    move-result-object p1

    const-string v0, "History.single(jailKeeper.jailScreen)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->IGNORE_SYSTEM_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 258
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->systemPermissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    sget-object v1, Lcom/squareup/ui/main/MainActivityPrimaryPermissions;->PRIMARY_SYSTEM_PERMISSIONS:[Lcom/squareup/systempermissions/SystemPermission;

    array-length v2, v1

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/squareup/systempermissions/SystemPermission;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->hasNeverAsked([Lcom/squareup/systempermissions/SystemPermission;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 259
    sget-object p1, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsScreen;->INSTANCE:Lcom/squareup/ui/systempermissions/EnableDeviceSettingsScreen;

    invoke-static {p1}, Lflow/History;->single(Ljava/lang/Object;)Lflow/History;

    move-result-object p1

    const-string v0, "History.single(EnableDev\u2026eSettingsScreen.INSTANCE)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 264
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->systemPermissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    sget-object v1, Lcom/squareup/ui/main/MainActivityPrimaryPermissions;->PRIMARY_SYSTEM_PERMISSIONS:[Lcom/squareup/systempermissions/SystemPermission;

    array-length v2, v1

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/squareup/systempermissions/SystemPermission;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->getFirstDenied([Lcom/squareup/systempermissions/SystemPermission;)Lcom/squareup/systempermissions/SystemPermission;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 265
    sget-object p1, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedScreen;->INSTANCE:Lcom/squareup/ui/systempermissions/SystemPermissionRevokedScreen;

    invoke-static {p1}, Lflow/History;->single(Ljava/lang/Object;)Lflow/History;

    move-result-object p1

    const-string v0, "History.single(SystemPer\u2026onRevokedScreen.INSTANCE)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 269
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->startHistoryFactory:Lcom/squareup/ui/main/HistoryFactory;

    const-string v1, "startHistoryFactory"

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    iget-object v2, p0, Lcom/squareup/ui/main/MainActivityContainer;->home:Lcom/squareup/ui/main/Home;

    invoke-interface {v0, v2, p1}, Lcom/squareup/ui/main/HistoryFactory;->createHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;

    move-result-object v0

    .line 273
    iget-object v2, p0, Lcom/squareup/ui/main/MainActivityContainer;->startHistoryFactory:Lcom/squareup/ui/main/HistoryFactory;

    if-nez v2, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-interface {v2}, Lcom/squareup/ui/main/HistoryFactory;->getPermissions()Ljava/util/Set;

    move-result-object v2

    .line 277
    invoke-direct {p0}, Lcom/squareup/ui/main/MainActivityContainer;->setStartHistoryFactoryToDefault()V

    .line 281
    invoke-direct {p0}, Lcom/squareup/ui/main/MainActivityContainer;->getLockScreenMonitor()Lcom/squareup/permissions/ui/LockScreenMonitor;

    move-result-object v3

    invoke-interface {v3}, Lcom/squareup/permissions/ui/LockScreenMonitor;->showLockScreen()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 282
    invoke-virtual {v0}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p1

    .line 283
    invoke-direct {p0}, Lcom/squareup/ui/main/MainActivityContainer;->getLockScreenMonitor()Lcom/squareup/permissions/ui/LockScreenMonitor;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/permissions/ui/LockScreenMonitor;->getEmployeeLockScreenKey()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-virtual {p1, v0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    move-result-object p1

    .line 284
    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    const-string v0, "requestedHistory.buildUp\u2026Key())\n          .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :cond_5
    if-eqz v2, :cond_c

    .line 291
    iget-object v3, p0, Lcom/squareup/ui/main/MainActivityContainer;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    invoke-virtual {v3, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->hasAnyPermission(Ljava/util/Set;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 293
    iget-object v3, p0, Lcom/squareup/ui/main/MainActivityContainer;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 294
    new-instance v4, Lcom/squareup/ui/main/MainActivityContainer$nextHistoryToShowWhileResetting$1;

    invoke-direct {v4, p0, v0}, Lcom/squareup/ui/main/MainActivityContainer$nextHistoryToShowWhileResetting$1;-><init>(Lcom/squareup/ui/main/MainActivityContainer;Lflow/History;)V

    check-cast v4, Lcom/squareup/permissions/PermissionGatekeeper$When;

    .line 293
    invoke-virtual {v3, v2, v4}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAnyAccessGranted(Ljava/util/Set;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    .line 313
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->nextHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object p1, p0, Lcom/squareup/ui/main/MainActivityContainer;->nextHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_6

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_6
    check-cast p1, Lflow/History;

    return-object p1

    .line 318
    :cond_7
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->startHistoryFactory:Lcom/squareup/ui/main/HistoryFactory;

    if-nez v0, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-interface {v0}, Lcom/squareup/ui/main/HistoryFactory;->getPermissions()Ljava/util/Set;

    move-result-object v0

    if-nez v0, :cond_9

    const/4 v0, 0x1

    goto :goto_0

    :cond_9
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_b

    .line 321
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->startHistoryFactory:Lcom/squareup/ui/main/HistoryFactory;

    if-nez v0, :cond_a

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityContainer;->home:Lcom/squareup/ui/main/Home;

    invoke-interface {v0, v1, p1}, Lcom/squareup/ui/main/HistoryFactory;->createHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;

    move-result-object p1

    return-object p1

    .line 318
    :cond_b
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Default startHistoryFactory must not require permissions"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_c
    return-object v0
.end method

.method private final onActivityCreate(Landroid/content/Intent;)V
    .locals 1

    .line 473
    invoke-direct {p0}, Lcom/squareup/ui/main/MainActivityContainer;->getIntentParser()Lcom/squareup/ui/main/IntentParser;

    move-result-object v0

    .line 474
    invoke-interface {v0, p1}, Lcom/squareup/ui/main/IntentParser;->parseIntent(Landroid/content/Intent;)Lcom/squareup/ui/main/HistoryFactory;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "it"

    .line 475
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/main/MainActivityContainer;->startHistoryFactory:Lcom/squareup/ui/main/HistoryFactory;

    goto :goto_0

    .line 476
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/main/MainActivityContainer;->setStartHistoryFactoryToDefault()V

    :goto_0
    return-void
.end method

.method private final onActivityNewIntent(Landroid/content/Intent;)V
    .locals 4

    .line 414
    invoke-direct {p0}, Lcom/squareup/ui/main/MainActivityContainer;->getX2ScreenRunner()Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->isBranReady()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415
    invoke-virtual {p0}, Lcom/squareup/ui/main/MainActivityContainer;->requireFlow()Lflow/Flow;

    move-result-object p1

    .line 416
    new-instance v0, Lcom/squareup/register/widgets/WarningDialogScreen;

    .line 417
    new-instance v1, Lcom/squareup/widgets/warning/WarningIds;

    .line 418
    sget v2, Lcom/squareup/common/bootstrap/R$string;->complete_payment_in_progress_title:I

    .line 419
    sget v3, Lcom/squareup/common/bootstrap/R$string;->complete_payment_in_progress_message:I

    .line 417
    invoke-direct {v1, v2, v3}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    check-cast v1, Lcom/squareup/widgets/warning/Warning;

    .line 416
    invoke-direct {v0, v1}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    .line 415
    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 426
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/main/MainActivityContainer;->getIntentParser()Lcom/squareup/ui/main/IntentParser;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/ui/main/IntentParser;->isGoBackIntent(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 427
    invoke-virtual {p0}, Lcom/squareup/ui/main/MainActivityContainer;->requireFlow()Lflow/Flow;

    move-result-object p1

    invoke-virtual {p1}, Lflow/Flow;->goBack()Z

    goto :goto_1

    .line 429
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/MainActivityContainer;->onActivityCreate(Landroid/content/Intent;)V

    .line 430
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivityContainer;->nextHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/History;

    .line 431
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/MainActivityContainer;->nextHistoryToShowWhileResetting(Lflow/History;)Lflow/History;

    move-result-object p1

    .line 433
    invoke-virtual {p1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    .line 435
    invoke-direct {p0}, Lcom/squareup/ui/main/MainActivityContainer;->getX2ScreenRunner()Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->isBranReady()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityContainer;->home:Lcom/squareup/ui/main/Home;

    const-string v2, "topScreen"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v0, p1}, Lcom/squareup/ui/main/HomeKt;->contains(Lcom/squareup/ui/main/Home;Ljava/lang/Object;Lflow/History;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 436
    const-class p1, Lcom/squareup/ui/permissions/PermissionDeniedScreen;

    invoke-direct {p0, p1}, Lcom/squareup/ui/main/MainActivityContainer;->currentPathIncludes(Ljava/lang/Class;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 439
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivityContainer;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/permissions/PermissionGatekeeper;->dismiss(Z)V

    .line 441
    :cond_2
    invoke-direct {p0}, Lcom/squareup/ui/main/MainActivityContainer;->resetHistory()V

    goto :goto_0

    .line 443
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/ui/main/MainActivityContainer;->requireFlow()Lflow/Flow;

    move-result-object v0

    sget-object v1, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-virtual {v0, p1, v1}, Lflow/Flow;->setHistory(Lflow/History;Lflow/Direction;)V

    .line 449
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivityContainer;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isUnlocked()Z

    move-result p1

    if-nez p1, :cond_4

    .line 450
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivityContainer;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->clearCurrentEmployee()V

    :cond_4
    :goto_1
    return-void
.end method

.method private final onNewBranchLink(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "Got branch link: %s"

    .line 405
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 407
    new-instance v0, Landroid/content/Intent;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/main/MainActivityContainer;->onActivityNewIntent(Landroid/content/Intent;)V

    return-void
.end method

.method private final resetForMissingPayment(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 4

    .line 514
    iget-object v0, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->framesFromBottom()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/container/ContainerTreeKey;

    if-nez v2, :cond_1

    .line 516
    const-class v2, Lcom/squareup/ui/buyer/InBuyerFlow;

    invoke-virtual {v3, v2}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 517
    const-class v2, Lcom/squareup/ui/tender/RequiresPayment;

    invoke-static {v3, v2}, Lcom/squareup/util/Objects;->isAnnotated(Ljava/lang/Object;Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    if-nez v2, :cond_3

    return-object v0

    .line 524
    :cond_3
    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityContainer;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->hasPayment()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityContainer;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->hasReceiptForLastPayment()Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_1

    .line 526
    :cond_4
    new-instance v0, Lcom/squareup/container/RedirectStep$Result;

    .line 528
    iget-object p1, p1, Lflow/Traversal;->origin:Lflow/History;

    invoke-direct {p0, p1}, Lcom/squareup/ui/main/MainActivityContainer;->nextHistoryToShowWhileResetting(Lflow/History;)Lflow/History;

    move-result-object p1

    sget-object v1, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {p1, v1}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    const-string v1, "Missing payment, redirect home."

    .line 526
    invoke-direct {v0, v1, p1}, Lcom/squareup/container/RedirectStep$Result;-><init>(Ljava/lang/String;Lcom/squareup/container/Command;)V

    :cond_5
    :goto_1
    return-object v0
.end method

.method private final resetHistory()V
    .locals 3

    .line 242
    invoke-virtual {p0}, Lcom/squareup/ui/main/MainActivityContainer;->requireFlow()Lflow/Flow;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityContainer;->nextHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflow/History;

    invoke-direct {p0, v1}, Lcom/squareup/ui/main/MainActivityContainer;->nextHistoryToShowWhileResetting(Lflow/History;)Lflow/History;

    move-result-object v1

    sget-object v2, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-virtual {v0, v1, v2}, Lflow/Flow;->setHistory(Lflow/History;Lflow/Direction;)V

    return-void
.end method

.method private final resetHistory(Lcom/squareup/ui/main/HistoryFactory;Lflow/Direction;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 400
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivityContainer;->startHistoryFactory:Lcom/squareup/ui/main/HistoryFactory;

    .line 401
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/main/MainActivityContainer;->requireFlow()Lflow/Flow;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->nextHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/History;

    invoke-direct {p0, v0}, Lcom/squareup/ui/main/MainActivityContainer;->nextHistoryToShowWhileResetting(Lflow/History;)Lflow/History;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lflow/Flow;->setHistory(Lflow/History;Lflow/Direction;)V

    return-void
.end method

.method private final setStartHistoryFactoryToDefault()V
    .locals 1

    .line 480
    new-instance v0, Lcom/squareup/ui/main/MainActivityContainer$setStartHistoryFactoryToDefault$1;

    invoke-direct {v0}, Lcom/squareup/ui/main/MainActivityContainer$setStartHistoryFactoryToDefault$1;-><init>()V

    check-cast v0, Lcom/squareup/ui/main/HistoryFactory;

    iput-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->startHistoryFactory:Lcom/squareup/ui/main/HistoryFactory;

    return-void
.end method

.method private final shouldSkipJail()Z
    .locals 3

    .line 491
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

    invoke-interface {v0}, Lcom/squareup/jailkeeper/JailKeeper;->getState()Lcom/squareup/jailkeeper/JailKeeper$State;

    move-result-object v0

    .line 492
    sget-object v1, Lcom/squareup/jailkeeper/JailKeeper$State;->FAILED:Lcom/squareup/jailkeeper/JailKeeper$State;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    return v2

    .line 498
    :cond_0
    sget-object v1, Lcom/squareup/jailkeeper/JailKeeper$State;->READY:Lcom/squareup/jailkeeper/JailKeeper$State;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

    invoke-interface {v0}, Lcom/squareup/jailkeeper/JailKeeper;->foregroundSync()Lcom/squareup/jailkeeper/JailKeeper$State;

    move-result-object v0

    sget-object v1, Lcom/squareup/jailkeeper/JailKeeper$State;->READY:Lcom/squareup/jailkeeper/JailKeeper$State;

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v2, 0x1

    :cond_2
    if-eqz v2, :cond_3

    .line 501
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

    invoke-interface {v0}, Lcom/squareup/jailkeeper/JailKeeper;->backgroundSync()V

    :cond_3
    return v2
.end method


# virtual methods
.method public final asPosContainer()Lcom/squareup/ui/main/PosContainer;
    .locals 1

    .line 198
    new-instance v0, Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1;-><init>(Lcom/squareup/ui/main/MainActivityContainer;)V

    check-cast v0, Lcom/squareup/ui/main/PosContainer;

    return-object v0
.end method

.method protected buildDispatchPipeline(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/container/DispatchStep;",
            ">;)V"
        }
    .end annotation

    const-string v0, "pipeline"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 349
    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    new-instance v1, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$1;-><init>(Lcom/squareup/ui/main/MainActivityContainer;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 354
    new-instance v1, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2;-><init>(Lcom/squareup/ui/main/MainActivityContainer;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 373
    new-instance v1, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$3;-><init>(Lcom/squareup/ui/main/MainActivityContainer;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 379
    new-instance v1, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$4;-><init>(Lcom/squareup/ui/main/MainActivityContainer;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 384
    invoke-super {p0, p1}, Lcom/squareup/container/ContainerPresenter;->buildDispatchPipeline(Ljava/util/List;)V

    .line 387
    new-instance p1, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$5;

    invoke-direct {p1, p0}, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$5;-><init>(Lcom/squareup/ui/main/MainActivityContainer;)V

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected buildRedirectPipeline()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/container/RedirectStep;",
            ">;"
        }
    .end annotation

    .line 328
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 331
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Lcom/squareup/ui/main/MainActivityContainer$buildRedirectPipeline$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/main/MainActivityContainer$buildRedirectPipeline$1;-><init>(Lcom/squareup/ui/main/MainActivityContainer;)V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 335
    new-instance v2, Lcom/squareup/ui/main/MainActivityContainer$buildRedirectPipeline$2;

    invoke-direct {v2, p0}, Lcom/squareup/ui/main/MainActivityContainer$buildRedirectPipeline$2;-><init>(Lcom/squareup/ui/main/MainActivityContainer;)V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 340
    invoke-super {p0}, Lcom/squareup/container/ContainerPresenter;->buildRedirectPipeline()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 341
    invoke-direct {p0}, Lcom/squareup/ui/main/MainActivityContainer;->getRedirectPipelineDecorator()Lcom/squareup/ui/main/MainActivityContainer$RedirectPipelineDecorator;

    move-result-object v2

    .line 342
    invoke-interface {v2, v0}, Lcom/squareup/ui/main/MainActivityContainer$RedirectPipelineDecorator;->updatePipeline(Ljava/util/List;)V

    .line 343
    new-instance v2, Lcom/squareup/ui/main/MainActivityContainer$buildRedirectPipeline$3;

    invoke-direct {v2, p0}, Lcom/squareup/ui/main/MainActivityContainer$buildRedirectPipeline$3;-><init>(Lcom/squareup/ui/main/MainActivityContainer;)V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 345
    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic dropView(Lcom/squareup/container/ContainerView;)V
    .locals 0

    .line 77
    check-cast p1, Lcom/squareup/ui/main/MainActivityScope$View;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/MainActivityContainer;->dropView(Lcom/squareup/ui/main/MainActivityScope$View;)V

    return-void
.end method

.method public dropView(Lcom/squareup/ui/main/MainActivityScope$View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    invoke-virtual {p0}, Lcom/squareup/ui/main/MainActivityContainer;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/MainActivityScope$View;

    if-ne p1, v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->hasView:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 178
    :cond_0
    check-cast p1, Lcom/squareup/container/ContainerView;

    invoke-super {p0, p1}, Lcom/squareup/container/ContainerPresenter;->dropView(Lcom/squareup/container/ContainerView;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 77
    check-cast p1, Lcom/squareup/ui/main/MainActivityScope$View;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/MainActivityContainer;->dropView(Lcom/squareup/ui/main/MainActivityScope$View;)V

    return-void
.end method

.method public bridge synthetic extractBundleService(Lcom/squareup/container/ContainerView;)Lmortar/bundler/BundleService;
    .locals 0

    .line 77
    check-cast p1, Lcom/squareup/ui/main/MainActivityScope$View;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/MainActivityContainer;->extractBundleService(Lcom/squareup/ui/main/MainActivityScope$View;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected extractBundleService(Lcom/squareup/ui/main/MainActivityScope$View;)Lmortar/bundler/BundleService;
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 185
    invoke-virtual {p0}, Lcom/squareup/ui/main/MainActivityContainer;->getView()Ljava/lang/Object;

    move-result-object p1

    const-string v0, "getView()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/ui/main/MainActivityScope$View;

    invoke-interface {p1}, Lcom/squareup/ui/main/MainActivityScope$View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    const-string v0, "BundleService.getBundleService(getView().context)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 77
    check-cast p1, Lcom/squareup/ui/main/MainActivityScope$View;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/MainActivityContainer;->extractBundleService(Lcom/squareup/ui/main/MainActivityScope$View;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected getAdditionalContainerLayerSetup()Lcom/squareup/container/AdditionalContainerLayerSetup;
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->additionalContainerLayerSetup:Lcom/squareup/container/AdditionalContainerLayerSetup;

    return-object v0
.end method

.method public getDefaultHistory()Lflow/History;
    .locals 1

    const/4 v0, 0x0

    .line 130
    invoke-direct {p0, v0}, Lcom/squareup/ui/main/MainActivityContainer;->nextHistoryToShowWhileResetting(Lflow/History;)Lflow/History;

    move-result-object v0

    return-object v0
.end method

.method public getFlow()Lflow/Flow;
    .locals 1

    .line 127
    invoke-super {p0}, Lcom/squareup/container/ContainerPresenter;->getFlow()Lflow/Flow;

    move-result-object v0

    return-object v0
.end method

.method protected initialDetailScreenForAnnotation(Lcom/squareup/container/layer/Master;)Lflow/path/Path;
    .locals 2

    const-string v0, "annotation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 189
    invoke-interface {p1}, Lcom/squareup/container/layer/Master;->applet()Ljava/lang/Class;

    move-result-object p1

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->lazyApplets:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/Applets;

    invoke-interface {v0}, Lcom/squareup/applet/Applets;->getApplets()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 191
    invoke-static {p1}, Lkotlin/jvm/JvmClassMappingKt;->getJavaClass(Lkotlin/reflect/KClass;)Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->filterIsInstance(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 192
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/squareup/applet/Applet;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/squareup/applet/Applet;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/squareup/applet/Applet;->getInitialDetailScreen()Lflow/path/Path;

    move-result-object v0

    if-eqz v0, :cond_1

    return-object v0

    .line 194
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown applet type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public onBackPressed()V
    .locals 1

    .line 459
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->glassConfirmController:Lcom/squareup/widgets/glass/GlassConfirmController;

    invoke-virtual {v0}, Lcom/squareup/widgets/glass/GlassConfirmController;->maybeCancelAndRemoveGlass()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/squareup/container/ContainerPresenter;->onBackPressed()V

    :cond_0
    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    invoke-super {p0, p1}, Lcom/squareup/container/ContainerPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 146
    move-object v0, p0

    check-cast v0, Lcom/squareup/ui/main/MainActivityContainer;

    iget-object v0, v0, Lcom/squareup/ui/main/MainActivityContainer;->startHistoryFactory:Lcom/squareup/ui/main/HistoryFactory;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/main/MainActivityContainer;->setStartHistoryFactoryToDefault()V

    .line 148
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/main/MainActivityContainer;->getRegistrar()Lmortar/Scoped;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->deepLinkHelper:Lcom/squareup/analytics/DeepLinkHelper;

    invoke-interface {v0}, Lcom/squareup/analytics/DeepLinkHelper;->deepLinkPath()Lrx/Observable;

    move-result-object v0

    const-string v1, "deepLinkHelper.deepLinkPath()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 152
    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityContainer;->hasView:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v1, Lio/reactivex/Observable;

    invoke-static {v1}, Lcom/squareup/util/rx2/Rx2TransformersKt;->gateBy(Lio/reactivex/Observable;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 153
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "deepLinkHelper.deepLinkP\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    new-instance v1, Lcom/squareup/ui/main/MainActivityContainer$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/MainActivityContainer$onEnterScope$2;-><init>(Lcom/squareup/ui/main/MainActivityContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer;->screenNavigationLogger:Lcom/squareup/navigation/ScreenNavigationLogger;

    .line 158
    invoke-virtual {p0}, Lcom/squareup/ui/main/MainActivityContainer;->asPosContainer()Lcom/squareup/ui/main/PosContainer;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/ui/main/PosContainer;->topOfTraversalCompleting()Lio/reactivex/Observable;

    move-result-object v1

    .line 159
    const-class v2, Lcom/squareup/ui/main/MainActivityScope;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    .line 156
    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/navigation/ScreenNavigationLogger;->init(Lmortar/MortarScope;Lio/reactivex/Observable;Ljava/lang/String;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 164
    invoke-virtual {p0}, Lcom/squareup/ui/main/MainActivityContainer;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/MainActivityScope$View;

    if-eqz v0, :cond_0

    check-cast v0, Lcom/squareup/rootview/RootView;

    .line 166
    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityContainer;->rootViewSetup:Lcom/squareup/rootview/RootViewSetup;

    check-cast v0, Landroid/view/View;

    invoke-interface {v1, v0}, Lcom/squareup/rootview/RootViewSetup;->setUpRootView(Landroid/view/View;)V

    .line 168
    invoke-super {p0, p1}, Lcom/squareup/container/ContainerPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 170
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivityContainer;->hasView:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 171
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivityContainer;->unsyncedOpenTicketsSpinner:Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;

    invoke-interface {p1, v0}, Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;->setUpSpinner(Landroid/view/View;)V

    return-void

    .line 164
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.rootview.RootView"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final requireFlow()Lflow/Flow;
    .locals 2

    .line 467
    invoke-virtual {p0}, Lcom/squareup/ui/main/MainActivityContainer;->getFlow()Lflow/Flow;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "flow"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
