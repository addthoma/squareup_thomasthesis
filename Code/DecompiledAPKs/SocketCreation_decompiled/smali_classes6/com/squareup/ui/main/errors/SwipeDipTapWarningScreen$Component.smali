.class public interface abstract Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Component;
.super Ljava/lang/Object;
.source "SwipeDipTapWarningScreen.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract mainScheduler()Lrx/Scheduler;
    .annotation runtime Lcom/squareup/thread/Main;
    .end annotation
.end method

.method public abstract workflow()Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;
.end method
