.class public final Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DarkTamperErrorScreen;
.super Lcom/squareup/ui/main/errors/NoPaymentWarningScreen;
.source "ConcreteWarningScreens.java"


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent$FromFactory;
    value = Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$ComponentFactory;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/ConcreteWarningScreens;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DarkTamperErrorScreen"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DarkTamperErrorScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final cancellable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 446
    sget-object v0, Lcom/squareup/ui/main/errors/-$$Lambda$ConcreteWarningScreens$DarkTamperErrorScreen$gXMieGO3cOTHxIBIcMl9SyNpOC8;->INSTANCE:Lcom/squareup/ui/main/errors/-$$Lambda$ConcreteWarningScreens$DarkTamperErrorScreen$gXMieGO3cOTHxIBIcMl9SyNpOC8;

    .line 447
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DarkTamperErrorScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0

    .line 417
    invoke-direct {p0}, Lcom/squareup/ui/main/errors/NoPaymentWarningScreen;-><init>()V

    .line 418
    iput-boolean p1, p0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DarkTamperErrorScreen;->cancellable:Z

    return-void
.end method

.method public static cancellableDarkTamperErrorScreen()Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DarkTamperErrorScreen;
    .locals 2

    .line 422
    new-instance v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DarkTamperErrorScreen;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DarkTamperErrorScreen;-><init>(Z)V

    return-object v0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DarkTamperErrorScreen;
    .locals 1

    .line 448
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 449
    :goto_0
    new-instance p0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DarkTamperErrorScreen;

    invoke-direct {p0, v0}, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DarkTamperErrorScreen;-><init>(Z)V

    return-object p0
.end method

.method public static noncancellableDarkTamperErrorScreen()Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DarkTamperErrorScreen;
    .locals 2

    .line 426
    new-instance v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DarkTamperErrorScreen;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DarkTamperErrorScreen;-><init>(Z)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 453
    iget-boolean p2, p0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DarkTamperErrorScreen;->cancellable:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method protected getInitialViewData(Lcom/squareup/util/Res;)Lcom/squareup/ui/main/errors/WarningScreenData;
    .locals 6

    .line 434
    new-instance p1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    sget-object v1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->DISMISS:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;-><init>(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;ZZLjava/lang/String;I)V

    .line 437
    new-instance v0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    invoke-direct {v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;-><init>()V

    iget-boolean v1, p0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DarkTamperErrorScreen;->cancellable:Z

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 438
    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->defaultButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    iget-boolean v0, p0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DarkTamperErrorScreen;->cancellable:Z

    .line 439
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->cancellable(Z)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_EXCLAMATION:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 440
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->terminal_damaged_message:I

    .line 441
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->messageId(I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->terminal_damaged_title:I

    .line 442
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->titleId(I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    .line 443
    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->build()Lcom/squareup/ui/main/errors/WarningScreenData;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 430
    sget v0, Lcom/squareup/cardreader/ui/R$layout;->dark_warning_view:I

    return v0
.end method
