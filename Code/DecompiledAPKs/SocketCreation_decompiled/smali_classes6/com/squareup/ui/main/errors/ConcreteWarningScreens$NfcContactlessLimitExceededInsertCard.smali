.class public final Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcContactlessLimitExceededInsertCard;
.super Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen;
.source "ConcreteWarningScreens.java"


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent$FromFactory;
    value = Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$ComponentFactory;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/ConcreteWarningScreens;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NfcContactlessLimitExceededInsertCard"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcContactlessLimitExceededInsertCard;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcContactlessLimitExceededInsertCard;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 248
    new-instance v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcContactlessLimitExceededInsertCard;

    invoke-direct {v0}, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcContactlessLimitExceededInsertCard;-><init>()V

    sput-object v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcContactlessLimitExceededInsertCard;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcContactlessLimitExceededInsertCard;

    .line 266
    sget-object v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcContactlessLimitExceededInsertCard;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcContactlessLimitExceededInsertCard;

    .line 267
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcContactlessLimitExceededInsertCard;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 252
    invoke-direct {p0}, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen;-><init>()V

    return-void
.end method


# virtual methods
.method protected getInitialScreenData(Lcom/squareup/util/Res;)Lcom/squareup/ui/main/errors/WarningScreenData;
    .locals 2

    .line 256
    new-instance p1, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    invoke-direct {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;-><init>()V

    sget-object v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->KILL_TENDER_INITIATED_BY_READER_AND_TURN_OFF_NFC_FIELD:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    sget v1, Lcom/squareup/common/strings/R$string;->ok:I

    .line 257
    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->defaultButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CONTACTLESS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 259
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/R$string;->contactless_limit_exceeded_insert_card_message:I

    .line 260
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->messageId(I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/R$string;->contactless_limit_exceeded_insert_card_title:I

    .line 262
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->titleId(I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    .line 263
    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->build()Lcom/squareup/ui/main/errors/WarningScreenData;

    move-result-object p1

    return-object p1
.end method
