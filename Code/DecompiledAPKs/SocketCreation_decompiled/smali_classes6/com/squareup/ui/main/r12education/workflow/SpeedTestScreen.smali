.class public final Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;
.super Ljava/lang/Object;
.source "SpeedTestScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;,
        Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 \u00112\u00020\u0001:\u0002\u0011\u0012B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0008\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "state",
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;",
        "(Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;)V",
        "getState",
        "()Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "Companion",
        "ScreenState",
        "reader-tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;


# instance fields
.field private final state:Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;->Companion:Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$Companion;

    .line 24
    const-class v0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;)V
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;->state:Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;

    return-void
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 6
    sget-object v0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;ILjava/lang/Object;)Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;->state:Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;->copy(Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;)Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;->state:Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;

    return-object v0
.end method

.method public final copy(Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;)Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;

    invoke-direct {v0, p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;-><init>(Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;

    iget-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;->state:Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;

    iget-object p1, p1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;->state:Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getState()Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;->state:Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;->state:Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SpeedTestScreen(state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;->state:Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
