.class public final Lcom/squareup/ui/main/OrderEntryHomeScreenSelector_Factory;
.super Ljava/lang/Object;
.source "OrderEntryHomeScreenSelector_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/OrderEntryHomeScreenSelector;",
        ">;"
    }
.end annotation


# instance fields
.field private final openTicketsSelectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final timecardsHomeScreenRedirectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/api/TimecardsHomeScreenRedirector;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/api/TimecardsHomeScreenRedirector;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector_Factory;->openTicketsSelectorProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector_Factory;->timecardsHomeScreenRedirectorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/OrderEntryHomeScreenSelector_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/api/TimecardsHomeScreenRedirector;",
            ">;)",
            "Lcom/squareup/ui/main/OrderEntryHomeScreenSelector_Factory;"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;Ldagger/Lazy;Lcom/squareup/ui/timecards/api/TimecardsHomeScreenRedirector;)Lcom/squareup/ui/main/OrderEntryHomeScreenSelector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Lcom/squareup/ui/timecards/api/TimecardsHomeScreenRedirector;",
            ")",
            "Lcom/squareup/ui/main/OrderEntryHomeScreenSelector;"
        }
    .end annotation

    .line 51
    new-instance v0, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector;-><init>(Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;Ldagger/Lazy;Lcom/squareup/ui/timecards/api/TimecardsHomeScreenRedirector;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/OrderEntryHomeScreenSelector;
    .locals 3

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector_Factory;->openTicketsSelectorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;

    iget-object v1, p0, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector_Factory;->timecardsHomeScreenRedirectorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/timecards/api/TimecardsHomeScreenRedirector;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector_Factory;->newInstance(Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;Ldagger/Lazy;Lcom/squareup/ui/timecards/api/TimecardsHomeScreenRedirector;)Lcom/squareup/ui/main/OrderEntryHomeScreenSelector;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector_Factory;->get()Lcom/squareup/ui/main/OrderEntryHomeScreenSelector;

    move-result-object v0

    return-object v0
.end method
