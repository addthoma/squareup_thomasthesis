.class Lcom/squareup/ui/payment/SwipeHandler$1;
.super Ljava/lang/Object;
.source "SwipeHandler.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/payment/SwipeHandler;->setSwipeHandler(Lmortar/MortarScope;Lcom/squareup/ui/seller/SwipedCardHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/payment/SwipeHandler;

.field final synthetic val$swipeHandler:Lcom/squareup/ui/seller/SwipedCardHandler;


# direct methods
.method constructor <init>(Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/ui/seller/SwipedCardHandler;)V
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/ui/payment/SwipeHandler$1;->this$0:Lcom/squareup/ui/payment/SwipeHandler;

    iput-object p2, p0, Lcom/squareup/ui/payment/SwipeHandler$1;->val$swipeHandler:Lcom/squareup/ui/seller/SwipedCardHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler$1;->this$0:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-static {v0}, Lcom/squareup/ui/payment/SwipeHandler;->access$100(Lcom/squareup/ui/payment/SwipeHandler;)Lcom/squareup/ui/seller/SwipedCardHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/payment/SwipeHandler$1;->val$swipeHandler:Lcom/squareup/ui/seller/SwipedCardHandler;

    if-ne v0, v1, :cond_0

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler$1;->this$0:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-static {v0}, Lcom/squareup/ui/payment/SwipeHandler;->access$200(Lcom/squareup/ui/payment/SwipeHandler;)Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/ui/payment/SwipeHandler;->access$102(Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/ui/seller/SwipedCardHandler;)Lcom/squareup/ui/seller/SwipedCardHandler;

    :cond_0
    return-void
.end method
