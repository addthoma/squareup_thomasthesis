.class public final Lcom/squareup/ui/systempermissions/AboutDeviceSettingsScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "AboutDeviceSettingsScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/systempermissions/AboutDeviceSettingsScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/systempermissions/AboutDeviceSettingsScreen$Component;,
        Lcom/squareup/ui/systempermissions/AboutDeviceSettingsScreen$ParentComponent;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/systempermissions/AboutDeviceSettingsScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    sget-object v0, Lcom/squareup/ui/systempermissions/-$$Lambda$AboutDeviceSettingsScreen$YpxrAAGTwnAXhrXdUsxOeRSHWHk;->INSTANCE:Lcom/squareup/ui/systempermissions/-$$Lambda$AboutDeviceSettingsScreen$YpxrAAGTwnAXhrXdUsxOeRSHWHk;

    .line 30
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/systempermissions/AboutDeviceSettingsScreen;
    .locals 0

    .line 30
    new-instance p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsScreen;

    invoke-direct {p0}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsScreen;-><init>()V

    return-object p0
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 33
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SYSTEM_PERMISSIONS_ENABLE_SETTINGS:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 37
    sget v0, Lcom/squareup/common/bootstrap/R$layout;->about_device_settings_view:I

    return v0
.end method
