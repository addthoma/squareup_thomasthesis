.class public final Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter_Factory;
.super Ljava/lang/Object;
.source "SystemPermissionsPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final askedPermissionsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/EnumSetLocalSetting<",
            "Lcom/squareup/systempermissions/SystemPermission;",
            ">;>;"
        }
    .end annotation
.end field

.field private final checkerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/systempermissions/SystemPermissionsChecker;",
            ">;"
        }
    .end annotation
.end field

.field private final pauseAndResumePresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumePresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/EnumSetLocalSetting<",
            "Lcom/squareup/systempermissions/SystemPermission;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/systempermissions/SystemPermissionsChecker;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter_Factory;->askedPermissionsProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter_Factory;->pauseAndResumePresenterProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter_Factory;->checkerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/EnumSetLocalSetting<",
            "Lcom/squareup/systempermissions/SystemPermission;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/systempermissions/SystemPermissionsChecker;",
            ">;)",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/EnumSetLocalSetting;Lcom/squareup/pauses/PauseAndResumePresenter;Lcom/squareup/systempermissions/SystemPermissionsChecker;)Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/EnumSetLocalSetting<",
            "Lcom/squareup/systempermissions/SystemPermission;",
            ">;",
            "Lcom/squareup/pauses/PauseAndResumePresenter;",
            "Lcom/squareup/systempermissions/SystemPermissionsChecker;",
            ")",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;-><init>(Lcom/squareup/settings/EnumSetLocalSetting;Lcom/squareup/pauses/PauseAndResumePresenter;Lcom/squareup/systempermissions/SystemPermissionsChecker;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter_Factory;->askedPermissionsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/EnumSetLocalSetting;

    iget-object v1, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter_Factory;->pauseAndResumePresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/pauses/PauseAndResumePresenter;

    iget-object v2, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter_Factory;->checkerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/systempermissions/SystemPermissionsChecker;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter_Factory;->newInstance(Lcom/squareup/settings/EnumSetLocalSetting;Lcom/squareup/pauses/PauseAndResumePresenter;Lcom/squareup/systempermissions/SystemPermissionsChecker;)Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter_Factory;->get()Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    move-result-object v0

    return-object v0
.end method
