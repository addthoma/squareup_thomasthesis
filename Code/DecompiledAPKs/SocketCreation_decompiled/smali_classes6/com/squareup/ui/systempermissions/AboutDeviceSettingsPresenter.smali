.class public Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;
.super Lmortar/ViewPresenter;
.source "AboutDeviceSettingsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/marin/widgets/MarinCardActionBar;

.field private final device:Lcom/squareup/util/Device;

.field private final flow:Lflow/Flow;


# direct methods
.method constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/marin/widgets/MarinCardActionBar;Lflow/Flow;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;->device:Lcom/squareup/util/Device;

    .line 23
    iput-object p2, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinCardActionBar;

    .line 24
    iput-object p3, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;->flow:Lflow/Flow;

    return-void
.end method


# virtual methods
.method isPhone()Z
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    return v0
.end method

.method isPortrait()Z
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPortrait()Z

    move-result v0

    return v0
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 28
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 30
    new-instance p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v1, "About Device Settings"

    .line 31
    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/systempermissions/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;

    invoke-direct {v1, v0}, Lcom/squareup/ui/systempermissions/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;-><init>(Lflow/Flow;)V

    .line 32
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 33
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinCardActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinCardActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method
