.class public interface abstract Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;
.super Ljava/lang/Object;
.source "SystemPermissionsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PermissionListener"
.end annotation


# virtual methods
.method public abstract onDenied(ILcom/squareup/systempermissions/SystemPermission;)V
.end method

.method public abstract onGranted(ILcom/squareup/systempermissions/SystemPermission;)V
.end method
