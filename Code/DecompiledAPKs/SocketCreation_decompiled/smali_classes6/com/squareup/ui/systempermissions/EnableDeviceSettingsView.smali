.class public Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;
.super Lcom/squareup/ui/StickyNoOverscrollScrollView;
.source "EnableDeviceSettingsView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field appNameFormatter:Lcom/squareup/util/AppNameFormatter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private enableDeviceStorageButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

.field private enableLocationButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

.field private enablePhoneAccessButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

.field private enableSettingsSubtitle:Lcom/squareup/widgets/MessageView;

.field private enableSettingsTitle:Lcom/squareup/widgets/MessageView;

.field private learnMoreMessage:Lcom/squareup/widgets/MessageView;

.field presenter:Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/StickyNoOverscrollScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    const-class p2, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsScreen$Component;->inject(Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 103
    sget v0, Lcom/squareup/common/bootstrap/R$id;->title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;->enableSettingsTitle:Lcom/squareup/widgets/MessageView;

    .line 104
    sget v0, Lcom/squareup/crm/R$id;->subtitle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;->enableSettingsSubtitle:Lcom/squareup/widgets/MessageView;

    .line 105
    sget v0, Lcom/squareup/common/bootstrap/R$id;->enable_device_settings_location_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphTextView;

    iput-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;->enableLocationButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 106
    sget v0, Lcom/squareup/common/bootstrap/R$id;->enable_device_settings_storage_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphTextView;

    iput-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;->enableDeviceStorageButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 107
    sget v0, Lcom/squareup/common/bootstrap/R$id;->enable_device_settings_phone_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphTextView;

    iput-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;->enablePhoneAccessButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 108
    sget v0, Lcom/squareup/common/bootstrap/R$id;->enable_device_settings_learn_more:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;->learnMoreMessage:Lcom/squareup/widgets/MessageView;

    return-void
.end method


# virtual methods
.method markPermissionGranted(Lcom/squareup/systempermissions/SystemPermission;)V
    .locals 3

    .line 83
    sget-object v0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView$5;->$SwitchMap$com$squareup$systempermissions$SystemPermission:[I

    invoke-virtual {p1}, Lcom/squareup/systempermissions/SystemPermission;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x2

    if-eq v0, v1, :cond_2

    if-eq v0, v2, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 91
    iget-object p1, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;->enablePhoneAccessButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    goto :goto_0

    .line 94
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown permission: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;->enableDeviceStorageButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    goto :goto_0

    .line 85
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;->enableLocationButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    :goto_0
    const/4 v0, 0x0

    .line 97
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setEnabled(Z)V

    .line 98
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, v0, v2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;I)V

    .line 99
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->requestLayout()V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;->presenter:Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;->presenter:Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->dropView(Ljava/lang/Object;)V

    .line 78
    invoke-super {p0}, Lcom/squareup/ui/StickyNoOverscrollScrollView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 42
    invoke-super {p0}, Lcom/squareup/ui/StickyNoOverscrollScrollView;->onFinishInflate()V

    .line 43
    invoke-direct {p0}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;->bindViews()V

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;->enableSettingsTitle:Lcom/squareup/widgets/MessageView;

    sget v1, Lcom/squareup/common/bootstrap/R$string;->enable_device_settings_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;->enableSettingsSubtitle:Lcom/squareup/widgets/MessageView;

    iget-object v1, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    sget v2, Lcom/squareup/common/bootstrap/R$string;->enable_device_settings_description:I

    invoke-interface {v1, v2}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;->enableLocationButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    new-instance v1, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView$1;-><init>(Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;->enableDeviceStorageButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    new-instance v1, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView$2;-><init>(Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;->enablePhoneAccessButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    new-instance v1, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView$3;-><init>(Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;->learnMoreMessage:Lcom/squareup/widgets/MessageView;

    new-instance v1, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView$4;-><init>(Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsView;->presenter:Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
