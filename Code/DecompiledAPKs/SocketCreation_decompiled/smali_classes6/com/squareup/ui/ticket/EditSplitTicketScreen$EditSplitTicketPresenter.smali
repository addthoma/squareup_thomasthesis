.class Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;
.super Lmortar/ViewPresenter;
.source "EditSplitTicketScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/EditSplitTicketScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "EditSplitTicketPresenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/ticket/EditSplitTicketView;",
        ">;"
    }
.end annotation


# static fields
.field private static final KEY_EDIT_TICKET_STATE:Ljava/lang/String; = "editTicketState"


# instance fields
.field private final cardNameHandler:Lcom/squareup/tickets/TicketCardNameHandler;

.field private editState:Lcom/squareup/ui/ticket/EditTicketState;

.field private final flow:Lflow/Flow;

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final predefinedTickets:Lcom/squareup/opentickets/PredefinedTickets;

.field private final res:Lcom/squareup/util/Res;

.field private splitTicketId:Ljava/lang/String;

.field private final splitTicketPresenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

.field private ticketGroups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/SplitTicketPresenter;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketCardNameHandler;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/tickets/OpenTicketsSettings;Lflow/Flow;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 69
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->splitTicketPresenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    .line 71
    iput-object p2, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->res:Lcom/squareup/util/Res;

    .line 72
    iput-object p3, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->cardNameHandler:Lcom/squareup/tickets/TicketCardNameHandler;

    .line 73
    iput-object p4, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->predefinedTickets:Lcom/squareup/opentickets/PredefinedTickets;

    .line 74
    iput-object p5, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 75
    iput-object p6, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->flow:Lflow/Flow;

    .line 76
    new-instance p1, Lcom/squareup/ui/ticket/EditTicketState;

    invoke-interface {p5}, Lcom/squareup/tickets/OpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result p2

    invoke-direct {p1, p2}, Lcom/squareup/ui/ticket/EditTicketState;-><init>(Z)V

    iput-object p1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    return-void
.end method

.method private getNameHint()Ljava/lang/String;
    .locals 2

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v1}, Lcom/squareup/tickets/OpenTicketsSettings;->isSwipeToCreateTicketAllowed()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_ticket_name_hint_swipe_allowed:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_ticket_name_hint:I

    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private loadTicketGroups()V
    .locals 2

    .line 200
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$EditSplitTicketScreen$EditSplitTicketPresenter$zDcYBqEjQfWMLgBxiaxkvR1LkDc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$EditSplitTicketScreen$EditSplitTicketPresenter$zDcYBqEjQfWMLgBxiaxkvR1LkDc;-><init>(Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private updateButtons()V
    .locals 2

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/EditTicketState;->setDeleteButtonVisible(Z)V

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/EditTicketState;->setVoidButtonVisible(Z)V

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/EditTicketState;->setCompButtonVisible(Z)V

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/EditTicketState;->setUncompButtonVisible(Z)V

    return-void
.end method


# virtual methods
.method buildActionBar()V
    .locals 4

    .line 130
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/EditSplitTicketView;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditSplitTicketView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    .line 131
    invoke-virtual {v2}, Lcom/squareup/ui/ticket/EditTicketState;->canSave()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->save:I

    .line 132
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/ticket/-$$Lambda$JU9T4bKFFmyTJYA7CZpvL3znm0M;

    invoke-direct {v2, p0}, Lcom/squareup/ui/ticket/-$$Lambda$JU9T4bKFFmyTJYA7CZpvL3znm0M;-><init>(Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;)V

    .line 133
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 134
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideSecondaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    const/4 v2, 0x1

    .line 135
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 136
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->getActionBarTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/ticket/-$$Lambda$adsQJ4flEhCi4c6PSUWXWkTmXLM;

    invoke-direct {v2, p0}, Lcom/squareup/ui/ticket/-$$Lambda$adsQJ4flEhCi4c6PSUWXWkTmXLM;-><init>(Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;)V

    .line 137
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 138
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 130
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method closeCard()V
    .locals 1

    .line 182
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/EditSplitTicketView;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditSplitTicketView;->hideKeyboard()V

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method getActionBarTitle()Ljava/lang/String;
    .locals 2

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketState;->isConvertingToCustomTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->predefined_tickets_convert_to_custom_ticket:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/EditTicketState;->isCustomTicket()Z

    move-result v1

    if-eqz v1, :cond_1

    sget v1, Lcom/squareup/orderentry/R$string;->predefined_tickets_edit_custom_ticket:I

    goto :goto_0

    :cond_1
    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_edit_ticket:I

    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$loadTicketGroups$2$EditSplitTicketScreen$EditSplitTicketPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->predefinedTickets:Lcom/squareup/opentickets/PredefinedTickets;

    invoke-interface {v0}, Lcom/squareup/opentickets/PredefinedTickets;->getAllTicketGroups()Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$EditSplitTicketScreen$EditSplitTicketPresenter$1mrzwFbhI-oNd_nCUg4blzzzNx0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$EditSplitTicketScreen$EditSplitTicketPresenter$1mrzwFbhI-oNd_nCUg4blzzzNx0;-><init>(Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;)V

    .line 202
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$1$EditSplitTicketScreen$EditSplitTicketPresenter(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 203
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->ticketGroups:Ljava/util/List;

    .line 204
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/EditSplitTicketView;

    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->ticketGroups:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/EditSplitTicketView;->setTicketGroups(Ljava/util/List;)V

    return-void
.end method

.method public synthetic lambda$onLoad$0$EditSplitTicketScreen$EditSplitTicketPresenter(Lcom/squareup/ui/ticket/EditSplitTicketView;Ljava/lang/String;Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 1

    .line 109
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_0

    .line 110
    iget-object p3, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Lcom/squareup/ui/ticket/EditTicketState;->setCardNotStoredMessageVisible(Z)V

    .line 111
    iget-object p3, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {p3, p2}, Lcom/squareup/ui/ticket/EditTicketState;->setName(Ljava/lang/String;)V

    .line 112
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->hasView()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 113
    invoke-virtual {p1}, Lcom/squareup/ui/ticket/EditSplitTicketView;->refresh()V

    .line 116
    :cond_0
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method onCancelSelected()V
    .locals 0

    .line 152
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->closeCard()V

    return-void
.end method

.method onConvertToCustomTicketClicked()V
    .locals 3

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketState;->convertToCustomTicket()V

    .line 161
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/EditSplitTicketView;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditSplitTicketView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->getActionBarTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 162
    invoke-direct {p0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->loadTicketGroups()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 80
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->cardNameHandler:Lcom/squareup/tickets/TicketCardNameHandler;

    invoke-interface {v0, p1}, Lcom/squareup/tickets/TicketCardNameHandler;->registerToScope(Lmortar/MortarScope;)V

    .line 82
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->splitTicketPresenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getEditingSplitTicketId()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->splitTicketId:Ljava/lang/String;

    .line 83
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->splitTicketPresenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->splitTicketId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getTicketName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/EditTicketState;->setName(Ljava/lang/String;)V

    .line 84
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-direct {p0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->getNameHint()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/EditTicketState;->setNameHint(Ljava/lang/String;)V

    .line 85
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->splitTicketPresenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->splitTicketId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getTicketNote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/EditTicketState;->setNote(Ljava/lang/String;)V

    .line 86
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->splitTicketPresenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->splitTicketId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getTicketGroup(Ljava/lang/String;)Lcom/squareup/api/items/TicketGroup;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/EditTicketState;->setSelectedGroup(Lcom/squareup/api/items/TicketGroup;)V

    .line 87
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->splitTicketPresenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->splitTicketId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getTicketTemplate(Ljava/lang/String;)Lcom/squareup/api/items/TicketTemplate;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/EditTicketState;->setTicketTemplate(Lcom/squareup/api/items/TicketTemplate;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 2

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->splitTicketPresenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->setEditingSplitTicketId(Ljava/lang/String;)V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 91
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 92
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/EditSplitTicketView;

    if-nez p1, :cond_0

    .line 95
    invoke-direct {p0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->updateButtons()V

    goto :goto_0

    :cond_0
    const-string v1, "editTicketState"

    .line 97
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/EditTicketState;

    iput-object p1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    .line 100
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->buildActionBar()V

    .line 101
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/EditSplitTicketView;->setEditState(Lcom/squareup/ui/ticket/EditTicketState;)V

    .line 103
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/EditTicketState;->areTicketGroupsVisible()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 104
    invoke-direct {p0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->loadTicketGroups()V

    .line 107
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditSplitTicketView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object p1

    .line 108
    iget-object v1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->cardNameHandler:Lcom/squareup/tickets/TicketCardNameHandler;

    new-instance v2, Lcom/squareup/ui/ticket/-$$Lambda$EditSplitTicketScreen$EditSplitTicketPresenter$fIZLeGeQnhoqmIrJO3ObLlU50EM;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/ticket/-$$Lambda$EditSplitTicketScreen$EditSplitTicketPresenter$fIZLeGeQnhoqmIrJO3ObLlU50EM;-><init>(Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;Lcom/squareup/ui/ticket/EditSplitTicketView;)V

    invoke-interface {v1, p1, v2}, Lcom/squareup/tickets/TicketCardNameHandler;->setCallback(Lmortar/MortarScope;Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 121
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    const-string v1, "editTicketState"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method onSaveClicked()V
    .locals 5

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketState;->getName()Ljava/lang/String;

    move-result-object v0

    .line 167
    iget-object v1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/EditTicketState;->getNote()Ljava/lang/String;

    move-result-object v1

    .line 168
    iget-object v2, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v2}, Lcom/squareup/ui/ticket/EditTicketState;->getSelectedGroup()Lcom/squareup/api/items/TicketGroup;

    move-result-object v2

    .line 169
    iget-object v3, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v3}, Lcom/squareup/ui/ticket/EditTicketState;->getTicketTemplate()Lcom/squareup/api/items/TicketTemplate;

    move-result-object v3

    if-nez v2, :cond_0

    if-nez v3, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    .line 170
    :cond_0
    new-instance v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;

    invoke-direct {v4}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;-><init>()V

    .line 173
    invoke-virtual {v4, v2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;->ticket_group(Lcom/squareup/api/items/TicketGroup;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;

    move-result-object v2

    .line 174
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;->ticket_template(Lcom/squareup/api/items/TicketTemplate;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;

    move-result-object v2

    .line 175
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object v2

    .line 177
    :goto_0
    iget-object v3, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->splitTicketPresenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v4, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->splitTicketId:Ljava/lang/String;

    invoke-virtual {v3, v4, v0, v1, v2}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->updateTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V

    .line 178
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->closeCard()V

    return-void
.end method

.method onTicketNameChanged()V
    .locals 2

    .line 156
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/EditSplitTicketView;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditSplitTicketView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/EditTicketState;->canSave()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method
