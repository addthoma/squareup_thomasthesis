.class Lcom/squareup/ui/ticket/NewTicketView$1;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "NewTicketView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ticket/NewTicketView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/ticket/NewTicketView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/NewTicketView;)V
    .locals 0

    .line 326
    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$1;->this$0:Lcom/squareup/ui/ticket/NewTicketView;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 328
    iget-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$1;->this$0:Lcom/squareup/ui/ticket/NewTicketView;

    iget-object p1, p1, Lcom/squareup/ui/ticket/NewTicketView;->presenter:Lcom/squareup/ui/ticket/NewTicketPresenter;

    iget-object p2, p0, Lcom/squareup/ui/ticket/NewTicketView$1;->this$0:Lcom/squareup/ui/ticket/NewTicketView;

    invoke-static {p2}, Lcom/squareup/ui/ticket/NewTicketView;->access$700(Lcom/squareup/ui/ticket/NewTicketView;)Lcom/squareup/ui/XableEditText;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/ticket/NewTicketPresenter;->onSearchTextChanged(Ljava/lang/String;)V

    return-void
.end method
