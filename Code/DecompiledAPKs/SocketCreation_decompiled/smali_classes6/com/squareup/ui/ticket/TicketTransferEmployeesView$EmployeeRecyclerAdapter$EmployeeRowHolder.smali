.class Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeeRowHolder;
.super Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeesAdapterHolder;
.source "TicketTransferEmployeesView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EmployeeRowHolder"
.end annotation


# instance fields
.field private final employeeName:Landroid/widget/TextView;

.field private final radioBox:Lcom/squareup/marin/widgets/MarinRadioBox;

.field final synthetic this$1:Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;Landroid/view/ViewGroup;)V
    .locals 1

    .line 61
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeeRowHolder;->this$1:Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;

    .line 62
    sget v0, Lcom/squareup/orderentry/R$layout;->ticket_employee_row:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeesAdapterHolder;-><init>(Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;Landroid/view/ViewGroup;I)V

    .line 63
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeeRowHolder;->itemView:Landroid/view/View;

    sget p2, Lcom/squareup/orderentry/R$id;->ticket_transfer_employee_name:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeeRowHolder;->employeeName:Landroid/widget/TextView;

    .line 64
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeeRowHolder;->itemView:Landroid/view/View;

    sget p2, Lcom/squareup/orderentry/R$id;->ticket_transfer_employee_radiobox:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/MarinRadioBox;

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeeRowHolder;->radioBox:Lcom/squareup/marin/widgets/MarinRadioBox;

    return-void
.end method


# virtual methods
.method bindRow(I)V
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeeRowHolder;->this$1:Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;->employeeList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;

    .line 69
    iget-object v0, p1, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;->info:Lcom/squareup/permissions/EmployeeInfo;

    iget-object v0, v0, Lcom/squareup/permissions/EmployeeInfo;->employeeToken:Ljava/lang/String;

    .line 70
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeeRowHolder;->employeeName:Landroid/widget/TextView;

    iget-object p1, p1, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;->displayName:Ljava/lang/String;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeeRowHolder;->radioBox:Lcom/squareup/marin/widgets/MarinRadioBox;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeeRowHolder;->this$1:Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;

    iget-object v1, v1, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/TicketTransferEmployeesView;

    iget-object v1, v1, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->presenter:Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->isSelected(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinRadioBox;->setChecked(Z)V

    .line 72
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeeRowHolder;->itemView:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeeRowHolder$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeeRowHolder$1;-><init>(Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeeRowHolder;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
