.class public Lcom/squareup/ui/ticket/BaseTicketListView;
.super Landroid/widget/LinearLayout;
.source "BaseTicketListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;,
        Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;,
        Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;
    }
.end annotation


# static fields
.field private static final SEARCH_DELAY_MS:J = 0x64L


# instance fields
.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final fadeDurationShort:I

.field private final gutter:I

.field private final gutterHalf:I

.field private final gutterQuarter:I

.field private noTicketsErrorMessage:Lcom/squareup/widgets/MessageView;

.field private noTicketsErrorTitle:Lcom/squareup/marketfont/MarketTextView;

.field private noTicketsErrorView:Landroid/widget/LinearLayout;

.field private noTicketsTitle:Lcom/squareup/marketfont/MarketTextView;

.field private noTicketsView:Landroid/widget/LinearLayout;

.field presenter:Lcom/squareup/ui/ticket/TicketListPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

.field private recyclerAdapter:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

.field protected recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private searchBar:Lcom/squareup/ui/XableEditText;

.field private final searchRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .line 569
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 570
    new-instance p2, Lcom/squareup/ui/ticket/-$$Lambda$BaseTicketListView$__3o0f6L5SCQdluOlgvWK0I8IH4;

    invoke-direct {p2, p0}, Lcom/squareup/ui/ticket/-$$Lambda$BaseTicketListView$__3o0f6L5SCQdluOlgvWK0I8IH4;-><init>(Lcom/squareup/ui/ticket/BaseTicketListView;)V

    iput-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->searchRunnable:Ljava/lang/Runnable;

    .line 572
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/BaseTicketListView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    .line 573
    sget v0, Lcom/squareup/marin/R$dimen;->marin_gutter:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->gutter:I

    .line 574
    sget v0, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->gutterHalf:I

    .line 575
    sget v0, Lcom/squareup/marin/R$dimen;->marin_gutter_quarter:I

    .line 576
    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->gutterQuarter:I

    const/high16 v0, 0x10e0000

    .line 578
    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p2

    int-to-double v0, p2

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    div-double/2addr v0, v2

    double-to-int p2, v0

    iput p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->fadeDurationShort:I

    .line 580
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/BaseTicketListView;->isInEditMode()Z

    move-result p2

    if-eqz p2, :cond_0

    return-void

    .line 582
    :cond_0
    const-class p2, Lcom/squareup/ui/ticket/TicketListPresenter$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketListPresenter$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/ticket/TicketListPresenter$Component;->inject(Lcom/squareup/ui/ticket/BaseTicketListView;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/squareup/ui/ticket/BaseTicketListView;)Ljava/lang/Runnable;
    .locals 0

    .line 71
    iget-object p0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->searchRunnable:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/ticket/BaseTicketListView;)I
    .locals 0

    .line 71
    iget p0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->gutterHalf:I

    return p0
.end method

.method static synthetic access$700(Lcom/squareup/ui/ticket/BaseTicketListView;)I
    .locals 0

    .line 71
    iget p0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->gutterQuarter:I

    return p0
.end method

.method static synthetic access$900(Lcom/squareup/ui/ticket/BaseTicketListView;)I
    .locals 0

    .line 71
    iget p0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->gutter:I

    return p0
.end method

.method private bindViews()V
    .locals 1

    .line 685
    sget v0, Lcom/squareup/orderentry/R$id;->ticket_list_view_recycler_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 686
    sget v0, Lcom/squareup/orderentry/R$id;->ticket_list_view_search_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->searchBar:Lcom/squareup/ui/XableEditText;

    .line 687
    sget v0, Lcom/squareup/orderentry/R$id;->ticket_list_no_tickets:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->noTicketsView:Landroid/widget/LinearLayout;

    .line 688
    sget v0, Lcom/squareup/orderentry/R$id;->detail_confirmation_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->noTicketsTitle:Lcom/squareup/marketfont/MarketTextView;

    .line 689
    sget v0, Lcom/squareup/orderentry/R$id;->ticket_error_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->noTicketsErrorView:Landroid/widget/LinearLayout;

    .line 690
    sget v0, Lcom/squareup/orderentry/R$id;->ticket_error_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->noTicketsErrorTitle:Lcom/squareup/marketfont/MarketTextView;

    .line 691
    sget v0, Lcom/squareup/orderentry/R$id;->ticket_error_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->noTicketsErrorMessage:Lcom/squareup/widgets/MessageView;

    .line 692
    sget v0, Lcom/squareup/orderentry/R$id;->ticket_list_view_progress:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/DelayedLoadingProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    return-void
.end method


# virtual methods
.method clearSearchBar()V
    .locals 2

    .line 649
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->searchBar:Lcom/squareup/ui/XableEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected getTicketRowText()Ljava/lang/CharSequence;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method hideSoftKeyboard()V
    .locals 0

    .line 681
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method hideSpinner()V
    .locals 1

    .line 677
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    invoke-virtual {v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->hide()V

    return-void
.end method

.method public synthetic lambda$new$0$BaseTicketListView()V
    .locals 1

    .line 570
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->searchBar:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/ticket/BaseTicketListView;->onStartSearch(Ljava/lang/String;)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 586
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 587
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/BaseTicketListView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 588
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->presenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onBindTicketRowPhone(Lcom/squareup/tickets/TicketRowCursorList$TicketRow;)V
    .locals 0

    return-void
.end method

.method protected onBindTicketRowTablet(Lcom/squareup/tickets/TicketRowCursorList$TicketRow;Landroid/widget/CompoundButton;)V
    .locals 0

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 615
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->presenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ticket/TicketListPresenter;->dropView(Ljava/lang/Object;)V

    .line 616
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 592
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 593
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/BaseTicketListView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 594
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/ticket/BaseTicketListView;->bindViews()V

    .line 596
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/BaseTicketListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 597
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;-><init>(Lcom/squareup/ui/ticket/BaseTicketListView;)V

    iput-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->recyclerAdapter:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    .line 598
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->recyclerAdapter:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 599
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setOverScrollMode(I)V

    .line 601
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->searchBar:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/ticket/BaseTicketListView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/BaseTicketListView$1;-><init>(Lcom/squareup/ui/ticket/BaseTicketListView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 610
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$iw3N6K0-E-4u1UP3JArBgJrR_m0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$iw3N6K0-E-4u1UP3JArBgJrR_m0;-><init>(Lcom/squareup/ui/ticket/BaseTicketListView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/DelayedLoadingProgressBar;->setCallback(Lcom/squareup/ui/DelayedLoadingProgressBar$DelayedLoadingProgressBarCallback;)V

    .line 611
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    invoke-virtual {v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->show()V

    return-void
.end method

.method protected onHideProgress()V
    .locals 0

    return-void
.end method

.method protected onNewTicketClicked(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method protected onStartSearch(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected onTemplateClicked(Landroid/widget/CompoundButton;Lcom/squareup/ui/ticket/TicketInfo;)V
    .locals 0

    return-void
.end method

.method protected onTicketClicked(Landroid/widget/CompoundButton;Lcom/squareup/ui/ticket/TicketInfo;)V
    .locals 0

    return-void
.end method

.method setList(Lcom/squareup/ui/Ranger;Lcom/squareup/tickets/TicketRowCursorList;Ljava/util/Map;Ljava/util/List;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/Ranger<",
            "Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;",
            "Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;",
            ">;",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            "Ljava/util/Map<",
            "Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;Z)V"
        }
    .end annotation

    .line 632
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->recyclerAdapter:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->setList(Lcom/squareup/ui/Ranger;Lcom/squareup/tickets/TicketRowCursorList;Ljava/util/Map;Ljava/util/List;Z)V

    return-void
.end method

.method setListVisible(Z)V
    .locals 1

    .line 620
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 621
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->noTicketsView:Landroid/widget/LinearLayout;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setSearchBarHint(Ljava/lang/String;)V
    .locals 1

    .line 645
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->searchBar:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setHint(Ljava/lang/String;)V

    return-void
.end method

.method setSearchBarVisible(Z)V
    .locals 1

    .line 637
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->searchBar:Lcom/squareup/ui/XableEditText;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showListWithFade()V
    .locals 2

    .line 625
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget v1, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->fadeDurationShort:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    .line 626
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->noTicketsView:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showNoTicketsMessage(Ljava/lang/String;ZZ)V
    .locals 2

    .line 653
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->searchBar:Lcom/squareup/ui/XableEditText;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 654
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    const/4 v0, 0x1

    if-eqz p2, :cond_1

    .line 657
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->noTicketsErrorTitle:Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->presenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/TicketListPresenter;->getErrorTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 658
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->noTicketsErrorMessage:Lcom/squareup/widgets/MessageView;

    iget-object v1, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->presenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/TicketListPresenter;->getErrorMessage()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p3, :cond_0

    .line 660
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->noTicketsErrorView:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->fadeDurationShort:I

    invoke-static {p2, v1}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 662
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->noTicketsErrorView:Landroid/widget/LinearLayout;

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    goto :goto_0

    .line 665
    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->noTicketsErrorView:Landroid/widget/LinearLayout;

    invoke-static {p2, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 668
    :goto_0
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->noTicketsTitle:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p2, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p3, :cond_2

    .line 670
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->noTicketsView:Landroid/widget/LinearLayout;

    iget p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->fadeDurationShort:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_1

    .line 672
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->noTicketsView:Landroid/widget/LinearLayout;

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    :goto_1
    return-void
.end method

.method showSearchBarWithFade()V
    .locals 2

    .line 641
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->searchBar:Lcom/squareup/ui/XableEditText;

    iget v1, p0, Lcom/squareup/ui/ticket/BaseTicketListView;->fadeDurationShort:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    return-void
.end method
