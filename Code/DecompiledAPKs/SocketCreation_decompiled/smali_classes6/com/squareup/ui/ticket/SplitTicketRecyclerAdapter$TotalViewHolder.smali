.class Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TotalViewHolder;
.super Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ViewHolder;
.source "SplitTicketRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TotalViewHolder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

.field private final totalRow:Lcom/squareup/ui/cart/CartEntryView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;Landroid/view/View;)V
    .locals 0

    .line 353
    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TotalViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    .line 354
    invoke-direct {p0, p2}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 355
    sget p1, Lcom/squareup/orderentry/R$id;->split_ticket_tax_total:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/CartEntryView;

    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TotalViewHolder;->totalRow:Lcom/squareup/ui/cart/CartEntryView;

    return-void
.end method


# virtual methods
.method public bind()V
    .locals 5

    .line 359
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TotalViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v0}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$200(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/ticket/SplitTicketPresenter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TotalViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->ticketMutable(Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 360
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TotalViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    .line 361
    invoke-static {v1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$500(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    move-result-object v1

    sget v2, Lcom/squareup/orderentry/R$string;->open_tickets_total:I

    iget-object v3, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TotalViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v3}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$200(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/ticket/SplitTicketPresenter;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TotalViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v4}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getOrderTotal(Ljava/lang/String;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, Lcom/squareup/ui/cart/CartEntryViewModelFactory;->total(ILcom/squareup/protos/common/Money;Z)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object v1

    .line 363
    iget-object v2, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TotalViewHolder;->totalRow:Lcom/squareup/ui/cart/CartEntryView;

    invoke-virtual {v2, v1}, Lcom/squareup/ui/cart/CartEntryView;->present(Lcom/squareup/ui/cart/CartEntryViewModel;)V

    if-nez v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TotalViewHolder;->totalRow:Lcom/squareup/ui/cart/CartEntryView;

    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TotalViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$800(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$900(Landroid/view/View;I)V

    :cond_0
    return-void
.end method
