.class public final Lcom/squareup/ui/ticket/EditSplitTicketScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "EditSplitTicketScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/ui/main/EmvSwipePassthroughEnabler$SwipePassthrough;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/ticket/EditSplitTicketScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/EditSplitTicketScreen$Component;,
        Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/ticket/EditSplitTicketScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/ticket/EditSplitTicketScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/ui/ticket/EditSplitTicketScreen;

    invoke-direct {v0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/ticket/EditSplitTicketScreen;->INSTANCE:Lcom/squareup/ui/ticket/EditSplitTicketScreen;

    .line 215
    sget-object v0, Lcom/squareup/ui/ticket/EditSplitTicketScreen;->INSTANCE:Lcom/squareup/ui/ticket/EditSplitTicketScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/ticket/EditSplitTicketScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    return-void
.end method


# virtual methods
.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 48
    sget-object v0, Lcom/squareup/ui/ticket/SplitTicketScreen;->INSTANCE:Lcom/squareup/ui/ticket/SplitTicketScreen;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 218
    sget v0, Lcom/squareup/orderentry/R$layout;->edit_split_ticket_view:I

    return v0
.end method
