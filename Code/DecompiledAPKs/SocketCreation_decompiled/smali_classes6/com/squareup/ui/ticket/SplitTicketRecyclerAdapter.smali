.class Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "SplitTicketRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TotalViewHolder;,
        Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TaxViewHolder;,
        Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CompViewHolder;,
        Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TicketNoteViewHolder;,
        Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$PerItemDiscountViewHolder;,
        Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;,
        Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;,
        Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final BOTTOM_BORDER_SELECTOR_ID:I

.field private static final ORDER_CASH_DISCOUNT_TYPE:I = 0x2

.field private static final ORDER_COMP_TYPE:I = 0x6

.field private static final ORDER_ITEM_TYPE:I = 0x1

.field private static final ORDER_PER_ITEM_DISCOUNT_TYPE:I = 0x3

.field private static final ORDER_TAX_TYPE:I = 0x4

.field private static final ORDER_TOTAL_TYPE:I = 0x5

.field private static final TICKET_NOTE_TYPE:I


# instance fields
.field private final checkBoxSize:I

.field private final gutter:I

.field private final presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

.field private final splitTicketWidth:I

.field private final ticketId:Ljava/lang/String;

.field private final viewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 34
    sget v0, Lcom/squareup/marin/R$drawable;->marin_selector_clear_border_bottom_light_gray_1px:I

    sput v0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->BOTTOM_BORDER_SELECTOR_ID:I

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/ticket/SplitTicketPresenter;Ljava/lang/String;Lcom/squareup/util/Res;)V
    .locals 0

    .line 44
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    .line 46
    invoke-virtual {p1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getCartEntryViewModelFactory()Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->viewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    .line 47
    iput-object p2, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->ticketId:Ljava/lang/String;

    .line 49
    sget p1, Lcom/squareup/marin/R$dimen;->marin_split_ticket_gutter:I

    invoke-interface {p3, p1}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->gutter:I

    .line 50
    sget p1, Lcom/squareup/marin/R$dimen;->marin_checkbox_size:I

    invoke-interface {p3, p1}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->checkBoxSize:I

    .line 51
    sget p1, Lcom/squareup/marin/R$dimen;->marin_split_ticket_width:I

    .line 52
    invoke-interface {p3, p1}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->splitTicketWidth:I

    const/4 p1, 0x1

    .line 53
    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->setHasStableIds(Z)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;I)I
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->positionAdjustedForNote(I)I

    move-result p0

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Ljava/lang/String;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->ticketId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/ticket/SplitTicketPresenter;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    return-object p0
.end method

.method static synthetic access$300()I
    .locals 1

    .line 23
    sget v0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->BOTTOM_BORDER_SELECTOR_ID:I

    return v0
.end method

.method static synthetic access$500(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/cart/CartEntryViewModelFactory;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->viewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;I)I
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->positionAdjustedForDiscount(I)I

    move-result p0

    return p0
.end method

.method static synthetic access$800(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)I
    .locals 0

    .line 23
    iget p0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->gutter:I

    return p0
.end method

.method static synthetic access$900(Landroid/view/View;I)V
    .locals 0

    .line 23
    invoke-static {p0, p1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->setLeftPadding(Landroid/view/View;I)V

    return-void
.end method

.method private getLastItemPosition()I
    .locals 3

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->ticketId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getItemCount(Ljava/lang/String;)I

    move-result v0

    .line 152
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v2, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->ticketId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->shouldShowTicketNote(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method private positionAdjustedForDiscount(I)I
    .locals 2

    .line 167
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->positionAdjustedForNote(I)I

    move-result p1

    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->ticketId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getItemCount(Ljava/lang/String;)I

    move-result v0

    sub-int/2addr p1, v0

    return p1
.end method

.method private positionAdjustedForNote(I)I
    .locals 2

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->ticketId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->shouldShowTicketNote(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 p1, p1, -0x1

    :cond_0
    return p1
.end method

.method private static setLeftPadding(Landroid/view/View;I)V
    .locals 3

    .line 171
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    .line 172
    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    .line 171
    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 3

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->ticketId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getItemCount(Ljava/lang/String;)I

    move-result v0

    .line 126
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v2, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->ticketId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->shouldShowTicketNote(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    .line 129
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v2, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->ticketId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getCartLevelCashDiscountCount(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 130
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v2, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->ticketId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->shouldShowPerItemDiscounts(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    .line 133
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v2, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->ticketId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->shouldShowComps(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    add-int/lit8 v0, v0, 0x1

    .line 136
    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v2, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->ticketId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->shouldShowTaxes(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    add-int/lit8 v0, v0, 0x1

    .line 139
    :cond_3
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v2, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->ticketId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->shouldShowTotal(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    add-int/lit8 v0, v0, 0x1

    :cond_4
    return v0
.end method

.method public getItemId(I)J
    .locals 2

    if-nez p1, :cond_0

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->ticketId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->shouldShowTicketNote(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 117
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->getLastItemPosition()I

    move-result v0

    if-le p1, v0, :cond_2

    :cond_1
    neg-int p1, p1

    :goto_0
    int-to-long v0, p1

    return-wide v0

    .line 120
    :cond_2
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->positionAdjustedForNote(I)I

    move-result p1

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->ticketId:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getItem(Ljava/lang/String;I)Lcom/squareup/checkout/CartItem;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 8

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->ticketId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->shouldShowTicketNote(Ljava/lang/String;)Z

    move-result v0

    .line 58
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v2, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->ticketId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->shouldShowPerItemDiscounts(Ljava/lang/String;)Z

    move-result v1

    .line 59
    iget-object v2, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v3, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->ticketId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->shouldShowComps(Ljava/lang/String;)Z

    move-result v2

    .line 60
    iget-object v3, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v4, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->ticketId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->shouldShowTaxes(Ljava/lang/String;)Z

    move-result v3

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 64
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->getLastItemPosition()I

    move-result v0

    .line 66
    invoke-direct {p0}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->getLastItemPosition()I

    move-result v4

    iget-object v5, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v6, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->ticketId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getCartLevelCashDiscountCount(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    if-eqz v1, :cond_1

    add-int/lit8 v5, v4, 0x1

    goto :goto_0

    :cond_1
    move v5, v4

    :goto_0
    if-eqz v2, :cond_2

    add-int/lit8 v6, v5, 0x1

    goto :goto_1

    :cond_2
    move v6, v5

    :goto_1
    add-int/lit8 v7, v6, 0x1

    if-gt p1, v0, :cond_3

    const/4 p1, 0x1

    return p1

    :cond_3
    if-gt p1, v4, :cond_4

    const/4 p1, 0x2

    return p1

    :cond_4
    if-ne p1, v5, :cond_5

    if-eqz v1, :cond_5

    const/4 p1, 0x3

    return p1

    :cond_5
    if-ne p1, v6, :cond_6

    if-eqz v2, :cond_6

    const/4 p1, 0x6

    return p1

    :cond_6
    if-ne p1, v7, :cond_7

    if-eqz v3, :cond_7

    const/4 p1, 0x4

    return p1

    :cond_7
    const/4 p1, 0x5

    return p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->onBindViewHolder(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ViewHolder;I)V
    .locals 0

    .line 147
    invoke-virtual {p1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ViewHolder;->bind()V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 23
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ViewHolder;
    .locals 2

    packed-switch p2, :pswitch_data_0

    .line 111
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid view type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 102
    :pswitch_0
    new-instance p2, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CompViewHolder;

    sget v0, Lcom/squareup/orderentry/R$layout;->split_ticket_tax_total_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CompViewHolder;-><init>(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;Landroid/view/View;)V

    return-object p2

    .line 108
    :pswitch_1
    new-instance p2, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TotalViewHolder;

    sget v0, Lcom/squareup/orderentry/R$layout;->split_ticket_tax_total_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TotalViewHolder;-><init>(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;Landroid/view/View;)V

    return-object p2

    .line 105
    :pswitch_2
    new-instance p2, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TaxViewHolder;

    sget v0, Lcom/squareup/orderentry/R$layout;->split_ticket_tax_total_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TaxViewHolder;-><init>(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;Landroid/view/View;)V

    return-object p2

    .line 99
    :pswitch_3
    new-instance p2, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$PerItemDiscountViewHolder;

    sget v0, Lcom/squareup/orderentry/R$layout;->split_ticket_tax_total_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$PerItemDiscountViewHolder;-><init>(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;Landroid/view/View;)V

    return-object p2

    .line 96
    :pswitch_4
    new-instance p2, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;

    sget v0, Lcom/squareup/orderentry/R$layout;->split_ticket_item_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;-><init>(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;Landroid/view/View;)V

    return-object p2

    .line 93
    :pswitch_5
    new-instance p2, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;

    sget v0, Lcom/squareup/orderentry/R$layout;->split_ticket_item_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;-><init>(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;Landroid/view/View;)V

    return-object p2

    .line 90
    :pswitch_6
    new-instance p2, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TicketNoteViewHolder;

    sget v0, Lcom/squareup/orderentry/R$layout;->split_ticket_note_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TicketNoteViewHolder;-><init>(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;Landroid/view/View;)V

    return-object p2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
