.class public final Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "TicketCompScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final compDiscountsCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/CompDiscountsCache;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionCompsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TransactionComps;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/CompDiscountsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TransactionComps;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)V"
        }
    .end annotation

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p2, p0, Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p3, p0, Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;->compDiscountsCacheProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p4, p0, Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p5, p0, Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p6, p0, Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;->transactionCompsProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p7, p0, Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p8, p0, Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p9, p0, Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;->currencyProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/CompDiscountsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TransactionComps;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)",
            "Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;"
        }
    .end annotation

    .line 72
    new-instance v10, Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/CompDiscountsCache;Lflow/Flow;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionComps;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/tickets/voidcomp/CompDiscountsCache;",
            "Lflow/Flow;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/payment/TransactionComps;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;"
        }
    .end annotation

    .line 79
    new-instance v10, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/CompDiscountsCache;Lflow/Flow;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionComps;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;
    .locals 10

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;->compDiscountsCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/tickets/voidcomp/CompDiscountsCache;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;->transactionCompsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/payment/TransactionComps;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;->currencyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static/range {v1 .. v9}, Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;->newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/CompDiscountsCache;Lflow/Flow;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionComps;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketCompScreen_Presenter_Factory;->get()Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
