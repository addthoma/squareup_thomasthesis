.class final enum Lcom/squareup/ui/ticket/NewTicketView$RowType;
.super Ljava/lang/Enum;
.source "NewTicketView.java"

# interfaces
.implements Lcom/squareup/ui/Ranger$RowType;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/NewTicketView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "RowType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/ticket/NewTicketView$RowType;",
        ">;",
        "Lcom/squareup/ui/Ranger$RowType<",
        "Lcom/squareup/ui/ticket/NewTicketView$HolderType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/ticket/NewTicketView$RowType;

.field public static final enum CUSTOM_TICKET_BUTTON_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

.field public static final enum NO_RESULTS_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

.field public static final enum NO_TICKETS_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

.field public static final enum SELECT_TICKET_GROUP_HEADER_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

.field public static final enum TICKET_GROUP_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

.field public static final enum TICKET_TEMPLATE_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;


# instance fields
.field private final holderType:Lcom/squareup/ui/ticket/NewTicketView$HolderType;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 65
    new-instance v0, Lcom/squareup/ui/ticket/NewTicketView$RowType;

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$HolderType;->CUSTOM_TICKET_BUTTON_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    const/4 v2, 0x0

    const-string v3, "CUSTOM_TICKET_BUTTON_ROW"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/ui/ticket/NewTicketView$RowType;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/NewTicketView$HolderType;)V

    sput-object v0, Lcom/squareup/ui/ticket/NewTicketView$RowType;->CUSTOM_TICKET_BUTTON_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

    .line 66
    new-instance v0, Lcom/squareup/ui/ticket/NewTicketView$RowType;

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$HolderType;->SELECT_TICKET_GROUP_HEADER_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    const/4 v3, 0x1

    const-string v4, "SELECT_TICKET_GROUP_HEADER_ROW"

    invoke-direct {v0, v4, v3, v1}, Lcom/squareup/ui/ticket/NewTicketView$RowType;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/NewTicketView$HolderType;)V

    sput-object v0, Lcom/squareup/ui/ticket/NewTicketView$RowType;->SELECT_TICKET_GROUP_HEADER_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

    .line 67
    new-instance v0, Lcom/squareup/ui/ticket/NewTicketView$RowType;

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$HolderType;->NO_RESULTS_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    const/4 v4, 0x2

    const-string v5, "NO_RESULTS_ROW"

    invoke-direct {v0, v5, v4, v1}, Lcom/squareup/ui/ticket/NewTicketView$RowType;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/NewTicketView$HolderType;)V

    sput-object v0, Lcom/squareup/ui/ticket/NewTicketView$RowType;->NO_RESULTS_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

    .line 68
    new-instance v0, Lcom/squareup/ui/ticket/NewTicketView$RowType;

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$HolderType;->NO_TICKETS_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    const/4 v5, 0x3

    const-string v6, "NO_TICKETS_ROW"

    invoke-direct {v0, v6, v5, v1}, Lcom/squareup/ui/ticket/NewTicketView$RowType;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/NewTicketView$HolderType;)V

    sput-object v0, Lcom/squareup/ui/ticket/NewTicketView$RowType;->NO_TICKETS_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

    .line 69
    new-instance v0, Lcom/squareup/ui/ticket/NewTicketView$RowType;

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$HolderType;->TICKET_GROUP_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    const/4 v6, 0x4

    const-string v7, "TICKET_GROUP_ROW"

    invoke-direct {v0, v7, v6, v1}, Lcom/squareup/ui/ticket/NewTicketView$RowType;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/NewTicketView$HolderType;)V

    sput-object v0, Lcom/squareup/ui/ticket/NewTicketView$RowType;->TICKET_GROUP_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

    .line 70
    new-instance v0, Lcom/squareup/ui/ticket/NewTicketView$RowType;

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$HolderType;->TICKET_TEMPLATE_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    const/4 v7, 0x5

    const-string v8, "TICKET_TEMPLATE_ROW"

    invoke-direct {v0, v8, v7, v1}, Lcom/squareup/ui/ticket/NewTicketView$RowType;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/NewTicketView$HolderType;)V

    sput-object v0, Lcom/squareup/ui/ticket/NewTicketView$RowType;->TICKET_TEMPLATE_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/ui/ticket/NewTicketView$RowType;

    .line 64
    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$RowType;->CUSTOM_TICKET_BUTTON_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$RowType;->SELECT_TICKET_GROUP_HEADER_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$RowType;->NO_RESULTS_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$RowType;->NO_TICKETS_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$RowType;->TICKET_GROUP_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$RowType;->TICKET_TEMPLATE_ROW:Lcom/squareup/ui/ticket/NewTicketView$RowType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/ui/ticket/NewTicketView$RowType;->$VALUES:[Lcom/squareup/ui/ticket/NewTicketView$RowType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/ui/ticket/NewTicketView$HolderType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/NewTicketView$HolderType;",
            ")V"
        }
    .end annotation

    .line 74
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 75
    iput-object p3, p0, Lcom/squareup/ui/ticket/NewTicketView$RowType;->holderType:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/ticket/NewTicketView$RowType;
    .locals 1

    .line 64
    const-class v0, Lcom/squareup/ui/ticket/NewTicketView$RowType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/ticket/NewTicketView$RowType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/ticket/NewTicketView$RowType;
    .locals 1

    .line 64
    sget-object v0, Lcom/squareup/ui/ticket/NewTicketView$RowType;->$VALUES:[Lcom/squareup/ui/ticket/NewTicketView$RowType;

    invoke-virtual {v0}, [Lcom/squareup/ui/ticket/NewTicketView$RowType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/ticket/NewTicketView$RowType;

    return-object v0
.end method


# virtual methods
.method public getHolderType()Lcom/squareup/ui/ticket/NewTicketView$HolderType;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketView$RowType;->holderType:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    return-object v0
.end method

.method public bridge synthetic getHolderType()Ljava/lang/Enum;
    .locals 1

    .line 64
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/NewTicketView$RowType;->getHolderType()Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    move-result-object v0

    return-object v0
.end method
