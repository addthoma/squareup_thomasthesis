.class Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;
.super Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$BaseViewHolder;
.source "NewTicketView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TicketGroupViewHolder"
.end annotation


# instance fields
.field private final predefinedTicketRow:Lcom/squareup/noho/NohoRow;

.field final synthetic this$1:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;

.field private ticketGroup:Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;)V
    .locals 1

    .line 181
    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;->this$1:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;

    .line 182
    sget v0, Lcom/squareup/orderentry/R$layout;->predefined_tickets_group_or_template_row:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$BaseViewHolder;-><init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;I)V

    .line 183
    iget-object p2, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/orderentry/R$id;->predefined_ticket_row:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/noho/NohoRow;

    iput-object p2, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;->predefinedTicketRow:Lcom/squareup/noho/NohoRow;

    .line 184
    iget-object p2, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;->predefinedTicketRow:Lcom/squareup/noho/NohoRow;

    sget-object v0, Lcom/squareup/noho/AccessoryType;->DISCLOSURE:Lcom/squareup/noho/AccessoryType;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoRow;->setAccessory(Lcom/squareup/noho/AccessoryType;)V

    .line 185
    iget-object p2, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;->itemView:Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder$1;-><init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/NewTicketView$1;)V
    .locals 0

    .line 175
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;-><init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;
    .locals 0

    .line 175
    iget-object p0, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;->ticketGroup:Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    return-object p0
.end method


# virtual methods
.method protected bindRow(Lcom/squareup/ui/ticket/NewTicketView$RowType;II)V
    .locals 1

    .line 194
    iget-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;->this$1:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;

    invoke-static {p1}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->access$100(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;->ticketGroup:Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    .line 195
    iget-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;->predefinedTicketRow:Lcom/squareup/noho/NohoRow;

    iget-object p2, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;->ticketGroup:Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;->this$1:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;

    iget-object p3, p3, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->this$0:Lcom/squareup/ui/ticket/NewTicketView;

    iget-object p3, p3, Lcom/squareup/ui/ticket/NewTicketView;->presenter:Lcom/squareup/ui/ticket/NewTicketPresenter;

    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;->ticketGroup:Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    .line 196
    invoke-virtual {p3, v0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->getAvailableTemplateCount(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)I

    move-result p3

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p3

    .line 195
    invoke-static {p1, p2, p3}, Lcom/squareup/ui/ticket/NohoRowsKt;->setLabelAndValueForTicketRow(Lcom/squareup/noho/NohoRow;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected bridge synthetic bindRow(Ljava/lang/Enum;II)V
    .locals 0

    .line 175
    check-cast p1, Lcom/squareup/ui/ticket/NewTicketView$RowType;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketGroupViewHolder;->bindRow(Lcom/squareup/ui/ticket/NewTicketView$RowType;II)V

    return-void
.end method
