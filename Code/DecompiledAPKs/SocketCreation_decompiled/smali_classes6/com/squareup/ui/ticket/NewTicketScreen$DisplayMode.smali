.class final enum Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;
.super Ljava/lang/Enum;
.source "NewTicketScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/NewTicketScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "DisplayMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

.field public static final enum SHOW_TICKET_GROUPS:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

.field public static final enum SHOW_TICKET_TEMPLATES_FOR_SELECTED_GROUP:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

.field public static final enum SHOW_TICKET_TEMPLATES_FOR_SOLE_GROUP:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;


# instance fields
.field public final hasCustomTicketButton:Z

.field public final hasSearchBar:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 66
    new-instance v0, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "SHOW_TICKET_GROUPS"

    invoke-direct {v0, v3, v1, v1, v2}, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->SHOW_TICKET_GROUPS:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    .line 74
    new-instance v0, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    const-string v3, "SHOW_TICKET_TEMPLATES_FOR_SELECTED_GROUP"

    invoke-direct {v0, v3, v2, v2, v1}, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->SHOW_TICKET_TEMPLATES_FOR_SELECTED_GROUP:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    .line 79
    new-instance v0, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    const/4 v3, 0x2

    const-string v4, "SHOW_TICKET_TEMPLATES_FOR_SOLE_GROUP"

    invoke-direct {v0, v4, v3, v2, v2}, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->SHOW_TICKET_TEMPLATES_FOR_SOLE_GROUP:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    .line 62
    sget-object v4, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->SHOW_TICKET_GROUPS:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->SHOW_TICKET_TEMPLATES_FOR_SELECTED_GROUP:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->SHOW_TICKET_TEMPLATES_FOR_SOLE_GROUP:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->$VALUES:[Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)V"
        }
    .end annotation

    .line 84
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 85
    iput-boolean p3, p0, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->hasSearchBar:Z

    .line 86
    iput-boolean p4, p0, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->hasCustomTicketButton:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;
    .locals 1

    .line 62
    const-class v0, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;
    .locals 1

    .line 62
    sget-object v0, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->$VALUES:[Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    invoke-virtual {v0}, [Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    return-object v0
.end method
