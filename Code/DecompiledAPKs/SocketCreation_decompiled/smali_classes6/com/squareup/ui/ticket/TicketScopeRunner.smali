.class public Lcom/squareup/ui/ticket/TicketScopeRunner;
.super Ljava/lang/Object;
.source "TicketScopeRunner.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/TicketScopeRunner$PermittedToViewAllTicketsKey;
    }
.end annotation


# instance fields
.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final employeeToken:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/util/Optional<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final permittedToViewAllTickets:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final permittedToViewAllTicketsKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final x2TicketRunner:Lcom/squareup/ui/ticket/X2TicketRunner;


# direct methods
.method constructor <init>(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/BundleKey;Lcom/squareup/ui/ticket/X2TicketRunner;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/ui/ticket/X2TicketRunner;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 44
    iput-object p2, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 45
    iput-object p3, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 46
    iput-object p4, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->permittedToViewAllTicketsKey:Lcom/squareup/BundleKey;

    .line 47
    iput-object p5, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->x2TicketRunner:Lcom/squareup/ui/ticket/X2TicketRunner;

    const/4 p3, 0x0

    const/4 p4, 0x0

    .line 60
    :try_start_0
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketScopeRunner;->permittedToViewAllTickets()Z

    move-result p5
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 61
    :try_start_1
    invoke-interface {p1}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEmployeeFilteringEnabled()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 62
    invoke-interface {p2}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    move-object p3, p1

    goto :goto_1

    :catch_0
    move-exception p1

    goto :goto_0

    :catch_1
    move-exception p1

    const/4 p5, 0x0

    :goto_0
    new-array p2, p4, [Ljava/lang/Object;

    const-string p4, "Couldn\'t get current employee token and/or confirm permission to view all tickets after process death."

    .line 65
    invoke-static {p1, p4, p2}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 68
    :cond_0
    :goto_1
    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->permittedToViewAllTickets:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 70
    invoke-static {p3}, Lcom/squareup/util/Optional;->ofNullable(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->employeeToken:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method static synthetic lambda$onPermittedToViewAllTicketsChanged$0(Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    .line 108
    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private listenForPasscodeEmployeeManagementChanges(Lmortar/MortarScope;)V
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentEmployee()Lio/reactivex/Observable;

    move-result-object v0

    .line 125
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$TicketScopeRunner$9NGPlAh6uEyy0L9hfRRCkE6Gw1c;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketScopeRunner$9NGPlAh6uEyy0L9hfRRCkE6Gw1c;-><init>(Lcom/squareup/ui/ticket/TicketScopeRunner;)V

    .line 126
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 124
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method private permittedToViewAllTickets()Z
    .locals 2

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEmployeeFilteringEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    sget-object v1, Lcom/squareup/permissions/Permission;->OPEN_TICKET_MANAGE_ALL:Lcom/squareup/permissions/Permission;

    .line 152
    invoke-interface {v0, v1}, Lcom/squareup/permissions/EmployeeManagement;->hasPermission(Lcom/squareup/permissions/Permission;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private updatePermittedToViewAllTickets(Lcom/squareup/util/Optional;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->employeeToken:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 147
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->permittedToViewAllTickets:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketScopeRunner;->permittedToViewAllTickets()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public employeeToken()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->employeeToken:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 92
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$listenForPasscodeEmployeeManagementChanges$1$TicketScopeRunner(Lcom/squareup/util/Optional;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 127
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 128
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->employeeToken:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void

    .line 141
    :cond_0
    sget-object v0, Lcom/squareup/ui/ticket/-$$Lambda$3op2ueWGimKaLgQdK41mLZc4aS4;->INSTANCE:Lcom/squareup/ui/ticket/-$$Lambda$3op2ueWGimKaLgQdK41mLZc4aS4;

    invoke-virtual {p1, v0}, Lcom/squareup/util/Optional;->map(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/TicketScopeRunner;->updatePermittedToViewAllTickets(Lcom/squareup/util/Optional;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/TicketScopeRunner;->listenForPasscodeEmployeeManagementChanges(Lmortar/MortarScope;)V

    goto :goto_0

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 83
    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Optional;->ofNullable(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object v0

    .line 82
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/TicketScopeRunner;->updatePermittedToViewAllTickets(Lcom/squareup/util/Optional;)V

    .line 85
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->x2TicketRunner:Lcom/squareup/ui/ticket/X2TicketRunner;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->permittedToViewAllTicketsKey:Lcom/squareup/BundleKey;

    invoke-virtual {v0, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->permittedToViewAllTickets:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onPermittedToViewAllTicketsChanged()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->permittedToViewAllTickets:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/ticket/-$$Lambda$TicketScopeRunner$z5HS29csDaMst6h934rE-YhMgMY;->INSTANCE:Lcom/squareup/ui/ticket/-$$Lambda$TicketScopeRunner$z5HS29csDaMst6h934rE-YhMgMY;

    .line 108
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->permittedToViewAllTicketsKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->permittedToViewAllTickets:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    return-void
.end method

.method public setPermittedToViewAllTickets(Z)V
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketScopeRunner;->permittedToViewAllTickets:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
