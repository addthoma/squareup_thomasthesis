.class public final Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;
.super Ljava/lang/Object;
.source "SplitTicketPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/SplitTicketPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final cartEntryViewModelFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartEntryViewModelFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final coordinatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/SplitTicketCoordinator;",
            ">;"
        }
    .end annotation
.end field

.field private final customerManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final voidCompSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartEntryViewModelFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/SplitTicketCoordinator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;->cartEntryViewModelFactoryProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p2, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;->actionBarProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p3, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p4, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p5, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p6, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;->coordinatorProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p7, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p8, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p9, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p10, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartEntryViewModelFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/SplitTicketCoordinator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;"
        }
    .end annotation

    .line 80
    new-instance v11, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/ui/cart/CartEntryViewModelFactory;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/util/Res;Lcom/squareup/print/PrinterStations;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/ticket/SplitTicketCoordinator;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/crm/CustomerManagementSettings;Lflow/Flow;)Lcom/squareup/ui/ticket/SplitTicketPresenter;
    .locals 12

    .line 88
    new-instance v11, Lcom/squareup/ui/ticket/SplitTicketPresenter;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/ticket/SplitTicketPresenter;-><init>(Lcom/squareup/ui/cart/CartEntryViewModelFactory;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/util/Res;Lcom/squareup/print/PrinterStations;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/ticket/SplitTicketCoordinator;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/crm/CustomerManagementSettings;Lflow/Flow;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/SplitTicketPresenter;
    .locals 11

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;->cartEntryViewModelFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;->actionBarProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/print/PrinterStations;

    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;->coordinatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/crm/CustomerManagementSettings;

    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lflow/Flow;

    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;->newInstance(Lcom/squareup/ui/cart/CartEntryViewModelFactory;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/util/Res;Lcom/squareup/print/PrinterStations;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/ticket/SplitTicketCoordinator;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/crm/CustomerManagementSettings;Lflow/Flow;)Lcom/squareup/ui/ticket/SplitTicketPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/SplitTicketPresenter_Factory;->get()Lcom/squareup/ui/ticket/SplitTicketPresenter;

    move-result-object v0

    return-object v0
.end method
