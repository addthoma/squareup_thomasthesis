.class public final Lcom/squareup/ui/ticket/TicketListView_MembersInjector;
.super Ljava/lang/Object;
.source "TicketListView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/ticket/TicketListView;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider2:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketListView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/ui/ticket/TicketListView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/ui/ticket/TicketListView_MembersInjector;->presenterProvider2:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/ticket/TicketListView;",
            ">;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/ui/ticket/TicketListView_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/ticket/TicketListView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/ui/ticket/TicketListView;Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketListView;->presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/ticket/TicketListView;)V
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/ticket/BaseTicketListView_MembersInjector;->injectPresenter(Lcom/squareup/ui/ticket/BaseTicketListView;Lcom/squareup/ui/ticket/TicketListPresenter;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/ui/ticket/BaseTicketListView_MembersInjector;->injectDevice(Lcom/squareup/ui/ticket/BaseTicketListView;Lcom/squareup/util/Device;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListView_MembersInjector;->presenterProvider2:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/ticket/TicketListView_MembersInjector;->injectPresenter(Lcom/squareup/ui/ticket/TicketListView;Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/ticket/TicketListView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/TicketListView_MembersInjector;->injectMembers(Lcom/squareup/ui/ticket/TicketListView;)V

    return-void
.end method
