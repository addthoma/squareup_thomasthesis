.class Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "TicketTransferEmployeesScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/ticket/TicketTransferEmployeesView;",
        ">;"
    }
.end annotation


# static fields
.field private static final SELECTED_EMPLOYEE_KEY:Ljava/lang/String; = "SELECTED_EMPLOYEE_KEY"


# instance fields
.field private final cardActionBar:Lcom/squareup/marin/widgets/MarinCardActionBar;

.field private employeeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeQueries:Lcom/squareup/permissions/Employees;

.field private final eventSink:Lcom/squareup/badbus/BadEventSink;

.field private filter:Ljava/lang/String;

.field private final flow:Lflow/Flow;

.field private forTransactionTicket:Z

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

.field private final res:Lcom/squareup/util/Res;

.field private selectedEmployeeToken:Ljava/lang/String;

.field private final ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/marin/widgets/MarinCardActionBar;Lcom/squareup/ui/ticket/TicketActionScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/permissions/Employees;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/payment/Transaction;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/ticket/OpenTicketsRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 128
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 129
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->flow:Lflow/Flow;

    .line 130
    iput-object p2, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->cardActionBar:Lcom/squareup/marin/widgets/MarinCardActionBar;

    .line 131
    iput-object p3, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    .line 132
    iput-object p4, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 133
    iput-object p5, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->employeeQueries:Lcom/squareup/permissions/Employees;

    .line 134
    iput-object p6, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 135
    iput-object p7, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 136
    iput-object p8, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    .line 137
    iput-object p9, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->eventSink:Lcom/squareup/badbus/BadEventSink;

    .line 138
    iput-object p10, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 139
    iput-object p11, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    return-void
.end method

.method static synthetic access$102(Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->employeeList:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$200(Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;)Ljava/lang/Object;
    .locals 0

    .line 103
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;)Ljava/lang/String;
    .locals 0

    .line 103
    iget-object p0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->filter:Ljava/lang/String;

    return-object p0
.end method

.method static filterEmployees(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;",
            ">;"
        }
    .end annotation

    .line 210
    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p1

    .line 213
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    .line 215
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 216
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;

    .line 217
    iget-object v2, v1, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;->searchableDisplayName:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 218
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method private getActionBarTitle()Ljava/lang/String;
    .locals 3

    .line 288
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->getSelectedCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 289
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_transfer_tickets_one:I

    .line 290
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderentry/R$string;->open_tickets_transfer_tickets_many:I

    .line 291
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "number"

    .line 292
    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 293
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 294
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private getToastText(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 277
    iget-object p2, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/orderentry/R$string;->open_tickets_transferred_one:I

    .line 278
    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_transferred_many:I

    .line 279
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "number"

    .line 280
    invoke-virtual {v0, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 282
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "employee_name"

    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 283
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 284
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private showMasterDetailTicketScreen()Z
    .locals 1

    .line 298
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 299
    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method


# virtual methods
.method isSelected(Ljava/lang/String;)Z
    .locals 1

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->selectedEmployeeToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public synthetic lambda$null$0$TicketTransferEmployeesScreen$Presenter(Ljava/util/Set;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 175
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 176
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/permissions/Employee;

    .line 177
    new-instance v2, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;

    .line 178
    invoke-static {v1}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeInfo(Lcom/squareup/permissions/Employee;)Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v1

    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-direct {v2, v1, v3}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;-><init>(Lcom/squareup/permissions/EmployeeInfo;Lcom/squareup/util/Res;)V

    .line 177
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 180
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    return-object v0
.end method

.method public synthetic lambda$onLoad$1$TicketTransferEmployeesScreen$Presenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->employeeQueries:Lcom/squareup/permissions/Employees;

    .line 173
    invoke-virtual {v0}, Lcom/squareup/permissions/Employees;->activeEmployees()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$TicketTransferEmployeesScreen$Presenter$PKSwAUmmrZB3en5F7F8nomwjJFw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketTransferEmployeesScreen$Presenter$PKSwAUmmrZB3en5F7F8nomwjJFw;-><init>(Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;)V

    .line 174
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter$1;-><init>(Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;)V

    .line 183
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeWith(Lio/reactivex/Observer;)Lio/reactivex/Observer;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter$1;

    return-object v0
.end method

.method onBackPressed()Z
    .locals 1

    .line 242
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    move-result v0

    return v0
.end method

.method onEmployeeClicked(Ljava/lang/String;)V
    .locals 1

    .line 236
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->selectedEmployeeToken:Ljava/lang/String;

    .line 237
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->cardActionBar:Lcom/squareup/marin/widgets/MarinCardActionBar;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->selectedEmployeeToken:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinCardActionBar;->setPrimaryButtonEnabled(Z)V

    .line 238
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->refreshList()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 143
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;

    .line 144
    invoke-static {p1}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;->access$000(Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->forTransactionTicket:Z

    .line 145
    iget-boolean p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->forTransactionTicket:Z

    if-eqz p1, :cond_0

    .line 146
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketSelection;->ticketInfoFromTransaction(Lcom/squareup/payment/Transaction;)Lcom/squareup/ui/ticket/TicketInfo;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->addSelectedTicket(Lcom/squareup/ui/ticket/TicketInfo;)V

    :cond_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 5

    .line 151
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->getSelectedCount()I

    move-result v0

    const/4 v1, 0x1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v2, "Must have at least one ticket to transfer!"

    invoke-static {v0, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 155
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;

    if-eqz p1, :cond_1

    const-string v2, "SELECTED_EMPLOYEE_KEY"

    .line 158
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->selectedEmployeeToken:Ljava/lang/String;

    .line 161
    :cond_1
    iget-boolean p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->forTransactionTicket:Z

    if-nez p1, :cond_3

    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->showMasterDetailTicketScreen()Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_1

    :cond_2
    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_2

    :cond_3
    :goto_1
    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 164
    :goto_2
    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->cardActionBar:Lcom/squareup/marin/widgets/MarinCardActionBar;

    new-instance v3, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 165
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->getActionBarTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance v3, Lcom/squareup/ui/ticket/-$$Lambda$kX9qv-QO7p6HeX45FHwyD69PG0s;

    invoke-direct {v3, p0}, Lcom/squareup/ui/ticket/-$$Lambda$kX9qv-QO7p6HeX45FHwyD69PG0s;-><init>(Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;)V

    .line 166
    invoke-virtual {p1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/orderentry/R$string;->open_tickets_transfer:I

    .line 167
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->selectedEmployeeToken:Ljava/lang/String;

    .line 168
    invoke-static {v3}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    xor-int/2addr v1, v3

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$gYZzfnd-MBL7eNEqbF1hUWESCNc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$gYZzfnd-MBL7eNEqbF1hUWESCNc;-><init>(Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;)V

    .line 169
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 170
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    .line 164
    invoke-virtual {v2, p1}, Lcom/squareup/marin/widgets/MarinCardActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 172
    new-instance p1, Lcom/squareup/ui/ticket/-$$Lambda$TicketTransferEmployeesScreen$Presenter$fsutebSqfUeyRLC1ZhfOw0oLe9o;

    invoke-direct {p1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketTransferEmployeesScreen$Presenter$fsutebSqfUeyRLC1ZhfOw0oLe9o;-><init>(Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;)V

    invoke-static {v0, p1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 204
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->selectedEmployeeToken:Ljava/lang/String;

    const-string v1, "SELECTED_EMPLOYEE_KEY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method onTextSearched(Ljava/lang/String;)V
    .locals 2

    .line 229
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->filter:Ljava/lang/String;

    .line 230
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;

    .line 231
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->employeeList:Ljava/util/List;

    invoke-static {p1, v1}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->filterEmployees(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->updateEmployees(Ljava/util/List;)V

    .line 232
    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->scrollListToTop()V

    return-void
.end method

.method onTransferClicked()V
    .locals 6

    .line 247
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->employeeList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;

    .line 248
    iget-object v3, v1, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;->info:Lcom/squareup/permissions/EmployeeInfo;

    iget-object v3, v3, Lcom/squareup/permissions/EmployeeInfo;->employeeToken:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->selectedEmployeeToken:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_1
    move-object v1, v2

    :goto_0
    const/4 v0, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :goto_1
    new-array v0, v0, [Ljava/lang/Object;

    .line 253
    iget-object v5, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->selectedEmployeeToken:Ljava/lang/String;

    aput-object v5, v0, v3

    const-string v3, "Could not find an employee with id %s!"

    invoke-static {v4, v3, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 258
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->forTransactionTicket:Z

    if-eqz v0, :cond_3

    .line 261
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v3, v1, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;->info:Lcom/squareup/permissions/EmployeeInfo;

    invoke-virtual {v0, v3}, Lcom/squareup/payment/Transaction;->setEmployee(Lcom/squareup/permissions/EmployeeInfo;)V

    .line 262
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 263
    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->getDisplayNameOrDefault()Ljava/lang/String;

    move-result-object v4

    .line 262
    invoke-virtual {v0, v3, v4}, Lcom/squareup/print/OrderPrintingDispatcher;->printStubAndTicket(Lcom/squareup/payment/Order;Ljava/lang/String;)V

    .line 264
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->updateCurrentTicketBeforeReset()V

    .line 265
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->eventSink:Lcom/squareup/badbus/BadEventSink;

    new-instance v3, Lcom/squareup/payment/OrderEntryEvents$TicketSaved;

    invoke-direct {v3}, Lcom/squareup/payment/OrderEntryEvents$TicketSaved;-><init>()V

    invoke-interface {v0, v3}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    .line 266
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->reset()V

    .line 269
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->getSelectedCount()I

    move-result v0

    .line 270
    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    iget-object v4, v1, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;->info:Lcom/squareup/permissions/EmployeeInfo;

    invoke-virtual {v3, v4}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->transferSelectedTicketsTo(Lcom/squareup/permissions/EmployeeInfo;)V

    .line 271
    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v1, v1, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;->displayName:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->getToastText(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v4, v0, v2}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 273
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->finishTicketTransfer(Lflow/Flow;)V

    return-void
.end method
