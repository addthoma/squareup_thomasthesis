.class public Lcom/squareup/ui/ticket/GroupListView;
.super Landroid/widget/LinearLayout;
.source "GroupListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/GroupListView$SmoothScrollLayoutManager;,
        Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;,
        Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;,
        Lcom/squareup/ui/ticket/GroupListView$GroupRow;
    }
.end annotation


# instance fields
.field private blockingPopup:Lcom/squareup/caller/BlockingPopup;

.field private container:Landroid/widget/LinearLayout;

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final fadeDuration:I

.field private final fadeDurationShort:I

.field private loadingGroupPopup:Lcom/squareup/caller/ProgressPopup;

.field private logOutButton:Lcom/squareup/marketfont/MarketButton;

.field private logOutButtonDivider:Landroid/view/View;

.field private logOutButtonPadder:Landroid/view/View;

.field presenter:Lcom/squareup/ui/ticket/GroupListPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

.field private recyclerAdapter:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private shouldShowLogOutButton:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .line 339
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 341
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const/high16 v0, 0x10e0000

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/ticket/GroupListView;->fadeDuration:I

    .line 342
    iget p2, p0, Lcom/squareup/ui/ticket/GroupListView;->fadeDuration:I

    int-to-double v0, p2

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    div-double/2addr v0, v2

    double-to-int p2, v0

    iput p2, p0, Lcom/squareup/ui/ticket/GroupListView;->fadeDurationShort:I

    .line 343
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListView;->isInEditMode()Z

    move-result p2

    if-eqz p2, :cond_0

    return-void

    .line 344
    :cond_0
    new-instance p2, Lcom/squareup/caller/ProgressPopup;

    invoke-direct {p2, p1}, Lcom/squareup/caller/ProgressPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/ticket/GroupListView;->loadingGroupPopup:Lcom/squareup/caller/ProgressPopup;

    .line 345
    new-instance p2, Lcom/squareup/caller/BlockingPopup;

    invoke-direct {p2, p1}, Lcom/squareup/caller/BlockingPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/ticket/GroupListView;->blockingPopup:Lcom/squareup/caller/BlockingPopup;

    .line 346
    const-class p2, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$Component;->inject(Lcom/squareup/ui/ticket/GroupListView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 444
    sget v0, Lcom/squareup/orderentry/R$id;->master_group_list_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->container:Landroid/widget/LinearLayout;

    .line 445
    sget v0, Lcom/squareup/orderentry/R$id;->master_group_list_view_recycler_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 446
    sget v0, Lcom/squareup/orderentry/R$id;->master_group_list_view_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/DelayedLoadingProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    .line 447
    sget v0, Lcom/squareup/orderentry/R$id;->master_group_list_view_log_out_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->logOutButton:Lcom/squareup/marketfont/MarketButton;

    .line 448
    sget v0, Lcom/squareup/orderentry/R$id;->master_group_list_view_log_out_button_padder:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->logOutButtonPadder:Landroid/view/View;

    .line 449
    sget v0, Lcom/squareup/orderentry/R$id;->master_group_list_view_log_out_button_divider:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->logOutButtonDivider:Landroid/view/View;

    return-void
.end method

.method private isSelectedSectionVisible()Z
    .locals 4

    .line 505
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    .line 507
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 509
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/marin/R$dimen;->marin_divider_width_1dp:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    .line 511
    div-int/2addr v0, v1

    .line 513
    iget-boolean v1, p0, Lcom/squareup/ui/ticket/GroupListView;->shouldShowLogOutButton:Z

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, -0x1

    .line 517
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/ticket/GroupListView;->recyclerAdapter:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;

    invoke-static {v1}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->access$400(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;)I

    move-result v1

    if-ge v1, v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private logOutButtonCoversSection(II)Z
    .locals 5

    .line 522
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 523
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    move-result v1

    .line 524
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    move-result v2

    const/4 v3, 0x0

    if-lt p2, v1, :cond_1

    if-ge v2, p2, :cond_0

    goto :goto_0

    .line 531
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/ticket/GroupListView;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v1

    .line 533
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v4, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v2, p1

    .line 535
    invoke-virtual {v0, p2}, Landroidx/recyclerview/widget/LinearLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object p1

    .line 536
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result p1

    sub-int/2addr v1, p1

    if-ge v1, v2, :cond_1

    const/4 v3, 0x1

    :cond_1
    :goto_0
    return v3
.end method

.method private resizeLogOutButtonPadder(I)I
    .locals 6

    .line 458
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/GroupListView;->shouldShowLogOutButton:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 466
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    .line 468
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/lit8 p1, p1, 0x4

    mul-int v3, v2, p1

    .line 475
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/squareup/marin/R$dimen;->marin_divider_width_1dp:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const/4 v5, 0x1

    sub-int/2addr p1, v5

    mul-int p1, p1, v4

    add-int/2addr v3, p1

    const/4 p1, -0x1

    if-gt v3, v0, :cond_1

    .line 482
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, p1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 483
    iget-object p1, p0, Lcom/squareup/ui/ticket/GroupListView;->logOutButtonPadder:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 484
    iget-object p1, p0, Lcom/squareup/ui/ticket/GroupListView;->logOutButtonDivider:Landroid/view/View;

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return v1

    :cond_1
    add-int v1, v2, v4

    .line 488
    div-int v1, v0, v1

    mul-int v2, v2, v1

    add-int/lit8 v1, v1, -0x2

    mul-int v1, v1, v4

    add-int/2addr v2, v1

    sub-int/2addr v0, v2

    .line 498
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, p1, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 499
    iget-object p1, p0, Lcom/squareup/ui/ticket/GroupListView;->logOutButtonPadder:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 500
    iget-object p1, p0, Lcom/squareup/ui/ticket/GroupListView;->logOutButtonDivider:Landroid/view/View;

    invoke-static {p1, v5}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return v0
.end method


# virtual methods
.method ensureSelectedSectionIsVisible()V
    .locals 2

    .line 438
    invoke-direct {p0}, Lcom/squareup/ui/ticket/GroupListView;->isSelectedSectionVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/ticket/GroupListView;->recyclerAdapter:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;

    invoke-static {v1}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->access$400(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    :cond_0
    return-void
.end method

.method fadeInLogOutButton()V
    .locals 3

    const/4 v0, 0x1

    .line 421
    iput-boolean v0, p0, Lcom/squareup/ui/ticket/GroupListView;->shouldShowLogOutButton:Z

    .line 422
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->logOutButton:Lcom/squareup/marketfont/MarketButton;

    iget-object v1, p0, Lcom/squareup/ui/ticket/GroupListView;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/GroupListPresenter;->getLogOutButtonText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 423
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->logOutButton:Lcom/squareup/marketfont/MarketButton;

    iget v1, p0, Lcom/squareup/ui/ticket/GroupListView;->fadeDuration:I

    invoke-static {v0, v1, v1}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;II)V

    .line 425
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/GroupListPresenter;->getGroupCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/GroupListView;->resizeLogOutButtonPadder(I)I

    move-result v0

    .line 426
    iget-object v1, p0, Lcom/squareup/ui/ticket/GroupListView;->recyclerAdapter:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;

    invoke-static {v1}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->access$400(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;)I

    move-result v1

    .line 428
    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/ticket/GroupListView;->logOutButtonCoversSection(II)Z

    move-result v0

    .line 430
    new-instance v2, Lcom/squareup/ui/ticket/-$$Lambda$GroupListView$tesyLxp0Ux8aEBnsg8hbtBZC7x4;

    invoke-direct {v2, p0, v0, v1}, Lcom/squareup/ui/ticket/-$$Lambda$GroupListView$tesyLxp0Ux8aEBnsg8hbtBZC7x4;-><init>(Lcom/squareup/ui/ticket/GroupListView;ZI)V

    iget v0, p0, Lcom/squareup/ui/ticket/GroupListView;->fadeDuration:I

    int-to-long v0, v0

    invoke-virtual {p0, v2, v0, v1}, Lcom/squareup/ui/ticket/GroupListView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method getSelectedGroup()Lcom/squareup/api/items/TicketGroup;
    .locals 1

    .line 407
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/GroupListPresenter;->getSelectedGroup()Lcom/squareup/api/items/TicketGroup;

    move-result-object v0

    return-object v0
.end method

.method getSelectedSection()Ljava/lang/String;
    .locals 1

    .line 403
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/GroupListPresenter;->getSelectedSection()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method hideSpinner()V
    .locals 1

    .line 411
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    invoke-virtual {v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->hide()V

    return-void
.end method

.method public synthetic lambda$fadeInLogOutButton$1$GroupListView(ZI)V
    .locals 0

    if-eqz p1, :cond_0

    .line 431
    iget-object p1, p0, Lcom/squareup/ui/ticket/GroupListView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz p1, :cond_0

    .line 432
    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->smoothScrollToPosition(I)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$GroupListView()V
    .locals 1

    .line 370
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/GroupListPresenter;->onProgressHidden()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 375
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 376
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/GroupListPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/GroupListView;->blockingPopup:Lcom/squareup/caller/BlockingPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/caller/BlockingPopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 378
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/GroupListPresenter;->loadingGroupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/GroupListView;->loadingGroupPopup:Lcom/squareup/caller/ProgressPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 379
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ticket/GroupListPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 383
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ticket/GroupListPresenter;->dropView(Ljava/lang/Object;)V

    .line 384
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/GroupListPresenter;->loadingGroupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/GroupListView;->loadingGroupPopup:Lcom/squareup/caller/ProgressPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 385
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/GroupListPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/GroupListView;->blockingPopup:Lcom/squareup/caller/BlockingPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/caller/BlockingPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 386
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 350
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 351
    invoke-direct {p0}, Lcom/squareup/ui/ticket/GroupListView;->bindViews()V

    .line 353
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->logOutButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/ui/ticket/GroupListView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/GroupListView$1;-><init>(Lcom/squareup/ui/ticket/GroupListView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 359
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Lcom/squareup/ui/ticket/GroupListView$SmoothScrollLayoutManager;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/ticket/GroupListView$SmoothScrollLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 362
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$dimen;->marin_divider_width_1dp:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 363
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$color;->marin_light_gray:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 364
    new-instance v2, Lcom/squareup/ui/DividerDecoration;

    invoke-direct {v2, v0, v1}, Lcom/squareup/ui/DividerDecoration;-><init>(II)V

    .line 365
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 367
    new-instance v0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/GroupListView;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    iget-object v2, p0, Lcom/squareup/ui/ticket/GroupListView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;-><init>(Lcom/squareup/ui/ticket/GroupListPresenter;Landroidx/recyclerview/widget/RecyclerView;)V

    iput-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->recyclerAdapter:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;

    .line 368
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/ticket/GroupListView;->recyclerAdapter:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 370
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$GroupListView$USRNeZFDPztE0aNXRUIOZhJ2IVc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$GroupListView$USRNeZFDPztE0aNXRUIOZhJ2IVc;-><init>(Lcom/squareup/ui/ticket/GroupListView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/DelayedLoadingProgressBar;->setCallback(Lcom/squareup/ui/DelayedLoadingProgressBar$DelayedLoadingProgressBarCallback;)V

    .line 371
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    invoke-virtual {v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->show()V

    return-void
.end method

.method setList(Lcom/squareup/ui/Ranger;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/Ranger<",
            "Lcom/squareup/ui/ticket/GroupListView$GroupRow;",
            "Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;)V"
        }
    .end annotation

    .line 390
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->recyclerAdapter:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;

    invoke-static {v0, p1, p2}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->access$300(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;Lcom/squareup/ui/Ranger;Ljava/util/List;)V

    .line 391
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/GroupListView;->resizeLogOutButtonPadder(I)I

    return-void
.end method

.method setLogOutButtonVisible(Z)V
    .locals 2

    .line 415
    iput-boolean p1, p0, Lcom/squareup/ui/ticket/GroupListView;->shouldShowLogOutButton:Z

    .line 416
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->logOutButton:Lcom/squareup/marketfont/MarketButton;

    iget-object v1, p0, Lcom/squareup/ui/ticket/GroupListView;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/GroupListPresenter;->getLogOutButtonText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 417
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView;->logOutButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showList(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 396
    iget-object p1, p0, Lcom/squareup/ui/ticket/GroupListView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget v0, p0, Lcom/squareup/ui/ticket/GroupListView;->fadeDurationShort:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 398
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/ticket/GroupListView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    :goto_0
    return-void
.end method
