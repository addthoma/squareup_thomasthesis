.class final Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;
.super Ljava/lang/Object;
.source "EditTicketScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/EditTicketScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "EditTicketController"
.end annotation


# instance fields
.field protected final analytics:Lcom/squareup/analytics/Analytics;

.field protected final device:Lcom/squareup/util/Device;

.field protected final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field protected final eventSink:Lcom/squareup/badbus/BadEventSink;

.field protected final openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

.field protected final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field protected final orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

.field private final ticketCreatedListener:Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;

.field protected final tickets:Lcom/squareup/tickets/Tickets;

.field protected final ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

.field protected final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/tickets/Tickets;Lcom/squareup/payment/Transaction;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Device;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/ticket/OpenTicketsRunner;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->analytics:Lcom/squareup/analytics/Analytics;

    .line 257
    iput-object p2, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->tickets:Lcom/squareup/tickets/Tickets;

    .line 258
    iput-object p3, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->transaction:Lcom/squareup/payment/Transaction;

    .line 259
    iput-object p4, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 260
    iput-object p5, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->device:Lcom/squareup/util/Device;

    .line 261
    iput-object p6, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    .line 262
    iput-object p7, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    .line 263
    iput-object p8, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 264
    iput-object p9, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    .line 265
    iput-object p10, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->eventSink:Lcom/squareup/badbus/BadEventSink;

    .line 266
    iput-object p11, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->ticketCreatedListener:Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;

    return-void
.end method

.method private logChanges(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 349
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOpenTicketName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    .line 350
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOpenTicketNote()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 352
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    sget-object p2, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;->BOTH:Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

    invoke-virtual {p1, p2}, Lcom/squareup/log/tickets/OpenTicketsLogger;->logExitEditTicket(Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;)V

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    .line 354
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    sget-object p2, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;->NAME:Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

    invoke-virtual {p1, p2}, Lcom/squareup/log/tickets/OpenTicketsLogger;->logExitEditTicket(Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;)V

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    .line 356
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    sget-object p2, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;->NOTE:Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

    invoke-virtual {p1, p2}, Lcom/squareup/log/tickets/OpenTicketsLogger;->logExitEditTicket(Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;)V

    goto :goto_0

    .line 360
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    sget-object p2, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;->CANCEL:Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

    invoke-virtual {p1, p2}, Lcom/squareup/log/tickets/OpenTicketsLogger;->logExitEditTicket(Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;)V

    :goto_0
    return-void
.end method

.method private updateTransaction(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;Z)V
    .locals 2

    if-nez p3, :cond_0

    .line 367
    iget-object p3, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->transaction:Lcom/squareup/payment/Transaction;

    const/4 p4, 0x0

    invoke-virtual {p3, p4}, Lcom/squareup/payment/Transaction;->setPredefinedTicket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V

    goto :goto_0

    .line 369
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->transaction:Lcom/squareup/payment/Transaction;

    new-instance v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;-><init>()V

    .line 370
    invoke-virtual {v1, p3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;->ticket_group(Lcom/squareup/api/items/TicketGroup;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;

    move-result-object p3

    .line 371
    invoke-virtual {p3, p4}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;->ticket_template(Lcom/squareup/api/items/TicketTemplate;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;

    move-result-object p3

    .line 372
    invoke-virtual {p3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object p3

    .line 369
    invoke-virtual {v0, p3}, Lcom/squareup/payment/Transaction;->setPredefinedTicket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V

    .line 375
    :goto_0
    iget-object p3, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p3, p1, p2}, Lcom/squareup/payment/Transaction;->setOpenTicketNameAndNote(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p5, :cond_1

    .line 377
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->transaction:Lcom/squareup/payment/Transaction;

    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {p2}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeInfo()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/payment/Transaction;->setEmployee(Lcom/squareup/permissions/EmployeeInfo;)V

    :cond_1
    return-void
.end method


# virtual methods
.method createNewEmptyTicketAndExit(Lflow/Flow;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;)V
    .locals 9

    .line 271
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_NEW_TICKET_WITHOUT_EXISTING_CART:Lcom/squareup/analytics/RegisterActionName;

    invoke-virtual {v0, v1}, Lcom/squareup/log/tickets/OpenTicketsLogger;->logTicketAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 272
    iget-object v2, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 273
    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeInfo()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v7

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    .line 272
    invoke-interface/range {v2 .. v8}, Lcom/squareup/tickets/Tickets;->addEmptyTicketAndLock(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;Lcom/squareup/permissions/EmployeeInfo;Ljava/util/Date;)Lcom/squareup/tickets/OpenTicket;

    move-result-object p2

    .line 274
    iget-object p3, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p3, p2}, Lcom/squareup/payment/Transaction;->setTicket(Lcom/squareup/tickets/OpenTicket;)V

    .line 275
    iget-object p3, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->ticketCreatedListener:Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;

    invoke-static {p3}, Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;->access$000(Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;)V

    .line 276
    iget-object p3, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance p4, Lcom/squareup/log/tickets/OpenTicketSaved;

    invoke-direct {p4, p2}, Lcom/squareup/log/tickets/OpenTicketSaved;-><init>(Lcom/squareup/tickets/OpenTicket;)V

    invoke-interface {p3, p4}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 277
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->finishCreateNewEmptyTicketAndExit(Lflow/Flow;)V

    return-void
.end method

.method editExistingTicketAndExit(Lflow/Flow;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;)V
    .locals 8

    .line 290
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->tickets:Lcom/squareup/tickets/Tickets;

    new-instance v7, Lcom/squareup/ui/ticket/-$$Lambda$EditTicketScreen$EditTicketController$kDDfrL2sBAxMJiBe3nChaPCWMFA;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/ticket/-$$Lambda$EditTicketScreen$EditTicketController$kDDfrL2sBAxMJiBe3nChaPCWMFA;-><init>(Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;)V

    invoke-interface {v0, p2, v7}, Lcom/squareup/tickets/Tickets;->getTicketAndLock(Ljava/lang/String;Lcom/squareup/tickets/TicketsCallback;)V

    .line 295
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->finishEditExistingTicketAndExit(Lflow/Flow;)V

    return-void
.end method

.method editTransactionTicketAndExit(Lflow/Flow;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;)V
    .locals 6

    .line 282
    invoke-direct {p0, p2, p3}, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->logChanges(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    .line 283
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->updateTransaction(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;Z)V

    .line 284
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->eventSink:Lcom/squareup/badbus/BadEventSink;

    new-instance p3, Lcom/squareup/tickets/TicketEdited;

    invoke-direct {p3}, Lcom/squareup/tickets/TicketEdited;-><init>()V

    invoke-interface {p2, p3}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    .line 285
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->finishEditTransactionTicketAndExit(Lflow/Flow;)V

    return-void
.end method

.method public synthetic lambda$editExistingTicketAndExit$0$EditTicketScreen$EditTicketController(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;Lcom/squareup/tickets/TicketsResult;)V
    .locals 0

    .line 291
    invoke-interface {p5}, Lcom/squareup/tickets/TicketsResult;->get()Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Lcom/squareup/tickets/OpenTicket;

    .line 292
    invoke-virtual {p5, p1, p2, p3, p4}, Lcom/squareup/tickets/OpenTicket;->updateFeatureDetails(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;)V

    .line 293
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->tickets:Lcom/squareup/tickets/Tickets;

    invoke-interface {p1, p5}, Lcom/squareup/tickets/Tickets;->updateTicketAndUnlock(Lcom/squareup/tickets/OpenTicket;)V

    return-void
.end method

.method saveForSplitTicket(Lflow/Flow;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;)V
    .locals 8

    .line 305
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_NEW_TICKET_WITH_EXISTING_CART:Lcom/squareup/analytics/RegisterActionName;

    invoke-virtual {v0, v1}, Lcom/squareup/log/tickets/OpenTicketsLogger;->logTicketAction(Lcom/squareup/analytics/RegisterActionName;)V

    const/4 v7, 0x1

    move-object v2, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    .line 306
    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->updateTransaction(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;Z)V

    .line 307
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v1, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 309
    invoke-interface {v1}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeInfo()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v5

    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    .line 308
    invoke-interface/range {v0 .. v6}, Lcom/squareup/tickets/Tickets;->addEmptyTicketAndLock(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;Lcom/squareup/permissions/EmployeeInfo;Ljava/util/Date;)Lcom/squareup/tickets/OpenTicket;

    move-result-object p2

    .line 311
    iget-object p3, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p3}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object p3

    invoke-static {p3}, Lcom/squareup/payment/Order$Builder;->fromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order$Builder;

    move-result-object p3

    .line 312
    invoke-virtual {p2}, Lcom/squareup/tickets/OpenTicket;->getIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object p4

    invoke-virtual {p3, p4}, Lcom/squareup/payment/Order$Builder;->ticketIdPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/payment/Order$Builder;

    move-result-object p3

    .line 313
    invoke-virtual {p3}, Lcom/squareup/payment/Order$Builder;->build()Lcom/squareup/payment/Order;

    move-result-object p3

    .line 314
    iget-object p4, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p4, p2, p3}, Lcom/squareup/payment/Transaction;->setTicketFromSplit(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/payment/Order;)V

    .line 315
    iget-object p3, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->ticketCreatedListener:Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;

    invoke-static {p3}, Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;->access$000(Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;)V

    .line 316
    iget-object p3, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance p4, Lcom/squareup/log/tickets/OpenTicketSaved;

    invoke-direct {p4, p2}, Lcom/squareup/log/tickets/OpenTicketSaved;-><init>(Lcom/squareup/tickets/OpenTicket;)V

    invoke-interface {p3, p4}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 317
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->finishSaveForSplitTicket(Lflow/Flow;)V

    return-void
.end method

.method saveTransactionToNewTicket(Lflow/Flow;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;Z)V
    .locals 8

    .line 322
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_NEW_TICKET_WITH_EXISTING_CART:Lcom/squareup/analytics/RegisterActionName;

    invoke-virtual {v0, v1}, Lcom/squareup/log/tickets/OpenTicketsLogger;->logTicketAction(Lcom/squareup/analytics/RegisterActionName;)V

    const/4 v7, 0x1

    move-object v2, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    .line 323
    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->updateTransaction(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;Z)V

    .line 324
    iget-object p3, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p3}, Lcom/squareup/payment/Transaction;->attributeChargeIfPossible()V

    if-eqz p6, :cond_0

    .line 327
    iget-object p3, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object p4, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p4}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object p4

    invoke-virtual {p3, p4, p2}, Lcom/squareup/print/OrderPrintingDispatcher;->printBillStubAndTicket(Lcom/squareup/payment/Order;Ljava/lang/String;)V

    goto :goto_0

    .line 330
    :cond_0
    iget-object p3, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object p4, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p4}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object p4

    invoke-virtual {p3, p4, p2}, Lcom/squareup/print/OrderPrintingDispatcher;->printStubAndTicket(Lcom/squareup/payment/Order;Ljava/lang/String;)V

    .line 334
    :goto_0
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->tickets:Lcom/squareup/tickets/Tickets;

    new-instance p3, Ljava/util/Date;

    invoke-direct {p3}, Ljava/util/Date;-><init>()V

    iget-object p4, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p4}, Lcom/squareup/payment/Transaction;->buildCartProtoForTicket()Lcom/squareup/protos/client/bills/Cart;

    move-result-object p4

    invoke-interface {p2, p3, p4}, Lcom/squareup/tickets/Tickets;->addTicket(Ljava/util/Date;Lcom/squareup/protos/client/bills/Cart;)V

    .line 335
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance p3, Lcom/squareup/log/tickets/OpenTicketSaved;

    iget-object p4, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p4}, Lcom/squareup/payment/Transaction;->getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object p4

    invoke-direct {p3, p4}, Lcom/squareup/log/tickets/OpenTicketSaved;-><init>(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V

    invoke-interface {p2, p3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 337
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->device:Lcom/squareup/util/Device;

    invoke-interface {p2}, Lcom/squareup/util/Device;->isPhone()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 338
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {p2}, Lcom/squareup/orderentry/OrderEntryScreenState;->setPendingTicketSavedAlert()V

    goto :goto_1

    .line 340
    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->eventSink:Lcom/squareup/badbus/BadEventSink;

    new-instance p3, Lcom/squareup/payment/OrderEntryEvents$TicketSaved;

    invoke-direct {p3}, Lcom/squareup/payment/OrderEntryEvents$TicketSaved;-><init>()V

    invoke-interface {p2, p3}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    .line 342
    :goto_1
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->reset()V

    .line 344
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->ticketCreatedListener:Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;

    invoke-static {p2}, Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;->access$000(Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;)V

    .line 345
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    iget-object p3, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->device:Lcom/squareup/util/Device;

    invoke-interface {p3}, Lcom/squareup/util/Device;->isPhone()Z

    move-result p3

    invoke-virtual {p2, p1, p3, p6}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->finishSaveTransactionToNewTicket(Lflow/Flow;ZZ)V

    return-void
.end method
