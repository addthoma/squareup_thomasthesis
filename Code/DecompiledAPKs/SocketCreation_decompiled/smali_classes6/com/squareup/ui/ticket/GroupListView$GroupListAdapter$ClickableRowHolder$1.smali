.class Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "GroupListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;->bindRow(Lcom/squareup/ui/ticket/GroupListView$GroupRow;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;

.field final synthetic val$adapterIndex:I


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;I)V
    .locals 0

    .line 93
    iput-object p1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder$1;->this$1:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;

    iput p2, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder$1;->val$adapterIndex:I

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder$1;->this$1:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;

    invoke-static {v0}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;->access$000(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;)Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;

    move-result-object v0

    iget v1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder$1;->val$adapterIndex:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->setSelectedSectionPosition(I)V

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder$1;->this$1:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;->onRowClicked(Landroid/view/View;)V

    return-void
.end method
