.class public Lcom/squareup/ui/photo/ItemPhoto$Factory$ImageUriUpdated;
.super Ljava/lang/Object;
.source "ItemPhoto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/photo/ItemPhoto$Factory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ImageUriUpdated"
.end annotation


# instance fields
.field private final itemId:Ljava/lang/String;

.field private final uri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    iput-object p1, p0, Lcom/squareup/ui/photo/ItemPhoto$Factory$ImageUriUpdated;->itemId:Ljava/lang/String;

    .line 118
    iput-object p2, p0, Lcom/squareup/ui/photo/ItemPhoto$Factory$ImageUriUpdated;->uri:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getItemId()Ljava/lang/String;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/photo/ItemPhoto$Factory$ImageUriUpdated;->itemId:Ljava/lang/String;

    return-object v0
.end method

.method public getUri()Ljava/lang/String;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/photo/ItemPhoto$Factory$ImageUriUpdated;->uri:Ljava/lang/String;

    return-object v0
.end method
