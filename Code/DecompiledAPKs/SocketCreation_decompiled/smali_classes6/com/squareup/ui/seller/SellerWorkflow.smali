.class abstract Lcom/squareup/ui/seller/SellerWorkflow;
.super Ljava/lang/Object;
.source "SellerWorkflow.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

.field protected final hudToaster:Lcom/squareup/payment/PaymentHudToaster;

.field private final offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

.field protected final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field protected final tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

.field protected final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

.field protected final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/TipDeterminerFactory;)V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/ui/seller/SellerWorkflow;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    .line 46
    iput-object p2, p0, Lcom/squareup/ui/seller/SellerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    .line 47
    iput-object p3, p0, Lcom/squareup/ui/seller/SellerWorkflow;->hudToaster:Lcom/squareup/payment/PaymentHudToaster;

    .line 48
    iput-object p4, p0, Lcom/squareup/ui/seller/SellerWorkflow;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    .line 49
    iput-object p5, p0, Lcom/squareup/ui/seller/SellerWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 50
    iput-object p6, p0, Lcom/squareup/ui/seller/SellerWorkflow;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    .line 51
    iput-object p7, p0, Lcom/squareup/ui/seller/SellerWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    .line 52
    iput-object p8, p0, Lcom/squareup/ui/seller/SellerWorkflow;->tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

    return-void
.end method

.method private isOfflineLimitExceeded(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerWorkflow;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {v0}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/seller/SellerWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 141
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStoreAndForwardSettings()Lcom/squareup/settings/server/StoreAndForwardSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/StoreAndForwardSettings;->isStoreAndForwardEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    invoke-virtual {p0, p1}, Lcom/squareup/ui/seller/SellerWorkflow;->canProcessInstrumentOffline(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/seller/SellerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    .line 143
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->isOfflineTransactionLimitExceeded()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private tipAppliedInBuyerCheckout()Z
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public authorize(Ljava/lang/Object;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;"
        }
    .end annotation

    const-string v0, "instrument"

    .line 64
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 66
    invoke-virtual {p0, p1}, Lcom/squareup/ui/seller/SellerWorkflow;->validateInstrument(Ljava/lang/Object;)V

    .line 68
    invoke-direct {p0, p1}, Lcom/squareup/ui/seller/SellerWorkflow;->isOfflineLimitExceeded(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    invoke-virtual {p0}, Lcom/squareup/ui/seller/SellerWorkflow;->onOfflineLimitExceeded()V

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/seller/SellerWorkflow;->analytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    invoke-virtual {p1}, Lcom/squareup/analytics/StoreAndForwardAnalytics;->logTransactionLimitExceeded()V

    .line 71
    sget-object p1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->SHOW_OFFLINE_MODE_TRANSACTION_LIMIT_REACHED_WARNING_DIALOG_SCREEN:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    return-object p1

    .line 74
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/seller/SellerWorkflow;->onWillTryToAuth(Ljava/lang/Object;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p0, v0, p1}, Lcom/squareup/ui/seller/SellerWorkflow;->isInAuthRange(Lcom/squareup/payment/Transaction;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerWorkflow;->hudToaster:Lcom/squareup/payment/PaymentHudToaster;

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentHudToaster;->cancel()V

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/seller/SellerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->asBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    .line 79
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->startSingleTenderBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    .line 81
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/seller/SellerWorkflow;->createTender(Ljava/lang/Object;)Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object p1

    .line 82
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/payment/tender/BaseTender$Builder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 84
    invoke-direct {p0}, Lcom/squareup/ui/seller/SellerWorkflow;->tipAppliedInBuyerCheckout()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 86
    iget-object v1, p0, Lcom/squareup/ui/seller/SellerWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v1}, Lcom/squareup/payment/TenderInEdit;->clearSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->build()Lcom/squareup/payment/tender/SmartCardTender;

    move-result-object v1

    .line 87
    instance-of v2, p1, Lcom/squareup/payment/tender/MagStripeTenderBuilder;

    if-eqz v2, :cond_3

    .line 88
    move-object v2, p1

    check-cast v2, Lcom/squareup/payment/tender/MagStripeTenderBuilder;

    invoke-virtual {v1}, Lcom/squareup/payment/tender/SmartCardTender;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 89
    invoke-virtual {v1}, Lcom/squareup/payment/tender/SmartCardTender;->getTipPercentage()Lcom/squareup/util/Percentage;

    move-result-object v1

    .line 88
    invoke-virtual {v2, v3, v1}, Lcom/squareup/payment/tender/MagStripeTenderBuilder;->setTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V

    goto :goto_1

    .line 91
    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/seller/SellerWorkflow;->tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

    invoke-virtual {v1, p1}, Lcom/squareup/payment/TipDeterminerFactory;->createFor(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/payment/TipDeterminer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/TipDeterminer;->showPreAuthTipScreen()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0, p1}, Lcom/squareup/payment/TenderInEdit;->editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    .line 93
    sget-object p1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_BUYER_FLOW:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    return-object p1

    .line 96
    :cond_3
    :goto_1
    invoke-virtual {v0, p1}, Lcom/squareup/payment/BillPayment;->addTender(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/payment/tender/BaseTender;

    move-result-object p1

    .line 97
    invoke-virtual {v0, p1}, Lcom/squareup/payment/BillPayment;->shouldCallAuthOnTender(Lcom/squareup/payment/tender/BaseTender;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 99
    sget-object p1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_BUYER_FLOW_AND_AUTHORIZE:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    return-object p1

    .line 101
    :cond_4
    sget-object p1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_BUYER_FLOW:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    return-object p1

    .line 103
    :cond_5
    invoke-virtual {p0, p1}, Lcom/squareup/ui/seller/SellerWorkflow;->toastOutOfRange(Ljava/lang/Object;)Z

    .line 104
    sget-object p1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->DO_NOTHING:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    return-object p1
.end method

.method protected canProcessInstrumentOffline(Ljava/lang/Object;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    const/4 p1, 0x1

    return p1
.end method

.method protected abstract createTender(Ljava/lang/Object;)Lcom/squareup/payment/tender/BaseTender$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/squareup/payment/tender/BaseTender$Builder;"
        }
    .end annotation
.end method

.method protected isInAuthRange(Lcom/squareup/payment/Transaction;Ljava/lang/Object;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Transaction;",
            "TT;)Z"
        }
    .end annotation

    .line 112
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->paymentIsWithinRange()Z

    move-result p2

    if-nez p2, :cond_1

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->ingoreTransactionLimits()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method protected isInstrumentReadyToAuthorize(Ljava/lang/Object;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    const/4 p1, 0x1

    return p1
.end method

.method protected onOfflineLimitExceeded()V
    .locals 0

    return-void
.end method

.method protected onWillTryToAuth(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    return-void
.end method

.method public readyToAuthorize(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 56
    invoke-virtual {p0, p1}, Lcom/squareup/ui/seller/SellerWorkflow;->isInstrumentReadyToAuthorize(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p0, v0, p1}, Lcom/squareup/ui/seller/SellerWorkflow;->isInAuthRange(Lcom/squareup/payment/Transaction;Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method protected toastOutOfRange(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .line 120
    iget-object p1, p0, Lcom/squareup/ui/seller/SellerWorkflow;->hudToaster:Lcom/squareup/payment/PaymentHudToaster;

    iget-object v0, p0, Lcom/squareup/ui/seller/SellerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1, v0}, Lcom/squareup/payment/PaymentHudToaster;->toastPaymentOutOfRange(Lcom/squareup/payment/Transaction;)Z

    move-result p1

    return p1
.end method

.method protected abstract validateInstrument(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method
