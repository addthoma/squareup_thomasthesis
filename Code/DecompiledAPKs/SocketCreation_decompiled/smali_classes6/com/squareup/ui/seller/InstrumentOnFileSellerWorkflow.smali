.class public Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;
.super Lcom/squareup/ui/seller/SellerWorkflow;
.source "InstrumentOnFileSellerWorkflow.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/seller/SellerWorkflow<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final customerSettings:Lcom/squareup/crm/CustomerManagementSettings;

.field private final expirationHelper:Lcom/squareup/card/ExpirationHelper;

.field private final paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

.field private final x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method constructor <init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/card/ExpirationHelper;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 52
    invoke-direct/range {p0 .. p8}, Lcom/squareup/ui/seller/SellerWorkflow;-><init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/TipDeterminerFactory;)V

    .line 54
    iput-object p9, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->expirationHelper:Lcom/squareup/card/ExpirationHelper;

    .line 55
    iput-object p10, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->customerSettings:Lcom/squareup/crm/CustomerManagementSettings;

    .line 56
    iput-object p11, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    .line 57
    iput-object p12, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    .line 58
    iput-object p13, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    return-void
.end method

.method private processSingleTender(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;Ljava/lang/String;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 8

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v7, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->CARD_ON_FILE_CHARGE_ATTEMPT:Lcom/squareup/analytics/RegisterActionName;

    iget-object v4, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->instrument_token:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    .line 134
    invoke-virtual {v1}, Lcom/squareup/papersignature/PaperSignatureSettings;->isSignOnPrintedReceiptEnabled()Z

    move-result v6

    const/4 v5, 0x0

    move-object v1, v7

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 132
    invoke-interface {v0, v7}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->tenderInstrumentOnFile(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 139
    sget-object p1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->DO_NOTHING:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    return-object p1

    .line 142
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->readyToAuthorize(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 146
    invoke-virtual {p0, p1}, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->authorize(Ljava/lang/Object;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1

    .line 143
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Selected instrument must be ready to authorize."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private processSplitTender(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;Ljava/lang/String;Lcom/squareup/tenderpayment/TenderCompleter;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 8

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v7, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->CARD_ON_FILE_CHARGE_ATTEMPT:Lcom/squareup/analytics/RegisterActionName;

    iget-object v4, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->instrument_token:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    .line 114
    invoke-virtual {v1}, Lcom/squareup/papersignature/PaperSignatureSettings;->isSignOnPrintedReceiptEnabled()Z

    move-result v6

    const/4 v5, 0x1

    move-object v1, v7

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 112
    invoke-interface {v0, v7}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 116
    iget-object p2, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {p2}, Lcom/squareup/payment/TenderInEdit;->clearInstrumentTender()Lcom/squareup/payment/tender/InstrumentTender$Builder;

    move-result-object p2

    .line 117
    invoke-virtual {p2, p1}, Lcom/squareup/payment/tender/InstrumentTender$Builder;->setInstrument(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;)V

    .line 119
    iget-object p1, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v0, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    .line 120
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCustomerSummary()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->customer_id:Ljava/lang/String;

    .line 119
    invoke-interface {p1, p2, v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->splitTenderInstrumentOnFile(Lcom/squareup/payment/tender/InstrumentTender$Builder;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 121
    sget-object p1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->DO_NOTHING:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    return-object p1

    .line 124
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/squareup/payment/BillPayment;->addTender(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/payment/tender/BaseTender;

    move-result-object p1

    .line 125
    iget-object p2, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/payment/BillPayment;->shouldCallAuthOnTender(Lcom/squareup/payment/tender/BaseTender;)Z

    move-result p1

    .line 127
    invoke-interface {p3, p1}, Lcom/squareup/tenderpayment/TenderCompleter;->completeTenderAndAuthorize(Z)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method protected createTender(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;)Lcom/squareup/payment/tender/BaseTender$Builder;
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/TenderFactory;->createInstrument()Lcom/squareup/payment/tender/InstrumentTender$Builder;

    move-result-object v0

    .line 63
    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/InstrumentTender$Builder;->setInstrument(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;)V

    .line 65
    iget-object p1, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getCustomerId()Ljava/lang/String;

    move-result-object p1

    .line 66
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 69
    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/InstrumentTender$Builder;->setContactToken(Ljava/lang/String;)V

    return-object v0

    .line 67
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "cannot create InstrumentTender without contact token."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method protected bridge synthetic createTender(Ljava/lang/Object;)Lcom/squareup/payment/tender/BaseTender$Builder;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->createTender(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;)Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object p1

    return-object p1
.end method

.method protected isInAuthRange(Lcom/squareup/payment/Transaction;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;)Z
    .locals 4

    .line 81
    iget-object v0, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-ne v0, v1, :cond_1

    .line 82
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p2

    iget-object p2, p2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p2, v0, v2

    if-lez p2, :cond_0

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->paymentIsAboveMaximum()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1

    .line 84
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/seller/SellerWorkflow;->isInAuthRange(Lcom/squareup/payment/Transaction;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method protected bridge synthetic isInAuthRange(Lcom/squareup/payment/Transaction;Ljava/lang/Object;)Z
    .locals 0

    .line 36
    check-cast p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->isInAuthRange(Lcom/squareup/payment/Transaction;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;)Z

    move-result p1

    return p1
.end method

.method public onConfirmChargeCardOnFile(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;Lcom/squareup/tenderpayment/TenderCompleter;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 5

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->customerSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isCardOnFileEnabled()Z

    move-result v0

    const-string v1, "Trying to charge card on file with feature disabled."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCustomerSummary()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    const-string v4, "Trying to charge card on file without a selected instrument."

    .line 98
    invoke-static {v3, v4}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    const-string v2, "Trying to charge card on file without a contact."

    .line 100
    invoke-static {v1, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 101
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->customer_id:Ljava/lang/String;

    .line 103
    iget-object v1, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 104
    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->processSplitTender(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;Ljava/lang/String;Lcom/squareup/tenderpayment/TenderCompleter;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1

    .line 106
    :cond_2
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->processSingleTender(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;Ljava/lang/String;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1
.end method

.method protected validateInstrument(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;)V
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->expirationHelper:Lcom/squareup/card/ExpirationHelper;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/card/ExpirationHelper;->isExpired(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 76
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Attempted to authorize an expired card."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method protected bridge synthetic validateInstrument(Ljava/lang/Object;)V
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;->validateInstrument(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;)V

    return-void
.end method
