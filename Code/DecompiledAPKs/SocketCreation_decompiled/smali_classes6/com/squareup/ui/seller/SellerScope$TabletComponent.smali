.class public interface abstract Lcom/squareup/ui/seller/SellerScope$TabletComponent;
.super Ljava/lang/Object;
.source "SellerScope.java"

# interfaces
.implements Lcom/squareup/ui/seller/SellerScope$BaseComponent;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/seller/SellerScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/seller/SellerScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TabletComponent"
.end annotation


# virtual methods
.method public abstract homeTablet()Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;
.end method
