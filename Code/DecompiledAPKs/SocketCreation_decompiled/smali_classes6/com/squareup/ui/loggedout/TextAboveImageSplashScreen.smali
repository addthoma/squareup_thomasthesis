.class public final Lcom/squareup/ui/loggedout/TextAboveImageSplashScreen;
.super Lcom/squareup/ui/loggedout/InLoggedOutActivityScope;
.source "TextAboveImageSplashScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/LocksOrientation;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/loggedout/TextAboveImageSplashScreen$ParentComponent;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTextAboveImageSplashScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TextAboveImageSplashScreen.kt\ncom/squareup/ui/loggedout/TextAboveImageSplashScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,54:1\n52#2:55\n*E\n*S KotlinDebug\n*F\n+ 1 TextAboveImageSplashScreen.kt\ncom/squareup/ui/loggedout/TextAboveImageSplashScreen\n*L\n46#1:55\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0001\u0017B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0008\u0010\u0015\u001a\u00020\u0016H\u0016R\u001e\u0010\u0006\u001a\u0010\u0012\u000c\u0012\n \u0008*\u0004\u0018\u00010\u00000\u00000\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\nX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\r\u001a\u00020\nX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000c\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/ui/loggedout/TextAboveImageSplashScreen;",
        "Lcom/squareup/ui/loggedout/InLoggedOutActivityScope;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/container/LocksOrientation;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "()V",
        "CREATOR",
        "Lcom/squareup/container/ContainerTreeKey$PathCreator;",
        "kotlin.jvm.PlatformType",
        "orientationForPhone",
        "Lcom/squareup/workflow/WorkflowViewFactory$Orientation;",
        "getOrientationForPhone",
        "()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;",
        "orientationForTablet",
        "getOrientationForTablet",
        "getAnalyticsName",
        "Lcom/squareup/analytics/RegisterViewName;",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "",
        "ParentComponent",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Lcom/squareup/container/ContainerTreeKey$PathCreator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/container/ContainerTreeKey$PathCreator<",
            "Lcom/squareup/ui/loggedout/TextAboveImageSplashScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/loggedout/TextAboveImageSplashScreen;

.field private static final orientationForPhone:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

.field private static final orientationForTablet:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 32
    new-instance v0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreen;

    invoke-direct {v0}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreen;->INSTANCE:Lcom/squareup/ui/loggedout/TextAboveImageSplashScreen;

    .line 37
    sget-object v0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreen;->INSTANCE:Lcom/squareup/ui/loggedout/TextAboveImageSplashScreen;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "PathCreator.forSingleton\u2026xtAboveImageSplashScreen)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreen;->CREATOR:Lcom/squareup/container/ContainerTreeKey$PathCreator;

    .line 39
    sget-object v0, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->SENSOR_PORTRAIT:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    sput-object v0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreen;->orientationForPhone:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    .line 41
    sget-object v0, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->UNLOCKED:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    sput-object v0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreen;->orientationForTablet:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/squareup/ui/loggedout/InLoggedOutActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 48
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->WELCOME_SPLASH:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getOrientationForPhone()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;
    .locals 1

    .line 39
    sget-object v0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreen;->orientationForPhone:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-object v0
.end method

.method public getOrientationForTablet()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreen;->orientationForTablet:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "view.context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    const-class v0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreen$ParentComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreen$ParentComponent;

    .line 46
    invoke-interface {p1}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreen$ParentComponent;->textBelowImageSplashCoordinator()Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 43
    sget v0, Lcom/squareup/loggedout/R$layout;->text_above_image_splash_page_container:I

    return v0
.end method
