.class public final Lcom/squareup/ui/loggedout/LoggedOutActivityScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "LoggedOutActivityScope.java"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/loggedout/LoggedOutActivityScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope;

    invoke-direct {v0}, Lcom/squareup/ui/loggedout/LoggedOutActivityScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope;->INSTANCE:Lcom/squareup/ui/loggedout/LoggedOutActivityScope;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
