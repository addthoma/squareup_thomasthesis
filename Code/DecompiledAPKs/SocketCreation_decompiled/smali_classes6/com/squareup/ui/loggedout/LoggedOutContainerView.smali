.class public Lcom/squareup/ui/loggedout/LoggedOutContainerView;
.super Lcom/squareup/container/ContainerRelativeLayout;
.source "LoggedOutContainerView.java"


# instance fields
.field badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field container:Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final tourPopup:Lcom/squareup/ui/tour/LearnMoreTourPopup;

.field tourPresenter:Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 26
    invoke-direct {p0, p1, p2}, Lcom/squareup/container/ContainerRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    const-class p2, Lcom/squareup/loggedout/CommonLoggedOutActivityComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/loggedout/CommonLoggedOutActivityComponent;

    invoke-interface {p2, p0}, Lcom/squareup/loggedout/CommonLoggedOutActivityComponent;->inject(Lcom/squareup/ui/loggedout/LoggedOutContainerView;)V

    .line 28
    new-instance p2, Lcom/squareup/ui/tour/LearnMoreTourPopup;

    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutContainerView;->tourPresenter:Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;

    invoke-direct {p2, p1, v0}, Lcom/squareup/ui/tour/LearnMoreTourPopup;-><init>(Landroid/content/Context;Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;)V

    iput-object p2, p0, Lcom/squareup/ui/loggedout/LoggedOutContainerView;->tourPopup:Lcom/squareup/ui/tour/LearnMoreTourPopup;

    return-void
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 2

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutContainerView;->tourPresenter:Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/loggedout/LoggedOutContainerView;->tourPopup:Lcom/squareup/ui/tour/LearnMoreTourPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutContainerView;->container:Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->dropView(Lcom/squareup/container/ContainerView;)V

    .line 42
    invoke-super {p0}, Lcom/squareup/container/ContainerRelativeLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 32
    invoke-super {p0}, Lcom/squareup/container/ContainerRelativeLayout;->onFinishInflate()V

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutContainerView;->container:Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->takeView(Ljava/lang/Object;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutContainerView;->tourPresenter:Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/loggedout/LoggedOutContainerView;->tourPopup:Lcom/squareup/ui/tour/LearnMoreTourPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onRequestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutContainerView;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {v0}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    invoke-static {p2}, Lcom/squareup/accessibility/AccessibilityScrubber;->getPiiRecord(Landroid/view/accessibility/AccessibilityEvent;)Landroid/view/accessibility/AccessibilityRecord;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 49
    invoke-static {p0, p2, v0}, Lcom/squareup/accessibility/AccessibilityScrubber;->sendPiiScrubbedEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;Landroid/view/accessibility/AccessibilityRecord;)V

    const/4 p1, 0x0

    return p1

    .line 54
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/squareup/container/ContainerRelativeLayout;->onRequestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result p1

    return p1
.end method
