.class public Lcom/squareup/ui/loggedout/LoggedOutActivity;
.super Lcom/squareup/ui/SquareActivity;
.source "LoggedOutActivity.java"

# interfaces
.implements Lcom/squareup/container/ContainerActivity;
.implements Lcom/squareup/account/ImpersonationHelper$View;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/loggedout/LoggedOutActivity$Module;
    }
.end annotation


# static fields
.field private static final DIRECT_TO_CREATE_ACCOUNT_EXTRA:Ljava/lang/String;

.field private static final DIRECT_TO_LOGIN_EXTRA:Ljava/lang/String;


# instance fields
.field browserLauncher:Lcom/squareup/util/BrowserLauncher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field container:Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field containerActivityDelegate:Lcom/squareup/container/ContainerActivityDelegate;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field createAccountStarter:Lcom/squareup/loggedout/CreateAccountStarter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field deepLinkHelper:Lcom/squareup/analytics/DeepLinkHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field encryptedEmailsFromReferrals:Lcom/squareup/analytics/EncryptedEmailsFromReferrals;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field impersonationHelper:Lcom/squareup/account/ImpersonationHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field loggedOutFinisher:Lcom/squareup/ui/loggedout/LoggedOutFinisher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mainScheduler:Lrx/Scheduler;
    .annotation runtime Lcom/squareup/thread/Main;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field runner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final subscriptions:Lrx/subscriptions/CompositeSubscription;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 53
    const-class v0, Lcom/squareup/ui/loggedout/LoggedOutActivity;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 54
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".directToLogin"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/squareup/ui/loggedout/LoggedOutActivity;->DIRECT_TO_LOGIN_EXTRA:Ljava/lang/String;

    .line 59
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 60
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ".directToCreateAccount"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->DIRECT_TO_CREATE_ACCOUNT_EXTRA:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 78
    sget-object v0, Lcom/squareup/ui/SquareActivity$Preconditions;->NO_AUTH_NEEDED:Lcom/squareup/ui/SquareActivity$Preconditions;

    invoke-direct {p0, v0}, Lcom/squareup/ui/SquareActivity;-><init>(Lcom/squareup/ui/SquareActivity$Preconditions;)V

    .line 75
    new-instance v0, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v0}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    return-void
.end method

.method private directToCreateAccount()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "LoggedOutActivity direct to create account"

    .line 265
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 266
    invoke-virtual {p0}, Lcom/squareup/ui/loggedout/LoggedOutActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 267
    sget-object v1, Lcom/squareup/ui/loggedout/LoggedOutActivity;->DIRECT_TO_CREATE_ACCOUNT_EXTRA:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 268
    invoke-virtual {p0, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity;->setIntent(Landroid/content/Intent;)V

    .line 269
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->createAccountStarter:Lcom/squareup/loggedout/CreateAccountStarter;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/squareup/loggedout/CreateAccountStarter;->startCreateAccount(Z)V

    return-void
.end method

.method private directToCreateAccountScreenEnabled()Z
    .locals 3

    .line 245
    invoke-virtual {p0}, Lcom/squareup/ui/loggedout/LoggedOutActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/loggedout/LoggedOutActivity;->DIRECT_TO_CREATE_ACCOUNT_EXTRA:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private directToLogin()V
    .locals 3

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "LoggedOutActivity direct to login"

    .line 253
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 254
    invoke-virtual {p0}, Lcom/squareup/ui/loggedout/LoggedOutActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 255
    sget-object v2, Lcom/squareup/ui/loggedout/LoggedOutActivity;->DIRECT_TO_LOGIN_EXTRA:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 256
    invoke-virtual {p0, v1}, Lcom/squareup/ui/loggedout/LoggedOutActivity;->setIntent(Landroid/content/Intent;)V

    .line 257
    iget-object v1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->runner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->startLoginFlow(Z)V

    return-void
.end method

.method private directToLoginScreenEnabled()Z
    .locals 3

    .line 241
    invoke-virtual {p0}, Lcom/squareup/ui/loggedout/LoggedOutActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/loggedout/LoggedOutActivity;->DIRECT_TO_LOGIN_EXTRA:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static putDirectToCreateAccount(Landroid/content/Intent;)V
    .locals 2

    .line 187
    sget-object v0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->DIRECT_TO_CREATE_ACCOUNT_EXTRA:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-void
.end method

.method public static putDirectToLogin(Landroid/content/Intent;)V
    .locals 2

    .line 177
    sget-object v0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->DIRECT_TO_LOGIN_EXTRA:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method protected doBackPressed()V
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->runner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->onBackPressed()V

    return-void
.end method

.method protected doCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 86
    invoke-super {p0, p1}, Lcom/squareup/ui/SquareActivity;->doCreate(Landroid/os/Bundle;)V

    .line 88
    invoke-static {p0}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->deepLinkHelper:Lcom/squareup/analytics/DeepLinkHelper;

    .line 89
    invoke-interface {v0}, Lcom/squareup/analytics/DeepLinkHelper;->encryptedEmail()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->mainScheduler:Lrx/Scheduler;

    .line 90
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->encryptedEmailsFromReferrals:Lcom/squareup/analytics/EncryptedEmailsFromReferrals;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/loggedout/-$$Lambda$98FlnvsQrkkannI-_EzvQ4fq1C8;

    invoke-direct {v2, v1}, Lcom/squareup/ui/loggedout/-$$Lambda$98FlnvsQrkkannI-_EzvQ4fq1C8;-><init>(Lcom/squareup/analytics/EncryptedEmailsFromReferrals;)V

    .line 91
    invoke-virtual {v0, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 88
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 93
    invoke-static {p0}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->deepLinkHelper:Lcom/squareup/analytics/DeepLinkHelper;

    .line 94
    invoke-interface {v0}, Lcom/squareup/analytics/DeepLinkHelper;->anonymousVisitorToken()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->mainScheduler:Lrx/Scheduler;

    .line 95
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->runner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/loggedout/-$$Lambda$FMXwB0lsTVTayOdx-ZxkBALkEiE;

    invoke-direct {v2, v1}, Lcom/squareup/ui/loggedout/-$$Lambda$FMXwB0lsTVTayOdx-ZxkBALkEiE;-><init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)V

    .line 96
    invoke-virtual {v0, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 93
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 98
    iget-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->deepLinkHelper:Lcom/squareup/analytics/DeepLinkHelper;

    invoke-interface {p1, p0}, Lcom/squareup/analytics/DeepLinkHelper;->onActivityCreate(Landroid/app/Activity;)V

    .line 100
    iget-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

    invoke-virtual {p1, p0}, Lcom/squareup/ui/SoftInputPresenter;->takeView(Ljava/lang/Object;)V

    .line 101
    iget-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->impersonationHelper:Lcom/squareup/account/ImpersonationHelper;

    invoke-virtual {p1, p0}, Lcom/squareup/account/ImpersonationHelper;->takeView(Ljava/lang/Object;)V

    .line 102
    iget-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    invoke-interface {p1, p0}, Lcom/squareup/util/BrowserLauncher;->takeActivity(Landroid/app/Activity;)V

    .line 103
    iget-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->containerActivityDelegate:Lcom/squareup/container/ContainerActivityDelegate;

    invoke-virtual {p1, p0}, Lcom/squareup/container/ContainerActivityDelegate;->takeActivity(Lcom/squareup/container/ContainerActivity;)V

    .line 104
    iget-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->loggedOutFinisher:Lcom/squareup/ui/loggedout/LoggedOutFinisher;

    invoke-virtual {p1, p0}, Lcom/squareup/ui/loggedout/LoggedOutFinisher;->takeActivity(Lcom/squareup/ui/loggedout/LoggedOutActivity;)V

    .line 105
    iget-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->containerActivityDelegate:Lcom/squareup/container/ContainerActivityDelegate;

    sget-object v0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope;->INSTANCE:Lcom/squareup/ui/loggedout/LoggedOutActivityScope;

    sget v1, Lcom/squareup/loggedout/R$layout;->logged_out_container:I

    .line 106
    invoke-virtual {p1, v0, p0, v1}, Lcom/squareup/container/ContainerActivityDelegate;->inflateRootScreen(Lflow/path/Path;Landroid/content/Context;I)Landroid/view/View;

    move-result-object p1

    .line 105
    invoke-virtual {p0, p1}, Lcom/squareup/ui/loggedout/LoggedOutActivity;->setContentView(Landroid/view/View;)V

    .line 109
    invoke-direct {p0}, Lcom/squareup/ui/loggedout/LoggedOutActivity;->directToLoginScreenEnabled()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 110
    invoke-direct {p0}, Lcom/squareup/ui/loggedout/LoggedOutActivity;->directToLogin()V

    goto :goto_0

    .line 111
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/loggedout/LoggedOutActivity;->directToCreateAccountScreenEnabled()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 112
    invoke-direct {p0}, Lcom/squareup/ui/loggedout/LoggedOutActivity;->directToCreateAccount()V

    .line 115
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->runner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->finishActivity()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/loggedout/-$$Lambda$LoggedOutActivity$TBKy14mWC6uQH3kCBdNtjctISXk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/loggedout/-$$Lambda$LoggedOutActivity$TBKy14mWC6uQH3kCBdNtjctISXk;-><init>(Lcom/squareup/ui/loggedout/LoggedOutActivity;)V

    .line 116
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 115
    invoke-virtual {p1, v0}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method public finish()V
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->container:Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;

    if-eqz v0, :cond_0

    .line 165
    invoke-virtual {v0}, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->onActivityFinish()V

    .line 167
    :cond_0
    invoke-super {p0}, Lcom/squareup/ui/SquareActivity;->finish()V

    return-void
.end method

.method public getBundleService()Lmortar/bundler/BundleService;
    .locals 1

    .line 153
    invoke-static {p0}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object v0

    return-object v0
.end method

.method protected inject(Lmortar/MortarScope;)V
    .locals 1

    .line 82
    const-class v0, Lcom/squareup/loggedout/CommonLoggedOutActivityComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/loggedout/CommonLoggedOutActivityComponent;

    invoke-interface {p1, p0}, Lcom/squareup/loggedout/CommonLoggedOutActivityComponent;->inject(Lcom/squareup/ui/loggedout/LoggedOutActivity;)V

    return-void
.end method

.method public synthetic lambda$doCreate$0$LoggedOutActivity(Lkotlin/Unit;)V
    .locals 2

    const/4 p1, 0x0

    new-array v0, p1, [Ljava/lang/Object;

    const-string v1, "Runner requested to finish activity, finishing."

    .line 117
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 120
    invoke-static {p0, p1}, Lcom/squareup/ui/PaymentActivity;->exitWithResult(Landroid/app/Activity;I)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 148
    invoke-super {p0, p1, p2, p3}, Lcom/squareup/ui/SquareActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->impersonationHelper:Lcom/squareup/account/ImpersonationHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/account/ImpersonationHelper;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->containerActivityDelegate:Lcom/squareup/container/ContainerActivityDelegate;

    invoke-virtual {v0, p0}, Lcom/squareup/container/ContainerActivityDelegate;->dropActivity(Lcom/squareup/container/ContainerActivity;)V

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    invoke-interface {v0, p0}, Lcom/squareup/util/BrowserLauncher;->dropActivity(Landroid/app/Activity;)V

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/SoftInputPresenter;->dropView(Ljava/lang/Object;)V

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->impersonationHelper:Lcom/squareup/account/ImpersonationHelper;

    invoke-virtual {v0, p0}, Lcom/squareup/account/ImpersonationHelper;->dropView(Ljava/lang/Object;)V

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->loggedOutFinisher:Lcom/squareup/ui/loggedout/LoggedOutFinisher;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/loggedout/LoggedOutFinisher;->dropActivity(Lcom/squareup/ui/loggedout/LoggedOutActivity;)V

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->clear()V

    .line 144
    invoke-super {p0}, Lcom/squareup/ui/SquareActivity;->onDestroy()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    .line 129
    invoke-super {p0}, Lcom/squareup/ui/SquareActivity;->onResume()V

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivity;->runner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->maybeSignInWithSingleSignOnSessionToken()V

    return-void
.end method
