.class public final Lcom/squareup/ui/loggedout/SplashCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SplashCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;,
        Lcom/squareup/ui/loggedout/SplashCoordinator$Pages;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSplashCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SplashCoordinator.kt\ncom/squareup/ui/loggedout/SplashCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,102:1\n1103#2,7:103\n1103#2,7:110\n*E\n*S KotlinDebug\n*F\n+ 1 SplashCoordinator.kt\ncom/squareup/ui/loggedout/SplashCoordinator\n*L\n58#1,7:103\n64#1,7:110\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0002\u0010\u0011B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000e\u0008\u0001\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/ui/loggedout/SplashCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "scopeRunner",
        "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "pages",
        "",
        "Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;",
        "encryptedEmailsFromReferrals",
        "Lcom/squareup/analytics/EncryptedEmailsFromReferrals;",
        "(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/analytics/Analytics;Ljava/util/List;Lcom/squareup/analytics/EncryptedEmailsFromReferrals;)V",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "Pages",
        "SplashPage",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final encryptedEmailsFromReferrals:Lcom/squareup/analytics/EncryptedEmailsFromReferrals;

.field private final pages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/analytics/Analytics;Ljava/util/List;Lcom/squareup/analytics/EncryptedEmailsFromReferrals;)V
    .locals 1
    .param p3    # Ljava/util/List;
        .annotation runtime Lcom/squareup/ui/loggedout/SplashCoordinator$Pages;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;",
            "Lcom/squareup/analytics/Analytics;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;",
            ">;",
            "Lcom/squareup/analytics/EncryptedEmailsFromReferrals;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "scopeRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pages"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "encryptedEmailsFromReferrals"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/loggedout/SplashCoordinator;->scopeRunner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/loggedout/SplashCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p3, p0, Lcom/squareup/ui/loggedout/SplashCoordinator;->pages:Ljava/util/List;

    iput-object p4, p0, Lcom/squareup/ui/loggedout/SplashCoordinator;->encryptedEmailsFromReferrals:Lcom/squareup/analytics/EncryptedEmailsFromReferrals;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/ui/loggedout/SplashCoordinator;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/loggedout/SplashCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getEncryptedEmailsFromReferrals$p(Lcom/squareup/ui/loggedout/SplashCoordinator;)Lcom/squareup/analytics/EncryptedEmailsFromReferrals;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/loggedout/SplashCoordinator;->encryptedEmailsFromReferrals:Lcom/squareup/analytics/EncryptedEmailsFromReferrals;

    return-object p0
.end method

.method public static final synthetic access$getPages$p(Lcom/squareup/ui/loggedout/SplashCoordinator;)Ljava/util/List;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/loggedout/SplashCoordinator;->pages:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$getScopeRunner$p(Lcom/squareup/ui/loggedout/SplashCoordinator;)Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/loggedout/SplashCoordinator;->scopeRunner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    return-object p0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    sget v0, Lcom/squareup/loggedout/R$id;->sign_in:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    .line 58
    check-cast v0, Landroid/view/View;

    .line 103
    new-instance v1, Lcom/squareup/ui/loggedout/SplashCoordinator$attach$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/loggedout/SplashCoordinator$attach$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/ui/loggedout/SplashCoordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    sget v0, Lcom/squareup/loggedout/R$id;->create_account:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    .line 64
    check-cast v0, Landroid/view/View;

    .line 110
    new-instance v1, Lcom/squareup/ui/loggedout/SplashCoordinator$attach$$inlined$onClickDebounced$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/loggedout/SplashCoordinator$attach$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/ui/loggedout/SplashCoordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    sget v0, Lcom/squareup/loggedout/R$id;->splash_pager:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/viewpager/widget/ViewPager;

    .line 70
    sget v1, Lcom/squareup/loggedout/R$id;->page_indicator:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/marin/widgets/MarinPageIndicator;

    .line 72
    new-instance v2, Lcom/squareup/ui/loggedout/SplashPagerAdapter;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "view.context"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/squareup/ui/loggedout/SplashCoordinator;->pages:Ljava/util/List;

    invoke-direct {v2, v3, v4}, Lcom/squareup/ui/loggedout/SplashPagerAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    check-cast v2, Landroidx/viewpager/widget/PagerAdapter;

    invoke-virtual {v0, v2}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    const/4 v2, 0x0

    .line 73
    invoke-virtual {v0, v2}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    .line 75
    sget-object v3, Lcom/squareup/ui/loggedout/SplashCoordinator$attach$3;->INSTANCE:Lcom/squareup/ui/loggedout/SplashCoordinator$attach$3;

    check-cast v3, Landroidx/viewpager/widget/ViewPager$PageTransformer;

    invoke-virtual {v0, v2, v3}, Landroidx/viewpager/widget/ViewPager;->setPageTransformer(ZLandroidx/viewpager/widget/ViewPager$PageTransformer;)V

    .line 84
    new-instance v2, Lcom/squareup/ui/loggedout/SplashCoordinator$attach$4;

    invoke-direct {v2, p0}, Lcom/squareup/ui/loggedout/SplashCoordinator$attach$4;-><init>(Lcom/squareup/ui/loggedout/SplashCoordinator;)V

    check-cast v2, Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    invoke-virtual {v0, v2}, Landroidx/viewpager/widget/ViewPager;->addOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    .line 94
    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinPageIndicator;->setViewPager(Landroidx/viewpager/widget/ViewPager;)V

    .line 96
    new-instance v0, Lcom/squareup/ui/loggedout/SplashCoordinator$attach$5;

    invoke-direct {v0, p0}, Lcom/squareup/ui/loggedout/SplashCoordinator$attach$5;-><init>(Lcom/squareup/ui/loggedout/SplashCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
