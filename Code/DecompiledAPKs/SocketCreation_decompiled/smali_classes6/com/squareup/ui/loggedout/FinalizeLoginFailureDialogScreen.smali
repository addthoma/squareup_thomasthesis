.class public final Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogScreen;
.super Lcom/squareup/container/ContainerTreeKey;
.source "FinalizeLoginFailureDialogScreen.kt"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogScreen$FactoryProxy;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogScreen$FactoryProxy;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0005\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u0002:\u0001\u000fB\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0014\u0010\n\u001a\u00020\u000bX\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000cR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogScreen;",
        "Lcom/squareup/container/ContainerTreeKey;",
        "Lcom/squareup/container/MaybePersistent;",
        "dialogFactory",
        "Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;",
        "sessionToken",
        "",
        "(Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;Ljava/lang/String;)V",
        "getDialogFactory",
        "()Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;",
        "isPersistent",
        "",
        "()Z",
        "getSessionToken",
        "()Ljava/lang/String;",
        "FactoryProxy",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dialogFactory:Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;

.field private final isPersistent:Z

.field private final sessionToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;Ljava/lang/String;)V
    .locals 1

    const-string v0, "dialogFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sessionToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/squareup/container/ContainerTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogScreen;->dialogFactory:Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;

    iput-object p2, p0, Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogScreen;->sessionToken:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getDialogFactory()Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogScreen;->dialogFactory:Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;

    return-object v0
.end method

.method public final getSessionToken()Ljava/lang/String;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogScreen;->sessionToken:Ljava/lang/String;

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    .line 22
    iget-boolean v0, p0, Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogScreen;->isPersistent:Z

    return v0
.end method
