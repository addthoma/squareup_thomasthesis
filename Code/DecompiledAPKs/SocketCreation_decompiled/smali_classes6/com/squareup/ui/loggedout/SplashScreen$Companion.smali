.class public final Lcom/squareup/ui/loggedout/SplashScreen$Companion;
.super Ljava/lang/Object;
.source "SplashScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/loggedout/SplashScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u001e\u0010\u0003\u001a\u0010\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/loggedout/SplashScreen$Companion;",
        "",
        "()V",
        "CREATOR",
        "Lcom/squareup/container/ContainerTreeKey$PathCreator;",
        "Lcom/squareup/ui/loggedout/SplashScreen;",
        "kotlin.jvm.PlatformType",
        "INSTANCE",
        "getINSTANCE",
        "()Lcom/squareup/ui/loggedout/SplashScreen;",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/squareup/ui/loggedout/SplashScreen$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getINSTANCE()Lcom/squareup/ui/loggedout/SplashScreen;
    .locals 1

    .line 24
    invoke-static {}, Lcom/squareup/ui/loggedout/SplashScreen;->access$getINSTANCE$cp()Lcom/squareup/ui/loggedout/SplashScreen;

    move-result-object v0

    return-object v0
.end method
