.class public Lcom/squareup/ui/tour/EmptyLearnMoreTourPager;
.super Ljava/lang/Object;
.source "EmptyLearnMoreTourPager.java"

# interfaces
.implements Lcom/squareup/ui/tour/LearnMoreTourPager;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getLearnMoreItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/tour/Tour$HasTourPages;",
            ">;"
        }
    .end annotation

    .line 15
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
