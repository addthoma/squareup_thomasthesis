.class Lcom/squareup/usb/UsbDiscoverer$PermissionReceiver;
.super Landroid/content/BroadcastReceiver;
.source "UsbDiscoverer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/usb/UsbDiscoverer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PermissionReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/usb/UsbDiscoverer;


# direct methods
.method private constructor <init>(Lcom/squareup/usb/UsbDiscoverer;)V
    .locals 0

    .line 231
    iput-object p1, p0, Lcom/squareup/usb/UsbDiscoverer$PermissionReceiver;->this$0:Lcom/squareup/usb/UsbDiscoverer;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/usb/UsbDiscoverer;Lcom/squareup/usb/UsbDiscoverer$1;)V
    .locals 0

    .line 231
    invoke-direct {p0, p1}, Lcom/squareup/usb/UsbDiscoverer$PermissionReceiver;-><init>(Lcom/squareup/usb/UsbDiscoverer;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .line 233
    iget-object p1, p0, Lcom/squareup/usb/UsbDiscoverer$PermissionReceiver;->this$0:Lcom/squareup/usb/UsbDiscoverer;

    invoke-static {p1, p2}, Lcom/squareup/usb/UsbDiscoverer;->access$100(Lcom/squareup/usb/UsbDiscoverer;Landroid/content/Intent;)Landroid/hardware/usb/UsbDevice;

    move-result-object p1

    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result p1

    .line 234
    iget-object v0, p0, Lcom/squareup/usb/UsbDiscoverer$PermissionReceiver;->this$0:Lcom/squareup/usb/UsbDiscoverer;

    invoke-static {v0, p1}, Lcom/squareup/usb/UsbDiscoverer;->access$200(Lcom/squareup/usb/UsbDiscoverer;I)Lcom/squareup/usb/UsbDiscoverer$DeviceListener;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/squareup/usb/UsbDiscoverer$PermissionReceiver;->this$0:Lcom/squareup/usb/UsbDiscoverer;

    invoke-static {v0, p2}, Lcom/squareup/usb/UsbDiscoverer;->access$100(Lcom/squareup/usb/UsbDiscoverer;Landroid/content/Intent;)Landroid/hardware/usb/UsbDevice;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "permission"

    .line 238
    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 240
    iget-object p2, p0, Lcom/squareup/usb/UsbDiscoverer$PermissionReceiver;->this$0:Lcom/squareup/usb/UsbDiscoverer;

    invoke-static {p2}, Lcom/squareup/usb/UsbDiscoverer;->access$300(Lcom/squareup/usb/UsbDiscoverer;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object p2

    invoke-virtual {p2, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_2

    .line 241
    invoke-interface {p1, v0}, Lcom/squareup/usb/UsbDiscoverer$DeviceListener;->deviceAvailable(Landroid/hardware/usb/UsbDevice;)V

    .line 242
    iget-object p1, p0, Lcom/squareup/usb/UsbDiscoverer$PermissionReceiver;->this$0:Lcom/squareup/usb/UsbDiscoverer;

    invoke-static {p1}, Lcom/squareup/usb/UsbDiscoverer;->access$300(Lcom/squareup/usb/UsbDiscoverer;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object p1

    invoke-virtual {p1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 245
    :cond_1
    iget-object p2, p0, Lcom/squareup/usb/UsbDiscoverer$PermissionReceiver;->this$0:Lcom/squareup/usb/UsbDiscoverer;

    invoke-static {p2}, Lcom/squareup/usb/UsbDiscoverer;->access$300(Lcom/squareup/usb/UsbDiscoverer;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object p2

    invoke-virtual {p2, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 246
    invoke-interface {p1, v0}, Lcom/squareup/usb/UsbDiscoverer$DeviceListener;->deviceUnavailable(Landroid/hardware/usb/UsbDevice;)V

    :cond_2
    :goto_0
    return-void
.end method
