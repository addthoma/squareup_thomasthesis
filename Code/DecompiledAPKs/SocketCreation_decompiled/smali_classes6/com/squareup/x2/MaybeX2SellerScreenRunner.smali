.class public interface abstract Lcom/squareup/x2/MaybeX2SellerScreenRunner;
.super Ljava/lang/Object;
.source "MaybeX2SellerScreenRunner.java"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/x2/BadMaybeSquareDeviceCheck;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/x2/MaybeX2SellerScreenRunner$NoOpForProd;
    }
.end annotation


# virtual methods
.method public abstract activitySearchCardRead()Z
.end method

.method public abstract branReady()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract cancelProcessingDippedCardForCardOnFile()Z
.end method

.method public abstract cancelSaveCard()Z
.end method

.method public abstract checkingGiftCardBalance()Z
.end method

.method public abstract configuringGiftCard(Lcom/squareup/protos/common/Money;)Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract configuringTender(Lcom/squareup/comms/protos/common/TenderType;)Z
.end method

.method public abstract continueConfirmation(Lcom/squareup/comms/protos/common/PostTransactionAction;)V
.end method

.method public abstract disableContactlessField()Z
.end method

.method public abstract dismissGiftCardActivation()Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract dismissGiftCardBalanceCheck()Z
.end method

.method public abstract dismissSaveCustomer()Z
.end method

.method public abstract displayCardOnFileAuth(Lcom/squareup/ui/main/RegisterTreeKey;)Z
.end method

.method public abstract displayCustomerDetails(Lcom/squareup/protos/client/rolodex/Contact;)Z
.end method

.method public abstract displayEGiftCardActivation(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;)Z
.end method

.method public abstract displayGiftCardActivation(Lcom/squareup/protos/common/Money;)Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract displayGiftCardBalance(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Z
.end method

.method public abstract displayGiftCardBalanceCheck()Z
.end method

.method public abstract enableContactlessField()Z
.end method

.method public abstract ensurePipScreen(Lcom/squareup/x2/ui/tender/PipTenderScreen;)V
.end method

.method public abstract enterCartForRetail()V
.end method

.method public abstract enteringActivitySearch()Z
.end method

.method public abstract enteringCustomersApplet()Z
.end method

.method public abstract enteringOrderTicketName()Z
.end method

.method public abstract enteringSaveCardOnFile()Z
.end method

.method public abstract enteringSaveCustomer()Z
.end method

.method public abstract errorCheckingGiftCardBalance()Z
.end method

.method public abstract exitingActivitySearch()Z
.end method

.method public abstract exitingCustomersApplet()Z
.end method

.method public abstract firstPaymentDialog()Lcom/squareup/ui/main/RegisterTreeKey;
.end method

.method public abstract getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;
.end method

.method public abstract getStateForSaveCard()Lcom/squareup/ui/crm/flow/SaveCardSharedState;
.end method

.method public abstract goBackInCustomerFlow()Z
.end method

.method public abstract invoiceCreated()Z
.end method

.method public abstract isBranReady()Z
.end method

.method public abstract isCardInsertedOnCartMonitor()Z
.end method

.method public abstract isOnPostTransactionMonitor()Z
.end method

.method public abstract isOnSignatureMonitor()Z
.end method

.method public abstract isOnTipMonitor()Z
.end method

.method public abstract loadingGiftCard()Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract onBuyerContactEntered()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/x2/customers/CustomerInfoWithState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onCanDisplayDetailsToBuyer()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onCardOnFileResult()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onCreateOrEditCustomer(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/x2/customers/CustomerInfoWithState$State;)Z
.end method

.method public abstract onOrderTicketNameResult()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/x2/tender/X2OrderTicketNameResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onPaymentMethodSelected()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onPipOrTenderDismissed()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end method

.method public abstract pipTenderScope()Lcom/squareup/container/ContainerTreeKey;
.end method

.method public abstract promptForPayment()Z
.end method

.method public abstract promptForPayment(Lcom/squareup/payment/tender/BaseTender$Builder;)Z
.end method

.method public abstract removeCompletedTender(Lcom/squareup/protos/common/Money;)Z
.end method

.method public abstract saveCustomerCardOnFile(Lcom/squareup/Card;Lcom/squareup/payment/BillPayment;)Z
.end method

.method public abstract saveCustomerContact(Lcom/squareup/protos/client/rolodex/Contact;)Z
.end method

.method public abstract savingCardOnFileCanceled(Z)Z
.end method

.method public abstract sellerCreatingCustomer()Z
.end method

.method public abstract showActivateAccountConfirmation()Z
.end method

.method public abstract showPipScreen(Lcom/squareup/x2/ui/tender/PipTenderScreen;)V
.end method

.method public abstract splitTenderInstrumentOnFile(Lcom/squareup/payment/tender/InstrumentTender$Builder;Ljava/lang/String;)Z
.end method

.method public abstract startOnboarding()V
.end method

.method public abstract startProcessingDippedCardForCardOnFile()Z
.end method

.method public abstract tenderCash(Lcom/squareup/payment/tender/CashTender$Builder;)Z
.end method

.method public abstract tenderFlowCanceled()Z
.end method

.method public abstract tenderInstrumentOnFile(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;Ljava/lang/String;)Z
.end method

.method public abstract tenderKeyedCard(Lcom/squareup/protos/common/Money;Lcom/squareup/Card;)Z
.end method

.method public abstract tenderOther(Lcom/squareup/payment/tender/OtherTender$Builder;)Z
.end method

.method public abstract tipMonitorOnSellerSkippingTip()V
.end method
