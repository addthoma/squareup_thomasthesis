.class public interface abstract annotation Lcom/squareup/x2/settings/X2SettingsScreenTypeKey;
.super Ljava/lang/Object;
.source "X2SettingsScreenTypeKey.java"

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation runtime Ldagger/MapKey;
.end annotation

.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->RUNTIME:Ljava/lang/annotation/RetentionPolicy;
.end annotation


# virtual methods
.method public abstract screenType()Lcom/squareup/x2/settings/ScreenType;
.end method
