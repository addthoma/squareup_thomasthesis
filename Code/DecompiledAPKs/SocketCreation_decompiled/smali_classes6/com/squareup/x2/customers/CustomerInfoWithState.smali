.class public Lcom/squareup/x2/customers/CustomerInfoWithState;
.super Ljava/lang/Object;
.source "CustomerInfoWithState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/x2/customers/CustomerInfoWithState$State;
    }
.end annotation


# instance fields
.field private customerInformation:Lcom/squareup/comms/protos/common/CustomerInformation;

.field private state:Lcom/squareup/x2/customers/CustomerInfoWithState$State;


# direct methods
.method public constructor <init>(Lcom/squareup/comms/protos/common/CustomerInformation;Lcom/squareup/x2/customers/CustomerInfoWithState$State;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/x2/customers/CustomerInfoWithState;->customerInformation:Lcom/squareup/comms/protos/common/CustomerInformation;

    .line 26
    iput-object p2, p0, Lcom/squareup/x2/customers/CustomerInfoWithState;->state:Lcom/squareup/x2/customers/CustomerInfoWithState$State;

    return-void
.end method

.method private toContact(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/comms/protos/common/CustomerInformation;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 9

    if-eqz p1, :cond_0

    .line 39
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Contact;->newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/squareup/protos/client/rolodex/Contact$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/rolodex/Contact$Builder;-><init>()V

    .line 40
    :goto_0
    new-instance v0, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;-><init>()V

    iget-object v1, p2, Lcom/squareup/comms/protos/common/CustomerInformation;->first_name:Ljava/lang/String;

    .line 41
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->given_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/comms/protos/common/CustomerInformation;->last_name:Ljava/lang/String;

    .line 42
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->surname(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/comms/protos/common/CustomerInformation;->email:Ljava/lang/String;

    .line 43
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->email_address(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/comms/protos/common/CustomerInformation;->phone_number:Ljava/lang/String;

    .line 44
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/comms/protos/common/CustomerInformation;->company:Ljava/lang/String;

    .line 45
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->company_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object v0

    new-instance v8, Lcom/squareup/address/Address;

    iget-object v2, p2, Lcom/squareup/comms/protos/common/CustomerInformation;->street_address:Ljava/lang/String;

    iget-object v3, p2, Lcom/squareup/comms/protos/common/CustomerInformation;->apartment:Ljava/lang/String;

    iget-object v4, p2, Lcom/squareup/comms/protos/common/CustomerInformation;->city:Ljava/lang/String;

    iget-object v5, p2, Lcom/squareup/comms/protos/common/CustomerInformation;->state:Ljava/lang/String;

    iget-object v6, p2, Lcom/squareup/comms/protos/common/CustomerInformation;->postal_code:Ljava/lang/String;

    const/4 v7, 0x0

    move-object v1, v8

    invoke-direct/range {v1 .. v7}, Lcom/squareup/address/Address;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;)V

    .line 46
    invoke-static {v8}, Lcom/squareup/crm/util/RolodexProtoHelper;->toGlobalAddress(Lcom/squareup/address/Address;)Lcom/squareup/protos/common/location/GlobalAddress;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object v0

    .line 50
    iget-object p2, p2, Lcom/squareup/comms/protos/common/CustomerInformation;->birthday:Lcom/squareup/comms/protos/common/CustomerInformation$Birthday;

    if-eqz p2, :cond_1

    .line 52
    new-instance v1, Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v2, p2, Lcom/squareup/comms/protos/common/CustomerInformation$Birthday;->year:Ljava/lang/Integer;

    iget v3, p2, Lcom/squareup/comms/protos/common/CustomerInformation$Birthday;->month:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget p2, p2, Lcom/squareup/comms/protos/common/CustomerInformation$Birthday;->day:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-direct {v1, v2, v3, p2}, Lcom/squareup/protos/common/time/YearMonthDay;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->birthday(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    .line 55
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->build()Lcom/squareup/protos/client/rolodex/CustomerProfile;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public getState()Lcom/squareup/x2/customers/CustomerInfoWithState$State;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/x2/customers/CustomerInfoWithState;->state:Lcom/squareup/x2/customers/CustomerInfoWithState$State;

    return-object v0
.end method

.method public updateContact(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/x2/customers/CustomerInfoWithState;->customerInformation:Lcom/squareup/comms/protos/common/CustomerInformation;

    invoke-direct {p0, p1, v0}, Lcom/squareup/x2/customers/CustomerInfoWithState;->toContact(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/comms/protos/common/CustomerInformation;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    return-object p1
.end method
