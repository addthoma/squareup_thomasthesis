.class Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Registration;
.super Ljava/lang/Object;
.source "ActivityVisibilityPresenter.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Registration"
.end annotation


# instance fields
.field final registrant:Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;

.field final synthetic this$0:Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;


# direct methods
.method private constructor <init>(Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;)V
    .locals 0

    .line 77
    iput-object p1, p0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Registration;->this$0:Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p2, p0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Registration;->registrant:Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$1;)V
    .locals 0

    .line 74
    invoke-direct {p0, p1, p2}, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Registration;-><init>(Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    if-eqz p1, :cond_2

    .line 91
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 93
    :cond_1
    check-cast p1, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Registration;

    .line 95
    iget-object v0, p0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Registration;->registrant:Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;

    iget-object p1, p1, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Registration;->registrant:Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Registration;->registrant:Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Registration;->this$0:Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;

    invoke-static {v0}, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->access$100(Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method
