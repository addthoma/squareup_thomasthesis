.class Lcom/squareup/widgets/OnDeleteEditText$CustomInputConnectionWrapper;
.super Landroid/view/inputmethod/InputConnectionWrapper;
.source "OnDeleteEditText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/OnDeleteEditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomInputConnectionWrapper"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/widgets/OnDeleteEditText;


# direct methods
.method constructor <init>(Lcom/squareup/widgets/OnDeleteEditText;Landroid/view/inputmethod/InputConnection;Z)V
    .locals 0

    .line 61
    iput-object p1, p0, Lcom/squareup/widgets/OnDeleteEditText$CustomInputConnectionWrapper;->this$0:Lcom/squareup/widgets/OnDeleteEditText;

    const-string p1, "target"

    .line 62
    invoke-static {p2, p1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/inputmethod/InputConnection;

    invoke-direct {p0, p1, p3}, Landroid/view/inputmethod/InputConnectionWrapper;-><init>(Landroid/view/inputmethod/InputConnection;Z)V

    return-void
.end method


# virtual methods
.method public deleteSurroundingText(II)Z
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/widgets/OnDeleteEditText$CustomInputConnectionWrapper;->this$0:Lcom/squareup/widgets/OnDeleteEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/OnDeleteEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 67
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v1

    .line 68
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/InputConnectionWrapper;->deleteSurroundingText(II)Z

    move-result p1

    .line 69
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result p2

    if-eqz p1, :cond_0

    if-nez v1, :cond_0

    if-nez p2, :cond_0

    .line 71
    iget-object p2, p0, Lcom/squareup/widgets/OnDeleteEditText$CustomInputConnectionWrapper;->this$0:Lcom/squareup/widgets/OnDeleteEditText;

    invoke-static {p2}, Lcom/squareup/widgets/OnDeleteEditText;->access$000(Lcom/squareup/widgets/OnDeleteEditText;)Lcom/squareup/widgets/OnDeleteEditText$OnDeleteKeyListener;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 73
    new-instance p2, Landroid/view/KeyEvent;

    const/4 v0, 0x0

    const/16 v1, 0x43

    invoke-direct {p2, v0, v1}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {p0, p2}, Lcom/squareup/widgets/OnDeleteEditText$CustomInputConnectionWrapper;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    :cond_0
    return p1
.end method
