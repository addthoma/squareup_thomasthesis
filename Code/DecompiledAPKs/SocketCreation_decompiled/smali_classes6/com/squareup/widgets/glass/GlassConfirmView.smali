.class public Lcom/squareup/widgets/glass/GlassConfirmView;
.super Landroid/view/View;
.source "GlassConfirmView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;
    }
.end annotation


# instance fields
.field private cancelListener:Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;

.field private delegateTargeted:Z

.field private delegateView:Landroid/view/View;

.field private delegateViewBounds:Landroid/graphics/Rect;

.field private slop:I

.field private slopBounds:Landroid/graphics/Rect;


# direct methods
.method constructor <init>(Landroid/view/View;Landroid/view/View;Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;)V
    .locals 8

    .line 55
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 57
    iput-object p1, p0, Lcom/squareup/widgets/glass/GlassConfirmView;->delegateView:Landroid/view/View;

    .line 58
    iput-object p3, p0, Lcom/squareup/widgets/glass/GlassConfirmView;->cancelListener:Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;

    const/4 v0, 0x2

    new-array v1, v0, [I

    .line 61
    invoke-virtual {p2, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 62
    new-instance v2, Landroid/graphics/Rect;

    const/4 v3, 0x0

    aget v4, v1, v3

    const/4 v5, 0x1

    aget v6, v1, v5

    aget v3, v1, v3

    .line 63
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v7

    add-int/2addr v3, v7

    aget v1, v1, v5

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result p2

    add-int/2addr v1, p2

    invoke-direct {v2, v4, v6, v3, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, p0, Lcom/squareup/widgets/glass/GlassConfirmView;->delegateViewBounds:Landroid/graphics/Rect;

    .line 65
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result p1

    iput p1, p0, Lcom/squareup/widgets/glass/GlassConfirmView;->slop:I

    .line 66
    new-instance p1, Landroid/graphics/Rect;

    iget-object p2, p0, Lcom/squareup/widgets/glass/GlassConfirmView;->delegateViewBounds:Landroid/graphics/Rect;

    invoke-direct {p1, p2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object p1, p0, Lcom/squareup/widgets/glass/GlassConfirmView;->slopBounds:Landroid/graphics/Rect;

    .line 67
    iget-object p1, p0, Lcom/squareup/widgets/glass/GlassConfirmView;->slopBounds:Landroid/graphics/Rect;

    iget p2, p0, Lcom/squareup/widgets/glass/GlassConfirmView;->slop:I

    neg-int v1, p2

    neg-int p2, p2

    invoke-virtual {p1, v1, p2}, Landroid/graphics/Rect;->inset(II)V

    .line 69
    new-instance p1, Lcom/squareup/widgets/glass/GlassConfirmView$1;

    invoke-direct {p1, p0, p3}, Lcom/squareup/widgets/glass/GlassConfirmView$1;-><init>(Lcom/squareup/widgets/glass/GlassConfirmView;Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;)V

    invoke-virtual {p0, p1}, Lcom/squareup/widgets/glass/GlassConfirmView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/glass/GlassConfirmView;->setImportantForAccessibility(I)V

    return-void
.end method

.method private onDelegateTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .line 97
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 98
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 103
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v2, :cond_2

    if-eq v2, v4, :cond_1

    if-eq v2, v3, :cond_1

    const/4 v6, 0x3

    if-eq v2, v6, :cond_0

    goto :goto_0

    .line 120
    :cond_0
    iget-boolean v2, p0, Lcom/squareup/widgets/glass/GlassConfirmView;->delegateTargeted:Z

    .line 121
    iput-boolean v5, p0, Lcom/squareup/widgets/glass/GlassConfirmView;->delegateTargeted:Z

    goto :goto_1

    .line 112
    :cond_1
    iget-boolean v2, p0, Lcom/squareup/widgets/glass/GlassConfirmView;->delegateTargeted:Z

    if-eqz v2, :cond_4

    .line 114
    iget-object v6, p0, Lcom/squareup/widgets/glass/GlassConfirmView;->slopBounds:Landroid/graphics/Rect;

    invoke-virtual {v6, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-nez v6, :cond_4

    const/4 v6, 0x0

    goto :goto_2

    .line 105
    :cond_2
    iget-object v2, p0, Lcom/squareup/widgets/glass/GlassConfirmView;->delegateViewBounds:Landroid/graphics/Rect;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 106
    iput-boolean v4, p0, Lcom/squareup/widgets/glass/GlassConfirmView;->delegateTargeted:Z

    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v2, 0x0

    :cond_4
    :goto_1
    const/4 v6, 0x1

    :goto_2
    if-eqz v2, :cond_6

    if-eqz v6, :cond_5

    .line 127
    iget-object v2, p0, Lcom/squareup/widgets/glass/GlassConfirmView;->delegateViewBounds:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v2

    int-to-float v0, v0

    iget-object v2, p0, Lcom/squareup/widgets/glass/GlassConfirmView;->delegateViewBounds:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->setLocation(FF)V

    goto :goto_3

    .line 131
    :cond_5
    iget v0, p0, Lcom/squareup/widgets/glass/GlassConfirmView;->slop:I

    mul-int/lit8 v1, v0, 0x2

    neg-int v1, v1

    int-to-float v1, v1

    mul-int/lit8 v0, v0, 0x2

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 133
    :goto_3
    iget-object v0, p0, Lcom/squareup/widgets/glass/GlassConfirmView;->delegateView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_4

    :cond_6
    const/4 v0, 0x0

    .line 136
    :goto_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    if-ne p1, v4, :cond_7

    .line 137
    iput-boolean v5, p0, Lcom/squareup/widgets/glass/GlassConfirmView;->delegateTargeted:Z

    :cond_7
    return v0
.end method


# virtual methods
.method getCancelListener()Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/widgets/glass/GlassConfirmView;->cancelListener:Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;

    return-object v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 79
    invoke-direct {p0, p1}, Lcom/squareup/widgets/glass/GlassConfirmView;->onDelegateTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 82
    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method
