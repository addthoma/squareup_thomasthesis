.class Lcom/squareup/widgets/glass/GlassConfirmController$1;
.super Ljava/lang/Object;
.source "GlassConfirmController.java"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/widgets/glass/GlassConfirmController;->installGlass(Landroid/view/View;Landroid/view/View;Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/widgets/glass/GlassConfirmController;


# direct methods
.method constructor <init>(Lcom/squareup/widgets/glass/GlassConfirmController;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/squareup/widgets/glass/GlassConfirmController$1;->this$0:Lcom/squareup/widgets/glass/GlassConfirmController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 0

    .line 59
    iget-object p1, p0, Lcom/squareup/widgets/glass/GlassConfirmController$1;->this$0:Lcom/squareup/widgets/glass/GlassConfirmController;

    invoke-virtual {p1}, Lcom/squareup/widgets/glass/GlassConfirmController;->maybeCancelAndRemoveGlass()Z

    return-void
.end method
