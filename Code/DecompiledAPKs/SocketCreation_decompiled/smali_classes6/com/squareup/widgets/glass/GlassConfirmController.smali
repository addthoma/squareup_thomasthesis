.class public Lcom/squareup/widgets/glass/GlassConfirmController;
.super Ljava/lang/Object;
.source "GlassConfirmController.java"


# static fields
.field private static instance:Lcom/squareup/widgets/glass/GlassConfirmController;


# instance fields
.field private glassFrame:Lcom/squareup/widgets/glass/GlassConfirmView;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static instance()Lcom/squareup/widgets/glass/GlassConfirmController;
    .locals 1

    .line 19
    sget-object v0, Lcom/squareup/widgets/glass/GlassConfirmController;->instance:Lcom/squareup/widgets/glass/GlassConfirmController;

    if-nez v0, :cond_0

    .line 20
    new-instance v0, Lcom/squareup/widgets/glass/GlassConfirmController;

    invoke-direct {v0}, Lcom/squareup/widgets/glass/GlassConfirmController;-><init>()V

    sput-object v0, Lcom/squareup/widgets/glass/GlassConfirmController;->instance:Lcom/squareup/widgets/glass/GlassConfirmController;

    .line 22
    :cond_0
    sget-object v0, Lcom/squareup/widgets/glass/GlassConfirmController;->instance:Lcom/squareup/widgets/glass/GlassConfirmController;

    return-object v0
.end method


# virtual methods
.method public installGlass(Landroid/view/View;Landroid/view/View;Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;)V
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/widgets/glass/GlassConfirmController;->glassFrame:Lcom/squareup/widgets/glass/GlassConfirmView;

    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {p0}, Lcom/squareup/widgets/glass/GlassConfirmController;->maybeCancelAndRemoveGlass()Z

    .line 53
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 54
    new-instance v1, Lcom/squareup/widgets/glass/GlassConfirmView;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/widgets/glass/GlassConfirmView;-><init>(Landroid/view/View;Landroid/view/View;Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;)V

    iput-object v1, p0, Lcom/squareup/widgets/glass/GlassConfirmController;->glassFrame:Lcom/squareup/widgets/glass/GlassConfirmView;

    .line 55
    iget-object p1, p0, Lcom/squareup/widgets/glass/GlassConfirmController;->glassFrame:Lcom/squareup/widgets/glass/GlassConfirmView;

    new-instance p2, Lcom/squareup/widgets/glass/GlassConfirmController$1;

    invoke-direct {p2, p0}, Lcom/squareup/widgets/glass/GlassConfirmController$1;-><init>(Lcom/squareup/widgets/glass/GlassConfirmController;)V

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/glass/GlassConfirmView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 62
    new-instance p1, Landroid/view/ViewGroup$LayoutParams;

    const/4 p2, -0x1

    invoke-direct {p1, p2, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 63
    iget-object p2, p0, Lcom/squareup/widgets/glass/GlassConfirmController;->glassFrame:Lcom/squareup/widgets/glass/GlassConfirmView;

    invoke-virtual {v0, p2, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public installGlass(Landroid/view/View;Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;)V
    .locals 0

    .line 35
    invoke-virtual {p0, p1, p1, p2}, Lcom/squareup/widgets/glass/GlassConfirmController;->installGlass(Landroid/view/View;Landroid/view/View;Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;)V

    return-void
.end method

.method public maybeCancelAndRemoveGlass()Z
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/widgets/glass/GlassConfirmController;->glassFrame:Lcom/squareup/widgets/glass/GlassConfirmView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/widgets/glass/GlassConfirmView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/squareup/widgets/glass/GlassConfirmController;->glassFrame:Lcom/squareup/widgets/glass/GlassConfirmView;

    invoke-virtual {v0}, Lcom/squareup/widgets/glass/GlassConfirmView;->getCancelListener()Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;->onCancel()V

    .line 94
    invoke-virtual {p0}, Lcom/squareup/widgets/glass/GlassConfirmController;->removeGlass()Z

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public removeGlass()Z
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/squareup/widgets/glass/GlassConfirmController;->glassFrame:Lcom/squareup/widgets/glass/GlassConfirmView;

    if-eqz v0, :cond_1

    .line 73
    invoke-virtual {v0}, Lcom/squareup/widgets/glass/GlassConfirmView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/squareup/widgets/glass/GlassConfirmController;->glassFrame:Lcom/squareup/widgets/glass/GlassConfirmView;

    .line 75
    iput-object v1, p0, Lcom/squareup/widgets/glass/GlassConfirmController;->glassFrame:Lcom/squareup/widgets/glass/GlassConfirmView;

    .line 77
    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 78
    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    const/4 v0, 0x1

    return v0

    .line 81
    :cond_0
    iput-object v1, p0, Lcom/squareup/widgets/glass/GlassConfirmController;->glassFrame:Lcom/squareup/widgets/glass/GlassConfirmView;

    :cond_1
    const/4 v0, 0x0

    return v0
.end method
