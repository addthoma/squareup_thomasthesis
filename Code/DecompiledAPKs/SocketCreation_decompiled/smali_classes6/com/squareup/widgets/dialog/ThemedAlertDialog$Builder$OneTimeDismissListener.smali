.class Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder$OneTimeDismissListener;
.super Ljava/lang/Object;
.source "ThemedAlertDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OneTimeDismissListener"
.end annotation


# instance fields
.field private called:Z

.field private final onDismissListener:Landroid/content/DialogInterface$OnDismissListener;


# direct methods
.method private constructor <init>(Landroid/content/DialogInterface$OnDismissListener;)V
    .locals 0

    .line 332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333
    iput-object p1, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder$OneTimeDismissListener;->onDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/DialogInterface$OnDismissListener;Lcom/squareup/widgets/dialog/ThemedAlertDialog$1;)V
    .locals 0

    .line 328
    invoke-direct {p0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder$OneTimeDismissListener;-><init>(Landroid/content/DialogInterface$OnDismissListener;)V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .line 337
    iget-boolean v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder$OneTimeDismissListener;->called:Z

    if-eqz v0, :cond_0

    return-void

    .line 338
    :cond_0
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder$OneTimeDismissListener;->onDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-interface {v0, p1}, Landroid/content/DialogInterface$OnDismissListener;->onDismiss(Landroid/content/DialogInterface;)V

    const/4 p1, 0x1

    .line 339
    iput-boolean p1, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder$OneTimeDismissListener;->called:Z

    return-void
.end method
