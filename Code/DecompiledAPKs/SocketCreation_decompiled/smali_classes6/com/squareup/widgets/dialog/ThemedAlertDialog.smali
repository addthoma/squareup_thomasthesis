.class public Lcom/squareup/widgets/dialog/ThemedAlertDialog;
.super Lcom/squareup/dialog/GlassAlertDialog;
.source "ThemedAlertDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;
    }
.end annotation


# instance fields
.field private alertController:Lcom/squareup/widgets/dialog/internal/AlertController;


# direct methods
.method private constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/squareup/dialog/GlassAlertDialog;-><init>(Landroid/content/Context;I)V

    .line 40
    new-instance p2, Lcom/squareup/widgets/dialog/internal/AlertController;

    invoke-virtual {p0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-direct {p2, p1, p0, v0}, Lcom/squareup/widgets/dialog/internal/AlertController;-><init>(Landroid/content/Context;Landroid/content/DialogInterface;Landroid/view/Window;)V

    iput-object p2, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog;->alertController:Lcom/squareup/widgets/dialog/internal/AlertController;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;ILcom/squareup/widgets/dialog/ThemedAlertDialog$1;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/widgets/dialog/ThemedAlertDialog;)Lcom/squareup/widgets/dialog/internal/AlertController;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog;->alertController:Lcom/squareup/widgets/dialog/internal/AlertController;

    return-object p0
.end method


# virtual methods
.method public getButton(I)Landroid/widget/Button;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog;->alertController:Lcom/squareup/widgets/dialog/internal/AlertController;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/dialog/internal/AlertController;->getButton(I)Landroid/widget/Button;

    move-result-object p1

    return-object p1
.end method

.method public getListView()Landroid/widget/ListView;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog;->alertController:Lcom/squareup/widgets/dialog/internal/AlertController;

    invoke-virtual {v0}, Lcom/squareup/widgets/dialog/internal/AlertController;->getListView()Landroid/widget/ListView;

    move-result-object v0

    return-object v0
.end method

.method public getMessage()Ljava/lang/CharSequence;
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog;->alertController:Lcom/squareup/widgets/dialog/internal/AlertController;

    invoke-virtual {v0}, Lcom/squareup/widgets/dialog/internal/AlertController;->getMessage()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog;->alertController:Lcom/squareup/widgets/dialog/internal/AlertController;

    invoke-virtual {v0}, Lcom/squareup/widgets/dialog/internal/AlertController;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 46
    iget-object p1, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog;->alertController:Lcom/squareup/widgets/dialog/internal/AlertController;

    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/internal/AlertController;->installContent()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog;->alertController:Lcom/squareup/widgets/dialog/internal/AlertController;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/widgets/dialog/internal/AlertController;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 75
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/squareup/dialog/GlassAlertDialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog;->alertController:Lcom/squareup/widgets/dialog/internal/AlertController;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/widgets/dialog/internal/AlertController;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 80
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/squareup/dialog/GlassAlertDialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 6

    .line 62
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog;->alertController:Lcom/squareup/widgets/dialog/internal/AlertController;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/widgets/dialog/internal/AlertController;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;Z)V

    return-void
.end method

.method public setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V
    .locals 6

    .line 58
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog;->alertController:Lcom/squareup/widgets/dialog/internal/AlertController;

    const/4 v3, 0x0

    const/4 v5, 0x0

    move v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/widgets/dialog/internal/AlertController;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;Z)V

    return-void
.end method

.method public setMessage(Ljava/lang/CharSequence;)V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog;->alertController:Lcom/squareup/widgets/dialog/internal/AlertController;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/dialog/internal/AlertController;->setMessage(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/widgets/dialog/ThemedAlertDialog;->alertController:Lcom/squareup/widgets/dialog/internal/AlertController;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/dialog/internal/AlertController;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method
