.class public Lcom/squareup/widgets/list/FixedViewListAdapter$Row;
.super Ljava/lang/Object;
.source "FixedViewListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/list/FixedViewListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Row"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final isSeparator:Z

.field public final payload:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public final view:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(ZLandroid/view/View;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZTV;TT;)V"
        }
    .end annotation

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-boolean p1, p0, Lcom/squareup/widgets/list/FixedViewListAdapter$Row;->isSeparator:Z

    .line 80
    iput-object p2, p0, Lcom/squareup/widgets/list/FixedViewListAdapter$Row;->view:Landroid/view/View;

    .line 81
    iput-object p3, p0, Lcom/squareup/widgets/list/FixedViewListAdapter$Row;->payload:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/widgets/list/FixedViewListAdapter$Row;)Z
    .locals 0

    .line 73
    iget-boolean p0, p0, Lcom/squareup/widgets/list/FixedViewListAdapter$Row;->isSeparator:Z

    return p0
.end method

.method public static item(Landroid/view/View;)Lcom/squareup/widgets/list/FixedViewListAdapter$Row;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "V:",
            "Landroid/view/View;",
            ">(TV;)",
            "Lcom/squareup/widgets/list/FixedViewListAdapter$Row<",
            "TT;TV;>;"
        }
    .end annotation

    .line 95
    new-instance v0, Lcom/squareup/widgets/list/FixedViewListAdapter$Row;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/squareup/widgets/list/FixedViewListAdapter$Row;-><init>(ZLandroid/view/View;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static item(Ljava/lang/Object;Landroid/view/View;)Lcom/squareup/widgets/list/FixedViewListAdapter$Row;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "V:",
            "Landroid/view/View;",
            ">(TT;TV;)",
            "Lcom/squareup/widgets/list/FixedViewListAdapter$Row<",
            "TT;TV;>;"
        }
    .end annotation

    .line 91
    new-instance v0, Lcom/squareup/widgets/list/FixedViewListAdapter$Row;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p1, p0}, Lcom/squareup/widgets/list/FixedViewListAdapter$Row;-><init>(ZLandroid/view/View;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static separator(Landroid/view/View;)Lcom/squareup/widgets/list/FixedViewListAdapter$Row;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "V:",
            "Landroid/view/View;",
            ">(TV;)",
            "Lcom/squareup/widgets/list/FixedViewListAdapter$Row<",
            "TT;TV;>;"
        }
    .end annotation

    .line 86
    new-instance v0, Lcom/squareup/widgets/list/FixedViewListAdapter$Row;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/squareup/widgets/list/FixedViewListAdapter$Row;-><init>(ZLandroid/view/View;Ljava/lang/Object;)V

    return-object v0
.end method
