.class public final Lcom/squareup/widgets/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final add_color:I = 0x7f06001b

.field public static final add_color_disabled:I = 0x7f06001c

.field public static final backspace_disabled:I = 0x7f060026

.field public static final button_disabled:I = 0x7f060033

.field public static final button_normal:I = 0x7f060036

.field public static final button_pressed:I = 0x7f060037

.field public static final check_color:I = 0x7f06005d

.field public static final check_color_bg:I = 0x7f06005e

.field public static final check_color_bg_actived:I = 0x7f06005f

.field public static final check_color_bg_disabled:I = 0x7f060060

.field public static final check_color_disabled:I = 0x7f060061

.field public static final default_title_indicator_footer_color:I = 0x7f060082

.field public static final default_title_indicator_selected_color:I = 0x7f060083

.field public static final default_title_indicator_text_color:I = 0x7f060084

.field public static final default_underline_indicator_selected_color:I = 0x7f060085

.field public static final letters_text_color:I = 0x7f060127

.field public static final letters_text_color_disabled:I = 0x7f060128

.field public static final line_color:I = 0x7f06012d

.field public static final pin_submit_color:I = 0x7f06028e

.field public static final skip_color_bg:I = 0x7f0602d7

.field public static final skip_color_bg_pressed:I = 0x7f0602d8

.field public static final text_color:I = 0x7f0602f4

.field public static final text_color_disabled:I = 0x7f0602f5

.field public static final vpi__background_holo_dark:I = 0x7f06030e

.field public static final vpi__background_holo_light:I = 0x7f06030f

.field public static final vpi__bright_foreground_disabled_holo_dark:I = 0x7f060310

.field public static final vpi__bright_foreground_disabled_holo_light:I = 0x7f060311

.field public static final vpi__bright_foreground_holo_dark:I = 0x7f060312

.field public static final vpi__bright_foreground_holo_light:I = 0x7f060313

.field public static final vpi__bright_foreground_inverse_holo_dark:I = 0x7f060314

.field public static final vpi__bright_foreground_inverse_holo_light:I = 0x7f060315


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
