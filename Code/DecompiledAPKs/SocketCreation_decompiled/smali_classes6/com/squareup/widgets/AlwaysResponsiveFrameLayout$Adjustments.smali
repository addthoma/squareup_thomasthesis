.class Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;
.super Ljava/lang/Object;
.source "AlwaysResponsiveFrameLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/AlwaysResponsiveFrameLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Adjustments"
.end annotation


# instance fields
.field private final originalLeftPadding:I

.field private final originalRighttPadding:I

.field private final targetChildWidth:I


# direct methods
.method constructor <init>(ILandroid/view/View;)V
    .locals 0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput p1, p0, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;->targetChildWidth:I

    .line 73
    invoke-virtual {p2}, Landroid/view/View;->getPaddingLeft()I

    move-result p1

    iput p1, p0, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;->originalLeftPadding:I

    .line 74
    invoke-virtual {p2}, Landroid/view/View;->getPaddingRight()I

    move-result p1

    iput p1, p0, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;->originalRighttPadding:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;)I
    .locals 0

    .line 66
    iget p0, p0, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;->targetChildWidth:I

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;)I
    .locals 0

    .line 66
    iget p0, p0, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;->originalLeftPadding:I

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;)I
    .locals 0

    .line 66
    iget p0, p0, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;->originalRighttPadding:I

    return p0
.end method
