.class public abstract Lcom/squareup/utilities/ui/DeviceModule;
.super Ljava/lang/Object;
.source "DeviceModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindDevice(Lcom/squareup/utilities/ui/RealDevice;)Lcom/squareup/util/Device;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
