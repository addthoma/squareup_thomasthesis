.class public final Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;
.super Ljava/lang/Object;
.source "AbstractWorkflowViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0096\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002Jb\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u00060\u0004\"\u0008\u0008\u0000\u0010\u0005*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0006*\u00020\u00012\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u00060\u00082$\u0010\t\u001a \u0012\u0016\u0012\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u00060\u000c0\u000b\u0012\u0004\u0012\u00020\r0\nH\u0007JZ\u0010\u0003\u001a\u0018\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u00020\u00100\u000ej\u0008\u0012\u0004\u0012\u0002H\u000f`\u0011\"\u0008\u0008\u0000\u0010\u000f*\u00020\u00122\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u000f0\u00142$\u0010\t\u001a \u0012\u0016\u0012\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u00020\u00100\u000c0\u000b\u0012\u0004\u0012\u00020\r0\nJ\u0080\u0001\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u00060\u0004\"\u0008\u0008\u0000\u0010\u0005*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0006*\u00020\u00012\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u00060\u00082\u0008\u0008\u0001\u0010\u0016\u001a\u00020\u00172\u0008\u0008\u0002\u0010\u0018\u001a\u00020\u00192\u0008\u0008\u0002\u0010\u001a\u001a\u00020\u001b2$\u0010\u001c\u001a \u0012\u0016\u0012\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u00060\u000c0\u000b\u0012\u0004\u0012\u00020\u001d0\nH\u0007Jl\u0010\u0015\u001a\u0018\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u00020\u00100\u001ej\u0008\u0012\u0004\u0012\u0002H\u000f`\u001f\"\u0008\u0008\u0000\u0010\u000f*\u00020\u00122\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u000f0\u00142\u0008\u0008\u0001\u0010\u0016\u001a\u00020\u00172\u0008\u0008\u0002\u0010\u0018\u001a\u00020\u00192\u0008\u0008\u0002\u0010\u001a\u001a\u00020\u001b2\u0018\u0010 \u001a\u0014\u0012\u0004\u0012\u00020!\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u000f0\"0\nJ\u00cc\u0001\u0010#\u001a\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u00060\u0004\"\u0008\u0008\u0000\u0010\u0005*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0006*\u00020\u00012\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u00060\u00082\u0008\u0008\u0002\u0010\u0018\u001a\u00020\u00192\u0085\u0001\u0010$\u001a\u0080\u0001\u0012%\u0012#\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u00060\u000c0\u000b\u00a2\u0006\u000c\u0008&\u0012\u0008\u0008\'\u0012\u0004\u0008\u0008((\u0012\u0013\u0012\u00110)\u00a2\u0006\u000c\u0008&\u0012\u0008\u0008\'\u0012\u0004\u0008\u0008(*\u0012\u0015\u0012\u0013\u0018\u00010+\u00a2\u0006\u000c\u0008&\u0012\u0008\u0008\'\u0012\u0004\u0008\u0008(,\u0012\u0013\u0012\u00110-\u00a2\u0006\u000c\u0008&\u0012\u0008\u0008\'\u0012\u0004\u0008\u0008(.\u0012\u0004\u0012\u00020!0%j\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u0006`/\u00a8\u00060"
    }
    d2 = {
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;",
        "",
        "()V",
        "bindDialog",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;",
        "D",
        "E",
        "key",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "dialogForScreen",
        "Lkotlin/Function1;",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/DialogFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;",
        "RenderingT",
        "",
        "Lcom/squareup/workflow/V2DialogBinding;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "type",
        "Lkotlin/reflect/KClass;",
        "bindLayout",
        "layoutId",
        "",
        "hint",
        "Lcom/squareup/workflow/ScreenHint;",
        "inflater",
        "Lcom/squareup/workflow/InflaterDelegate;",
        "coordinatorForScreens",
        "Lcom/squareup/coordinators/Coordinator;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;",
        "Lcom/squareup/workflow/V2LayoutBinding;",
        "runnerConstructor",
        "Landroid/view/View;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "bindViewBuilder",
        "viewBuilder",
        "Lkotlin/Function4;",
        "Lkotlin/ParameterName;",
        "name",
        "screens",
        "Landroid/content/Context;",
        "contextForNewView",
        "Landroid/view/ViewGroup;",
        "container",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "containerHints",
        "Lcom/squareup/workflow/ViewBuilder;",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 241
    invoke-direct {p0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;-><init>()V

    return-void
.end method

.method public static synthetic bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;
    .locals 6

    and-int/lit8 p7, p6, 0x4

    if-eqz p7, :cond_0

    .line 270
    new-instance p3, Lcom/squareup/workflow/ScreenHint;

    invoke-direct {p3}, Lcom/squareup/workflow/ScreenHint;-><init>()V

    :cond_0
    move-object v3, p3

    and-int/lit8 p3, p6, 0x8

    if-eqz p3, :cond_1

    .line 271
    sget-object p3, Lcom/squareup/workflow/InflaterDelegate$Real;->INSTANCE:Lcom/squareup/workflow/InflaterDelegate$Real;

    move-object p4, p3

    check-cast p4, Lcom/squareup/workflow/InflaterDelegate;

    :cond_1
    move-object v4, p4

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout(Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lkotlin/reflect/KClass;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;
    .locals 6

    and-int/lit8 p7, p6, 0x4

    if-eqz p7, :cond_0

    .line 258
    new-instance p3, Lcom/squareup/workflow/ScreenHint;

    invoke-direct {p3}, Lcom/squareup/workflow/ScreenHint;-><init>()V

    :cond_0
    move-object v3, p3

    and-int/lit8 p3, p6, 0x8

    if-eqz p3, :cond_1

    .line 259
    sget-object p3, Lcom/squareup/workflow/InflaterDelegate$Real;->INSTANCE:Lcom/squareup/workflow/InflaterDelegate$Real;

    move-object p4, p3

    check-cast p4, Lcom/squareup/workflow/InflaterDelegate;

    :cond_1
    move-object v4, p4

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout(Lkotlin/reflect/KClass;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic bindViewBuilder$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/ScreenHint;Lkotlin/jvm/functions/Function4;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    .line 250
    new-instance p2, Lcom/squareup/workflow/ScreenHint;

    invoke-direct {p2}, Lcom/squareup/workflow/ScreenHint;-><init>()V

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindViewBuilder(Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/ScreenHint;Lkotlin/jvm/functions/Function4;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "TD;TE;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "TD;TE;>;>;+",
            "Lcom/squareup/workflow/DialogFactory;",
            ">;)",
            "Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding<",
            "TD;TE;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use V2Screen class binding instead of Screen.KEY."
    .end annotation

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dialogForScreen"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 283
    new-instance v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealDialogBinding;

    invoke-direct {v0, p1, p2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealDialogBinding;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    return-object v0
.end method

.method public final bindDialog(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<RenderingT::",
            "Lcom/squareup/workflow/legacy/V2Screen;",
            ">(",
            "Lkotlin/reflect/KClass<",
            "TRenderingT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;+",
            "Lcom/squareup/workflow/DialogFactory;",
            ">;)",
            "Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;"
        }
    .end annotation

    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dialogForScreen"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 288
    new-instance v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealDialogBinding;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p1, v1, v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p1

    invoke-direct {v0, p1, p2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealDialogBinding;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    return-object v0
.end method

.method public final bindLayout(Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "TD;TE;>;I",
            "Lcom/squareup/workflow/ScreenHint;",
            "Lcom/squareup/workflow/InflaterDelegate;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "TD;TE;>;>;+",
            "Lcom/squareup/coordinators/Coordinator;",
            ">;)",
            "Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding<",
            "TD;TE;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use LayoutRunner instead of Coordinator."
    .end annotation

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hint"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inflater"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "coordinatorForScreens"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 274
    new-instance v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;

    new-instance v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion$bindLayout$2;

    invoke-direct {v1, p5}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion$bindLayout$2;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object v6, v1

    check-cast v6, Lkotlin/jvm/functions/Function2;

    move-object v1, v0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    return-object v0
.end method

.method public final bindLayout(Lkotlin/reflect/KClass;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<RenderingT::",
            "Lcom/squareup/workflow/legacy/V2Screen;",
            ">(",
            "Lkotlin/reflect/KClass<",
            "TRenderingT;>;I",
            "Lcom/squareup/workflow/ScreenHint;",
            "Lcom/squareup/workflow/InflaterDelegate;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "+",
            "Lcom/squareup/workflow/ui/LayoutRunner<",
            "TRenderingT;>;>;)",
            "Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;"
        }
    .end annotation

    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hint"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inflater"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "runnerConstructor"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 262
    new-instance v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p1, v1, v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    new-instance p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion$bindLayout$1;

    invoke-direct {p1, p5}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion$bindLayout$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function2;

    move-object v1, v0

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    return-object v0
.end method

.method public final bindViewBuilder(Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/ScreenHint;Lkotlin/jvm/functions/Function4;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "TD;TE;>;",
            "Lcom/squareup/workflow/ScreenHint;",
            "Lkotlin/jvm/functions/Function4<",
            "-",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "TD;TE;>;>;-",
            "Landroid/content/Context;",
            "-",
            "Landroid/view/ViewGroup;",
            "-",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "+",
            "Landroid/view/View;",
            ">;)",
            "Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding<",
            "TD;TE;>;"
        }
    .end annotation

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hint"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewBuilder"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 253
    new-instance v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$ViewBuilderBinding;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$ViewBuilderBinding;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/ScreenHint;Lkotlin/jvm/functions/Function4;)V

    check-cast v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    return-object v0
.end method
