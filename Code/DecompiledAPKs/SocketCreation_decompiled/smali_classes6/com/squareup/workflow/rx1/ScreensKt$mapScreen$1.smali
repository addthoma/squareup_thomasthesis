.class public final Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1;
.super Ljava/lang/Object;
.source "Screens.kt"

# interfaces
.implements Lcom/squareup/workflow/rx1/Workflow;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/rx1/ScreensKt;->mapScreen(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/rx1/Workflow<",
        "Lcom/squareup/workflow/ScreenState<",
        "+TR2;>;TE;TO;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000+\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0004*\u0001\u0000\u0008\n\u0018\u000020\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\u0002\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0001j\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002`\u0003J\u0008\u0010\r\u001a\u00020\u000eH\u0016J\u0015\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00028\u0001H\u0016\u00a2\u0006\u0002\u0010\u0011R\u001c\u0010\u0004\u001a\n\u0012\u0006\u0008\u0001\u0012\u00028\u00020\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007RP\u0010\u0008\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00028\u0000 \n*\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u00020\u0002 \n*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00028\u0000 \n*\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u00020\u0002\u0018\u00010\t0\tX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0012"
    }
    d2 = {
        "com/squareup/workflow/rx1/ScreensKt$mapScreen$1",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "Lcom/squareup/workflow/ScreenState;",
        "Lcom/squareup/workflow/rx1/ScreenWorkflow;",
        "result",
        "Lrx/Single;",
        "getResult",
        "()Lrx/Single;",
        "state",
        "Lrx/Observable;",
        "kotlin.jvm.PlatformType",
        "getState",
        "()Lrx/Observable;",
        "abandon",
        "",
        "sendEvent",
        "event",
        "(Ljava/lang/Object;)V",
        "pure-rx1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $mapper:Lkotlin/jvm/functions/Function1;

.field final synthetic $this_mapScreen:Lcom/squareup/workflow/rx1/Workflow;

.field private final state:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/squareup/workflow/ScreenState<",
            "TR2;>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "+",
            "Lcom/squareup/workflow/ScreenState<",
            "+TR1;>;-TE;+TO;>;",
            "Lkotlin/jvm/functions/Function1;",
            ")V"
        }
    .end annotation

    .line 41
    iput-object p1, p0, Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1;->$this_mapScreen:Lcom/squareup/workflow/rx1/Workflow;

    iput-object p2, p0, Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1;->$mapper:Lkotlin/jvm/functions/Function1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iget-object p1, p0, Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1;->$this_mapScreen:Lcom/squareup/workflow/rx1/Workflow;

    invoke-interface {p1}, Lcom/squareup/workflow/rx1/Workflow;->getState()Lrx/Observable;

    move-result-object p1

    new-instance p2, Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1$state$1;

    invoke-direct {p2, p0}, Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1$state$1;-><init>(Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1;)V

    check-cast p2, Lrx/functions/Func1;

    invoke-virtual {p1, p2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1;->state:Lrx/Observable;

    return-void
.end method


# virtual methods
.method public abandon()V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1;->$this_mapScreen:Lcom/squareup/workflow/rx1/Workflow;

    invoke-interface {v0}, Lcom/squareup/workflow/rx1/Workflow;->abandon()V

    return-void
.end method

.method public getResult()Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Single<",
            "+TO;>;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1;->$this_mapScreen:Lcom/squareup/workflow/rx1/Workflow;

    invoke-interface {v0}, Lcom/squareup/workflow/rx1/Workflow;->getResult()Lrx/Single;

    move-result-object v0

    return-object v0
.end method

.method public getState()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/workflow/ScreenState<",
            "TR2;>;>;"
        }
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1;->state:Lrx/Observable;

    return-object v0
.end method

.method public sendEvent(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1;->$this_mapScreen:Lcom/squareup/workflow/rx1/Workflow;

    invoke-interface {v0, p1}, Lcom/squareup/workflow/rx1/Workflow;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public toCompletable()Lrx/Completable;
    .locals 1

    .line 41
    invoke-static {p0}, Lcom/squareup/workflow/rx1/Workflow$DefaultImpls;->toCompletable(Lcom/squareup/workflow/rx1/Workflow;)Lrx/Completable;

    move-result-object v0

    return-object v0
.end method
