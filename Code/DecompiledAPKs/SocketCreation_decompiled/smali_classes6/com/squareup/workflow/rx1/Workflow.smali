.class public interface abstract Lcom/squareup/workflow/rx1/Workflow;
.super Ljava/lang/Object;
.source "Workflow.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/WorkflowInput;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/rx1/Workflow$DefaultImpls;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        "O:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/WorkflowInput<",
        "TE;>;"
    }
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "Use com.squareup.workflow.Workflow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u0000*\n\u0008\u0000\u0010\u0001 \u0001*\u00020\u0002*\n\u0008\u0001\u0010\u0003 \u0000*\u00020\u0002*\n\u0008\u0002\u0010\u0004 \u0001*\u00020\u00022\u0008\u0012\u0004\u0012\u0002H\u00030\u0005J\u0008\u0010\u000e\u001a\u00020\u000fH\u0016J\u0008\u0010\u0010\u001a\u00020\u0011H\u0016R\u001a\u0010\u0006\u001a\n\u0012\u0006\u0008\u0001\u0012\u00028\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR\u001a\u0010\n\u001a\n\u0012\u0006\u0008\u0001\u0012\u00028\u00000\u000bX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/workflow/rx1/Workflow;",
        "S",
        "",
        "E",
        "O",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "result",
        "Lrx/Single;",
        "getResult",
        "()Lrx/Single;",
        "state",
        "Lrx/Observable;",
        "getState",
        "()Lrx/Observable;",
        "abandon",
        "",
        "toCompletable",
        "Lrx/Completable;",
        "pure-rx1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract abandon()V
.end method

.method public abstract getResult()Lrx/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Single<",
            "+TO;>;"
        }
    .end annotation
.end method

.method public abstract getState()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "+TS;>;"
        }
    .end annotation
.end method

.method public abstract toCompletable()Lrx/Completable;
.end method
