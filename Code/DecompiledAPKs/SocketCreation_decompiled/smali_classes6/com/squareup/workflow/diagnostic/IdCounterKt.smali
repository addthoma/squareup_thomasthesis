.class public final Lcom/squareup/workflow/diagnostic/IdCounterKt;
.super Ljava/lang/Object;
.source "IdCounter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\u001a\u000f\u0010\u0000\u001a\u00020\u0001*\u0004\u0018\u00010\u0002H\u0080\u0008\u00a8\u0006\u0003"
    }
    d2 = {
        "createId",
        "",
        "Lcom/squareup/workflow/diagnostic/IdCounter;",
        "workflow-runtime"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final createId(Lcom/squareup/workflow/diagnostic/IdCounter;)J
    .locals 2

    if-eqz p0, :cond_0

    .line 14
    invoke-virtual {p0}, Lcom/squareup/workflow/diagnostic/IdCounter;->createId()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method
