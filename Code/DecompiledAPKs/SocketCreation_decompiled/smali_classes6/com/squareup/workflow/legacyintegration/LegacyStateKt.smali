.class public final Lcom/squareup/workflow/legacyintegration/LegacyStateKt;
.super Ljava/lang/Object;
.source "LegacyState.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLegacyState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LegacyState.kt\ncom/squareup/workflow/legacyintegration/LegacyStateKt\n+ 2 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPool$Companion\n+ 3 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPoolKt\n*L\n1#1,144:1\n76#1,4:147\n76#1,4:153\n97#1:159\n76#1,4:160\n335#2:145\n335#2:151\n335#2:157\n335#2:164\n412#3:146\n412#3:152\n412#3:158\n412#3:165\n*E\n*S KotlinDebug\n*F\n+ 1 LegacyState.kt\ncom/squareup/workflow/legacyintegration/LegacyStateKt\n*L\n97#1,4:153\n76#1:145\n97#1:157\n76#1:146\n97#1:158\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u0082\u0001\u0010\u0000\u001a6\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00050\u0001j\u0014\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u0004`\u0006\"\n\u0008\u0000\u0010\u0003\u0018\u0001*\u00020\u0007\"\n\u0008\u0001\u0010\u0005\u0018\u0001*\u00020\u0007\"\n\u0008\u0002\u0010\u0004\u0018\u0001*\u00020\u00072\u0006\u0010\u0008\u001a\u0002H\u00032\u0008\u0010\t\u001a\u0004\u0018\u00010\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000cH\u0086\u0008\u00a2\u0006\u0002\u0010\r\u001ai\u0010\u0000\u001a6\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u0002H\u00040\u0002\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u0002H\u00050\u0001j\u0014\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u0004`\u0006\"\n\u0008\u0000\u0010\u0005\u0018\u0001*\u00020\u0007\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u00020\u00072\u0008\u0010\t\u001a\u0004\u0018\u00010\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000cH\u0086\u0008\u001a7\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0002\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0007\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0007*\u00020\u00102\u0006\u0010\u0008\u001a\u0002H\u0003\u00a2\u0006\u0002\u0010\u0011\u001a\u001a\u0010\u0012\u001a\u00020\u000e*\u00020\u00132\u000e\u0010\u0014\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0002*X\u0010\u0000\u001a\u0004\u0008\u0000\u0010\u0003\u001a\u0004\u0008\u0001\u0010\u0005\u001a\u0004\u0008\u0002\u0010\u0004\" \u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00050\u00012 \u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00050\u0001\u00a8\u0006\u0015"
    }
    d2 = {
        "LegacyHandle",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "InputT",
        "RenderingT",
        "OutputT",
        "Lcom/squareup/workflow/legacyintegration/LegacyHandle;",
        "",
        "input",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "name",
        "",
        "(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "",
        "readLegacyState",
        "Lokio/BufferedSource;",
        "(Lokio/BufferedSource;Ljava/lang/Object;)Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "writeLegacyState",
        "Lokio/BufferedSink;",
        "state",
        "v2-legacy-integration"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic LegacyHandle(Lcom/squareup/workflow/Snapshot;Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Snapshot;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lkotlin/Unit;",
            "TRenderingT;>;",
            "Lkotlin/Unit;",
            "TOutputT;>;"
        }
    .end annotation

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    sget-object v2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 153
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowPool;->Companion:Lcom/squareup/workflow/legacy/WorkflowPool$Companion;

    .line 154
    new-instance v0, Lcom/squareup/workflow/legacyintegration/FakeKClass;

    invoke-direct {v0}, Lcom/squareup/workflow/legacyintegration/FakeKClass;-><init>()V

    check-cast v0, Lkotlin/reflect/KClass;

    .line 155
    new-instance v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, v0

    move-object v4, p0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/workflow/legacyintegration/LegacyState;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 158
    new-instance p0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v1, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-class v2, Lkotlin/Unit;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const/4 v3, 0x4

    const-string v4, "OutputT"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v3, Ljava/lang/Object;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 157
    invoke-virtual {p0, p1}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->makeWorkflowId(Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object p0

    new-instance p1, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-direct {p1, p0, v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)V

    return-object p1
.end method

.method public static final synthetic LegacyHandle(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<InputT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            ">(TInputT;",
            "Lcom/squareup/workflow/Snapshot;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "TInputT;TRenderingT;>;TInputT;TOutputT;>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowPool;->Companion:Lcom/squareup/workflow/legacy/WorkflowPool$Companion;

    .line 77
    new-instance v0, Lcom/squareup/workflow/legacyintegration/FakeKClass;

    invoke-direct {v0}, Lcom/squareup/workflow/legacyintegration/FakeKClass;-><init>()V

    check-cast v0, Lkotlin/reflect/KClass;

    .line 78
    new-instance v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, v0

    move-object v2, p0

    move-object v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/workflow/legacyintegration/LegacyState;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 146
    new-instance p0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class p1, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    const/4 v1, 0x4

    const-string v2, "InputT"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v2, Ljava/lang/Object;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "OutputT"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Ljava/lang/Object;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-direct {p0, p1, v2, v1}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 145
    invoke-virtual {p0, p2}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->makeWorkflowId(Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object p0

    new-instance p1, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-direct {p1, p0, v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)V

    return-object p1
.end method

.method public static synthetic LegacyHandle$default(Lcom/squareup/workflow/Snapshot;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 6

    and-int/lit8 p2, p2, 0x2

    if-eqz p2, :cond_0

    const-string p1, ""

    :cond_0
    const-string p2, "name"

    .line 96
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 160
    sget-object p2, Lcom/squareup/workflow/legacy/WorkflowPool;->Companion:Lcom/squareup/workflow/legacy/WorkflowPool$Companion;

    .line 161
    new-instance p2, Lcom/squareup/workflow/legacyintegration/FakeKClass;

    invoke-direct {p2}, Lcom/squareup/workflow/legacyintegration/FakeKClass;-><init>()V

    check-cast p2, Lkotlin/reflect/KClass;

    .line 162
    new-instance p2, Lcom/squareup/workflow/legacyintegration/LegacyState;

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p2

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/squareup/workflow/legacyintegration/LegacyState;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 165
    new-instance p0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class p3, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    const-class v0, Lkotlin/Unit;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v1, 0x4

    const-string v2, "OutputT"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Ljava/lang/Object;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-direct {p0, p3, v0, v1}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 164
    invoke-virtual {p0, p1}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->makeWorkflowId(Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object p0

    new-instance p1, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-direct {p1, p0, p2}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)V

    return-object p1
.end method

.method public static synthetic LegacyHandle$default(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 6

    const/4 p4, 0x4

    and-int/2addr p3, p4

    if-eqz p3, :cond_0

    const-string p2, ""

    :cond_0
    const-string p3, "input"

    .line 74
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "name"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowPool;->Companion:Lcom/squareup/workflow/legacy/WorkflowPool$Companion;

    .line 148
    new-instance p3, Lcom/squareup/workflow/legacyintegration/FakeKClass;

    invoke-direct {p3}, Lcom/squareup/workflow/legacyintegration/FakeKClass;-><init>()V

    check-cast p3, Lkotlin/reflect/KClass;

    .line 149
    new-instance p3, Lcom/squareup/workflow/legacyintegration/LegacyState;

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/workflow/legacyintegration/LegacyState;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 152
    new-instance p0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class p1, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    const-string v0, "InputT"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v0, Ljava/lang/Object;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v1, "OutputT"

    invoke-static {p4, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class p4, Ljava/lang/Object;

    invoke-static {p4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p4

    invoke-direct {p0, p1, v0, p4}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 151
    invoke-virtual {p0, p2}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->makeWorkflowId(Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object p0

    new-instance p1, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-direct {p1, p0, p3}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)V

    return-object p1
.end method

.method public static final readLegacyState(Lokio/BufferedSource;Ljava/lang/Object;)Lcom/squareup/workflow/legacyintegration/LegacyState;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<InputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lokio/BufferedSource;",
            "TInputT;)",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "TInputT;TRenderingT;>;"
        }
    .end annotation

    const-string v0, "$this$readLegacyState"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    new-instance v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    .line 143
    sget-object v1, Lcom/squareup/workflow/legacyintegration/LegacyStateKt$readLegacyState$1;->INSTANCE:Lcom/squareup/workflow/legacyintegration/LegacyStateKt$readLegacyState$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v1}, Lcom/squareup/workflow/SnapshotKt;->readNullable(Lokio/BufferedSource;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;

    move-result-object p0

    move-object v4, p0

    check-cast v4, Lcom/squareup/workflow/Snapshot;

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, v0

    move-object v2, p1

    .line 142
    invoke-direct/range {v1 .. v6}, Lcom/squareup/workflow/legacyintegration/LegacyState;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public static final writeLegacyState(Lokio/BufferedSink;Lcom/squareup/workflow/legacyintegration/LegacyState;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lokio/BufferedSink;",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "**>;)V"
        }
    .end annotation

    const-string v0, "$this$writeLegacyState"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    sget-object v0, Lcom/squareup/workflow/legacyintegration/LegacyStateKt$writeLegacyState$1;->INSTANCE:Lcom/squareup/workflow/legacyintegration/LegacyStateKt$writeLegacyState$1;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-static {p0, p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeNullable(Lokio/BufferedSink;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Lokio/BufferedSink;

    return-void
.end method
