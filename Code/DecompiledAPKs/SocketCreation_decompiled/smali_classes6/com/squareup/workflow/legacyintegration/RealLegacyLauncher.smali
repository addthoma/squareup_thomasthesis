.class public final Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher;
.super Ljava/lang/Object;
.source "LegacyLauncher.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<InputT:",
        "Ljava/lang/Object;",
        "OutputT:",
        "Ljava/lang/Object;",
        "RenderingT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
        "Lcom/squareup/workflow/legacyintegration/LegacyState<",
        "TInputT;TRenderingT;>;TInputT;TOutputT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u0002*\u0008\u0008\u0002\u0010\u0004*\u00020\u00022 \u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00040\u0006\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00030\u0005B\'\u0012\u0018\u0010\u0007\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJB\u0010\u000c\u001a \u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00020\u0006\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\r2\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00020\u00062\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0007\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher;",
        "InputT",
        "",
        "OutputT",
        "RenderingT",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "workflow",
        "Lcom/squareup/workflow/Workflow;",
        "context",
        "Lkotlin/coroutines/CoroutineContext;",
        "(Lcom/squareup/workflow/Workflow;Lkotlin/coroutines/CoroutineContext;)V",
        "launch",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "initialState",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "v2-legacy-integration"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final context:Lkotlin/coroutines/CoroutineContext;

.field private final workflow:Lcom/squareup/workflow/Workflow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Workflow<",
            "TInputT;TOutputT;TRenderingT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/Workflow;Lkotlin/coroutines/CoroutineContext;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Workflow<",
            "-TInputT;+TOutputT;+TRenderingT;>;",
            "Lkotlin/coroutines/CoroutineContext;",
            ")V"
        }
    .end annotation

    const-string v0, "workflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher;->workflow:Lcom/squareup/workflow/Workflow;

    iput-object p2, p0, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher;->context:Lkotlin/coroutines/CoroutineContext;

    return-void
.end method


# virtual methods
.method public launch(Lcom/squareup/workflow/legacyintegration/LegacyState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "TInputT;TRenderingT;>;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "TInputT;TRenderingT;>;TInputT;TOutputT;>;"
        }
    .end annotation

    const-string v0, "initialState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflows"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getInput()Ljava/lang/Object;

    move-result-object p2

    const v0, 0x7fffffff

    .line 98
    invoke-static {v0}, Lkotlinx/coroutines/channels/ChannelKt;->Channel(I)Lkotlinx/coroutines/channels/Channel;

    move-result-object v0

    .line 99
    new-instance v1, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$inputsFlow$1;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$inputsFlow$1;-><init>(Lkotlinx/coroutines/channels/Channel;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-static {v1}, Lkotlinx/coroutines/flow/FlowKt;->flow(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v1

    .line 102
    invoke-interface {v0, p2}, Lkotlinx/coroutines/channels/Channel;->offer(Ljava/lang/Object;)Z

    .line 104
    iget-object v2, p0, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher;->context:Lkotlin/coroutines/CoroutineContext;

    invoke-static {v2}, Lkotlinx/coroutines/CoroutineScopeKt;->CoroutineScope(Lkotlin/coroutines/CoroutineContext;)Lkotlinx/coroutines/CoroutineScope;

    move-result-object v2

    .line 107
    iget-object v3, p0, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher;->workflow:Lcom/squareup/workflow/Workflow;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    .line 108
    new-instance v4, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1;

    invoke-direct {v4, p2, v2, v0}, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1;-><init>(Ljava/lang/Object;Lkotlinx/coroutines/CoroutineScope;Lkotlinx/coroutines/channels/Channel;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    .line 106
    invoke-static {v2, v3, v1, p1, v4}, Lcom/squareup/workflow/LaunchWorkflowKt;->launchWorkflowIn(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Workflow;

    return-object p1
.end method

.method public bridge synthetic launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 0

    .line 83
    check-cast p1, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher;->launch(Lcom/squareup/workflow/legacyintegration/LegacyState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method
