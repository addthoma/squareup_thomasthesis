.class final Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "LegacyWorkflowAdapter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;->render(Ljava/lang/Object;Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "TOutputT;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState<",
        "TStateT;TEventT;TOutputT;>;+TOutputT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0000\n\u0002\u0008\u0004\u0010\u0000\u001a \u0012\u0016\u0012\u0014\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0002\u0012\u0004\u0012\u0002H\u00050\u0001\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0007\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0007\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0007\"\u0008\u0008\u0003\u0010\u0005*\u00020\u0007\"\u0008\u0008\u0004\u0010\u0008*\u00020\u00072\u0006\u0010\t\u001a\u0002H\u0005H\n\u00a2\u0006\u0004\u0008\n\u0010\u000b"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;",
        "StateT",
        "EventT",
        "OutputT",
        "InputT",
        "",
        "RenderingT",
        "result",
        "invoke",
        "(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$render$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$render$2;

    invoke-direct {v0}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$render$2;-><init>()V

    sput-object v0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$render$2;->INSTANCE:Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$render$2;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TOutputT;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState<",
            "TStateT;TEventT;TOutputT;>;TOutputT;>;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 61
    invoke-virtual {p0, p1}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$render$2;->invoke(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
