.class public final Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapterKt$asV2Workflow$1;
.super Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;
.source "LegacyWorkflowAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapterKt;->asV2Workflow(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/Renderer;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter<",
        "TStateT;TStateT;TEventT;TOutputT;TRenderingT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLegacyWorkflowAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LegacyWorkflowAdapter.kt\ncom/squareup/workflow/legacyintegration/LegacyWorkflowAdapterKt$asV2Workflow$1\n*L\n1#1,194:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000+\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002 \u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u0001J+\u0010\u0002\u001a\u00028\u00032\u0006\u0010\u0003\u001a\u00028\u00002\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00028\u00010\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016\u00a2\u0006\u0002\u0010\u0008J\u0015\u0010\t\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u000bJ9\u0010\u000c\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\r2\u0006\u0010\u000e\u001a\u00028\u00002\u0008\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u0006\u001a\u00020\u0007H\u0016\u00a2\u0006\u0002\u0010\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "com/squareup/workflow/legacyintegration/LegacyWorkflowAdapterKt$asV2Workflow$1",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;",
        "render",
        "state",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;",
        "startWorkflow",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "input",
        "(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;",
        "v2-legacy-integration"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $renderer:Lcom/squareup/workflow/legacy/Renderer;

.field final synthetic $restorer:Lkotlin/jvm/functions/Function1;

.field final synthetic $snapshotter:Lkotlin/jvm/functions/Function1;

.field final synthetic $this_asV2Workflow:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lkotlin/jvm/functions/Function1;Lcom/squareup/workflow/legacy/Renderer;Lkotlin/jvm/functions/Function1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "TStateT;-TEventT;+TOutputT;>;",
            "Lkotlin/jvm/functions/Function1;",
            "Lcom/squareup/workflow/legacy/Renderer;",
            "Lkotlin/jvm/functions/Function1;",
            ")V"
        }
    .end annotation

    .line 34
    iput-object p1, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapterKt$asV2Workflow$1;->$this_asV2Workflow:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    iput-object p2, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapterKt$asV2Workflow$1;->$restorer:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapterKt$asV2Workflow$1;->$renderer:Lcom/squareup/workflow/legacy/Renderer;

    iput-object p4, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapterKt$asV2Workflow$1;->$snapshotter:Lkotlin/jvm/functions/Function1;

    invoke-direct {p0}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public render(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TStateT;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-TEventT;>;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")TRenderingT;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflows"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapterKt$asV2Workflow$1;->$renderer:Lcom/squareup/workflow/legacy/Renderer;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/workflow/legacy/Renderer;->render(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public snapshot(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TStateT;)",
            "Lcom/squareup/workflow/Snapshot;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapterKt$asV2Workflow$1;->$snapshotter:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public startWorkflow(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TStateT;",
            "Lcom/squareup/workflow/Snapshot;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "TStateT;TEventT;TOutputT;>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflows"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 41
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapterKt$asV2Workflow$1;->$restorer:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    if-eqz p2, :cond_0

    move-object p1, p2

    .line 42
    :cond_0
    iget-object p2, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapterKt$asV2Workflow$1;->$this_asV2Workflow:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    invoke-interface {p2, p1, p3}, Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;->launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method
