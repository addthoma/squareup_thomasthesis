.class final Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$render$1$2;
.super Lkotlin/jvm/internal/Lambda;
.source "LegacyWorkflowAdapter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$render$1;->invoke(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState<",
        "TStateT;TEventT;TOutputT;>;",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState<",
        "TStateT;TEventT;TOutputT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0010\u0000\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0001\"\u0008\u0008\u0000\u0010\u0005*\u00020\u0006\"\u0008\u0008\u0001\u0010\u0002*\u00020\u0006\"\u0008\u0008\u0002\u0010\u0003*\u00020\u0006\"\u0008\u0008\u0003\u0010\u0004*\u00020\u0006\"\u0008\u0008\u0004\u0010\u0007*\u00020\u00062\u0018\u0010\u0008\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0001H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;",
        "StateT",
        "EventT",
        "OutputT",
        "InputT",
        "",
        "RenderingT",
        "it",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $newState:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$render$1$2;->$newState:Ljava/lang/Object;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState<",
            "TStateT;TEventT;TOutputT;>;)",
            "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState<",
            "TStateT;TEventT;TOutputT;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    iget-object v4, p0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$render$1$2;->$newState:Ljava/lang/Object;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xb

    const/4 v7, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->copy$default(Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;Lcom/squareup/workflow/legacy/Workflow;Lkotlinx/coroutines/channels/ReceiveChannel;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;ILjava/lang/Object;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 61
    check-cast p1, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$render$1$2;->invoke(Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;

    move-result-object p1

    return-object p1
.end method
