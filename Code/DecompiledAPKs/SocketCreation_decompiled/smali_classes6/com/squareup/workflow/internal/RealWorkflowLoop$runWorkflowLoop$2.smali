.class final Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "WorkflowLoop.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/internal/RealWorkflowLoop;->runWorkflowLoop(Lcom/squareup/workflow/StatefulWorkflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "*>;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowLoop.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowLoop.kt\ncom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2\n+ 2 Channels.common.kt\nkotlinx/coroutines/channels/ChannelsKt__Channels_commonKt\n+ 3 Select.kt\nkotlinx/coroutines/selects/SelectKt\n*L\n1#1,169:1\n158#2,3:170\n165#2:182\n161#2,6:183\n190#3,9:173\n*E\n*S KotlinDebug\n*F\n+ 1 WorkflowLoop.kt\ncom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2\n*L\n74#1,3:170\n74#1:182\n74#1,6:183\n74#1,9:173\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0001\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\u0008\u0000\u0010\u0002\"\u0004\u0008\u0001\u0010\u0003\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0005\"\u0004\u0008\u0003\u0010\u0006*\u00020\u0007H\u008a@\u00a2\u0006\u0004\u0008\u0008\u0010\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "PropsT",
        "StateT",
        "OutputT",
        "",
        "RenderingT",
        "Lkotlinx/coroutines/CoroutineScope;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.workflow.internal.RealWorkflowLoop$runWorkflowLoop$2"
    f = "WorkflowLoop.kt"
    i = {
        0x0,
        0x0,
        0x0,
        0x0,
        0x0,
        0x1,
        0x1,
        0x1,
        0x1,
        0x1,
        0x1,
        0x1,
        0x1,
        0x1,
        0x1,
        0x1,
        0x1,
        0x2,
        0x2,
        0x2,
        0x2,
        0x2,
        0x2,
        0x2,
        0x2,
        0x2,
        0x2,
        0x2,
        0x2,
        0x2,
        0x3,
        0x3,
        0x3,
        0x3,
        0x3,
        0x3,
        0x3,
        0x3,
        0x3,
        0x3,
        0x3,
        0x3
    }
    l = {
        0x4c,
        0x69,
        0x6a,
        0xad
    }
    m = "invokeSuspend"
    n = {
        "$this$coroutineScope",
        "inputsChannel",
        "$this$consume$iv",
        "cause$iv",
        "$this$consume",
        "$this$coroutineScope",
        "inputsChannel",
        "$this$consume$iv",
        "cause$iv",
        "$this$consume",
        "output",
        "input",
        "inputsClosed",
        "idCounter",
        "rendering",
        "snapshot",
        "rootNode",
        "$this$coroutineScope",
        "inputsChannel",
        "$this$consume$iv",
        "cause$iv",
        "$this$consume",
        "output",
        "input",
        "inputsClosed",
        "idCounter",
        "rendering",
        "snapshot",
        "rootNode",
        "it",
        "$this$coroutineScope",
        "inputsChannel",
        "$this$consume$iv",
        "cause$iv",
        "$this$consume",
        "output",
        "input",
        "inputsClosed",
        "idCounter",
        "rendering",
        "snapshot",
        "rootNode"
    }
    s = {
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$5",
        "L$6",
        "L$7",
        "L$8",
        "L$9",
        "L$10",
        "L$11",
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$5",
        "L$6",
        "L$7",
        "L$8",
        "L$9",
        "L$10",
        "L$11",
        "L$12",
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$5",
        "L$6",
        "L$7",
        "L$8",
        "L$9",
        "L$10",
        "L$12"
    }
.end annotation


# instance fields
.field final synthetic $diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

.field final synthetic $initialSnapshot:Lcom/squareup/workflow/Snapshot;

.field final synthetic $initialState:Ljava/lang/Object;

.field final synthetic $onOutput:Lkotlin/jvm/functions/Function2;

.field final synthetic $onRendering:Lkotlin/jvm/functions/Function2;

.field final synthetic $props:Lkotlinx/coroutines/flow/Flow;

.field final synthetic $workerContext:Lkotlin/coroutines/CoroutineContext;

.field final synthetic $workflow:Lcom/squareup/workflow/StatefulWorkflow;

.field L$0:Ljava/lang/Object;

.field L$1:Ljava/lang/Object;

.field L$10:Ljava/lang/Object;

.field L$11:Ljava/lang/Object;

.field L$12:Ljava/lang/Object;

.field L$2:Ljava/lang/Object;

.field L$3:Ljava/lang/Object;

.field L$4:Ljava/lang/Object;

.field L$5:Ljava/lang/Object;

.field L$6:Ljava/lang/Object;

.field L$7:Ljava/lang/Object;

.field L$8:Ljava/lang/Object;

.field L$9:Ljava/lang/Object;

.field label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;

.field final synthetic this$0:Lcom/squareup/workflow/internal/RealWorkflowLoop;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/internal/RealWorkflowLoop;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lcom/squareup/workflow/StatefulWorkflow;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->this$0:Lcom/squareup/workflow/internal/RealWorkflowLoop;

    iput-object p2, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$props:Lkotlinx/coroutines/flow/Flow;

    iput-object p3, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    iput-object p4, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$workflow:Lcom/squareup/workflow/StatefulWorkflow;

    iput-object p5, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$initialSnapshot:Lcom/squareup/workflow/Snapshot;

    iput-object p6, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$workerContext:Lkotlin/coroutines/CoroutineContext;

    iput-object p7, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$initialState:Ljava/lang/Object;

    iput-object p8, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$onRendering:Lkotlin/jvm/functions/Function2;

    iput-object p9, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$onOutput:Lkotlin/jvm/functions/Function2;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p10}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;

    iget-object v2, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->this$0:Lcom/squareup/workflow/internal/RealWorkflowLoop;

    iget-object v3, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$props:Lkotlinx/coroutines/flow/Flow;

    iget-object v4, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    iget-object v5, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$workflow:Lcom/squareup/workflow/StatefulWorkflow;

    iget-object v6, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$initialSnapshot:Lcom/squareup/workflow/Snapshot;

    iget-object v7, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$workerContext:Lkotlin/coroutines/CoroutineContext;

    iget-object v8, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$initialState:Ljava/lang/Object;

    iget-object v9, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$onRendering:Lkotlin/jvm/functions/Function2;

    iget-object v10, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$onOutput:Lkotlin/jvm/functions/Function2;

    move-object v1, v0

    move-object v11, p2

    invoke-direct/range {v1 .. v11}, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;-><init>(Lcom/squareup/workflow/internal/RealWorkflowLoop;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lcom/squareup/workflow/StatefulWorkflow;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 35

    move-object/from16 v1, p0

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 69
    iget v2, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->label:I

    const/4 v3, 0x4

    const/4 v4, 0x3

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-eqz v2, :cond_4

    if-eq v2, v6, :cond_3

    if-eq v2, v5, :cond_2

    if-eq v2, v4, :cond_1

    if-ne v2, v3, :cond_0

    iget-object v2, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$12:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/workflow/internal/WorkflowNode;

    iget-object v6, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$11:Ljava/lang/Object;

    check-cast v6, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;

    iget-object v6, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$10:Ljava/lang/Object;

    check-cast v6, Lcom/squareup/workflow/Snapshot;

    iget-object v6, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$9:Ljava/lang/Object;

    iget-object v6, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$8:Ljava/lang/Object;

    check-cast v6, Lcom/squareup/workflow/diagnostic/IdCounter;

    iget-object v7, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$7:Ljava/lang/Object;

    check-cast v7, Lkotlin/jvm/internal/Ref$BooleanRef;

    iget-object v8, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$6:Ljava/lang/Object;

    check-cast v8, Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v9, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$5:Ljava/lang/Object;

    iget-object v9, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$4:Ljava/lang/Object;

    check-cast v9, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v10, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$3:Ljava/lang/Object;

    check-cast v10, Ljava/lang/Throwable;

    iget-object v11, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$2:Ljava/lang/Object;

    check-cast v11, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v12, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$1:Ljava/lang/Object;

    check-cast v12, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v13, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$0:Ljava/lang/Object;

    check-cast v13, Lkotlinx/coroutines/CoroutineScope;

    :try_start_0
    invoke-static/range {p1 .. p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object/from16 v3, p1

    move-object v14, v6

    move-object v4, v7

    const/4 v5, 0x4

    move-object v6, v0

    move-object v0, v1

    move-object/from16 v32, v13

    move-object v13, v9

    move-object v9, v10

    move-object v10, v11

    move-object v11, v12

    move-object/from16 v12, v32

    goto/16 :goto_b

    :catchall_0
    move-exception v0

    move-object/from16 v18, v2

    :goto_0
    move-object v10, v11

    goto/16 :goto_c

    .line 143
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_1
    iget-object v2, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$12:Ljava/lang/Object;

    iget-object v2, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$11:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/workflow/internal/WorkflowNode;

    iget-object v6, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$10:Ljava/lang/Object;

    check-cast v6, Lcom/squareup/workflow/Snapshot;

    iget-object v7, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$9:Ljava/lang/Object;

    iget-object v8, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$8:Ljava/lang/Object;

    check-cast v8, Lcom/squareup/workflow/diagnostic/IdCounter;

    iget-object v9, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$7:Ljava/lang/Object;

    check-cast v9, Lkotlin/jvm/internal/Ref$BooleanRef;

    iget-object v10, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$6:Ljava/lang/Object;

    check-cast v10, Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v11, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$5:Ljava/lang/Object;

    iget-object v12, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$4:Ljava/lang/Object;

    check-cast v12, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v13, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$3:Ljava/lang/Object;

    check-cast v13, Ljava/lang/Throwable;

    iget-object v14, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$2:Ljava/lang/Object;

    check-cast v14, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v15, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$1:Ljava/lang/Object;

    check-cast v15, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v3, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$0:Ljava/lang/Object;

    check-cast v3, Lkotlinx/coroutines/CoroutineScope;

    :try_start_1
    invoke-static/range {p1 .. p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v5, v0

    move-object v0, v1

    move-object v4, v2

    move-object v1, v11

    move-object v11, v14

    const/4 v2, 0x3

    move-object v14, v8

    move-object v8, v10

    goto/16 :goto_8

    :catchall_1
    move-exception v0

    move-object/from16 v18, v2

    move-object v10, v14

    goto/16 :goto_c

    :cond_2
    iget-object v2, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$11:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/workflow/internal/WorkflowNode;

    iget-object v3, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$10:Ljava/lang/Object;

    check-cast v3, Lcom/squareup/workflow/Snapshot;

    iget-object v6, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$9:Ljava/lang/Object;

    iget-object v7, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$8:Ljava/lang/Object;

    check-cast v7, Lcom/squareup/workflow/diagnostic/IdCounter;

    iget-object v8, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$7:Ljava/lang/Object;

    check-cast v8, Lkotlin/jvm/internal/Ref$BooleanRef;

    iget-object v9, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$6:Ljava/lang/Object;

    check-cast v9, Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v10, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$5:Ljava/lang/Object;

    iget-object v11, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$4:Ljava/lang/Object;

    check-cast v11, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v12, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$3:Ljava/lang/Object;

    check-cast v12, Ljava/lang/Throwable;

    iget-object v13, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$2:Ljava/lang/Object;

    check-cast v13, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v14, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$1:Ljava/lang/Object;

    check-cast v14, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v15, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$0:Ljava/lang/Object;

    check-cast v15, Lkotlinx/coroutines/CoroutineScope;

    :try_start_2
    invoke-static/range {p1 .. p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-object v4, v0

    move-object v0, v1

    move-object v1, v2

    const/4 v2, 0x2

    move-object/from16 v32, v6

    move-object v6, v3

    move-object v3, v15

    move-object v15, v14

    move-object v14, v7

    move-object/from16 v7, v32

    move-object/from16 v33, v9

    move-object v9, v8

    move-object/from16 v8, v33

    move-object/from16 v34, v11

    move-object v11, v10

    move-object v10, v13

    move-object v13, v12

    move-object/from16 v12, v34

    goto/16 :goto_7

    :catchall_2
    move-exception v0

    move-object/from16 v18, v2

    move-object v10, v13

    goto/16 :goto_c

    :cond_3
    iget-object v2, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$6:Ljava/lang/Object;

    check-cast v2, Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v3, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$5:Ljava/lang/Object;

    check-cast v3, Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v8, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$4:Ljava/lang/Object;

    check-cast v8, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v9, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$3:Ljava/lang/Object;

    check-cast v9, Ljava/lang/Throwable;

    iget-object v10, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$2:Ljava/lang/Object;

    check-cast v10, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v11, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$1:Ljava/lang/Object;

    check-cast v11, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v12, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$0:Ljava/lang/Object;

    check-cast v12, Lkotlinx/coroutines/CoroutineScope;

    :try_start_3
    invoke-static/range {p1 .. p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-object v13, v8

    move-object v8, v3

    move-object/from16 v3, p1

    goto :goto_1

    :catchall_3
    move-exception v0

    move-object v1, v0

    goto/16 :goto_d

    :cond_4
    invoke-static/range {p1 .. p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object v12, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->p$:Lkotlinx/coroutines/CoroutineScope;

    .line 72
    iget-object v2, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$props:Lkotlinx/coroutines/flow/Flow;

    invoke-static {v2, v12}, Lkotlinx/coroutines/flow/FlowKt;->produceIn(Lkotlinx/coroutines/flow/Flow;Lkotlinx/coroutines/CoroutineScope;)Lkotlinx/coroutines/channels/ReceiveChannel;

    move-result-object v8

    .line 170
    move-object v9, v7

    check-cast v9, Ljava/lang/Throwable;

    .line 76
    :try_start_4
    new-instance v2, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v2}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iput-object v12, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$0:Ljava/lang/Object;

    iput-object v8, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$1:Ljava/lang/Object;

    iput-object v8, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$2:Ljava/lang/Object;

    iput-object v9, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$3:Ljava/lang/Object;

    iput-object v8, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$4:Ljava/lang/Object;

    iput-object v2, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$5:Ljava/lang/Object;

    iput-object v2, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$6:Ljava/lang/Object;

    iput v6, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->label:I

    invoke-interface {v8, v1}, Lkotlinx/coroutines/channels/ReceiveChannel;->receive(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_a

    if-ne v3, v0, :cond_5

    return-object v0

    :cond_5
    move-object v10, v8

    move-object v11, v10

    move-object v13, v11

    move-object v8, v2

    .line 69
    :goto_1
    :try_start_5
    iput-object v3, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 77
    new-instance v2, Lkotlin/jvm/internal/Ref$BooleanRef;

    invoke-direct {v2}, Lkotlin/jvm/internal/Ref$BooleanRef;-><init>()V

    const/4 v3, 0x0

    iput-boolean v3, v2, Lkotlin/jvm/internal/Ref$BooleanRef;->element:Z

    .line 78
    iget-object v14, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    if-eqz v14, :cond_6

    new-instance v14, Lcom/squareup/workflow/diagnostic/IdCounter;

    invoke-direct {v14}, Lcom/squareup/workflow/diagnostic/IdCounter;-><init>()V

    goto :goto_2

    :cond_6
    move-object v14, v7

    .line 80
    :goto_2
    iget-object v15, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$workflow:Lcom/squareup/workflow/StatefulWorkflow;

    check-cast v15, Lcom/squareup/workflow/Workflow;

    invoke-static {v15, v7, v6, v7}, Lcom/squareup/workflow/internal/WorkflowIdKt;->id$default(Lcom/squareup/workflow/Workflow;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/internal/WorkflowId;

    move-result-object v18

    .line 81
    iget-object v15, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$workflow:Lcom/squareup/workflow/StatefulWorkflow;

    .line 82
    iget-object v3, v8, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 83
    iget-object v6, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$initialSnapshot:Lcom/squareup/workflow/Snapshot;

    if-eqz v6, :cond_9

    invoke-virtual {v6}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v6

    if-eqz v6, :cond_9

    invoke-virtual {v6}, Lokio/ByteString;->size()I

    move-result v19

    if-nez v19, :cond_7

    const/16 v17, 0x1

    goto :goto_3

    :cond_7
    const/16 v17, 0x0

    :goto_3
    invoke-static/range {v17 .. v17}, Lkotlin/coroutines/jvm/internal/Boxing;->boxBoolean(Z)Ljava/lang/Boolean;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v17

    if-nez v17, :cond_8

    goto :goto_4

    :cond_8
    move-object v6, v7

    :goto_4
    move-object/from16 v21, v6

    goto :goto_5

    :cond_9
    move-object/from16 v21, v7

    .line 84
    :goto_5
    invoke-interface {v12}, Lkotlinx/coroutines/CoroutineScope;->getCoroutineContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object v22

    const/16 v23, 0x0

    .line 85
    iget-object v6, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$workerContext:Lkotlin/coroutines/CoroutineContext;

    const/16 v24, 0x0

    .line 87
    iget-object v7, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    .line 89
    iget-object v4, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$initialState:Ljava/lang/Object;

    const/16 v29, 0x20

    const/16 v30, 0x0

    .line 79
    new-instance v31, Lcom/squareup/workflow/internal/WorkflowNode;

    move-object/from16 v17, v31

    move-object/from16 v19, v15

    move-object/from16 v20, v3

    move-object/from16 v25, v7

    move-object/from16 v26, v14

    move-object/from16 v27, v4

    move-object/from16 v28, v6

    invoke-direct/range {v17 .. v30}, Lcom/squareup/workflow/internal/WorkflowNode;-><init>(Lcom/squareup/workflow/internal/WorkflowId;Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;Lokio/ByteString;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function1;Ljava/lang/Long;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lcom/squareup/workflow/diagnostic/IdCounter;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move-object v6, v0

    move-object v0, v1

    move-object v4, v2

    move-object/from16 v2, v31

    const/4 v3, 0x0

    .line 94
    :goto_6
    :try_start_6
    invoke-interface {v12}, Lkotlinx/coroutines/CoroutineScope;->getCoroutineContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object v7

    invoke-static {v7}, Lkotlinx/coroutines/JobKt;->ensureActive(Lkotlin/coroutines/CoroutineContext;)V

    .line 96
    iget-object v7, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    if-eqz v7, :cond_a

    iget-object v15, v8, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    invoke-interface {v7, v15}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onBeforeRenderPass(Ljava/lang/Object;)V

    sget-object v7, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 97
    :cond_a
    iget-object v7, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->this$0:Lcom/squareup/workflow/internal/RealWorkflowLoop;

    iget-object v15, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$workflow:Lcom/squareup/workflow/StatefulWorkflow;

    iget-object v5, v8, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    invoke-virtual {v7, v2, v15, v5}, Lcom/squareup/workflow/internal/RealWorkflowLoop;->doRender(Lcom/squareup/workflow/internal/WorkflowNode;Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 98
    iget-object v7, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    if-eqz v7, :cond_b

    .line 99
    invoke-interface {v7, v5}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onAfterRenderPass(Ljava/lang/Object;)V

    .line 100
    invoke-interface {v7}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onBeforeSnapshotPass()V

    .line 101
    sget-object v7, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 102
    :cond_b
    iget-object v7, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$workflow:Lcom/squareup/workflow/StatefulWorkflow;

    invoke-virtual {v2, v7}, Lcom/squareup/workflow/internal/WorkflowNode;->snapshot(Lcom/squareup/workflow/StatefulWorkflow;)Lcom/squareup/workflow/Snapshot;

    move-result-object v7

    .line 103
    iget-object v15, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    if-eqz v15, :cond_c

    invoke-interface {v15}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onAfterSnapshotPass()V

    sget-object v15, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 105
    :cond_c
    iget-object v15, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$onRendering:Lkotlin/jvm/functions/Function2;

    new-instance v1, Lcom/squareup/workflow/RenderingAndSnapshot;

    invoke-direct {v1, v5, v7}, Lcom/squareup/workflow/RenderingAndSnapshot;-><init>(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)V

    iput-object v12, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$0:Ljava/lang/Object;

    iput-object v11, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$1:Ljava/lang/Object;

    iput-object v10, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$2:Ljava/lang/Object;

    iput-object v9, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$3:Ljava/lang/Object;

    iput-object v13, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$4:Ljava/lang/Object;

    iput-object v3, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$5:Ljava/lang/Object;

    iput-object v8, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$6:Ljava/lang/Object;

    iput-object v4, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$7:Ljava/lang/Object;

    iput-object v14, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$8:Ljava/lang/Object;

    iput-object v5, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$9:Ljava/lang/Object;

    iput-object v7, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$10:Ljava/lang/Object;

    iput-object v2, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$11:Ljava/lang/Object;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_9

    move-object/from16 v18, v2

    const/4 v2, 0x2

    :try_start_7
    iput v2, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->label:I

    invoke-interface {v15, v1, v0}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_8

    if-ne v1, v6, :cond_d

    return-object v6

    :cond_d
    move-object v15, v11

    move-object/from16 v1, v18

    move-object v11, v3

    move-object v3, v12

    move-object v12, v13

    move-object v13, v9

    move-object v9, v4

    move-object v4, v6

    move-object v6, v7

    move-object v7, v5

    :goto_7
    if-eqz v11, :cond_f

    .line 106
    :try_start_8
    iget-object v5, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$onOutput:Lkotlin/jvm/functions/Function2;

    iput-object v3, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$0:Ljava/lang/Object;

    iput-object v15, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$1:Ljava/lang/Object;

    iput-object v10, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$2:Ljava/lang/Object;

    iput-object v13, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$3:Ljava/lang/Object;

    iput-object v12, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$4:Ljava/lang/Object;

    iput-object v11, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$5:Ljava/lang/Object;

    iput-object v8, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$6:Ljava/lang/Object;

    iput-object v9, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$7:Ljava/lang/Object;

    iput-object v14, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$8:Ljava/lang/Object;

    iput-object v7, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$9:Ljava/lang/Object;

    iput-object v6, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$10:Ljava/lang/Object;

    iput-object v1, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$11:Ljava/lang/Object;

    iput-object v11, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$12:Ljava/lang/Object;

    const/4 v2, 0x3

    iput v2, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->label:I

    invoke-interface {v5, v11, v0}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    if-ne v5, v4, :cond_e

    return-object v4

    :cond_e
    move-object v5, v4

    move-object v4, v1

    move-object v1, v11

    move-object v11, v10

    :goto_8
    :try_start_9
    sget-object v10, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    move-object v10, v13

    move-object v13, v5

    move-object v5, v6

    move-object v6, v14

    move-object/from16 v32, v1

    move-object v1, v0

    move-object/from16 v0, v32

    move-object/from16 v33, v12

    move-object v12, v4

    move-object v4, v9

    move-object/from16 v9, v33

    goto :goto_9

    :catchall_4
    move-exception v0

    move-object/from16 v18, v4

    goto/16 :goto_0

    :catchall_5
    move-exception v0

    move-object/from16 v18, v1

    goto/16 :goto_c

    :cond_f
    const/4 v2, 0x3

    move-object v5, v6

    move-object v6, v14

    move-object/from16 v32, v1

    move-object v1, v0

    move-object v0, v11

    move-object v11, v10

    move-object v10, v13

    move-object v13, v4

    move-object v4, v9

    move-object v9, v12

    move-object/from16 v12, v32

    .line 173
    :goto_9
    :try_start_a
    iput-object v3, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$0:Ljava/lang/Object;

    iput-object v15, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$1:Ljava/lang/Object;

    iput-object v11, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$2:Ljava/lang/Object;

    iput-object v10, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$3:Ljava/lang/Object;

    iput-object v9, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$4:Ljava/lang/Object;

    iput-object v0, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$5:Ljava/lang/Object;

    iput-object v8, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$6:Ljava/lang/Object;

    iput-object v4, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$7:Ljava/lang/Object;

    iput-object v6, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$8:Ljava/lang/Object;

    iput-object v7, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$9:Ljava/lang/Object;

    iput-object v5, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$10:Ljava/lang/Object;

    iput-object v1, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$11:Ljava/lang/Object;

    iput-object v12, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->L$12:Ljava/lang/Object;

    const/4 v5, 0x4

    iput v5, v1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->label:I

    .line 174
    new-instance v7, Lkotlinx/coroutines/selects/SelectBuilderImpl;

    invoke-direct {v7, v1}, Lkotlinx/coroutines/selects/SelectBuilderImpl;-><init>(Lkotlin/coroutines/Continuation;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_7

    .line 176
    :try_start_b
    move-object v0, v7

    check-cast v0, Lkotlinx/coroutines/selects/SelectBuilder;

    .line 112
    iget-boolean v14, v4, Lkotlin/jvm/internal/Ref$BooleanRef;->element:Z

    if-nez v14, :cond_10

    .line 115
    invoke-interface {v15}, Lkotlinx/coroutines/channels/ReceiveChannel;->getOnReceiveOrNull()Lkotlinx/coroutines/selects/SelectClause1;

    move-result-object v14

    new-instance v16, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;

    const/16 v19, 0x0

    move-object/from16 v18, v16

    move-object/from16 v20, v4

    move-object/from16 v21, v8

    move-object/from16 v22, v12

    move-object/from16 v23, v1

    move-object/from16 v24, v3

    move-object/from16 v25, v15

    invoke-direct/range {v18 .. v25}, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;-><init>(Lkotlin/coroutines/Continuation;Lkotlin/jvm/internal/Ref$BooleanRef;Lkotlin/jvm/internal/Ref$ObjectRef;Lcom/squareup/workflow/internal/WorkflowNode;Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;Lkotlinx/coroutines/CoroutineScope;Lkotlinx/coroutines/channels/ReceiveChannel;)V

    move-object/from16 v2, v16

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-interface {v0, v14, v2}, Lkotlinx/coroutines/selects/SelectBuilder;->invoke(Lkotlinx/coroutines/selects/SelectClause1;Lkotlin/jvm/functions/Function2;)V

    .line 129
    :cond_10
    invoke-virtual {v12, v0}, Lcom/squareup/workflow/internal/WorkflowNode;->tick(Lkotlinx/coroutines/selects/SelectBuilder;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_6

    goto :goto_a

    :catchall_6
    move-exception v0

    .line 178
    :try_start_c
    invoke-virtual {v7, v0}, Lkotlinx/coroutines/selects/SelectBuilderImpl;->handleBuilderException(Ljava/lang/Throwable;)V

    .line 180
    :goto_a
    invoke-virtual {v7}, Lkotlinx/coroutines/selects/SelectBuilderImpl;->getResult()Ljava/lang/Object;

    move-result-object v0

    .line 173
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v2

    if-ne v0, v2, :cond_11

    invoke-static {v1}, Lkotlin/coroutines/jvm/internal/DebugProbesKt;->probeCoroutineSuspended(Lkotlin/coroutines/Continuation;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_7

    :cond_11
    if-ne v0, v13, :cond_12

    return-object v13

    :cond_12
    move-object v14, v6

    move-object v2, v12

    move-object v6, v13

    move-object v12, v3

    move-object v13, v9

    move-object v9, v10

    move-object v10, v11

    move-object v11, v15

    move-object v3, v0

    move-object v0, v1

    :goto_b
    const/4 v5, 0x2

    move-object/from16 v1, p0

    goto/16 :goto_6

    :catchall_7
    move-exception v0

    move-object v10, v11

    move-object/from16 v18, v12

    goto :goto_c

    :catchall_8
    move-exception v0

    goto :goto_c

    :catchall_9
    move-exception v0

    move-object/from16 v18, v2

    .line 141
    :goto_c
    :try_start_d
    invoke-virtual/range {v18 .. v18}, Lcom/squareup/workflow/internal/WorkflowNode;->cancel()V

    throw v0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    :catchall_a
    move-exception v0

    move-object v1, v0

    move-object v10, v8

    .line 185
    :goto_d
    :try_start_e
    throw v1
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_b

    :catchall_b
    move-exception v0

    move-object v2, v0

    .line 182
    invoke-static {v10, v1}, Lkotlinx/coroutines/channels/ChannelsKt;->cancelConsumed(Lkotlinx/coroutines/channels/ReceiveChannel;Ljava/lang/Throwable;)V

    throw v2
.end method
