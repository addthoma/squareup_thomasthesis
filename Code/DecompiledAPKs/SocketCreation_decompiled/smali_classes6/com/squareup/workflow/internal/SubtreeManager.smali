.class public final Lcom/squareup/workflow/internal/SubtreeManager;
.super Ljava/lang/Object;
.source "SubtreeManager.kt"

# interfaces
.implements Lcom/squareup/workflow/internal/RealRenderContext$Renderer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<StateT:",
        "Ljava/lang/Object;",
        "OutputT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/internal/RealRenderContext$Renderer<",
        "TStateT;TOutputT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSubtreeManager.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SubtreeManager.kt\ncom/squareup/workflow/internal/SubtreeManager\n+ 2 ActiveStagingList.kt\ncom/squareup/workflow/internal/ActiveStagingList\n+ 3 InlineLinkedList.kt\ncom/squareup/workflow/internal/InlineLinkedList\n*L\n1#1,209:1\n73#2:210\n76#2,5:217\n90#2:222\n61#2:229\n62#2,2:254\n85#2:256\n85#2:263\n106#3,6:211\n106#3,6:223\n76#3,24:230\n106#3,6:257\n106#3,6:264\n*E\n*S KotlinDebug\n*F\n+ 1 SubtreeManager.kt\ncom/squareup/workflow/internal/SubtreeManager\n*L\n121#1:210\n121#1,5:217\n137#1:222\n144#1:229\n144#1,2:254\n157#1:256\n164#1:263\n121#1,6:211\n137#1,6:223\n144#1,24:230\n157#1,6:257\n164#1,6:264\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u0000*\u0004\u0008\u0000\u0010\u0001*\u0008\u0008\u0001\u0010\u0002*\u00020\u00032\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u0004BY\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012 \u0010\u0007\u001a\u001c\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\t\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0008\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\r\u0012\n\u0008\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u0012\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0011J\u0006\u0010\u001a\u001a\u00020\u001bJ\u0085\u0001\u0010\u001c\u001a\u001a\u0012\u0004\u0012\u0002H\u001d\u0012\u0004\u0012\u0002H\u001e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0014\"\u0004\u0008\u0002\u0010\u001d\"\u0008\u0008\u0003\u0010\u001e*\u00020\u0003\"\u0004\u0008\u0004\u0010\u001f2\u0018\u0010 \u001a\u0014\u0012\u0004\u0012\u0002H\u001d\u0012\u0004\u0012\u0002H\u001e\u0012\u0004\u0012\u0002H\u001f0!2\u0006\u0010\"\u001a\u0002H\u001d2\u0006\u0010#\u001a\u00020$2\u001e\u0010%\u001a\u001a\u0012\u0004\u0012\u0002H\u001e\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\t0\u0008H\u0002\u00a2\u0006\u0002\u0010&J(\u0010\'\u001a$\u0012 \u0012\u001e\u0012\u0014\u0012\u0012\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0017j\u0002`\u0018\u0012\u0004\u0012\u00020*0)0(Jm\u0010+\u001a\u0002H\u001f\"\u0004\u0008\u0002\u0010\u001d\"\u0008\u0008\u0003\u0010\u001e*\u00020\u0003\"\u0004\u0008\u0004\u0010\u001f2\u0018\u0010 \u001a\u0014\u0012\u0004\u0012\u0002H\u001d\u0012\u0004\u0012\u0002H\u001e\u0012\u0004\u0012\u0002H\u001f0!2\u0006\u0010,\u001a\u0002H\u001d2\u0006\u0010#\u001a\u00020$2\u001e\u0010%\u001a\u001a\u0012\u0004\u0012\u0002H\u001e\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\t0\u0008H\u0016\u00a2\u0006\u0002\u0010-J0\u0010.\u001a\u00020\u001b2(\u0010/\u001a$\u0012 \u0012\u001e\u0012\u0014\u0012\u0012\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0017j\u0002`\u0018\u0012\u0004\u0012\u00020\u00190)0(J \u00100\u001a\u00020\u001b\"\u0008\u0008\u0002\u00101*\u00020\u00032\u000e\u00102\u001a\n\u0012\u0006\u0012\u0004\u0018\u0001H103R$\u0010\u0012\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00140\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000c\u001a\u0004\u0018\u00010\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010\u0007\u001a\u001c\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\t\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R*\u0010\u0015\u001a\u001e\u0012\u0014\u0012\u0012\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0017j\u0002`\u0018\u0012\u0004\u0012\u00020\u00190\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00064"
    }
    d2 = {
        "Lcom/squareup/workflow/internal/SubtreeManager;",
        "StateT",
        "OutputT",
        "",
        "Lcom/squareup/workflow/internal/RealRenderContext$Renderer;",
        "contextForChildren",
        "Lkotlin/coroutines/CoroutineContext;",
        "emitActionToParent",
        "Lkotlin/Function1;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "parentDiagnosticId",
        "",
        "diagnosticListener",
        "Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
        "idCounter",
        "Lcom/squareup/workflow/diagnostic/IdCounter;",
        "workerContext",
        "(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function1;JLcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lcom/squareup/workflow/diagnostic/IdCounter;Lkotlin/coroutines/CoroutineContext;)V",
        "children",
        "Lcom/squareup/workflow/internal/ActiveStagingList;",
        "Lcom/squareup/workflow/internal/WorkflowChildNode;",
        "snapshotCache",
        "",
        "Lcom/squareup/workflow/internal/WorkflowId;",
        "Lcom/squareup/workflow/internal/AnyId;",
        "Lokio/ByteString;",
        "commitRenderedChildren",
        "",
        "createChildNode",
        "ChildPropsT",
        "ChildOutputT",
        "ChildRenderingT",
        "child",
        "Lcom/squareup/workflow/Workflow;",
        "initialProps",
        "key",
        "",
        "handler",
        "(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/internal/WorkflowChildNode;",
        "createChildSnapshots",
        "",
        "Lkotlin/Pair;",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "props",
        "(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;",
        "restoreChildrenFromSnapshots",
        "childSnapshots",
        "tickChildren",
        "T",
        "selector",
        "Lkotlinx/coroutines/selects/SelectBuilder;",
        "workflow-runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private children:Lcom/squareup/workflow/internal/ActiveStagingList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/internal/ActiveStagingList<",
            "Lcom/squareup/workflow/internal/WorkflowChildNode<",
            "****>;>;"
        }
    .end annotation
.end field

.field private final contextForChildren:Lkotlin/coroutines/CoroutineContext;

.field private final diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

.field private final emitActionToParent:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final idCounter:Lcom/squareup/workflow/diagnostic/IdCounter;

.field private final parentDiagnosticId:J

.field private final snapshotCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/internal/WorkflowId<",
            "***>;",
            "Lokio/ByteString;",
            ">;"
        }
    .end annotation
.end field

.field private final workerContext:Lkotlin/coroutines/CoroutineContext;


# direct methods
.method public constructor <init>(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function1;JLcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lcom/squareup/workflow/diagnostic/IdCounter;Lkotlin/coroutines/CoroutineContext;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/CoroutineContext;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;+",
            "Ljava/lang/Object;",
            ">;J",
            "Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
            "Lcom/squareup/workflow/diagnostic/IdCounter;",
            "Lkotlin/coroutines/CoroutineContext;",
            ")V"
        }
    .end annotation

    const-string v0, "contextForChildren"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emitActionToParent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workerContext"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/internal/SubtreeManager;->contextForChildren:Lkotlin/coroutines/CoroutineContext;

    iput-object p2, p0, Lcom/squareup/workflow/internal/SubtreeManager;->emitActionToParent:Lkotlin/jvm/functions/Function1;

    iput-wide p3, p0, Lcom/squareup/workflow/internal/SubtreeManager;->parentDiagnosticId:J

    iput-object p5, p0, Lcom/squareup/workflow/internal/SubtreeManager;->diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    iput-object p6, p0, Lcom/squareup/workflow/internal/SubtreeManager;->idCounter:Lcom/squareup/workflow/diagnostic/IdCounter;

    iput-object p7, p0, Lcom/squareup/workflow/internal/SubtreeManager;->workerContext:Lkotlin/coroutines/CoroutineContext;

    .line 108
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/squareup/workflow/internal/SubtreeManager;->snapshotCache:Ljava/util/Map;

    .line 110
    new-instance p1, Lcom/squareup/workflow/internal/ActiveStagingList;

    invoke-direct {p1}, Lcom/squareup/workflow/internal/ActiveStagingList;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/internal/SubtreeManager;->children:Lcom/squareup/workflow/internal/ActiveStagingList;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function1;JLcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lcom/squareup/workflow/diagnostic/IdCounter;Lkotlin/coroutines/CoroutineContext;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 10

    and-int/lit8 v0, p8, 0x8

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 98
    move-object v0, v1

    check-cast v0, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    move-object v7, v0

    goto :goto_0

    :cond_0
    move-object v7, p5

    :goto_0
    and-int/lit8 v0, p8, 0x10

    if-eqz v0, :cond_1

    .line 99
    move-object v0, v1

    check-cast v0, Lcom/squareup/workflow/diagnostic/IdCounter;

    move-object v8, v0

    goto :goto_1

    :cond_1
    move-object/from16 v8, p6

    :goto_1
    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_2

    .line 100
    sget-object v0, Lkotlin/coroutines/EmptyCoroutineContext;->INSTANCE:Lkotlin/coroutines/EmptyCoroutineContext;

    check-cast v0, Lkotlin/coroutines/CoroutineContext;

    move-object v9, v0

    goto :goto_2

    :cond_2
    move-object/from16 v9, p7

    :goto_2
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-wide v5, p3

    invoke-direct/range {v2 .. v9}, Lcom/squareup/workflow/internal/SubtreeManager;-><init>(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function1;JLcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lcom/squareup/workflow/diagnostic/IdCounter;Lkotlin/coroutines/CoroutineContext;)V

    return-void
.end method

.method public static final synthetic access$getEmitActionToParent$p(Lcom/squareup/workflow/internal/SubtreeManager;)Lkotlin/jvm/functions/Function1;
    .locals 0

    .line 94
    iget-object p0, p0, Lcom/squareup/workflow/internal/SubtreeManager;->emitActionToParent:Lkotlin/jvm/functions/Function1;

    return-object p0
.end method

.method private final createChildNode(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/internal/WorkflowChildNode;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ChildPropsT:",
            "Ljava/lang/Object;",
            "ChildOutputT:",
            "Ljava/lang/Object;",
            "ChildRenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Workflow<",
            "-TChildPropsT;+TChildOutputT;+TChildRenderingT;>;TChildPropsT;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-TChildOutputT;+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;>;)",
            "Lcom/squareup/workflow/internal/WorkflowChildNode<",
            "TChildPropsT;TChildOutputT;TStateT;TOutputT;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    .line 185
    invoke-static {v1, v2}, Lcom/squareup/workflow/internal/WorkflowIdKt;->id(Lcom/squareup/workflow/Workflow;Ljava/lang/String;)Lcom/squareup/workflow/internal/WorkflowId;

    move-result-object v3

    .line 186
    new-instance v15, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v15}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    const/4 v2, 0x0

    iput-object v2, v15, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 188
    new-instance v2, Lcom/squareup/workflow/internal/SubtreeManager$createChildNode$1;

    invoke-direct {v2, v0, v15}, Lcom/squareup/workflow/internal/SubtreeManager$createChildNode$1;-><init>(Lcom/squareup/workflow/internal/SubtreeManager;Lkotlin/jvm/internal/Ref$ObjectRef;)V

    .line 193
    new-instance v14, Lcom/squareup/workflow/internal/WorkflowNode;

    .line 195
    invoke-interface/range {p1 .. p1}, Lcom/squareup/workflow/Workflow;->asStatefulWorkflow()Lcom/squareup/workflow/StatefulWorkflow;

    move-result-object v4

    .line 197
    iget-object v5, v0, Lcom/squareup/workflow/internal/SubtreeManager;->snapshotCache:Ljava/util/Map;

    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lokio/ByteString;

    .line 198
    iget-object v7, v0, Lcom/squareup/workflow/internal/SubtreeManager;->contextForChildren:Lkotlin/coroutines/CoroutineContext;

    .line 199
    new-instance v5, Lcom/squareup/workflow/internal/SubtreeManager$createChildNode$workflowNode$1;

    invoke-direct {v5, v2}, Lcom/squareup/workflow/internal/SubtreeManager$createChildNode$workflowNode$1;-><init>(Lcom/squareup/workflow/internal/SubtreeManager$createChildNode$1;)V

    move-object v8, v5

    check-cast v8, Lkotlin/jvm/functions/Function1;

    .line 200
    iget-wide v9, v0, Lcom/squareup/workflow/internal/SubtreeManager;->parentDiagnosticId:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 201
    iget-object v10, v0, Lcom/squareup/workflow/internal/SubtreeManager;->diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    .line 202
    iget-object v11, v0, Lcom/squareup/workflow/internal/SubtreeManager;->idCounter:Lcom/squareup/workflow/diagnostic/IdCounter;

    .line 203
    iget-object v13, v0, Lcom/squareup/workflow/internal/SubtreeManager;->workerContext:Lkotlin/coroutines/CoroutineContext;

    const/4 v12, 0x0

    const/16 v16, 0x200

    const/16 v17, 0x0

    move-object v2, v14

    move-object/from16 v5, p2

    move-object/from16 v18, v14

    move/from16 v14, v16

    move-object v0, v15

    move-object/from16 v15, v17

    .line 193
    invoke-direct/range {v2 .. v15}, Lcom/squareup/workflow/internal/WorkflowNode;-><init>(Lcom/squareup/workflow/internal/WorkflowId;Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;Lokio/ByteString;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function1;Ljava/lang/Long;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lcom/squareup/workflow/diagnostic/IdCounter;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 205
    new-instance v2, Lcom/squareup/workflow/internal/WorkflowChildNode;

    move-object/from16 v3, p4

    move-object/from16 v4, v18

    invoke-direct {v2, v1, v3, v4}, Lcom/squareup/workflow/internal/WorkflowChildNode;-><init>(Lcom/squareup/workflow/Workflow;Lkotlin/jvm/functions/Function1;Lcom/squareup/workflow/internal/WorkflowNode;)V

    .line 206
    iput-object v2, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    return-object v2
.end method


# virtual methods
.method public final commitRenderedChildren()V
    .locals 4

    .line 121
    iget-object v0, p0, Lcom/squareup/workflow/internal/SubtreeManager;->children:Lcom/squareup/workflow/internal/ActiveStagingList;

    .line 210
    invoke-static {v0}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getActive$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object v1

    .line 211
    invoke-virtual {v1}, Lcom/squareup/workflow/internal/InlineLinkedList;->getHead()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_0

    .line 213
    move-object v2, v1

    check-cast v2, Lcom/squareup/workflow/internal/WorkflowChildNode;

    .line 122
    invoke-virtual {v2}, Lcom/squareup/workflow/internal/WorkflowChildNode;->getWorkflowNode()Lcom/squareup/workflow/internal/WorkflowNode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/workflow/internal/WorkflowNode;->cancel()V

    .line 123
    iget-object v3, p0, Lcom/squareup/workflow/internal/SubtreeManager;->snapshotCache:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/squareup/workflow/internal/WorkflowChildNode;->getId()Lcom/squareup/workflow/internal/WorkflowId;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    invoke-interface {v1}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->getNextListNode()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v1

    goto :goto_0

    .line 217
    :cond_0
    invoke-static {v0}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getActive$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object v1

    .line 218
    invoke-static {v0}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getStaging$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$setActive$p(Lcom/squareup/workflow/internal/ActiveStagingList;Lcom/squareup/workflow/internal/InlineLinkedList;)V

    .line 219
    invoke-static {v0, v1}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$setStaging$p(Lcom/squareup/workflow/internal/ActiveStagingList;Lcom/squareup/workflow/internal/InlineLinkedList;)V

    .line 220
    invoke-static {v0}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getStaging$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/internal/InlineLinkedList;->clear()V

    return-void
.end method

.method public final createChildSnapshots()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lkotlin/Pair<",
            "Lcom/squareup/workflow/internal/WorkflowId<",
            "***>;",
            "Lcom/squareup/workflow/Snapshot;",
            ">;>;"
        }
    .end annotation

    .line 163
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 164
    iget-object v1, p0, Lcom/squareup/workflow/internal/SubtreeManager;->children:Lcom/squareup/workflow/internal/ActiveStagingList;

    .line 263
    invoke-static {v1}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getActive$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object v1

    .line 264
    invoke-virtual {v1}, Lcom/squareup/workflow/internal/InlineLinkedList;->getHead()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_0

    .line 266
    move-object v2, v1

    check-cast v2, Lcom/squareup/workflow/internal/WorkflowChildNode;

    .line 165
    invoke-virtual {v2}, Lcom/squareup/workflow/internal/WorkflowChildNode;->getWorkflowNode()Lcom/squareup/workflow/internal/WorkflowNode;

    move-result-object v3

    invoke-virtual {v2}, Lcom/squareup/workflow/internal/WorkflowChildNode;->getWorkflow()Lcom/squareup/workflow/Workflow;

    move-result-object v4

    invoke-interface {v4}, Lcom/squareup/workflow/Workflow;->asStatefulWorkflow()Lcom/squareup/workflow/StatefulWorkflow;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/workflow/internal/WorkflowNode;->snapshot(Lcom/squareup/workflow/StatefulWorkflow;)Lcom/squareup/workflow/Snapshot;

    move-result-object v3

    .line 166
    move-object v4, v0

    check-cast v4, Ljava/util/Collection;

    invoke-virtual {v2}, Lcom/squareup/workflow/internal/WorkflowChildNode;->getId()Lcom/squareup/workflow/internal/WorkflowId;

    move-result-object v2

    invoke-static {v2, v3}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 267
    invoke-interface {v1}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->getNextListNode()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public render(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ChildPropsT:",
            "Ljava/lang/Object;",
            "ChildOutputT:",
            "Ljava/lang/Object;",
            "ChildRenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Workflow<",
            "-TChildPropsT;+TChildOutputT;+TChildRenderingT;>;TChildPropsT;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-TChildOutputT;+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;>;)TChildRenderingT;"
        }
    .end annotation

    const-string v0, "child"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/squareup/workflow/internal/SubtreeManager;->children:Lcom/squareup/workflow/internal/ActiveStagingList;

    .line 222
    invoke-static {v0}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getStaging$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object v0

    .line 223
    invoke-virtual {v0}, Lcom/squareup/workflow/internal/InlineLinkedList;->getHead()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    .line 225
    move-object v1, v0

    check-cast v1, Lcom/squareup/workflow/internal/WorkflowChildNode;

    .line 138
    invoke-virtual {v1, p1, p3}, Lcom/squareup/workflow/internal/WorkflowChildNode;->matches(Lcom/squareup/workflow/Workflow;Ljava/lang/String;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 226
    invoke-interface {v0}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->getNextListNode()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v0

    goto :goto_0

    .line 139
    :cond_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Expected keys to be unique for "

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ": key="

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 138
    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2

    .line 144
    :cond_1
    iget-object v0, p0, Lcom/squareup/workflow/internal/SubtreeManager;->children:Lcom/squareup/workflow/internal/ActiveStagingList;

    .line 229
    invoke-static {v0}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getActive$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object v1

    .line 230
    invoke-virtual {v1}, Lcom/squareup/workflow/internal/InlineLinkedList;->getHead()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v2

    const/4 v3, 0x0

    .line 231
    move-object v4, v3

    check-cast v4, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-object v5, v4

    :goto_1
    if-eqz v2, :cond_5

    .line 234
    move-object v6, v2

    check-cast v6, Lcom/squareup/workflow/internal/WorkflowChildNode;

    .line 145
    invoke-virtual {v6, p1, p3}, Lcom/squareup/workflow/internal/WorkflowChildNode;->matches(Lcom/squareup/workflow/Workflow;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    if-nez v5, :cond_2

    .line 238
    invoke-interface {v2}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->getNextListNode()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/workflow/internal/InlineLinkedList;->setHead(Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;)V

    goto :goto_2

    .line 240
    :cond_2
    invoke-interface {v2}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->getNextListNode()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v3

    invoke-interface {v5, v3}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->setNextListNode(Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;)V

    .line 242
    :goto_2
    invoke-virtual {v1}, Lcom/squareup/workflow/internal/InlineLinkedList;->getTail()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 243
    invoke-virtual {v1, v5}, Lcom/squareup/workflow/internal/InlineLinkedList;->setTail(Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;)V

    .line 245
    :cond_3
    invoke-interface {v2, v4}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->setNextListNode(Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;)V

    goto :goto_3

    .line 250
    :cond_4
    invoke-interface {v2}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->getNextListNode()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v5

    move-object v7, v5

    move-object v5, v2

    move-object v2, v7

    goto :goto_1

    :cond_5
    move-object v2, v3

    :goto_3
    if-eqz v2, :cond_6

    goto :goto_4

    .line 146
    :cond_6
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/workflow/internal/SubtreeManager;->createChildNode(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/internal/WorkflowChildNode;

    move-result-object p3

    move-object v2, p3

    check-cast v2, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    .line 254
    :goto_4
    invoke-static {v0}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getStaging$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object p3

    invoke-virtual {p3, v2}, Lcom/squareup/workflow/internal/InlineLinkedList;->plusAssign(Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;)V

    .line 144
    check-cast v2, Lcom/squareup/workflow/internal/WorkflowChildNode;

    .line 148
    invoke-virtual {v2, p4}, Lcom/squareup/workflow/internal/WorkflowChildNode;->setHandler(Lkotlin/jvm/functions/Function1;)V

    .line 149
    invoke-interface {p1}, Lcom/squareup/workflow/Workflow;->asStatefulWorkflow()Lcom/squareup/workflow/StatefulWorkflow;

    move-result-object p1

    invoke-virtual {v2, p1, p2}, Lcom/squareup/workflow/internal/WorkflowChildNode;->render(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final restoreChildrenFromSnapshots(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/workflow/internal/WorkflowId<",
            "***>;+",
            "Lokio/ByteString;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "childSnapshots"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lcom/squareup/workflow/internal/SubtreeManager;->snapshotCache:Ljava/util/Map;

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lkotlin/collections/MapsKt;->toMap(Ljava/lang/Iterable;)Ljava/util/Map;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    return-void
.end method

.method public final tickChildren(Lkotlinx/coroutines/selects/SelectBuilder;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlinx/coroutines/selects/SelectBuilder<",
            "-TT;>;)V"
        }
    .end annotation

    const-string v0, "selector"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lcom/squareup/workflow/internal/SubtreeManager;->children:Lcom/squareup/workflow/internal/ActiveStagingList;

    .line 256
    invoke-static {v0}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getActive$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object v0

    .line 257
    invoke-virtual {v0}, Lcom/squareup/workflow/internal/InlineLinkedList;->getHead()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    .line 259
    move-object v1, v0

    check-cast v1, Lcom/squareup/workflow/internal/WorkflowChildNode;

    .line 158
    invoke-virtual {v1}, Lcom/squareup/workflow/internal/WorkflowChildNode;->getWorkflowNode()Lcom/squareup/workflow/internal/WorkflowNode;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/squareup/workflow/internal/WorkflowNode;->tick(Lkotlinx/coroutines/selects/SelectBuilder;)V

    .line 260
    invoke-interface {v0}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->getNextListNode()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-void
.end method
