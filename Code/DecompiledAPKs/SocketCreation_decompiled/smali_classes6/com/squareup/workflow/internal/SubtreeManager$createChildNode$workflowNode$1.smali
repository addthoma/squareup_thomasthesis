.class final synthetic Lcom/squareup/workflow/internal/SubtreeManager$createChildNode$workflowNode$1;
.super Lkotlin/jvm/internal/FunctionReference;
.source "SubtreeManager.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/internal/SubtreeManager;->createChildNode(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/internal/WorkflowChildNode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/FunctionReference;",
        "Lkotlin/jvm/functions/Function1<",
        "TChildOutputT;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u0004\u0018\u00010\u0001\"\u0004\u0008\u0000\u0010\u0002\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0001\"\u0004\u0008\u0002\u0010\u0004\"\u0004\u0008\u0003\u0010\u0005\"\u0008\u0008\u0004\u0010\u0006*\u00020\u00012\u0015\u0010\u0007\u001a\u0011H\u0003\u00a2\u0006\u000c\u0008\u0008\u0012\u0008\u0008\t\u0012\u0004\u0008\u0008(\n\u00a2\u0006\u0004\u0008\u000b\u0010\u000c"
    }
    d2 = {
        "<anonymous>",
        "",
        "ChildPropsT",
        "ChildOutputT",
        "ChildRenderingT",
        "StateT",
        "OutputT",
        "p1",
        "Lkotlin/ParameterName;",
        "name",
        "output",
        "invoke",
        "(Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $acceptChildOutput$1:Lcom/squareup/workflow/internal/SubtreeManager$createChildNode$1;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/internal/SubtreeManager$createChildNode$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/internal/SubtreeManager$createChildNode$workflowNode$1;->$acceptChildOutput$1:Lcom/squareup/workflow/internal/SubtreeManager$createChildNode$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/FunctionReference;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    const-string v0, "acceptChildOutput"

    return-object v0
.end method

.method public final getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "invoke(Ljava/lang/Object;)Ljava/lang/Object;"

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TChildOutputT;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/workflow/internal/SubtreeManager$createChildNode$workflowNode$1;->$acceptChildOutput$1:Lcom/squareup/workflow/internal/SubtreeManager$createChildNode$1;

    .line 199
    invoke-virtual {v0, p1}, Lcom/squareup/workflow/internal/SubtreeManager$createChildNode$1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
