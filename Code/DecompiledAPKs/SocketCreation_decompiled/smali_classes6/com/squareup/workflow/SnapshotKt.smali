.class public final Lcom/squareup/workflow/SnapshotKt;
.super Ljava/lang/Object;
.source "Snapshot.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSnapshot.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,181:1\n1591#2,2:182\n*E\n*S KotlinDebug\n*F\n+ 1 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n158#1,2:182\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0002\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0008\u001a,\u0010\u0000\u001a\u0002H\u0001\"\u0004\u0008\u0000\u0010\u0001*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u00010\u0004H\u0086\u0008\u00a2\u0006\u0002\u0010\u0006\u001a\n\u0010\u0007\u001a\u00020\u0008*\u00020\u0005\u001a\n\u0010\t\u001a\u00020\u0002*\u00020\u0005\u001a$\u0010\n\u001a\u0002H\u0001\"\u0010\u0008\u0000\u0010\u0001\u0018\u0001*\u0008\u0012\u0004\u0012\u0002H\u00010\u000b*\u00020\u0005H\u0086\u0008\u00a2\u0006\u0002\u0010\u000c\u001a\n\u0010\r\u001a\u00020\u000e*\u00020\u0005\u001a2\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u00010\u0010\"\u0004\u0008\u0000\u0010\u0001*\u00020\u00052\u0017\u0010\u0011\u001a\u0013\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u00010\u0004\u00a2\u0006\u0002\u0008\u0012H\u0086\u0008\u001a4\u0010\u0013\u001a\u0004\u0018\u0001H\u0001\"\u0008\u0008\u0000\u0010\u0001*\u00020\u0014*\u00020\u00052\u0017\u0010\u0011\u001a\u0013\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u00010\u0004\u00a2\u0006\u0002\u0008\u0012\u00a2\u0006\u0002\u0010\u0015\u001a&\u0010\u0016\u001a\u0004\u0018\u0001H\u0001\"\u0010\u0008\u0000\u0010\u0001\u0018\u0001*\u0008\u0012\u0004\u0012\u0002H\u00010\u000b*\u00020\u0005H\u0086\u0008\u00a2\u0006\u0002\u0010\u000c\u001a\u000c\u0010\u0017\u001a\u0004\u0018\u00010\u0018*\u00020\u0005\u001a\n\u0010\u0019\u001a\u00020\u0018*\u00020\u0005\u001a\u0012\u0010\u001a\u001a\u00020\u001b*\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0008\u001a\u0012\u0010\u001d\u001a\u00020\u001b*\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\u0002\u001a\'\u0010\u001f\u001a\u00020\u001b\"\u000e\u0008\u0000\u0010\u0001*\u0008\u0012\u0004\u0012\u0002H\u00010\u000b*\u00020\u001b2\u0006\u0010 \u001a\u0002H\u0001\u00a2\u0006\u0002\u0010!\u001a\u0012\u0010\"\u001a\u00020\u001b*\u00020\u001b2\u0006\u0010#\u001a\u00020\u000e\u001a@\u0010$\u001a\u00020\u001b\"\u0004\u0008\u0000\u0010\u0001*\u00020\u001b2\u000c\u0010%\u001a\u0008\u0012\u0004\u0012\u0002H\u00010\u00102\u001d\u0010&\u001a\u0019\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u00020(0\'\u00a2\u0006\u0002\u0008\u0012H\u0086\u0008\u001aB\u0010)\u001a\u00020\u001b\"\u0008\u0008\u0000\u0010\u0001*\u00020\u0014*\u00020\u001b2\u0008\u0010*\u001a\u0004\u0018\u0001H\u00012\u001d\u0010&\u001a\u0019\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u00020(0\'\u00a2\u0006\u0002\u0008\u0012\u00a2\u0006\u0002\u0010+\u001a)\u0010,\u001a\u00020\u001b\"\u000e\u0008\u0000\u0010\u0001*\u0008\u0012\u0004\u0012\u0002H\u00010\u000b*\u00020\u001b2\u0008\u0010 \u001a\u0004\u0018\u0001H\u0001\u00a2\u0006\u0002\u0010!\u001a\u0014\u0010-\u001a\u00020\u001b*\u00020\u001b2\u0008\u0010.\u001a\u0004\u0018\u00010\u0018\u001a\u0012\u0010/\u001a\u00020\u001b*\u00020\u001b2\u0006\u0010.\u001a\u00020\u0018\u00a8\u00060"
    }
    d2 = {
        "parse",
        "T",
        "Lokio/ByteString;",
        "block",
        "Lkotlin/Function1;",
        "Lokio/BufferedSource;",
        "(Lokio/ByteString;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;",
        "readBooleanFromInt",
        "",
        "readByteStringWithLength",
        "readEnumByOrdinal",
        "",
        "(Lokio/BufferedSource;)Ljava/lang/Enum;",
        "readFloat",
        "",
        "readList",
        "",
        "reader",
        "Lkotlin/ExtensionFunctionType;",
        "readNullable",
        "",
        "(Lokio/BufferedSource;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;",
        "readOptionalEnumByOrdinal",
        "readOptionalUtf8WithLength",
        "",
        "readUtf8WithLength",
        "writeBooleanAsInt",
        "Lokio/BufferedSink;",
        "bool",
        "writeByteStringWithLength",
        "bytes",
        "writeEnumByOrdinal",
        "enumVal",
        "(Lokio/BufferedSink;Ljava/lang/Enum;)Lokio/BufferedSink;",
        "writeFloat",
        "float",
        "writeList",
        "values",
        "writer",
        "Lkotlin/Function2;",
        "",
        "writeNullable",
        "obj",
        "(Lokio/BufferedSink;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Lokio/BufferedSink;",
        "writeOptionalEnumByOrdinal",
        "writeOptionalUtf8WithLength",
        "str",
        "writeUtf8WithLength",
        "workflow-core"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final parse(Lokio/ByteString;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lokio/ByteString;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lokio/BufferedSource;",
            "+TT;>;)TT;"
        }
    .end annotation

    const-string v0, "$this$parse"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p0}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p0

    invoke-interface {p1, p0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static final readBooleanFromInt(Lokio/BufferedSource;)Z
    .locals 1

    const-string v0, "$this$readBooleanFromInt"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static final readByteStringWithLength(Lokio/BufferedSource;)Lokio/ByteString;
    .locals 2

    const-string v0, "$this$readByteStringWithLength"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    int-to-long v0, v0

    .line 136
    invoke-interface {p0, v0, v1}, Lokio/BufferedSource;->readByteString(J)Lokio/ByteString;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic readEnumByOrdinal(Lokio/BufferedSource;)Ljava/lang/Enum;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum<",
            "TT;>;>(",
            "Lokio/BufferedSource;",
            ")TT;"
        }
    .end annotation

    const-string v0, "$this$readEnumByOrdinal"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x4

    const-string v1, "T"

    .line 148
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v0, Ljava/lang/Enum;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Enum;

    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result p0

    aget-object p0, v0, p0

    const-string v0, "T::class.java.enumConstants[readInt()]"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final readFloat(Lokio/BufferedSource;)F
    .locals 1

    const-string v0, "$this$readFloat"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result p0

    return p0
.end method

.method public static final readList(Lokio/BufferedSource;Lkotlin/jvm/functions/Function1;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lokio/BufferedSource;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lokio/BufferedSource;",
            "+TT;>;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$readList"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reader"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-interface {p1, p0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public static final readNullable(Lokio/BufferedSource;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lokio/BufferedSource;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lokio/BufferedSource;",
            "+TT;>;)TT;"
        }
    .end annotation

    const-string v0, "$this$readNullable"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reader"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, p0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final synthetic readOptionalEnumByOrdinal(Lokio/BufferedSource;)Ljava/lang/Enum;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum<",
            "TT;>;>(",
            "Lokio/BufferedSource;",
            ")TT;"
        }
    .end annotation

    const-string v0, "$this$readOptionalEnumByOrdinal"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    sget-object v0, Lcom/squareup/workflow/SnapshotKt$readOptionalEnumByOrdinal$1;->INSTANCE:Lcom/squareup/workflow/SnapshotKt$readOptionalEnumByOrdinal$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->readNullable(Lokio/BufferedSource;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Enum;

    return-object p0
.end method

.method public static final readOptionalUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$readOptionalUtf8WithLength"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    sget-object v0, Lcom/squareup/workflow/SnapshotKt$readOptionalUtf8WithLength$1;->INSTANCE:Lcom/squareup/workflow/SnapshotKt$readOptionalUtf8WithLength$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->readNullable(Lokio/BufferedSource;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    return-object p0
.end method

.method public static final readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$readUtf8WithLength"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readByteStringWithLength(Lokio/BufferedSource;)Lokio/ByteString;

    move-result-object p0

    invoke-virtual {p0}, Lokio/ByteString;->utf8()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;
    .locals 1

    const-string v0, "$this$writeBooleanAsInt"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-interface {p0, p1}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    move-result-object p0

    return-object p0
.end method

.method public static final writeByteStringWithLength(Lokio/BufferedSink;Lokio/ByteString;)Lokio/BufferedSink;
    .locals 1

    const-string v0, "$this$writeByteStringWithLength"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bytes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result v0

    invoke-interface {p0, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    move-result-object v0

    .line 131
    invoke-interface {v0, p1}, Lokio/BufferedSink;->write(Lokio/ByteString;)Lokio/BufferedSink;

    return-object p0
.end method

.method public static final writeEnumByOrdinal(Lokio/BufferedSink;Ljava/lang/Enum;)Lokio/BufferedSink;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum<",
            "TT;>;>(",
            "Lokio/BufferedSink;",
            "TT;)",
            "Lokio/BufferedSink;"
        }
    .end annotation

    const-string v0, "$this$writeEnumByOrdinal"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "enumVal"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    invoke-interface {p0, p1}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    move-result-object p0

    return-object p0
.end method

.method public static final writeFloat(Lokio/BufferedSink;F)Lokio/BufferedSink;
    .locals 1

    const-string v0, "$this$writeFloat"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    invoke-static {p1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result p1

    invoke-interface {p0, p1}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    move-result-object p0

    return-object p0
.end method

.method public static final writeList(Lokio/BufferedSink;Ljava/util/List;Lkotlin/jvm/functions/Function2;)Lokio/BufferedSink;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lokio/BufferedSink;",
            "Ljava/util/List<",
            "+TT;>;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lokio/BufferedSink;",
            "-TT;",
            "Lkotlin/Unit;",
            ">;)",
            "Lokio/BufferedSink;"
        }
    .end annotation

    const-string v0, "$this$writeList"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "values"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "writer"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p0, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 160
    check-cast p1, Ljava/lang/Iterable;

    .line 182
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 160
    invoke-interface {p2, p0, v0}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public static final writeNullable(Lokio/BufferedSink;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Lokio/BufferedSink;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lokio/BufferedSink;",
            "TT;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lokio/BufferedSink;",
            "-TT;",
            "Lkotlin/Unit;",
            ">;)",
            "Lokio/BufferedSink;"
        }
    .end annotation

    const-string v0, "$this$writeNullable"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "writer"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 99
    :goto_0
    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    if-eqz p1, :cond_1

    .line 100
    invoke-interface {p2, p0, p1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object p0
.end method

.method public static final writeOptionalEnumByOrdinal(Lokio/BufferedSink;Ljava/lang/Enum;)Lokio/BufferedSink;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum<",
            "TT;>;>(",
            "Lokio/BufferedSink;",
            "TT;)",
            "Lokio/BufferedSink;"
        }
    .end annotation

    const-string v0, "$this$writeOptionalEnumByOrdinal"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    sget-object v0, Lcom/squareup/workflow/SnapshotKt$writeOptionalEnumByOrdinal$1;->INSTANCE:Lcom/squareup/workflow/SnapshotKt$writeOptionalEnumByOrdinal$1;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-static {p0, p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeNullable(Lokio/BufferedSink;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Lokio/BufferedSink;

    move-result-object p0

    return-object p0
.end method

.method public static final writeOptionalUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;
    .locals 1

    const-string v0, "$this$writeOptionalUtf8WithLength"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    sget-object v0, Lcom/squareup/workflow/SnapshotKt$writeOptionalUtf8WithLength$1$1;->INSTANCE:Lcom/squareup/workflow/SnapshotKt$writeOptionalUtf8WithLength$1$1;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-static {p0, p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeNullable(Lokio/BufferedSink;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Lokio/BufferedSink;

    return-object p0
.end method

.method public static final writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;
    .locals 1

    const-string v0, "$this$writeUtf8WithLength"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "str"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    sget-object v0, Lokio/ByteString;->Companion:Lokio/ByteString$Companion;

    invoke-virtual {v0, p1}, Lokio/ByteString$Companion;->encodeUtf8(Ljava/lang/String;)Lokio/ByteString;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/workflow/SnapshotKt;->writeByteStringWithLength(Lokio/BufferedSink;Lokio/ByteString;)Lokio/BufferedSink;

    move-result-object p0

    return-object p0
.end method
