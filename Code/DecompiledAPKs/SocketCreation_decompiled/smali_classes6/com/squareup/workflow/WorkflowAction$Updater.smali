.class public final Lcom/squareup/workflow/WorkflowAction$Updater;
.super Ljava/lang/Object;
.source "WorkflowAction.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Updater"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        "O:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\n\n\u0002\u0010\u0002\n\u0000\u0018\u0000*\u0004\u0008\u0002\u0010\u0001*\n\u0008\u0003\u0010\u0002 \u0000*\u00020\u00032\u00020\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00028\u0002\u00a2\u0006\u0002\u0010\u0005J\u0013\u0010\r\u001a\u00020\u000e2\u0006\u0010\n\u001a\u00028\u0003\u00a2\u0006\u0002\u0010\u0005R\u001c\u0010\u0004\u001a\u00028\u0002X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\t\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\u0005R\u001e\u0010\n\u001a\u0004\u0018\u00018\u0003X\u0080\u000e\u00a2\u0006\u0010\n\u0002\u0010\t\u001a\u0004\u0008\u000b\u0010\u0007\"\u0004\u0008\u000c\u0010\u0005\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "S",
        "O",
        "",
        "nextState",
        "(Ljava/lang/Object;)V",
        "getNextState",
        "()Ljava/lang/Object;",
        "setNextState",
        "Ljava/lang/Object;",
        "output",
        "getOutput$workflow_core",
        "setOutput$workflow_core",
        "setOutput",
        "",
        "workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private nextState:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS;"
        }
    .end annotation
.end field

.field private output:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TO;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/WorkflowAction$Updater;->nextState:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final getNextState()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TS;"
        }
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/squareup/workflow/WorkflowAction$Updater;->nextState:Ljava/lang/Object;

    return-object v0
.end method

.method public final getOutput$workflow_core()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TO;"
        }
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/squareup/workflow/WorkflowAction$Updater;->output:Ljava/lang/Object;

    return-object v0
.end method

.method public final setNextState(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)V"
        }
    .end annotation

    .line 34
    iput-object p1, p0, Lcom/squareup/workflow/WorkflowAction$Updater;->nextState:Ljava/lang/Object;

    return-void
.end method

.method public final setOutput(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TO;)V"
        }
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iput-object p1, p0, Lcom/squareup/workflow/WorkflowAction$Updater;->output:Ljava/lang/Object;

    return-void
.end method

.method public final setOutput$workflow_core(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TO;)V"
        }
    .end annotation

    .line 35
    iput-object p1, p0, Lcom/squareup/workflow/WorkflowAction$Updater;->output:Ljava/lang/Object;

    return-void
.end method
