.class public final Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
.super Ljava/lang/Object;
.source "WorkflowPool.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/legacy/WorkflowPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Handle"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        "O:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\n\u0008\u0001\u0010\u0003 \u0000*\u00020\u0002*\n\u0008\u0002\u0010\u0004 \u0001*\u00020\u00022\u00020\u0002B)\u0008\u0001\u0012\u0018\u0010\u0005\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\u0008J \u0010\u000e\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0006H\u00c0\u0003\u00a2\u0006\u0002\u0008\u000fJ\u000e\u0010\u0010\u001a\u00028\u0000H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000cJF\u0010\u0011\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00002\u001a\u0008\u0002\u0010\u0005\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00028\u0000H\u00c6\u0001\u00a2\u0006\u0002\u0010\u0012J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0002H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R&\u0010\u0005\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0006X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0013\u0010\u0007\u001a\u00028\u0000\u00a2\u0006\n\n\u0002\u0010\r\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "S",
        "",
        "E",
        "O",
        "id",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Id;",
        "state",
        "(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)V",
        "getId$legacy_workflow_core",
        "()Lcom/squareup/workflow/legacy/WorkflowPool$Id;",
        "getState",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "component1",
        "component1$legacy_workflow_core",
        "component2",
        "copy",
        "(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "legacy-workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final id:Lcom/squareup/workflow/legacy/WorkflowPool$Id;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowPool$Id<",
            "TS;TE;TO;>;"
        }
    .end annotation
.end field

.field private final state:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Id<",
            "TS;-TE;+TO;>;TS;)V"
        }
    .end annotation

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->id:Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    iput-object p2, p0, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->state:Ljava/lang/Object;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->id:Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->state:Ljava/lang/Object;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->copy(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1$legacy_workflow_core()Lcom/squareup/workflow/legacy/WorkflowPool$Id;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Id<",
            "TS;TE;TO;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->id:Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    return-object v0
.end method

.method public final component2()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TS;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->state:Ljava/lang/Object;

    return-object v0
.end method

.method public final copy(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Id<",
            "TS;-TE;+TO;>;TS;)",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "TS;TE;TO;>;"
        }
    .end annotation

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-direct {v0, p1, p2}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->id:Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    iget-object v1, p1, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->id:Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->state:Ljava/lang/Object;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->state:Ljava/lang/Object;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getId$legacy_workflow_core()Lcom/squareup/workflow/legacy/WorkflowPool$Id;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Id<",
            "TS;TE;TO;>;"
        }
    .end annotation

    .line 127
    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->id:Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    return-object v0
.end method

.method public final getState()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TS;"
        }
    .end annotation

    .line 128
    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->state:Ljava/lang/Object;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->id:Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->state:Ljava/lang/Object;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Handle(id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->id:Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->state:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
