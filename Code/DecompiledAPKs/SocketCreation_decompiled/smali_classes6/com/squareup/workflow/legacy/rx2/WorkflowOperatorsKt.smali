.class public final Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt;
.super Ljava/lang/Object;
.source "WorkflowOperators.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001at\u0010\u0000\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0001\"\u0008\u0008\u0000\u0010\u0005*\u00020\u0006\"\u0008\u0008\u0001\u0010\u0002*\u00020\u0006\"\u0008\u0008\u0002\u0010\u0003*\u00020\u0006\"\u0008\u0008\u0003\u0010\u0004*\u00020\u0006*\u0014\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u00012\u001a\u0010\u0007\u001a\u0016\u0012\u0004\u0012\u0002H\u0005\u0012\u000c\u0012\n\u0012\u0006\u0008\u0001\u0012\u0002H\u00020\t0\u0008H\u0007\u001a\u0018\u0010\n\u001a\u00020\u000b*\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0001H\u0007\u00a8\u0006\u000c"
    }
    d2 = {
        "switchMapState",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "S2",
        "E",
        "O",
        "S1",
        "",
        "transform",
        "Lkotlin/Function1;",
        "Lio/reactivex/Observable;",
        "toCompletable",
        "Lio/reactivex/Completable;",
        "legacy-workflow-rx2"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final switchMapState(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S1:",
            "Ljava/lang/Object;",
            "S2:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "+TS1;-TE;+TO;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TS1;+",
            "Lio/reactivex/Observable<",
            "+TS2;>;>;)",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "TS2;TE;TO;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use com.squareup.workflow.Workflow"
    .end annotation

    const-string v0, "$this$switchMapState"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transform"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    new-instance v0, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt$switchMapState$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt$switchMapState$1;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function3;

    invoke-static {p0, v0}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt;->switchMapState(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function3;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p0

    return-object p0
.end method

.method public static final toCompletable(Lcom/squareup/workflow/legacy/Workflow;)Lio/reactivex/Completable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "***>;)",
            "Lio/reactivex/Completable;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use com.squareup.workflow.Workflow"
    .end annotation

    const-string v0, "$this$toCompletable"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-static {p0}, Lcom/squareup/workflow/legacy/rx2/WorkflowsKt;->getState(Lcom/squareup/workflow/legacy/Workflow;)Lio/reactivex/Observable;

    move-result-object p0

    invoke-virtual {p0}, Lio/reactivex/Observable;->ignoreElements()Lio/reactivex/Completable;

    move-result-object p0

    const-string v0, "state.ignoreElements()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
