.class final Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "EventChannel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-TR;>;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEventChannel.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EventChannel.kt\ncom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1\n+ 2 Select.kt\nkotlinx/coroutines/selects/SelectKt\n*L\n1#1,147:1\n190#2,9:148\n*E\n*S KotlinDebug\n*F\n+ 1 EventChannel.kt\ncom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1\n*L\n98#1,9:148\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u0001H\u0001H\u0001\"\u0008\u0008\u0000\u0010\u0001*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003*\u00020\u0005H\u008a@\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "R",
        "kotlin.jvm.PlatformType",
        "",
        "E",
        "Lkotlinx/coroutines/CoroutineScope;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.workflow.legacy.rx2.EventChannelKt$asEventChannel$1$select$1"
    f = "EventChannel.kt"
    i = {
        0x0,
        0x0,
        0x1,
        0x1,
        0x2,
        0x2,
        0x2
    }
    l = {
        0x94,
        0x9c,
        0x82
    }
    m = "invokeSuspend"
    n = {
        "$this$rxSingle",
        "selectionJob",
        "$this$rxSingle",
        "selectionJob",
        "$this$rxSingle",
        "cancellation",
        "cause"
    }
    s = {
        "L$0",
        "L$1",
        "L$0",
        "L$1",
        "L$0",
        "L$1",
        "L$2"
    }
.end annotation


# instance fields
.field final synthetic $block:Lkotlin/jvm/functions/Function1;

.field L$0:Ljava/lang/Object;

.field L$1:Ljava/lang/Object;

.field L$2:Ljava/lang/Object;

.field L$3:Ljava/lang/Object;

.field label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;

.field final synthetic this$0:Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1;Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->this$0:Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1;

    iput-object p2, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->$block:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p3}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;

    iget-object v1, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->this$0:Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1;

    iget-object v2, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->$block:Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v1, v2, p2}, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;-><init>(Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1;Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 12

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 91
    iget v1, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->label:I

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz v1, :cond_3

    if-eq v1, v5, :cond_2

    if-eq v1, v3, :cond_1

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->L$3:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;

    iget-object v0, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->L$2:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Throwable;

    iget-object v0, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->L$1:Ljava/lang/Object;

    check-cast v0, Ljava/util/concurrent/CancellationException;

    iget-object v0, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->L$0:Ljava/lang/Object;

    check-cast v0, Lkotlinx/coroutines/CoroutineScope;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 135
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 91
    :cond_1
    iget-object v1, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->L$1:Ljava/lang/Object;

    check-cast v1, Lkotlinx/coroutines/CompletableJob;

    iget-object v3, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->L$0:Ljava/lang/Object;

    check-cast v3, Lkotlinx/coroutines/CoroutineScope;

    :try_start_0
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception p1

    move-object v1, p1

    move-object p1, v3

    goto/16 :goto_3

    :cond_2
    iget-object v1, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->L$2:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;

    iget-object v1, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->L$1:Ljava/lang/Object;

    check-cast v1, Lkotlinx/coroutines/CompletableJob;

    iget-object v6, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->L$0:Ljava/lang/Object;

    check-cast v6, Lkotlinx/coroutines/CoroutineScope;

    :try_start_1
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    move-object v1, p1

    move-object p1, v6

    goto/16 :goto_3

    :cond_3
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    .line 97
    :try_start_2
    invoke-interface {p1}, Lkotlinx/coroutines/CoroutineScope;->getCoroutineContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object v1

    sget-object v6, Lkotlinx/coroutines/Job;->Key:Lkotlinx/coroutines/Job$Key;

    check-cast v6, Lkotlin/coroutines/CoroutineContext$Key;

    invoke-interface {v1, v6}, Lkotlin/coroutines/CoroutineContext;->get(Lkotlin/coroutines/CoroutineContext$Key;)Lkotlin/coroutines/CoroutineContext$Element;

    move-result-object v1

    check-cast v1, Lkotlinx/coroutines/Job;

    invoke-static {v1}, Lkotlinx/coroutines/JobKt;->Job(Lkotlinx/coroutines/Job;)Lkotlinx/coroutines/CompletableJob;

    move-result-object v1

    .line 148
    iput-object p1, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->L$0:Ljava/lang/Object;

    iput-object v1, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->L$1:Ljava/lang/Object;

    iput-object p0, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->L$2:Ljava/lang/Object;

    iput v5, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->label:I

    .line 149
    new-instance v6, Lkotlinx/coroutines/selects/SelectBuilderImpl;

    invoke-direct {v6, p0}, Lkotlinx/coroutines/selects/SelectBuilderImpl;-><init>(Lkotlin/coroutines/Continuation;)V
    :try_end_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_2 .. :try_end_2} :catch_2

    .line 151
    :try_start_3
    move-object v7, v6

    check-cast v7, Lkotlinx/coroutines/selects/SelectBuilder;

    .line 100
    new-instance v8, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;

    move-object v9, v1

    check-cast v9, Lkotlinx/coroutines/Job;

    invoke-direct {v8, v7, v9}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;-><init>(Lkotlinx/coroutines/selects/SelectBuilder;Lkotlinx/coroutines/Job;)V

    .line 101
    iget-object v9, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->$block:Lkotlin/jvm/functions/Function1;

    invoke-interface {v9, v8}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    iget-object v9, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->this$0:Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1;

    iget-object v9, v9, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1;->$this_asEventChannel:Lkotlinx/coroutines/channels/ReceiveChannel;

    invoke-interface {v9}, Lkotlinx/coroutines/channels/ReceiveChannel;->getOnReceiveOrNull()Lkotlinx/coroutines/selects/SelectClause1;

    move-result-object v9

    new-instance v10, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1$1$1;

    invoke-direct {v10, v8, v4}, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1$1$1;-><init>(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;Lkotlin/coroutines/Continuation;)V

    check-cast v10, Lkotlin/jvm/functions/Function2;

    invoke-interface {v7, v9, v10}, Lkotlinx/coroutines/selects/SelectBuilder;->invoke(Lkotlinx/coroutines/selects/SelectClause1;Lkotlin/jvm/functions/Function2;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v7

    .line 153
    :try_start_4
    invoke-virtual {v6, v7}, Lkotlinx/coroutines/selects/SelectBuilderImpl;->handleBuilderException(Ljava/lang/Throwable;)V

    .line 155
    :goto_0
    invoke-virtual {v6}, Lkotlinx/coroutines/selects/SelectBuilderImpl;->getResult()Ljava/lang/Object;

    move-result-object v6

    .line 148
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v7

    if-ne v6, v7, :cond_4

    invoke-static {p0}, Lkotlin/coroutines/jvm/internal/DebugProbesKt;->probeCoroutineSuspended(Lkotlin/coroutines/Continuation;)V
    :try_end_4
    .catch Ljava/util/concurrent/CancellationException; {:try_start_4 .. :try_end_4} :catch_2

    :cond_4
    if-ne v6, v0, :cond_5

    return-object v0

    :cond_5
    move-object v11, v6

    move-object v6, p1

    move-object p1, v11

    .line 156
    :goto_1
    :try_start_5
    check-cast p1, Lio/reactivex/SingleSource;

    iput-object v6, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->L$0:Ljava/lang/Object;

    iput-object v1, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->L$1:Ljava/lang/Object;

    iput v3, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->label:I

    .line 121
    invoke-static {p1, p0}, Lkotlinx/coroutines/rx2/RxAwaitKt;->await(Lio/reactivex/SingleSource;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1
    :try_end_5
    .catch Ljava/util/concurrent/CancellationException; {:try_start_5 .. :try_end_5} :catch_1

    if-ne p1, v0, :cond_6

    return-object v0

    :cond_6
    move-object v3, v6

    .line 124
    :goto_2
    :try_start_6
    invoke-static {v1, v4, v5, v4}, Lkotlinx/coroutines/Job$DefaultImpls;->cancel$default(Lkotlinx/coroutines/Job;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V
    :try_end_6
    .catch Ljava/util/concurrent/CancellationException; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_5

    :catch_2
    move-exception v1

    .line 126
    :goto_3
    iget-object v3, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->this$0:Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1;

    invoke-static {v3, v1}, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1;->access$unwrapRealCause(Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1;Ljava/util/concurrent/CancellationException;)Ljava/lang/Throwable;

    move-result-object v3

    if-nez v3, :cond_9

    .line 130
    iput-object p1, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->L$0:Ljava/lang/Object;

    iput-object v1, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->L$1:Ljava/lang/Object;

    iput-object v3, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->L$2:Ljava/lang/Object;

    iput-object p0, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->L$3:Ljava/lang/Object;

    iput v2, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;->label:I

    new-instance p1, Lkotlin/coroutines/SafeContinuation;

    invoke-static {p0}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->intercepted(Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object v1

    invoke-direct {p1, v1}, Lkotlin/coroutines/SafeContinuation;-><init>(Lkotlin/coroutines/Continuation;)V

    move-object v1, p1

    check-cast v1, Lkotlin/coroutines/Continuation;

    invoke-virtual {p1}, Lkotlin/coroutines/SafeContinuation;->getOrThrow()Ljava/lang/Object;

    move-result-object p1

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_7

    invoke-static {p0}, Lkotlin/coroutines/jvm/internal/DebugProbesKt;->probeCoroutineSuspended(Lkotlin/coroutines/Continuation;)V

    :cond_7
    if-ne p1, v0, :cond_8

    return-object v0

    .line 133
    :cond_8
    :goto_4
    check-cast p1, Ljava/lang/Void;

    :goto_5
    return-object p1

    :cond_9
    throw v3
.end method
