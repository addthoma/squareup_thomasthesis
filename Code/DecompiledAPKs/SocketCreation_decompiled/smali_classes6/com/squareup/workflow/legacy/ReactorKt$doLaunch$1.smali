.class final Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "Reactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function4;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacy/ReactorKt;->doLaunch(Lcom/squareup/workflow/legacy/Reactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;Lkotlin/coroutines/CoroutineContext;)Lcom/squareup/workflow/legacy/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function4<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlinx/coroutines/channels/SendChannel<",
        "-TS;>;",
        "Lkotlinx/coroutines/channels/ReceiveChannel<",
        "+TE;>;",
        "Lkotlin/coroutines/Continuation<",
        "-TO;>;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0002H\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0001*\u00020\u0003*\u00020\u00052\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00040\tH\u008a@\u00a2\u0006\u0004\u0008\n\u0010\u000b"
    }
    d2 = {
        "<anonymous>",
        "O",
        "S",
        "",
        "E",
        "Lkotlinx/coroutines/CoroutineScope;",
        "stateChannel",
        "Lkotlinx/coroutines/channels/SendChannel;",
        "eventChannel",
        "Lkotlinx/coroutines/channels/ReceiveChannel;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.workflow.legacy.ReactorKt$doLaunch$1"
    f = "Reactor.kt"
    i = {
        0x0,
        0x0,
        0x0,
        0x0,
        0x0,
        0x0,
        0x1,
        0x1,
        0x1,
        0x1,
        0x1,
        0x1
    }
    l = {
        0x104,
        0x108
    }
    m = "invokeSuspend"
    n = {
        "$this$workflow",
        "stateChannel",
        "eventChannel",
        "reaction",
        "currentState",
        "state",
        "$this$workflow",
        "stateChannel",
        "eventChannel",
        "reaction",
        "currentState",
        "state"
    }
    s = {
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$5",
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$5"
    }
.end annotation


# instance fields
.field final synthetic $initialState:Ljava/lang/Object;

.field final synthetic $this_doLaunch:Lcom/squareup/workflow/legacy/Reactor;

.field final synthetic $workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

.field L$0:Ljava/lang/Object;

.field L$1:Ljava/lang/Object;

.field L$2:Ljava/lang/Object;

.field L$3:Ljava/lang/Object;

.field L$4:Ljava/lang/Object;

.field L$5:Ljava/lang/Object;

.field label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;

.field private p$0:Lkotlinx/coroutines/channels/SendChannel;

.field private p$1:Lkotlinx/coroutines/channels/ReceiveChannel;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/Reactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->$this_doLaunch:Lcom/squareup/workflow/legacy/Reactor;

    iput-object p2, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->$initialState:Ljava/lang/Object;

    iput-object p3, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    const/4 p1, 0x4

    invoke-direct {p0, p1, p4}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Lkotlinx/coroutines/CoroutineScope;Lkotlinx/coroutines/channels/SendChannel;Lkotlinx/coroutines/channels/ReceiveChannel;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlinx/coroutines/CoroutineScope;",
            "Lkotlinx/coroutines/channels/SendChannel<",
            "-TS;>;",
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "+TE;>;",
            "Lkotlin/coroutines/Continuation<",
            "-TO;>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$create"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stateChannel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventChannel"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "continuation"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;

    iget-object v1, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->$this_doLaunch:Lcom/squareup/workflow/legacy/Reactor;

    iget-object v2, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->$initialState:Ljava/lang/Object;

    iget-object v3, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    invoke-direct {v0, v1, v2, v3, p4}, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;-><init>(Lcom/squareup/workflow/legacy/Reactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;Lkotlin/coroutines/Continuation;)V

    iput-object p1, v0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    iput-object p2, v0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->p$0:Lkotlinx/coroutines/channels/SendChannel;

    iput-object p3, v0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->p$1:Lkotlinx/coroutines/channels/ReceiveChannel;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    check-cast p2, Lkotlinx/coroutines/channels/SendChannel;

    check-cast p3, Lkotlinx/coroutines/channels/ReceiveChannel;

    check-cast p4, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->create(Lkotlinx/coroutines/CoroutineScope;Lkotlinx/coroutines/channels/SendChannel;Lkotlinx/coroutines/channels/ReceiveChannel;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 13

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 253
    iget v1, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->label:I

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eqz v1, :cond_2

    if-eq v1, v3, :cond_1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$5:Ljava/lang/Object;

    iget-object v1, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$4:Ljava/lang/Object;

    iget-object v4, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$3:Ljava/lang/Object;

    check-cast v4, Lcom/squareup/workflow/legacy/Reaction;

    iget-object v4, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$2:Ljava/lang/Object;

    check-cast v4, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v5, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$1:Ljava/lang/Object;

    check-cast v5, Lkotlinx/coroutines/channels/SendChannel;

    iget-object v6, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$0:Ljava/lang/Object;

    check-cast v6, Lkotlinx/coroutines/CoroutineScope;

    :try_start_0
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v8, v0

    move-object v0, p0

    move-object v11, v5

    move-object v5, v1

    move-object v1, v11

    goto/16 :goto_2

    :catchall_0
    move-exception p1

    move-object v0, p1

    move-object p1, p0

    goto/16 :goto_3

    .line 275
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 253
    :cond_1
    iget-object v1, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$5:Ljava/lang/Object;

    iget-object v4, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$4:Ljava/lang/Object;

    iget-object v5, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$3:Ljava/lang/Object;

    check-cast v5, Lcom/squareup/workflow/legacy/Reaction;

    iget-object v6, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$2:Ljava/lang/Object;

    check-cast v6, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v7, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$1:Ljava/lang/Object;

    check-cast v7, Lkotlinx/coroutines/channels/SendChannel;

    iget-object v8, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$0:Ljava/lang/Object;

    check-cast v8, Lkotlinx/coroutines/CoroutineScope;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    move-object p1, p0

    move-object v11, v8

    move-object v8, v0

    move-object v0, v5

    move-object v5, v7

    move-object v7, v1

    move-object v1, v4

    move-object v4, v6

    move-object v6, v11

    goto :goto_1

    :cond_2
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    iget-object v1, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->p$0:Lkotlinx/coroutines/channels/SendChannel;

    iget-object v4, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->p$1:Lkotlinx/coroutines/channels/ReceiveChannel;

    .line 254
    new-instance v5, Lcom/squareup/workflow/legacy/EnterState;

    iget-object v6, p0, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->$initialState:Ljava/lang/Object;

    invoke-direct {v5, v6}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast v5, Lcom/squareup/workflow/legacy/Reaction;

    move-object v6, v0

    move-object v0, p1

    move-object p1, p0

    .line 257
    :goto_0
    instance-of v7, v5, Lcom/squareup/workflow/legacy/EnterState;

    if-eqz v7, :cond_6

    .line 258
    move-object v7, v5

    check-cast v7, Lcom/squareup/workflow/legacy/EnterState;

    invoke-virtual {v7}, Lcom/squareup/workflow/legacy/EnterState;->getState()Ljava/lang/Object;

    move-result-object v7

    .line 260
    iput-object v0, p1, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$0:Ljava/lang/Object;

    iput-object v1, p1, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$1:Ljava/lang/Object;

    iput-object v4, p1, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$2:Ljava/lang/Object;

    iput-object v5, p1, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$3:Ljava/lang/Object;

    iput-object v7, p1, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$4:Ljava/lang/Object;

    iput-object v7, p1, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$5:Ljava/lang/Object;

    iput v3, p1, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->label:I

    invoke-interface {v1, v7, p1}, Lkotlinx/coroutines/channels/SendChannel;->send(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v8

    if-ne v8, v6, :cond_3

    return-object v6

    :cond_3
    move-object v8, v6

    move-object v6, v0

    move-object v0, v5

    move-object v5, v1

    move-object v1, v7

    .line 264
    :goto_1
    :try_start_1
    iget-object v9, p1, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->$this_doLaunch:Lcom/squareup/workflow/legacy/Reactor;

    iget-object v10, p1, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    iput-object v6, p1, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$0:Ljava/lang/Object;

    iput-object v5, p1, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$1:Ljava/lang/Object;

    iput-object v4, p1, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$2:Ljava/lang/Object;

    iput-object v0, p1, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$3:Ljava/lang/Object;

    iput-object v1, p1, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$4:Ljava/lang/Object;

    iput-object v7, p1, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->L$5:Ljava/lang/Object;

    iput v2, p1, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->label:I

    invoke-interface {v9, v1, v4, v10, p1}, Lcom/squareup/workflow/legacy/Reactor;->onReact(Ljava/lang/Object;Lkotlinx/coroutines/channels/ReceiveChannel;Lcom/squareup/workflow/legacy/WorkflowPool;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    if-ne v0, v8, :cond_4

    return-object v8

    :cond_4
    move-object v11, v0

    move-object v0, p1

    move-object p1, v11

    move-object v12, v5

    move-object v5, v1

    move-object v1, v12

    .line 253
    :goto_2
    :try_start_2
    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v5, p1

    move-object p1, v0

    move-object v0, v6

    move-object v6, v8

    goto :goto_0

    :catchall_1
    move-exception p1

    move-object v1, v5

    move-object v11, v0

    move-object v0, p1

    move-object p1, v11

    goto :goto_3

    :catchall_2
    move-exception v0

    .line 267
    :goto_3
    instance-of v2, v0, Ljava/util/concurrent/CancellationException;

    if-eqz v2, :cond_5

    throw v0

    .line 268
    :cond_5
    new-instance v2, Lcom/squareup/workflow/legacy/ReactorException;

    .line 270
    iget-object p1, p1, Lcom/squareup/workflow/legacy/ReactorKt$doLaunch$1;->$this_doLaunch:Lcom/squareup/workflow/legacy/Reactor;

    .line 268
    invoke-direct {v2, v0, p1, v1}, Lcom/squareup/workflow/legacy/ReactorException;-><init>(Ljava/lang/Throwable;Lcom/squareup/workflow/legacy/Reactor;Ljava/lang/Object;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    :cond_6
    if-eqz v5, :cond_7

    .line 275
    check-cast v5, Lcom/squareup/workflow/legacy/FinishWith;

    invoke-virtual {v5}, Lcom/squareup/workflow/legacy/FinishWith;->getResult()Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_7
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.workflow.legacy.FinishWith<O>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
