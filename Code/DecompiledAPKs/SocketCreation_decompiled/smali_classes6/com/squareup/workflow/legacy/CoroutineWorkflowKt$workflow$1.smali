.class final Lcom/squareup/workflow/legacy/CoroutineWorkflowKt$workflow$1;
.super Lkotlin/jvm/internal/Lambda;
.source "CoroutineWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacy/CoroutineWorkflowKt;->workflow(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function4;)Lcom/squareup/workflow/legacy/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Throwable;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0005*\u00020\u00032\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "S",
        "",
        "E",
        "O",
        "cancelReason",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $events:Lkotlinx/coroutines/channels/Channel;

.field final synthetic $state:Lkotlinx/coroutines/channels/BroadcastChannel;


# direct methods
.method constructor <init>(Lkotlinx/coroutines/channels/BroadcastChannel;Lkotlinx/coroutines/channels/Channel;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/legacy/CoroutineWorkflowKt$workflow$1;->$state:Lkotlinx/coroutines/channels/BroadcastChannel;

    iput-object p2, p0, Lcom/squareup/workflow/legacy/CoroutineWorkflowKt$workflow$1;->$events:Lkotlinx/coroutines/channels/Channel;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/legacy/CoroutineWorkflowKt$workflow$1;->invoke(Ljava/lang/Throwable;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Throwable;)V
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/workflow/legacy/CoroutineWorkflowKt$workflow$1;->$state:Lkotlinx/coroutines/channels/BroadcastChannel;

    invoke-interface {v0, p1}, Lkotlinx/coroutines/channels/BroadcastChannel;->close(Ljava/lang/Throwable;)Z

    .line 67
    iget-object v0, p0, Lcom/squareup/workflow/legacy/CoroutineWorkflowKt$workflow$1;->$events:Lkotlinx/coroutines/channels/Channel;

    invoke-interface {v0, p1}, Lkotlinx/coroutines/channels/Channel;->close(Ljava/lang/Throwable;)Z

    return-void
.end method
