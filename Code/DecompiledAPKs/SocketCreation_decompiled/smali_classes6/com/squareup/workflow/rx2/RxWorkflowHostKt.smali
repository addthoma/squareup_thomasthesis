.class public final Lcom/squareup/workflow/rx2/RxWorkflowHostKt;
.super Ljava/lang/Object;
.source "RxWorkflowHost.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001ah\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0008\u0008\u0000\u0010\u0004*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0002*\u00020\u0005\"\u0004\u0008\u0002\u0010\u00032\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u0002H\u00040\u00072\u0018\u0010\u0008\u001a\u0014\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\t2\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\u000c\u001a\u00020\r\u00a8\u0006\u000e"
    }
    d2 = {
        "runAsRx",
        "Lcom/squareup/workflow/rx2/RxWorkflowHost;",
        "O",
        "R",
        "I",
        "",
        "inputs",
        "Lkotlinx/coroutines/flow/Flow;",
        "workflow",
        "Lcom/squareup/workflow/Workflow;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "context",
        "Lkotlin/coroutines/CoroutineContext;",
        "runtime-extensions"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final runAsRx(Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Workflow;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;)Lcom/squareup/workflow/rx2/RxWorkflowHost;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlinx/coroutines/flow/Flow<",
            "+TI;>;",
            "Lcom/squareup/workflow/Workflow<",
            "-TI;+TO;+TR;>;",
            "Lcom/squareup/workflow/Snapshot;",
            "Lkotlin/coroutines/CoroutineContext;",
            ")",
            "Lcom/squareup/workflow/rx2/RxWorkflowHost<",
            "TO;TR;>;"
        }
    .end annotation

    const-string v0, "inputs"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-static {p3}, Lkotlinx/coroutines/CoroutineScopeKt;->CoroutineScope(Lkotlin/coroutines/CoroutineContext;)Lkotlinx/coroutines/CoroutineScope;

    move-result-object p3

    .line 30
    sget-object v0, Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1;->INSTANCE:Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    .line 28
    invoke-static {p3, p1, p0, p2, v0}, Lcom/squareup/workflow/LaunchWorkflowKt;->launchWorkflowIn(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/workflow/rx2/RxWorkflowHost;

    return-object p0
.end method

.method public static synthetic runAsRx$default(Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Workflow;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;ILjava/lang/Object;)Lcom/squareup/workflow/rx2/RxWorkflowHost;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p2, 0x0

    .line 26
    check-cast p2, Lcom/squareup/workflow/Snapshot;

    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/workflow/rx2/RxWorkflowHostKt;->runAsRx(Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Workflow;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;)Lcom/squareup/workflow/rx2/RxWorkflowHost;

    move-result-object p0

    return-object p0
.end method
