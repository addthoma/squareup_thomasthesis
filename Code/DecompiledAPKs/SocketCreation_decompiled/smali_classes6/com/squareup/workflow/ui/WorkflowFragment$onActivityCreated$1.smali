.class final synthetic Lcom/squareup/workflow/ui/WorkflowFragment$onActivityCreated$1;
.super Lkotlin/jvm/internal/FunctionReference;
.source "WorkflowFragment.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/ui/WorkflowFragment;->onActivityCreated(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/FunctionReference;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/squareup/workflow/ui/WorkflowRunner$Config<",
        "TPropsT;TOutputT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\u0008\u0000\u0010\u0002\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0004\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/ui/WorkflowRunner$Config;",
        "PropsT",
        "OutputT",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/workflow/ui/WorkflowFragment;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lkotlin/jvm/internal/FunctionReference;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    const-string v0, "onCreateWorkflow"

    return-object v0
.end method

.method public final getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 1

    const-class v0, Lcom/squareup/workflow/ui/WorkflowFragment;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    return-object v0
.end method

.method public final getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "onCreateWorkflow()Lcom/squareup/workflow/ui/WorkflowRunner$Config;"

    return-object v0
.end method

.method public final invoke()Lcom/squareup/workflow/ui/WorkflowRunner$Config;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/ui/WorkflowRunner$Config<",
            "TPropsT;TOutputT;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/ui/WorkflowFragment$onActivityCreated$1;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/workflow/ui/WorkflowFragment;

    .line 88
    invoke-virtual {v0}, Lcom/squareup/workflow/ui/WorkflowFragment;->onCreateWorkflow()Lcom/squareup/workflow/ui/WorkflowRunner$Config;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 57
    invoke-virtual {p0}, Lcom/squareup/workflow/ui/WorkflowFragment$onActivityCreated$1;->invoke()Lcom/squareup/workflow/ui/WorkflowRunner$Config;

    move-result-object v0

    return-object v0
.end method
