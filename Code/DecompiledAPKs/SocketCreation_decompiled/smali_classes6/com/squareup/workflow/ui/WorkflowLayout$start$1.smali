.class final Lcom/squareup/workflow/ui/WorkflowLayout$start$1;
.super Lkotlin/jvm/internal/Lambda;
.source "WorkflowLayout.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/ui/WorkflowLayout;->start(Lio/reactivex/Observable;Lcom/squareup/workflow/ui/ContainerHints;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Object;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $hintsWithDefaults:Lcom/squareup/workflow/ui/ContainerHints;

.field final synthetic this$0:Lcom/squareup/workflow/ui/WorkflowLayout;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/ui/WorkflowLayout;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/ui/WorkflowLayout$start$1;->this$0:Lcom/squareup/workflow/ui/WorkflowLayout;

    iput-object p2, p0, Lcom/squareup/workflow/ui/WorkflowLayout$start$1;->$hintsWithDefaults:Lcom/squareup/workflow/ui/ContainerHints;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 36
    invoke-virtual {p0, p1}, Lcom/squareup/workflow/ui/WorkflowLayout$start$1;->invoke(Ljava/lang/Object;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Object;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/workflow/ui/WorkflowLayout$start$1;->this$0:Lcom/squareup/workflow/ui/WorkflowLayout;

    iget-object v1, p0, Lcom/squareup/workflow/ui/WorkflowLayout$start$1;->$hintsWithDefaults:Lcom/squareup/workflow/ui/ContainerHints;

    invoke-static {v0, p1, v1}, Lcom/squareup/workflow/ui/WorkflowLayout;->access$show(Lcom/squareup/workflow/ui/WorkflowLayout;Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
