.class public final Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion;
.super Ljava/lang/Object;
.source "BackStackContainer.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/ViewBinding;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/ui/backstack/BackStackContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/ViewBinding<",
        "Lcom/squareup/workflow/ui/backstack/BackStackScreen<",
        "*>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J1\u0010\u0008\u001a\u00020\t2\n\u0010\n\u001a\u0006\u0012\u0002\u0008\u00030\u00022\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0096\u0001R\u001c\u0010\u0004\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00020\u0005X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion;",
        "Lcom/squareup/workflow/ui/ViewBinding;",
        "Lcom/squareup/workflow/ui/backstack/BackStackScreen;",
        "()V",
        "type",
        "Lkotlin/reflect/KClass;",
        "getType",
        "()Lkotlin/reflect/KClass;",
        "buildView",
        "Landroid/view/View;",
        "initialRendering",
        "initialContainerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "contextForNewView",
        "Landroid/content/Context;",
        "container",
        "Landroid/view/ViewGroup;",
        "backstack-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/workflow/ui/BuilderBinding;


# direct methods
.method private constructor <init>()V
    .locals 3

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    new-instance v0, Lcom/squareup/workflow/ui/BuilderBinding;

    .line 156
    const-class v1, Lcom/squareup/workflow/ui/backstack/BackStackScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    .line 157
    sget-object v2, Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion$1;->INSTANCE:Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion$1;

    check-cast v2, Lkotlin/jvm/functions/Function4;

    .line 155
    invoke-direct {v0, v1, v2}, Lcom/squareup/workflow/ui/BuilderBinding;-><init>(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function4;)V

    iput-object v0, p0, Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion;->$$delegate_0:Lcom/squareup/workflow/ui/BuilderBinding;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 154
    invoke-direct {p0}, Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public buildView(Lcom/squareup/workflow/ui/backstack/BackStackScreen;Lcom/squareup/workflow/ui/ContainerHints;Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/ui/backstack/BackStackScreen<",
            "*>;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "initialRendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialContainerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contextForNewView"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion;->$$delegate_0:Lcom/squareup/workflow/ui/BuilderBinding;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/workflow/ui/BuilderBinding;->buildView(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic buildView(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    .line 154
    check-cast p1, Lcom/squareup/workflow/ui/backstack/BackStackScreen;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion;->buildView(Lcom/squareup/workflow/ui/backstack/BackStackScreen;Lcom/squareup/workflow/ui/ContainerHints;Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getType()Lkotlin/reflect/KClass;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/reflect/KClass<",
            "Lcom/squareup/workflow/ui/backstack/BackStackScreen<",
            "*>;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion;->$$delegate_0:Lcom/squareup/workflow/ui/BuilderBinding;

    invoke-virtual {v0}, Lcom/squareup/workflow/ui/BuilderBinding;->getType()Lkotlin/reflect/KClass;

    move-result-object v0

    return-object v0
.end method
