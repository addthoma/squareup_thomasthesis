.class final Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$result$1;
.super Ljava/lang/Object;
.source "WorkflowRunnerViewModel.kt"

# interfaces
.implements Lio/reactivex/functions/Action;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;-><init>(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/WorkflowSession;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "OutputT",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$result$1;->this$0:Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 71
    iget-object v0, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$result$1;->this$0:Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;

    invoke-static {v0}, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->access$getScope$p(Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;)Lkotlinx/coroutines/CoroutineScope;

    move-result-object v0

    new-instance v1, Ljava/util/concurrent/CancellationException;

    const-string v2, "WorkflowRunnerViewModel delivered result"

    invoke-direct {v1, v2}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lkotlinx/coroutines/CoroutineScopeKt;->cancel(Lkotlinx/coroutines/CoroutineScope;Ljava/util/concurrent/CancellationException;)V

    return-void
.end method
