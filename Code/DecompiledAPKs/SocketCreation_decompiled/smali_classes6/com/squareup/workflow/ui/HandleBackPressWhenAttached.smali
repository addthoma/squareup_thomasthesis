.class final Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;
.super Ljava/lang/Object;
.source "BackPressHandler.kt"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBackPressHandler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BackPressHandler.kt\ncom/squareup/workflow/ui/HandleBackPressWhenAttached\n*L\n1#1,83:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\'\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0008\t*\u0001\u000c\u0008\u0002\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0010\u0010\u0004\u001a\u000c\u0012\u0004\u0012\u00020\u00060\u0005j\u0002`\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u000e\u001a\u00020\u00062\u0006\u0010\u000f\u001a\u00020\u0003H\u0016J\u0010\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u0011\u001a\u00020\u0003H\u0016J\u0006\u0010\u0012\u001a\u00020\u0006J\u0006\u0010\u0013\u001a\u00020\u0006R\u001b\u0010\u0004\u001a\u000c\u0012\u0004\u0012\u00020\u00060\u0005j\u0002`\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0010\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\rR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;",
        "Landroid/view/View$OnAttachStateChangeListener;",
        "view",
        "Landroid/view/View;",
        "handler",
        "Lkotlin/Function0;",
        "",
        "Lcom/squareup/workflow/ui/BackPressHandler;",
        "(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V",
        "getHandler",
        "()Lkotlin/jvm/functions/Function0;",
        "onBackPressedCallback",
        "com/squareup/workflow/ui/HandleBackPressWhenAttached$onBackPressedCallback$1",
        "Lcom/squareup/workflow/ui/HandleBackPressWhenAttached$onBackPressedCallback$1;",
        "onViewAttachedToWindow",
        "attachedView",
        "onViewDetachedFromWindow",
        "detachedView",
        "start",
        "stop",
        "core-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final handler:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onBackPressedCallback:Lcom/squareup/workflow/ui/HandleBackPressWhenAttached$onBackPressedCallback$1;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;->view:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;->handler:Lkotlin/jvm/functions/Function0;

    .line 48
    new-instance p1, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached$onBackPressedCallback$1;

    const/4 p2, 0x1

    invoke-direct {p1, p0, p2}, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached$onBackPressedCallback$1;-><init>(Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;Z)V

    iput-object p1, p0, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;->onBackPressedCallback:Lcom/squareup/workflow/ui/HandleBackPressWhenAttached$onBackPressedCallback$1;

    return-void
.end method


# virtual methods
.method public final getHandler()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;->handler:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 2

    const-string v0, "attachedView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;->view:Landroid/view/View;

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_2

    .line 71
    iget-object p1, p0, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;->view:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "view.context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/workflow/ui/BackPressHandlerKt;->onBackPressedDispatcherOwnerOrNull(Landroid/content/Context;)Landroidx/activity/OnBackPressedDispatcherOwner;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 73
    invoke-interface {p1}, Landroidx/activity/OnBackPressedDispatcherOwner;->getOnBackPressedDispatcher()Landroidx/activity/OnBackPressedDispatcher;

    move-result-object v0

    check-cast p1, Landroidx/lifecycle/LifecycleOwner;

    iget-object v1, p0, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;->onBackPressedCallback:Lcom/squareup/workflow/ui/HandleBackPressWhenAttached$onBackPressedCallback$1;

    check-cast v1, Landroidx/activity/OnBackPressedCallback;

    invoke-virtual {v0, p1, v1}, Landroidx/activity/OnBackPressedDispatcher;->addCallback(Landroidx/lifecycle/LifecycleOwner;Landroidx/activity/OnBackPressedCallback;)V

    :cond_1
    return-void

    .line 70
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Failed requirement."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 1

    const-string v0, "detachedView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;->view:Landroid/view/View;

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    .line 66
    iget-object p1, p0, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;->onBackPressedCallback:Lcom/squareup/workflow/ui/HandleBackPressWhenAttached$onBackPressedCallback$1;

    invoke-virtual {p1}, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached$onBackPressedCallback$1;->remove()V

    return-void

    .line 65
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Failed requirement."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final start()V
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;->view:Landroid/view/View;

    move-object v1, p0

    check-cast v1, Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;->view:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;->onViewAttachedToWindow(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public final stop()V
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;->view:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;->onViewDetachedFromWindow(Landroid/view/View;)V

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/squareup/workflow/ui/HandleBackPressWhenAttached;->view:Landroid/view/View;

    move-object v1, p0

    check-cast v1, Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    return-void
.end method
