.class public final Lcom/squareup/workflow/ui/ViewRegistry$Companion;
.super Lcom/squareup/workflow/ui/ContainerHintKey;
.source "ViewRegistry.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/ui/ViewRegistry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/ui/ContainerHintKey<",
        "Lcom/squareup/workflow/ui/ViewRegistry;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViewRegistry.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ViewRegistry.kt\ncom/squareup/workflow/ui/ViewRegistry$Companion\n*L\n1#1,126:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0086\u0003\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003R\u0014\u0010\u0004\u001a\u00020\u00028VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/workflow/ui/ViewRegistry$Companion;",
        "Lcom/squareup/workflow/ui/ContainerHintKey;",
        "Lcom/squareup/workflow/ui/ViewRegistry;",
        "()V",
        "default",
        "getDefault",
        "()Lcom/squareup/workflow/ui/ViewRegistry;",
        "core-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/workflow/ui/ViewRegistry$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 86
    new-instance v0, Lcom/squareup/workflow/ui/ViewRegistry$Companion;

    invoke-direct {v0}, Lcom/squareup/workflow/ui/ViewRegistry$Companion;-><init>()V

    sput-object v0, Lcom/squareup/workflow/ui/ViewRegistry$Companion;->$$INSTANCE:Lcom/squareup/workflow/ui/ViewRegistry$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 86
    const-class v0, Lcom/squareup/workflow/ui/ViewRegistry;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/workflow/ui/ContainerHintKey;-><init>(Lkotlin/reflect/KClass;)V

    return-void
.end method


# virtual methods
.method public getDefault()Lcom/squareup/workflow/ui/ViewRegistry;
    .locals 2

    .line 88
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There should always be a ViewRegistry hint, this is bug in Workflow."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic getDefault()Ljava/lang/Object;
    .locals 1

    .line 86
    invoke-virtual {p0}, Lcom/squareup/workflow/ui/ViewRegistry$Companion;->getDefault()Lcom/squareup/workflow/ui/ViewRegistry;

    move-result-object v0

    return-object v0
.end method
