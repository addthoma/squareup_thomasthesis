.class public abstract Lcom/squareup/workflow/ui/WorkflowFragment;
.super Landroidx/fragment/app/Fragment;
.source "WorkflowFragment.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PropsT:",
        "Ljava/lang/Object;",
        "OutputT:",
        "Ljava/lang/Object;",
        ">",
        "Landroidx/fragment/app/Fragment;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008&\u0018\u0000*\u0004\u0008\u0000\u0010\u0001*\u0008\u0008\u0001\u0010\u0002*\u00020\u00032\u00020\u0004B\u0005\u00a2\u0006\u0002\u0010\u0005J\u0012\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0016J\"\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012J\u0014\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u001aH$R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00028\u00010\u0007X\u0082.\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0008\u001a\u00020\tX\u00a4\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000bR\u001a\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00028\u00010\u00078DX\u0084\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/workflow/ui/WorkflowFragment;",
        "PropsT",
        "OutputT",
        "",
        "Landroidx/fragment/app/Fragment;",
        "()V",
        "_runner",
        "Lcom/squareup/workflow/ui/WorkflowRunner;",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "getContainerHints",
        "()Lcom/squareup/workflow/ui/ContainerHints;",
        "runner",
        "getRunner",
        "()Lcom/squareup/workflow/ui/WorkflowRunner;",
        "onActivityCreated",
        "",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onCreateView",
        "Lcom/squareup/workflow/ui/WorkflowLayout;",
        "inflater",
        "Landroid/view/LayoutInflater;",
        "container",
        "Landroid/view/ViewGroup;",
        "onCreateWorkflow",
        "Lcom/squareup/workflow/ui/WorkflowRunner$Config;",
        "core-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private _runner:Lcom/squareup/workflow/ui/WorkflowRunner;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/ui/WorkflowRunner<",
            "+TOutputT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 57
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract getContainerHints()Lcom/squareup/workflow/ui/ContainerHints;
.end method

.method protected final getRunner()Lcom/squareup/workflow/ui/WorkflowRunner;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/ui/WorkflowRunner<",
            "TOutputT;>;"
        }
    .end annotation

    .line 75
    iget-object v0, p0, Lcom/squareup/workflow/ui/WorkflowFragment;->_runner:Lcom/squareup/workflow/ui/WorkflowRunner;

    if-nez v0, :cond_0

    const-string v1, "_runner"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .line 86
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 88
    sget-object p1, Lcom/squareup/workflow/ui/WorkflowRunner;->Companion:Lcom/squareup/workflow/ui/WorkflowRunner$Companion;

    move-object v0, p0

    check-cast v0, Landroidx/fragment/app/Fragment;

    new-instance v1, Lcom/squareup/workflow/ui/WorkflowFragment$onActivityCreated$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/workflow/ui/WorkflowFragment;

    invoke-direct {v1, v2}, Lcom/squareup/workflow/ui/WorkflowFragment$onActivityCreated$1;-><init>(Lcom/squareup/workflow/ui/WorkflowFragment;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/workflow/ui/WorkflowRunner$Companion;->startWorkflow(Landroidx/fragment/app/Fragment;Lkotlin/jvm/functions/Function0;)Lcom/squareup/workflow/ui/WorkflowRunner;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/workflow/ui/WorkflowFragment;->_runner:Lcom/squareup/workflow/ui/WorkflowRunner;

    .line 90
    invoke-virtual {p0}, Lcom/squareup/workflow/ui/WorkflowFragment;->getView()Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Lcom/squareup/workflow/ui/WorkflowLayout;

    invoke-virtual {p0}, Lcom/squareup/workflow/ui/WorkflowFragment;->getRunner()Lcom/squareup/workflow/ui/WorkflowRunner;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/workflow/ui/WorkflowRunner;->getRenderings()Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/workflow/ui/WorkflowFragment;->getContainerHints()Lcom/squareup/workflow/ui/ContainerHints;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/workflow/ui/WorkflowLayout;->start(Lio/reactivex/Observable;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.workflow.ui.WorkflowLayout"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 0

    .line 57
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/workflow/ui/WorkflowFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Lcom/squareup/workflow/ui/WorkflowLayout;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Lcom/squareup/workflow/ui/WorkflowLayout;
    .locals 1

    const-string p2, "inflater"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    new-instance p2, Lcom/squareup/workflow/ui/WorkflowLayout;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string p3, "inflater.context"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p3, 0x0

    const/4 v0, 0x2

    invoke-direct {p2, p1, p3, v0, p3}, Lcom/squareup/workflow/ui/WorkflowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p2
.end method

.method protected abstract onCreateWorkflow()Lcom/squareup/workflow/ui/WorkflowRunner$Config;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/ui/WorkflowRunner$Config<",
            "TPropsT;TOutputT;>;"
        }
    .end annotation
.end method
