.class Lcom/starmicronics/starmgsio/P;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/starmicronics/starmgsio/y;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/starmgsio/P$a;
    }
.end annotation


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/starmicronics/starmgsio/ConnectionInfo;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/content/Context;

.field private c:Lcom/starmicronics/starmgsio/n;

.field private d:Lcom/starmicronics/starmgsio/S;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/starmicronics/starmgsio/P;->a:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/starmicronics/starmgsio/P;->c:Lcom/starmicronics/starmgsio/n;

    iput-object v0, p0, Lcom/starmicronics/starmgsio/P;->d:Lcom/starmicronics/starmgsio/S;

    iput-object p1, p0, Lcom/starmicronics/starmgsio/P;->b:Landroid/content/Context;

    return-void
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/P;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/P;->b:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/P;Lcom/starmicronics/starmgsio/S;)Lcom/starmicronics/starmgsio/S;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/P;->d:Lcom/starmicronics/starmgsio/S;

    return-object p1
.end method

.method static synthetic b(Lcom/starmicronics/starmgsio/P;)Lcom/starmicronics/starmgsio/S;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/P;->d:Lcom/starmicronics/starmgsio/S;

    return-object p0
.end method

.method static synthetic c(Lcom/starmicronics/starmgsio/P;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/P;->a:Ljava/util/ArrayList;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/P;->c:Lcom/starmicronics/starmgsio/n;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/starmicronics/starmgsio/n;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/starmicronics/starmgsio/P;->c:Lcom/starmicronics/starmgsio/n;

    iget-object v0, p0, Lcom/starmicronics/starmgsio/P;->d:Lcom/starmicronics/starmgsio/S;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/starmicronics/starmgsio/S;->a()V

    :cond_0
    return-void
.end method

.method public a(Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;)V
    .locals 3

    iget-object v0, p0, Lcom/starmicronics/starmgsio/P;->c:Lcom/starmicronics/starmgsio/n;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/starmicronics/starmgsio/P;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    new-instance v0, Lcom/starmicronics/starmgsio/P$a;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, p1, v1}, Lcom/starmicronics/starmgsio/P$a;-><init>(Lcom/starmicronics/starmgsio/P;Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/starmicronics/starmgsio/P;->c:Lcom/starmicronics/starmgsio/n;

    iget-object p1, p0, Lcom/starmicronics/starmgsio/P;->c:Lcom/starmicronics/starmgsio/n;

    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    :cond_1
    return-void
.end method
