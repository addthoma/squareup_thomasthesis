.class Lcom/starmicronics/starmgsio/P$a;
.super Lcom/starmicronics/starmgsio/n;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starmgsio/P;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field private b:Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;

.field private c:Landroid/os/Handler;

.field final synthetic d:Lcom/starmicronics/starmgsio/P;


# direct methods
.method constructor <init>(Lcom/starmicronics/starmgsio/P;Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/P$a;->d:Lcom/starmicronics/starmgsio/P;

    invoke-direct {p0}, Lcom/starmicronics/starmgsio/n;-><init>()V

    iput-object p2, p0, Lcom/starmicronics/starmgsio/P$a;->b:Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;

    iput-object p3, p0, Lcom/starmicronics/starmgsio/P$a;->c:Landroid/os/Handler;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/starmicronics/starmgsio/n;->a:Z

    return-void
.end method

.method private a(Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;)V
    .locals 9

    iget-object v0, p0, Lcom/starmicronics/starmgsio/P$a;->d:Lcom/starmicronics/starmgsio/P;

    invoke-static {v0}, Lcom/starmicronics/starmgsio/P;->a(Lcom/starmicronics/starmgsio/P;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "usb"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/usb/UsbManager;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getInterfaceCount()I

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v2

    sget v3, Lcom/starmicronics/starmgsio/Q;->a:I

    if-eq v2, v3, :cond_3

    goto :goto_0

    :cond_3
    sget-object v2, Lcom/starmicronics/starmgsio/Q;->b:[I

    array-length v3, v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_1
    const/4 v6, 0x1

    if-ge v5, v3, :cond_5

    aget v7, v2, v5

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v8

    if-ne v7, v8, :cond_4

    const/4 v2, 0x1

    goto :goto_2

    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_5
    const/4 v2, 0x0

    :goto_2
    if-nez v2, :cond_6

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/starmicronics/starmgsio/P$a;->d:Lcom/starmicronics/starmgsio/P;

    new-instance v3, Lcom/starmicronics/starmgsio/S;

    invoke-static {v2}, Lcom/starmicronics/starmgsio/P;->a(Lcom/starmicronics/starmgsio/P;)Landroid/content/Context;

    move-result-object v5

    invoke-direct {v3, v5}, Lcom/starmicronics/starmgsio/S;-><init>(Landroid/content/Context;)V

    invoke-static {v2, v3}, Lcom/starmicronics/starmgsio/P;->a(Lcom/starmicronics/starmgsio/P;Lcom/starmicronics/starmgsio/S;)Lcom/starmicronics/starmgsio/S;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1d

    if-gt v3, v2, :cond_7

    iget-object v2, p0, Lcom/starmicronics/starmgsio/P$a;->d:Lcom/starmicronics/starmgsio/P;

    invoke-static {v2}, Lcom/starmicronics/starmgsio/P;->b(Lcom/starmicronics/starmgsio/P;)Lcom/starmicronics/starmgsio/S;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/starmicronics/starmgsio/S;->a(Landroid/hardware/usb/UsbDevice;)Z

    move-result v2

    if-nez v2, :cond_7

    goto :goto_0

    :cond_7
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/starmicronics/starmgsio/T;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getProductName()Ljava/lang/String;

    move-result-object v1

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/starmicronics/starmgsio/P$a;->d:Lcom/starmicronics/starmgsio/P;

    invoke-static {v3}, Lcom/starmicronics/starmgsio/P;->c(Lcom/starmicronics/starmgsio/P;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/starmicronics/starmgsio/ConnectionInfo;

    invoke-virtual {v5}, Lcom/starmicronics/starmgsio/ConnectionInfo;->getIdentifier()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_8

    const/4 v4, 0x1

    :cond_9
    if-nez v4, :cond_1

    new-instance v3, Lcom/starmicronics/starmgsio/z$a;

    invoke-direct {v3}, Lcom/starmicronics/starmgsio/z$a;-><init>()V

    invoke-virtual {v3, v2, v1}, Lcom/starmicronics/starmgsio/z$a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/starmicronics/starmgsio/z$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/starmicronics/starmgsio/z$a;->build()Lcom/starmicronics/starmgsio/z;

    move-result-object v1

    iget-object v2, p0, Lcom/starmicronics/starmgsio/P$a;->d:Lcom/starmicronics/starmgsio/P;

    invoke-static {v2}, Lcom/starmicronics/starmgsio/P;->c(Lcom/starmicronics/starmgsio/P;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/starmicronics/starmgsio/P$a;->c:Landroid/os/Handler;

    new-instance v3, Lcom/starmicronics/starmgsio/O;

    invoke-direct {v3, p0, p1, v1}, Lcom/starmicronics/starmgsio/O;-><init>(Lcom/starmicronics/starmgsio/P$a;Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;Lcom/starmicronics/starmgsio/z;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    :cond_a
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    :goto_0
    iget-boolean v0, p0, Lcom/starmicronics/starmgsio/n;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/starmicronics/starmgsio/P$a;->b:Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;

    invoke-direct {p0, v0}, Lcom/starmicronics/starmgsio/P$a;->a(Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;)V

    const-wide/16 v0, 0x1f4

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    :cond_0
    return-void
.end method
