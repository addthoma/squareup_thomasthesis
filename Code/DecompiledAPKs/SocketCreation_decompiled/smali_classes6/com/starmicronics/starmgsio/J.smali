.class Lcom/starmicronics/starmgsio/J;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/starmicronics/starmgsio/ScaleSetting;",
            "[B>;"
        }
    .end annotation
.end field

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/starmicronics/starmgsio/ScaleSetting;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private c:[B


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/starmicronics/starmgsio/J;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/starmicronics/starmgsio/J;->b:Ljava/util/Map;

    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/starmicronics/starmgsio/J;->c:[B

    return-void
.end method

.method private a([BI)I
    .locals 7

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    if-eqz p1, :cond_7

    if-ltz p2, :cond_7

    array-length v2, p1

    if-ge v2, p2, :cond_0

    goto :goto_3

    :cond_0
    array-length v2, p1

    const/4 v3, 0x1

    if-ge v2, v3, :cond_1

    return v0

    :cond_1
    array-length v2, p1

    sub-int/2addr v2, p2

    const/4 v4, 0x5

    if-ge v2, v4, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x5

    :goto_0
    new-array v2, v2, [B

    array-length v5, v2

    invoke-static {p1, p2, v2, v0, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 p2, 0x0

    new-array v5, v4, [B

    fill-array-data v5, :array_0

    invoke-static {v2, v0, v5}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :goto_1
    const/4 v3, 0x5

    goto :goto_2

    :cond_3
    new-array v5, v3, [B

    const/4 v6, 0x6

    aput-byte v6, v5, v0

    invoke-static {v2, v0, v5}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_2

    :cond_4
    new-array v5, v4, [B

    fill-array-data v5, :array_1

    invoke-static {p1, v0, v5}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v5

    if-eqz v5, :cond_5

    goto :goto_1

    :cond_5
    new-array v4, v3, [B

    const/16 v5, 0x15

    aput-byte v5, v4, v0

    invoke-static {p1, v0, v4}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result p1

    if-eqz p1, :cond_6

    goto :goto_2

    :cond_6
    move-object v1, p2

    const/4 v3, 0x0

    :goto_2
    sget-object p1, Lcom/starmicronics/starmgsio/ScaleSetting;->ZeroPointAdjustment:Lcom/starmicronics/starmgsio/ScaleSetting;

    invoke-direct {p0, p1, v1}, Lcom/starmicronics/starmgsio/J;->a(Lcom/starmicronics/starmgsio/ScaleSetting;Ljava/lang/Boolean;)V

    sget-object p1, Lcom/starmicronics/starmgsio/ScaleSetting;->ZeroPointAdjustment:Lcom/starmicronics/starmgsio/ScaleSetting;

    invoke-direct {p0, p1, v2}, Lcom/starmicronics/starmgsio/J;->a(Lcom/starmicronics/starmgsio/ScaleSetting;[B)V

    return v3

    :cond_7
    :goto_3
    return v0

    nop

    :array_0
    .array-data 1
        0x41t
        0x30t
        0x30t
        0xdt
        0xat
    .end array-data

    nop

    :array_1
    .array-data 1
        0x45t
        0x30t
        0x31t
        0xdt
        0xat
    .end array-data
.end method

.method private a(Lcom/starmicronics/starmgsio/ScaleSetting;Ljava/lang/Boolean;)V
    .locals 1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/starmicronics/starmgsio/J;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private a(Lcom/starmicronics/starmgsio/ScaleSetting;[B)V
    .locals 1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/starmicronics/starmgsio/J;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method


# virtual methods
.method a(Lcom/starmicronics/starmgsio/ScaleSetting;)Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/J;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object v0, p0, Lcom/starmicronics/starmgsio/J;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    return-object p1
.end method

.method a([B)V
    .locals 7

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_4

    invoke-static {}, Lcom/starmicronics/starmgsio/ScaleSetting;->values()[Lcom/starmicronics/starmgsio/ScaleSetting;

    move-result-object v2

    array-length v3, v2

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v3, :cond_2

    aget-object v5, v2, v4

    invoke-virtual {p0, v5}, Lcom/starmicronics/starmgsio/J;->a(Lcom/starmicronics/starmgsio/ScaleSetting;)Ljava/lang/Boolean;

    move-result-object v6

    if-eqz v6, :cond_0

    goto :goto_2

    :cond_0
    sget-object v6, Lcom/starmicronics/starmgsio/I;->a:[I

    invoke-virtual {v5}, Ljava/lang/Enum;->ordinal()I

    move-result v5

    aget v5, v6, v5

    invoke-direct {p0, p1, v1}, Lcom/starmicronics/starmgsio/J;->a([BI)I

    move-result v5

    if-lez v5, :cond_1

    add-int/2addr v1, v5

    const/4 v2, 0x1

    goto :goto_3

    :cond_1
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_3
    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/starmicronics/starmgsio/J;->c:[B

    aget-byte v3, p1, v1

    invoke-static {v2, v3}, Lcom/starmicronics/starmgsio/U;->a([BB)[B

    move-result-object v2

    iput-object v2, p0, Lcom/starmicronics/starmgsio/J;->c:[B

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    return-void
.end method

.method a()[B
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/J;->c:[B

    return-object v0
.end method
