.class public final enum Lcom/starmicronics/starmgsio/ScaleData$Unit;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starmgsio/ScaleData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Unit"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starmgsio/ScaleData$Unit;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/starmicronics/starmgsio/ScaleData$Unit;

.field public static final enum BAT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

.field public static final enum COEFFICIENT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

.field public static final enum CT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

.field public static final enum DWT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

.field public static final enum G:Lcom/starmicronics/starmgsio/ScaleData$Unit;

.field public static final enum GN:Lcom/starmicronics/starmgsio/ScaleData$Unit;

.field public static final enum INVALID:Lcom/starmicronics/starmgsio/ScaleData$Unit;

.field public static final enum LB:Lcom/starmicronics/starmgsio/ScaleData$Unit;

.field public static final enum MG:Lcom/starmicronics/starmgsio/ScaleData$Unit;

.field public static final enum MOM:Lcom/starmicronics/starmgsio/ScaleData$Unit;

.field public static final enum MSG:Lcom/starmicronics/starmgsio/ScaleData$Unit;

.field public static final enum OZ:Lcom/starmicronics/starmgsio/ScaleData$Unit;

.field public static final enum OZT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

.field public static final enum PCS:Lcom/starmicronics/starmgsio/ScaleData$Unit;

.field public static final enum PERCENT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

.field public static final enum TLH:Lcom/starmicronics/starmgsio/ScaleData$Unit;

.field public static final enum TLS:Lcom/starmicronics/starmgsio/ScaleData$Unit;

.field public static final enum TLT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

.field public static final enum TO:Lcom/starmicronics/starmgsio/ScaleData$Unit;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/4 v1, 0x0

    const-string v2, "INVALID"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starmgsio/ScaleData$Unit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->INVALID:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/4 v2, 0x1

    const-string v3, "MG"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starmgsio/ScaleData$Unit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->MG:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/4 v3, 0x2

    const-string v4, "G"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/starmgsio/ScaleData$Unit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->G:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/4 v4, 0x3

    const-string v5, "CT"

    invoke-direct {v0, v5, v4}, Lcom/starmicronics/starmgsio/ScaleData$Unit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->CT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/4 v5, 0x4

    const-string v6, "MOM"

    invoke-direct {v0, v6, v5}, Lcom/starmicronics/starmgsio/ScaleData$Unit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->MOM:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/4 v6, 0x5

    const-string v7, "OZ"

    invoke-direct {v0, v7, v6}, Lcom/starmicronics/starmgsio/ScaleData$Unit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->OZ:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/4 v7, 0x6

    const-string v8, "LB"

    invoke-direct {v0, v8, v7}, Lcom/starmicronics/starmgsio/ScaleData$Unit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->LB:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/4 v8, 0x7

    const-string v9, "OZT"

    invoke-direct {v0, v9, v8}, Lcom/starmicronics/starmgsio/ScaleData$Unit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->OZT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/16 v9, 0x8

    const-string v10, "DWT"

    invoke-direct {v0, v10, v9}, Lcom/starmicronics/starmgsio/ScaleData$Unit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->DWT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/16 v10, 0x9

    const-string v11, "GN"

    invoke-direct {v0, v11, v10}, Lcom/starmicronics/starmgsio/ScaleData$Unit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->GN:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/16 v11, 0xa

    const-string v12, "TLH"

    invoke-direct {v0, v12, v11}, Lcom/starmicronics/starmgsio/ScaleData$Unit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->TLH:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/16 v12, 0xb

    const-string v13, "TLS"

    invoke-direct {v0, v13, v12}, Lcom/starmicronics/starmgsio/ScaleData$Unit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->TLS:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/16 v13, 0xc

    const-string v14, "TLT"

    invoke-direct {v0, v14, v13}, Lcom/starmicronics/starmgsio/ScaleData$Unit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->TLT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/16 v14, 0xd

    const-string v15, "TO"

    invoke-direct {v0, v15, v14}, Lcom/starmicronics/starmgsio/ScaleData$Unit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->TO:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/16 v15, 0xe

    const-string v14, "MSG"

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starmgsio/ScaleData$Unit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->MSG:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const-string v14, "BAT"

    const/16 v15, 0xf

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starmgsio/ScaleData$Unit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->BAT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const-string v14, "PCS"

    const/16 v15, 0x10

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starmgsio/ScaleData$Unit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->PCS:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const-string v14, "PERCENT"

    const/16 v15, 0x11

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starmgsio/ScaleData$Unit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->PERCENT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const-string v14, "COEFFICIENT"

    const/16 v15, 0x12

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starmgsio/ScaleData$Unit;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->COEFFICIENT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/16 v0, 0x13

    new-array v0, v0, [Lcom/starmicronics/starmgsio/ScaleData$Unit;

    sget-object v14, Lcom/starmicronics/starmgsio/ScaleData$Unit;->INVALID:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    aput-object v14, v0, v1

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->MG:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->G:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    aput-object v1, v0, v3

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->CT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    aput-object v1, v0, v4

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->MOM:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    aput-object v1, v0, v5

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->OZ:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    aput-object v1, v0, v6

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->LB:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    aput-object v1, v0, v7

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->OZT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    aput-object v1, v0, v8

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->DWT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    aput-object v1, v0, v9

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->GN:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    aput-object v1, v0, v10

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->TLH:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    aput-object v1, v0, v11

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->TLS:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    aput-object v1, v0, v12

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->TLT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    aput-object v1, v0, v13

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->TO:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->MSG:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->BAT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->PCS:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->PERCENT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->COEFFICIENT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->$VALUES:[Lcom/starmicronics/starmgsio/ScaleData$Unit;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starmgsio/ScaleData$Unit;
    .locals 1

    const-class v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starmgsio/ScaleData$Unit;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starmgsio/ScaleData$Unit;
    .locals 1

    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->$VALUES:[Lcom/starmicronics/starmgsio/ScaleData$Unit;

    invoke-virtual {v0}, [Lcom/starmicronics/starmgsio/ScaleData$Unit;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starmgsio/ScaleData$Unit;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/starmicronics/starmgsio/E;->a:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "INVALID"

    goto :goto_0

    :pswitch_0
    const-string v0, "MUL"

    goto :goto_0

    :pswitch_1
    const-string v0, "%"

    goto :goto_0

    :pswitch_2
    const-string v0, "PCS"

    goto :goto_0

    :pswitch_3
    const-string v0, "bht"

    goto :goto_0

    :pswitch_4
    const-string v0, "msg"

    goto :goto_0

    :pswitch_5
    const-string v0, "tola"

    goto :goto_0

    :pswitch_6
    const-string v0, "tl"

    goto :goto_0

    :pswitch_7
    const-string v0, "gr"

    goto :goto_0

    :pswitch_8
    const-string v0, "dwt"

    goto :goto_0

    :pswitch_9
    const-string v0, "ozt"

    goto :goto_0

    :pswitch_a
    const-string v0, "lb"

    goto :goto_0

    :pswitch_b
    const-string v0, "oz"

    goto :goto_0

    :pswitch_c
    const-string v0, "mom"

    goto :goto_0

    :pswitch_d
    const-string v0, "ct"

    goto :goto_0

    :pswitch_e
    const-string v0, "g"

    goto :goto_0

    :pswitch_f
    const-string v0, "mg"

    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
