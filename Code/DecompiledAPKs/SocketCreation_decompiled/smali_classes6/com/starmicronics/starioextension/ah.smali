.class abstract Lcom/starmicronics/starioextension/ah;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/starioextension/ah$a;,
        Lcom/starmicronics/starioextension/ah$b;
    }
.end annotation


# static fields
.field static final a:I = 0x18

.field private static final b:I = 0x800

.field private static final c:I = 0xc

.field private static final d:[[I


# instance fields
.field private e:I

.field private f:[B

.field private g:[B

.field private h:I

.field private i:I

.field private j:Lcom/starmicronics/starioextension/ah$b;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x65

    new-array v0, v0, [[I

    const/16 v1, 0x18

    new-array v2, v1, [I

    fill-array-data v2, :array_0

    const/4 v3, 0x0

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_1

    const/4 v3, 0x1

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_2

    const/4 v3, 0x2

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_3

    const/4 v3, 0x3

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_4

    const/4 v3, 0x4

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_5

    const/4 v3, 0x5

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_6

    const/4 v3, 0x6

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_7

    const/4 v3, 0x7

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_8

    const/16 v3, 0x8

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_9

    const/16 v3, 0x9

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_a

    const/16 v3, 0xa

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_b

    const/16 v3, 0xb

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_c

    const/16 v3, 0xc

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_d

    const/16 v3, 0xd

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_e

    const/16 v3, 0xe

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_f

    const/16 v3, 0xf

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_10

    const/16 v3, 0x10

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_11

    const/16 v3, 0x11

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_12

    const/16 v3, 0x12

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_13

    const/16 v3, 0x13

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_14

    const/16 v3, 0x14

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_15

    const/16 v3, 0x15

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_16

    const/16 v3, 0x16

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_17

    const/16 v3, 0x17

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_18

    aput-object v2, v0, v1

    new-array v2, v1, [I

    fill-array-data v2, :array_19

    const/16 v3, 0x19

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_1a

    const/16 v3, 0x1a

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_1b

    const/16 v3, 0x1b

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_1c

    const/16 v3, 0x1c

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_1d

    const/16 v3, 0x1d

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_1e

    const/16 v3, 0x1e

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_1f

    const/16 v3, 0x1f

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_20

    const/16 v3, 0x20

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_21

    const/16 v3, 0x21

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_22

    const/16 v3, 0x22

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_23

    const/16 v3, 0x23

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_24

    const/16 v3, 0x24

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_25

    const/16 v3, 0x25

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_26

    const/16 v3, 0x26

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_27

    const/16 v3, 0x27

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_28

    const/16 v3, 0x28

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_29

    const/16 v3, 0x29

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_2a

    const/16 v3, 0x2a

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_2b

    const/16 v3, 0x2b

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_2c

    const/16 v3, 0x2c

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_2d

    const/16 v3, 0x2d

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_2e

    const/16 v3, 0x2e

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_2f

    const/16 v3, 0x2f

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_30

    const/16 v3, 0x30

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_31

    const/16 v3, 0x31

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_32

    const/16 v3, 0x32

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_33

    const/16 v3, 0x33

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_34

    const/16 v3, 0x34

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_35

    const/16 v3, 0x35

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_36

    const/16 v3, 0x36

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_37

    const/16 v3, 0x37

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_38

    const/16 v3, 0x38

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_39

    const/16 v3, 0x39

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_3a

    const/16 v3, 0x3a

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_3b

    const/16 v3, 0x3b

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_3c

    const/16 v3, 0x3c

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_3d

    const/16 v3, 0x3d

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_3e

    const/16 v3, 0x3e

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_3f

    const/16 v3, 0x3f

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_40

    const/16 v3, 0x40

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_41

    const/16 v3, 0x41

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_42

    const/16 v3, 0x42

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_43

    const/16 v3, 0x43

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_44

    const/16 v3, 0x44

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_45

    const/16 v3, 0x45

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_46

    const/16 v3, 0x46

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_47

    const/16 v3, 0x47

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_48

    const/16 v3, 0x48

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_49

    const/16 v3, 0x49

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_4a

    const/16 v3, 0x4a

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_4b

    const/16 v3, 0x4b

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_4c

    const/16 v3, 0x4c

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_4d

    const/16 v3, 0x4d

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_4e

    const/16 v3, 0x4e

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_4f

    const/16 v3, 0x4f

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_50

    const/16 v3, 0x50

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_51

    const/16 v3, 0x51

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_52

    const/16 v3, 0x52

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_53

    const/16 v3, 0x53

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_54

    const/16 v3, 0x54

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_55

    const/16 v3, 0x55

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_56

    const/16 v3, 0x56

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_57

    const/16 v3, 0x57

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_58

    const/16 v3, 0x58

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_59

    const/16 v3, 0x59

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_5a

    const/16 v3, 0x5a

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_5b

    const/16 v3, 0x5b

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_5c

    const/16 v3, 0x5c

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_5d

    const/16 v3, 0x5d

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_5e

    const/16 v3, 0x5e

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_5f

    const/16 v3, 0x5f

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_60

    const/16 v3, 0x60

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_61

    const/16 v3, 0x61

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_62

    const/16 v3, 0x62

    aput-object v2, v0, v3

    new-array v2, v1, [I

    fill-array-data v2, :array_63

    const/16 v3, 0x63

    aput-object v2, v0, v3

    new-array v1, v1, [I

    fill-array-data v1, :array_64

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sput-object v0, Lcom/starmicronics/starioextension/ah;->d:[[I

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x0
        0x0
        0x0
        0x600
        0xf00
        0xf00
        0x600
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_2
    .array-data 4
        0x30c0
        0x30c0
        0x30c0
        0x30c0
        0x30c0
        0x30c0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_3
    .array-data 4
        0x0
        0x1980
        0x1980
        0x1980
        0x1980
        0x1980
        0x7fe0
        0x7fe0
        0x1980
        0x1980
        0x1980
        0x1980
        0x1980
        0x7fe0
        0x7fe0
        0x1980
        0x1980
        0x1980
        0x1980
        0x1980
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_4
    .array-data 4
        0x0
        0x600
        0x600
        0x1f80
        0x3fc0
        0x76e0
        0x6660
        0x6660
        0x6600
        0x7600
        0x3e00
        0x1f80
        0x7c0
        0x6e0
        0x660
        0x6660
        0x6660
        0x76e0
        0x3fc0
        0x1f80
        0x600
        0x600
        0x0
        0x0
    .end array-data

    :array_5
    .array-data 4
        0x0
        0x7800
        0xfc60
        0xccc0
        0xccc0
        0xcd80
        0xcd80
        0xff00
        0x7b00
        0x600
        0xc00
        0xc00
        0x1bc0
        0x1fe0
        0x3660
        0x3660
        0x6660
        0x6660
        0xc7e0
        0x3c0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_6
    .array-data 4
        0x0
        0x1e00
        0x3f00
        0x3300
        0x6300
        0x6300
        0x6600
        0x6e00
        0x3c00
        0x3800
        0x3860
        0x6c60
        0x6c60
        0xc6c0
        0xc6c0
        0xc380
        0xc380
        0xe7c0
        0x7ee0
        0x3c60
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_7
    .array-data 4
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_8
    .array-data 4
        0x0
        0xc0
        0x180
        0x300
        0x300
        0x600
        0x600
        0x600
        0xc00
        0xc00
        0xc00
        0xc00
        0xc00
        0xc00
        0xc00
        0xc00
        0x600
        0x600
        0x600
        0x300
        0x300
        0x180
        0xc0
        0x0
    .end array-data

    :array_9
    .array-data 4
        0x0
        0x3000
        0x1800
        0xc00
        0xc00
        0x600
        0x600
        0x600
        0x300
        0x300
        0x300
        0x300
        0x300
        0x300
        0x300
        0x300
        0x600
        0x600
        0x600
        0xc00
        0xc00
        0x1800
        0x3000
        0x0
    .end array-data

    :array_a
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x600
        0x600
        0x6660
        0x36c0
        0x1f80
        0xf00
        0xf00
        0x1f80
        0x36c0
        0x6660
        0x600
        0x600
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_b
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x600
        0x600
        0x600
        0x600
        0x7fe0
        0x7fe0
        0x600
        0x600
        0x600
        0x600
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_c
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x600
        0xf00
        0xf00
        0x700
        0x600
        0xc00
        0x0
        0x0
    .end array-data

    :array_d
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x7fe0
        0x7fe0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_e
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x600
        0xf00
        0xf00
        0x600
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_f
    .array-data 4
        0x0
        0x60
        0x60
        0xc0
        0xc0
        0xc0
        0x180
        0x180
        0x300
        0x300
        0x300
        0x600
        0x600
        0xc00
        0xc00
        0xc00
        0x1800
        0x1800
        0x3000
        0x3000
        0x3000
        0x6000
        0x6000
        0x0
    .end array-data

    :array_10
    .array-data 4
        0x0
        0xf00
        0x1f80
        0x30c0
        0x30c0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x30c0
        0x30c0
        0x1f80
        0xf00
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_11
    .array-data 4
        0x0
        0x300
        0x700
        0x1f00
        0x1f00
        0x300
        0x300
        0x300
        0x300
        0x300
        0x300
        0x300
        0x300
        0x300
        0x300
        0x300
        0x300
        0x300
        0x300
        0x300
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_12
    .array-data 4
        0x0
        0xf00
        0x3fc0
        0x30c0
        0x6060
        0x6060
        0x60
        0x60
        0xc0
        0x1c0
        0x380
        0xf00
        0x1c00
        0x3800
        0x3000
        0x6000
        0x6000
        0x6000
        0x7fe0
        0x7fe0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_13
    .array-data 4
        0x0
        0xf00
        0x3fc0
        0x30c0
        0x6060
        0x6060
        0x60
        0x60
        0xc0
        0x7c0
        0x780
        0xc0
        0x60
        0x60
        0x60
        0x6060
        0x6060
        0x30c0
        0x3fc0
        0xf00
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_14
    .array-data 4
        0x0
        0x180
        0x380
        0x380
        0x780
        0x780
        0xd80
        0xd80
        0x1980
        0x1980
        0x3180
        0x3180
        0x6180
        0x6180
        0x7fe0
        0x7fe0
        0x180
        0x180
        0x180
        0x180
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_15
    .array-data 4
        0x0
        0x7fc0
        0x7fc0
        0x6000
        0x6000
        0x6000
        0x6000
        0x6f00
        0x7fc0
        0x70c0
        0x6060
        0x60
        0x60
        0x60
        0x60
        0x6060
        0x6060
        0x30c0
        0x3fc0
        0xf00
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_16
    .array-data 4
        0x0
        0xf00
        0x3fc0
        0x30c0
        0x6060
        0x6060
        0x6000
        0x6000
        0x6f00
        0x7fc0
        0x70c0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x30c0
        0x3fc0
        0xf00
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_17
    .array-data 4
        0x0
        0x7fe0
        0x7fe0
        0x60
        0x60
        0xc0
        0xc0
        0x180
        0x180
        0x300
        0x300
        0x300
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_18
    .array-data 4
        0x0
        0xf00
        0x3fc0
        0x30c0
        0x6060
        0x6060
        0x6060
        0x6060
        0x30c0
        0x1f80
        0x1f80
        0x30c0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x30c0
        0x3fc0
        0xf00
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_19
    .array-data 4
        0x0
        0xf00
        0x3fc0
        0x30c0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x30e0
        0x3fe0
        0xf60
        0x60
        0x60
        0x6060
        0x6060
        0x30c0
        0x3fc0
        0xf00
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_1a
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x600
        0xf00
        0xf00
        0x600
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x600
        0xf00
        0xf00
        0x600
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_1b
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x600
        0xf00
        0xf00
        0x600
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x600
        0xf00
        0xf00
        0x700
        0x600
        0xc00
        0x0
        0x0
    .end array-data

    :array_1c
    .array-data 4
        0x0
        0x0
        0x20
        0x60
        0xe0
        0x1c0
        0x380
        0x700
        0xe00
        0x1c00
        0x3800
        0x3800
        0x1c00
        0xe00
        0x700
        0x380
        0x1c0
        0xe0
        0x60
        0x20
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_1d
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x7fe0
        0x7fe0
        0x0
        0x0
        0x0
        0x0
        0x7fe0
        0x7fe0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_1e
    .array-data 4
        0x0
        0x0
        0x4000
        0x6000
        0x7000
        0x3800
        0x1c00
        0xe00
        0x700
        0x380
        0x1c0
        0x1c0
        0x380
        0x700
        0xe00
        0x1c00
        0x3800
        0x7000
        0x6000
        0x4000
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_1f
    .array-data 4
        0x0
        0xf00
        0x3fc0
        0x30c0
        0x6060
        0x6060
        0x6060
        0xc0
        0x1c0
        0x380
        0x300
        0x600
        0x600
        0x600
        0x0
        0x0
        0x600
        0xf00
        0xf00
        0x600
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_20
    .array-data 4
        0x0
        0xf00
        0x3fc0
        0x30c0
        0x6060
        0x6060
        0x6060
        0x63e0
        0x67e0
        0x6660
        0x6c60
        0x6c60
        0x6c60
        0x6c60
        0x6660
        0x67e0
        0x61c0
        0x6000
        0x6000
        0x3000
        0x3fc0
        0xfc0
        0x0
        0x0
    .end array-data

    :array_21
    .array-data 4
        0x0
        0x600
        0x600
        0x600
        0xf00
        0xf00
        0xf00
        0x1980
        0x1980
        0x1980
        0x1980
        0x30c0
        0x30c0
        0x3fc0
        0x3fc0
        0x30c0
        0x6060
        0x6060
        0x6060
        0x6060
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_22
    .array-data 4
        0x0
        0x7e00
        0x7f80
        0x6180
        0x60c0
        0x60c0
        0x60c0
        0x60c0
        0x6180
        0x7f80
        0x7fc0
        0x60c0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x60c0
        0x7fc0
        0x7f00
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_23
    .array-data 4
        0x0
        0xf00
        0x3fc0
        0x30c0
        0x6060
        0x6060
        0x6060
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6060
        0x6060
        0x6060
        0x30c0
        0x3fc0
        0xf00
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_24
    .array-data 4
        0x0
        0x7c00
        0x7f00
        0x6380
        0x61c0
        0x60c0
        0x60e0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x60e0
        0x60c0
        0x61c0
        0x6380
        0x7f00
        0x7c00
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_25
    .array-data 4
        0x0
        0x7fc0
        0x7fc0
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x7f80
        0x7f80
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x7fc0
        0x7fc0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_26
    .array-data 4
        0x0
        0x7fc0
        0x7fc0
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x7f80
        0x7f80
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_27
    .array-data 4
        0x0
        0xf00
        0x3fc0
        0x30c0
        0x6060
        0x6060
        0x6060
        0x6000
        0x6000
        0x6000
        0x63e0
        0x63e0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x30e0
        0x3fe0
        0xf60
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_28
    .array-data 4
        0x0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x7fe0
        0x7fe0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_29
    .array-data 4
        0x0
        0x1f80
        0x1f80
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x1f80
        0x1f80
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_2a
    .array-data 4
        0x0
        0x1e0
        0x1e0
        0xc0
        0xc0
        0xc0
        0xc0
        0xc0
        0xc0
        0xc0
        0xc0
        0xc0
        0xc0
        0xc0
        0x60c0
        0x60c0
        0x60c0
        0x3180
        0x3f80
        0xe00
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_2b
    .array-data 4
        0x0
        0x60e0
        0x60c0
        0x61c0
        0x6180
        0x6380
        0x6300
        0x6700
        0x6600
        0x6e00
        0x6c00
        0x7e00
        0x7e00
        0x7700
        0x7300
        0x6380
        0x6180
        0x61c0
        0x60c0
        0x60e0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_2c
    .array-data 4
        0x0
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x7fe0
        0x7fe0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_2d
    .array-data 4
        0x0
        0x6060
        0x6060
        0x70e0
        0x70e0
        0x70e0
        0x79e0
        0x79e0
        0x7fe0
        0x6f60
        0x6f60
        0x6660
        0x6660
        0x6660
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_2e
    .array-data 4
        0x0
        0x6060
        0x6060
        0x7060
        0x7060
        0x7860
        0x7860
        0x7c60
        0x6c60
        0x6e60
        0x6660
        0x6760
        0x6360
        0x63e0
        0x61e0
        0x61e0
        0x60e0
        0x60e0
        0x6060
        0x6060
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_2f
    .array-data 4
        0x0
        0xf00
        0x3fc0
        0x30c0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x30c0
        0x3fc0
        0xf00
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_30
    .array-data 4
        0x0
        0x7f00
        0x7fc0
        0x60c0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x60c0
        0x7fc0
        0x7f00
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_31
    .array-data 4
        0x0
        0xf00
        0x3fc0
        0x30c0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6660
        0x6760
        0x63e0
        0x31c0
        0x3fe0
        0xf60
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_32
    .array-data 4
        0x0
        0x7e00
        0x7f80
        0x6180
        0x60c0
        0x60c0
        0x60c0
        0x60c0
        0x60c0
        0x6180
        0x7f80
        0x7f00
        0x6300
        0x6380
        0x6180
        0x61c0
        0x60c0
        0x60e0
        0x6060
        0x6060
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_33
    .array-data 4
        0x0
        0xf00
        0x3fc0
        0x30c0
        0x6060
        0x6060
        0x6000
        0x3000
        0x3c00
        0x1f00
        0x780
        0x1c0
        0xc0
        0x60
        0x6060
        0x6060
        0x6060
        0x30c0
        0x3fc0
        0xf00
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_34
    .array-data 4
        0x0
        0x7fe0
        0x7fe0
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_35
    .array-data 4
        0x0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x30c0
        0x3fc0
        0xf00
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_36
    .array-data 4
        0x0
        0x6060
        0x6060
        0x6060
        0x6060
        0x30c0
        0x30c0
        0x30c0
        0x30c0
        0x1980
        0x1980
        0x1980
        0x1980
        0xf00
        0xf00
        0xf00
        0x600
        0x600
        0x600
        0x600
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_37
    .array-data 4
        0x0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6660
        0x6660
        0x6660
        0x6f60
        0x6f60
        0x6f60
        0x7fe0
        0x39c0
        0x39c0
        0x39c0
        0x39c0
        0x30c0
        0x30c0
        0x30c0
        0x30c0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_38
    .array-data 4
        0x0
        0x6060
        0x6060
        0x30c0
        0x30c0
        0x1980
        0x1980
        0xf00
        0xf00
        0x600
        0x600
        0xf00
        0xf00
        0x1980
        0x1980
        0x30c0
        0x30c0
        0x6060
        0x6060
        0x6060
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_39
    .array-data 4
        0x0
        0x6060
        0x6060
        0x6060
        0x30c0
        0x30c0
        0x1980
        0x1980
        0xf00
        0xf00
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_3a
    .array-data 4
        0x0
        0x7fc0
        0x7fc0
        0x180
        0x180
        0x300
        0x300
        0x300
        0x600
        0x600
        0x600
        0xc00
        0xc00
        0x1800
        0x1800
        0x1800
        0x3000
        0x3000
        0x7fc0
        0x7fc0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_3b
    .array-data 4
        0x0
        0x1fc0
        0x1fc0
        0x1800
        0x1800
        0x1800
        0x1800
        0x1800
        0x1800
        0x1800
        0x1800
        0x1800
        0x1800
        0x1800
        0x1800
        0x1800
        0x1800
        0x1800
        0x1800
        0x1800
        0x1800
        0x1fc0
        0x1fc0
        0x0
    .end array-data

    :array_3c
    .array-data 4
        0x0
        0x6000
        0x6000
        0x3000
        0x3000
        0x3000
        0x1800
        0x1800
        0xc00
        0xc00
        0xc00
        0x600
        0x600
        0x300
        0x300
        0x300
        0x180
        0x180
        0xc0
        0xc0
        0xc0
        0x60
        0x60
        0x0
    .end array-data

    :array_3d
    .array-data 4
        0x0
        0x3f80
        0x3f80
        0x180
        0x180
        0x180
        0x180
        0x180
        0x180
        0x180
        0x180
        0x180
        0x180
        0x180
        0x180
        0x180
        0x180
        0x180
        0x180
        0x180
        0x180
        0x3f80
        0x3f80
        0x0
    .end array-data

    :array_3e
    .array-data 4
        0xe00
        0x1b00
        0x3180
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_3f
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0xfff0
        0xfff0
    .end array-data

    :array_40
    .array-data 4
        0xc00
        0x600
        0x300
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_41
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x700
        0x1fc0
        0x38c0
        0x3060
        0x60
        0x7e0
        0x1fe0
        0x3c60
        0x7060
        0x6060
        0x6060
        0x70e0
        0x3fe0
        0x1f60
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_42
    .array-data 4
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6f80    # 3.9999E-41f
        0x7fc0
        0x70c0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x70c0
        0x7fc0
        0x6f80    # 3.9999E-41f
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_43
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0xf00
        0x3fc0
        0x30c0
        0x6060
        0x6060
        0x6000
        0x6000
        0x6000
        0x6000
        0x6060
        0x6060
        0x30c0
        0x3fc0
        0xf00
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_44
    .array-data 4
        0x60
        0x60
        0x60
        0x60
        0x60
        0x60
        0x1f60
        0x3fe0
        0x30e0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x30e0
        0x3fe0
        0x1f60
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_45
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0xf00
        0x3fc0
        0x30c0
        0x6060
        0x6060
        0x6060
        0x7fe0
        0x7fe0
        0x6000
        0x6000
        0x6060
        0x30e0
        0x3fc0
        0xf00
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_46
    .array-data 4
        0xe0
        0x3e0
        0x300
        0x600
        0x600
        0x600
        0x3fe0
        0x3fe0
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_47
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0xf60
        0x3fe0
        0x30e0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x30e0
        0x3fe0
        0xf60
        0x60
        0x60
        0x6060
        0x70c0
        0x3fc0
        0xf00
    .end array-data

    :array_48
    .array-data 4
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6f00
        0x7fc0
        0x70c0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_49
    .array-data 4
        0x0
        0x600
        0xf00
        0x600
        0x0
        0x0
        0x1e00
        0x1e00
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x1f80
        0x1f80
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_4a
    .array-data 4
        0x0
        0x180
        0x3c0
        0x180
        0x0
        0x0
        0x780
        0x780
        0x180
        0x180
        0x180
        0x180
        0x180
        0x180
        0x180
        0x180
        0x180
        0x180
        0x180
        0x180
        0x380
        0x700
        0x3f00
        0x3c00
    .end array-data

    :array_4b
    .array-data 4
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
        0x60e0
        0x61c0
        0x6380
        0x6700
        0x6e00
        0x7c00
        0x7800
        0x7800
        0x7c00
        0x6e00
        0x6700
        0x6380
        0x61c0
        0x60e0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_4c
    .array-data 4
        0x1e00
        0x1e00
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x1f80
        0x1f80
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_4d
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x6cc0
        0x7fe0
        0x7760
        0x6660
        0x6660
        0x6660
        0x6660
        0x6660
        0x6660
        0x6660
        0x6660
        0x6660
        0x6660
        0x6660
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_4e
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x6f00
        0x7fc0
        0x70c0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_4f
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0xf00
        0x3fc0
        0x30c0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x30c0
        0x3fc0
        0xf00
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_50
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x6f00
        0x7fc0
        0x70c0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x70c0
        0x7fc0
        0x6f00
        0x6000
        0x6000
        0x6000
        0x6000
        0x6000
    .end array-data

    :array_51
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0xf60
        0x3fe0
        0x30e0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x30e0
        0x3fe0
        0xf60
        0x60
        0x60
        0x60
        0x60
        0x60
    .end array-data

    :array_52
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x30e0
        0x33e0
        0x3700
        0x3c00
        0x3800
        0x3000
        0x3000
        0x3000
        0x3000
        0x3000
        0x3000
        0x3000
        0x3000
        0x3000
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_53
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0xf00
        0x3fc0
        0x70e0
        0x6060
        0x6000
        0x7000
        0x3f00
        0xfc0
        0xe0
        0x60
        0x6060
        0x70e0
        0x3fc0
        0xf00
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_54
    .array-data 4
        0x0
        0x600
        0x600
        0x600
        0x600
        0x600
        0x3fe0
        0x3fe0
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x700
        0x3e0
        0x1e0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_55
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x30e0
        0x3fe0
        0xf60
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_56
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x6060
        0x6060
        0x6060
        0x30c0
        0x30c0
        0x30c0
        0x1980
        0x1980
        0x1980
        0xf00
        0xf00
        0xf00
        0x600
        0x600
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_57
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x6060
        0x6060
        0x6660
        0x6660
        0x6660
        0x6f60
        0x6f60
        0x6f60
        0x3fc0
        0x39c0
        0x39c0
        0x30c0
        0x30c0
        0x30c0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_58
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x60c0
        0x60c0
        0x3180
        0x3180
        0x1b00
        0x1f00
        0xe00
        0xe00
        0x1f00
        0x1b00
        0x3180
        0x3180
        0x60c0
        0x60c0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_59
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x6060
        0x6060
        0x6060
        0x30c0
        0x30c0
        0x30c0
        0x1980
        0x1980
        0x1980
        0xf00
        0xf00
        0xf00
        0x600
        0x600
        0xc00
        0x1c00
        0x7800
        0x7000
    .end array-data

    :array_5a
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3fe0
        0x3fe0
        0xc0
        0x1c0
        0x180
        0x380
        0x300
        0x600
        0xe00
        0xc00
        0x1c00
        0x1800
        0x3fe0
        0x3fe0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_5b
    .array-data 4
        0x0
        0xe0
        0x3e0
        0x300
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0xc00
        0x3800
        0xc00
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x300
        0x3e0
        0xe0
        0x0
    .end array-data

    :array_5c
    .array-data 4
        0x0
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x0
    .end array-data

    :array_5d
    .array-data 4
        0x0
        0x7000
        0x7c00
        0xc00
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x300
        0x1c0
        0x300
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0x600
        0xc00
        0x7c00
        0x7000
        0x0
    .end array-data

    :array_5e
    .array-data 4
        0x1800
        0x3e20
        0x47c0
        0x180
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_5f
    .array-data 4
        0xff00
        0xff80
        0xc1c0
        0xc0c0
        0xc0c0
        0xc0c0
        0xc0c0
        0xc1c0
        0xff80
        0xff00
        0x0
        0x0
        0x3000
        0x3000
        0x3000
        0x3000
        0x3000
        0x3000
        0x3000
        0x3000
        0x3ff0
        0x3ff0
        0x0
        0x0
    .end array-data

    :array_60
    .array-data 4
        0x0
        0x7fe0
        0x7fe0
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x6060
        0x7fe0
        0x7fe0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_61
    .array-data 4
        0xffc0
        0xffc0
        0xc000
        0xc000
        0xff80
        0xff80
        0xc000
        0xc000
        0xc000
        0xc000
        0x0
        0x0
        0x300
        0x700
        0xf00
        0x300
        0x300
        0x300
        0x300
        0x300
        0x300
        0x300
        0x0
        0x0
    .end array-data

    :array_62
    .array-data 4
        0xffc0
        0xffc0
        0xc000
        0xc000
        0xff80
        0xff80
        0xc000
        0xc000
        0xc000
        0xc000
        0x0
        0x0
        0x1fe0
        0x3ff0
        0x3030
        0x30
        0xe0
        0x380
        0xe00
        0x1800
        0x3ff0
        0x3ff0
        0x0
        0x0
    .end array-data

    :array_63
    .array-data 4
        0xffc0
        0xffc0
        0xc000
        0xc000
        0xff80
        0xff80
        0xc000
        0xc000
        0xc000
        0xc000
        0x0
        0x0
        0x1fe0
        0x3ff0
        0x3030
        0x30
        0x7e0
        0x7e0
        0x30
        0x3030
        0x3ff0
        0x1fe0
        0x0
        0x0
    .end array-data

    :array_64
    .array-data 4
        0xffc0
        0xffc0
        0xc000
        0xc000
        0xff80
        0xff80
        0xc000
        0xc000
        0xc000
        0xc000
        0x0
        0x0
        0x1c0
        0x3c0
        0x6c0
        0xcc0
        0x18c0
        0x30c0
        0x3ff0
        0x3ff0
        0xc0
        0xc0
        0x0
        0x0
    .end array-data
.end method

.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/starmicronics/starioextension/ah;->e:I

    const/16 v0, 0x100

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/starmicronics/starioextension/ah;->f:[B

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/starmicronics/starioextension/ah;->g:[B

    iput p1, p0, Lcom/starmicronics/starioextension/ah;->h:I

    const/4 p1, 0x0

    iput p1, p0, Lcom/starmicronics/starioextension/ah;->i:I

    sget-object p1, Lcom/starmicronics/starioextension/ah$b;->a:Lcom/starmicronics/starioextension/ah$b;

    iput-object p1, p0, Lcom/starmicronics/starioextension/ah;->j:Lcom/starmicronics/starioextension/ah$b;

    return-void
.end method

.method private a(I)V
    .locals 6

    new-instance v0, Lcom/starmicronics/starioextension/ah$1;

    invoke-direct {v0, p0}, Lcom/starmicronics/starioextension/ah$1;-><init>(Lcom/starmicronics/starioextension/ah;)V

    iget v1, p0, Lcom/starmicronics/starioextension/ah;->e:I

    add-int v2, v1, p1

    const/16 v3, 0x800

    if-gt v2, v3, :cond_0

    div-int/lit8 v1, v1, 0x8

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    iget v2, p0, Lcom/starmicronics/starioextension/ah;->e:I

    rem-int/lit8 v2, v2, 0x8

    ushr-int/2addr v0, v2

    iget-object v2, p0, Lcom/starmicronics/starioextension/ah;->f:[B

    aget-byte v3, v2, v1

    ushr-int/lit8 v4, v0, 0x18

    int-to-byte v4, v4

    or-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    add-int/lit8 v3, v1, 0x1

    aget-byte v4, v2, v3

    ushr-int/lit8 v5, v0, 0x10

    int-to-byte v5, v5

    or-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    add-int/lit8 v3, v1, 0x2

    aget-byte v4, v2, v3

    ushr-int/lit8 v5, v0, 0x8

    int-to-byte v5, v5

    or-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    add-int/lit8 v1, v1, 0x3

    aget-byte v3, v2, v1

    int-to-byte v0, v0

    or-int/2addr v0, v3

    int-to-byte v0, v0

    aput-byte v0, v2, v1

    :cond_0
    iget-object v0, p0, Lcom/starmicronics/starioextension/ah;->j:Lcom/starmicronics/starioextension/ah$b;

    sget-object v1, Lcom/starmicronics/starioextension/ah$b;->b:Lcom/starmicronics/starioextension/ah$b;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/starmicronics/starioextension/ah$b;->c:Lcom/starmicronics/starioextension/ah$b;

    iput-object v0, p0, Lcom/starmicronics/starioextension/ah;->j:Lcom/starmicronics/starioextension/ah$b;

    :cond_1
    iget v0, p0, Lcom/starmicronics/starioextension/ah;->e:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/starmicronics/starioextension/ah;->e:I

    iget v0, p0, Lcom/starmicronics/starioextension/ah;->i:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/starmicronics/starioextension/ah;->i:I

    return-void
.end method

.method private b(I)V
    .locals 2

    iget-object v0, p0, Lcom/starmicronics/starioextension/ah;->j:Lcom/starmicronics/starioextension/ah$b;

    sget-object v1, Lcom/starmicronics/starioextension/ah$b;->c:Lcom/starmicronics/starioextension/ah$b;

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/starmicronics/starioextension/ah;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/starmicronics/starioextension/ah;->i:I

    sget-object v0, Lcom/starmicronics/starioextension/ah$b;->b:Lcom/starmicronics/starioextension/ah$b;

    iput-object v0, p0, Lcom/starmicronics/starioextension/ah;->j:Lcom/starmicronics/starioextension/ah$b;

    :cond_0
    iget v0, p0, Lcom/starmicronics/starioextension/ah;->e:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/starmicronics/starioextension/ah;->e:I

    iget v0, p0, Lcom/starmicronics/starioextension/ah;->i:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/starmicronics/starioextension/ah;->i:I

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)V
    .locals 8

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_5

    aget-object v3, p1, v2

    const/4 v4, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v5

    const/16 v6, 0x30

    const/4 v7, 0x1

    if-eq v5, v6, :cond_1

    const/16 v6, 0x31

    if-eq v5, v6, :cond_0

    goto :goto_1

    :cond_0
    const-string v5, "1"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v4, 0x0

    goto :goto_1

    :cond_1
    const-string v5, "0"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v4, 0x1

    :cond_2
    :goto_1
    if-eqz v4, :cond_4

    if-eq v4, v7, :cond_3

    goto :goto_2

    :cond_3
    invoke-direct {p0, p2}, Lcom/starmicronics/starioextension/ah;->b(I)V

    goto :goto_2

    :cond_4
    invoke-direct {p0, p2}, Lcom/starmicronics/starioextension/ah;->a(I)V

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_5
    return-void
.end method

.method public a(Ljava/lang/String;II)V
    .locals 10

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_9

    aget-object v3, p1, v2

    const/4 v4, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v5

    const/16 v6, 0x4e

    const/4 v7, 0x3

    const/4 v8, 0x2

    const/4 v9, 0x1

    if-eq v5, v6, :cond_3

    const/16 v6, 0x57

    if-eq v5, v6, :cond_2

    const/16 v6, 0x6e

    if-eq v5, v6, :cond_1

    const/16 v6, 0x77

    if-eq v5, v6, :cond_0

    goto :goto_1

    :cond_0
    const-string v5, "w"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v4, 0x3

    goto :goto_1

    :cond_1
    const-string v5, "n"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const-string v5, "W"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v4, 0x2

    goto :goto_1

    :cond_3
    const-string v5, "N"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v4, 0x0

    :cond_4
    :goto_1
    if-eqz v4, :cond_8

    if-eq v4, v9, :cond_7

    if-eq v4, v8, :cond_6

    if-eq v4, v7, :cond_5

    goto :goto_2

    :cond_5
    invoke-direct {p0, p2}, Lcom/starmicronics/starioextension/ah;->b(I)V

    goto :goto_2

    :cond_6
    invoke-direct {p0, p2}, Lcom/starmicronics/starioextension/ah;->a(I)V

    goto :goto_2

    :cond_7
    invoke-direct {p0, p3}, Lcom/starmicronics/starioextension/ah;->b(I)V

    goto :goto_2

    :cond_8
    invoke-direct {p0, p3}, Lcom/starmicronics/starioextension/ah;->a(I)V

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_9
    return-void
.end method

.method public abstract a([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;)V
.end method

.method public a([I)V
    .locals 14

    iget v0, p0, Lcom/starmicronics/starioextension/ah;->i:I

    if-gtz v0, :cond_0

    return-void

    :cond_0
    array-length v1, p1

    div-int v1, v0, v1

    const/16 v2, 0xc

    if-ge v1, v2, :cond_1

    return-void

    :cond_1
    iget v2, p0, Lcom/starmicronics/starioextension/ah;->h:I

    add-int/lit8 v3, v1, -0xc

    array-length v4, p1

    rem-int/2addr v0, v4

    add-int/2addr v3, v0

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iget v0, p0, Lcom/starmicronics/starioextension/ah;->e:I

    add-int/lit8 v0, v0, 0x7

    div-int/lit8 v0, v0, 0x8

    mul-int/lit8 v3, v0, 0x18

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/starmicronics/starioextension/ah;->g:[B

    const/4 v3, 0x0

    move v4, v2

    const/4 v2, 0x0

    :goto_0
    array-length v5, p1

    if-ge v2, v5, :cond_4

    aget v5, p1, v2

    add-int/lit8 v5, v5, -0x20

    if-ltz v5, :cond_2

    sget-object v6, Lcom/starmicronics/starioextension/ah;->d:[[I

    aget-object v5, v6, v5

    goto :goto_1

    :cond_2
    sget-object v5, Lcom/starmicronics/starioextension/ah;->d:[[I

    aget-object v5, v5, v3

    :goto_1
    const/4 v6, 0x0

    const/4 v7, 0x0

    :goto_2
    const/16 v8, 0x18

    if-ge v6, v8, :cond_3

    aget v8, v5, v6

    rem-int/lit8 v9, v4, 0x8

    packed-switch v9, :pswitch_data_0

    shl-int/lit8 v8, v8, 0x8

    iget-object v9, p0, Lcom/starmicronics/starioextension/ah;->g:[B

    div-int/lit8 v10, v4, 0x8

    add-int v11, v10, v7

    aget-byte v12, v9, v11

    shr-int/lit8 v13, v8, 0x10

    or-int/2addr v12, v13

    int-to-byte v12, v12

    aput-byte v12, v9, v11

    add-int/lit8 v10, v10, 0x1

    add-int/2addr v10, v7

    aget-byte v11, v9, v10

    shr-int/lit8 v8, v8, 0x8

    or-int/2addr v8, v11

    int-to-byte v8, v8

    aput-byte v8, v9, v10

    goto/16 :goto_3

    :pswitch_0
    shl-int/lit8 v8, v8, 0x1

    iget-object v9, p0, Lcom/starmicronics/starioextension/ah;->g:[B

    div-int/lit8 v10, v4, 0x8

    add-int v11, v10, v7

    aget-byte v12, v9, v11

    shr-int/lit8 v13, v8, 0x10

    or-int/2addr v12, v13

    int-to-byte v12, v12

    aput-byte v12, v9, v11

    add-int/lit8 v11, v10, 0x1

    add-int/2addr v11, v7

    aget-byte v12, v9, v11

    shr-int/lit8 v13, v8, 0x8

    or-int/2addr v12, v13

    int-to-byte v12, v12

    aput-byte v12, v9, v11

    add-int/lit8 v10, v10, 0x2

    add-int/2addr v10, v7

    aget-byte v11, v9, v10

    or-int/2addr v8, v11

    int-to-byte v8, v8

    aput-byte v8, v9, v10

    goto/16 :goto_3

    :pswitch_1
    shl-int/lit8 v8, v8, 0x2

    iget-object v9, p0, Lcom/starmicronics/starioextension/ah;->g:[B

    div-int/lit8 v10, v4, 0x8

    add-int v11, v10, v7

    aget-byte v12, v9, v11

    shr-int/lit8 v13, v8, 0x10

    or-int/2addr v12, v13

    int-to-byte v12, v12

    aput-byte v12, v9, v11

    add-int/lit8 v11, v10, 0x1

    add-int/2addr v11, v7

    aget-byte v12, v9, v11

    shr-int/lit8 v13, v8, 0x8

    or-int/2addr v12, v13

    int-to-byte v12, v12

    aput-byte v12, v9, v11

    add-int/lit8 v10, v10, 0x2

    add-int/2addr v10, v7

    aget-byte v11, v9, v10

    or-int/2addr v8, v11

    int-to-byte v8, v8

    aput-byte v8, v9, v10

    goto/16 :goto_3

    :pswitch_2
    shl-int/lit8 v8, v8, 0x3

    iget-object v9, p0, Lcom/starmicronics/starioextension/ah;->g:[B

    div-int/lit8 v10, v4, 0x8

    add-int v11, v10, v7

    aget-byte v12, v9, v11

    shr-int/lit8 v13, v8, 0x10

    or-int/2addr v12, v13

    int-to-byte v12, v12

    aput-byte v12, v9, v11

    add-int/lit8 v11, v10, 0x1

    add-int/2addr v11, v7

    aget-byte v12, v9, v11

    shr-int/lit8 v13, v8, 0x8

    or-int/2addr v12, v13

    int-to-byte v12, v12

    aput-byte v12, v9, v11

    add-int/lit8 v10, v10, 0x2

    add-int/2addr v10, v7

    aget-byte v11, v9, v10

    or-int/2addr v8, v11

    int-to-byte v8, v8

    aput-byte v8, v9, v10

    goto :goto_3

    :pswitch_3
    shl-int/lit8 v8, v8, 0x4

    iget-object v9, p0, Lcom/starmicronics/starioextension/ah;->g:[B

    div-int/lit8 v10, v4, 0x8

    add-int v11, v10, v7

    aget-byte v12, v9, v11

    shr-int/lit8 v13, v8, 0x10

    or-int/2addr v12, v13

    int-to-byte v12, v12

    aput-byte v12, v9, v11

    add-int/lit8 v10, v10, 0x1

    add-int/2addr v10, v7

    aget-byte v11, v9, v10

    shr-int/lit8 v8, v8, 0x8

    or-int/2addr v8, v11

    int-to-byte v8, v8

    aput-byte v8, v9, v10

    goto :goto_3

    :pswitch_4
    shl-int/lit8 v8, v8, 0x5

    iget-object v9, p0, Lcom/starmicronics/starioextension/ah;->g:[B

    div-int/lit8 v10, v4, 0x8

    add-int v11, v10, v7

    aget-byte v12, v9, v11

    shr-int/lit8 v13, v8, 0x10

    or-int/2addr v12, v13

    int-to-byte v12, v12

    aput-byte v12, v9, v11

    add-int/lit8 v10, v10, 0x1

    add-int/2addr v10, v7

    aget-byte v11, v9, v10

    shr-int/lit8 v8, v8, 0x8

    or-int/2addr v8, v11

    int-to-byte v8, v8

    aput-byte v8, v9, v10

    goto :goto_3

    :pswitch_5
    shl-int/lit8 v8, v8, 0x6

    iget-object v9, p0, Lcom/starmicronics/starioextension/ah;->g:[B

    div-int/lit8 v10, v4, 0x8

    add-int v11, v10, v7

    aget-byte v12, v9, v11

    shr-int/lit8 v13, v8, 0x10

    or-int/2addr v12, v13

    int-to-byte v12, v12

    aput-byte v12, v9, v11

    add-int/lit8 v10, v10, 0x1

    add-int/2addr v10, v7

    aget-byte v11, v9, v10

    shr-int/lit8 v8, v8, 0x8

    or-int/2addr v8, v11

    int-to-byte v8, v8

    aput-byte v8, v9, v10

    goto :goto_3

    :pswitch_6
    shl-int/lit8 v8, v8, 0x7

    iget-object v9, p0, Lcom/starmicronics/starioextension/ah;->g:[B

    div-int/lit8 v10, v4, 0x8

    add-int v11, v10, v7

    aget-byte v12, v9, v11

    shr-int/lit8 v13, v8, 0x10

    or-int/2addr v12, v13

    int-to-byte v12, v12

    aput-byte v12, v9, v11

    add-int/lit8 v10, v10, 0x1

    add-int/2addr v10, v7

    aget-byte v11, v9, v10

    shr-int/lit8 v8, v8, 0x8

    or-int/2addr v8, v11

    int-to-byte v8, v8

    aput-byte v8, v9, v10

    :goto_3
    add-int/lit8 v6, v6, 0x1

    add-int/2addr v7, v0

    goto/16 :goto_2

    :cond_3
    add-int/lit8 v2, v2, 0x1

    add-int/2addr v4, v1

    goto/16 :goto_0

    :cond_4
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method a()[B
    .locals 3

    iget-object v0, p0, Lcom/starmicronics/starioextension/ah;->f:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget v1, p0, Lcom/starmicronics/starioextension/ah;->e:I

    add-int/lit8 v1, v1, 0x7

    div-int/lit8 v1, v1, 0x8

    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    return-object v0
.end method

.method b()[B
    .locals 3

    iget-object v0, p0, Lcom/starmicronics/starioextension/ah;->g:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget v1, p0, Lcom/starmicronics/starioextension/ah;->e:I

    add-int/lit8 v1, v1, 0x7

    div-int/lit8 v1, v1, 0x8

    const/4 v2, 0x0

    mul-int/lit8 v1, v1, 0x18

    invoke-static {v0, v2, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/starmicronics/starioextension/ah;->e:I

    return v0
.end method
