.class Lcom/starmicronics/starioextension/c;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/starioextension/c$a;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/util/List;[BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZ)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;[B",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;",
            "IZ)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    if-nez p1, :cond_0

    return-void

    :cond_0
    new-instance v2, Lcom/starmicronics/starioextension/c$1;

    invoke-direct {v2}, Lcom/starmicronics/starioextension/c$1;-><init>()V

    new-instance v3, Lcom/starmicronics/starioextension/c$3;

    invoke-direct {v3}, Lcom/starmicronics/starioextension/c$3;-><init>()V

    new-instance v4, Lcom/starmicronics/starioextension/c$4;

    invoke-direct {v4}, Lcom/starmicronics/starioextension/c$4;-><init>()V

    const/4 v5, 0x1

    move/from16 v6, p4

    if-ge v6, v5, :cond_1

    const/4 v6, 0x1

    :cond_1
    const/16 v7, 0xff

    if-le v6, v7, :cond_2

    const/16 v6, 0xff

    :cond_2
    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Byte;

    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    invoke-static/range {p5 .. p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Byte;

    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v4

    move-object/from16 v7, p3

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    sget-object v7, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;->Code128:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;

    const/4 v8, 0x5

    const/4 v9, 0x4

    const/4 v10, 0x3

    const/4 v11, 0x2

    const/16 v12, 0x62

    const/16 v13, 0x1b

    const/4 v14, 0x6

    const/4 v15, 0x0

    if-ne v1, v7, :cond_3

    invoke-static/range {p1 .. p1}, Lcom/starmicronics/starioextension/c;->a([B)[B

    move-result-object v1

    if-eqz v1, :cond_5

    new-array v7, v14, [B

    aput-byte v13, v7, v15

    aput-byte v12, v7, v5

    aput-byte v2, v7, v11

    aput-byte v4, v7, v10

    aput-byte v3, v7, v9

    int-to-byte v2, v6

    aput-byte v2, v7, v8

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    sget-object v7, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;->Code93:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;

    if-ne v1, v7, :cond_4

    invoke-static/range {p1 .. p1}, Lcom/starmicronics/starioextension/c;->b([B)[B

    move-result-object v1

    if-eqz v1, :cond_5

    new-array v7, v14, [B

    aput-byte v13, v7, v15

    aput-byte v12, v7, v5

    aput-byte v2, v7, v11

    aput-byte v4, v7, v10

    aput-byte v3, v7, v9

    int-to-byte v2, v6

    aput-byte v2, v7, v8

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    new-array v1, v14, [B

    aput-byte v13, v1, v15

    aput-byte v12, v1, v5

    aput-byte v2, v1, v11

    aput-byte v4, v1, v10

    aput-byte v3, v1, v9

    int-to-byte v2, v6

    aput-byte v2, v1, v8

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface/range {p0 .. p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    :goto_0
    new-array v1, v5, [B

    const/16 v2, 0x1e

    aput-byte v2, v1, v15

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static a(Ljava/util/List;[BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZI)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;[B",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;",
            "IZI)V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-static {p2, p6}, Lcom/starmicronics/starioextension/ah$a;->a(Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;I)Lcom/starmicronics/starioextension/ah;

    move-result-object p2

    invoke-virtual {p2, p1, p3}, Lcom/starmicronics/starioextension/ah;->a([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;)V

    invoke-virtual {p2}, Lcom/starmicronics/starioextension/ah;->a()[B

    move-result-object p1

    if-nez p1, :cond_1

    return-void

    :cond_1
    invoke-virtual {p2}, Lcom/starmicronics/starioextension/ah;->c()I

    move-result p3

    add-int/lit8 p3, p3, 0x7

    div-int/lit8 p3, p3, 0x8

    const/4 p6, 0x0

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    const/16 v2, 0x62

    const/4 v3, 0x3

    const/4 v4, 0x1

    if-ge v0, p4, :cond_2

    new-array v3, v3, [B

    aput-byte v2, v3, p6

    rem-int/lit16 v2, p3, 0x100

    int-to-byte v2, v2

    aput-byte v2, v3, v4

    div-int/lit16 v2, p3, 0x100

    int-to-byte v2, v2

    aput-byte v2, v3, v1

    invoke-interface {p0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    if-eqz p5, :cond_5

    invoke-virtual {p2}, Lcom/starmicronics/starioextension/ah;->b()[B

    move-result-object p1

    if-nez p1, :cond_3

    return-void

    :cond_3
    const/4 p2, 0x6

    new-array p4, p2, [B

    fill-array-data p4, :array_0

    invoke-interface {p0, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 p4, 0x0

    :goto_1
    const/16 p5, 0x18

    if-ge p4, p5, :cond_4

    new-array p5, v3, [B

    aput-byte v2, p5, p6

    rem-int/lit16 v0, p3, 0x100

    int-to-byte v0, v0

    aput-byte v0, p5, v4

    div-int/lit16 v0, p3, 0x100

    int-to-byte v0, v0

    aput-byte v0, p5, v1

    invoke-interface {p0, p5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    mul-int p5, p4, p3

    add-int/lit8 p4, p4, 0x1

    mul-int v0, p4, p3

    invoke-static {p1, p5, v0}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object p5

    invoke-interface {p0, p5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    new-array p1, p2, [B

    fill-array-data p1, :array_1

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    return-void

    :array_0
    .array-data 1
        0x1bt
        0x2at
        0x72t
        0x59t
        0x32t
        0x0t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x1bt
        0x2at
        0x72t
        0x59t
        0x38t
        0x0t
    .end array-data
.end method

.method private static a([B)[B
    .locals 16

    move-object/from16 v0, p0

    array-length v1, v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-gt v1, v2, :cond_0

    return-object v3

    :cond_0
    const/4 v1, 0x0

    aget-byte v4, v0, v1

    const/16 v5, 0x7b

    if-eq v4, v5, :cond_1

    return-object v3

    :cond_1
    sget-object v4, Lcom/starmicronics/starioextension/c$a;->b:Lcom/starmicronics/starioextension/c$a;

    aget-byte v6, v0, v2

    packed-switch v6, :pswitch_data_0

    return-object v3

    :pswitch_0
    sget-object v4, Lcom/starmicronics/starioextension/c$a;->c:Lcom/starmicronics/starioextension/c$a;

    goto :goto_0

    :pswitch_1
    sget-object v4, Lcom/starmicronics/starioextension/c$a;->a:Lcom/starmicronics/starioextension/c$a;

    :goto_0
    :pswitch_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const/4 v7, 0x2

    move-object v9, v4

    const/4 v4, 0x2

    const/4 v8, 0x2

    :goto_1
    array-length v10, v0

    if-ge v4, v10, :cond_1e

    aget-byte v10, v0, v8

    const/4 v11, 0x4

    const/4 v12, 0x3

    const/16 v13, 0x25

    if-ne v10, v5, :cond_12

    add-int/lit8 v8, v8, 0x1

    add-int/lit8 v4, v4, 0x1

    array-length v10, v0

    if-gt v10, v4, :cond_2

    return-object v3

    :cond_2
    aget-byte v10, v0, v8

    sget-object v14, Lcom/starmicronics/starioextension/c$2;->a:[I

    invoke-virtual {v9}, Lcom/starmicronics/starioextension/c$a;->ordinal()I

    move-result v15

    aget v14, v14, v15

    const/16 v1, 0x42

    const/16 v5, 0x31

    const/16 v15, 0x53

    if-eq v14, v2, :cond_d

    const/16 v2, 0x41

    if-eq v14, v7, :cond_7

    if-eq v14, v12, :cond_8

    if-eq v14, v11, :cond_3

    const/16 v2, 0x7b

    goto/16 :goto_5

    :cond_3
    if-eq v10, v5, :cond_6

    if-eq v10, v2, :cond_5

    if-eq v10, v1, :cond_4

    return-object v3

    :cond_4
    sget-object v9, Lcom/starmicronics/starioextension/c$a;->b:Lcom/starmicronics/starioextension/c$a;

    goto :goto_3

    :cond_5
    sget-object v9, Lcom/starmicronics/starioextension/c$a;->a:Lcom/starmicronics/starioextension/c$a;

    goto :goto_3

    :cond_6
    :pswitch_3
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    :goto_2
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_3
    const/16 v2, 0x7b

    goto/16 :goto_c

    :cond_7
    sget-object v9, Lcom/starmicronics/starioextension/c$a;->a:Lcom/starmicronics/starioextension/c$a;

    if-ne v10, v15, :cond_8

    return-object v3

    :cond_8
    if-eq v10, v2, :cond_c

    const/16 v1, 0x43

    if-eq v10, v1, :cond_b

    if-eq v10, v15, :cond_a

    const/16 v1, 0x7b

    if-eq v10, v1, :cond_9

    packed-switch v10, :pswitch_data_1

    return-object v3

    :pswitch_4
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x34

    goto :goto_4

    :pswitch_5
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x33

    goto :goto_4

    :pswitch_6
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x32

    :goto_4
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    goto :goto_2

    :cond_9
    const/16 v2, 0x7b

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    goto/16 :goto_8

    :cond_a
    const/16 v2, 0x7b

    sget-object v9, Lcom/starmicronics/starioextension/c$a;->d:Lcom/starmicronics/starioextension/c$a;

    goto/16 :goto_c

    :cond_b
    const/16 v2, 0x7b

    goto :goto_7

    :cond_c
    const/16 v2, 0x7b

    sget-object v9, Lcom/starmicronics/starioextension/c$a;->a:Lcom/starmicronics/starioextension/c$a;

    goto/16 :goto_c

    :cond_d
    const/16 v2, 0x7b

    sget-object v9, Lcom/starmicronics/starioextension/c$a;->b:Lcom/starmicronics/starioextension/c$a;

    if-ne v10, v15, :cond_e

    return-object v3

    :cond_e
    :goto_5
    if-eq v10, v1, :cond_11

    const/16 v1, 0x43

    if-eq v10, v1, :cond_10

    if-eq v10, v15, :cond_f

    packed-switch v10, :pswitch_data_2

    return-object v3

    :pswitch_7
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x34

    goto :goto_6

    :pswitch_8
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x33

    goto :goto_6

    :pswitch_9
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x32

    :goto_6
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    goto :goto_8

    :pswitch_a
    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    goto :goto_8

    :cond_f
    sget-object v9, Lcom/starmicronics/starioextension/c$a;->e:Lcom/starmicronics/starioextension/c$a;

    goto/16 :goto_c

    :cond_10
    :goto_7
    sget-object v9, Lcom/starmicronics/starioextension/c$a;->c:Lcom/starmicronics/starioextension/c$a;

    goto/16 :goto_c

    :cond_11
    sget-object v9, Lcom/starmicronics/starioextension/c$a;->b:Lcom/starmicronics/starioextension/c$a;

    goto/16 :goto_c

    :cond_12
    const/16 v2, 0x7b

    aget-byte v1, v0, v8

    sget-object v5, Lcom/starmicronics/starioextension/c$2;->a:[I

    invoke-virtual {v9}, Lcom/starmicronics/starioextension/c$a;->ordinal()I

    move-result v10

    aget v5, v5, v10

    const/16 v10, 0x20

    const/16 v14, 0x30

    const/4 v15, 0x1

    if-eq v5, v15, :cond_1a

    if-eq v5, v7, :cond_15

    if-eq v5, v12, :cond_16

    if-eq v5, v11, :cond_13

    goto :goto_a

    :cond_13
    if-ltz v1, :cond_14

    const/16 v5, 0x63

    if-gt v1, v5, :cond_14

    div-int/lit8 v5, v1, 0xa

    add-int/2addr v5, v14

    int-to-byte v5, v5

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    rem-int/lit8 v1, v1, 0xa

    add-int/2addr v1, v14

    int-to-byte v1, v1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    :goto_8
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_c

    :cond_14
    return-object v3

    :cond_15
    sget-object v9, Lcom/starmicronics/starioextension/c$a;->a:Lcom/starmicronics/starioextension/c$a;

    :cond_16
    if-ne v1, v13, :cond_17

    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    goto :goto_8

    :cond_17
    const/16 v5, 0x7f

    if-ne v1, v5, :cond_18

    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x35

    :goto_9
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    goto :goto_8

    :cond_18
    if-lt v1, v10, :cond_19

    const/16 v5, 0x7e

    if-gt v1, v5, :cond_19

    aget-byte v1, v0, v8

    goto :goto_9

    :cond_19
    return-object v3

    :cond_1a
    sget-object v9, Lcom/starmicronics/starioextension/c$a;->b:Lcom/starmicronics/starioextension/c$a;

    :goto_a
    if-ne v1, v13, :cond_1b

    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v14}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    goto :goto_8

    :cond_1b
    if-ltz v1, :cond_1c

    const/16 v5, 0x1f

    if-gt v1, v5, :cond_1c

    invoke-static {v13}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x40

    int-to-byte v1, v1

    :goto_b
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    goto :goto_8

    :cond_1c
    if-lt v1, v10, :cond_1d

    const/16 v5, 0x5f

    if-gt v1, v5, :cond_1d

    aget-byte v1, v0, v8

    goto :goto_b

    :goto_c
    const/4 v1, 0x1

    add-int/2addr v8, v1

    add-int/2addr v4, v1

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/16 v5, 0x7b

    goto/16 :goto_1

    :cond_1d
    return-object v3

    :cond_1e
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [B

    const/4 v1, 0x0

    :goto_d
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1f

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Byte;

    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    aput-byte v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    :cond_1f
    return-object v0

    :pswitch_data_0
    .packed-switch 0x41
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x31
        :pswitch_3
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x31
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
    .end packed-switch
.end method

.method static b(Ljava/util/List;[BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;[B",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;",
            "IZ)V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/starmicronics/starioextension/c$5;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/c$5;-><init>()V

    new-instance v1, Lcom/starmicronics/starioextension/c$6;

    invoke-direct {v1}, Lcom/starmicronics/starioextension/c$6;-><init>()V

    new-instance v2, Lcom/starmicronics/starioextension/c$7;

    invoke-direct {v2}, Lcom/starmicronics/starioextension/c$7;-><init>()V

    const/4 v3, 0x1

    if-ge p4, v3, :cond_1

    const/4 p4, 0x1

    :cond_1
    const/16 v4, 0xff

    if-le p4, v4, :cond_2

    const/16 p4, 0xff

    :cond_2
    const/16 v4, 0xd

    new-array v4, v4, [B

    const/4 v5, 0x0

    const/16 v6, 0x1d

    aput-byte v6, v4, v5

    const/16 v5, 0x48

    aput-byte v5, v4, v3

    const/4 v3, 0x2

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p5

    invoke-interface {v2, p5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/lang/Byte;

    invoke-virtual {p5}, Ljava/lang/Byte;->byteValue()B

    move-result p5

    aput-byte p5, v4, v3

    const/4 p5, 0x3

    aput-byte v6, v4, p5

    const/4 p5, 0x4

    const/16 v2, 0x68

    aput-byte v2, v4, p5

    const/4 p5, 0x5

    int-to-byte p4, p4

    aput-byte p4, v4, p5

    const/4 p4, 0x6

    aput-byte v6, v4, p4

    const/4 p4, 0x7

    const/16 p5, 0x77

    aput-byte p5, v4, p4

    const/16 p4, 0x8

    invoke-interface {v1, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Byte;

    invoke-virtual {p3}, Ljava/lang/Byte;->byteValue()B

    move-result p3

    aput-byte p3, v4, p4

    const/16 p3, 0x9

    aput-byte v6, v4, p3

    const/16 p3, 0xa

    const/16 p4, 0x6b

    aput-byte p4, v4, p3

    const/16 p3, 0xb

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Byte;

    invoke-virtual {p2}, Ljava/lang/Byte;->byteValue()B

    move-result p2

    aput-byte p2, v4, p3

    const/16 p2, 0xc

    array-length p3, p1

    int-to-byte p3, p3

    aput-byte p3, v4, p2

    invoke-interface {p0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private static b([B)[B
    .locals 7

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    array-length v1, p0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_3

    aget-byte v4, p0, v3

    const/16 v5, 0x25

    if-ne v4, v5, :cond_1

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v4, 0x30

    :cond_0
    :goto_1
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    const/16 v6, 0x7f

    if-ne v4, v6, :cond_2

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v4, 0x35

    goto :goto_1

    :cond_2
    if-ltz v4, :cond_0

    const/16 v6, 0x1f

    if-gt v4, v6, :cond_0

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x40

    int-to-byte v4, v4

    goto :goto_1

    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p0

    new-array p0, p0, [B

    :goto_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_4

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Byte;

    invoke-virtual {v1}, Ljava/lang/Byte;->byteValue()B

    move-result v1

    aput-byte v1, p0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_4
    return-object p0
.end method

.method static c(Ljava/util/List;[BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZ)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;[B",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;",
            "IZ)V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/starmicronics/starioextension/c$8;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/c$8;-><init>()V

    new-instance v1, Lcom/starmicronics/starioextension/c$9;

    invoke-direct {v1}, Lcom/starmicronics/starioextension/c$9;-><init>()V

    new-instance v2, Lcom/starmicronics/starioextension/c$10;

    invoke-direct {v2}, Lcom/starmicronics/starioextension/c$10;-><init>()V

    const/4 v3, 0x1

    if-ge p4, v3, :cond_1

    const/4 p4, 0x1

    :cond_1
    const/16 v4, 0xff

    if-le p4, v4, :cond_2

    const/16 p4, 0xff

    :cond_2
    const/16 v4, 0x9

    new-array v4, v4, [B

    const/4 v5, 0x0

    const/16 v6, 0x1d

    aput-byte v6, v4, v5

    const/16 v7, 0x48

    aput-byte v7, v4, v3

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p5

    invoke-interface {v2, p5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/lang/Byte;

    invoke-virtual {p5}, Ljava/lang/Byte;->byteValue()B

    move-result p5

    const/4 v2, 0x2

    aput-byte p5, v4, v2

    const/4 p5, 0x3

    aput-byte v6, v4, p5

    const/16 v7, 0x68

    const/4 v8, 0x4

    aput-byte v7, v4, v8

    const/4 v7, 0x5

    int-to-byte p4, p4

    aput-byte p4, v4, v7

    const/4 p4, 0x6

    aput-byte v6, v4, p4

    const/4 p4, 0x7

    const/16 v7, 0x77

    aput-byte v7, v4, p4

    const/16 p4, 0x8

    invoke-interface {v1, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Byte;

    invoke-virtual {p3}, Ljava/lang/Byte;->byteValue()B

    move-result p3

    aput-byte p3, v4, p4

    invoke-interface {p0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object p3, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;->Code128:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;

    const/16 p4, 0x6b

    if-ne p2, p3, :cond_3

    invoke-static {p1}, Lcom/starmicronics/starioextension/c;->c([B)[B

    move-result-object p1

    if-eqz p1, :cond_4

    new-array p3, v8, [B

    aput-byte v6, p3, v5

    aput-byte p4, p3, v3

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Byte;

    invoke-virtual {p2}, Ljava/lang/Byte;->byteValue()B

    move-result p2

    aput-byte p2, p3, v2

    array-length p2, p1

    int-to-byte p2, p2

    aput-byte p2, p3, p5

    invoke-interface {p0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    new-array p3, v8, [B

    aput-byte v6, p3, v5

    aput-byte p4, p3, v3

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Byte;

    invoke-virtual {p2}, Ljava/lang/Byte;->byteValue()B

    move-result p2

    aput-byte p2, p3, v2

    array-length p2, p1

    int-to-byte p2, p2

    aput-byte p2, p3, p5

    invoke-interface {p0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_0
    new-array p1, v3, [B

    const/16 p2, 0xa

    aput-byte p2, p1, v5

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private static c([B)[B
    .locals 17

    move-object/from16 v0, p0

    array-length v1, v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-gt v1, v2, :cond_0

    return-object v3

    :cond_0
    const/4 v1, 0x0

    aget-byte v4, v0, v1

    const/16 v5, 0x7b

    if-eq v4, v5, :cond_1

    return-object v3

    :cond_1
    sget-object v4, Lcom/starmicronics/starioextension/c$a;->b:Lcom/starmicronics/starioextension/c$a;

    aget-byte v6, v0, v2

    packed-switch v6, :pswitch_data_0

    return-object v3

    :pswitch_0
    sget-object v4, Lcom/starmicronics/starioextension/c$a;->c:Lcom/starmicronics/starioextension/c$a;

    goto :goto_0

    :pswitch_1
    sget-object v4, Lcom/starmicronics/starioextension/c$a;->a:Lcom/starmicronics/starioextension/c$a;

    :goto_0
    :pswitch_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const/4 v7, 0x2

    move-object v9, v4

    const/4 v4, 0x2

    const/4 v8, 0x2

    :goto_1
    array-length v10, v0

    if-ge v4, v10, :cond_17

    aget-byte v10, v0, v8

    const/4 v11, 0x4

    const/4 v12, 0x3

    if-ne v10, v5, :cond_f

    add-int/lit8 v8, v8, 0x1

    add-int/lit8 v4, v4, 0x1

    array-length v10, v0

    if-gt v10, v4, :cond_2

    return-object v3

    :cond_2
    aget-byte v10, v0, v8

    sget-object v13, Lcom/starmicronics/starioextension/c$2;->a:[I

    invoke-virtual {v9}, Lcom/starmicronics/starioextension/c$a;->ordinal()I

    move-result v14

    aget v13, v13, v14

    const/16 v15, 0x43

    const/16 v1, 0x42

    const/16 v16, -0x3f

    const/16 v14, 0x53

    if-eq v13, v2, :cond_a

    const/16 v2, 0x41

    if-eq v13, v7, :cond_5

    if-eq v13, v12, :cond_6

    if-eq v13, v11, :cond_3

    goto :goto_3

    :cond_3
    const/16 v11, 0x31

    if-eq v10, v11, :cond_4

    if-eq v10, v2, :cond_9

    if-eq v10, v1, :cond_e

    return-object v3

    :cond_4
    :pswitch_3
    invoke-static/range {v16 .. v16}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    goto/16 :goto_6

    :cond_5
    sget-object v9, Lcom/starmicronics/starioextension/c$a;->a:Lcom/starmicronics/starioextension/c$a;

    if-ne v10, v14, :cond_6

    return-object v3

    :cond_6
    if-eq v10, v2, :cond_9

    if-eq v10, v15, :cond_d

    if-eq v10, v14, :cond_8

    if-eq v10, v5, :cond_7

    packed-switch v10, :pswitch_data_1

    return-object v3

    :pswitch_4
    const/16 v1, -0x3c

    goto :goto_2

    :pswitch_5
    const/16 v1, -0x3d

    goto :goto_2

    :pswitch_6
    const/16 v1, -0x3e

    :goto_2
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    goto/16 :goto_6

    :cond_7
    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    goto/16 :goto_6

    :cond_8
    sget-object v9, Lcom/starmicronics/starioextension/c$a;->d:Lcom/starmicronics/starioextension/c$a;

    goto/16 :goto_7

    :cond_9
    sget-object v9, Lcom/starmicronics/starioextension/c$a;->a:Lcom/starmicronics/starioextension/c$a;

    goto/16 :goto_7

    :cond_a
    sget-object v9, Lcom/starmicronics/starioextension/c$a;->b:Lcom/starmicronics/starioextension/c$a;

    if-ne v10, v14, :cond_b

    return-object v3

    :cond_b
    :goto_3
    if-eq v10, v1, :cond_e

    if-eq v10, v15, :cond_d

    if-eq v10, v14, :cond_c

    packed-switch v10, :pswitch_data_2

    return-object v3

    :cond_c
    sget-object v9, Lcom/starmicronics/starioextension/c$a;->e:Lcom/starmicronics/starioextension/c$a;

    goto :goto_7

    :cond_d
    sget-object v9, Lcom/starmicronics/starioextension/c$a;->c:Lcom/starmicronics/starioextension/c$a;

    goto :goto_7

    :cond_e
    sget-object v9, Lcom/starmicronics/starioextension/c$a;->b:Lcom/starmicronics/starioextension/c$a;

    goto :goto_7

    :cond_f
    aget-byte v1, v0, v8

    sget-object v2, Lcom/starmicronics/starioextension/c$2;->a:[I

    invoke-virtual {v9}, Lcom/starmicronics/starioextension/c$a;->ordinal()I

    move-result v10

    aget v2, v2, v10

    const/4 v10, 0x1

    if-eq v2, v10, :cond_15

    if-eq v2, v7, :cond_12

    if-eq v2, v12, :cond_13

    if-eq v2, v11, :cond_10

    goto :goto_4

    :cond_10
    if-ltz v1, :cond_11

    const/16 v2, 0x63

    if-gt v1, v2, :cond_11

    div-int/lit8 v2, v1, 0xa

    add-int/lit8 v2, v2, 0x30

    int-to-byte v2, v2

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    rem-int/lit8 v1, v1, 0xa

    add-int/lit8 v1, v1, 0x30

    int-to-byte v1, v1

    goto :goto_5

    :cond_11
    return-object v3

    :cond_12
    sget-object v9, Lcom/starmicronics/starioextension/c$a;->a:Lcom/starmicronics/starioextension/c$a;

    :cond_13
    const/16 v2, 0x20

    if-lt v1, v2, :cond_14

    const/16 v2, 0x7e

    if-gt v1, v2, :cond_14

    aget-byte v1, v0, v8

    goto :goto_5

    :cond_14
    return-object v3

    :cond_15
    sget-object v9, Lcom/starmicronics/starioextension/c$a;->b:Lcom/starmicronics/starioextension/c$a;

    :goto_4
    if-ltz v1, :cond_16

    const/16 v2, 0x5f

    if-gt v1, v2, :cond_16

    aget-byte v1, v0, v8

    :goto_5
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    :goto_6
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_7
    const/4 v1, 0x1

    add-int/2addr v8, v1

    add-int/2addr v4, v1

    const/4 v1, 0x0

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_16
    return-object v3

    :cond_17
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [B

    const/4 v1, 0x0

    :goto_8
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_18

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Byte;

    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    aput-byte v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_18
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x41
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x31
        :pswitch_3
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x31
        :pswitch_3
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method static d(Ljava/util/List;[BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;[B",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;",
            "IZ)V"
        }
    .end annotation

    return-void
.end method
