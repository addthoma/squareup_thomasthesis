.class public interface abstract Lcom/starmicronics/starioextension/IPeripheralCommandParser;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;
    }
.end annotation


# virtual methods
.method public abstract createSendCommands()[B
.end method

.method public abstract parse([BI)Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;
.end method
