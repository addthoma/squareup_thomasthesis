.class Lcom/starmicronics/starioextension/ae;
.super Lcom/starmicronics/starioextension/e;


# static fields
.field private static final a:I = 0x1


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "[B>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/starmicronics/starioextension/e;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/starmicronics/starioextension/ae;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public appendSound(Lcom/starmicronics/starioextension/SoundSetting;)V
    .locals 9

    const-string v0, "Interval"

    const-string v1, "Delay"

    const-string v2, "Count"

    const-string v3, "SoundNumber"

    const-string v4, "SoundStorageArea"

    filled-new-array {v4, v3, v2, v1, v0}, [Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/starmicronics/starioextension/SoundSetting;->getChangedParameters([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    if-gtz v6, :cond_b

    new-instance v5, Lcom/starmicronics/starioextension/ae$1;

    invoke-direct {v5, p0}, Lcom/starmicronics/starioextension/ae$1;-><init>(Lcom/starmicronics/starioextension/ae;)V

    invoke-virtual {p1, v4}, Lcom/starmicronics/starioextension/SoundSetting;->isParameterChanged(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {p1, v3}, Lcom/starmicronics/starioextension/SoundSetting;->isParameterChanged(Ljava/lang/String;)Z

    move-result v3

    const/4 v6, 0x0

    const/4 v7, 0x1

    if-eqz v4, :cond_1

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/SoundSetting;->getSoundStorageArea()Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/SoundSetting;->getSoundNumber()I

    move-result v4

    if-lt v4, v7, :cond_0

    const/16 v5, 0xff

    if-gt v4, v5, :cond_0

    goto :goto_1

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-array v0, v7, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    const-string v1, "SoundNumber %d is out of range. (1-255)"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    if-eqz v4, :cond_2

    if-eqz v3, :cond_3

    :cond_2
    if-nez v4, :cond_4

    if-nez v3, :cond_3

    goto :goto_0

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Not allowed to specify only one of SoundStorageArea and SoundNumber."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    :goto_0
    sget-object v3, Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;->Area1:Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    const/4 v4, 0x1

    :goto_1
    invoke-virtual {p1}, Lcom/starmicronics/starioextension/SoundSetting;->getCount()I

    move-result v5

    invoke-virtual {p1, v2}, Lcom/starmicronics/starioextension/SoundSetting;->isParameterChanged(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    const/4 v5, 0x1

    :cond_5
    if-lt v5, v7, :cond_a

    const v2, 0xffff

    if-gt v5, v2, :cond_a

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/SoundSetting;->getDelay()I

    move-result v8

    invoke-virtual {p1, v1}, Lcom/starmicronics/starioextension/SoundSetting;->isParameterChanged(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v8, 0x0

    :cond_6
    if-ltz v8, :cond_9

    if-gt v8, v2, :cond_9

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/SoundSetting;->getInterval()I

    move-result v1

    invoke-virtual {p1, v0}, Lcom/starmicronics/starioextension/SoundSetting;->isParameterChanged(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const/4 v1, 0x0

    :cond_7
    if-ltz v1, :cond_8

    if-gt v1, v2, :cond_8

    iget-object p1, p0, Lcom/starmicronics/starioextension/ae;->b:Ljava/util/List;

    const/16 v0, 0xd

    new-array v0, v0, [B

    const/16 v2, 0x1b

    aput-byte v2, v0, v6

    const/16 v2, 0x1d

    aput-byte v2, v0, v7

    const/4 v2, 0x2

    const/16 v7, 0x73

    aput-byte v7, v0, v2

    const/4 v2, 0x3

    const/16 v7, 0x4f

    aput-byte v7, v0, v2

    const/4 v2, 0x4

    aput-byte v6, v0, v2

    const/4 v2, 0x5

    aput-byte v3, v0, v2

    const/4 v2, 0x6

    int-to-byte v3, v4

    aput-byte v3, v0, v2

    const/4 v2, 0x7

    rem-int/lit16 v3, v5, 0x100

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    const/16 v2, 0x8

    div-int/lit16 v5, v5, 0x100

    int-to-byte v3, v5

    aput-byte v3, v0, v2

    const/16 v2, 0x9

    rem-int/lit16 v3, v8, 0x100

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    const/16 v2, 0xa

    div-int/lit16 v8, v8, 0x100

    int-to-byte v3, v8

    aput-byte v3, v0, v2

    const/16 v2, 0xb

    rem-int/lit16 v3, v1, 0x100

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    const/16 v2, 0xc

    div-int/lit16 v1, v1, 0x100

    int-to-byte v1, v1

    aput-byte v1, v0, v2

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-array v1, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/SoundSetting;->getInterval()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v1, v6

    const-string p1, "Interval %d is out of range. (0-65535)"

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-array v1, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/SoundSetting;->getDelay()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v1, v6

    const-string p1, "Delay %d is out of range. (0-65535)"

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-array v1, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/SoundSetting;->getCount()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v1, v6

    const-string p1, "Count %d is out of range. (1-65535)"

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0, v5}, Lcom/starmicronics/starioextension/ae;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public appendSoundData([BLcom/starmicronics/starioextension/SoundSetting;)V
    .locals 0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "Unsupported function in this model."

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getCommands()[B
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ae;->b:Ljava/util/List;

    invoke-static {v0}, Lcom/starmicronics/starioextension/as;->b(Ljava/util/List;)[B

    move-result-object v0

    return-object v0
.end method
