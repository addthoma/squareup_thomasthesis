.class public final enum Lcom/starmicronics/starioextension/StarIoExt$Emulation;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/StarIoExt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Emulation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starioextension/StarIoExt$Emulation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/starmicronics/starioextension/StarIoExt$Emulation;

.field public static final enum EscPos:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

.field public static final enum EscPosMobile:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

.field public static final enum None:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

.field public static final enum StarDotImpact:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

.field public static final enum StarGraphic:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

.field public static final enum StarLine:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

.field public static final enum StarPRNT:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

.field public static final enum StarPRNTL:Lcom/starmicronics/starioextension/StarIoExt$Emulation;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    const/4 v1, 0x0

    const-string v2, "None"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starioextension/StarIoExt$Emulation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->None:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    const/4 v2, 0x1

    const-string v3, "StarPRNT"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starioextension/StarIoExt$Emulation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->StarPRNT:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    const/4 v3, 0x2

    const-string v4, "StarPRNTL"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/starioextension/StarIoExt$Emulation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->StarPRNTL:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    const/4 v4, 0x3

    const-string v5, "StarLine"

    invoke-direct {v0, v5, v4}, Lcom/starmicronics/starioextension/StarIoExt$Emulation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->StarLine:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    const/4 v5, 0x4

    const-string v6, "StarGraphic"

    invoke-direct {v0, v6, v5}, Lcom/starmicronics/starioextension/StarIoExt$Emulation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->StarGraphic:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    const/4 v6, 0x5

    const-string v7, "EscPos"

    invoke-direct {v0, v7, v6}, Lcom/starmicronics/starioextension/StarIoExt$Emulation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->EscPos:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    const/4 v7, 0x6

    const-string v8, "EscPosMobile"

    invoke-direct {v0, v8, v7}, Lcom/starmicronics/starioextension/StarIoExt$Emulation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->EscPosMobile:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    const/4 v8, 0x7

    const-string v9, "StarDotImpact"

    invoke-direct {v0, v9, v8}, Lcom/starmicronics/starioextension/StarIoExt$Emulation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->StarDotImpact:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    sget-object v9, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->None:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    aput-object v9, v0, v1

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->StarPRNT:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->StarPRNTL:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->StarLine:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    aput-object v1, v0, v4

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->StarGraphic:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    aput-object v1, v0, v5

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->EscPos:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    aput-object v1, v0, v6

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->EscPosMobile:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    aput-object v1, v0, v7

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->StarDotImpact:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    aput-object v1, v0, v8

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->$VALUES:[Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starioextension/StarIoExt$Emulation;
    .locals 1

    const-class v0, Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starioextension/StarIoExt$Emulation;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->$VALUES:[Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    invoke-virtual {v0}, [Lcom/starmicronics/starioextension/StarIoExt$Emulation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    return-object v0
.end method
