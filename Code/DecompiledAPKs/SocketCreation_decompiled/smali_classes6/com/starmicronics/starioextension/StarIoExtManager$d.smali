.class Lcom/starmicronics/starioextension/StarIoExtManager$d;
.super Ljava/lang/Thread;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/StarIoExtManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcom/starmicronics/starioextension/StarIoExtManager;

.field private final b:I

.field private final c:I

.field private d:Z


# direct methods
.method private constructor <init>(Lcom/starmicronics/starioextension/StarIoExtManager;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$d;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/16 p1, 0x64

    iput p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$d;->b:I

    const/16 p1, 0x32

    iput p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$d;->c:I

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$d;->d:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/StarIoExtManager$d;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;)V

    return-void
.end method


# virtual methods
.method declared-synchronized a(Z)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$d;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method declared-synchronized a()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$d;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$d;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->n(Lcom/starmicronics/starioextension/StarIoExtManager;)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 4

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/starmicronics/starioextension/StarIoExtManager$d;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$d;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$d;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Z)I

    move-result v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    if-nez v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/starmicronics/starioextension/StarIoExtManager$d;->a(Z)V

    goto :goto_2

    :cond_1
    const/4 v1, 0x0

    :goto_1
    const/16 v2, 0x32

    if-ge v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/starmicronics/starioextension/StarIoExtManager$d;->a()Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0

    :cond_2
    const-wide/16 v2, 0x64

    :try_start_1
    invoke-static {v2, v3, v0}, Ljava/lang/Thread;->sleep(JI)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_3
    :goto_2
    return-void
.end method
