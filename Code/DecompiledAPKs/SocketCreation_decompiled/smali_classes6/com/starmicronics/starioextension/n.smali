.class Lcom/starmicronics/starioextension/n;
.super Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a([BI)B
    .locals 3

    array-length v0, p0

    if-lt v0, p1, :cond_3

    const/4 v0, 0x0

    add-int/lit8 p1, p1, -0x1

    move v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ltz v0, :cond_1

    rem-int/lit8 v2, p1, 0x2

    add-int/2addr v2, v0

    rem-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_0

    aget-byte v2, p0, v0

    add-int/lit8 v2, v2, -0x30

    mul-int/lit8 v2, v2, 0x3

    goto :goto_1

    :cond_0
    aget-byte v2, p0, v0

    add-int/lit8 v2, v2, -0x30

    :goto_1
    add-int/2addr v1, v2

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    rem-int/lit8 v1, v1, 0xa

    if-eqz v1, :cond_2

    rsub-int/lit8 v1, v1, 0xa

    :cond_2
    add-int/lit8 v1, v1, 0x30

    int-to-byte p0, v1

    return p0

    :cond_3
    new-instance p0, Ljava/lang/IllegalStateException;

    invoke-direct {p0}, Ljava/lang/IllegalStateException;-><init>()V

    throw p0
.end method
