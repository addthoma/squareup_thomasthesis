.class Lcom/starmicronics/starioextension/w;
.super Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;",
            ")V"
        }
    .end annotation

    new-instance v0, Lcom/starmicronics/starioextension/w$1;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/w$1;-><init>()V

    const/4 v1, 0x4

    new-array v1, v1, [B

    const/4 v2, 0x0

    const/16 v3, 0xa

    aput-byte v3, v1, v2

    const/4 v2, 0x1

    const/16 v3, 0x1b

    aput-byte v3, v1, v2

    const/4 v2, 0x2

    const/16 v3, 0x64

    aput-byte v3, v1, v2

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Byte;

    invoke-virtual {p1}, Ljava/lang/Byte;->byteValue()B

    move-result p1

    const/4 v0, 0x3

    aput-byte p1, v1, v0

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static b(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;",
            ")V"
        }
    .end annotation

    new-instance v0, Lcom/starmicronics/starioextension/w$2;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/w$2;-><init>()V

    const/4 v1, 0x4

    new-array v2, v1, [B

    fill-array-data v2, :array_0

    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x3

    new-array v3, v2, [B

    fill-array-data v3, :array_1

    invoke-interface {p0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-array v1, v1, [B

    fill-array-data v1, :array_2

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-array p1, v2, [B

    fill-array-data p1, :array_3

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :array_0
    .array-data 1
        0x1bt
        0x2at
        0x72t
        0x65t
    .end array-data

    :array_1
    .array-data 1
        0x1bt
        0xct
        0x19t
    .end array-data

    :array_2
    .array-data 1
        0x1bt
        0x2at
        0x72t
        0x46t
    .end array-data

    :array_3
    .array-data 1
        0x1bt
        0xct
        0x0t
    .end array-data
.end method

.method static c(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;",
            ")V"
        }
    .end annotation

    new-instance v0, Lcom/starmicronics/starioextension/w$3;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/w$3;-><init>()V

    const/4 v1, 0x5

    new-array v1, v1, [B

    const/4 v2, 0x0

    const/16 v3, 0xa

    aput-byte v3, v1, v2

    const/4 v3, 0x1

    const/16 v4, 0x1d

    aput-byte v4, v1, v3

    const/4 v3, 0x2

    const/16 v4, 0x56

    aput-byte v4, v1, v3

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Byte;

    invoke-virtual {p1}, Ljava/lang/Byte;->byteValue()B

    move-result p1

    const/4 v0, 0x3

    aput-byte p1, v1, v0

    const/4 p1, 0x4

    aput-byte v2, v1, p1

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static d(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;",
            ")V"
        }
    .end annotation

    sget-object v0, Lcom/starmicronics/starioextension/w$4;->a:[I

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x4

    const/4 v1, 0x1

    if-eq p1, v1, :cond_0

    const/4 v2, 0x2

    if-eq p1, v2, :cond_0

    new-array p1, v1, [B

    const/4 v1, 0x0

    const/16 v2, 0xa

    aput-byte v2, p1, v1

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-array p1, v0, [B

    fill-array-data p1, :array_0

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    new-array p1, v0, [B

    fill-array-data p1, :array_1

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    nop

    :array_0
    .array-data 1
        0xat
        0xat
        0xat
        0xat
    .end array-data

    :array_1
    .array-data 1
        0x1bt
        0x7at
        0x1bt
        0x79t
    .end array-data
.end method
