.class Lcom/starmicronics/starioextension/ab;
.super Lcom/starmicronics/starioextension/d;


# instance fields
.field private b:Z


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/starmicronics/starioextension/d;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/starmicronics/starioextension/ab;->b:Z

    return-void
.end method


# virtual methods
.method protected a(Lcom/starmicronics/starioextension/j;IZ)V
    .locals 1

    iget-boolean v0, p0, Lcom/starmicronics/starioextension/ab;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1, p2, p3}, Lcom/starmicronics/starioextension/i;->f(Ljava/util/List;Lcom/starmicronics/starioextension/j;IZ)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1, p2, p3}, Lcom/starmicronics/starioextension/i;->g(Ljava/util/List;Lcom/starmicronics/starioextension/j;IZ)V

    :goto_0
    return-void
.end method

.method public append(B)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/af;->a(Ljava/util/List;B)V

    return-void
.end method

.method public append([B)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/af;->a(Ljava/util/List;[B)V

    return-void
.end method

.method public appendAbsolutePosition(I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/a;->c(Ljava/util/List;I)V

    return-void
.end method

.method public appendAlignment(Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/b;->c(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V

    return-void
.end method

.method public appendBarcode([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZ)V
    .locals 6

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/starmicronics/starioextension/c;->b(Ljava/util/List;[BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZ)V

    return-void
.end method

.method public appendBarcodeWithAbsolutePosition([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZI)V
    .locals 0

    invoke-virtual {p0, p6}, Lcom/starmicronics/starioextension/ab;->appendAbsolutePosition(I)V

    invoke-virtual/range {p0 .. p5}, Lcom/starmicronics/starioextension/ab;->appendBarcode([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZ)V

    return-void
.end method

.method public appendBlackMark(Lcom/starmicronics/starioextension/ICommandBuilder$BlackMarkType;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/k;->d(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$BlackMarkType;)V

    return-void
.end method

.method public appendCharacterSpace(I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/m;->c(Ljava/util/List;Ljava/lang/Integer;)V

    return-void
.end method

.method public appendCodePage(Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/s;->c(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;)V

    return-void
.end method

.method public appendCutPaper(Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/w;->c(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;)V

    return-void
.end method

.method public appendEmphasis(Z)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/y;->c(Ljava/util/List;Z)V

    return-void
.end method

.method public appendFontStyle(Lcom/starmicronics/starioextension/ICommandBuilder$FontStyleType;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/ad;->c(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$FontStyleType;)V

    return-void
.end method

.method public appendHorizontalTabPosition([I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/ag;->c(Ljava/util/List;[I)V

    return-void
.end method

.method public appendInitialization(Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/aj;->c(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/starmicronics/starioextension/ab;->b:Z

    return-void
.end method

.method public appendInternational(Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/al;->c(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;)V

    return-void
.end method

.method public appendInvert(Z)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/am;->c(Ljava/util/List;Z)V

    return-void
.end method

.method public appendLineFeed()V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/starmicronics/starioextension/aq;->c(Ljava/util/List;)V

    return-void
.end method

.method public appendLineFeed(I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/aq;->c(Ljava/util/List;I)V

    return-void
.end method

.method public appendLineSpace(I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/ar;->c(Ljava/util/List;I)V

    return-void
.end method

.method public appendLogo(Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1, p2}, Lcom/starmicronics/starioextension/at;->d(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;I)V

    return-void
.end method

.method public appendMultiple(II)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1, p2}, Lcom/starmicronics/starioextension/ay;->c(Ljava/util/List;II)V

    return-void
.end method

.method public appendPageModeRotation(Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/ba;->c(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V

    return-void
.end method

.method public appendPageModeVerticalAbsolutePosition(I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/bb;->c(Ljava/util/List;I)V

    return-void
.end method

.method public appendPdf417([BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;II)V
    .locals 7

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-static/range {v0 .. v6}, Lcom/starmicronics/starioextension/bc;->b(Ljava/util/List;[BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;II)V

    return-void
.end method

.method public appendPdf417WithAbsolutePosition([BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;III)V
    .locals 0

    invoke-virtual {p0, p7}, Lcom/starmicronics/starioextension/ab;->appendAbsolutePosition(I)V

    invoke-virtual/range {p0 .. p6}, Lcom/starmicronics/starioextension/ab;->appendPdf417([BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;II)V

    return-void
.end method

.method public appendPeripheral(Lcom/starmicronics/starioextension/ICommandBuilder$PeripheralChannel;I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1, p2}, Lcom/starmicronics/starioextension/bd;->c(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$PeripheralChannel;I)V

    return-void
.end method

.method public appendPrintableArea(Lcom/starmicronics/starioextension/ICommandBuilder$PrintableAreaType;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/be;->b(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$PrintableAreaType;)V

    return-void
.end method

.method public appendQrCode([BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1, p2, p3, p4}, Lcom/starmicronics/starioextension/bf;->b(Ljava/util/List;[BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;I)V

    return-void
.end method

.method public appendQrCodeWithAbsolutePosition([BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;II)V
    .locals 0

    invoke-virtual {p0, p5}, Lcom/starmicronics/starioextension/ab;->appendAbsolutePosition(I)V

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/starmicronics/starioextension/ab;->appendQrCode([BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;I)V

    return-void
.end method

.method public appendSound(Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;III)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1, p2, p3, p4}, Lcom/starmicronics/starioextension/bh;->c(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;III)V

    return-void
.end method

.method public appendTopMargin(I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/bn;->d(Ljava/util/List;I)V

    return-void
.end method

.method public appendUnderLine(Z)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/bo;->c(Ljava/util/List;Z)V

    return-void
.end method

.method public appendUnitFeed(I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/bp;->c(Ljava/util/List;I)V

    return-void
.end method

.method public beginDocument()V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/starmicronics/starioextension/g;->c(Ljava/util/List;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/starmicronics/starioextension/ab;->b:Z

    return-void
.end method

.method public beginPageMode(Landroid/graphics/Rect;Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0, p1, p2}, Lcom/starmicronics/starioextension/h;->c(Ljava/util/List;Landroid/graphics/Rect;Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/starmicronics/starioextension/ab;->b:Z

    return-void
.end method

.method public endDocument()V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/starmicronics/starioextension/z;->c(Ljava/util/List;)V

    return-void
.end method

.method public endPageMode()V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/ab;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/starmicronics/starioextension/aa;->c(Ljava/util/List;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/starmicronics/starioextension/ab;->b:Z

    return-void
.end method
