.class Lcom/starmicronics/starioextension/bj;
.super Lcom/starmicronics/starioextension/d;


# instance fields
.field private b:Z


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/starmicronics/starioextension/d;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/starmicronics/starioextension/bj;->b:Z

    return-void
.end method


# virtual methods
.method protected a(Lcom/starmicronics/starioextension/j;IZ)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1, p2, p3}, Lcom/starmicronics/starioextension/i;->e(Ljava/util/List;Lcom/starmicronics/starioextension/j;IZ)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/starmicronics/starioextension/bj;->b:Z

    return-void
.end method

.method public append(B)V
    .locals 0

    return-void
.end method

.method public append([B)V
    .locals 0

    return-void
.end method

.method public appendAbsolutePosition(I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/a;->b(Ljava/util/List;I)V

    return-void
.end method

.method public appendAlignment(Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/b;->b(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V

    return-void
.end method

.method public appendBarcode([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZ)V
    .locals 7

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v6}, Lcom/starmicronics/starioextension/c;->a(Ljava/util/List;[BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZI)V

    return-void
.end method

.method public appendBarcodeWithAbsolutePosition([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZI)V
    .locals 7

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-static/range {v0 .. v6}, Lcom/starmicronics/starioextension/c;->a(Ljava/util/List;[BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZI)V

    return-void
.end method

.method public appendBlackMark(Lcom/starmicronics/starioextension/ICommandBuilder$BlackMarkType;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/k;->c(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$BlackMarkType;)V

    return-void
.end method

.method public appendCharacterSpace(I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/bp;->b(Ljava/util/List;I)V

    return-void
.end method

.method public appendCodePage(Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/s;->b(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;)V

    return-void
.end method

.method public appendCutPaper(Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/w;->b(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;)V

    return-void
.end method

.method public appendEmphasis(Z)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/y;->b(Ljava/util/List;Z)V

    return-void
.end method

.method public appendFontStyle(Lcom/starmicronics/starioextension/ICommandBuilder$FontStyleType;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/ad;->b(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$FontStyleType;)V

    return-void
.end method

.method public appendHorizontalTabPosition([I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/ag;->b(Ljava/util/List;[I)V

    return-void
.end method

.method public appendInitialization(Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/aj;->b(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/starmicronics/starioextension/bj;->b:Z

    return-void
.end method

.method public appendInternational(Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/al;->b(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;)V

    return-void
.end method

.method public appendInvert(Z)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/am;->b(Ljava/util/List;Z)V

    return-void
.end method

.method public appendLineFeed()V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/starmicronics/starioextension/aq;->b(Ljava/util/List;)V

    return-void
.end method

.method public appendLineFeed(I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/aq;->b(Ljava/util/List;I)V

    return-void
.end method

.method public appendLineSpace(I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/ar;->b(Ljava/util/List;I)V

    return-void
.end method

.method public appendLogo(Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1, p2}, Lcom/starmicronics/starioextension/at;->c(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;I)V

    return-void
.end method

.method public appendMultiple(II)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1, p2}, Lcom/starmicronics/starioextension/ay;->b(Ljava/util/List;II)V

    return-void
.end method

.method public appendPageModeRotation(Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/ba;->b(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V

    return-void
.end method

.method public appendPageModeVerticalAbsolutePosition(I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/bb;->b(Ljava/util/List;I)V

    return-void
.end method

.method public appendPdf417([BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;II)V
    .locals 8

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    const/4 v7, 0x0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-static/range {v0 .. v7}, Lcom/starmicronics/starioextension/bc;->a(Ljava/util/List;[BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;III)V

    return-void
.end method

.method public appendPdf417WithAbsolutePosition([BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;III)V
    .locals 8

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-static/range {v0 .. v7}, Lcom/starmicronics/starioextension/bc;->a(Ljava/util/List;[BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;III)V

    return-void
.end method

.method public appendPeripheral(Lcom/starmicronics/starioextension/ICommandBuilder$PeripheralChannel;I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1, p2}, Lcom/starmicronics/starioextension/bd;->a(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$PeripheralChannel;I)V

    return-void
.end method

.method public appendPrintableArea(Lcom/starmicronics/starioextension/ICommandBuilder$PrintableAreaType;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/be;->a(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$PrintableAreaType;)V

    return-void
.end method

.method public appendQrCode([BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;I)V
    .locals 6

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-static/range {v0 .. v5}, Lcom/starmicronics/starioextension/bf;->a(Ljava/util/List;[BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;II)V

    return-void
.end method

.method public appendQrCodeWithAbsolutePosition([BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;II)V
    .locals 6

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/starmicronics/starioextension/bf;->a(Ljava/util/List;[BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;II)V

    return-void
.end method

.method public appendSound(Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;III)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1, p2, p3, p4}, Lcom/starmicronics/starioextension/bh;->a(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;III)V

    return-void
.end method

.method public appendTopMargin(I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/bn;->c(Ljava/util/List;I)V

    return-void
.end method

.method public appendUnderLine(Z)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/bo;->b(Ljava/util/List;Z)V

    return-void
.end method

.method public appendUnitFeed(I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/bp;->b(Ljava/util/List;I)V

    return-void
.end method

.method public beginDocument()V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/starmicronics/starioextension/g;->b(Ljava/util/List;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/starmicronics/starioextension/bj;->b:Z

    return-void
.end method

.method public beginPageMode(Landroid/graphics/Rect;Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0, p1, p2}, Lcom/starmicronics/starioextension/h;->b(Ljava/util/List;Landroid/graphics/Rect;Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/starmicronics/starioextension/bj;->b:Z

    return-void
.end method

.method public endDocument()V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/starmicronics/starioextension/z;->b(Ljava/util/List;)V

    return-void
.end method

.method public endPageMode()V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bj;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/starmicronics/starioextension/aa;->b(Ljava/util/List;)V

    return-void
.end method
