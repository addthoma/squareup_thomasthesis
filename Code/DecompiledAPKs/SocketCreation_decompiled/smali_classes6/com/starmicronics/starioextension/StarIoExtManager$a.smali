.class final enum Lcom/starmicronics/starioextension/StarIoExtManager$a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/StarIoExtManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starioextension/StarIoExtManager$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/starmicronics/starioextension/StarIoExtManager$a;

.field public static final enum b:Lcom/starmicronics/starioextension/StarIoExtManager$a;

.field public static final enum c:Lcom/starmicronics/starioextension/StarIoExtManager$a;

.field public static final enum d:Lcom/starmicronics/starioextension/StarIoExtManager$a;

.field public static final enum e:Lcom/starmicronics/starioextension/StarIoExtManager$a;

.field public static final enum f:Lcom/starmicronics/starioextension/StarIoExtManager$a;

.field public static final enum g:Lcom/starmicronics/starioextension/StarIoExtManager$a;

.field public static final enum h:Lcom/starmicronics/starioextension/StarIoExtManager$a;

.field public static final enum i:Lcom/starmicronics/starioextension/StarIoExtManager$a;

.field public static final enum j:Lcom/starmicronics/starioextension/StarIoExtManager$a;

.field public static final enum k:Lcom/starmicronics/starioextension/StarIoExtManager$a;

.field public static final enum l:Lcom/starmicronics/starioextension/StarIoExtManager$a;

.field public static final enum m:Lcom/starmicronics/starioextension/StarIoExtManager$a;

.field public static final enum n:Lcom/starmicronics/starioextension/StarIoExtManager$a;

.field public static final enum o:Lcom/starmicronics/starioextension/StarIoExtManager$a;

.field public static final enum p:Lcom/starmicronics/starioextension/StarIoExtManager$a;

.field public static final enum q:Lcom/starmicronics/starioextension/StarIoExtManager$a;

.field public static final enum r:Lcom/starmicronics/starioextension/StarIoExtManager$a;

.field private static final synthetic s:[Lcom/starmicronics/starioextension/StarIoExtManager$a;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const/4 v1, 0x0

    const-string v2, "PrinterImpossible"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starioextension/StarIoExtManager$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;->a:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const/4 v2, 0x1

    const-string v3, "AccessoryConnectSuccess"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starioextension/StarIoExtManager$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;->b:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const/4 v3, 0x2

    const-string v4, "AccessoryConnectFailure"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/starioextension/StarIoExtManager$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;->c:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const/4 v4, 0x3

    const-string v5, "AccessoryDisconnect"

    invoke-direct {v0, v5, v4}, Lcom/starmicronics/starioextension/StarIoExtManager$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;->d:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const/4 v5, 0x4

    const-string v6, "PrinterOnline"

    invoke-direct {v0, v6, v5}, Lcom/starmicronics/starioextension/StarIoExtManager$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;->e:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const/4 v6, 0x5

    const-string v7, "PrinterOffline"

    invoke-direct {v0, v7, v6}, Lcom/starmicronics/starioextension/StarIoExtManager$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;->f:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const/4 v7, 0x6

    const-string v8, "PrinterPaperReady"

    invoke-direct {v0, v8, v7}, Lcom/starmicronics/starioextension/StarIoExtManager$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;->g:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const/4 v8, 0x7

    const-string v9, "PrinterPaperNearEmpty"

    invoke-direct {v0, v9, v8}, Lcom/starmicronics/starioextension/StarIoExtManager$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;->h:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const/16 v9, 0x8

    const-string v10, "PrinterPaperEmpty"

    invoke-direct {v0, v10, v9}, Lcom/starmicronics/starioextension/StarIoExtManager$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;->i:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const/16 v10, 0x9

    const-string v11, "PrinterCoverOpen"

    invoke-direct {v0, v11, v10}, Lcom/starmicronics/starioextension/StarIoExtManager$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;->j:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const/16 v11, 0xa

    const-string v12, "PrinterCoverClose"

    invoke-direct {v0, v12, v11}, Lcom/starmicronics/starioextension/StarIoExtManager$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;->k:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const/16 v12, 0xb

    const-string v13, "CashDrawerOpen"

    invoke-direct {v0, v13, v12}, Lcom/starmicronics/starioextension/StarIoExtManager$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;->l:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const/16 v13, 0xc

    const-string v14, "CashDrawerClose"

    invoke-direct {v0, v14, v13}, Lcom/starmicronics/starioextension/StarIoExtManager$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;->m:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const/16 v14, 0xd

    const-string v15, "BarcodeReaderImpossible"

    invoke-direct {v0, v15, v14}, Lcom/starmicronics/starioextension/StarIoExtManager$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;->n:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const/16 v15, 0xe

    const-string v14, "BarcodeReaderConnect"

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/StarIoExtManager$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;->o:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const-string v14, "BarcodeReaderDisconnect"

    const/16 v15, 0xf

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/StarIoExtManager$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;->p:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const-string v14, "BarcodeDataReceive"

    const/16 v15, 0x10

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/StarIoExtManager$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;->q:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const-string v14, "Updated"

    const/16 v15, 0x11

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/StarIoExtManager$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;->r:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const/16 v0, 0x12

    new-array v0, v0, [Lcom/starmicronics/starioextension/StarIoExtManager$a;

    sget-object v14, Lcom/starmicronics/starioextension/StarIoExtManager$a;->a:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    aput-object v14, v0, v1

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$a;->b:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$a;->c:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$a;->d:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$a;->e:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$a;->f:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    aput-object v1, v0, v6

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$a;->g:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    aput-object v1, v0, v7

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$a;->h:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    aput-object v1, v0, v8

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$a;->i:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    aput-object v1, v0, v9

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$a;->j:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    aput-object v1, v0, v10

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$a;->k:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    aput-object v1, v0, v11

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$a;->l:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    aput-object v1, v0, v12

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$a;->m:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    aput-object v1, v0, v13

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$a;->n:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$a;->o:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$a;->p:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$a;->q:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$a;->r:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;->s:[Lcom/starmicronics/starioextension/StarIoExtManager$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starioextension/StarIoExtManager$a;
    .locals 1

    const-class v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starioextension/StarIoExtManager$a;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starioextension/StarIoExtManager$a;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;->s:[Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-virtual {v0}, [Lcom/starmicronics/starioextension/StarIoExtManager$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starioextension/StarIoExtManager$a;

    return-object v0
.end method
