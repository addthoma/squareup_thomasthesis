.class final Lcom/starmicronics/starioextension/w$2;
.super Ljava/util/HashMap;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/starmicronics/starioextension/w;->b(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap<",
        "Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;",
        "[B>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 4

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;->FullCut:Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;

    const/4 v1, 0x2

    new-array v2, v1, [B

    fill-array-data v2, :array_0

    invoke-virtual {p0, v0, v2}, Lcom/starmicronics/starioextension/w$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;->PartialCut:Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;

    const/4 v2, 0x3

    new-array v3, v2, [B

    fill-array-data v3, :array_1

    invoke-virtual {p0, v0, v3}, Lcom/starmicronics/starioextension/w$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;->FullCutWithFeed:Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;

    new-array v1, v1, [B

    fill-array-data v1, :array_2

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/w$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;->PartialCutWithFeed:Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;

    new-array v1, v2, [B

    fill-array-data v1, :array_3

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/w$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :array_0
    .array-data 1
        0x38t
        0x0t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x31t
        0x32t
        0x0t
    .end array-data

    :array_2
    .array-data 1
        0x39t
        0x0t
    .end array-data

    nop

    :array_3
    .array-data 1
        0x31t
        0x33t
        0x0t
    .end array-data
.end method
