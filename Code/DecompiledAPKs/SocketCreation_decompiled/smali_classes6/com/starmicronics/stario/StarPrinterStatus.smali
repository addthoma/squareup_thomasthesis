.class public Lcom/starmicronics/stario/StarPrinterStatus;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field public blackMarkError:Z

.field public compulsionSwitch:Z

.field public connectedInterface:I

.field public coverOpen:Z

.field public cutterError:Z

.field public etbAvailable:Z

.field public etbCounter:I

.field public headThermistorError:Z

.field public headUpError:Z

.field public mechError:Z

.field public offline:Z

.field public overTemp:Z

.field public pageModeCmdError:Z

.field public peelerPaperPresent:Z

.field public presenterPaperJamError:Z

.field public presenterPaperPresent:Z

.field public presenterState:I

.field public raw:[B

.field public rawLength:I

.field public receiptBlackMarkDetection:Z

.field public receiptPaperEmpty:Z

.field public receiptPaperNearEmptyInner:Z

.field public receiptPaperNearEmptyOuter:Z

.field public receiveBufferOverflow:Z

.field public slipBOF:Z

.field public slipCOF:Z

.field public slipPaperPresent:Z

.field public slipTOF:Z

.field public stackerFull:Z

.field public unrecoverableError:Z

.field public validationPaperPresent:Z

.field public voltageError:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x40

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method
