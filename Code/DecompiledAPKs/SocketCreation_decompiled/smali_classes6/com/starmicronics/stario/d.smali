.class Lcom/starmicronics/stario/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/starmicronics/stario/c;


# static fields
.field private static b:Ljava/lang/String;

.field private static c:Ljava/lang/String;


# instance fields
.field private a:Lcom/starmicronics/stario/StarIOPort;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

.field private o:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

.field private p:Z

.field private q:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

.field private r:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

.field private s:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

.field private t:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

.field private u:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

.field private v:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

.field private w:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

.field private x:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

.field private y:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;ILcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    const/16 v1, 0x2710

    iput v1, p0, Lcom/starmicronics/stario/d;->d:I

    iput-object v0, p0, Lcom/starmicronics/stario/d;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/starmicronics/stario/d;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/starmicronics/stario/d;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/starmicronics/stario/d;->h:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/starmicronics/stario/d;->i:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/d;->j:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/starmicronics/stario/d;->k:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/d;->l:Z

    iput-boolean v1, p0, Lcom/starmicronics/stario/d;->m:Z

    sget-object v1, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;->DISABLE:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    iput-object v1, p0, Lcom/starmicronics/stario/d;->n:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    sget-object v1, Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;->StarDeviceTypeDesktopPrinter:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    iput-object v1, p0, Lcom/starmicronics/stario/d;->o:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    iput-boolean v0, p0, Lcom/starmicronics/stario/d;->p:Z

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->NOSUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/d;->q:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->NOSUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/d;->r:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->NOSUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/d;->s:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->NOSUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/d;->t:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->NOSUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/d;->u:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->NOSUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/d;->v:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->NOSUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/d;->w:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->NOSUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/d;->x:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->NOSUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/d;->y:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    const-string v0, "BT:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sput-object p1, Lcom/starmicronics/stario/d;->b:Ljava/lang/String;

    sput-object p2, Lcom/starmicronics/stario/d;->c:Ljava/lang/String;

    iput p3, p0, Lcom/starmicronics/stario/d;->d:I

    iput-object p4, p0, Lcom/starmicronics/stario/d;->o:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    return-void

    :cond_0
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "This fuction is available form the bluetooth interface."

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iget-object v1, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v2, v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget v1, p0, Lcom/starmicronics/stario/d;->d:I

    invoke-direct {p0, v0, v3, v1}, Lcom/starmicronics/stario/d;->a([BBI)[B

    move-result-object v1

    array-length v0, v0

    aget-byte v0, v1, v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->NOSUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    :goto_0
    iput-object v0, p0, Lcom/starmicronics/stario/d;->q:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    return-void

    :array_0
    .array-data 1
        0x1bt
        0x1ct
        0x26t
        0x52t
        0x31t
    .end array-data
.end method

.method private a(Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sget-object v1, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;->PINCODE:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    if-ne p1, v1, :cond_0

    array-length p1, v0

    add-int/lit8 p1, p1, -0x1

    const/16 v1, 0x33

    aput-byte v1, v0, p1

    :cond_0
    iget-object p1, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    const/4 v1, 0x0

    array-length v2, v0

    invoke-virtual {p1, v0, v1, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget p1, p0, Lcom/starmicronics/stario/d;->d:I

    invoke-direct {p0, p1}, Lcom/starmicronics/stario/d;->a(I)[B

    move-result-object p1

    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-byte v0, p1, v0

    const/16 v1, 0x32

    if-eq v0, v1, :cond_2

    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-byte p1, p1, v0

    const/16 v0, 0x31

    if-eq p1, v0, :cond_1

    return-void

    :cond_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Returned the error response from printer."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Returned the non-supported response from printer."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :array_0
    .array-data 1
        0x1bt
        0x1ct
        0x26t
        0x53t
        0x33t
        0x1t
        0x0t
        0x31t
    .end array-data
.end method

.method private a(Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x7

    new-array v1, v1, [B

    const/4 v2, 0x0

    const/16 v3, 0x1b

    aput-byte v3, v1, v2

    const/4 v3, 0x1

    const/16 v4, 0x1c

    aput-byte v4, v1, v3

    const/4 v4, 0x2

    const/16 v5, 0x26

    aput-byte v5, v1, v4

    const/4 v4, 0x3

    const/16 v5, 0x53

    aput-byte v5, v1, v4

    const/16 v4, 0x32

    const/4 v5, 0x4

    aput-byte v4, v1, v5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    rem-int/lit16 v5, v5, 0x100

    int-to-byte v5, v5

    const/4 v6, 0x5

    aput-byte v5, v1, v6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    div-int/lit16 v5, v5, 0x100

    int-to-byte v5, v5

    const/4 v6, 0x6

    aput-byte v5, v1, v6

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Lcom/starmicronics/stario/d;->a(Ljava/util/List;)[B

    move-result-object p1

    iget-object v0, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v1, p1

    invoke-virtual {v0, p1, v2, v1}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget p1, p0, Lcom/starmicronics/stario/d;->d:I

    invoke-direct {p0, p1}, Lcom/starmicronics/stario/d;->a(I)[B

    move-result-object p1

    array-length v0, p1

    sub-int/2addr v0, v3

    aget-byte v0, p1, v0

    if-eq v0, v4, :cond_1

    array-length v0, p1

    sub-int/2addr v0, v3

    aget-byte p1, p1, v0

    const/16 v0, 0x31

    if-eq p1, v0, :cond_0

    return-void

    :cond_0
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Returned the error response from printer."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Returned the non-supported response from printer."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    iget v0, p0, Lcom/starmicronics/stario/d;->d:I

    invoke-static {p1, p2, v0}, Lcom/starmicronics/stario/StarIOPort;->getPort(Ljava/lang/String;Ljava/lang/String;I)Lcom/starmicronics/stario/StarIOPort;

    move-result-object p1

    iput-object p1, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    return-void
.end method

.method private a(Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/starmicronics/stario/d;->isOpened()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/starmicronics/stario/d;->a()V

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/starmicronics/stario/d;->b()Ljava/lang/String;

    invoke-direct {p0}, Lcom/starmicronics/stario/d;->c()Ljava/lang/String;

    invoke-direct {p0}, Lcom/starmicronics/stario/d;->d()Ljava/lang/String;

    invoke-direct {p0}, Lcom/starmicronics/stario/d;->e()Z

    invoke-direct {p0}, Lcom/starmicronics/stario/d;->f()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    invoke-direct {p0}, Lcom/starmicronics/stario/d;->i()Z

    invoke-direct {p0}, Lcom/starmicronics/stario/d;->g()Z

    invoke-direct {p0}, Lcom/starmicronics/stario/d;->h()Z

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/starmicronics/stario/d;->b()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/starmicronics/stario/d;->e:Ljava/lang/String;

    invoke-direct {p0}, Lcom/starmicronics/stario/d;->c()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/starmicronics/stario/d;->f:Ljava/lang/String;

    invoke-direct {p0}, Lcom/starmicronics/stario/d;->d()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/starmicronics/stario/d;->g:Ljava/lang/String;

    invoke-direct {p0}, Lcom/starmicronics/stario/d;->e()Z

    move-result p1

    iput-boolean p1, p0, Lcom/starmicronics/stario/d;->j:Z

    invoke-direct {p0}, Lcom/starmicronics/stario/d;->f()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    move-result-object p1

    iput-object p1, p0, Lcom/starmicronics/stario/d;->n:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    invoke-direct {p0}, Lcom/starmicronics/stario/d;->i()Z

    move-result p1

    iput-boolean p1, p0, Lcom/starmicronics/stario/d;->k:Z

    invoke-direct {p0}, Lcom/starmicronics/stario/d;->g()Z

    move-result p1

    iput-boolean p1, p0, Lcom/starmicronics/stario/d;->l:Z

    invoke-direct {p0}, Lcom/starmicronics/stario/d;->h()Z

    move-result p1

    iput-boolean p1, p0, Lcom/starmicronics/stario/d;->m:Z

    :goto_0
    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/starmicronics/stario/d;->p:Z

    return-void

    :cond_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Do not open a port."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a(I)[B
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/16 p1, 0x80

    new-array p1, p1, [B

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    iget v6, p0, Lcom/starmicronics/stario/d;->d:I

    int-to-long v6, v6

    cmp-long v8, v4, v6

    if-gtz v8, :cond_4

    iget-object v4, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v5, p1

    sub-int/2addr v5, v3

    invoke-virtual {v4, p1, v3, v5}, Lcom/starmicronics/stario/StarIOPort;->readPort([BII)I

    move-result v4

    if-ltz v4, :cond_3

    add-int/2addr v3, v4

    const/4 v4, 0x4

    if-lt v3, v4, :cond_2

    aget-byte v0, p1, v2

    const/16 v1, 0x1b

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    aget-byte v0, p1, v0

    const/16 v1, 0x1c

    if-ne v0, v1, :cond_1

    const/4 v0, 0x2

    aget-byte v0, p1, v0

    const/16 v1, 0x26

    if-ne v0, v1, :cond_1

    const/4 v0, 0x3

    aget-byte v1, p1, v0

    const/16 v4, 0x30

    if-eq v1, v4, :cond_0

    aget-byte v1, p1, v0

    const/16 v4, 0x31

    if-eq v1, v4, :cond_0

    aget-byte v0, p1, v0

    const/16 v1, 0x32

    if-ne v0, v1, :cond_1

    :cond_0
    new-array v0, v3, [B

    invoke-static {p1, v2, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0

    :cond_1
    const/4 p1, 0x0

    return-object p1

    :cond_2
    const-wide/16 v4, 0x64

    invoke-static {v4, v5}, Landroid/os/SystemClock;->sleep(J)V

    goto :goto_0

    :cond_3
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Could not read the response data."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Occured the timeout during reading the response data."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private static a(Ljava/util/List;)[B
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;)[B"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    array-length v3, v3

    add-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-array v1, v2, [B

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    array-length v5, v5

    invoke-static {v4, v0, v1, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    array-length v4, v4

    add-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    return-object v1
.end method

.method private a([BBI)[B
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/16 p3, 0x100

    new-array p3, p3, [B

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    iget v6, p0, Lcom/starmicronics/stario/d;->d:I

    int-to-long v6, v6

    cmp-long v8, v4, v6

    if-gtz v8, :cond_6

    const-wide/16 v4, 0x64

    invoke-static {v4, v5}, Landroid/os/SystemClock;->sleep(J)V

    iget-object v4, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v5, p3

    sub-int/2addr v5, v3

    invoke-virtual {v4, p3, v3, v5}, Lcom/starmicronics/stario/StarIOPort;->readPort([BII)I

    move-result v4

    if-ltz v4, :cond_5

    add-int/2addr v3, v4

    new-array v4, v3, [B

    invoke-static {p3, v2, v4, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v5, p1

    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v5, v5, 0x1

    if-lt v3, v5, :cond_0

    const/4 v5, 0x0

    :goto_1
    array-length v6, p1

    if-ge v5, v6, :cond_2

    aget-byte v6, p1, v5

    aget-byte v7, v4, v5

    if-ne v6, v7, :cond_1

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "Read unexpected response data."

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    add-int/lit8 v5, v3, -0x1

    :goto_2
    array-length v6, p1

    if-lt v5, v6, :cond_0

    aget-byte v6, v4, v5

    if-ne v6, p2, :cond_3

    return-object v4

    :cond_3
    aget-byte v6, v4, v5

    const/4 v7, -0x1

    if-ne v6, v7, :cond_4

    goto :goto_0

    :cond_4
    add-int/lit8 v5, v5, -0x1

    goto :goto_2

    :cond_5
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "Could not read the response data."

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_6
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "Occured the timeout during reading the response data."

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private b()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iget-object v1, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v2, v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget v1, p0, Lcom/starmicronics/stario/d;->d:I

    invoke-direct {p0, v0, v3, v1}, Lcom/starmicronics/stario/d;->a([BBI)[B

    move-result-object v1

    array-length v2, v0

    aget-byte v2, v1, v2

    if-nez v2, :cond_0

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->NOSUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/d;->r:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v2, p0, Lcom/starmicronics/stario/d;->r:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    new-instance v2, Ljava/lang/String;

    array-length v3, v0

    array-length v4, v1

    array-length v0, v0

    sub-int/2addr v4, v0

    add-int/lit8 v4, v4, -0x1

    invoke-direct {v2, v1, v3, v4}, Ljava/lang/String;-><init>([BII)V

    move-object v0, v2

    :goto_0
    return-object v0

    nop

    :array_0
    .array-data 1
        0x1bt
        0x1ct
        0x26t
        0x52t
        0x32t
    .end array-data
.end method

.method private b(Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x7

    new-array v1, v1, [B

    const/4 v2, 0x0

    const/16 v3, 0x1b

    aput-byte v3, v1, v2

    const/4 v3, 0x1

    const/16 v4, 0x1c

    aput-byte v4, v1, v3

    const/4 v4, 0x2

    const/16 v5, 0x26

    aput-byte v5, v1, v4

    const/4 v4, 0x3

    const/16 v5, 0x53

    aput-byte v5, v1, v4

    const/4 v4, 0x4

    const/16 v5, 0x34

    aput-byte v5, v1, v4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    rem-int/lit16 v4, v4, 0x100

    int-to-byte v4, v4

    const/4 v5, 0x5

    aput-byte v4, v1, v5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    div-int/lit16 v4, v4, 0x100

    int-to-byte v4, v4

    const/4 v5, 0x6

    aput-byte v4, v1, v5

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Lcom/starmicronics/stario/d;->a(Ljava/util/List;)[B

    move-result-object p1

    iget-object v0, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v1, p1

    invoke-virtual {v0, p1, v2, v1}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget p1, p0, Lcom/starmicronics/stario/d;->d:I

    invoke-direct {p0, p1}, Lcom/starmicronics/stario/d;->a(I)[B

    move-result-object p1

    array-length v0, p1

    sub-int/2addr v0, v3

    aget-byte v0, p1, v0

    const/16 v1, 0x32

    if-eq v0, v1, :cond_1

    array-length v0, p1

    sub-int/2addr v0, v3

    aget-byte p1, p1, v0

    const/16 v0, 0x31

    if-eq p1, v0, :cond_0

    return-void

    :cond_0
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Returned the error response from printer."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Returned the non-supported response from printer."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private b(Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    if-eqz p1, :cond_0

    array-length p1, v0

    add-int/lit8 p1, p1, -0x1

    const/16 v1, 0x30

    aput-byte v1, v0, p1

    :cond_0
    iget-object p1, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    const/4 v1, 0x0

    array-length v2, v0

    invoke-virtual {p1, v0, v1, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget p1, p0, Lcom/starmicronics/stario/d;->d:I

    invoke-direct {p0, p1}, Lcom/starmicronics/stario/d;->a(I)[B

    move-result-object p1

    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-byte v0, p1, v0

    const/16 v1, 0x32

    if-eq v0, v1, :cond_2

    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-byte p1, p1, v0

    const/16 v0, 0x31

    if-eq p1, v0, :cond_1

    return-void

    :cond_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Returned the error response from printer."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Returned the non-supported response from printer."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :array_0
    .array-data 1
        0x1bt
        0x1ct
        0x26t
        0x53t
        0x35t
        0x1t
        0x0t
        0x31t
    .end array-data
.end method

.method private c()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iget-object v1, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v2, v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget v1, p0, Lcom/starmicronics/stario/d;->d:I

    invoke-direct {p0, v0, v3, v1}, Lcom/starmicronics/stario/d;->a([BBI)[B

    move-result-object v1

    array-length v2, v0

    aget-byte v2, v1, v2

    if-nez v2, :cond_0

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->NOSUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/d;->s:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v2, p0, Lcom/starmicronics/stario/d;->s:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    new-instance v2, Ljava/lang/String;

    array-length v3, v0

    array-length v4, v1

    array-length v0, v0

    sub-int/2addr v4, v0

    add-int/lit8 v4, v4, -0x1

    invoke-direct {v2, v1, v3, v4}, Ljava/lang/String;-><init>([BII)V

    move-object v0, v2

    :goto_0
    return-object v0

    nop

    :array_0
    .array-data 1
        0x1bt
        0x1ct
        0x26t
        0x52t
        0x34t
    .end array-data
.end method

.method private c(Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x7

    new-array v1, v1, [B

    const/4 v2, 0x0

    const/16 v3, 0x1b

    aput-byte v3, v1, v2

    const/4 v3, 0x1

    const/16 v4, 0x1c

    aput-byte v4, v1, v3

    const/4 v4, 0x2

    const/16 v5, 0x26

    aput-byte v5, v1, v4

    const/4 v4, 0x3

    const/16 v5, 0x53

    aput-byte v5, v1, v4

    const/4 v4, 0x4

    const/16 v5, 0x36

    aput-byte v5, v1, v4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    rem-int/lit16 v4, v4, 0x100

    int-to-byte v4, v4

    const/4 v5, 0x5

    aput-byte v4, v1, v5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    div-int/lit16 v4, v4, 0x100

    int-to-byte v4, v4

    const/4 v5, 0x6

    aput-byte v4, v1, v5

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Lcom/starmicronics/stario/d;->a(Ljava/util/List;)[B

    move-result-object p1

    iget-object v0, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v1, p1

    invoke-virtual {v0, p1, v2, v1}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget p1, p0, Lcom/starmicronics/stario/d;->d:I

    invoke-direct {p0, p1}, Lcom/starmicronics/stario/d;->a(I)[B

    move-result-object p1

    array-length v0, p1

    sub-int/2addr v0, v3

    aget-byte v0, p1, v0

    const/16 v1, 0x32

    if-eq v0, v1, :cond_1

    array-length v0, p1

    sub-int/2addr v0, v3

    aget-byte p1, p1, v0

    const/16 v0, 0x31

    if-eq p1, v0, :cond_0

    return-void

    :cond_0
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Returned the error response from printer."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Returned the non-supported response from printer."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private c(Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    if-eqz p1, :cond_0

    array-length p1, v0

    add-int/lit8 p1, p1, -0x1

    const/16 v1, 0x30

    aput-byte v1, v0, p1

    :cond_0
    iget-object p1, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    const/4 v1, 0x0

    array-length v2, v0

    invoke-virtual {p1, v0, v1, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget p1, p0, Lcom/starmicronics/stario/d;->d:I

    invoke-direct {p0, p1}, Lcom/starmicronics/stario/d;->a(I)[B

    move-result-object p1

    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-byte v0, p1, v0

    const/16 v1, 0x32

    if-eq v0, v1, :cond_2

    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-byte p1, p1, v0

    const/16 v0, 0x31

    if-eq p1, v0, :cond_1

    return-void

    :cond_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Returned the error response from printer."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Returned the non-supported response from printer."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :array_0
    .array-data 1
        0x1bt
        0x1ct
        0x26t
        0x53t
        0x39t
        0x1t
        0x0t
        0x31t
    .end array-data
.end method

.method private d()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iget-object v1, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v2, v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget v1, p0, Lcom/starmicronics/stario/d;->d:I

    invoke-direct {p0, v0, v3, v1}, Lcom/starmicronics/stario/d;->a([BBI)[B

    move-result-object v1

    array-length v2, v0

    aget-byte v2, v1, v2

    if-nez v2, :cond_0

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->NOSUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/d;->t:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v2, p0, Lcom/starmicronics/stario/d;->t:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    new-instance v2, Ljava/lang/String;

    array-length v3, v0

    array-length v4, v1

    array-length v0, v0

    sub-int/2addr v4, v0

    add-int/lit8 v4, v4, -0x1

    invoke-direct {v2, v1, v3, v4}, Ljava/lang/String;-><init>([BII)V

    move-object v0, v2

    :goto_0
    return-object v0

    nop

    :array_0
    .array-data 1
        0x1bt
        0x1ct
        0x26t
        0x52t
        0x36t
    .end array-data
.end method

.method private d(Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x7

    new-array v1, v1, [B

    const/4 v2, 0x0

    const/16 v3, 0x1b

    aput-byte v3, v1, v2

    const/4 v3, 0x1

    const/16 v4, 0x1c

    aput-byte v4, v1, v3

    const/4 v4, 0x2

    const/16 v5, 0x26

    aput-byte v5, v1, v4

    const/4 v4, 0x3

    const/16 v5, 0x53

    aput-byte v5, v1, v4

    const/16 v4, 0x31

    const/4 v5, 0x4

    aput-byte v4, v1, v5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    rem-int/lit16 v5, v5, 0x100

    int-to-byte v5, v5

    const/4 v6, 0x5

    aput-byte v5, v1, v6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    div-int/lit16 v5, v5, 0x100

    int-to-byte v5, v5

    const/4 v6, 0x6

    aput-byte v5, v1, v6

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Lcom/starmicronics/stario/d;->a(Ljava/util/List;)[B

    move-result-object p1

    iget-object v0, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v1, p1

    invoke-virtual {v0, p1, v2, v1}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget p1, p0, Lcom/starmicronics/stario/d;->d:I

    invoke-direct {p0, p1}, Lcom/starmicronics/stario/d;->a(I)[B

    move-result-object p1

    array-length v0, p1

    sub-int/2addr v0, v3

    aget-byte v0, p1, v0

    const/16 v1, 0x32

    if-eq v0, v1, :cond_1

    array-length v0, p1

    sub-int/2addr v0, v3

    aget-byte p1, p1, v0

    if-eq p1, v4, :cond_0

    return-void

    :cond_0
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Returned the error response from printer."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Returned the non-supported response from printer."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private d(Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    if-eqz p1, :cond_0

    array-length p1, v0

    add-int/lit8 p1, p1, -0x1

    const/16 v1, 0x30

    aput-byte v1, v0, p1

    :cond_0
    iget-object p1, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    const/4 v1, 0x0

    array-length v2, v0

    invoke-virtual {p1, v0, v1, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget p1, p0, Lcom/starmicronics/stario/d;->d:I

    invoke-direct {p0, p1}, Lcom/starmicronics/stario/d;->a(I)[B

    move-result-object p1

    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-byte v0, p1, v0

    const/16 v1, 0x32

    if-eq v0, v1, :cond_2

    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-byte p1, p1, v0

    const/16 v0, 0x31

    if-eq p1, v0, :cond_1

    return-void

    :cond_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Returned the error response from printer."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Returned the non-supported response from printer."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :array_0
    .array-data 1
        0x1bt
        0x1ct
        0x26t
        0x53t
        0x3at
        0x1t
        0x0t
        0x31t
    .end array-data
.end method

.method private e()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iget-object v1, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v2, v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget v1, p0, Lcom/starmicronics/stario/d;->d:I

    invoke-direct {p0, v0, v3, v1}, Lcom/starmicronics/stario/d;->a([BBI)[B

    move-result-object v1

    array-length v2, v0

    aget-byte v2, v1, v2

    if-nez v2, :cond_0

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->NOSUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/d;->v:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v2, p0, Lcom/starmicronics/stario/d;->v:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    array-length v0, v0

    aget-byte v0, v1, v0

    const/16 v1, 0x30

    if-ne v0, v1, :cond_1

    const/4 v3, 0x1

    :cond_1
    :goto_0
    return v3

    :array_0
    .array-data 1
        0x1bt
        0x1ct
        0x26t
        0x52t
        0x35t
    .end array-data
.end method

.method private f()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;->DISABLE:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    const/4 v1, 0x5

    new-array v1, v1, [B

    fill-array-data v1, :array_0

    iget-object v2, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v3, v1

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4, v3}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget v2, p0, Lcom/starmicronics/stario/d;->d:I

    invoke-direct {p0, v1, v4, v2}, Lcom/starmicronics/stario/d;->a([BBI)[B

    move-result-object v2

    array-length v3, v1

    aget-byte v3, v2, v3

    if-nez v3, :cond_0

    sget-object v1, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->NOSUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v1, p0, Lcom/starmicronics/stario/d;->u:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    goto :goto_0

    :cond_0
    sget-object v3, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v3, p0, Lcom/starmicronics/stario/d;->u:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    array-length v1, v1

    aget-byte v1, v2, v1

    const/16 v2, 0x33

    if-ne v1, v2, :cond_1

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;->PINCODE:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    :cond_1
    :goto_0
    return-object v0

    nop

    :array_0
    .array-data 1
        0x1bt
        0x1ct
        0x26t
        0x52t
        0x33t
    .end array-data
.end method

.method private g()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iget-object v1, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v2, v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget v1, p0, Lcom/starmicronics/stario/d;->d:I

    invoke-direct {p0, v0, v3, v1}, Lcom/starmicronics/stario/d;->a([BBI)[B

    move-result-object v1

    array-length v2, v0

    aget-byte v2, v1, v2

    if-nez v2, :cond_0

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->NOSUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/d;->x:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v2, p0, Lcom/starmicronics/stario/d;->x:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    array-length v0, v0

    aget-byte v0, v1, v0

    const/16 v1, 0x30

    if-ne v0, v1, :cond_1

    const/4 v3, 0x1

    :cond_1
    :goto_0
    return v3

    :array_0
    .array-data 1
        0x1bt
        0x1ct
        0x26t
        0x52t
        0x39t
    .end array-data
.end method

.method private h()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iget-object v1, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v2, v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget v1, p0, Lcom/starmicronics/stario/d;->d:I

    invoke-direct {p0, v0, v3, v1}, Lcom/starmicronics/stario/d;->a([BBI)[B

    move-result-object v1

    array-length v2, v0

    aget-byte v2, v1, v2

    if-nez v2, :cond_0

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->NOSUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/d;->y:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v2, p0, Lcom/starmicronics/stario/d;->y:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    array-length v0, v0

    aget-byte v0, v1, v0

    const/16 v1, 0x30

    if-ne v0, v1, :cond_1

    const/4 v3, 0x1

    :cond_1
    :goto_0
    return v3

    :array_0
    .array-data 1
        0x1bt
        0x1ct
        0x26t
        0x52t
        0x3at
    .end array-data
.end method

.method private i()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->NOSUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/d;->w:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    const/4 v0, 0x1

    return v0
.end method

.method private j()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iget-object v1, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v2, v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget v0, p0, Lcom/starmicronics/stario/d;->d:I

    invoke-direct {p0, v0}, Lcom/starmicronics/stario/d;->a(I)[B

    move-result-object v0

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-byte v1, v0, v1

    const/16 v2, 0x32

    if-eq v1, v2, :cond_1

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-byte v0, v0, v1

    const/16 v1, 0x31

    if-eq v0, v1, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Returned write error response from printer."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Returned write non-supported response from printer."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :array_0
    .array-data 1
        0x1bt
        0x1ct
        0x26t
        0x57t
    .end array-data
.end method


# virtual methods
.method public apply()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/starmicronics/stario/d;->isOpened()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Lcom/starmicronics/stario/d;->p:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/starmicronics/stario/d;->a(Z)V

    :cond_0
    iget-object v0, p0, Lcom/starmicronics/stario/d;->r:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v1, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/starmicronics/stario/d;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Lcom/starmicronics/stario/d;->a(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/starmicronics/stario/d;->s:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v1, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/starmicronics/stario/d;->f:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-direct {p0, v0}, Lcom/starmicronics/stario/d;->b(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/starmicronics/stario/d;->t:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v1, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/starmicronics/stario/d;->g:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-direct {p0, v0}, Lcom/starmicronics/stario/d;->c(Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/starmicronics/stario/d;->v:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v1, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    if-ne v0, v1, :cond_4

    iget-boolean v0, p0, Lcom/starmicronics/stario/d;->j:Z

    invoke-direct {p0, v0}, Lcom/starmicronics/stario/d;->b(Z)V

    :cond_4
    iget-object v0, p0, Lcom/starmicronics/stario/d;->u:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v1, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/starmicronics/stario/d;->n:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    invoke-direct {p0, v0}, Lcom/starmicronics/stario/d;->a(Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;)V

    :cond_5
    iget-object v0, p0, Lcom/starmicronics/stario/d;->q:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v1, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/starmicronics/stario/d;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    invoke-direct {p0, v0}, Lcom/starmicronics/stario/d;->d(Ljava/lang/String;)V

    :cond_6
    iget-object v0, p0, Lcom/starmicronics/stario/d;->x:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v1, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    if-ne v0, v1, :cond_7

    iget-boolean v0, p0, Lcom/starmicronics/stario/d;->l:Z

    invoke-direct {p0, v0}, Lcom/starmicronics/stario/d;->c(Z)V

    :cond_7
    iget-object v0, p0, Lcom/starmicronics/stario/d;->y:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v1, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    if-ne v0, v1, :cond_8

    iget-boolean v0, p0, Lcom/starmicronics/stario/d;->m:Z

    invoke-direct {p0, v0}, Lcom/starmicronics/stario/d;->d(Z)V

    :cond_8
    invoke-direct {p0}, Lcom/starmicronics/stario/d;->j()V

    return-void

    :cond_9
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Do not open a port."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    iget-object v0, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    invoke-static {v0}, Lcom/starmicronics/stario/StarIOPort;->releasePort(Lcom/starmicronics/stario/StarIOPort;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/starmicronics/stario/d;->i:Z

    return-void
.end method

.method public getAutoConnect()Z
    .locals 1

    iget-boolean v0, p0, Lcom/starmicronics/stario/d;->j:Z

    return v0
.end method

.method public getAutoConnectCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/d;->v:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    return-object v0
.end method

.method public getBluetoothDeviceName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/d;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getBluetoothDeviceNameCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/d;->r:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    return-object v0
.end method

.method public getBluetoothDiagnosticMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/starmicronics/stario/d;->m:Z

    return v0
.end method

.method public getBluetoothDiagnosticModeCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/d;->y:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    return-object v0
.end method

.method public getDiscoveryPermission()Z
    .locals 1

    iget-boolean v0, p0, Lcom/starmicronics/stario/d;->l:Z

    return v0
.end method

.method public getDiscoveryPermissionCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/d;->x:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    return-object v0
.end method

.method public getPairingPermission()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getPairingPermissionCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/d;->w:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    return-object v0
.end method

.method public getPinCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/d;->h:Ljava/lang/String;

    return-object v0
.end method

.method public getPinCodeCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/d;->q:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    return-object v0
.end method

.method public getSecurityType()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/d;->n:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    return-object v0
.end method

.method public getSecurityTypeCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/d;->u:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    return-object v0
.end method

.method public getiOSPortName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/d;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getiOSPortNameCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/d;->s:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    return-object v0
.end method

.method public isOpened()Z
    .locals 1

    iget-boolean v0, p0, Lcom/starmicronics/stario/d;->i:Z

    return v0
.end method

.method public loadSetting()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/starmicronics/stario/d;->a(Z)V

    return-void
.end method

.method public open()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/starmicronics/stario/d;->isOpened()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    :try_start_0
    sget-object v0, Lcom/starmicronics/stario/d;->b:Ljava/lang/String;

    sget-object v2, Lcom/starmicronics/stario/d;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/starmicronics/stario/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v1, p0, Lcom/starmicronics/stario/d;->i:Z
    :try_end_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    invoke-static {v1}, Lcom/starmicronics/stario/StarIOPort;->releasePort(Lcom/starmicronics/stario/StarIOPort;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/starmicronics/stario/d;->a:Lcom/starmicronics/stario/StarIOPort;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/starmicronics/stario/d;->i:Z

    throw v0
.end method

.method public setAutoConnect(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/starmicronics/stario/d;->j:Z

    return-void
.end method

.method public setBluetoothDeviceName(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/starmicronics/stario/d;->e:Ljava/lang/String;

    iput-object p1, p0, Lcom/starmicronics/stario/d;->g:Ljava/lang/String;

    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x10

    if-gt v0, v1, :cond_2

    const-string v0, "^[0-9a-zA-Z;:!?#$%&,.@_\\-= \\\\/\\*\\+~\\^\\[\\{\\(\\]\\}\\)\\|]+$"

    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object p1, p0, Lcom/starmicronics/stario/d;->e:Ljava/lang/String;

    iput-object p1, p0, Lcom/starmicronics/stario/d;->g:Ljava/lang/String;

    return-void

    :cond_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Invalid bluetooth device name characters."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid bluetooth device name length. length = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setBluetoothDiagnosticMode(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/starmicronics/stario/d;->m:Z

    return-void
.end method

.method public setDiscoveryPermission(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/starmicronics/stario/d;->l:Z

    return-void
.end method

.method public setPairingPermission(Z)V
    .locals 0

    return-void
.end method

.method public setPinCode(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/starmicronics/stario/d;->h:Ljava/lang/String;

    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x10

    if-gt v0, v1, :cond_2

    const-string v0, "^[0-9a-zA-Z]+$"

    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object p1, p0, Lcom/starmicronics/stario/d;->h:Ljava/lang/String;

    return-void

    :cond_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Invalid pincode characters."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid pincode characters length. length = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setSecurityType(Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/stario/d;->n:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    return-void
.end method

.method public setiOSPortName(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/starmicronics/stario/d;->f:Ljava/lang/String;

    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x10

    if-gt v0, v1, :cond_2

    const-string v0, "^[0-9a-zA-Z;:!?#$%&,.@_\\-= \\\\/\\*\\+~\\^\\[\\{\\(\\]\\}\\)\\|]+$"

    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object p1, p0, Lcom/starmicronics/stario/d;->f:Ljava/lang/String;

    return-void

    :cond_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Invalid iOS port name characters."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid iOS port name length. length = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
