.class Lcom/starmicronics/stario/b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/starmicronics/stario/c;


# static fields
.field private static b:Ljava/lang/String;

.field private static c:Ljava/lang/String;


# instance fields
.field private a:Lcom/starmicronics/stario/StarIOPort;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

.field private n:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

.field private o:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

.field private p:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

.field private q:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

.field private r:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

.field private s:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

.field private t:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

.field private u:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

.field private v:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

.field private w:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;ILcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    const/16 v1, 0x2710

    iput v1, p0, Lcom/starmicronics/stario/b;->d:I

    iput-object v0, p0, Lcom/starmicronics/stario/b;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/starmicronics/stario/b;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/starmicronics/stario/b;->g:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/starmicronics/stario/b;->h:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/b;->i:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/starmicronics/stario/b;->j:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/b;->k:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/b;->l:Z

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;->SSP:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    iput-object v0, p0, Lcom/starmicronics/stario/b;->m:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;->StarDeviceTypeDesktopPrinter:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    iput-object v0, p0, Lcom/starmicronics/stario/b;->n:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/b;->o:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/b;->p:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/b;->q:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->NOSUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/b;->r:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/b;->s:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/b;->t:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/b;->u:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/b;->v:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;->SUPPORT:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    iput-object v0, p0, Lcom/starmicronics/stario/b;->w:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;->StarDeviceTypePortablePrinter:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    if-eq p4, v0, :cond_1

    const-string v0, "BT:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sput-object p1, Lcom/starmicronics/stario/b;->b:Ljava/lang/String;

    sput-object p2, Lcom/starmicronics/stario/b;->c:Ljava/lang/String;

    iput p3, p0, Lcom/starmicronics/stario/b;->d:I

    iput-object p4, p0, Lcom/starmicronics/stario/b;->n:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    return-void

    :cond_0
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "This fuction is available form the bluetooth interface."

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "The portable printer is not supported."

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a(Ljava/lang/String;II)Ljava/lang/String;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/16 v0, 0x200

    new-array v0, v0, [B

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v1

    int-to-long v7, p3

    cmp-long v9, v5, v7

    if-gtz v9, :cond_7

    const-wide/16 v5, 0xc8

    invoke-static {v5, v6}, Landroid/os/SystemClock;->sleep(J)V

    iget-object v5, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v6, v0

    sub-int/2addr v6, v4

    invoke-virtual {v5, v0, v4, v6}, Lcom/starmicronics/stario/StarIOPort;->readPort([BII)I

    move-result v5

    if-ltz v5, :cond_6

    add-int/2addr v4, v5

    new-array v5, v4, [B

    invoke-static {v0, v3, v5, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v5}, Ljava/lang/String;-><init>([B)V

    const-string v5, "\r\nERROR\r\n"

    invoke-virtual {v6, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    const/4 v7, -0x1

    if-ne v5, v7, :cond_5

    const-string v5, "\r\nOK\r\n"

    invoke-virtual {v6, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    if-eq v8, v7, :cond_0

    if-eqz p1, :cond_4

    const-string v8, ""

    if-ne p1, v8, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v6, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-ne v5, v7, :cond_2

    goto :goto_0

    :cond_2
    invoke-virtual {v6, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p3

    if-nez p2, :cond_3

    return-object p3

    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    add-int/2addr p1, p2

    invoke-virtual {p3, v3, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_4
    :goto_1
    return-object v5

    :cond_5
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "Returned the error response from printer."

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_6
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "Could not read the response data."

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "Occured the timeout during reading the response data."

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    iget-object v0, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Landroid/os/SystemClock;->sleep(J)V

    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iget-object v1, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v2, v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    const-wide/16 v0, 0x5dc

    invoke-static {v0, v1}, Landroid/os/SystemClock;->sleep(J)V

    const-string v0, "AT*AILBA?\r"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iget-object v1, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v2, v0

    invoke-virtual {v1, v0, v3, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    const/16 v0, 0xc

    iget v1, p0, Lcom/starmicronics/stario/b;->d:I

    const-string v2, "*AILBA:"

    invoke-direct {p0, v2, v0, v1}, Lcom/starmicronics/stario/b;->a(Ljava/lang/String;II)Ljava/lang/String;

    return-void

    :cond_0
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "The port is null"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :array_0
    .array-data 1
        0x2ft
        0x2ft
        0x2ft
    .end array-data
.end method

.method private a(Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    iget-object p1, p0, Lcom/starmicronics/stario/b;->m:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;->SSP:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    if-ne p1, v0, :cond_0

    const-string p1, "AT*AGSM=4,1\r"

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/starmicronics/stario/b;->m:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;->DISABLE:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    if-ne p1, v0, :cond_1

    const-string p1, "AT*AGSM=1,1\r"

    goto :goto_0

    :cond_1
    const-string p1, "AT*AGSM=2,1\r"

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    iget-object v0, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v1, p1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2, v1}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    const/4 p1, 0x0

    iget v0, p0, Lcom/starmicronics/stario/b;->d:I

    invoke-direct {p0, p1, v2, v0}, Lcom/starmicronics/stario/b;->a(Ljava/lang/String;II)Ljava/lang/String;

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "AT*AGLN=\"%s\",1\r"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    iget-object v0, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v2, p1

    invoke-virtual {v0, p1, v1, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget p1, p0, Lcom/starmicronics/stario/b;->d:I

    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, p1}, Lcom/starmicronics/stario/b;->a(Ljava/lang/String;II)Ljava/lang/String;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    iget v0, p0, Lcom/starmicronics/stario/b;->d:I

    invoke-static {p1, p2, v0}, Lcom/starmicronics/stario/StarIOPort;->getPort(Ljava/lang/String;Ljava/lang/String;I)Lcom/starmicronics/stario/StarIOPort;

    move-result-object p1

    iput-object p1, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    const-wide/16 p1, 0x5dc

    invoke-static {p1, p2}, Landroid/os/SystemClock;->sleep(J)V

    return-void
.end method

.method private a(Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "0"

    if-eqz p1, :cond_0

    move-object v3, v2

    goto :goto_0

    :cond_0
    const-string v3, "255"

    :goto_0
    const/4 v4, 0x0

    aput-object v3, v1, v4

    const-string v3, "AT*ADDCP=%s,1\r"

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    iget-object v3, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v5, v1

    invoke-virtual {v3, v1, v4, v5}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget v1, p0, Lcom/starmicronics/stario/b;->d:I

    const/4 v3, 0x0

    invoke-direct {p0, v3, v4, v1}, Lcom/starmicronics/stario/b;->a(Ljava/lang/String;II)Ljava/lang/String;

    new-array v1, v0, [Ljava/lang/Object;

    if-eqz p1, :cond_1

    const-string v2, "1"

    :cond_1
    aput-object v2, v1, v4

    const-string v2, "AT*ADNRP=%s,1\r"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    iget-object v2, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v5, v1

    invoke-virtual {v2, v1, v4, v5}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget v1, p0, Lcom/starmicronics/stario/b;->d:I

    invoke-direct {p0, v3, v4, v1}, Lcom/starmicronics/stario/b;->a(Ljava/lang/String;II)Ljava/lang/String;

    new-array v0, v0, [Ljava/lang/Object;

    if-eqz p1, :cond_2

    const-string p1, "0,0012F3112233,2,1,\"\""

    goto :goto_1

    :cond_2
    const-string p1, "0,000000000000,0,0,\"\""

    :goto_1
    aput-object p1, v0, v4

    const-string p1, "AT*ADWDRP=%s,1\r"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    iget-object v0, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v1, p1

    invoke-virtual {v0, p1, v4, v1}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget p1, p0, Lcom/starmicronics/stario/b;->d:I

    invoke-direct {p0, v3, v4, p1}, Lcom/starmicronics/stario/b;->a(Ljava/lang/String;II)Ljava/lang/String;

    return-void
.end method

.method private b()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const-string v0, "AT*AGLN?\r"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iget-object v1, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v2, v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget v0, p0, Lcom/starmicronics/stario/b;->d:I

    const-string v1, "*AGLN:"

    invoke-direct {p0, v1, v3, v0}, Lcom/starmicronics/stario/b;->a(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const-string v0, "AT*ADIPS?\r"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iget-object v1, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v2, v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget v0, p0, Lcom/starmicronics/stario/b;->d:I

    const-string v1, "*ADIPS:"

    invoke-direct {p0, v1, v3, v0}, Lcom/starmicronics/stario/b;->a(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\r"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v4, 0x1

    add-int/2addr v2, v4

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v1, v4

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v3

    aput-object v0, v1, v4

    const-string p1, "AT*ADIPS=\"%s\"%s,1\r"

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    iget-object v0, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v1, p1

    invoke-virtual {v0, p1, v3, v1}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget p1, p0, Lcom/starmicronics/stario/b;->d:I

    const/4 v0, 0x0

    invoke-direct {p0, v0, v3, p1}, Lcom/starmicronics/stario/b;->a(Ljava/lang/String;II)Ljava/lang/String;

    return-void
.end method

.method private c()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const-string v0, "AT*ADIPS?\r"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iget-object v1, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v2, v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget v0, p0, Lcom/starmicronics/stario/b;->d:I

    const-string v1, "*ADIPS:"

    invoke-direct {p0, v1, v3, v0}, Lcom/starmicronics/stario/b;->a(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0x10

    if-gt v0, v2, :cond_0

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "AT*AGFP=\"%s\",1\r"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    iget-object v0, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v2, p1

    invoke-virtual {v0, p1, v1, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    const/4 p1, 0x0

    iget v0, p0, Lcom/starmicronics/stario/b;->d:I

    invoke-direct {p0, p1, v1, v0}, Lcom/starmicronics/stario/b;->a(Ljava/lang/String;II)Ljava/lang/String;

    return-void

    :cond_0
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "The length of entered pin code was invalid. Valid length is from 1 to 16."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private d()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const-string v0, "AT*ADDCP?\r"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iget-object v1, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v2, v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget v0, p0, Lcom/starmicronics/stario/b;->d:I

    const-string v1, "*ADDCP:"

    invoke-direct {p0, v1, v3, v0}, Lcom/starmicronics/stario/b;->a(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "*ADDCP:0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const-string v1, "*ADDCP:255"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    const/4 v0, 0x0

    :goto_0
    const-string v1, "AT*ADNRP?\r"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    iget-object v4, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v5, v1

    invoke-virtual {v4, v1, v3, v5}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget v1, p0, Lcom/starmicronics/stario/b;->d:I

    const-string v4, "*ADNRP:"

    invoke-direct {p0, v4, v3, v1}, Lcom/starmicronics/stario/b;->a(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    const-string v4, "*ADNRP:1"

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const-string v4, "*ADNRP:0"

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    const/4 v1, 0x0

    :goto_1
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    return v2

    :cond_2
    return v3
.end method

.method private e()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const-string v0, "AT*AGSM?\r"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iget-object v1, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v2, v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget v0, p0, Lcom/starmicronics/stario/b;->d:I

    const-string v1, "*AGSM:"

    invoke-direct {p0, v1, v3, v0}, Lcom/starmicronics/stario/b;->a(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "*AGSM:2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;->PINCODE:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    return-object v0

    :cond_0
    const-string v1, "*AGSM:4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;->SSP:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private f()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    iget-boolean v0, p0, Lcom/starmicronics/stario/b;->j:Z

    if-eqz v0, :cond_0

    const-string v0, "AT*AGPM=2,1\r"

    goto :goto_0

    :cond_0
    const-string v0, "AT*AGPM=1,1\r"

    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iget-object v1, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v2, v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    const/4 v0, 0x0

    iget v1, p0, Lcom/starmicronics/stario/b;->d:I

    invoke-direct {p0, v0, v3, v1}, Lcom/starmicronics/stario/b;->a(Ljava/lang/String;II)Ljava/lang/String;

    return-void
.end method

.method private g()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const-string v0, "AT*AGPM?\r"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iget-object v1, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v2, v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    iget v0, p0, Lcom/starmicronics/stario/b;->d:I

    const-string v1, "*AGPM:"

    invoke-direct {p0, v1, v3, v0}, Lcom/starmicronics/stario/b;->a(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "*AGPM:1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v3

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private h()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    iget-object v0, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    if-eqz v0, :cond_0

    const-string v0, "AT*ADDM\r"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iget-object v1, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    array-length v2, v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    const/4 v0, 0x0

    iget v1, p0, Lcom/starmicronics/stario/b;->d:I

    invoke-direct {p0, v0, v3, v1}, Lcom/starmicronics/stario/b;->a(Ljava/lang/String;II)Ljava/lang/String;

    return-void

    :cond_0
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "The port is null"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public apply()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/starmicronics/stario/b;->isOpened()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/starmicronics/stario/b;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/starmicronics/stario/b;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/starmicronics/stario/b;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Lcom/starmicronics/stario/b;->b(Ljava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Lcom/starmicronics/stario/b;->i:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/starmicronics/stario/b;->m:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    sget-object v1, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;->PINCODE:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    if-eq v0, v1, :cond_2

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "When securityType is PINCODE, autoConnect can not set \"true\"."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    :goto_0
    iget-boolean v0, p0, Lcom/starmicronics/stario/b;->i:Z

    invoke-direct {p0, v0}, Lcom/starmicronics/stario/b;->a(Z)V

    iget-object v0, p0, Lcom/starmicronics/stario/b;->m:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    invoke-direct {p0, v0}, Lcom/starmicronics/stario/b;->a(Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;)V

    iget-object v0, p0, Lcom/starmicronics/stario/b;->m:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    sget-object v1, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;->PINCODE:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    if-ne v0, v1, :cond_4

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/starmicronics/stario/b;->a(Z)V

    :cond_4
    iget-object v0, p0, Lcom/starmicronics/stario/b;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    invoke-direct {p0, v0}, Lcom/starmicronics/stario/b;->c(Ljava/lang/String;)V

    :cond_5
    invoke-direct {p0}, Lcom/starmicronics/stario/b;->f()V

    return-void

    :cond_6
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Do not open a port."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0}, Lcom/starmicronics/stario/b;->h()V
    :try_end_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    invoke-static {v2}, Lcom/starmicronics/stario/StarIOPort;->releasePort(Lcom/starmicronics/stario/StarIOPort;)V

    iput-object v1, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    iput-boolean v0, p0, Lcom/starmicronics/stario/b;->h:Z

    return-void

    :catchall_0
    move-exception v2

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_1
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    iget-object v3, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    invoke-static {v3}, Lcom/starmicronics/stario/StarIOPort;->releasePort(Lcom/starmicronics/stario/StarIOPort;)V

    iput-object v1, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    iput-boolean v0, p0, Lcom/starmicronics/stario/b;->h:Z

    throw v2
.end method

.method public getAutoConnect()Z
    .locals 1

    iget-boolean v0, p0, Lcom/starmicronics/stario/b;->i:Z

    return v0
.end method

.method public getAutoConnectCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/b;->t:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    return-object v0
.end method

.method public getBluetoothDeviceName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/b;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getBluetoothDeviceNameCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/b;->p:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    return-object v0
.end method

.method public getBluetoothDiagnosticMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/starmicronics/stario/b;->l:Z

    return v0
.end method

.method public getBluetoothDiagnosticModeCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/b;->w:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    return-object v0
.end method

.method public getDiscoveryPermission()Z
    .locals 1

    iget-boolean v0, p0, Lcom/starmicronics/stario/b;->k:Z

    return v0
.end method

.method public getDiscoveryPermissionCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/b;->v:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    return-object v0
.end method

.method public getPairingPermission()Z
    .locals 1

    iget-boolean v0, p0, Lcom/starmicronics/stario/b;->j:Z

    return v0
.end method

.method public getPairingPermissionCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/b;->u:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    return-object v0
.end method

.method public getPinCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/b;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getPinCodeCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/b;->o:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    return-object v0
.end method

.method public getSecurityType()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/b;->m:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    return-object v0
.end method

.method public getSecurityTypeCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/b;->s:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    return-object v0
.end method

.method public getiOSPortName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/b;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getiOSPortNameCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/b;->q:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;

    return-object v0
.end method

.method public isOpened()Z
    .locals 1

    iget-boolean v0, p0, Lcom/starmicronics/stario/b;->h:Z

    return v0
.end method

.method public loadSetting()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/starmicronics/stario/b;->isOpened()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/starmicronics/stario/b;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/starmicronics/stario/b;->e:Ljava/lang/String;

    invoke-direct {p0}, Lcom/starmicronics/stario/b;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/starmicronics/stario/b;->f:Ljava/lang/String;

    invoke-direct {p0}, Lcom/starmicronics/stario/b;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/starmicronics/stario/b;->i:Z

    invoke-direct {p0}, Lcom/starmicronics/stario/b;->e()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    move-result-object v0

    iput-object v0, p0, Lcom/starmicronics/stario/b;->m:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    invoke-direct {p0}, Lcom/starmicronics/stario/b;->g()Z

    move-result v0

    iput-boolean v0, p0, Lcom/starmicronics/stario/b;->j:Z

    return-void

    :cond_0
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Do not open a port."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public open()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/starmicronics/stario/b;->isOpened()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    :try_start_0
    sget-object v0, Lcom/starmicronics/stario/b;->b:Ljava/lang/String;

    sget-object v2, Lcom/starmicronics/stario/b;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/starmicronics/stario/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/starmicronics/stario/b;->a()V

    iput-boolean v1, p0, Lcom/starmicronics/stario/b;->h:Z
    :try_end_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    invoke-static {v1}, Lcom/starmicronics/stario/StarIOPort;->releasePort(Lcom/starmicronics/stario/StarIOPort;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/starmicronics/stario/b;->a:Lcom/starmicronics/stario/StarIOPort;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/starmicronics/stario/b;->h:Z

    throw v0
.end method

.method public setAutoConnect(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/starmicronics/stario/b;->i:Z

    return-void
.end method

.method public setBluetoothDeviceName(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/starmicronics/stario/b;->e:Ljava/lang/String;

    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x10

    if-gt v0, v1, :cond_2

    const-string v0, "^[0-9a-zA-Z;:!?#$%&,.@_\\-= \\\\/\\*\\+~\\^\\[\\{\\(\\]\\}\\)\\|]+$"

    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object p1, p0, Lcom/starmicronics/stario/b;->e:Ljava/lang/String;

    return-void

    :cond_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Invalid bluetooth device name characters."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid bluetooth device name length. length = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setBluetoothDiagnosticMode(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/starmicronics/stario/b;->l:Z

    return-void
.end method

.method public setDiscoveryPermission(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/starmicronics/stario/b;->k:Z

    return-void
.end method

.method public setPairingPermission(Z)V
    .locals 3

    sget-object v0, Lcom/starmicronics/stario/b$1;->a:[I

    iget-object v1, p0, Lcom/starmicronics/stario/b;->m:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    invoke-virtual {v1}, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    iput-boolean p1, p0, Lcom/starmicronics/stario/b;->j:Z

    goto :goto_0

    :cond_0
    iput-boolean v1, p0, Lcom/starmicronics/stario/b;->j:Z

    :goto_0
    return-void
.end method

.method public setPinCode(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/starmicronics/stario/b;->g:Ljava/lang/String;

    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x10

    if-gt v0, v1, :cond_2

    const-string v0, "^[0-9a-zA-Z]+$"

    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object p1, p0, Lcom/starmicronics/stario/b;->g:Ljava/lang/String;

    return-void

    :cond_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Invalid pincode characters."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid pincode characters length. length = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setSecurityType(Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;)V
    .locals 3

    sget-object v0, Lcom/starmicronics/stario/b$1;->a:[I

    invoke-virtual {p1}, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    goto :goto_0

    :cond_0
    iput-boolean v1, p0, Lcom/starmicronics/stario/b;->j:Z

    :goto_0
    iput-object p1, p0, Lcom/starmicronics/stario/b;->m:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    return-void
.end method

.method public setiOSPortName(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/starmicronics/stario/b;->f:Ljava/lang/String;

    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x10

    if-gt v0, v1, :cond_2

    const-string v0, "^[0-9a-zA-Z;:!?#$%&,.@_\\-= \\\\/\\*\\+~\\^\\[\\{\\(\\]\\}\\)\\|]+$"

    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object p1, p0, Lcom/starmicronics/stario/b;->f:Ljava/lang/String;

    return-void

    :cond_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Invalid iOS port name characters."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid iOS port name length. length = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
