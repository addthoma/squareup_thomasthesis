.class Lcom/starmicronics/stario/StarProxiPRNTManager$b;
.super Landroid/os/AsyncTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/stario/StarProxiPRNTManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/starmicronics/stario/StarProxiPRNTManager;


# direct methods
.method private constructor <init>(Lcom/starmicronics/stario/StarProxiPRNTManager;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager$b;->a:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/starmicronics/stario/StarProxiPRNTManager;Lcom/starmicronics/stario/StarProxiPRNTManager$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/starmicronics/stario/StarProxiPRNTManager$b;-><init>(Lcom/starmicronics/stario/StarProxiPRNTManager;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 9

    const/4 p1, 0x0

    const/4 v0, 0x0

    :goto_0
    invoke-static {}, Lcom/starmicronics/stario/StarProxiPRNTManager;->a()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/starmicronics/stario/StarProxiPRNTManager$b;->a:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v2}, Lcom/starmicronics/stario/StarProxiPRNTManager;->a(Lcom/starmicronics/stario/StarProxiPRNTManager;)Z

    move-result v2

    if-nez v2, :cond_0

    monitor-exit v1

    const/4 p1, 0x0

    return-object p1

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const-wide/16 v2, 0x3e8

    goto :goto_1

    :cond_1
    const-wide/16 v2, 0x1388

    :goto_1
    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    invoke-static {}, Lcom/starmicronics/stario/StarProxiPRNTManager;->a()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    :try_start_1
    iget-object v2, p0, Lcom/starmicronics/stario/StarProxiPRNTManager$b;->a:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v2}, Lcom/starmicronics/stario/StarProxiPRNTManager;->b(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/starmicronics/stario/StarProxiPRNTManager$b;->a:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v2}, Lcom/starmicronics/stario/StarProxiPRNTManager;->b(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    iget-object v4, p0, Lcom/starmicronics/stario/StarProxiPRNTManager$b;->a:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v4}, Lcom/starmicronics/stario/StarProxiPRNTManager;->b(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/HashMap;

    const-string v5, "Date"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Date;

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v7, "hhmmss"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v6, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    iget-object v6, p0, Lcom/starmicronics/stario/StarProxiPRNTManager$b;->a:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v6}, Lcom/starmicronics/stario/StarProxiPRNTManager;->b(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/HashMap;

    const-string v7, "found Port Name"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    sub-float/2addr v5, v4

    const/high16 v4, 0x40a00000    # 5.0f

    cmpl-float v4, v5, v4

    if-ltz v4, :cond_2

    iget-object v4, p0, Lcom/starmicronics/stario/StarProxiPRNTManager$b;->a:Lcom/starmicronics/stario/StarProxiPRNTManager;

    invoke-static {v4}, Lcom/starmicronics/stario/StarProxiPRNTManager;->c(Lcom/starmicronics/stario/StarProxiPRNTManager;)Lcom/starmicronics/stario/StarProxiPRNTManagerCallback;

    move-result-object v4

    invoke-interface {v4, v6, v3}, Lcom/starmicronics/stario/StarProxiPRNTManagerCallback;->onStateUpdated(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/util/ConcurrentModificationException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    goto :goto_3

    :catchall_0
    move-exception p1

    goto :goto_4

    :catch_0
    :goto_3
    :try_start_2
    monitor-exit v0

    move v0, v1

    goto/16 :goto_0

    :goto_4
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1

    :catchall_1
    move-exception p1

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw p1
.end method

.method protected a(Ljava/lang/Void;)V
    .locals 0

    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/starmicronics/stario/StarProxiPRNTManager$b;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/starmicronics/stario/StarProxiPRNTManager$b;->a(Ljava/lang/Void;)V

    return-void
.end method
