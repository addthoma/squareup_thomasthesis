.class Lcom/starmicronics/stario/e;
.super Lcom/starmicronics/stario/StarIOPort;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/stario/e$a;
    }
.end annotation


# static fields
.field private static e:Landroid/content/Context;

.field private static final p:[I

.field private static final q:[I

.field private static w:Landroid/content/Context;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:I

.field private f:Lcom/starmicronics/stario/e$a;

.field private g:Landroid/hardware/usb/UsbDevice;

.field private h:Landroid/hardware/usb/UsbInterface;

.field private i:Landroid/hardware/usb/UsbEndpoint;

.field private j:Landroid/hardware/usb/UsbEndpoint;

.field private k:Landroid/hardware/usb/UsbDeviceConnection;

.field private l:Landroid/hardware/usb/UsbManager;

.field private m:Z

.field private volatile n:Z

.field private o:Z

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Z

.field private final v:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/starmicronics/stario/e;->p:[I

    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/starmicronics/stario/e;->q:[I

    return-void

    :array_0
    .array-data 4
        0x2
        0x4
        0x6
        0xa
        0xc
        0xe
    .end array-data

    :array_1
    .array-data 4
        0x1
        0x3
        0x5
        0x9
        0xb
        0xf
        0x11
        0x15
        0x17
        0x43
        0x47
        0x49
    .end array-data
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/starmicronics/stario/StarIOPort;-><init>()V

    const-string v0, "StarLine"

    iput-object v0, p0, Lcom/starmicronics/stario/e;->r:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/starmicronics/stario/e;->s:Ljava/lang/String;

    iput-object v0, p0, Lcom/starmicronics/stario/e;->t:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/starmicronics/stario/e;->u:Z

    new-instance v0, Lcom/starmicronics/stario/e$1;

    invoke-direct {v0, p0}, Lcom/starmicronics/stario/e$1;-><init>(Lcom/starmicronics/stario/e;)V

    iput-object v0, p0, Lcom/starmicronics/stario/e;->v:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/starmicronics/stario/e;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/starmicronics/stario/e;->b:Ljava/lang/String;

    iput p3, p0, Lcom/starmicronics/stario/e;->c:I

    iput p3, p0, Lcom/starmicronics/stario/e;->d:I

    sput-object p4, Lcom/starmicronics/stario/e;->e:Landroid/content/Context;

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/starmicronics/stario/e;->g:Landroid/hardware/usb/UsbDevice;

    invoke-direct {p0}, Lcom/starmicronics/stario/e;->d()V

    return-void
.end method

.method private a([B)I
    .locals 3

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    add-int/lit8 v1, v1, -0x6

    if-ge v0, v1, :cond_1

    aget-byte v1, p1, v0

    const/16 v2, 0x1b

    if-ne v1, v2, :cond_0

    add-int/lit8 v1, v0, 0x1

    aget-byte v1, p1, v1

    const/16 v2, 0x1d

    if-ne v1, v2, :cond_0

    add-int/lit8 v1, v0, 0x2

    aget-byte v1, p1, v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    add-int/lit8 v0, v0, 0x6

    aget-byte p1, p1, v0

    return p1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method private a([BII)I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    new-array v0, p3, [B

    invoke-virtual {p0, v0, p2, p3}, Lcom/starmicronics/stario/e;->readPort([BII)I

    move-result p3

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v1, p3, :cond_1

    sub-int v3, p3, v1

    const/4 v4, 0x4

    if-lt v3, v4, :cond_0

    aget-byte v3, v0, v1

    and-int/lit16 v3, v3, 0x93

    const/16 v4, 0x10

    if-ne v3, v4, :cond_0

    add-int/lit8 v3, v1, 0x1

    aget-byte v3, v0, v3

    and-int/lit16 v3, v3, 0x90

    if-nez v3, :cond_0

    add-int/lit8 v3, v1, 0x2

    aget-byte v3, v0, v3

    and-int/lit16 v3, v3, 0x90

    if-nez v3, :cond_0

    add-int/lit8 v3, v1, 0x3

    aget-byte v3, v0, v3

    and-int/lit16 v3, v3, 0x90

    if-nez v3, :cond_0

    add-int/lit8 v1, v1, 0x4

    goto :goto_0

    :cond_0
    add-int v3, p2, v2

    aget-byte v4, v0, v1

    aput-byte v4, p1, v3

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method static synthetic a(Lcom/starmicronics/stario/e;)Landroid/content/BroadcastReceiver;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/stario/e;->v:Landroid/content/BroadcastReceiver;

    return-object p0
.end method

.method private a(I[Ljava/lang/String;Ljava/util/HashMap;)Landroid/hardware/usb/UsbDevice;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I[",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Landroid/hardware/usb/UsbDevice;",
            ">;)",
            "Landroid/hardware/usb/UsbDevice;"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v3, p2

    if-ge v1, v3, :cond_a

    aget-object v3, p2, v1

    invoke-virtual {p3, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v3}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v4

    const/16 v5, 0x519

    if-ne v4, v5, :cond_9

    iget-object v4, p0, Lcom/starmicronics/stario/e;->f:Lcom/starmicronics/stario/e$a;

    sget-object v5, Lcom/starmicronics/stario/e$a;->c:Lcom/starmicronics/stario/e$a;

    if-eq v4, v5, :cond_0

    iget-object v4, p0, Lcom/starmicronics/stario/e;->f:Lcom/starmicronics/stario/e$a;

    sget-object v5, Lcom/starmicronics/stario/e$a;->a:Lcom/starmicronics/stario/e$a;

    if-ne v4, v5, :cond_4

    :cond_0
    move v4, v2

    const/4 v2, 0x0

    :goto_1
    sget-object v5, Lcom/starmicronics/stario/e;->q:[I

    array-length v6, v5

    if-ge v2, v6, :cond_3

    aget v5, v5, v2

    invoke-virtual {v3}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v6

    if-ne v5, v6, :cond_1

    add-int/lit8 v4, v4, 0x1

    :cond_1
    if-ne v4, p1, :cond_2

    iput-boolean v0, p0, Lcom/starmicronics/stario/e;->o:Z

    return-object v3

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    move v2, v4

    :cond_4
    iget-object v4, p0, Lcom/starmicronics/stario/e;->f:Lcom/starmicronics/stario/e$a;

    sget-object v5, Lcom/starmicronics/stario/e$a;->c:Lcom/starmicronics/stario/e$a;

    if-eq v4, v5, :cond_5

    iget-object v4, p0, Lcom/starmicronics/stario/e;->f:Lcom/starmicronics/stario/e$a;

    sget-object v5, Lcom/starmicronics/stario/e$a;->b:Lcom/starmicronics/stario/e$a;

    if-ne v4, v5, :cond_9

    :cond_5
    move v4, v2

    const/4 v2, 0x0

    :goto_2
    sget-object v5, Lcom/starmicronics/stario/e;->p:[I

    array-length v6, v5

    if-ge v2, v6, :cond_8

    aget v5, v5, v2

    invoke-virtual {v3}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v6

    if-ne v5, v6, :cond_6

    add-int/lit8 v4, v4, 0x1

    :cond_6
    if-ne v4, p1, :cond_7

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/starmicronics/stario/e;->o:Z

    return-object v3

    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_8
    move v2, v4

    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_a
    const/4 p1, 0x0

    return-object p1
.end method

.method private a(Ljava/lang/String;[Ljava/lang/String;Ljava/util/HashMap;)Landroid/hardware/usb/UsbDevice;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Landroid/hardware/usb/UsbDevice;",
            ">;)",
            "Landroid/hardware/usb/UsbDevice;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    array-length v5, v2

    if-ge v4, v5, :cond_18

    aget-object v5, v2, v4

    move-object/from16 v6, p3

    invoke-virtual {v6, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v5}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v7

    const/16 v8, 0x519

    if-ne v7, v8, :cond_17

    iget-object v7, v0, Lcom/starmicronics/stario/e;->f:Lcom/starmicronics/stario/e$a;

    sget-object v8, Lcom/starmicronics/stario/e$a;->c:Lcom/starmicronics/stario/e$a;

    const-string v9, "SN:"

    const/4 v10, 0x3

    const-string v12, ""

    const/16 v14, 0x18

    const-string v15, "serial"

    const-string v11, "devnum"

    if-eq v7, v8, :cond_0

    iget-object v7, v0, Lcom/starmicronics/stario/e;->f:Lcom/starmicronics/stario/e$a;

    sget-object v8, Lcom/starmicronics/stario/e$a;->a:Lcom/starmicronics/stario/e$a;

    if-ne v7, v8, :cond_b

    :cond_0
    const/4 v7, 0x0

    :goto_1
    sget-object v8, Lcom/starmicronics/stario/e;->q:[I

    array-length v13, v8

    if-ge v7, v13, :cond_b

    aget v8, v8, v7

    invoke-virtual {v5}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v13

    if-ne v8, v13, :cond_a

    invoke-direct {v0, v5}, Lcom/starmicronics/stario/e;->a(Landroid/hardware/usb/UsbDevice;)Z

    move-result v8

    if-nez v8, :cond_1

    goto/16 :goto_4

    :cond_1
    invoke-virtual {v5}, Landroid/hardware/usb/UsbDevice;->getInterfaceCount()I

    move-result v8

    if-gtz v8, :cond_2

    goto/16 :goto_4

    :cond_2
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v8, v14, :cond_5

    invoke-virtual {v5}, Landroid/hardware/usb/UsbDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/starmicronics/stario/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v1, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    iput-boolean v3, v0, Lcom/starmicronics/stario/e;->o:Z

    return-object v5

    :cond_3
    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    iput-boolean v3, v0, Lcom/starmicronics/stario/e;->o:Z

    return-object v5

    :cond_4
    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    iput-boolean v3, v0, Lcom/starmicronics/stario/e;->o:Z

    return-object v5

    :cond_5
    invoke-virtual {v5, v3}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object v8

    iget-object v13, v0, Lcom/starmicronics/stario/e;->l:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v13, v5}, Landroid/hardware/usb/UsbManager;->openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual {v13, v8, v14}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    const/16 v14, 0x101

    new-array v10, v14, [B

    const/16 v18, 0xa1

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v23, 0x101

    const/16 v24, 0xc8

    move-object/from16 v17, v13

    move-object/from16 v22, v10

    invoke-virtual/range {v17 .. v24}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result v14

    invoke-virtual {v13, v8}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z

    invoke-virtual {v13}, Landroid/hardware/usb/UsbDeviceConnection;->close()V

    move-object v13, v12

    const/4 v8, 0x2

    :goto_2
    if-ge v8, v14, :cond_6

    aget-byte v3, v10, v8

    int-to-char v3, v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    add-int/lit8 v8, v8, 0x1

    move-object/from16 v2, p2

    const/4 v3, 0x0

    goto :goto_2

    :cond_6
    invoke-static {}, Lcom/starmicronics/stario/e;->e()Ljava/util/HashMap;

    move-result-object v2

    invoke-static {v5}, Lcom/starmicronics/stario/e;->b(Landroid/hardware/usb/UsbDevice;)I

    move-result v3

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v2, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/HashMap;

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    if-ne v3, v14, :cond_7

    invoke-virtual {v10, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v14

    if-eqz v14, :cond_7

    invoke-virtual {v1, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_7

    const/4 v10, 0x0

    iput-boolean v10, v0, Lcom/starmicronics/stario/e;->o:Z

    return-object v5

    :cond_7
    const/4 v10, 0x0

    goto :goto_3

    :cond_8
    const/4 v10, 0x0

    invoke-virtual {v1, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9

    invoke-virtual {v13, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_9

    iput-boolean v10, v0, Lcom/starmicronics/stario/e;->o:Z

    return-object v5

    :cond_9
    :try_start_0
    invoke-static {v5}, Lcom/starmicronics/stario/e;->b(Landroid/hardware/usb/UsbDevice;)I

    move-result v2

    invoke-static {}, Lcom/starmicronics/stario/e;->e()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    invoke-virtual {v3, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-ne v2, v3, :cond_a

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/starmicronics/stario/e;->o:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v5

    :catch_0
    :cond_a
    :goto_4
    add-int/lit8 v7, v7, 0x1

    move-object/from16 v2, p2

    const/4 v3, 0x0

    const/4 v10, 0x3

    const/16 v14, 0x18

    goto/16 :goto_1

    :cond_b
    iget-object v2, v0, Lcom/starmicronics/stario/e;->f:Lcom/starmicronics/stario/e$a;

    sget-object v3, Lcom/starmicronics/stario/e$a;->c:Lcom/starmicronics/stario/e$a;

    if-eq v2, v3, :cond_c

    iget-object v2, v0, Lcom/starmicronics/stario/e;->f:Lcom/starmicronics/stario/e$a;

    sget-object v3, Lcom/starmicronics/stario/e$a;->b:Lcom/starmicronics/stario/e$a;

    if-ne v2, v3, :cond_17

    :cond_c
    const/4 v2, 0x0

    :goto_5
    sget-object v3, Lcom/starmicronics/stario/e;->p:[I

    array-length v7, v3

    if-ge v2, v7, :cond_17

    aget v3, v3, v2

    invoke-virtual {v5}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v7

    if-ne v3, v7, :cond_16

    invoke-direct {v0, v5}, Lcom/starmicronics/stario/e;->a(Landroid/hardware/usb/UsbDevice;)Z

    move-result v3

    if-nez v3, :cond_d

    goto/16 :goto_8

    :cond_d
    invoke-virtual {v5}, Landroid/hardware/usb/UsbDevice;->getInterfaceCount()I

    move-result v3

    if-gtz v3, :cond_e

    goto/16 :goto_8

    :cond_e
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x18

    if-lt v3, v7, :cond_11

    invoke-virtual {v5}, Landroid/hardware/usb/UsbDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/starmicronics/stario/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x3

    invoke-virtual {v1, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_f

    const/4 v13, 0x1

    iput-boolean v13, v0, Lcom/starmicronics/stario/e;->o:Z

    return-object v5

    :cond_f
    const/4 v13, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    iput-boolean v13, v0, Lcom/starmicronics/stario/e;->o:Z

    return-object v5

    :cond_10
    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_16

    iput-boolean v13, v0, Lcom/starmicronics/stario/e;->o:Z

    return-object v5

    :cond_11
    const/4 v3, 0x0

    const/4 v10, 0x3

    const/4 v13, 0x1

    invoke-virtual {v5, v3}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object v8

    iget-object v14, v0, Lcom/starmicronics/stario/e;->l:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v14, v5}, Landroid/hardware/usb/UsbManager;->openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v14

    invoke-virtual {v14, v8, v13}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    const/16 v13, 0x101

    new-array v3, v13, [B

    const/16 v17, 0xc0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v22, 0x101

    const/16 v23, 0xc8

    move-object/from16 v16, v14

    move-object/from16 v21, v3

    invoke-virtual/range {v16 .. v23}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result v7

    invoke-virtual {v14, v8}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z

    invoke-virtual {v14}, Landroid/hardware/usb/UsbDeviceConnection;->close()V

    move-object v14, v12

    const/4 v8, 0x2

    :goto_6
    if-ge v8, v7, :cond_12

    aget-byte v10, v3, v8

    int-to-char v10, v10

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    add-int/lit8 v8, v8, 0x1

    const/4 v10, 0x3

    const/16 v13, 0x101

    goto :goto_6

    :cond_12
    invoke-static {}, Lcom/starmicronics/stario/e;->e()Ljava/util/HashMap;

    move-result-object v3

    invoke-static {v5}, Lcom/starmicronics/stario/e;->b(Landroid/hardware/usb/UsbDevice;)I

    move-result v7

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_7
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_14

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v3, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/HashMap;

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    if-ne v7, v13, :cond_13

    invoke-virtual {v10, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    if-eqz v13, :cond_13

    invoke-virtual {v10, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/CharSequence;

    invoke-virtual {v1, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_13

    const/4 v10, 0x1

    iput-boolean v10, v0, Lcom/starmicronics/stario/e;->o:Z

    return-object v5

    :cond_13
    const/4 v10, 0x1

    goto :goto_7

    :cond_14
    const/4 v10, 0x1

    invoke-virtual {v1, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_15

    invoke-virtual {v14, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-ne v3, v10, :cond_15

    iput-boolean v10, v0, Lcom/starmicronics/stario/e;->o:Z

    return-object v5

    :cond_15
    :try_start_1
    invoke-static {v5}, Lcom/starmicronics/stario/e;->b(Landroid/hardware/usb/UsbDevice;)I

    move-result v3

    invoke-static {}, Lcom/starmicronics/stario/e;->e()Ljava/util/HashMap;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/HashMap;

    invoke-virtual {v7, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-ne v3, v7, :cond_16

    const/4 v3, 0x1

    :try_start_2
    iput-boolean v3, v0, Lcom/starmicronics/stario/e;->o:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    return-object v5

    :catch_1
    :cond_16
    :goto_8
    const/4 v3, 0x1

    :catch_2
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_5

    :cond_17
    add-int/lit8 v4, v4, 0x1

    move-object/from16 v2, p2

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_18
    const/4 v1, 0x0

    return-object v1
.end method

.method public static a(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/starmicronics/stario/PortInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-static {p0}, Lcom/starmicronics/stario/e;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_c

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object p0, Lcom/starmicronics/stario/e;->w:Landroid/content/Context;

    sget-object p0, Lcom/starmicronics/stario/e;->w:Landroid/content/Context;

    const-string v1, "usb"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/hardware/usb/UsbManager;

    invoke-virtual {p0}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    invoke-virtual {p0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x18

    const-string v3, "USB:"

    const-string v4, "USB:SN:"

    const/16 v5, 0x519

    const-string v6, " SN:"

    const-string v7, ""

    if-lt v1, v2, :cond_4

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getInterfaceCount()I

    move-result v2

    if-nez v2, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v2

    if-eq v2, v5, :cond_2

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    new-instance v2, Lcom/starmicronics/stario/PortInfo;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getProductName()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v8, v7, v9, v1}, Lcom/starmicronics/stario/PortInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/starmicronics/stario/e;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v8, Lcom/starmicronics/stario/PortInfo;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getProductName()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v8, v2, v7, v9, v1}, Lcom/starmicronics/stario/PortInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_4
    invoke-static {}, Lcom/starmicronics/stario/e;->e()Ljava/util/HashMap;

    move-result-object v1

    :cond_5
    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v2}, Landroid/hardware/usb/UsbDevice;->getInterfaceCount()I

    move-result v8

    if-nez v8, :cond_6

    goto :goto_1

    :cond_6
    invoke-virtual {v2}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v8

    if-eq v8, v5, :cond_7

    goto :goto_1

    :cond_7
    invoke-static {v2}, Lcom/starmicronics/stario/e;->b(Landroid/hardware/usb/UsbDevice;)I

    move-result v2

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_8
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v1, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/HashMap;

    const-string v10, "devnum"

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    if-ne v2, v10, :cond_8

    const-string v10, "serial"

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    move-result v11

    const-string v12, "product"

    if-nez v11, :cond_9

    new-instance v11, Lcom/starmicronics/stario/PortInfo;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v9, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v11, v13, v7, v12, v9}, Lcom/starmicronics/stario/PortInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_9
    new-instance v11, Lcom/starmicronics/stario/PortInfo;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, "USBPort"

    invoke-virtual {v9, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v9, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v11, v13, v7, v12, v9}, Lcom/starmicronics/stario/PortInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_3
    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_a
    return-object v0

    :cond_b
    new-instance p0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Cannot find printer"

    invoke-direct {p0, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_c
    new-instance p0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "USB serial number is duplicated."

    invoke-direct {p0, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private a(Landroid/hardware/usb/UsbDevice;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    iget-object v0, p0, Lcom/starmicronics/stario/e;->l:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v0, p1}, Landroid/hardware/usb/UsbManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_5

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/starmicronics/stario/e;->m:Z

    iput-boolean v0, p0, Lcom/starmicronics/stario/e;->n:Z

    sget-object v2, Lcom/starmicronics/stario/e;->e:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.StarMicronics.StarIO.USB_PERMISSION"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v0, v3, v0}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/starmicronics/stario/e;->e:Landroid/content/Context;

    iget-object v4, p0, Lcom/starmicronics/stario/e;->v:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/starmicronics/stario/e;->l:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v2, p1, v0}, Landroid/hardware/usb/UsbManager;->requestPermission(Landroid/hardware/usb/UsbDevice;Landroid/app/PendingIntent;)V

    iget-object v0, p0, Lcom/starmicronics/stario/e;->l:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v0, p1}, Landroid/hardware/usb/UsbManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z

    move-result v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    :cond_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/starmicronics/stario/e;->l:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v0, p1}, Landroid/hardware/usb/UsbManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z

    move-result v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v2

    iget v6, p0, Lcom/starmicronics/stario/e;->c:I

    int-to-long v6, v6

    cmp-long v8, v4, v6

    if-gtz v8, :cond_1

    if-ne v0, v1, :cond_0

    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/16 v2, 0xe

    iget v3, p0, Lcom/starmicronics/stario/e;->c:I

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    :cond_2
    iget-boolean v2, p0, Lcom/starmicronics/stario/e;->m:Z

    const-string v3, "Firmware check firmware"

    const-wide/16 v4, 0x64

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/starmicronics/stario/e;->l:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v2, p1}, Landroid/hardware/usb/UsbManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z

    move-result v2

    if-nez v2, :cond_3

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    :catch_0
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {p1, v3}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_0
    iget-boolean v0, p0, Lcom/starmicronics/stario/e;->n:Z

    if-ne v0, v1, :cond_4

    :goto_1
    iget-object v0, p0, Lcom/starmicronics/stario/e;->l:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v0, p1}, Landroid/hardware/usb/UsbManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z

    move-result v0

    if-nez v0, :cond_4

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {p1, v3}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    iget-object v0, p0, Lcom/starmicronics/stario/e;->l:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v0, p1}, Landroid/hardware/usb/UsbManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z

    move-result p1

    return p1

    :cond_5
    return v1
.end method

.method static synthetic a(Lcom/starmicronics/stario/e;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/starmicronics/stario/e;->n:Z

    return p1
.end method

.method static a(Ljava/lang/String;)Z
    .locals 3

    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "USB:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v2, "USBVEN:"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p0

    const-string v0, "USBPRN:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_2

    return v1

    :cond_2
    const/4 p0, 0x0

    return p0
.end method

.method private b([B)B
    .locals 4

    invoke-virtual {p1}, [B->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [B

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_2

    aget-byte v2, p1, v1

    const/16 v3, 0x1b

    if-ne v2, v3, :cond_1

    add-int/lit8 v2, v1, 0x1

    aget-byte v2, p1, v2

    const/16 v3, 0x1d

    if-ne v2, v3, :cond_1

    add-int/lit8 v2, v1, 0x2

    aget-byte v2, p1, v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    move v2, v1

    :goto_1
    add-int/lit8 v3, v1, 0x8

    if-ge v2, v3, :cond_0

    aput-byte v0, p1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    move v1, v3

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    :goto_2
    if-ltz v1, :cond_4

    aget-byte v2, p1, v1

    and-int/lit16 v2, v2, 0x93

    const/16 v3, 0x12

    if-ne v2, v3, :cond_3

    aget-byte p1, p1, v1

    return p1

    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    :cond_4
    return v0
.end method

.method private static b(Landroid/hardware/usb/UsbDevice;)I
    .locals 2

    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object p0

    add-int/lit8 v1, v0, 0x3

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method static synthetic b()Landroid/content/Context;
    .locals 1

    sget-object v0, Lcom/starmicronics/stario/e;->e:Landroid/content/Context;

    return-object v0
.end method

.method private static b(Landroid/content/Context;)Z
    .locals 5

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x18

    if-lt v1, v2, :cond_2

    const-string v1, "usb"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/hardware/usb/UsbManager;

    invoke-virtual {p0}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v1

    const/16 v3, 0x519

    if-eq v1, v3, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/starmicronics/stario/e;->e()Ljava/util/HashMap;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    const-string v2, "serial"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    const/4 p0, 0x0

    const/4 v1, 0x0

    :goto_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge p0, v2, :cond_7

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_4

    :cond_4
    add-int/lit8 v2, p0, 0x1

    :goto_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_6

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v1, 0x1

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_6
    :goto_4
    add-int/lit8 p0, p0, 0x1

    goto :goto_2

    :cond_7
    return v1
.end method

.method static synthetic b(Lcom/starmicronics/stario/e;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/starmicronics/stario/e;->m:Z

    return p1
.end method

.method private b(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-nez v2, :cond_0

    return v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const-string v0, "/dev/bus/usb/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    const-string v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    aget-object v0, p0, v0

    const/4 v2, 0x1

    aget-object p0, p0, v2

    const-string v2, ""

    const-string v3, "^0+"

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    if-eqz v0, :cond_1

    if-eqz p0, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "-"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1
    return-object v1
.end method

.method private c()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    :try_start_0
    array-length v1, v0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v1}, Lcom/starmicronics/stario/e;->writePort([BII)V
    :try_end_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_3

    iget v0, p0, Lcom/starmicronics/stario/e;->c:I

    const/16 v1, 0x2710

    if-le v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/16 v0, 0x2710

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const/16 v1, 0x80

    new-array v5, v1, [B

    new-array v1, v1, [B

    const/4 v6, 0x4

    new-array v6, v6, [B

    fill-array-data v6, :array_1

    const/4 v7, 0x2

    new-array v7, v7, [B

    fill-array-data v7, :array_2

    const/4 v8, 0x0

    move-object v9, v8

    const/4 v8, 0x0

    :goto_1
    :try_start_1
    array-length v10, v5
    :try_end_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_1 .. :try_end_1} :catch_1

    if-ge v8, v10, :cond_6

    const-wide/16 v10, 0x64

    :try_start_2
    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_2 .. :try_end_2} :catch_1

    :try_start_3
    array-length v10, v1

    invoke-virtual {p0, v1, v2, v10}, Lcom/starmicronics/stario/e;->readPort([BII)I

    move-result v10

    aget-byte v11, v1, v2

    invoke-static {v11}, Lcom/starmicronics/stario/f;->a(B)I

    move-result v11

    if-ge v11, v10, :cond_2

    array-length v12, v5

    add-int v13, v8, v10

    sub-int/2addr v13, v11

    if-lt v12, v13, :cond_1

    sub-int/2addr v10, v11

    invoke-static {v1, v11, v5, v8, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v8, v10

    goto :goto_2

    :cond_1
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "FirmwareInfo data length was too long"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_2
    array-length v10, v6

    array-length v11, v7

    add-int/2addr v10, v11

    if-gt v10, v8, :cond_3

    new-array v9, v8, [B

    invoke-static {v5, v2, v9, v2, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v9, v6, v7}, Lcom/starmicronics/stario/f;->a([B[B[B)[B

    move-result-object v9

    :cond_3
    if-eqz v9, :cond_4

    goto :goto_3

    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v3

    int-to-long v12, v0

    cmp-long v14, v10, v12

    if-gtz v14, :cond_5

    goto :goto_1

    :cond_5
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    const-string v1, "There was no response of the printer within the timeout period."

    invoke-direct {v0, v1}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Interrupt occurred in retieveFwInfo"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_3 .. :try_end_3} :catch_1

    :cond_6
    :goto_3
    if-eqz v9, :cond_7

    invoke-static {v9}, Lcom/starmicronics/stario/f;->b([B)Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/starmicronics/stario/f$a;->a:Lcom/starmicronics/stario/f$a;

    invoke-virtual {v1}, Lcom/starmicronics/stario/f$a;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/starmicronics/stario/e;->s:Ljava/lang/String;

    sget-object v1, Lcom/starmicronics/stario/f$a;->b:Lcom/starmicronics/stario/f$a;

    invoke-virtual {v1}, Lcom/starmicronics/stario/f$a;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/starmicronics/stario/e;->t:Ljava/lang/String;

    return-void

    :cond_7
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Failed to parse model and version"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_3
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :array_0
    .array-data 1
        0x1bt
        0x23t
        0x2at
        0xat
        0x0t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x1bt
        0x23t
        0x2at
        0x2ct
    .end array-data

    :array_2
    .array-data 1
        0xat
        0x0t
    .end array-data
.end method

.method private static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    const-string v2, ""

    if-nez v1, :cond_0

    return-object v2

    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_1

    return-object v2

    :cond_1
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x3e8

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, p0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    const/16 p0, 0x400

    new-array v3, p0, [C

    :goto_0
    invoke-virtual {v1, v3}, Ljava/io/BufferedReader;->read([C)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2

    const/4 v5, 0x0

    invoke-static {v3, v5, v4}, Ljava/lang/String;->valueOf([CII)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-array v3, p0, [C

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_3

    move-object p0, v2

    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/io/IOException;->printStackTrace()V

    return-object v2

    :catch_1
    move-exception p0

    invoke-virtual {p0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    return-object v2
.end method

.method private d()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    iget-object v0, p0, Lcom/starmicronics/stario/e;->g:Landroid/hardware/usb/UsbDevice;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/starmicronics/stario/e;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "USB:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/starmicronics/stario/e$a;->c:Lcom/starmicronics/stario/e$a;

    :goto_0
    iput-object v0, p0, Lcom/starmicronics/stario/e;->f:Lcom/starmicronics/stario/e$a;

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/starmicronics/stario/e;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "USBVEN:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/starmicronics/stario/e$a;->b:Lcom/starmicronics/stario/e$a;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/starmicronics/stario/e;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "USBPRN:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/starmicronics/stario/e$a;->a:Lcom/starmicronics/stario/e$a;

    goto :goto_0

    :cond_3
    :goto_1
    sget-object v0, Lcom/starmicronics/stario/e;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/starmicronics/stario/e;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_18

    iget-object v0, p0, Lcom/starmicronics/stario/e;->a:Ljava/lang/String;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    add-int/2addr v0, v1

    const-string v2, ""

    iget-object v3, p0, Lcom/starmicronics/stario/e;->a:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x0

    if-le v3, v0, :cond_5

    iget-object v3, p0, Lcom/starmicronics/stario/e;->a:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-direct {p0, v0}, Lcom/starmicronics/stario/e;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :cond_4
    const/4 v3, 0x0

    :goto_2
    if-nez v3, :cond_6

    move-object v2, v0

    goto :goto_3

    :catch_0
    :cond_5
    const/4 v3, 0x1

    :cond_6
    :goto_3
    iget-object v0, p0, Lcom/starmicronics/stario/e;->b:Ljava/lang/String;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v5, "ESCPOS"

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    iput-object v5, p0, Lcom/starmicronics/stario/e;->r:Ljava/lang/String;

    goto :goto_4

    :cond_7
    const-string v0, "StarLine"

    iput-object v0, p0, Lcom/starmicronics/stario/e;->r:Ljava/lang/String;

    :goto_4
    sget-object v0, Lcom/starmicronics/stario/e;->e:Landroid/content/Context;

    const-string v5, "usb"

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/usb/UsbManager;

    iput-object v0, p0, Lcom/starmicronics/stario/e;->l:Landroid/hardware/usb/UsbManager;

    iget-object v0, p0, Lcom/starmicronics/stario/e;->l:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v5

    const-string v6, "Cannot find printer"

    if-nez v5, :cond_17

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v7

    new-array v7, v7, [Ljava/lang/String;

    invoke-interface {v5, v7}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    if-lez v3, :cond_8

    invoke-direct {p0, v3, v7, v0}, Lcom/starmicronics/stario/e;->a(I[Ljava/lang/String;Ljava/util/HashMap;)Landroid/hardware/usb/UsbDevice;

    move-result-object v0

    goto :goto_5

    :cond_8
    invoke-direct {p0, v2, v7, v0}, Lcom/starmicronics/stario/e;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/util/HashMap;)Landroid/hardware/usb/UsbDevice;

    move-result-object v0

    :goto_5
    iput-object v0, p0, Lcom/starmicronics/stario/e;->g:Landroid/hardware/usb/UsbDevice;

    iget-object v0, p0, Lcom/starmicronics/stario/e;->g:Landroid/hardware/usb/UsbDevice;

    if-eqz v0, :cond_16

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getInterfaceCount()I

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/starmicronics/stario/e;->g:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v0, v4}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/starmicronics/stario/e;->h:Landroid/hardware/usb/UsbInterface;

    iget-object v0, p0, Lcom/starmicronics/stario/e;->h:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbInterface;->getEndpointCount()I

    move-result v0

    const/4 v2, 0x0

    :goto_6
    if-ge v2, v0, :cond_b

    iget-object v3, p0, Lcom/starmicronics/stario/e;->h:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v3, v2}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;

    move-result-object v3

    invoke-virtual {v3}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v5

    const/16 v6, 0x80

    if-ne v5, v6, :cond_9

    iput-object v3, p0, Lcom/starmicronics/stario/e;->i:Landroid/hardware/usb/UsbEndpoint;

    goto :goto_7

    :cond_9
    invoke-virtual {v3}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v5

    if-nez v5, :cond_a

    iput-object v3, p0, Lcom/starmicronics/stario/e;->j:Landroid/hardware/usb/UsbEndpoint;

    :cond_a
    :goto_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    :cond_b
    iget-object v0, p0, Lcom/starmicronics/stario/e;->i:Landroid/hardware/usb/UsbEndpoint;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/starmicronics/stario/e;->j:Landroid/hardware/usb/UsbEndpoint;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/starmicronics/stario/e;->g:Landroid/hardware/usb/UsbDevice;

    invoke-direct {p0, v0}, Lcom/starmicronics/stario/e;->a(Landroid/hardware/usb/UsbDevice;)Z

    iget-object v0, p0, Lcom/starmicronics/stario/e;->l:Landroid/hardware/usb/UsbManager;

    iget-object v2, p0, Lcom/starmicronics/stario/e;->g:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v0, v2}, Landroid/hardware/usb/UsbManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/starmicronics/stario/e;->l:Landroid/hardware/usb/UsbManager;

    iget-object v2, p0, Lcom/starmicronics/stario/e;->g:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v0, v2}, Landroid/hardware/usb/UsbManager;->openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v0

    iput-object v0, p0, Lcom/starmicronics/stario/e;->k:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v0, p0, Lcom/starmicronics/stario/e;->k:Landroid/hardware/usb/UsbDeviceConnection;

    if-eqz v0, :cond_12

    iget-object v2, p0, Lcom/starmicronics/stario/e;->h:Landroid/hardware/usb/UsbInterface;

    iget-boolean v3, p0, Lcom/starmicronics/stario/e;->o:Z

    xor-int/2addr v3, v1

    invoke-virtual {v0, v2, v3}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/starmicronics/stario/e;->g:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_c

    iget-object v0, p0, Lcom/starmicronics/stario/e;->g:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v0

    const/4 v2, 0x4

    if-eq v0, v2, :cond_c

    iget-object v0, p0, Lcom/starmicronics/stario/e;->g:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v0

    const/4 v2, 0x5

    if-eq v0, v2, :cond_c

    iget-object v0, p0, Lcom/starmicronics/stario/e;->g:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v0

    const/4 v2, 0x6

    if-ne v0, v2, :cond_f

    :cond_c
    const/16 v0, 0x12

    new-array v2, v0, [B

    iget-object v5, p0, Lcom/starmicronics/stario/e;->k:Landroid/hardware/usb/UsbDeviceConnection;

    const/16 v6, 0x80

    const/4 v7, 0x6

    const/16 v8, 0x100

    const/4 v9, 0x0

    array-length v11, v2

    const/16 v12, 0xc8

    move-object v10, v2

    invoke-virtual/range {v5 .. v12}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result v3

    if-ne v3, v0, :cond_10

    const/16 v0, 0xc

    aget-byte v0, v2, v0

    const/16 v3, 0xd

    aget-byte v2, v2, v3

    shl-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    if-eq v0, v1, :cond_e

    const/16 v2, 0x100

    if-ne v0, v2, :cond_d

    goto :goto_8

    :cond_d
    const/4 v1, 0x0

    :cond_e
    :goto_8
    iput-boolean v1, p0, Lcom/starmicronics/stario/e;->u:Z

    :cond_f
    return-void

    :cond_10
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "unable to get device descriptor"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "unable to claim interface"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_12
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "unable to connect to printer"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/starmicronics/stario/e;->g:Landroid/hardware/usb/UsbDevice;

    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Permission denied"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_14
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Missing usb endpiont"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_15
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v0, v6}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_16
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Cannot find star micronics printer"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_17
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v0, v6}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_18
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "USB serial number is duplicated."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static e()Ljava/util/HashMap;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    new-instance v1, Ljava/io/File;

    const-string v2, "/sys/bus/usb/devices/"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_3

    aget-object v4, v1, v3

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "."

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, ".."

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    goto/16 :goto_1

    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "busnum"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/starmicronics/stario/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "devnum"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/starmicronics/stario/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v12, "idProduct"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/starmicronics/stario/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, "product"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/starmicronics/stario/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v16, v1

    const-string v1, "serial"

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/starmicronics/stario/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    move/from16 v17, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v18, v3

    const-string v3, "idVendor"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/starmicronics/stario/e;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v19, v0

    const-string v0, "0519"

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v8, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v5, v10, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v5, v1, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v5, v12, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v5, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v5, v14, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "parentPath"

    invoke-virtual {v5, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "USBPort"

    invoke-virtual {v5, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v1, v19

    invoke-virtual {v1, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_1
    move-object/from16 v1, v19

    goto :goto_2

    :cond_2
    :goto_1
    move-object/from16 v16, v1

    move/from16 v17, v2

    move/from16 v18, v3

    move-object v1, v0

    :goto_2
    add-int/lit8 v3, v18, 0x1

    move-object v0, v1

    move-object/from16 v1, v16

    move/from16 v2, v17

    goto/16 :goto_0

    :cond_3
    move-object v1, v0

    return-object v1
.end method


# virtual methods
.method protected a()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    iget-object v0, p0, Lcom/starmicronics/stario/e;->k:Landroid/hardware/usb/UsbDeviceConnection;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/starmicronics/stario/e;->h:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0, v1}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z

    iget-object v0, p0, Lcom/starmicronics/stario/e;->k:Landroid/hardware/usb/UsbDeviceConnection;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDeviceConnection;->close()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/starmicronics/stario/e;->g:Landroid/hardware/usb/UsbDevice;

    iput-object v0, p0, Lcom/starmicronics/stario/e;->h:Landroid/hardware/usb/UsbInterface;

    iput-object v0, p0, Lcom/starmicronics/stario/e;->k:Landroid/hardware/usb/UsbDeviceConnection;

    return-void
.end method

.method public beginCheckedBlock()Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    move-object/from16 v1, p0

    const-string v0, "StarLine"

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/starmicronics/stario/e;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v2

    iget-boolean v3, v2, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_4

    const-string v4, "Printer is offline"

    const/4 v5, 0x1

    if-eq v3, v5, :cond_14

    :try_start_1
    iget-object v3, v1, Lcom/starmicronics/stario/e;->r:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-boolean v2, v2, Lcom/starmicronics/stario/StarPrinterStatus;->etbAvailable:Z

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Checked block is not available for this printer"

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    iget-object v2, v1, Lcom/starmicronics/stario/e;->r:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/16 v3, 0x9

    const/16 v6, 0x8

    const/4 v7, 0x5

    const/16 v8, 0x1d

    const/4 v9, 0x6

    const/16 v10, 0x1b

    const/4 v11, 0x2

    const/4 v12, 0x4

    const/4 v13, 0x3

    const/4 v14, 0x0

    if-eqz v2, :cond_2

    const/16 v2, 0xa

    new-array v2, v2, [B

    aput-byte v10, v2, v14

    const/16 v15, 0x2a

    aput-byte v15, v2, v5

    const/16 v15, 0x72

    aput-byte v15, v2, v11

    const/16 v15, 0x42

    aput-byte v15, v2, v13

    aput-byte v10, v2, v12

    aput-byte v8, v2, v7

    aput-byte v13, v2, v9

    const/4 v15, 0x7

    aput-byte v12, v2, v15

    aput-byte v14, v2, v6

    aput-byte v14, v2, v3

    goto :goto_1

    :cond_2
    new-array v2, v9, [B

    aput-byte v10, v2, v14

    aput-byte v8, v2, v5

    aput-byte v13, v2, v11

    aput-byte v12, v2, v13

    aput-byte v14, v2, v12

    aput-byte v14, v2, v7

    :goto_1
    array-length v15, v2

    invoke-virtual {v1, v2, v14, v15}, Lcom/starmicronics/stario/e;->writePort([BII)V

    iget-object v15, v1, Lcom/starmicronics/stario/e;->r:Ljava/lang/String;

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    new-array v2, v12, [B

    aput-byte v10, v2, v14

    const/16 v15, 0x1e

    aput-byte v15, v2, v5

    const/16 v15, 0x45

    aput-byte v15, v2, v11

    aput-byte v14, v2, v13

    goto :goto_2

    :cond_3
    const-string v15, "ESCPOS"

    iget-object v2, v1, Lcom/starmicronics/stario/e;->r:Ljava/lang/String;

    invoke-virtual {v15, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    new-array v2, v9, [B

    aput-byte v10, v2, v14

    aput-byte v8, v2, v5

    aput-byte v13, v2, v11

    aput-byte v11, v2, v13

    aput-byte v14, v2, v12

    aput-byte v14, v2, v7

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    :goto_2
    array-length v15, v2

    invoke-virtual {v1, v2, v14, v15}, Lcom/starmicronics/stario/e;->writePort([BII)V

    iget v2, v1, Lcom/starmicronics/stario/e;->c:I

    const/16 v15, 0x2710

    if-le v2, v15, :cond_5

    iget v15, v1, Lcom/starmicronics/stario/e;->c:I

    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    iget-object v2, v1, Lcom/starmicronics/stario/e;->r:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_4

    const-string v2, "There was no response of the printer within the timeout period."

    if-eqz v0, :cond_9

    :try_start_2
    new-array v0, v5, [B

    const/16 v3, 0x17

    aput-byte v3, v0, v14

    array-length v3, v0

    invoke-virtual {v1, v0, v14, v3}, Lcom/starmicronics/stario/e;->writePort([BII)V

    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/starmicronics/stario/e;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    iget-boolean v3, v0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    if-eq v3, v5, :cond_8

    iget v3, v0, Lcom/starmicronics/stario/StarPrinterStatus;->etbCounter:I
    :try_end_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_4

    if-ne v3, v5, :cond_6

    :goto_4
    const/4 v7, 0x6

    goto/16 :goto_b

    :cond_6
    const-wide/16 v18, 0x1f4

    :try_start_3
    invoke-static/range {v18 .. v19}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_5

    :catch_0
    move-exception v0

    move-object v3, v0

    :try_start_4
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sub-long v18, v18, v16

    int-to-long v11, v15

    cmp-long v0, v18, v11

    if-gtz v0, :cond_7

    const/4 v11, 0x2

    const/4 v12, 0x4

    goto :goto_3

    :cond_7
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0, v2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v0, v4}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    new-array v0, v9, [B

    aput-byte v10, v0, v14

    aput-byte v8, v0, v5

    const/4 v11, 0x2

    aput-byte v13, v0, v11

    aput-byte v14, v0, v13

    const/4 v11, 0x4

    aput-byte v14, v0, v11

    aput-byte v14, v0, v7

    array-length v11, v0

    invoke-virtual {v1, v0, v14, v11}, Lcom/starmicronics/stario/e;->writePort([BII)V

    const/4 v0, 0x0

    :goto_6
    const/16 v11, 0x40

    new-array v11, v11, [B

    new-array v12, v13, [B

    const/16 v18, 0x10

    aput-byte v18, v12, v14

    const/16 v18, 0x4

    aput-byte v18, v12, v5

    const/16 v18, 0x2

    aput-byte v5, v12, v18

    array-length v7, v12

    invoke-virtual {v1, v12, v14, v7}, Lcom/starmicronics/stario/e;->writePort([BII)V

    const/4 v7, 0x0

    :goto_7
    if-nez v7, :cond_b

    array-length v7, v11

    invoke-direct {v1, v11, v14, v7}, Lcom/starmicronics/stario/e;->a([BII)I

    move-result v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20
    :try_end_4
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_4 .. :try_end_4} :catch_4

    sub-long v20, v20, v16

    int-to-long v8, v15

    cmp-long v22, v20, v8

    if-gtz v22, :cond_a

    const-wide/16 v8, 0xa

    :try_start_5
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_5 .. :try_end_5} :catch_4

    :catch_1
    const/16 v8, 0x1d

    const/4 v9, 0x6

    goto :goto_7

    :cond_a
    :try_start_6
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0, v2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    if-lt v7, v6, :cond_f

    invoke-direct {v1, v11}, Lcom/starmicronics/stario/e;->a([B)I

    move-result v8

    if-nez v8, :cond_f

    if-lt v7, v3, :cond_c

    invoke-direct {v1, v11}, Lcom/starmicronics/stario/e;->b([B)B

    move-result v0

    if-eqz v0, :cond_e

    goto :goto_a

    :cond_c
    const/4 v7, 0x0

    :goto_8
    if-nez v7, :cond_e

    const/16 v0, 0x40

    new-array v11, v0, [B

    array-length v0, v11

    invoke-direct {v1, v11, v14, v0}, Lcom/starmicronics/stario/e;->a([BII)I

    move-result v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8
    :try_end_6
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_6 .. :try_end_6} :catch_4

    sub-long v8, v8, v16

    int-to-long v12, v15

    cmp-long v0, v8, v12

    if-gtz v0, :cond_d

    const-wide/16 v8, 0xa

    :try_start_7
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_7 .. :try_end_7} :catch_4

    :catch_2
    const/4 v13, 0x3

    goto :goto_8

    :cond_d
    :try_start_8
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0, v2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    const/4 v0, 0x1

    :cond_f
    if-lt v7, v5, :cond_12

    if-ge v7, v6, :cond_12

    invoke-direct {v1, v11}, Lcom/starmicronics/stario/e;->b([B)B

    move-result v7

    if-eqz v7, :cond_11

    and-int/lit8 v7, v7, 0x8

    if-eq v7, v6, :cond_10

    goto :goto_9

    :cond_10
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v0, v4}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    :goto_9
    if-eqz v0, :cond_12

    :goto_a
    invoke-virtual/range {p0 .. p0}, Lcom/starmicronics/stario/e;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    goto/16 :goto_4

    :goto_b
    new-array v2, v7, [B

    aput-byte v10, v2, v14

    const/16 v8, 0x1d

    aput-byte v8, v2, v5

    const/4 v9, 0x2

    const/4 v11, 0x3

    aput-byte v11, v2, v9

    aput-byte v11, v2, v11

    const/4 v12, 0x4

    aput-byte v14, v2, v12

    const/4 v13, 0x5

    aput-byte v14, v2, v13

    array-length v3, v2

    invoke-virtual {v1, v2, v14, v3}, Lcom/starmicronics/stario/e;->writePort([BII)V

    return-object v0

    :cond_12
    const/4 v7, 0x6

    const/16 v8, 0x1d

    const/4 v9, 0x2

    const/4 v11, 0x3

    const/4 v12, 0x4

    const/4 v13, 0x5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18
    :try_end_8
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_8 .. :try_end_8} :catch_4

    sub-long v18, v18, v16

    int-to-long v5, v15

    cmp-long v20, v18, v5

    if-gtz v20, :cond_13

    const-wide/16 v5, 0xc8

    :try_start_9
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_9 .. :try_end_9} :catch_4

    :catch_3
    const/4 v5, 0x1

    const/16 v6, 0x8

    const/4 v7, 0x5

    const/4 v9, 0x6

    const/4 v13, 0x3

    goto/16 :goto_6

    :cond_13
    :try_start_a
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0, v2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_14
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v0, v4}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_a
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_a .. :try_end_a} :catch_4

    :catch_4
    move-exception v0

    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public endCheckedBlock()Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    move-object/from16 v1, p0

    const-string v0, "StarLine"

    :try_start_0
    iget-object v2, v1, Lcom/starmicronics/stario/e;->r:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/16 v3, 0x1b

    const/4 v4, 0x4

    const/4 v5, 0x3

    const/4 v6, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    if-eqz v2, :cond_0

    new-array v2, v4, [B

    aput-byte v3, v2, v8

    const/16 v9, 0x1e

    aput-byte v9, v2, v7

    const/16 v9, 0x61

    aput-byte v9, v2, v6

    aput-byte v6, v2, v5

    array-length v9, v2

    invoke-virtual {v1, v2, v8, v9}, Lcom/starmicronics/stario/e;->writePort([BII)V

    :cond_0
    const/4 v2, 0x0

    iget-object v9, v1, Lcom/starmicronics/stario/e;->r:Ljava/lang/String;

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    const/4 v10, 0x5

    const/16 v11, 0x1d

    const/4 v12, 0x6

    if-eqz v9, :cond_1

    new-array v2, v7, [B

    const/16 v9, 0x17

    aput-byte v9, v2, v8

    goto :goto_0

    :cond_1
    const-string v9, "ESCPOS"

    iget-object v13, v1, Lcom/starmicronics/stario/e;->r:Ljava/lang/String;

    invoke-virtual {v9, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    new-array v2, v12, [B

    aput-byte v3, v2, v8

    aput-byte v11, v2, v7

    aput-byte v5, v2, v6

    aput-byte v7, v2, v5

    aput-byte v8, v2, v4

    aput-byte v8, v2, v10

    :cond_2
    :goto_0
    array-length v9, v2

    invoke-virtual {v1, v2, v8, v9}, Lcom/starmicronics/stario/e;->writePort([BII)V

    new-array v2, v12, [B

    aput-byte v3, v2, v8

    aput-byte v11, v2, v7

    aput-byte v5, v2, v6

    aput-byte v4, v2, v5

    aput-byte v8, v2, v4

    aput-byte v8, v2, v10

    array-length v3, v2

    invoke-virtual {v1, v2, v8, v3}, Lcom/starmicronics/stario/e;->writePort([BII)V

    iget v2, v1, Lcom/starmicronics/stario/e;->d:I

    const/16 v3, 0x2710

    if-le v2, v3, :cond_3

    iget v3, v1, Lcom/starmicronics/stario/e;->d:I

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    iget-object v2, v1, Lcom/starmicronics/stario/e;->r:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_5

    const-string v2, "There was no response of the printer within the timeout period."

    if-eqz v0, :cond_7

    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/starmicronics/stario/e;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/starmicronics/stario/e;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    iget-boolean v4, v0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    if-ne v4, v7, :cond_4

    return-object v0

    :cond_4
    iget v4, v0, Lcom/starmicronics/stario/StarPrinterStatus;->etbCounter:I
    :try_end_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_1 .. :try_end_1} :catch_5

    if-ne v4, v6, :cond_5

    return-object v0

    :cond_5
    const-wide/16 v4, 0x1f4

    :try_start_2
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_2 .. :try_end_2} :catch_5

    goto :goto_2

    :catch_0
    move-exception v0

    move-object v4, v0

    :try_start_3
    invoke-virtual {v4}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v9

    int-to-long v11, v3

    cmp-long v0, v4, v11

    if-gtz v0, :cond_6

    goto :goto_1

    :cond_6
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0, v2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    const/4 v0, 0x0

    :goto_3
    const/16 v11, 0x40

    new-array v12, v11, [B

    new-array v13, v5, [B

    const/16 v14, 0x10

    aput-byte v14, v13, v8

    aput-byte v4, v13, v7

    aput-byte v7, v13, v6

    array-length v14, v13

    invoke-virtual {v1, v13, v8, v14}, Lcom/starmicronics/stario/e;->writePort([BII)V

    const/4 v13, 0x0

    :goto_4
    const-wide/16 v14, 0xa

    if-nez v13, :cond_9

    array-length v13, v12

    invoke-direct {v1, v12, v8, v13}, Lcom/starmicronics/stario/e;->a([BII)I

    move-result v13

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16
    :try_end_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_3 .. :try_end_3} :catch_5

    sub-long v16, v16, v9

    int-to-long v4, v3

    cmp-long v18, v16, v4

    if-gtz v18, :cond_8

    :try_start_4
    invoke-static {v14, v15}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_4 .. :try_end_4} :catch_5

    :catch_1
    const/4 v4, 0x4

    const/4 v5, 0x3

    goto :goto_4

    :cond_8
    :try_start_5
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0, v2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    const/16 v4, 0x8

    if-lt v13, v4, :cond_d

    invoke-direct {v1, v12}, Lcom/starmicronics/stario/e;->a([B)I

    move-result v5

    if-lez v5, :cond_d

    const/16 v0, 0x9

    if-lt v13, v0, :cond_a

    invoke-direct {v1, v12}, Lcom/starmicronics/stario/e;->b([B)B

    move-result v0

    if-eqz v0, :cond_c

    goto :goto_6

    :cond_a
    const/4 v13, 0x0

    :goto_5
    if-nez v13, :cond_c

    new-array v12, v11, [B

    array-length v0, v12

    invoke-direct {v1, v12, v8, v0}, Lcom/starmicronics/stario/e;->a([BII)I

    move-result v13
    :try_end_5
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_5 .. :try_end_5} :catch_5

    :try_start_6
    invoke-static {v14, v15}, Ljava/lang/Thread;->sleep(J)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_6 .. :try_end_6} :catch_5

    :catch_2
    :try_start_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v16, v16, v9

    move-object v0, v12

    int-to-long v11, v3

    cmp-long v18, v16, v11

    if-gtz v18, :cond_b

    move-object v12, v0

    const/16 v11, 0x40

    goto :goto_5

    :cond_b
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0, v2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    const/4 v0, 0x1

    :cond_d
    if-lt v13, v7, :cond_f

    if-ge v13, v4, :cond_f

    invoke-direct {v1, v12}, Lcom/starmicronics/stario/e;->b([B)B

    move-result v5

    if-eqz v5, :cond_e

    and-int/lit8 v5, v5, 0x8

    if-ne v5, v4, :cond_e

    goto :goto_6

    :cond_e
    if-eqz v0, :cond_f

    :goto_6
    invoke-virtual/range {p0 .. p0}, Lcom/starmicronics/stario/e;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    return-object v0

    :cond_f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4
    :try_end_7
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_7 .. :try_end_7} :catch_5

    sub-long/2addr v4, v9

    int-to-long v11, v3

    cmp-long v13, v4, v11

    if-gtz v13, :cond_10

    const-wide/16 v4, 0xc8

    :try_start_8
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_8 .. :try_end_8} :catch_5

    :catch_3
    const/4 v4, 0x4

    const/4 v5, 0x3

    goto/16 :goto_3

    :cond_10
    :try_start_9
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0, v2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_9
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_9 .. :try_end_9} :catch_4
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_9 .. :try_end_9} :catch_5

    :catch_4
    move-exception v0

    goto :goto_7

    :catch_5
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "can not write to printer"

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :goto_7
    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getDipSwitchInformation()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "This model is not supported this method."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFirmwareInformation()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/starmicronics/stario/e;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    iget-boolean v0, v0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-boolean v1, p0, Lcom/starmicronics/stario/e;->u:Z

    if-eqz v1, :cond_0

    const-string v1, "TSP100"

    iput-object v1, p0, Lcom/starmicronics/stario/e;->s:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, p0, Lcom/starmicronics/stario/e;->t:Ljava/lang/String;

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/starmicronics/stario/e;->c()V

    :goto_0
    iget-object v1, p0, Lcom/starmicronics/stario/e;->s:Ljava/lang/String;

    const-string v2, "ModelName"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/starmicronics/stario/e;->t:Ljava/lang/String;

    const-string v2, "FirmwareVersion"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0

    :cond_1
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "printer is offline."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public readPort([BII)I
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/starmicronics/stario/e;->d()V

    const/4 v0, 0x2

    new-array v0, v0, [B

    iget-boolean v1, p0, Lcom/starmicronics/stario/e;->o:Z

    const/4 v9, 0x0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/starmicronics/stario/e;->k:Landroid/hardware/usb/UsbDeviceConnection;

    const/16 v2, 0xc0

    const/4 v3, 0x3

    const/4 v5, 0x0

    array-length v7, v0

    iget v8, p0, Lcom/starmicronics/stario/e;->c:I

    move v4, p3

    move-object v6, v0

    invoke-virtual/range {v1 .. v8}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    aget-byte p3, v0, v9

    if-nez p3, :cond_0

    const/4 p3, 0x1

    aget-byte p3, v0, p3

    if-nez p3, :cond_0

    return v9

    :cond_0
    if-nez p2, :cond_2

    iget-object p2, p0, Lcom/starmicronics/stario/e;->k:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object p3, p0, Lcom/starmicronics/stario/e;->i:Landroid/hardware/usb/UsbEndpoint;

    array-length v0, p1

    iget v1, p0, Lcom/starmicronics/stario/e;->c:I

    invoke-virtual {p2, p3, p1, v0, v1}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result p1

    if-gez p1, :cond_1

    const/4 p1, 0x0

    :cond_1
    return p1

    :cond_2
    array-length p3, p1

    sub-int/2addr p3, p2

    new-array p3, p3, [B

    iget-object v0, p0, Lcom/starmicronics/stario/e;->k:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/starmicronics/stario/e;->i:Landroid/hardware/usb/UsbEndpoint;

    array-length v2, p3

    iget v3, p0, Lcom/starmicronics/stario/e;->c:I

    invoke-virtual {v0, v1, p3, v2, v3}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result v0

    :goto_0
    if-ge v9, v0, :cond_3

    add-int v1, p2, v9

    aget-byte v2, p3, v9

    aput-byte v2, p1, v1

    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_3
    return v0
.end method

.method public retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    move-object/from16 v0, p0

    invoke-direct/range {p0 .. p0}, Lcom/starmicronics/stario/e;->d()V

    new-instance v1, Lcom/starmicronics/stario/StarPrinterStatus;

    invoke-direct {v1}, Lcom/starmicronics/stario/StarPrinterStatus;-><init>()V

    iget-object v2, v0, Lcom/starmicronics/stario/e;->r:Ljava/lang/String;

    const-string v3, "StarLine"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const-wide/16 v3, 0xbb8

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v2, :cond_4

    const/16 v2, 0x64

    new-array v2, v2, [B

    array-length v7, v2

    invoke-virtual {v0, v2, v6, v7}, Lcom/starmicronics/stario/e;->readPort([BII)I

    iget-object v2, v1, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    iget-object v7, v1, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    array-length v7, v7

    invoke-virtual {v0, v2, v6, v7}, Lcom/starmicronics/stario/e;->readPort([BII)I

    move-result v2

    iput v2, v1, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    iget v2, v1, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    const/4 v7, 0x7

    if-lt v2, v7, :cond_0

    invoke-static {v1}, Lcom/starmicronics/stario/f;->c(Lcom/starmicronics/stario/StarPrinterStatus;)V

    return-object v1

    :cond_0
    const/4 v2, 0x4

    new-array v2, v2, [B

    fill-array-data v2, :array_0

    array-length v8, v2

    invoke-virtual {v0, v2, v6, v8}, Lcom/starmicronics/stario/e;->writePort([BII)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    :goto_0
    iget-object v2, v1, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    iget-object v10, v1, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    array-length v10, v10

    invoke-virtual {v0, v2, v6, v10}, Lcom/starmicronics/stario/e;->readPort([BII)I

    move-result v2

    iput v2, v1, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    iget v2, v1, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    if-lt v2, v7, :cond_2

    if-eqz v5, :cond_1

    const/4 v5, 0x0

    goto :goto_0

    :cond_1
    invoke-static {v1}, Lcom/starmicronics/stario/f;->c(Lcom/starmicronics/stario/StarPrinterStatus;)V

    return-object v1

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v8

    cmp-long v2, v3, v10

    if-ltz v2, :cond_3

    goto :goto_0

    :cond_3
    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "unable to read status"

    invoke-direct {v1, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    iget-object v2, v0, Lcom/starmicronics/stario/e;->r:Ljava/lang/String;

    const-string v7, "ESCPOS"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    const/4 v2, 0x6

    new-array v2, v2, [B

    fill-array-data v2, :array_1

    array-length v9, v2

    invoke-virtual {v0, v2, v6, v9}, Lcom/starmicronics/stario/e;->writePort([BII)V

    const/4 v2, 0x3

    new-array v9, v2, [B

    fill-array-data v9, :array_2

    array-length v10, v9

    invoke-virtual {v0, v9, v6, v10}, Lcom/starmicronics/stario/e;->writePort([BII)V

    :goto_1
    iget-object v9, v1, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    iget-object v10, v1, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    array-length v10, v10

    invoke-direct {v0, v9, v6, v10}, Lcom/starmicronics/stario/e;->a([BII)I

    move-result v9

    iput v9, v1, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    iget v9, v1, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    const-string v10, "There was no response of the printer within the timeout period."

    if-lt v9, v5, :cond_b

    iget-object v9, v1, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v9, v9, v6

    and-int/lit16 v9, v9, 0x93

    const/16 v13, 0x12

    if-ne v9, v13, :cond_b

    const-string v9, "Online/CashDrawer"

    invoke-static {v1, v9}, Lcom/starmicronics/stario/f;->a(Lcom/starmicronics/stario/StarPrinterStatus;Ljava/lang/String;)V

    iget-boolean v9, v1, Lcom/starmicronics/stario/StarPrinterStatus;->compulsionSwitch:Z

    iget-boolean v14, v1, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    new-array v15, v2, [B

    fill-array-data v15, :array_3

    array-length v11, v15

    invoke-virtual {v0, v15, v6, v11}, Lcom/starmicronics/stario/e;->writePort([BII)V

    :goto_2
    iget-object v11, v1, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    iget-object v12, v1, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    array-length v12, v12

    invoke-direct {v0, v11, v6, v12}, Lcom/starmicronics/stario/e;->a([BII)I

    move-result v11

    iput v11, v1, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    iget v11, v1, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    if-lt v11, v5, :cond_9

    iget-object v11, v1, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v11, v11, v6

    and-int/lit16 v11, v11, 0x93

    if-ne v11, v13, :cond_9

    const-string v11, "CoverOpen"

    invoke-static {v1, v11}, Lcom/starmicronics/stario/f;->a(Lcom/starmicronics/stario/StarPrinterStatus;Ljava/lang/String;)V

    iget-boolean v11, v1, Lcom/starmicronics/stario/StarPrinterStatus;->coverOpen:Z

    iget-boolean v12, v1, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    new-array v2, v2, [B

    fill-array-data v2, :array_4

    array-length v15, v2

    invoke-virtual {v0, v2, v6, v15}, Lcom/starmicronics/stario/e;->writePort([BII)V

    :goto_3
    iget-object v2, v1, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    iget-object v15, v1, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    array-length v15, v15

    invoke-direct {v0, v2, v6, v15}, Lcom/starmicronics/stario/e;->a([BII)I

    move-result v2

    iput v2, v1, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    iget v2, v1, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    if-lt v2, v5, :cond_7

    iget-object v2, v1, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v2, v2, v6

    and-int/lit16 v2, v2, 0x93

    if-ne v2, v13, :cond_7

    const-string v2, "PaperEmpty"

    invoke-static {v1, v2}, Lcom/starmicronics/stario/f;->a(Lcom/starmicronics/stario/StarPrinterStatus;Ljava/lang/String;)V

    iget-boolean v2, v1, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    iput-boolean v9, v1, Lcom/starmicronics/stario/StarPrinterStatus;->compulsionSwitch:Z

    iput-boolean v11, v1, Lcom/starmicronics/stario/StarPrinterStatus;->coverOpen:Z

    if-nez v14, :cond_6

    if-nez v12, :cond_6

    if-eqz v2, :cond_5

    goto :goto_4

    :cond_5
    const/4 v5, 0x0

    :cond_6
    :goto_4
    iput-boolean v5, v1, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    goto :goto_5

    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v17

    sub-long v17, v17, v7

    cmp-long v2, v3, v17

    if-ltz v2, :cond_8

    const-wide/16 v15, 0xa

    :try_start_0
    invoke-static/range {v15 .. v16}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    nop

    goto :goto_3

    :cond_8
    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v1, v10}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    sub-long/2addr v11, v7

    cmp-long v15, v3, v11

    if-ltz v15, :cond_a

    const-wide/16 v11, 0xa

    :try_start_1
    invoke-static {v11, v12}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    nop

    goto :goto_2

    :cond_a
    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v1, v10}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    sub-long/2addr v11, v7

    cmp-long v9, v3, v11

    if-ltz v9, :cond_c

    const-wide/16 v11, 0xa

    :try_start_2
    invoke-static {v11, v12}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_1

    :catch_2
    nop

    goto/16 :goto_1

    :cond_c
    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v1, v10}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_d
    :goto_5
    return-object v1

    nop

    :array_0
    .array-data 1
        0x1bt
        0x1et
        0x61t
        0x2t
    .end array-data

    :array_1
    .array-data 1
        0x1bt
        0x1dt
        0x3t
        0x4t
        0x0t
        0x0t
    .end array-data

    nop

    :array_2
    .array-data 1
        0x10t
        0x4t
        0x1t
    .end array-data

    :array_3
    .array-data 1
        0x10t
        0x4t
        0x2t
    .end array-data

    :array_4
    .array-data 1
        0x10t
        0x4t
        0x4t
    .end array-data
.end method

.method public setEndCheckedBlockTimeoutMillis(I)V
    .locals 0

    iput p1, p0, Lcom/starmicronics/stario/e;->d:I

    return-void
.end method

.method public writePort([BII)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/starmicronics/stario/e;->d()V

    new-array v0, p3, [B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length p3, v0

    move-object v2, v0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_3

    array-length v3, v2

    const v4, 0x8000

    if-ge v4, v3, :cond_0

    goto :goto_1

    :cond_0
    array-length v4, v2

    :goto_1
    iget-object v3, p0, Lcom/starmicronics/stario/e;->k:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v5, p0, Lcom/starmicronics/stario/e;->j:Landroid/hardware/usb/UsbEndpoint;

    iget v6, p0, Lcom/starmicronics/stario/e;->c:I

    invoke-virtual {v3, v5, v2, v4, v6}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result v2

    if-nez v2, :cond_1

    goto :goto_2

    :cond_1
    if-ltz v2, :cond_2

    add-int/2addr v0, v2

    array-length v2, p1

    sub-int/2addr v2, p2

    sub-int/2addr v2, v0

    new-array v2, v2, [B

    add-int v3, p2, v0

    array-length v4, v2

    invoke-static {p1, v3, v2, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    :cond_2
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "unable to claim write"

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_2
    return-void
.end method
