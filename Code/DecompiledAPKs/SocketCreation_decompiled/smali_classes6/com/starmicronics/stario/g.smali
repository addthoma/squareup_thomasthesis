.class Lcom/starmicronics/stario/g;
.super Lcom/starmicronics/stario/StarIOPort;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/stario/g$a;
    }
.end annotation


# static fields
.field private static k:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Landroid/bluetooth/BluetoothSocket;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:I

.field private f:Landroid/bluetooth/BluetoothSocket;

.field private g:Landroid/bluetooth/BluetoothDevice;

.field private h:Ljava/io/InputStream;

.field private i:Ljava/io/OutputStream;

.field private final j:Ljava/lang/String;

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/String;

.field private o:Z

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    sput-object v0, Lcom/starmicronics/stario/g;->k:Ljava/util/Vector;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const-string v0, "Firmware check firmware"

    invoke-direct {p0}, Lcom/starmicronics/stario/StarIOPort;-><init>()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;

    iput-object v1, p0, Lcom/starmicronics/stario/g;->g:Landroid/bluetooth/BluetoothDevice;

    const-string v1, "00001101-0000-1000-8000-00805F9B34FB"

    iput-object v1, p0, Lcom/starmicronics/stario/g;->j:Ljava/lang/String;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/starmicronics/stario/g;->l:Z

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/starmicronics/stario/g;->m:Z

    const-string v3, ""

    iput-object v3, p0, Lcom/starmicronics/stario/g;->n:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/starmicronics/stario/g;->o:Z

    iput v2, p0, Lcom/starmicronics/stario/g;->p:I

    iput-object p1, p0, Lcom/starmicronics/stario/g;->b:Ljava/lang/String;

    iput-object p2, p0, Lcom/starmicronics/stario/g;->c:Ljava/lang/String;

    iput p3, p0, Lcom/starmicronics/stario/g;->d:I

    iput p3, p0, Lcom/starmicronics/stario/g;->e:I

    invoke-direct {p0}, Lcom/starmicronics/stario/g;->d()V

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    iget p3, p0, Lcom/starmicronics/stario/g;->d:I

    int-to-long v3, p3

    add-long/2addr p1, v3

    iget-object p3, p0, Lcom/starmicronics/stario/g;->i:Ljava/io/OutputStream;

    invoke-static {}, Lcom/starmicronics/stario/f;->c()[B

    move-result-object v3

    invoke-static {}, Lcom/starmicronics/stario/f;->c()[B

    move-result-object v4

    array-length v4, v4

    invoke-virtual {p3, v3, v2, v4}, Ljava/io/OutputStream;->write([BII)V

    :goto_0
    const/16 p3, 0x64

    new-array v3, p3, [B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    const-wide/16 v4, 0x64

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_2

    :try_start_2
    iget-object v6, p0, Lcom/starmicronics/stario/g;->h:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->available()I

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/starmicronics/stario/g;->h:Ljava/io/InputStream;

    array-length v7, v3

    sub-int/2addr v7, v2

    invoke-virtual {v6, v3, v2, v7}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    add-int/2addr v3, v2

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    :goto_1
    if-lez v3, :cond_4

    iget-object p1, p0, Lcom/starmicronics/stario/g;->i:Ljava/io/OutputStream;

    invoke-static {}, Lcom/starmicronics/stario/f;->a()[B

    move-result-object p2

    invoke-static {}, Lcom/starmicronics/stario/f;->a()[B

    move-result-object v3

    array-length v3, v3

    invoke-virtual {p1, p2, v2, v3}, Ljava/io/OutputStream;->write([BII)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    iget v3, p0, Lcom/starmicronics/stario/g;->d:I

    int-to-long v6, v3

    add-long/2addr p1, v6

    new-array p3, p3, [B

    const/4 v3, 0x0

    const/4 v6, 0x0

    :goto_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    cmp-long v9, p1, v7

    if-lez v9, :cond_3

    iget-object v7, p0, Lcom/starmicronics/stario/g;->h:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->available()I

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/starmicronics/stario/g;->h:Ljava/io/InputStream;

    array-length v8, p3

    sub-int/2addr v8, v3

    invoke-virtual {v7, p3, v3, v8}, Ljava/io/InputStream;->read([BII)I

    move-result v7

    add-int/2addr v3, v7

    :goto_3
    if-gt v6, v3, :cond_1

    const/16 v7, 0x30

    aget-byte v8, p3, v6

    if-gt v7, v8, :cond_1

    aget-byte v7, p3, v6

    const/16 v8, 0x3f

    if-gt v7, v8, :cond_1

    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_1
    array-length v7, p3

    new-array v7, v7, [B

    sub-int v8, v3, v6

    invoke-static {p3, v6, v7, v2, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v8, 0x2

    new-array v8, v8, [B

    const/16 v9, 0xa

    aput-byte v9, v8, v2

    aput-byte v2, v8, v1

    new-array v9, v2, [B

    invoke-static {v7, v9, v8}, Lcom/starmicronics/stario/f;->a([B[B[B)[B

    move-result-object v7

    if-eqz v7, :cond_2

    new-instance p1, Ljava/lang/String;

    invoke-direct {p1, v7}, Ljava/lang/String;-><init>([B)V

    iput-object p1, p0, Lcom/starmicronics/stario/g;->n:Ljava/lang/String;

    iget-object p1, p0, Lcom/starmicronics/stario/g;->n:Ljava/lang/String;

    invoke-static {}, Lcom/starmicronics/stario/f;->b()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_2

    return-void

    :cond_2
    :try_start_3
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    :catch_0
    :try_start_4
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    invoke-virtual {p0}, Lcom/starmicronics/stario/g;->a()V

    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "Firmware check failed"

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    cmp-long p3, p1, v3

    if-ltz p3, :cond_5

    goto/16 :goto_0

    :cond_5
    new-instance p1, Ljava/util/concurrent/TimeoutException;

    invoke-direct {p1}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw p1

    :catch_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "call-version"

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "getPort: call-version timeout"

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :catch_3
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a(I)Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0}, Lcom/starmicronics/stario/g;->d()V

    const/4 v0, 0x2

    new-array v0, v0, [B

    const/16 v1, 0x1b

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    const/16 v1, 0x76

    const/4 v3, 0x1

    aput-byte v1, v0, v3

    iget-object v1, p0, Lcom/starmicronics/stario/g;->i:Ljava/io/OutputStream;

    array-length v4, v0

    invoke-virtual {v1, v0, v2, v4}, Ljava/io/OutputStream;->write([BII)V

    iget-object v0, p0, Lcom/starmicronics/stario/g;->i:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    :catch_0
    :goto_0
    new-instance v4, Lcom/starmicronics/stario/StarPrinterStatus;

    invoke-direct {v4}, Lcom/starmicronics/stario/StarPrinterStatus;-><init>()V

    iget-object v5, p0, Lcom/starmicronics/stario/g;->h:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->available()I

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/starmicronics/stario/g;->h:Ljava/io/InputStream;

    iget-object v6, v4, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    invoke-virtual {v5, v6, v2, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v5

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    if-ne v5, v3, :cond_1

    invoke-static {v4}, Lcom/starmicronics/stario/f;->b(Lcom/starmicronics/stario/StarPrinterStatus;)V

    return-object v4

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    sub-long/2addr v4, v0

    int-to-long v6, p1

    cmp-long v8, v4, v6

    if-gtz v8, :cond_2

    const-wide/16 v4, 0xc8

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :cond_2
    :try_start_2
    new-instance p1, Ljava/util/concurrent/TimeoutException;

    const-string v0, "There was no response of the printer within the timeout period."

    invoke-direct {p1, v0}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Failed to get parsed status"

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :catch_2
    move-exception p1

    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {p1}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p0

    const-string v0, "00:15:0E"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static b()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    sget-object v0, Lcom/starmicronics/stario/g;->k:Ljava/util/Vector;

    monitor-enter v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    :try_start_0
    sget-object v3, Lcom/starmicronics/stario/g;->k:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-lez v3, :cond_0

    sget-object v3, Lcom/starmicronics/stario/g;->k:Ljava/util/Vector;

    invoke-virtual {v3, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/bluetooth/BluetoothSocket;

    sget-object v4, Lcom/starmicronics/stario/g;->k:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    if-nez v2, :cond_1

    :try_start_2
    monitor-exit v0

    return-void

    :cond_1
    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Interup incomplete"

    invoke-direct {v1, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method private b(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/concurrent/TimeoutException;,
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/16 v0, 0x2710

    if-le p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/16 p1, 0x2710

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const/4 v2, 0x1

    new-array v2, v2, [B

    :goto_1
    :try_start_0
    array-length v3, v2

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v4, v3}, Lcom/starmicronics/stario/g;->readPort([BII)I

    move-result v3
    :try_end_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_1

    if-lez v3, :cond_2

    aget-byte v3, v2, v4

    const/16 v5, 0x26

    if-eq v3, v5, :cond_1

    aget-byte v3, v2, v4

    const/16 v4, 0x27

    if-ne v3, v4, :cond_2

    :cond_1
    return-void

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v0

    int-to-long v5, p1

    cmp-long v7, v3, v5

    if-gtz v7, :cond_3

    const-wide/16 v3, 0x64

    :try_start_1
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "called interrupt() during Thread.sleep()"

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    new-instance p1, Ljava/util/concurrent/TimeoutException;

    const-string v0, "There was no response of the printer within the timeout period."

    invoke-direct {p1, v0}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw p1

    :catch_1
    move-exception p1

    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {p1}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private c()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v1

    array-length v1, v1

    new-array v1, v1, [Landroid/bluetooth/BluetoothDevice;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/bluetooth/BluetoothDevice;

    const/4 v1, 0x0

    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    aget-object v2, v0, v1

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/starmicronics/stario/g;->a(Ljava/lang/String;)Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BT:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/starmicronics/stario/g;->b:Ljava/lang/String;

    return-void

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Cannot find bluetooth printer"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private d()V
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    move-object/from16 v1, p0

    iget-object v2, v1, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;

    if-eqz v2, :cond_0

    return-void

    :cond_0
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/starmicronics/stario/g;->e()V
    :try_end_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_13
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_12
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_11
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_10
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_f
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_e
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_d
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_c

    :try_start_1
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v3

    if-eqz v3, :cond_16

    iget-boolean v4, v1, Lcom/starmicronics/stario/g;->o:Z
    :try_end_1
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_1 .. :try_end_1} :catch_13
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_10
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_c

    const-string v5, "createInsecureRfcommSocketToServiceRecord"

    const-string v6, "Cannot find printer"

    const/16 v7, 0xc

    const-string v8, "00001101-0000-1000-8000-00805F9B34FB"

    const/4 v9, 0x0

    const/4 v10, 0x3

    const/4 v11, 0x1

    if-nez v4, :cond_a

    :try_start_2
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v4

    if-ne v4, v7, :cond_9

    iget-object v4, v1, Lcom/starmicronics/stario/g;->b:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-ne v4, v10, :cond_1

    invoke-direct/range {p0 .. p0}, Lcom/starmicronics/stario/g;->c()V

    :cond_1
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v7

    array-length v7, v7

    new-array v7, v7, [Landroid/bluetooth/BluetoothDevice;

    invoke-interface {v4, v7}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Landroid/bluetooth/BluetoothDevice;

    const/4 v7, 0x0

    :goto_0
    array-length v12, v4

    if-ge v7, v12, :cond_3

    aget-object v12, v4, v7

    invoke-virtual {v12}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v12

    iget-object v13, v1, Lcom/starmicronics/stario/g;->b:Ljava/lang/String;

    invoke-virtual {v13, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    aget-object v12, v4, v7

    invoke-virtual {v12}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/starmicronics/stario/g;->a(Ljava/lang/String;)Z

    move-result v12

    if-ne v12, v11, :cond_2

    aget-object v7, v4, v7

    iput-object v7, v1, Lcom/starmicronics/stario/g;->g:Landroid/bluetooth/BluetoothDevice;

    goto :goto_1

    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    iget-object v7, v1, Lcom/starmicronics/stario/g;->g:Landroid/bluetooth/BluetoothDevice;

    if-nez v7, :cond_5

    iget-object v7, v1, Lcom/starmicronics/stario/g;->b:Ljava/lang/String;

    invoke-virtual {v7, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/bluetooth/BluetoothAdapter;->checkBluetoothAddress(Ljava/lang/String;)Z

    move-result v7

    if-ne v7, v11, :cond_5

    const/4 v7, 0x0

    :goto_2
    array-length v12, v4

    if-ge v7, v12, :cond_5

    aget-object v12, v4, v7

    invoke-virtual {v12}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v12

    iget-object v13, v1, Lcom/starmicronics/stario/g;->b:Ljava/lang/String;

    invoke-virtual {v13, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    aget-object v12, v4, v7

    invoke-virtual {v12}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/starmicronics/stario/g;->a(Ljava/lang/String;)Z

    move-result v12

    if-ne v12, v11, :cond_4

    aget-object v4, v4, v7

    iput-object v4, v1, Lcom/starmicronics/stario/g;->g:Landroid/bluetooth/BluetoothDevice;

    goto :goto_3

    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_5
    :goto_3
    iget-object v4, v1, Lcom/starmicronics/stario/g;->g:Landroid/bluetooth/BluetoothDevice;

    if-eqz v4, :cond_8

    iget-boolean v4, v1, Lcom/starmicronics/stario/g;->l:Z

    if-eqz v4, :cond_6

    iget-object v4, v1, Lcom/starmicronics/stario/g;->g:Landroid/bluetooth/BluetoothDevice;

    invoke-static {v8}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/bluetooth/BluetoothDevice;->createRfcommSocketToServiceRecord(Ljava/util/UUID;)Landroid/bluetooth/BluetoothSocket;

    move-result-object v4

    :goto_4
    iput-object v4, v1, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;

    goto :goto_5

    :cond_6
    iget-object v4, v1, Lcom/starmicronics/stario/g;->g:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    new-array v6, v11, [Ljava/lang/Class;

    const-class v7, Ljava/util/UUID;

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    iget-object v5, v1, Lcom/starmicronics/stario/g;->g:Landroid/bluetooth/BluetoothDevice;

    new-array v6, v11, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/bluetooth/BluetoothSocket;

    goto :goto_4

    :goto_5
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    move-result v4

    if-ne v4, v11, :cond_7

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    :cond_7
    iget-object v3, v1, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothSocket;->connect()V

    iget-object v3, v1, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    iput-object v3, v1, Lcom/starmicronics/stario/g;->i:Ljava/io/OutputStream;

    iget-object v3, v1, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    iput-object v3, v1, Lcom/starmicronics/stario/g;->h:Ljava/io/InputStream;

    goto/16 :goto_e

    :cond_8
    new-instance v3, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v3, v6}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_9
    new-instance v3, Lcom/starmicronics/stario/StarIOPortException;

    const-string v4, "bluetooth adapter is off"

    invoke-direct {v3, v4}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12
    :try_end_2
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_2 .. :try_end_2} :catch_13
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_b
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_10
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_8
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_d
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_c

    :goto_6
    :try_start_3
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v4

    if-ne v4, v7, :cond_14

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_13

    iget-object v4, v1, Lcom/starmicronics/stario/g;->b:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-ne v4, v10, :cond_b

    invoke-direct/range {p0 .. p0}, Lcom/starmicronics/stario/g;->c()V

    :cond_b
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v14

    array-length v14, v14

    new-array v14, v14, [Landroid/bluetooth/BluetoothDevice;

    invoke-interface {v4, v14}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Landroid/bluetooth/BluetoothDevice;

    const/4 v14, 0x0

    :goto_7
    array-length v15, v4

    if-ge v14, v15, :cond_d

    aget-object v15, v4, v14

    invoke-virtual {v15}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v15

    iget-object v7, v1, Lcom/starmicronics/stario/g;->b:Ljava/lang/String;

    invoke-virtual {v7, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v15, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    aget-object v7, v4, v14

    invoke-virtual {v7}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/starmicronics/stario/g;->a(Ljava/lang/String;)Z

    move-result v7

    if-ne v7, v11, :cond_c

    aget-object v7, v4, v14

    goto :goto_8

    :cond_c
    add-int/lit8 v14, v14, 0x1

    const/16 v7, 0xc

    goto :goto_7

    :cond_d
    const/4 v7, 0x0

    :goto_8
    if-nez v7, :cond_f

    iget-object v14, v1, Lcom/starmicronics/stario/g;->b:Ljava/lang/String;

    invoke-virtual {v14, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Landroid/bluetooth/BluetoothAdapter;->checkBluetoothAddress(Ljava/lang/String;)Z

    move-result v14

    if-ne v14, v11, :cond_f

    const/4 v14, 0x0

    :goto_9
    array-length v15, v4

    if-ge v14, v15, :cond_f

    aget-object v15, v4, v14

    invoke-virtual {v15}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v15

    iget-object v2, v1, Lcom/starmicronics/stario/g;->b:Ljava/lang/String;

    invoke-virtual {v2, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    aget-object v2, v4, v14

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/starmicronics/stario/g;->a(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v11, :cond_e

    aget-object v7, v4, v14

    goto :goto_a

    :cond_e
    add-int/lit8 v14, v14, 0x1

    goto :goto_9

    :cond_f
    :goto_a
    if-eqz v7, :cond_12

    iget-boolean v2, v1, Lcom/starmicronics/stario/g;->l:Z

    if-eqz v2, :cond_10

    invoke-static {v8}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v7, v2}, Landroid/bluetooth/BluetoothDevice;->createRfcommSocketToServiceRecord(Ljava/util/UUID;)Landroid/bluetooth/BluetoothSocket;

    move-result-object v2

    :goto_b
    iput-object v2, v1, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;

    goto :goto_c

    :cond_10
    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    new-array v4, v11, [Ljava/lang/Class;

    const-class v14, Ljava/util/UUID;

    aput-object v14, v4, v9

    invoke-virtual {v2, v5, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    new-array v4, v11, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v14

    aput-object v14, v4, v9

    invoke-virtual {v2, v7, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothSocket;

    goto :goto_b

    :goto_c
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    move-result v2

    if-ne v2, v11, :cond_11

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    :cond_11
    iget-object v2, v1, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->connect()V

    iget-object v2, v1, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    iput-object v2, v1, Lcom/starmicronics/stario/g;->i:Ljava/io/OutputStream;

    iget-object v2, v1, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    iput-object v2, v1, Lcom/starmicronics/stario/g;->h:Ljava/io/InputStream;

    goto/16 :goto_e

    :cond_12
    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v2, v6}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_13
    new-instance v2, Lcom/starmicronics/stario/g$a;

    const-string v4, "Adapter failed: code 002"

    invoke-direct {v2, v1, v4}, Lcom/starmicronics/stario/g$a;-><init>(Lcom/starmicronics/stario/g;Ljava/lang/String;)V

    throw v2

    :cond_14
    new-instance v2, Lcom/starmicronics/stario/g$a;

    const-string v4, "Adapter failed: code 001"

    invoke-direct {v2, v1, v4}, Lcom/starmicronics/stario/g$a;-><init>(Lcom/starmicronics/stario/g;Ljava/lang/String;)V

    throw v2
    :try_end_3
    .catch Lcom/starmicronics/stario/g$a; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_3 .. :try_end_3} :catch_13
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_a
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_10
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_9
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_d
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_c

    :catch_0
    move-exception v0

    move-object v2, v0

    :try_start_4
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    iget-object v7, v1, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v7}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_4
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_4 .. :try_end_4} :catch_13
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_b
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_a
    .catch Ljava/lang/NoSuchMethodException; {:try_start_4 .. :try_end_4} :catch_10
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_9
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_4} :catch_8
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_4 .. :try_end_4} :catch_d
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_c

    const/4 v7, 0x0

    :try_start_5
    iput-object v7, v1, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;

    iput-object v7, v1, Lcom/starmicronics/stario/g;->i:Ljava/io/OutputStream;

    iput-object v7, v1, Lcom/starmicronics/stario/g;->h:Ljava/io/InputStream;
    :try_end_5
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_5 .. :try_end_5} :catch_13
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6
    .catch Ljava/lang/SecurityException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/lang/NoSuchMethodException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_c

    goto :goto_d

    :catch_1
    move-exception v0

    move-object v2, v0

    move-object v3, v7

    goto/16 :goto_f

    :catch_2
    move-exception v0

    move-object v2, v0

    move-object v3, v7

    goto/16 :goto_10

    :catch_3
    move-exception v0

    move-object v2, v0

    move-object v3, v7

    goto/16 :goto_11

    :catch_4
    move-object v3, v7

    goto/16 :goto_12

    :catch_5
    move-exception v0

    move-object v2, v0

    move-object v3, v7

    goto/16 :goto_13

    :catch_6
    move-exception v0

    move-object v2, v0

    move-object v3, v7

    goto/16 :goto_14

    :catch_7
    move-exception v0

    move-object v2, v0

    :try_start_6
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/starmicronics/stario/g$a;->getMessage()Ljava/lang/String;

    move-result-object v2

    :goto_d
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    sub-long/2addr v14, v12

    iget v7, v1, Lcom/starmicronics/stario/g;->p:I

    int-to-long v9, v7

    cmp-long v7, v14, v9

    if-gtz v7, :cond_15

    const-wide/16 v9, 0x1f4

    invoke-static {v9, v10}, Ljava/lang/Thread;->sleep(J)V

    const/16 v7, 0xc

    const/4 v9, 0x0

    const/4 v10, 0x3

    goto/16 :goto_6

    :cond_15
    new-instance v3, Lcom/starmicronics/stario/StarIOPortException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to connect port within the timeout period."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " : "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_16
    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    const-string v3, "No bluetooth adapter found"

    invoke-direct {v2, v3}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_6
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_6 .. :try_end_6} :catch_13
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_b
    .catch Ljava/lang/SecurityException; {:try_start_6 .. :try_end_6} :catch_a
    .catch Ljava/lang/NoSuchMethodException; {:try_start_6 .. :try_end_6} :catch_10
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_9
    .catch Ljava/lang/IllegalAccessException; {:try_start_6 .. :try_end_6} :catch_8
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_6 .. :try_end_6} :catch_d
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_c

    :catch_8
    move-exception v0

    move-object v2, v0

    const/4 v3, 0x0

    goto :goto_10

    :catch_9
    move-exception v0

    move-object v2, v0

    const/4 v3, 0x0

    goto :goto_11

    :catch_a
    move-exception v0

    move-object v2, v0

    const/4 v3, 0x0

    goto :goto_13

    :catch_b
    move-exception v0

    move-object v2, v0

    const/4 v3, 0x0

    goto :goto_14

    :catch_c
    :goto_e
    return-void

    :catch_d
    move-exception v0

    move-object v2, v0

    const/4 v3, 0x0

    :goto_f
    iput-object v3, v1, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;

    new-instance v3, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v3

    :catch_e
    move-exception v0

    const/4 v3, 0x0

    move-object v2, v0

    :goto_10
    iput-object v3, v1, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;

    new-instance v3, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v3

    :catch_f
    move-exception v0

    const/4 v3, 0x0

    move-object v2, v0

    :goto_11
    iput-object v3, v1, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;

    new-instance v3, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v3

    :catch_10
    const/4 v3, 0x0

    :goto_12
    iput-object v3, v1, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;

    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    const-string v3, "Need android version 3.1 to use unsecure method"

    invoke-direct {v2, v3}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2

    :catch_11
    move-exception v0

    const/4 v3, 0x0

    move-object v2, v0

    :goto_13
    iput-object v3, v1, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;

    new-instance v3, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v2}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v3

    :catch_12
    move-exception v0

    const/4 v3, 0x0

    move-object v2, v0

    :goto_14
    iput-object v3, v1, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;

    new-instance v3, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v3

    :catch_13
    move-exception v0

    move-object v2, v0

    throw v2
.end method

.method private e()V
    .locals 9

    iget-object v0, p0, Lcom/starmicronics/stario/g;->c:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "MINI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "u"

    const-string v2, "f"

    const-string v3, ";"

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/starmicronics/stario/g;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v5, :cond_4

    iget-object v0, p0, Lcom/starmicronics/stario/g;->c:Ljava/lang/String;

    const/16 v3, 0x3b

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/2addr v0, v5

    iget-object v3, p0, Lcom/starmicronics/stario/g;->c:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-ne v2, v5, :cond_0

    iput-boolean v5, p0, Lcom/starmicronics/stario/g;->a:Z

    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v5, :cond_4

    iput-boolean v4, p0, Lcom/starmicronics/stario/g;->l:Z

    goto :goto_2

    :cond_1
    iget-object v0, p0, Lcom/starmicronics/stario/g;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v3, v0

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v3, :cond_4

    aget-object v7, v0, v6

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-ne v8, v5, :cond_3

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-ne v8, v5, :cond_2

    iput-boolean v5, p0, Lcom/starmicronics/stario/g;->a:Z

    goto :goto_1

    :cond_2
    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-ne v7, v5, :cond_3

    iput-boolean v4, p0, Lcom/starmicronics/stario/g;->l:Z

    :cond_3
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_4
    :goto_2
    iget v0, p0, Lcom/starmicronics/stario/g;->d:I

    const/16 v1, 0x2710

    if-le v0, v1, :cond_5

    goto :goto_3

    :cond_5
    const/16 v0, 0x2710

    :goto_3
    iget-object v1, p0, Lcom/starmicronics/stario/g;->c:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/starmicronics/stario/f;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/starmicronics/stario/g;->p:I

    iget v0, p0, Lcom/starmicronics/stario/g;->p:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_6

    const/4 v4, 0x1

    :cond_6
    iput-boolean v4, p0, Lcom/starmicronics/stario/g;->o:Z

    return-void
.end method

.method private f()Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0}, Lcom/starmicronics/stario/g;->d()V

    const/4 v0, 0x3

    new-array v0, v0, [B

    const/16 v1, 0x10

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    const/4 v1, 0x4

    const/4 v3, 0x1

    aput-byte v1, v0, v3

    const/4 v4, 0x2

    aput-byte v1, v0, v4

    iget-object v1, p0, Lcom/starmicronics/stario/g;->i:Ljava/io/OutputStream;

    array-length v4, v0

    invoke-virtual {v1, v0, v2, v4}, Ljava/io/OutputStream;->write([BII)V

    iget-object v0, p0, Lcom/starmicronics/stario/g;->i:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x5

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/starmicronics/stario/g;->h:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->available()I

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    const-string v4, "Firmware check firmware"

    const-wide/16 v5, 0xc8

    if-nez v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :try_start_1
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :catch_0
    :try_start_2
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v0, v4}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v1, Lcom/starmicronics/stario/StarPrinterStatus;

    invoke-direct {v1}, Lcom/starmicronics/stario/StarPrinterStatus;-><init>()V

    iget-object v7, p0, Lcom/starmicronics/stario/g;->h:Ljava/io/InputStream;

    iget-object v8, v1, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    invoke-virtual {v7, v8, v2, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v7
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    if-eq v7, v3, :cond_1

    add-int/lit8 v0, v0, 0x1

    :try_start_3
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    :catch_1
    :try_start_4
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v0, v4}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {v1}, Lcom/starmicronics/stario/f;->b(Lcom/starmicronics/stario/StarPrinterStatus;)V

    return-object v1

    :cond_2
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Failed to get parsed status"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "failed to getStatus"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method protected a()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    const/16 v2, 0x13

    const/4 v3, 0x1

    if-ne v1, v2, :cond_0

    :try_start_1
    const-class v1, Landroid/bluetooth/BluetoothSocket;

    const-string v2, "mPfd"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    iget-object v2, p0, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/ParcelFileDescriptor;
    :try_end_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    :try_start_2
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V

    iget-object v1, p0, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    const/4 v3, 0x0

    goto :goto_0

    :catch_1
    nop

    :cond_0
    :goto_0
    if-eqz v3, :cond_1

    :try_start_3
    iget-object v1, p0, Lcom/starmicronics/stario/g;->i:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    iget-object v1, p0, Lcom/starmicronics/stario/g;->h:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    iget-object v1, p0, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_1
    const-wide/16 v1, 0x1f4

    :try_start_4
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    iput-object v0, p0, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    return-void

    :catch_3
    move-exception v1

    iput-object v0, p0, Lcom/starmicronics/stario/g;->f:Landroid/bluetooth/BluetoothSocket;

    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public beginCheckedBlock()Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/starmicronics/stario/g;->f()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    iget-boolean v1, v0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    if-nez v1, :cond_4

    invoke-virtual {p0}, Lcom/starmicronics/stario/g;->getFirmwareInformation()Ljava/util/Map;

    move-result-object v1

    const-string v2, "ModelName"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "FirmwareVersion"

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/starmicronics/stario/g;->m:Z

    const v4, 0x4019999a    # 2.4f

    const/4 v5, 0x1

    cmpl-float v4, v1, v4

    if-ltz v4, :cond_0

    :goto_0
    iput-boolean v5, p0, Lcom/starmicronics/stario/g;->m:Z

    goto :goto_1

    :cond_0
    const-string v4, "S220I"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    const v4, 0x40066666    # 2.1f

    cmpl-float v1, v1, v4

    if-ltz v1, :cond_1

    goto :goto_0

    :cond_1
    const-string v1, "S230I"

    invoke-virtual {v2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_0

    :cond_2
    :goto_1
    iget-boolean v1, p0, Lcom/starmicronics/stario/g;->m:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x6

    new-array v2, v1, [B

    fill-array-data v2, :array_0

    new-array v1, v1, [B

    fill-array-data v1, :array_1

    array-length v4, v2

    array-length v5, v1

    add-int/2addr v4, v5

    new-array v4, v4, [B

    array-length v5, v2

    invoke-static {v2, v3, v4, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v5, v2

    array-length v6, v1

    invoke-static {v1, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v1, v4

    invoke-virtual {p0, v4, v3, v1}, Lcom/starmicronics/stario/g;->writePort([BII)V

    :try_start_0
    iget v1, p0, Lcom/starmicronics/stario/g;->d:I

    invoke-direct {p0, v1}, Lcom/starmicronics/stario/g;->b(I)V
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_0

    array-length v1, v2

    invoke-virtual {p0, v2, v3, v1}, Lcom/starmicronics/stario/g;->writePort([BII)V

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_1
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Printer does not respond."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    return-object v0

    :cond_4
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Printer is offline"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :array_0
    .array-data 1
        0x1bt
        0x1ct
        0x15t
        0x5t
        0x0t
        0x0t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x1bt
        0x1ct
        0x15t
        0x6t
        0x0t
        0x0t
    .end array-data
.end method

.method public endCheckedBlock()Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    iget-boolean v0, p0, Lcom/starmicronics/stario/g;->m:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    const/4 v1, 0x0

    :try_start_0
    array-length v2, v0

    invoke-virtual {p0, v0, v1, v2}, Lcom/starmicronics/stario/g;->writePort([BII)V

    iget v0, p0, Lcom/starmicronics/stario/g;->e:I

    invoke-direct {p0, v0}, Lcom/starmicronics/stario/g;->b(I)V
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-direct {p0}, Lcom/starmicronics/stario/g;->f()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    :try_start_1
    iget v0, p0, Lcom/starmicronics/stario/g;->e:I

    const/16 v1, 0x2710

    if-le v0, v1, :cond_1

    iget v1, p0, Lcom/starmicronics/stario/g;->e:I

    :cond_1
    invoke-direct {p0, v1}, Lcom/starmicronics/stario/g;->a(I)Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0
    :try_end_1
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_1 .. :try_end_1} :catch_2

    return-object v0

    :catch_2
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    nop

    :array_0
    .array-data 1
        0x1bt
        0x1ct
        0x15t
        0x6t
        0x0t
        0x0t
    .end array-data
.end method

.method public getDipSwitchInformation()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "This model is not supported this method."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFirmwareInformation()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    iget-object v0, p0, Lcom/starmicronics/stario/g;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/starmicronics/stario/f;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public readPort([BII)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0}, Lcom/starmicronics/stario/g;->d()V

    iget-object v0, p0, Lcom/starmicronics/stario/g;->h:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lcom/starmicronics/stario/g;->h:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p2, -0x1

    if-ne p1, p2, :cond_1

    const/4 p1, 0x0

    :cond_1
    return p1

    :catch_0
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "Failed to read"

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/starmicronics/stario/g;->f()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    return-object v0
.end method

.method public setEndCheckedBlockTimeoutMillis(I)V
    .locals 0

    iput p1, p0, Lcom/starmicronics/stario/g;->e:I

    return-void
.end method

.method public writePort([BII)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0}, Lcom/starmicronics/stario/g;->d()V

    const/4 v0, 0x0

    const/16 v1, 0x400

    if-ge v1, p3, :cond_1

    const/16 v2, 0x400

    :goto_0
    if-ge v0, p3, :cond_2

    iget-object v3, p0, Lcom/starmicronics/stario/g;->i:Ljava/io/OutputStream;

    invoke-virtual {v3, p1, p2, v2}, Ljava/io/OutputStream;->write([BII)V

    add-int p2, v0, v2

    sub-int v0, p3, p2

    if-ge v0, v1, :cond_0

    move v2, v0

    :cond_0
    move v0, p2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/starmicronics/stario/g;->i:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    :cond_2
    iget-boolean p1, p0, Lcom/starmicronics/stario/g;->a:Z

    const/4 p2, 0x1

    if-ne p1, p2, :cond_3

    iget-object p1, p0, Lcom/starmicronics/stario/g;->i:Ljava/io/OutputStream;

    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    return-void

    :catch_0
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "failed to write"

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
