.class final Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1;
.super Lrx/subjects/Subject;
.source "ProcessorV2ToSubjectV1.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lrx/subjects/Subject<",
        "TT;TT;>;"
    }
.end annotation


# instance fields
.field final state:Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State<",
            "TT;>;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0, p1}, Lrx/subjects/Subject;-><init>(Lrx/Observable$OnSubscribe;)V

    .line 35
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1;->state:Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;

    return-void
.end method

.method static createWith(Lio/reactivex/processors/FlowableProcessor;)Lrx/subjects/Subject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/processors/FlowableProcessor<",
            "TT;>;)",
            "Lrx/subjects/Subject<",
            "TT;TT;>;"
        }
    .end annotation

    .line 27
    new-instance v0, Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;-><init>(Lio/reactivex/processors/FlowableProcessor;)V

    .line 28
    new-instance p0, Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1;

    invoke-direct {p0, v0}, Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1;-><init>(Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;)V

    return-object p0
.end method


# virtual methods
.method public hasObservers()Z
    .locals 1

    .line 55
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1;->state:Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;

    invoke-virtual {v0}, Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;->hasObservers()Z

    move-result v0

    return v0
.end method

.method public onCompleted()V
    .locals 1

    .line 50
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1;->state:Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;

    invoke-virtual {v0}, Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;->onCompleted()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 45
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1;->state:Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;

    invoke-virtual {v0, p1}, Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1;->state:Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;

    invoke-virtual {v0, p1}, Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;->onNext(Ljava/lang/Object;)V

    return-void
.end method
