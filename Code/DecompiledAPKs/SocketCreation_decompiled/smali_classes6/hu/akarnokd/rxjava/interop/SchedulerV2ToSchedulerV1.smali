.class final Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1;
.super Lrx/Scheduler;
.source "SchedulerV2ToSchedulerV1.java"

# interfaces
.implements Lrx/internal/schedulers/SchedulerLifecycle;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1$Action0V1ToRunnable;,
        Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1$WorkerV2ToWorkerV1;
    }
.end annotation


# instance fields
.field final source:Lio/reactivex/Scheduler;


# direct methods
.method constructor <init>(Lio/reactivex/Scheduler;)V
    .locals 0

    .line 33
    invoke-direct {p0}, Lrx/Scheduler;-><init>()V

    .line 34
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1;->source:Lio/reactivex/Scheduler;

    return-void
.end method


# virtual methods
.method public createWorker()Lrx/Scheduler$Worker;
    .locals 2

    .line 54
    new-instance v0, Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1$WorkerV2ToWorkerV1;

    iget-object v1, p0, Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1;->source:Lio/reactivex/Scheduler;

    invoke-virtual {v1}, Lio/reactivex/Scheduler;->createWorker()Lio/reactivex/Scheduler$Worker;

    move-result-object v1

    invoke-direct {v0, v1}, Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1$WorkerV2ToWorkerV1;-><init>(Lio/reactivex/Scheduler$Worker;)V

    return-object v0
.end method

.method public now()J
    .locals 2

    .line 39
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1;->source:Lio/reactivex/Scheduler;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, Lio/reactivex/Scheduler;->now(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public shutdown()V
    .locals 1

    .line 49
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1;->source:Lio/reactivex/Scheduler;

    invoke-virtual {v0}, Lio/reactivex/Scheduler;->shutdown()V

    return-void
.end method

.method public start()V
    .locals 1

    .line 44
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1;->source:Lio/reactivex/Scheduler;

    invoke-virtual {v0}, Lio/reactivex/Scheduler;->start()V

    return-void
.end method
