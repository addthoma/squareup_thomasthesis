.class final Lhu/akarnokd/rxjava/interop/MaybeV2ToCompletableV1;
.super Ljava/lang/Object;
.source "MaybeV2ToCompletableV1.java"

# interfaces
.implements Lrx/Completable$OnSubscribe;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhu/akarnokd/rxjava/interop/MaybeV2ToCompletableV1$MaybeV2Observer;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Completable$OnSubscribe;"
    }
.end annotation


# instance fields
.field final source:Lio/reactivex/MaybeSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/MaybeSource<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex/MaybeSource;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/MaybeSource<",
            "TT;>;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/MaybeV2ToCompletableV1;->source:Lio/reactivex/MaybeSource;

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 26
    check-cast p1, Lrx/CompletableSubscriber;

    invoke-virtual {p0, p1}, Lhu/akarnokd/rxjava/interop/MaybeV2ToCompletableV1;->call(Lrx/CompletableSubscriber;)V

    return-void
.end method

.method public call(Lrx/CompletableSubscriber;)V
    .locals 1

    .line 36
    new-instance v0, Lhu/akarnokd/rxjava/interop/MaybeV2ToCompletableV1$MaybeV2Observer;

    invoke-direct {v0, p1}, Lhu/akarnokd/rxjava/interop/MaybeV2ToCompletableV1$MaybeV2Observer;-><init>(Lrx/CompletableSubscriber;)V

    .line 37
    invoke-interface {p1, v0}, Lrx/CompletableSubscriber;->onSubscribe(Lrx/Subscription;)V

    .line 38
    iget-object p1, p0, Lhu/akarnokd/rxjava/interop/MaybeV2ToCompletableV1;->source:Lio/reactivex/MaybeSource;

    invoke-interface {p1, v0}, Lio/reactivex/MaybeSource;->subscribe(Lio/reactivex/MaybeObserver;)V

    return-void
.end method
