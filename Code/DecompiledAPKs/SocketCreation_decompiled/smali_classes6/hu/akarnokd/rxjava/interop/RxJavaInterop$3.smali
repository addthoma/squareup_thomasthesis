.class final Lhu/akarnokd/rxjava/interop/RxJavaInterop$3;
.super Ljava/lang/Object;
.source "RxJavaInterop.java"

# interfaces
.implements Lio/reactivex/SingleTransformer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Transformer(Lrx/Single$Transformer;)Lio/reactivex/SingleTransformer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/SingleTransformer<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic val$transformer:Lrx/Single$Transformer;


# direct methods
.method constructor <init>(Lrx/Single$Transformer;)V
    .locals 0

    .line 251
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$3;->val$transformer:Lrx/Single$Transformer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lio/reactivex/Single;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Single<",
            "TT;>;)",
            "Lio/reactivex/Single<",
            "TR;>;"
        }
    .end annotation

    .line 254
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$3;->val$transformer:Lrx/Single$Transformer;

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    invoke-interface {v0, p1}, Lrx/Single$Transformer;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lrx/Single;

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Lio/reactivex/Single;)Lio/reactivex/SingleSource;
    .locals 0

    .line 251
    invoke-virtual {p0, p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop$3;->apply(Lio/reactivex/Single;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
