.class final Lhu/akarnokd/rxjava/interop/SubjectV2ToSubjectV1$State;
.super Ljava/lang/Object;
.source "SubjectV2ToSubjectV1.java"

# interfaces
.implements Lrx/Observable$OnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhu/akarnokd/rxjava/interop/SubjectV2ToSubjectV1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$OnSubscribe<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final subject:Lio/reactivex/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/Subject<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex/subjects/Subject;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/subjects/Subject<",
            "TT;>;)V"
        }
    .end annotation

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/SubjectV2ToSubjectV1$State;->subject:Lio/reactivex/subjects/Subject;

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 60
    check-cast p1, Lrx/Subscriber;

    invoke-virtual {p0, p1}, Lhu/akarnokd/rxjava/interop/SubjectV2ToSubjectV1$State;->call(Lrx/Subscriber;)V

    return-void
.end method

.method public call(Lrx/Subscriber;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber<",
            "-TT;>;)V"
        }
    .end annotation

    .line 71
    new-instance v0, Lhu/akarnokd/rxjava/interop/SubjectV2ToSubjectV1$SourceObserver;

    invoke-direct {v0, p1}, Lhu/akarnokd/rxjava/interop/SubjectV2ToSubjectV1$SourceObserver;-><init>(Lrx/Subscriber;)V

    .line 73
    invoke-virtual {p1, v0}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    .line 74
    invoke-virtual {p1, v0}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    .line 76
    iget-object p1, p0, Lhu/akarnokd/rxjava/interop/SubjectV2ToSubjectV1$State;->subject:Lio/reactivex/subjects/Subject;

    invoke-virtual {p1, v0}, Lio/reactivex/subjects/Subject;->subscribe(Lio/reactivex/Observer;)V

    return-void
.end method

.method hasObservers()Z
    .locals 1

    .line 92
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV2ToSubjectV1$State;->subject:Lio/reactivex/subjects/Subject;

    invoke-virtual {v0}, Lio/reactivex/subjects/Subject;->hasObservers()Z

    move-result v0

    return v0
.end method

.method onCompleted()V
    .locals 1

    .line 88
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV2ToSubjectV1$State;->subject:Lio/reactivex/subjects/Subject;

    invoke-virtual {v0}, Lio/reactivex/subjects/Subject;->onComplete()V

    return-void
.end method

.method onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 84
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV2ToSubjectV1$State;->subject:Lio/reactivex/subjects/Subject;

    invoke-virtual {v0, p1}, Lio/reactivex/subjects/Subject;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 80
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV2ToSubjectV1$State;->subject:Lio/reactivex/subjects/Subject;

    invoke-virtual {v0, p1}, Lio/reactivex/subjects/Subject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
