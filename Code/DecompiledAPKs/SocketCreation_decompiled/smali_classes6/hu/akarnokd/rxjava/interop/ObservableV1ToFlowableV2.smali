.class final Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2;
.super Lio/reactivex/Flowable;
.source "ObservableV1ToFlowableV2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriberSubscription;,
        Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriber;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/Flowable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final source:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lrx/Observable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "TT;>;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Lio/reactivex/Flowable;-><init>()V

    .line 29
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2;->source:Lrx/Observable;

    return-void
.end method


# virtual methods
.method protected subscribeActual(Lorg/reactivestreams/Subscriber;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/reactivestreams/Subscriber<",
            "-TT;>;)V"
        }
    .end annotation

    .line 34
    new-instance v0, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriber;

    invoke-direct {v0, p1}, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriber;-><init>(Lorg/reactivestreams/Subscriber;)V

    .line 35
    new-instance v1, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriberSubscription;

    invoke-direct {v1, v0}, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriberSubscription;-><init>(Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriber;)V

    .line 36
    invoke-interface {p1, v1}, Lorg/reactivestreams/Subscriber;->onSubscribe(Lorg/reactivestreams/Subscription;)V

    .line 38
    iget-object p1, p0, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2;->source:Lrx/Observable;

    invoke-virtual {p1, v0}, Lrx/Observable;->unsafeSubscribe(Lrx/Subscriber;)Lrx/Subscription;

    return-void
.end method
