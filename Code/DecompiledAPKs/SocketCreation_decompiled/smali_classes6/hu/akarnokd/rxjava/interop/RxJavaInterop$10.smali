.class final Lhu/akarnokd/rxjava/interop/RxJavaInterop$10;
.super Ljava/lang/Object;
.source "RxJavaInterop.java"

# interfaces
.implements Lrx/Observable$Operator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Operator(Lio/reactivex/FlowableOperator;)Lrx/Observable$Operator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/Observable$Operator<",
        "TR;TT;>;"
    }
.end annotation


# instance fields
.field final synthetic val$operator:Lio/reactivex/FlowableOperator;


# direct methods
.method constructor <init>(Lio/reactivex/FlowableOperator;)V
    .locals 0

    .line 617
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$10;->val$operator:Lio/reactivex/FlowableOperator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 617
    check-cast p1, Lrx/Subscriber;

    invoke-virtual {p0, p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop$10;->call(Lrx/Subscriber;)Lrx/Subscriber;

    move-result-object p1

    return-object p1
.end method

.method public call(Lrx/Subscriber;)Lrx/Subscriber;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber<",
            "-TR;>;)",
            "Lrx/Subscriber<",
            "-TT;>;"
        }
    .end annotation

    .line 620
    new-instance v0, Lhu/akarnokd/rxjava/interop/FlowableV2ToObservableV1$SourceSubscriber;

    invoke-direct {v0, p1}, Lhu/akarnokd/rxjava/interop/FlowableV2ToObservableV1$SourceSubscriber;-><init>(Lrx/Subscriber;)V

    .line 622
    invoke-virtual {p1, v0}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    .line 623
    invoke-virtual {p1, v0}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    .line 628
    :try_start_0
    iget-object v1, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$10;->val$operator:Lio/reactivex/FlowableOperator;

    invoke-interface {v1, v0}, Lio/reactivex/FlowableOperator;->apply(Lorg/reactivestreams/Subscriber;)Lorg/reactivestreams/Subscriber;

    move-result-object v0

    const-string v1, "The operator returned a null Subscriber"

    invoke-static {v0, v1}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/reactivestreams/Subscriber;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 638
    new-instance p1, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriber;

    invoke-direct {p1, v0}, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriber;-><init>(Lorg/reactivestreams/Subscriber;)V

    .line 639
    new-instance v1, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriberSubscription;

    invoke-direct {v1, p1}, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriberSubscription;-><init>(Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriber;)V

    .line 640
    invoke-interface {v0, v1}, Lorg/reactivestreams/Subscriber;->onSubscribe(Lorg/reactivestreams/Subscription;)V

    return-object p1

    :catchall_0
    move-exception v0

    .line 630
    invoke-static {v0}, Lio/reactivex/exceptions/Exceptions;->throwIfFatal(Ljava/lang/Throwable;)V

    .line 631
    invoke-static {v0}, Lrx/exceptions/Exceptions;->throwIfFatal(Ljava/lang/Throwable;)V

    .line 632
    invoke-virtual {p1, v0}, Lrx/Subscriber;->onError(Ljava/lang/Throwable;)V

    .line 633
    invoke-static {}, Lrx/observers/Subscribers;->empty()Lrx/Subscriber;

    move-result-object p1

    .line 634
    invoke-virtual {p1}, Lrx/Subscriber;->unsubscribe()V

    return-object p1
.end method
