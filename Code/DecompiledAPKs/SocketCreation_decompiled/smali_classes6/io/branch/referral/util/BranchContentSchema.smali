.class public final enum Lio/branch/referral/util/BranchContentSchema;
.super Ljava/lang/Enum;
.source "BranchContentSchema.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lio/branch/referral/util/BranchContentSchema;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lio/branch/referral/util/BranchContentSchema;

.field public static final enum COMMERCE_AUCTION:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum COMMERCE_BUSINESS:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum COMMERCE_OTHER:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum COMMERCE_PRODUCT:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum COMMERCE_RESTAURANT:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum COMMERCE_SERVICE:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum COMMERCE_TRAVEL_FLIGHT:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum COMMERCE_TRAVEL_HOTEL:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum COMMERCE_TRAVEL_OTHER:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum GAME_STATE:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum MEDIA_IMAGE:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum MEDIA_MIXED:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum MEDIA_MUSIC:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum MEDIA_OTHER:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum MEDIA_VIDEO:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum OTHER:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum TEXT_ARTICLE:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum TEXT_BLOG:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum TEXT_OTHER:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum TEXT_RECIPE:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum TEXT_REVIEW:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum TEXT_SEARCH_RESULTS:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum TEXT_STORY:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum TEXT_TECHNICAL_DOC:Lio/branch/referral/util/BranchContentSchema;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 10
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const/4 v1, 0x0

    const-string v2, "COMMERCE_AUCTION"

    invoke-direct {v0, v2, v1}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_AUCTION:Lio/branch/referral/util/BranchContentSchema;

    .line 11
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const/4 v2, 0x1

    const-string v3, "COMMERCE_BUSINESS"

    invoke-direct {v0, v3, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_BUSINESS:Lio/branch/referral/util/BranchContentSchema;

    .line 12
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const/4 v3, 0x2

    const-string v4, "COMMERCE_OTHER"

    invoke-direct {v0, v4, v3}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_OTHER:Lio/branch/referral/util/BranchContentSchema;

    .line 13
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const/4 v4, 0x3

    const-string v5, "COMMERCE_PRODUCT"

    invoke-direct {v0, v5, v4}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_PRODUCT:Lio/branch/referral/util/BranchContentSchema;

    .line 14
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const/4 v5, 0x4

    const-string v6, "COMMERCE_RESTAURANT"

    invoke-direct {v0, v6, v5}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_RESTAURANT:Lio/branch/referral/util/BranchContentSchema;

    .line 15
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const/4 v6, 0x5

    const-string v7, "COMMERCE_SERVICE"

    invoke-direct {v0, v7, v6}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_SERVICE:Lio/branch/referral/util/BranchContentSchema;

    .line 16
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const/4 v7, 0x6

    const-string v8, "COMMERCE_TRAVEL_FLIGHT"

    invoke-direct {v0, v8, v7}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_TRAVEL_FLIGHT:Lio/branch/referral/util/BranchContentSchema;

    .line 17
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const/4 v8, 0x7

    const-string v9, "COMMERCE_TRAVEL_HOTEL"

    invoke-direct {v0, v9, v8}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_TRAVEL_HOTEL:Lio/branch/referral/util/BranchContentSchema;

    .line 18
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const/16 v9, 0x8

    const-string v10, "COMMERCE_TRAVEL_OTHER"

    invoke-direct {v0, v10, v9}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_TRAVEL_OTHER:Lio/branch/referral/util/BranchContentSchema;

    .line 19
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const/16 v10, 0x9

    const-string v11, "GAME_STATE"

    invoke-direct {v0, v11, v10}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->GAME_STATE:Lio/branch/referral/util/BranchContentSchema;

    .line 20
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const/16 v11, 0xa

    const-string v12, "MEDIA_IMAGE"

    invoke-direct {v0, v12, v11}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->MEDIA_IMAGE:Lio/branch/referral/util/BranchContentSchema;

    .line 21
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const/16 v12, 0xb

    const-string v13, "MEDIA_MIXED"

    invoke-direct {v0, v13, v12}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->MEDIA_MIXED:Lio/branch/referral/util/BranchContentSchema;

    .line 22
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const/16 v13, 0xc

    const-string v14, "MEDIA_MUSIC"

    invoke-direct {v0, v14, v13}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->MEDIA_MUSIC:Lio/branch/referral/util/BranchContentSchema;

    .line 23
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const/16 v14, 0xd

    const-string v15, "MEDIA_OTHER"

    invoke-direct {v0, v15, v14}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->MEDIA_OTHER:Lio/branch/referral/util/BranchContentSchema;

    .line 24
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const/16 v15, 0xe

    const-string v14, "MEDIA_VIDEO"

    invoke-direct {v0, v14, v15}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->MEDIA_VIDEO:Lio/branch/referral/util/BranchContentSchema;

    .line 25
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const-string v14, "OTHER"

    const/16 v15, 0xf

    invoke-direct {v0, v14, v15}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->OTHER:Lio/branch/referral/util/BranchContentSchema;

    .line 26
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const-string v14, "TEXT_ARTICLE"

    const/16 v15, 0x10

    invoke-direct {v0, v14, v15}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->TEXT_ARTICLE:Lio/branch/referral/util/BranchContentSchema;

    .line 27
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const-string v14, "TEXT_BLOG"

    const/16 v15, 0x11

    invoke-direct {v0, v14, v15}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->TEXT_BLOG:Lio/branch/referral/util/BranchContentSchema;

    .line 28
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const-string v14, "TEXT_OTHER"

    const/16 v15, 0x12

    invoke-direct {v0, v14, v15}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->TEXT_OTHER:Lio/branch/referral/util/BranchContentSchema;

    .line 29
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const-string v14, "TEXT_RECIPE"

    const/16 v15, 0x13

    invoke-direct {v0, v14, v15}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->TEXT_RECIPE:Lio/branch/referral/util/BranchContentSchema;

    .line 30
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const-string v14, "TEXT_REVIEW"

    const/16 v15, 0x14

    invoke-direct {v0, v14, v15}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->TEXT_REVIEW:Lio/branch/referral/util/BranchContentSchema;

    .line 31
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const-string v14, "TEXT_SEARCH_RESULTS"

    const/16 v15, 0x15

    invoke-direct {v0, v14, v15}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->TEXT_SEARCH_RESULTS:Lio/branch/referral/util/BranchContentSchema;

    .line 32
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const-string v14, "TEXT_STORY"

    const/16 v15, 0x16

    invoke-direct {v0, v14, v15}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->TEXT_STORY:Lio/branch/referral/util/BranchContentSchema;

    .line 33
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    const-string v14, "TEXT_TECHNICAL_DOC"

    const/16 v15, 0x17

    invoke-direct {v0, v14, v15}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->TEXT_TECHNICAL_DOC:Lio/branch/referral/util/BranchContentSchema;

    const/16 v0, 0x18

    new-array v0, v0, [Lio/branch/referral/util/BranchContentSchema;

    .line 9
    sget-object v14, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_AUCTION:Lio/branch/referral/util/BranchContentSchema;

    aput-object v14, v0, v1

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_BUSINESS:Lio/branch/referral/util/BranchContentSchema;

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_OTHER:Lio/branch/referral/util/BranchContentSchema;

    aput-object v1, v0, v3

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_PRODUCT:Lio/branch/referral/util/BranchContentSchema;

    aput-object v1, v0, v4

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_RESTAURANT:Lio/branch/referral/util/BranchContentSchema;

    aput-object v1, v0, v5

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_SERVICE:Lio/branch/referral/util/BranchContentSchema;

    aput-object v1, v0, v6

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_TRAVEL_FLIGHT:Lio/branch/referral/util/BranchContentSchema;

    aput-object v1, v0, v7

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_TRAVEL_HOTEL:Lio/branch/referral/util/BranchContentSchema;

    aput-object v1, v0, v8

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_TRAVEL_OTHER:Lio/branch/referral/util/BranchContentSchema;

    aput-object v1, v0, v9

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->GAME_STATE:Lio/branch/referral/util/BranchContentSchema;

    aput-object v1, v0, v10

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->MEDIA_IMAGE:Lio/branch/referral/util/BranchContentSchema;

    aput-object v1, v0, v11

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->MEDIA_MIXED:Lio/branch/referral/util/BranchContentSchema;

    aput-object v1, v0, v12

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->MEDIA_MUSIC:Lio/branch/referral/util/BranchContentSchema;

    aput-object v1, v0, v13

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->MEDIA_OTHER:Lio/branch/referral/util/BranchContentSchema;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->MEDIA_VIDEO:Lio/branch/referral/util/BranchContentSchema;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->OTHER:Lio/branch/referral/util/BranchContentSchema;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->TEXT_ARTICLE:Lio/branch/referral/util/BranchContentSchema;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->TEXT_BLOG:Lio/branch/referral/util/BranchContentSchema;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->TEXT_OTHER:Lio/branch/referral/util/BranchContentSchema;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->TEXT_RECIPE:Lio/branch/referral/util/BranchContentSchema;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->TEXT_REVIEW:Lio/branch/referral/util/BranchContentSchema;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->TEXT_SEARCH_RESULTS:Lio/branch/referral/util/BranchContentSchema;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->TEXT_STORY:Lio/branch/referral/util/BranchContentSchema;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lio/branch/referral/util/BranchContentSchema;->TEXT_TECHNICAL_DOC:Lio/branch/referral/util/BranchContentSchema;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->$VALUES:[Lio/branch/referral/util/BranchContentSchema;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getValue(Ljava/lang/String;)Lio/branch/referral/util/BranchContentSchema;
    .locals 5

    .line 37
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 38
    invoke-static {}, Lio/branch/referral/util/BranchContentSchema;->values()[Lio/branch/referral/util/BranchContentSchema;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 39
    invoke-virtual {v3}, Lio/branch/referral/util/BranchContentSchema;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_1
    return-object v3
.end method

.method public static valueOf(Ljava/lang/String;)Lio/branch/referral/util/BranchContentSchema;
    .locals 1

    .line 9
    const-class v0, Lio/branch/referral/util/BranchContentSchema;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lio/branch/referral/util/BranchContentSchema;

    return-object p0
.end method

.method public static values()[Lio/branch/referral/util/BranchContentSchema;
    .locals 1

    .line 9
    sget-object v0, Lio/branch/referral/util/BranchContentSchema;->$VALUES:[Lio/branch/referral/util/BranchContentSchema;

    invoke-virtual {v0}, [Lio/branch/referral/util/BranchContentSchema;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/branch/referral/util/BranchContentSchema;

    return-object v0
.end method
