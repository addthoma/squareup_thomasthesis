.class public Lcom/squareup/CardBrandGuesser;
.super Ljava/lang/Object;
.source "CardBrandGuesser.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static guessBrand(Ljava/lang/CharSequence;)Lcom/squareup/Card$Brand;
    .locals 16

    move-object/from16 v0, p0

    if-nez v0, :cond_0

    .line 16
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 17
    :cond_0
    invoke-interface/range {p0 .. p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    .line 18
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_1
    const/4 v2, 0x0

    .line 19
    invoke-interface {v0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v5, 0x36

    const/4 v6, 0x2

    const/16 v7, 0x38

    const/16 v8, 0x37

    const/16 v9, 0x31

    const/16 v10, 0x30

    const/16 v11, 0x39

    const/16 v12, 0x32

    const/16 v13, 0x33

    const/4 v14, 0x3

    const/4 v15, 0x4

    const/4 v3, 0x6

    const/4 v4, 0x5

    packed-switch v2, :pswitch_data_0

    .line 1430
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_0
    if-ge v1, v6, :cond_2

    .line 1378
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_2
    const/4 v2, 0x1

    .line 1379
    invoke-interface {v0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v8, :cond_3

    .line 1428
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_3
    if-ge v1, v14, :cond_4

    .line 1381
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1382
    :cond_4
    invoke-interface {v0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v9, :cond_11

    if-eq v2, v7, :cond_5

    .line 1426
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_5
    if-ge v1, v15, :cond_6

    .line 1400
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1401
    :cond_6
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v12, :cond_c

    if-eq v2, v13, :cond_7

    .line 1424
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_7
    if-ge v1, v4, :cond_8

    .line 1414
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1415
    :cond_8
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v13, :cond_9

    .line 1422
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_9
    if-ge v1, v3, :cond_a

    .line 1417
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1418
    :cond_a
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v12, :cond_b

    .line 1420
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1419
    :cond_b
    sget-object v0, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_c
    if-ge v1, v4, :cond_d

    .line 1403
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1404
    :cond_d
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v8, :cond_e

    .line 1411
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_e
    if-ge v1, v3, :cond_f

    .line 1406
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1407
    :cond_f
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v13, :cond_10

    .line 1409
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1408
    :cond_10
    sget-object v0, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_11
    if-ge v1, v15, :cond_12

    .line 1384
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1385
    :cond_12
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v12, :cond_13

    .line 1397
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_13
    if-ge v1, v4, :cond_14

    .line 1387
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1388
    :cond_14
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v12, :cond_15

    .line 1395
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_15
    if-ge v1, v3, :cond_16

    .line 1390
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1391
    :cond_16
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v8, :cond_17

    .line 1393
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1392
    :cond_17
    sget-object v0, Lcom/squareup/Card$Brand;->SQUARE_CAPITAL_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_1
    if-ge v1, v6, :cond_18

    .line 930
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_18
    const/4 v2, 0x1

    .line 931
    invoke-interface {v0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    packed-switch v2, :pswitch_data_1

    .line 1375
    :pswitch_2
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1374
    :pswitch_3
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1373
    :pswitch_4
    sget-object v0, Lcom/squareup/Card$Brand;->DISCOVER:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_5
    if-ge v1, v14, :cond_19

    .line 1363
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1364
    :cond_19
    invoke-interface {v0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    packed-switch v0, :pswitch_data_2

    .line 1371
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1370
    :pswitch_6
    sget-object v0, Lcom/squareup/Card$Brand;->DISCOVER:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1369
    :pswitch_7
    sget-object v0, Lcom/squareup/Card$Brand;->DISCOVER:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1368
    :pswitch_8
    sget-object v0, Lcom/squareup/Card$Brand;->DISCOVER:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1367
    :pswitch_9
    sget-object v0, Lcom/squareup/Card$Brand;->DISCOVER:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1366
    :pswitch_a
    sget-object v0, Lcom/squareup/Card$Brand;->DISCOVER:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1365
    :pswitch_b
    sget-object v0, Lcom/squareup/Card$Brand;->DISCOVER:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_c
    if-ge v1, v14, :cond_1a

    .line 1285
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1286
    :cond_1a
    invoke-interface {v0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v5, :cond_32

    if-eq v2, v8, :cond_2b

    if-eq v2, v11, :cond_1b

    .line 1360
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_1b
    if-ge v1, v15, :cond_1c

    .line 1320
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1321
    :cond_1c
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    packed-switch v2, :pswitch_data_3

    .line 1358
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_d
    if-ge v1, v4, :cond_1d

    .line 1336
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1337
    :cond_1d
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v10, :cond_24

    if-eq v2, v12, :cond_21

    if-eq v2, v5, :cond_1e

    .line 1356
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_1e
    if-ge v1, v3, :cond_1f

    .line 1351
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1352
    :cond_1f
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v9, :cond_20

    .line 1354
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1353
    :cond_20
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_21
    if-ge v1, v3, :cond_22

    .line 1345
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1346
    :cond_22
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v9, :cond_23

    .line 1348
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1347
    :cond_23
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_24
    if-ge v1, v3, :cond_25

    .line 1339
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1340
    :cond_25
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v8, :cond_26

    .line 1342
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1341
    :cond_26
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_e
    if-ge v1, v4, :cond_27

    .line 1325
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1326
    :cond_27
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v13, :cond_28

    .line 1333
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_28
    if-ge v1, v3, :cond_29

    .line 1328
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1329
    :cond_29
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v13, :cond_2a

    .line 1331
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1330
    :cond_2a
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1323
    :pswitch_f
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_2b
    if-ge v1, v15, :cond_2c

    .line 1304
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1305
    :cond_2c
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v12, :cond_2d

    .line 1317
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_2d
    if-ge v1, v4, :cond_2e

    .line 1307
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1308
    :cond_2e
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v10, :cond_2f

    .line 1315
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_2f
    if-ge v1, v3, :cond_30

    .line 1310
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1311
    :cond_30
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x34

    if-eq v0, v1, :cond_31

    .line 1313
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1312
    :cond_31
    sget-object v0, Lcom/squareup/Card$Brand;->EFTPOS:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_32
    if-ge v1, v15, :cond_33

    .line 1288
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1289
    :cond_33
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v9, :cond_34

    .line 1301
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_34
    if-ge v1, v4, :cond_35

    .line 1291
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1292
    :cond_35
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v12, :cond_36

    .line 1299
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_36
    if-ge v1, v3, :cond_37

    .line 1294
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1295
    :cond_37
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v11, :cond_38

    .line 1297
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1296
    :cond_38
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_10
    if-ge v1, v14, :cond_39

    .line 1077
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1078
    :cond_39
    invoke-interface {v0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    packed-switch v2, :pswitch_data_4

    .line 1282
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_11
    if-ge v1, v15, :cond_3a

    .line 1249
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1250
    :cond_3a
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v5, 0x34

    if-eq v2, v5, :cond_3b

    .line 1280
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_3b
    if-ge v1, v4, :cond_3c

    .line 1252
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1253
    :cond_3c
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v5, 0x34

    if-eq v2, v5, :cond_3f

    const/16 v5, 0x35

    if-eq v2, v5, :cond_3d

    .line 1278
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_3d
    if-ge v1, v3, :cond_3e

    .line 1269
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1270
    :cond_3e
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    packed-switch v0, :pswitch_data_5

    .line 1276
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1275
    :pswitch_12
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1274
    :pswitch_13
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1273
    :pswitch_14
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1272
    :pswitch_15
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1271
    :pswitch_16
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_3f
    if-ge v1, v3, :cond_40

    .line 1255
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1256
    :cond_40
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    packed-switch v0, :pswitch_data_6

    .line 1266
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1265
    :pswitch_17
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1264
    :pswitch_18
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1263
    :pswitch_19
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1262
    :pswitch_1a
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1261
    :pswitch_1b
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1260
    :pswitch_1c
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1259
    :pswitch_1d
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1258
    :pswitch_1e
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1257
    :pswitch_1f
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_20
    if-ge v1, v15, :cond_41

    .line 1220
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1221
    :cond_41
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    packed-switch v2, :pswitch_data_7

    .line 1246
    :pswitch_21
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1245
    :pswitch_22
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1244
    :pswitch_23
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1243
    :pswitch_24
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1242
    :pswitch_25
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1241
    :pswitch_26
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1240
    :pswitch_27
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1239
    :pswitch_28
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_29
    if-ge v1, v4, :cond_42

    .line 1223
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1224
    :cond_42
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v6, 0x34

    if-eq v2, v6, :cond_46

    if-eq v2, v5, :cond_43

    .line 1237
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_43
    if-ge v1, v3, :cond_44

    .line 1232
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1233
    :cond_44
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v7, :cond_45

    .line 1235
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1234
    :cond_45
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_46
    if-ge v1, v3, :cond_47

    .line 1226
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1227
    :cond_47
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v12, :cond_48

    .line 1229
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1228
    :cond_48
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_2a
    if-ge v1, v15, :cond_49

    .line 1193
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1194
    :cond_49
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v9, :cond_4f

    if-eq v2, v8, :cond_4a

    .line 1217
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_4a
    if-ge v1, v4, :cond_4b

    .line 1207
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1208
    :cond_4b
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v5, 0x34

    if-eq v2, v5, :cond_4c

    .line 1215
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_4c
    if-ge v1, v3, :cond_4d

    .line 1210
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1211
    :cond_4d
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v10, :cond_4e

    .line 1213
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1212
    :cond_4e
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_4f
    if-ge v1, v4, :cond_50

    .line 1196
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1197
    :cond_50
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v13, :cond_51

    .line 1204
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_51
    if-ge v1, v3, :cond_52

    .line 1199
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1200
    :cond_52
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x35

    if-eq v0, v1, :cond_53

    .line 1202
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1201
    :cond_53
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1191
    :pswitch_2b
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1190
    :pswitch_2c
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1189
    :pswitch_2d
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_2e
    if-ge v1, v15, :cond_54

    .line 1096
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1097
    :cond_54
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    packed-switch v2, :pswitch_data_8

    goto/16 :goto_0

    :pswitch_2f
    if-ge v1, v4, :cond_55

    .line 1153
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1154
    :cond_55
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    packed-switch v2, :pswitch_data_9

    .line 1170
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_30
    if-ge v1, v3, :cond_56

    .line 1160
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1161
    :cond_56
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    packed-switch v0, :pswitch_data_a

    .line 1168
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1167
    :pswitch_31
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1166
    :pswitch_32
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1165
    :pswitch_33
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1164
    :pswitch_34
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1163
    :pswitch_35
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1162
    :pswitch_36
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1158
    :pswitch_37
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1156
    :pswitch_38
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1151
    :pswitch_39
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1149
    :pswitch_3a
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1147
    :pswitch_3b
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1145
    :pswitch_3c
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1143
    :pswitch_3d
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1141
    :pswitch_3e
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1139
    :pswitch_3f
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_40
    if-ge v1, v4, :cond_57

    .line 1110
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1111
    :cond_57
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    packed-switch v2, :pswitch_data_b

    .line 1136
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1135
    :pswitch_41
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1133
    :pswitch_42
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1131
    :pswitch_43
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1129
    :pswitch_44
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1127
    :pswitch_45
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1125
    :pswitch_46
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1123
    :pswitch_47
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_48
    if-ge v1, v3, :cond_58

    .line 1113
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1114
    :cond_58
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v12, :cond_59

    packed-switch v0, :pswitch_data_c

    .line 1120
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1119
    :pswitch_49
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1118
    :pswitch_4a
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1117
    :pswitch_4b
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1116
    :pswitch_4c
    sget-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1115
    :cond_59
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_4d
    if-ge v1, v4, :cond_5a

    .line 1099
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1100
    :cond_5a
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v13, :cond_5b

    .line 1107
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_5b
    if-ge v1, v3, :cond_5c

    .line 1102
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1103
    :cond_5c
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v13, :cond_5d

    .line 1105
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1104
    :cond_5d
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :goto_0
    :pswitch_4e
    if-ge v1, v15, :cond_5e

    .line 1174
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1175
    :cond_5e
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v5, 0x34

    if-eq v2, v5, :cond_5f

    .line 1187
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_5f
    if-ge v1, v4, :cond_60

    .line 1177
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1178
    :cond_60
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v12, :cond_61

    .line 1185
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_61
    if-ge v1, v3, :cond_62

    .line 1180
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1181
    :cond_62
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v13, :cond_63

    .line 1183
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1182
    :cond_63
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_4f
    if-ge v1, v15, :cond_64

    .line 1080
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1081
    :cond_64
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v8, :cond_65

    .line 1093
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_65
    if-ge v1, v4, :cond_66

    .line 1083
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1084
    :cond_66
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v5, 0x34

    if-eq v2, v5, :cond_67

    .line 1091
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_67
    if-ge v1, v3, :cond_68

    .line 1086
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1087
    :cond_68
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v11, :cond_69

    .line 1089
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1088
    :cond_69
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_50
    if-ge v1, v14, :cond_6a

    .line 933
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 934
    :cond_6a
    invoke-interface {v0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v10, :cond_a0

    if-eq v2, v9, :cond_8d

    if-eq v2, v13, :cond_77

    if-eq v2, v5, :cond_6b

    .line 1074
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_6b
    if-ge v1, v15, :cond_6c

    .line 1048
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1049
    :cond_6c
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v10, :cond_72

    if-eq v2, v13, :cond_6d

    .line 1072
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_6d
    if-ge v1, v4, :cond_6e

    .line 1062
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1063
    :cond_6e
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v5, 0x35

    if-eq v2, v5, :cond_6f

    .line 1070
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_6f
    if-ge v1, v3, :cond_70

    .line 1065
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1066
    :cond_70
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v8, :cond_71

    .line 1068
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1067
    :cond_71
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_72
    if-ge v1, v4, :cond_73

    .line 1051
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1052
    :cond_73
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v7, :cond_74

    .line 1059
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_74
    if-ge v1, v3, :cond_75

    .line 1054
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1055
    :cond_75
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v11, :cond_76

    .line 1057
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1056
    :cond_76
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_77
    if-ge v1, v15, :cond_78

    .line 1002
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1003
    :cond_78
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v9, :cond_88

    if-eq v2, v12, :cond_7f

    if-eq v2, v5, :cond_79

    .line 1045
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_79
    if-ge v1, v4, :cond_7a

    .line 1034
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1035
    :cond_7a
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v7, :cond_7b

    .line 1043
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_7b
    if-ge v1, v3, :cond_7c

    .line 1037
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1038
    :cond_7c
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v12, :cond_7e

    if-eq v0, v13, :cond_7d

    .line 1041
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1040
    :cond_7d
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1039
    :cond_7e
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_7f
    if-ge v1, v4, :cond_80

    .line 1016
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1017
    :cond_80
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v9, :cond_84

    if-eq v2, v5, :cond_81

    .line 1031
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_81
    if-ge v1, v3, :cond_82

    .line 1026
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1027
    :cond_82
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v11, :cond_83

    .line 1029
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1028
    :cond_83
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_84
    if-ge v1, v3, :cond_85

    .line 1019
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1020
    :cond_85
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v10, :cond_87

    const/16 v1, 0x34

    if-eq v0, v1, :cond_86

    .line 1023
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1022
    :cond_86
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1021
    :cond_87
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_88
    if-ge v1, v4, :cond_89

    .line 1005
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1006
    :cond_89
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v5, :cond_8a

    .line 1013
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_8a
    if-ge v1, v3, :cond_8b

    .line 1008
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1009
    :cond_8b
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x34

    if-eq v0, v1, :cond_8c

    .line 1011
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 1010
    :cond_8c
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_8d
    if-ge v1, v15, :cond_8e

    .line 952
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 953
    :cond_8e
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v9, :cond_9f

    packed-switch v2, :pswitch_data_d

    .line 999
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_51
    if-ge v1, v4, :cond_8f

    .line 989
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 990
    :cond_8f
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v5, :cond_90

    .line 997
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_90
    if-ge v1, v3, :cond_91

    .line 992
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 993
    :cond_91
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v12, :cond_92

    .line 995
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 994
    :cond_92
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_52
    if-ge v1, v4, :cond_93

    .line 978
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 979
    :cond_93
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v7, :cond_94

    .line 986
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_94
    if-ge v1, v3, :cond_95

    .line 981
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 982
    :cond_95
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v8, :cond_96

    .line 984
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 983
    :cond_96
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_53
    if-ge v1, v4, :cond_97

    .line 967
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 968
    :cond_97
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v12, :cond_98

    .line 975
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_98
    if-ge v1, v3, :cond_99

    .line 970
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 971
    :cond_99
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x35

    if-eq v0, v1, :cond_9a

    .line 973
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 972
    :cond_9a
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_54
    if-ge v1, v4, :cond_9b

    .line 956
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 957
    :cond_9b
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v7, :cond_9c

    .line 964
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_9c
    if-ge v1, v3, :cond_9d

    .line 959
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 960
    :cond_9d
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v7, :cond_9e

    .line 962
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 961
    :cond_9e
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 954
    :cond_9f
    sget-object v0, Lcom/squareup/Card$Brand;->DISCOVER:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_a0
    if-ge v1, v15, :cond_a1

    .line 936
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 937
    :cond_a1
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v7, :cond_a2

    .line 949
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_a2
    if-ge v1, v4, :cond_a3

    .line 939
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 940
    :cond_a3
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v8, :cond_a4

    .line 947
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_a4
    if-ge v1, v3, :cond_a5

    .line 942
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 943
    :cond_a5
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v5, :cond_a6

    .line 945
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 944
    :cond_a6
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_55
    if-ge v1, v6, :cond_a7

    .line 170
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_a7
    const/4 v2, 0x1

    .line 171
    invoke-interface {v0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    packed-switch v2, :pswitch_data_e

    .line 927
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_56
    if-ge v1, v14, :cond_a8

    .line 623
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 624
    :cond_a8
    invoke-interface {v0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    packed-switch v2, :pswitch_data_f

    .line 925
    :pswitch_57
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_58
    if-ge v1, v15, :cond_a9

    .line 882
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 883
    :cond_a9
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v12, :cond_b4

    const/16 v5, 0x35

    if-eq v2, v5, :cond_af

    if-eq v2, v8, :cond_aa

    .line 923
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_aa
    if-ge v1, v4, :cond_ab

    .line 913
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 914
    :cond_ab
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v7, :cond_ac

    .line 921
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_ac
    if-ge v1, v3, :cond_ad

    .line 916
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 917
    :cond_ad
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v9, :cond_ae

    .line 919
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 918
    :cond_ae
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_af
    if-ge v1, v4, :cond_b0

    .line 902
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 903
    :cond_b0
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v5, 0x34

    if-eq v2, v5, :cond_b1

    .line 910
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_b1
    if-ge v1, v3, :cond_b2

    .line 905
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 906
    :cond_b2
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v12, :cond_b3

    .line 908
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 907
    :cond_b3
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_b4
    if-ge v1, v4, :cond_b5

    .line 885
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 886
    :cond_b5
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v5, 0x35

    if-eq v2, v5, :cond_b9

    if-eq v2, v11, :cond_b6

    .line 899
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_b6
    if-ge v1, v3, :cond_b7

    .line 894
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 895
    :cond_b7
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v8, :cond_b8

    .line 897
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 896
    :cond_b8
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_b9
    if-ge v1, v3, :cond_ba

    .line 888
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 889
    :cond_ba
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v9, :cond_bb

    .line 891
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 890
    :cond_bb
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_59
    if-ge v1, v15, :cond_bc

    .line 800
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 801
    :cond_bc
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v5, :cond_d8

    if-eq v2, v7, :cond_c5

    if-eq v2, v11, :cond_bd

    .line 879
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_bd
    if-ge v1, v4, :cond_be

    .line 863
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 864
    :cond_be
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v10, :cond_c2

    if-eq v2, v7, :cond_bf

    .line 877
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_bf
    if-ge v1, v3, :cond_c0

    .line 872
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 873
    :cond_c0
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v7, :cond_c1

    .line 875
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 874
    :cond_c1
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_c2
    if-ge v1, v3, :cond_c3

    .line 866
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 867
    :cond_c3
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v13, :cond_c4

    .line 869
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 868
    :cond_c4
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_c5
    if-ge v1, v4, :cond_c6

    .line 820
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 821
    :cond_c6
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v13, :cond_d5

    if-eq v2, v11, :cond_c7

    .line 860
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_c7
    if-ge v1, v3, :cond_c8

    .line 829
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 830
    :cond_c8
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v10, :cond_c9

    .line 858
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_c9
    const/4 v2, 0x7

    if-ge v1, v2, :cond_ca

    .line 832
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 833
    :cond_ca
    invoke-interface {v0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    if-eq v3, v10, :cond_cb

    .line 856
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_cb
    const/16 v3, 0x8

    if-ge v1, v3, :cond_cc

    .line 835
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 836
    :cond_cc
    invoke-interface {v0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v8, :cond_cd

    .line 854
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_cd
    const/16 v2, 0x9

    if-ge v1, v2, :cond_ce

    .line 838
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_ce
    const/16 v2, 0x8

    .line 839
    invoke-interface {v0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v10, :cond_d2

    const/16 v3, 0x35

    if-eq v2, v3, :cond_cf

    .line 852
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_cf
    const/16 v2, 0xa

    if-ge v1, v2, :cond_d0

    .line 847
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_d0
    const/16 v1, 0x9

    .line 848
    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v11, :cond_d1

    .line 850
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 849
    :cond_d1
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_d2
    const/16 v2, 0xa

    if-ge v1, v2, :cond_d3

    .line 841
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_d3
    const/16 v1, 0x9

    .line 842
    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v7, :cond_d4

    .line 844
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 843
    :cond_d4
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_d5
    if-ge v1, v3, :cond_d6

    .line 823
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 824
    :cond_d6
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v13, :cond_d7

    .line 826
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 825
    :cond_d7
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_d8
    if-ge v1, v4, :cond_d9

    .line 803
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 804
    :cond_d9
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v10, :cond_dd

    const/16 v5, 0x35

    if-eq v2, v5, :cond_da

    .line 817
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_da
    if-ge v1, v3, :cond_db

    .line 812
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 813
    :cond_db
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x34

    if-eq v0, v1, :cond_dc

    .line 815
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 814
    :cond_dc
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_dd
    if-ge v1, v3, :cond_de

    .line 806
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 807
    :cond_de
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v11, :cond_df

    .line 809
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 808
    :cond_df
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_5a
    if-ge v1, v15, :cond_e0

    .line 773
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 774
    :cond_e0
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v7, :cond_e6

    if-eq v2, v11, :cond_e1

    .line 797
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_e1
    if-ge v1, v4, :cond_e2

    .line 787
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 788
    :cond_e2
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v6, 0x35

    if-eq v2, v6, :cond_e3

    .line 795
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_e3
    if-ge v1, v3, :cond_e4

    .line 790
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 791
    :cond_e4
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v5, :cond_e5

    .line 793
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 792
    :cond_e5
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_e6
    if-ge v1, v4, :cond_e7

    .line 776
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 777
    :cond_e7
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v13, :cond_e8

    .line 784
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_e8
    if-ge v1, v3, :cond_e9

    .line 779
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 780
    :cond_e9
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v9, :cond_ea

    .line 782
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 781
    :cond_ea
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_5b
    if-ge v1, v15, :cond_eb

    .line 751
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 752
    :cond_eb
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v13, :cond_ec

    .line 770
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_ec
    if-ge v1, v4, :cond_ed

    .line 754
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 755
    :cond_ed
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v6, 0x35

    if-eq v2, v6, :cond_f1

    if-eq v2, v5, :cond_ee

    .line 768
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_ee
    if-ge v1, v3, :cond_ef

    .line 763
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 764
    :cond_ef
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v10, :cond_f0

    .line 766
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 765
    :cond_f0
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_f1
    if-ge v1, v3, :cond_f2

    .line 757
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 758
    :cond_f2
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v11, :cond_f3

    .line 760
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 759
    :cond_f3
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_5c
    if-ge v1, v15, :cond_f4

    .line 729
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 730
    :cond_f4
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v9, :cond_f5

    .line 748
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_f5
    if-ge v1, v4, :cond_f6

    .line 732
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 733
    :cond_f6
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v13, :cond_fa

    if-eq v2, v5, :cond_f7

    .line 746
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_f7
    if-ge v1, v3, :cond_f8

    .line 741
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 742
    :cond_f8
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v13, :cond_f9

    .line 744
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 743
    :cond_f9
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_fa
    if-ge v1, v3, :cond_fb

    .line 735
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 736
    :cond_fb
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v13, :cond_fc

    .line 738
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 737
    :cond_fc
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_5d
    if-ge v1, v15, :cond_fd

    .line 628
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 629
    :cond_fd
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    packed-switch v2, :pswitch_data_10

    .line 726
    :pswitch_5e
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_5f
    if-ge v1, v4, :cond_fe

    .line 683
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 684
    :cond_fe
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v10, :cond_10e

    if-eq v2, v12, :cond_10b

    if-eq v2, v7, :cond_108

    if-eq v2, v11, :cond_ff

    .line 724
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_ff
    if-ge v1, v3, :cond_100

    .line 704
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 705
    :cond_100
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v10, :cond_101

    .line 722
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_101
    const/4 v2, 0x7

    if-ge v1, v2, :cond_102

    .line 707
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 708
    :cond_102
    invoke-interface {v0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    if-eq v3, v12, :cond_103

    .line 720
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_103
    const/16 v3, 0x8

    if-ge v1, v3, :cond_104

    .line 710
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 711
    :cond_104
    invoke-interface {v0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v12, :cond_105

    .line 718
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_105
    const/16 v2, 0x9

    if-ge v1, v2, :cond_106

    .line 713
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_106
    const/16 v1, 0x8

    .line 714
    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x35

    if-eq v0, v1, :cond_107

    .line 716
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 715
    :cond_107
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_108
    if-ge v1, v3, :cond_109

    .line 698
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 699
    :cond_109
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v11, :cond_10a

    .line 701
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 700
    :cond_10a
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_10b
    if-ge v1, v3, :cond_10c

    .line 692
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 693
    :cond_10c
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v7, :cond_10d

    .line 695
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 694
    :cond_10d
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_10e
    if-ge v1, v3, :cond_10f

    .line 686
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 687
    :cond_10f
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v11, :cond_110

    .line 689
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 688
    :cond_110
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 681
    :pswitch_60
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_61
    if-ge v1, v4, :cond_111

    .line 668
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 669
    :cond_111
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v12, :cond_112

    .line 678
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_112
    if-ge v1, v3, :cond_113

    .line 671
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 672
    :cond_113
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    packed-switch v0, :pswitch_data_11

    .line 676
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 675
    :pswitch_62
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 674
    :pswitch_63
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 673
    :pswitch_64
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_65
    if-ge v1, v4, :cond_114

    .line 637
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 638
    :cond_114
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    packed-switch v2, :pswitch_data_12

    .line 665
    :pswitch_66
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_67
    if-ge v1, v3, :cond_115

    .line 646
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 647
    :cond_115
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    packed-switch v2, :pswitch_data_13

    goto :goto_1

    .line 657
    :pswitch_68
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 656
    :pswitch_69
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 655
    :pswitch_6a
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 654
    :pswitch_6b
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 653
    :pswitch_6c
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 652
    :pswitch_6d
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 651
    :pswitch_6e
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 650
    :pswitch_6f
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 649
    :pswitch_70
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 648
    :pswitch_71
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    :goto_1
    :pswitch_72
    if-ge v1, v3, :cond_116

    .line 660
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 661
    :cond_116
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v13, :cond_117

    .line 663
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 662
    :cond_117
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 644
    :pswitch_73
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 642
    :pswitch_74
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 640
    :pswitch_75
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 635
    :pswitch_76
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 633
    :pswitch_77
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 631
    :pswitch_78
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 626
    :pswitch_79
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_7a
    if-ge v1, v14, :cond_118

    .line 541
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 542
    :cond_118
    invoke-interface {v0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    packed-switch v2, :pswitch_data_14

    goto/16 :goto_5

    .line 618
    :pswitch_7b
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 616
    :pswitch_7c
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 614
    :pswitch_7d
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 612
    :pswitch_7e
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 610
    :pswitch_7f
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 608
    :pswitch_80
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 606
    :pswitch_81
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_82
    if-ge v1, v15, :cond_119

    .line 546
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 547
    :cond_119
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    packed-switch v2, :pswitch_data_15

    goto :goto_4

    .line 601
    :pswitch_83
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 599
    :pswitch_84
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 597
    :pswitch_85
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 595
    :pswitch_86
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 593
    :pswitch_87
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 591
    :pswitch_88
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 589
    :pswitch_89
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 587
    :pswitch_8a
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_8b
    if-ge v1, v4, :cond_11a

    .line 549
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 550
    :cond_11a
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    packed-switch v2, :pswitch_data_16

    goto :goto_3

    .line 582
    :pswitch_8c
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 580
    :pswitch_8d
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_8e
    if-ge v1, v3, :cond_11b

    .line 564
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 565
    :cond_11b
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    packed-switch v0, :pswitch_data_17

    goto :goto_2

    .line 575
    :pswitch_8f
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 574
    :pswitch_90
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 573
    :pswitch_91
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 572
    :pswitch_92
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 571
    :pswitch_93
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 570
    :pswitch_94
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 569
    :pswitch_95
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 568
    :pswitch_96
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 567
    :pswitch_97
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 566
    :pswitch_98
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 578
    :goto_2
    :pswitch_99
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 562
    :pswitch_9a
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 560
    :pswitch_9b
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 558
    :pswitch_9c
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 556
    :pswitch_9d
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 554
    :pswitch_9e
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 552
    :pswitch_9f
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 585
    :goto_3
    :pswitch_a0
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 604
    :goto_4
    :pswitch_a1
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 544
    :pswitch_a2
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 621
    :goto_5
    :pswitch_a3
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 539
    :pswitch_a4
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 538
    :pswitch_a5
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 537
    :pswitch_a6
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 536
    :pswitch_a7
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 535
    :pswitch_a8
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_a9
    if-ge v1, v14, :cond_11c

    .line 173
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 174
    :cond_11c
    invoke-interface {v0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v8, :cond_179

    packed-switch v2, :pswitch_data_18

    .line 533
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_aa
    if-ge v1, v15, :cond_11d

    .line 303
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 304
    :cond_11d
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v9, :cond_13e

    packed-switch v2, :pswitch_data_19

    .line 391
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_ab
    if-ge v1, v4, :cond_11e

    .line 380
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 381
    :cond_11e
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v11, :cond_11f

    .line 389
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_11f
    if-ge v1, v3, :cond_120

    .line 383
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 384
    :cond_120
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v10, :cond_122

    if-eq v0, v11, :cond_121

    .line 387
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 386
    :cond_121
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 385
    :cond_122
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_ac
    if-ge v1, v4, :cond_123

    .line 351
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 352
    :cond_123
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v10, :cond_12d

    if-eq v2, v12, :cond_12a

    const/16 v5, 0x34

    if-eq v2, v5, :cond_127

    if-eq v2, v11, :cond_124

    .line 377
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_124
    if-ge v1, v3, :cond_125

    .line 372
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 373
    :cond_125
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x35

    if-eq v0, v1, :cond_126

    .line 375
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 374
    :cond_126
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_127
    if-ge v1, v3, :cond_128

    .line 366
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 367
    :cond_128
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v12, :cond_129

    .line 369
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 368
    :cond_129
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_12a
    if-ge v1, v3, :cond_12b

    .line 360
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 361
    :cond_12b
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v9, :cond_12c

    .line 363
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 362
    :cond_12c
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_12d
    if-ge v1, v3, :cond_12e

    .line 354
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 355
    :cond_12e
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v7, :cond_12f

    .line 357
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 356
    :cond_12f
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_ad
    if-ge v1, v4, :cond_130

    .line 340
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 341
    :cond_130
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v5, :cond_131

    .line 348
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_131
    if-ge v1, v3, :cond_132

    .line 343
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 344
    :cond_132
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v9, :cond_133

    .line 346
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 345
    :cond_133
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_ae
    if-ge v1, v4, :cond_134

    .line 317
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 318
    :cond_134
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v5, 0x34

    if-eq v2, v5, :cond_13b

    const/16 v5, 0x35

    if-eq v2, v5, :cond_138

    if-eq v2, v11, :cond_135

    .line 337
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_135
    if-ge v1, v3, :cond_136

    .line 332
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 333
    :cond_136
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v13, :cond_137

    .line 335
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 334
    :cond_137
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_138
    if-ge v1, v3, :cond_139

    .line 326
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 327
    :cond_139
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v12, :cond_13a

    .line 329
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 328
    :cond_13a
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_13b
    if-ge v1, v3, :cond_13c

    .line 320
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 321
    :cond_13c
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v11, :cond_13d

    .line 323
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 322
    :cond_13d
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_13e
    if-ge v1, v4, :cond_13f

    .line 306
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 307
    :cond_13f
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v11, :cond_140

    .line 314
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_140
    if-ge v1, v3, :cond_141

    .line 309
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 310
    :cond_141
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x35

    if-eq v0, v1, :cond_142

    .line 312
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 311
    :cond_142
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_af
    if-ge v1, v15, :cond_143

    .line 287
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 288
    :cond_143
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v7, :cond_144

    .line 300
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_144
    if-ge v1, v4, :cond_145

    .line 290
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 291
    :cond_145
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v7, :cond_146

    .line 298
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_146
    if-ge v1, v3, :cond_147

    .line 293
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 294
    :cond_147
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v5, :cond_148

    .line 296
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 295
    :cond_148
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_b0
    if-ge v1, v15, :cond_149

    .line 254
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 255
    :cond_149
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v9, :cond_14f

    if-eq v2, v12, :cond_14a

    .line 284
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_14a
    if-ge v1, v4, :cond_14b

    .line 274
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 275
    :cond_14b
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v5, 0x35

    if-eq v2, v5, :cond_14c

    .line 282
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_14c
    if-ge v1, v3, :cond_14d

    .line 277
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 278
    :cond_14d
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v8, :cond_14e

    .line 280
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 279
    :cond_14e
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_14f
    if-ge v1, v4, :cond_150

    .line 257
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 258
    :cond_150
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v9, :cond_154

    if-eq v2, v13, :cond_151

    .line 271
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_151
    if-ge v1, v3, :cond_152

    .line 266
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 267
    :cond_152
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x35

    if-eq v0, v1, :cond_153

    .line 269
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 268
    :cond_153
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_154
    if-ge v1, v3, :cond_155

    .line 260
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 261
    :cond_155
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v8, :cond_156

    .line 263
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 262
    :cond_156
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_b1
    if-ge v1, v15, :cond_157

    .line 203
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 204
    :cond_157
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v10, :cond_166

    if-eq v2, v9, :cond_160

    if-eq v2, v8, :cond_158

    .line 251
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_158
    if-ge v1, v4, :cond_159

    .line 235
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 236
    :cond_159
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v8, :cond_15d

    if-eq v2, v11, :cond_15a

    .line 249
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_15a
    if-ge v1, v3, :cond_15b

    .line 244
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 245
    :cond_15b
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v13, :cond_15c

    .line 247
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 246
    :cond_15c
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_15d
    if-ge v1, v3, :cond_15e

    .line 238
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 239
    :cond_15e
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v8, :cond_15f

    .line 241
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 240
    :cond_15f
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_160
    if-ge v1, v4, :cond_161

    .line 223
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 224
    :cond_161
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v9, :cond_162

    .line 232
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_162
    if-ge v1, v3, :cond_163

    .line 226
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 227
    :cond_163
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v7, :cond_165

    if-eq v0, v11, :cond_164

    .line 230
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 229
    :cond_164
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 228
    :cond_165
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_166
    if-ge v1, v4, :cond_167

    .line 206
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 207
    :cond_167
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v10, :cond_16b

    if-eq v2, v9, :cond_168

    .line 220
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_168
    if-ge v1, v3, :cond_169

    .line 215
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 216
    :cond_169
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v12, :cond_16a

    .line 218
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 217
    :cond_16a
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_16b
    if-ge v1, v3, :cond_16c

    .line 209
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 210
    :cond_16c
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x34

    if-eq v0, v1, :cond_16d

    .line 212
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 211
    :cond_16d
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_b2
    if-ge v1, v15, :cond_16e

    .line 176
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 177
    :cond_16e
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v12, :cond_174

    if-eq v2, v8, :cond_16f

    .line 200
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_16f
    if-ge v1, v4, :cond_170

    .line 190
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 191
    :cond_170
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v5, :cond_171

    .line 198
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_171
    if-ge v1, v3, :cond_172

    .line 193
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 194
    :cond_172
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v5, :cond_173

    .line 196
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 195
    :cond_173
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_174
    if-ge v1, v4, :cond_175

    .line 179
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 180
    :cond_175
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v13, :cond_176

    .line 187
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_176
    if-ge v1, v3, :cond_177

    .line 182
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 183
    :cond_177
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x35

    if-eq v0, v1, :cond_178

    .line 185
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 184
    :cond_178
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_179
    if-ge v1, v15, :cond_17a

    .line 394
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 395
    :cond_17a
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v5, :cond_17b

    .line 531
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_17b
    if-ge v1, v4, :cond_17c

    .line 397
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 398
    :cond_17c
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v10, :cond_17d

    if-eq v2, v9, :cond_188

    .line 529
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_17d
    if-ge v1, v3, :cond_17e

    .line 400
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 401
    :cond_17e
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    packed-switch v2, :pswitch_data_1a

    goto/16 :goto_6

    :pswitch_b3
    const/4 v2, 0x7

    if-ge v1, v2, :cond_17f

    .line 500
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 501
    :cond_17f
    invoke-interface {v0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    packed-switch v0, :pswitch_data_1b

    .line 509
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 508
    :pswitch_b4
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 507
    :pswitch_b5
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 506
    :pswitch_b6
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 505
    :pswitch_b7
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 504
    :pswitch_b8
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 503
    :pswitch_b9
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 502
    :pswitch_ba
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_bb
    const/4 v2, 0x7

    if-ge v1, v2, :cond_180

    .line 488
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 489
    :cond_180
    invoke-interface {v0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    packed-switch v0, :pswitch_data_1c

    .line 497
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 496
    :pswitch_bc
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 495
    :pswitch_bd
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 494
    :pswitch_be
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 493
    :pswitch_bf
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 492
    :pswitch_c0
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 491
    :pswitch_c1
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 490
    :pswitch_c2
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_c3
    const/4 v2, 0x7

    if-ge v1, v2, :cond_181

    .line 476
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 477
    :cond_181
    invoke-interface {v0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    packed-switch v0, :pswitch_data_1d

    .line 485
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 484
    :pswitch_c4
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 483
    :pswitch_c5
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 482
    :pswitch_c6
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 481
    :pswitch_c7
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 480
    :pswitch_c8
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 479
    :pswitch_c9
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 478
    :pswitch_ca
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_cb
    const/4 v2, 0x7

    if-ge v1, v2, :cond_182

    .line 464
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 465
    :cond_182
    invoke-interface {v0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    packed-switch v0, :pswitch_data_1e

    .line 473
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 472
    :pswitch_cc
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 471
    :pswitch_cd
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 470
    :pswitch_ce
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 469
    :pswitch_cf
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 468
    :pswitch_d0
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 467
    :pswitch_d1
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 466
    :pswitch_d2
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_d3
    const/4 v2, 0x7

    if-ge v1, v2, :cond_183

    .line 452
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 453
    :cond_183
    invoke-interface {v0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    packed-switch v0, :pswitch_data_1f

    .line 461
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 460
    :pswitch_d4
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 459
    :pswitch_d5
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 458
    :pswitch_d6
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 457
    :pswitch_d7
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 456
    :pswitch_d8
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 455
    :pswitch_d9
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 454
    :pswitch_da
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_db
    const/4 v2, 0x7

    if-ge v1, v2, :cond_184

    .line 440
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 441
    :cond_184
    invoke-interface {v0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    packed-switch v0, :pswitch_data_20

    .line 449
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 448
    :pswitch_dc
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 447
    :pswitch_dd
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 446
    :pswitch_de
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 445
    :pswitch_df
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 444
    :pswitch_e0
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 443
    :pswitch_e1
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 442
    :pswitch_e2
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_e3
    const/4 v2, 0x7

    if-ge v1, v2, :cond_185

    .line 428
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 429
    :cond_185
    invoke-interface {v0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    packed-switch v0, :pswitch_data_21

    .line 437
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 436
    :pswitch_e4
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 435
    :pswitch_e5
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 434
    :pswitch_e6
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 433
    :pswitch_e7
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 432
    :pswitch_e8
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 431
    :pswitch_e9
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 430
    :pswitch_ea
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_eb
    const/4 v2, 0x7

    if-ge v1, v2, :cond_186

    .line 416
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 417
    :cond_186
    invoke-interface {v0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    packed-switch v0, :pswitch_data_22

    .line 425
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 424
    :pswitch_ec
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 423
    :pswitch_ed
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 422
    :pswitch_ee
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 421
    :pswitch_ef
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 420
    :pswitch_f0
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 419
    :pswitch_f1
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 418
    :pswitch_f2
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_f3
    const/4 v2, 0x7

    if-ge v1, v2, :cond_187

    .line 404
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 405
    :cond_187
    invoke-interface {v0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    packed-switch v0, :pswitch_data_23

    .line 413
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 412
    :pswitch_f4
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 411
    :pswitch_f5
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 410
    :pswitch_f6
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 409
    :pswitch_f7
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 408
    :pswitch_f8
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 407
    :pswitch_f9
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 406
    :pswitch_fa
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 402
    :pswitch_fb
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_188
    :goto_6
    if-ge v1, v3, :cond_189

    .line 513
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 514
    :cond_189
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v10, :cond_18a

    .line 527
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_18a
    const/4 v2, 0x7

    if-ge v1, v2, :cond_18b

    .line 516
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 517
    :cond_18b
    invoke-interface {v0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    packed-switch v0, :pswitch_data_24

    .line 525
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 524
    :pswitch_fc
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 523
    :pswitch_fd
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 522
    :pswitch_fe
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 521
    :pswitch_ff
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 520
    :pswitch_100
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 519
    :pswitch_101
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 518
    :pswitch_102
    sget-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object v0

    .line 168
    :pswitch_103
    sget-object v0, Lcom/squareup/Card$Brand;->VISA:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_104
    if-ge v1, v6, :cond_18c

    .line 92
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_18c
    const/4 v2, 0x1

    .line 93
    invoke-interface {v0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v10, :cond_19b

    packed-switch v2, :pswitch_data_25

    .line 166
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 165
    :pswitch_105
    sget-object v0, Lcom/squareup/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/Card$Brand;

    return-object v0

    .line 164
    :pswitch_106
    sget-object v0, Lcom/squareup/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/Card$Brand;

    return-object v0

    .line 163
    :pswitch_107
    sget-object v0, Lcom/squareup/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/Card$Brand;

    return-object v0

    .line 162
    :pswitch_108
    sget-object v0, Lcom/squareup/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_109
    if-ge v1, v14, :cond_18d

    .line 139
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 140
    :cond_18d
    invoke-interface {v0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    packed-switch v2, :pswitch_data_26

    .line 160
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 159
    :pswitch_10a
    sget-object v0, Lcom/squareup/Card$Brand;->JCB:Lcom/squareup/Card$Brand;

    return-object v0

    .line 157
    :pswitch_10b
    sget-object v0, Lcom/squareup/Card$Brand;->JCB:Lcom/squareup/Card$Brand;

    return-object v0

    .line 155
    :pswitch_10c
    sget-object v0, Lcom/squareup/Card$Brand;->JCB:Lcom/squareup/Card$Brand;

    return-object v0

    .line 153
    :pswitch_10d
    sget-object v0, Lcom/squareup/Card$Brand;->JCB:Lcom/squareup/Card$Brand;

    return-object v0

    .line 151
    :pswitch_10e
    sget-object v0, Lcom/squareup/Card$Brand;->JCB:Lcom/squareup/Card$Brand;

    return-object v0

    .line 149
    :pswitch_10f
    sget-object v0, Lcom/squareup/Card$Brand;->JCB:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_110
    if-ge v1, v15, :cond_18e

    .line 142
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 143
    :cond_18e
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v7, :cond_190

    if-eq v0, v11, :cond_18f

    .line 146
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 145
    :cond_18f
    sget-object v0, Lcom/squareup/Card$Brand;->JCB:Lcom/squareup/Card$Brand;

    return-object v0

    .line 144
    :cond_190
    sget-object v0, Lcom/squareup/Card$Brand;->JCB:Lcom/squareup/Card$Brand;

    return-object v0

    .line 137
    :pswitch_111
    sget-object v0, Lcom/squareup/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_112
    if-ge v1, v14, :cond_191

    .line 112
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 113
    :cond_191
    invoke-interface {v0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v13, :cond_192

    .line 135
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_192
    if-ge v1, v15, :cond_193

    .line 115
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 116
    :cond_193
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v8, :cond_194

    .line 133
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_194
    if-ge v1, v4, :cond_195

    .line 118
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 119
    :cond_195
    invoke-interface {v0, v15}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v9, :cond_196

    .line 131
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_196
    if-ge v1, v3, :cond_197

    .line 121
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 122
    :cond_197
    invoke-interface {v0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v10, :cond_198

    .line 129
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_198
    const/4 v2, 0x7

    if-ge v1, v2, :cond_199

    .line 124
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 125
    :cond_199
    invoke-interface {v0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v10, :cond_19a

    .line 127
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 126
    :cond_19a
    sget-object v0, Lcom/squareup/Card$Brand;->JCB:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_19b
    if-ge v1, v14, :cond_19c

    .line 95
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 96
    :cond_19c
    invoke-interface {v0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-eq v2, v11, :cond_19d

    packed-switch v2, :pswitch_data_27

    .line 109
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 102
    :pswitch_113
    sget-object v0, Lcom/squareup/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/Card$Brand;

    return-object v0

    .line 101
    :pswitch_114
    sget-object v0, Lcom/squareup/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/Card$Brand;

    return-object v0

    .line 100
    :pswitch_115
    sget-object v0, Lcom/squareup/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/Card$Brand;

    return-object v0

    .line 99
    :pswitch_116
    sget-object v0, Lcom/squareup/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/Card$Brand;

    return-object v0

    .line 98
    :pswitch_117
    sget-object v0, Lcom/squareup/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/Card$Brand;

    return-object v0

    .line 97
    :pswitch_118
    sget-object v0, Lcom/squareup/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_19d
    if-ge v1, v15, :cond_19e

    .line 104
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 105
    :cond_19e
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x35

    if-eq v0, v1, :cond_19f

    .line 107
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 106
    :cond_19f
    sget-object v0, Lcom/squareup/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_119
    if-ge v1, v6, :cond_1a0

    .line 21
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :cond_1a0
    const/4 v2, 0x1

    .line 22
    invoke-interface {v0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    packed-switch v2, :pswitch_data_28

    .line 89
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_11a
    if-ge v1, v14, :cond_1a1

    .line 74
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 75
    :cond_1a1
    invoke-interface {v0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    packed-switch v2, :pswitch_data_29

    .line 87
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_11b
    if-ge v1, v15, :cond_1a2

    .line 81
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 82
    :cond_1a2
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v10, :cond_1a3

    .line 85
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 84
    :cond_1a3
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 79
    :pswitch_11c
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 77
    :pswitch_11d
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 72
    :pswitch_11e
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 70
    :pswitch_11f
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 68
    :pswitch_120
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 66
    :pswitch_121
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_122
    if-ge v1, v14, :cond_1a4

    .line 24
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 25
    :cond_1a4
    invoke-interface {v0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    packed-switch v2, :pswitch_data_2a

    .line 63
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 62
    :pswitch_123
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 60
    :pswitch_124
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 58
    :pswitch_125
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 56
    :pswitch_126
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 54
    :pswitch_127
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 52
    :pswitch_128
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 50
    :pswitch_129
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_12a
    if-ge v1, v15, :cond_1a5

    .line 27
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 28
    :cond_1a5
    invoke-interface {v0, v14}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    packed-switch v0, :pswitch_data_2b

    .line 47
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object v0

    .line 46
    :pswitch_12b
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 44
    :pswitch_12c
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 42
    :pswitch_12d
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 40
    :pswitch_12e
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 38
    :pswitch_12f
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 36
    :pswitch_130
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 34
    :pswitch_131
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 32
    :pswitch_132
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    .line 30
    :pswitch_133
    sget-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_119
        :pswitch_104
        :pswitch_103
        :pswitch_55
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x30
        :pswitch_50
        :pswitch_2
        :pswitch_10
        :pswitch_c
        :pswitch_5
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x34
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x30
        :pswitch_f
        :pswitch_e
        :pswitch_d
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x31
        :pswitch_4f
        :pswitch_2e
        :pswitch_4e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_20
        :pswitch_11
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x30
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x31
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x30
        :pswitch_29
        :pswitch_21
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
    .end packed-switch

    :pswitch_data_8
    .packed-switch 0x30
        :pswitch_4d
        :pswitch_40
        :pswitch_3f
        :pswitch_3e
        :pswitch_3d
        :pswitch_3c
        :pswitch_3b
        :pswitch_3a
        :pswitch_39
        :pswitch_2f
    .end packed-switch

    :pswitch_data_9
    .packed-switch 0x30
        :pswitch_38
        :pswitch_37
        :pswitch_30
    .end packed-switch

    :pswitch_data_a
    .packed-switch 0x30
        :pswitch_36
        :pswitch_35
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
    .end packed-switch

    :pswitch_data_b
    .packed-switch 0x32
        :pswitch_48
        :pswitch_47
        :pswitch_46
        :pswitch_45
        :pswitch_44
        :pswitch_43
        :pswitch_42
        :pswitch_41
    .end packed-switch

    :pswitch_data_c
    .packed-switch 0x36
        :pswitch_4c
        :pswitch_4b
        :pswitch_4a
        :pswitch_49
    .end packed-switch

    :pswitch_data_d
    .packed-switch 0x36
        :pswitch_54
        :pswitch_53
        :pswitch_52
        :pswitch_51
    .end packed-switch

    :pswitch_data_e
    .packed-switch 0x30
        :pswitch_a9
        :pswitch_a8
        :pswitch_a7
        :pswitch_a6
        :pswitch_a5
        :pswitch_a4
        :pswitch_7a
        :pswitch_a3
        :pswitch_56
    .end packed-switch

    :pswitch_data_f
    .packed-switch 0x30
        :pswitch_79
        :pswitch_5d
        :pswitch_5c
        :pswitch_5b
        :pswitch_57
        :pswitch_5a
        :pswitch_57
        :pswitch_57
        :pswitch_59
        :pswitch_58
    .end packed-switch

    :pswitch_data_10
    .packed-switch 0x30
        :pswitch_78
        :pswitch_77
        :pswitch_76
        :pswitch_65
        :pswitch_5e
        :pswitch_5e
        :pswitch_61
        :pswitch_60
        :pswitch_5f
    .end packed-switch

    :pswitch_data_11
    .packed-switch 0x36
        :pswitch_64
        :pswitch_63
        :pswitch_62
    .end packed-switch

    :pswitch_data_12
    .packed-switch 0x30
        :pswitch_75
        :pswitch_74
        :pswitch_73
        :pswitch_67
        :pswitch_66
        :pswitch_72
    .end packed-switch

    :pswitch_data_13
    .packed-switch 0x30
        :pswitch_71
        :pswitch_70
        :pswitch_6f
        :pswitch_6e
        :pswitch_6d
        :pswitch_6c
        :pswitch_6b
        :pswitch_6a
        :pswitch_69
        :pswitch_68
    .end packed-switch

    :pswitch_data_14
    .packed-switch 0x30
        :pswitch_a2
        :pswitch_82
        :pswitch_a1
        :pswitch_81
        :pswitch_80
        :pswitch_7f
        :pswitch_7e
        :pswitch_7d
        :pswitch_7c
        :pswitch_7b
    .end packed-switch

    :pswitch_data_15
    .packed-switch 0x30
        :pswitch_8b
        :pswitch_a0
        :pswitch_8a
        :pswitch_89
        :pswitch_88
        :pswitch_87
        :pswitch_86
        :pswitch_85
        :pswitch_84
        :pswitch_83
    .end packed-switch

    :pswitch_data_16
    .packed-switch 0x30
        :pswitch_9f
        :pswitch_9e
        :pswitch_9d
        :pswitch_9c
        :pswitch_9b
        :pswitch_9a
        :pswitch_8e
        :pswitch_99
        :pswitch_8d
        :pswitch_8c
    .end packed-switch

    :pswitch_data_17
    .packed-switch 0x30
        :pswitch_98
        :pswitch_97
        :pswitch_96
        :pswitch_95
        :pswitch_94
        :pswitch_93
        :pswitch_92
        :pswitch_91
        :pswitch_90
        :pswitch_8f
    .end packed-switch

    :pswitch_data_18
    .packed-switch 0x30
        :pswitch_b2
        :pswitch_b1
        :pswitch_b0
        :pswitch_af
        :pswitch_aa
    .end packed-switch

    :pswitch_data_19
    .packed-switch 0x35
        :pswitch_ae
        :pswitch_ad
        :pswitch_ac
        :pswitch_ab
    .end packed-switch

    :pswitch_data_1a
    .packed-switch 0x30
        :pswitch_fb
        :pswitch_f3
        :pswitch_eb
        :pswitch_e3
        :pswitch_db
        :pswitch_d3
        :pswitch_cb
        :pswitch_c3
        :pswitch_bb
        :pswitch_b3
    .end packed-switch

    :pswitch_data_1b
    .packed-switch 0x30
        :pswitch_ba
        :pswitch_b9
        :pswitch_b8
        :pswitch_b7
        :pswitch_b6
        :pswitch_b5
        :pswitch_b4
    .end packed-switch

    :pswitch_data_1c
    .packed-switch 0x30
        :pswitch_c2
        :pswitch_c1
        :pswitch_c0
        :pswitch_bf
        :pswitch_be
        :pswitch_bd
        :pswitch_bc
    .end packed-switch

    :pswitch_data_1d
    .packed-switch 0x30
        :pswitch_ca
        :pswitch_c9
        :pswitch_c8
        :pswitch_c7
        :pswitch_c6
        :pswitch_c5
        :pswitch_c4
    .end packed-switch

    :pswitch_data_1e
    .packed-switch 0x30
        :pswitch_d2
        :pswitch_d1
        :pswitch_d0
        :pswitch_cf
        :pswitch_ce
        :pswitch_cd
        :pswitch_cc
    .end packed-switch

    :pswitch_data_1f
    .packed-switch 0x30
        :pswitch_da
        :pswitch_d9
        :pswitch_d8
        :pswitch_d7
        :pswitch_d6
        :pswitch_d5
        :pswitch_d4
    .end packed-switch

    :pswitch_data_20
    .packed-switch 0x30
        :pswitch_e2
        :pswitch_e1
        :pswitch_e0
        :pswitch_df
        :pswitch_de
        :pswitch_dd
        :pswitch_dc
    .end packed-switch

    :pswitch_data_21
    .packed-switch 0x30
        :pswitch_ea
        :pswitch_e9
        :pswitch_e8
        :pswitch_e7
        :pswitch_e6
        :pswitch_e5
        :pswitch_e4
    .end packed-switch

    :pswitch_data_22
    .packed-switch 0x30
        :pswitch_f2
        :pswitch_f1
        :pswitch_f0
        :pswitch_ef
        :pswitch_ee
        :pswitch_ed
        :pswitch_ec
    .end packed-switch

    :pswitch_data_23
    .packed-switch 0x30
        :pswitch_fa
        :pswitch_f9
        :pswitch_f8
        :pswitch_f7
        :pswitch_f6
        :pswitch_f5
        :pswitch_f4
    .end packed-switch

    :pswitch_data_24
    .packed-switch 0x30
        :pswitch_102
        :pswitch_101
        :pswitch_100
        :pswitch_ff
        :pswitch_fe
        :pswitch_fd
        :pswitch_fc
    .end packed-switch

    :pswitch_data_25
    .packed-switch 0x33
        :pswitch_112
        :pswitch_111
        :pswitch_109
        :pswitch_108
        :pswitch_107
        :pswitch_106
        :pswitch_105
    .end packed-switch

    :pswitch_data_26
    .packed-switch 0x32
        :pswitch_110
        :pswitch_10f
        :pswitch_10e
        :pswitch_10d
        :pswitch_10c
        :pswitch_10b
        :pswitch_10a
    .end packed-switch

    :pswitch_data_27
    .packed-switch 0x30
        :pswitch_118
        :pswitch_117
        :pswitch_116
        :pswitch_115
        :pswitch_114
        :pswitch_113
    .end packed-switch

    :pswitch_data_28
    .packed-switch 0x32
        :pswitch_122
        :pswitch_121
        :pswitch_120
        :pswitch_11f
        :pswitch_11e
        :pswitch_11a
    .end packed-switch

    :pswitch_data_29
    .packed-switch 0x30
        :pswitch_11d
        :pswitch_11c
        :pswitch_11b
    .end packed-switch

    :pswitch_data_2a
    .packed-switch 0x32
        :pswitch_12a
        :pswitch_129
        :pswitch_128
        :pswitch_127
        :pswitch_126
        :pswitch_125
        :pswitch_124
        :pswitch_123
    .end packed-switch

    :pswitch_data_2b
    .packed-switch 0x31
        :pswitch_133
        :pswitch_132
        :pswitch_131
        :pswitch_130
        :pswitch_12f
        :pswitch_12e
        :pswitch_12d
        :pswitch_12c
        :pswitch_12b
    .end packed-switch
.end method
