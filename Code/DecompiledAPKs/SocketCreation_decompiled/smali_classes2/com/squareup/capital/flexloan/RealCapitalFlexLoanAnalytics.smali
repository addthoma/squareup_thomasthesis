.class public final Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;
.super Ljava/lang/Object;
.source "RealCapitalFlexLoanAnalytics.kt"

# interfaces
.implements Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u001b\u0018\u00002\u00020\u0001:\u0001\"B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0002J\u0008\u0010\t\u001a\u00020\u0006H\u0016J\u0008\u0010\n\u001a\u00020\u0006H\u0016J\u0008\u0010\u000b\u001a\u00020\u0006H\u0016J\u0008\u0010\u000c\u001a\u00020\u0006H\u0016J\u0008\u0010\r\u001a\u00020\u0006H\u0016J\u0008\u0010\u000e\u001a\u00020\u0006H\u0016J\u0008\u0010\u000f\u001a\u00020\u0006H\u0016J.\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u00082\u0008\u0008\u0002\u0010\u0012\u001a\u00020\u00082\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u0008H\u0002J\u0008\u0010\u0014\u001a\u00020\u0006H\u0016J\u0010\u0010\u0015\u001a\u00020\u00062\u0006\u0010\u0012\u001a\u00020\u0008H\u0016J\u0008\u0010\u0016\u001a\u00020\u0006H\u0016J\u0008\u0010\u0017\u001a\u00020\u0006H\u0016J\u0010\u0010\u0018\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u0008H\u0016J\u0010\u0010\u0019\u001a\u00020\u00062\u0006\u0010\u0012\u001a\u00020\u0008H\u0016J\u0008\u0010\u001a\u001a\u00020\u0006H\u0016J\u0010\u0010\u001b\u001a\u00020\u00062\u0006\u0010\u0011\u001a\u00020\u0008H\u0016J\u0008\u0010\u001c\u001a\u00020\u0006H\u0016J\u0010\u0010\u001d\u001a\u00020\u00062\u0006\u0010\u0012\u001a\u00020\u0008H\u0016J\u0010\u0010\u001e\u001a\u00020\u00062\u0006\u0010\u0011\u001a\u00020\u0008H\u0016J\u0008\u0010\u001f\u001a\u00020\u0006H\u0016J\u0018\u0010 \u001a\u00020\u00062\u0006\u0010\u0012\u001a\u00020\u00082\u0006\u0010\u0013\u001a\u00020\u0008H\u0016J \u0010!\u001a\u00020\u00062\u0006\u0010\u0011\u001a\u00020\u00082\u0006\u0010\u0012\u001a\u00020\u00082\u0006\u0010\u0013\u001a\u00020\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/analytics/Analytics;)V",
        "logClick",
        "",
        "description",
        "",
        "logClickCapitalActiveOfferRow",
        "logClickCapitalActivePlanRow",
        "logClickCapitalChooseOffer",
        "logClickCapitalClosedPlanRow",
        "logClickCapitalManagePlan",
        "logClickCapitalPastDuePlanRow",
        "logClickCapitalPendingApplicationRow",
        "logImpression",
        "offerId",
        "planId",
        "errorCode",
        "logViewCapitalActiveOfferRow",
        "logViewCapitalActivePlan",
        "logViewCapitalActivePlanRow",
        "logViewCapitalBalanceAppletRow",
        "logViewCapitalBalanceAppletRowError",
        "logViewCapitalClosedPlan",
        "logViewCapitalClosedPlanRow",
        "logViewCapitalFlexOffer",
        "logViewCapitalIneligibleRow",
        "logViewCapitalPastDuePlan",
        "logViewCapitalPendingApplication",
        "logViewCapitalPendingApplicationRow",
        "logViewCapitalPlanError",
        "logViewCapitalSessionBridgeError",
        "CapitalFlexLoanImpressionsEvent",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method private final logClick(Ljava/lang/String;)V
    .locals 2

    .line 156
    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    invoke-direct {v1, p1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method private final logImpression(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 146
    new-instance v1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics$CapitalFlexLoanImpressionsEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    .line 145
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method static synthetic logImpression$default(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V
    .locals 1

    and-int/lit8 p6, p5, 0x2

    const-string v0, ""

    if-eqz p6, :cond_0

    move-object p2, v0

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    move-object p3, v0

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    move-object p4, v0

    .line 143
    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->logImpression(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public logClickCapitalActiveOfferRow()V
    .locals 1

    const-string v0, "Capital: Active Offer Row"

    .line 112
    invoke-direct {p0, v0}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logClickCapitalActivePlanRow()V
    .locals 1

    const-string v0, "Capital: Active Plan Row"

    .line 116
    invoke-direct {p0, v0}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logClickCapitalChooseOffer()V
    .locals 1

    const-string v0, "Capital: Choose Offer"

    .line 132
    invoke-direct {p0, v0}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logClickCapitalClosedPlanRow()V
    .locals 1

    const-string v0, "Capital: Closed Plan Row"

    .line 124
    invoke-direct {p0, v0}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logClickCapitalManagePlan()V
    .locals 1

    const-string v0, "Capital: Manage Plan"

    .line 136
    invoke-direct {p0, v0}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logClickCapitalPastDuePlanRow()V
    .locals 1

    const-string v0, "Capital: Past Due Plan Row"

    .line 120
    invoke-direct {p0, v0}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logClickCapitalPendingApplicationRow()V
    .locals 1

    const-string v0, "Capital: Pending Application Row"

    .line 128
    invoke-direct {p0, v0}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logViewCapitalActiveOfferRow()V
    .locals 7

    const-string v1, "Deposits: View Capital Offer Row"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object v0, p0

    .line 63
    invoke-static/range {v0 .. v6}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->logImpression$default(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public logViewCapitalActivePlan(Ljava/lang/String;)V
    .locals 8

    const-string v0, "planId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "Capital: View Active Plan"

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xa

    const/4 v7, 0x0

    move-object v1, p0

    move-object v4, p1

    .line 79
    invoke-static/range {v1 .. v7}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->logImpression$default(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public logViewCapitalActivePlanRow()V
    .locals 7

    const-string v1, "Deposits: View Capital Active Plan Row"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object v0, p0

    .line 59
    invoke-static/range {v0 .. v6}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->logImpression$default(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public logViewCapitalBalanceAppletRow()V
    .locals 7

    const-string v1, "Deposits: View Capital"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object v0, p0

    .line 47
    invoke-static/range {v0 .. v6}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->logImpression$default(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public logViewCapitalBalanceAppletRowError(Ljava/lang/String;)V
    .locals 8

    const-string v0, "errorCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "Deposits: View Capital Error"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x6

    const/4 v7, 0x0

    move-object v1, p0

    move-object v5, p1

    .line 51
    invoke-static/range {v1 .. v7}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->logImpression$default(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public logViewCapitalClosedPlan(Ljava/lang/String;)V
    .locals 8

    const-string v0, "planId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "Capital: View Closed Plan"

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xa

    const/4 v7, 0x0

    move-object v1, p0

    move-object v4, p1

    .line 87
    invoke-static/range {v1 .. v7}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->logImpression$default(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public logViewCapitalClosedPlanRow()V
    .locals 7

    const-string v1, "Deposits: View Capital Closed Plan Row"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object v0, p0

    .line 55
    invoke-static/range {v0 .. v6}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->logImpression$default(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public logViewCapitalFlexOffer(Ljava/lang/String;)V
    .locals 8

    const-string v0, "offerId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "Capital: View Offer"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object v1, p0

    move-object v3, p1

    .line 75
    invoke-static/range {v1 .. v7}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->logImpression$default(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public logViewCapitalIneligibleRow()V
    .locals 7

    const-string v1, "Deposits: View Capital Ineligible Row"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object v0, p0

    .line 67
    invoke-static/range {v0 .. v6}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->logImpression$default(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public logViewCapitalPastDuePlan(Ljava/lang/String;)V
    .locals 8

    const-string v0, "planId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "Capital: View Past Due Plan"

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xa

    const/4 v7, 0x0

    move-object v1, p0

    move-object v4, p1

    .line 83
    invoke-static/range {v1 .. v7}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->logImpression$default(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public logViewCapitalPendingApplication(Ljava/lang/String;)V
    .locals 8

    const-string v0, "offerId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "Capital: View Pending Application"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object v1, p0

    move-object v3, p1

    .line 91
    invoke-static/range {v1 .. v7}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->logImpression$default(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public logViewCapitalPendingApplicationRow()V
    .locals 7

    const-string v1, "Deposits: View Capital Application Row"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object v0, p0

    .line 71
    invoke-static/range {v0 .. v6}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->logImpression$default(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public logViewCapitalPlanError(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    const-string v0, "planId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorCode"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "Capital: View Plan Error"

    const/4 v3, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    .line 95
    invoke-static/range {v1 .. v7}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->logImpression$default(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public logViewCapitalSessionBridgeError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "offerId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "planId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorCode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Capital: View Session Bridge Error"

    .line 103
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanAnalytics;->logImpression(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
