.class public final Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;
.super Ljava/lang/Object;
.source "RealCapitalFlexLoanRequestDataStore.kt"

# interfaces
.implements Lcom/squareup/capital/flexloan/CapitalFlexLoanRequestDataStore;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealCapitalFlexLoanRequestDataStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealCapitalFlexLoanRequestDataStore.kt\ncom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore\n*L\n1#1,140:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u001c\u0010\r\u001a\u00020\u000e\"\u0004\u0008\u0000\u0010\u000f2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u0002H\u000f0\u0011H\u0002J\u001c\u0010\u0012\u001a\u00020\u000c\"\u0004\u0008\u0000\u0010\u000f2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u0002H\u000f0\u0011H\u0002J\u000e\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014H\u0016J\u0016\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u00142\u0006\u0010\u0018\u001a\u00020\u000cH\u0016J\u0010\u0010\u0019\u001a\u00020\u00172\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0016\u0010\u001c\u001a\u00020\u00172\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u0011H\u0002J\u0016\u0010\u001d\u001a\u00020\u00152\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u001e0\u0011H\u0002J\u0016\u0010\u001f\u001a\u00020\u00152\u000c\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\"0!H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanRequestDataStore;",
        "capitalService",
        "Lcom/squareup/capital/flexloan/CapitalService;",
        "userSettings",
        "Lcom/squareup/settings/server/UserSettingsProvider;",
        "capitalFlexLoanAnalytics",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;",
        "failureMessageFactory",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "(Lcom/squareup/capital/flexloan/CapitalService;Lcom/squareup/settings/server/UserSettingsProvider;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;Lcom/squareup/receiving/FailureMessageFactory;)V",
        "currentUnit",
        "",
        "errorFailureMessage",
        "Lcom/squareup/receiving/FailureMessage;",
        "T",
        "failure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;",
        "errorStatusCode",
        "fetchFlexLoanStatus",
        "Lio/reactivex/Single;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;",
        "fetchFlexPlan",
        "Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;",
        "planId",
        "handlePlanRequestSuccess",
        "planResponse",
        "Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse;",
        "handleRetrievePlanRequestFailure",
        "handleSearchCustomerRequestFailure",
        "Lcom/squareup/protos/capital/external/business/service/SearchCustomersResponse;",
        "handleSearchCustomerRequestSuccess",
        "customers",
        "",
        "Lcom/squareup/protos/capital/external/business/models/Customer;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final capitalFlexLoanAnalytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

.field private final capitalService:Lcom/squareup/capital/flexloan/CapitalService;

.field private final currentUnit:Ljava/lang/String;

.field private final failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final userSettings:Lcom/squareup/settings/server/UserSettingsProvider;


# direct methods
.method public constructor <init>(Lcom/squareup/capital/flexloan/CapitalService;Lcom/squareup/settings/server/UserSettingsProvider;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;Lcom/squareup/receiving/FailureMessageFactory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "capitalService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "userSettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capitalFlexLoanAnalytics"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "failureMessageFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;->capitalService:Lcom/squareup/capital/flexloan/CapitalService;

    iput-object p2, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;->userSettings:Lcom/squareup/settings/server/UserSettingsProvider;

    iput-object p3, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;->capitalFlexLoanAnalytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    iput-object p4, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    .line 31
    iget-object p1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;->userSettings:Lcom/squareup/settings/server/UserSettingsProvider;

    invoke-interface {p1}, Lcom/squareup/settings/server/UserSettingsProvider;->getToken()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;->currentUnit:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$handlePlanRequestSuccess(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse;)Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;->handlePlanRequestSuccess(Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse;)Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleRetrievePlanRequestFailure(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;->handleRetrievePlanRequestFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleSearchCustomerRequestFailure(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;->handleSearchCustomerRequestFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleSearchCustomerRequestSuccess(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;Ljava/util/List;)Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;->handleSearchCustomerRequestSuccess(Ljava/util/List;)Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;

    move-result-object p0

    return-object p0
.end method

.method private final errorFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/FailureMessage;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+TT;>;)",
            "Lcom/squareup/receiving/FailureMessage;"
        }
    .end annotation

    .line 132
    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    .line 134
    sget v1, Lcom/squareup/capital/flexloan/impl/R$string;->capital_error_title:I

    .line 135
    sget-object v2, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore$errorFailureMessage$1;->INSTANCE:Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore$errorFailureMessage$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 132
    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    return-object p1
.end method

.method private final errorStatusCode(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+TT;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 123
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    .line 124
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;->getStatusCode()I

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 125
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;->getStatusCode()I

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    const-string p1, ""

    :goto_0
    return-object p1
.end method

.method private final handlePlanRequestSuccess(Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse;)Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;
    .locals 2

    .line 86
    new-instance v0, Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus$PlanData;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse;->plan:Lcom/squareup/protos/capital/servicing/plan/models/Plan;

    const-string v1, "planResponse.plan"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus$PlanData;-><init>(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)V

    check-cast v0, Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;

    return-object v0
.end method

.method private final handleRetrievePlanRequestFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse;",
            ">;)",
            "Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;"
        }
    .end annotation

    .line 108
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;->errorStatusCode(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Ljava/lang/String;

    move-result-object v0

    .line 109
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;->errorFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 111
    new-instance v1, Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus$Failure;

    .line 112
    new-instance v2, Lcom/squareup/capital/flexloan/FailureData;

    .line 113
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v3

    .line 114
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v4

    .line 115
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getRetryable()Z

    move-result p1

    .line 112
    invoke-direct {v2, v3, v4, p1, v0}, Lcom/squareup/capital/flexloan/FailureData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 111
    invoke-direct {v1, v2}, Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus$Failure;-><init>(Lcom/squareup/capital/flexloan/FailureData;)V

    check-cast v1, Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;

    return-object v1
.end method

.method private final handleSearchCustomerRequestFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/capital/external/business/service/SearchCustomersResponse;",
            ">;)",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;"
        }
    .end annotation

    .line 92
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;->errorStatusCode(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Ljava/lang/String;

    move-result-object v0

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;->errorFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 95
    new-instance v1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Failure;

    .line 96
    new-instance v2, Lcom/squareup/capital/flexloan/FailureData;

    .line 97
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v3

    .line 98
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v4

    .line 99
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getRetryable()Z

    move-result p1

    .line 96
    invoke-direct {v2, v3, v4, p1, v0}, Lcom/squareup/capital/flexloan/FailureData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 95
    invoke-direct {v1, v2}, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Failure;-><init>(Lcom/squareup/capital/flexloan/FailureData;)V

    check-cast v1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;

    return-object v1
.end method

.method private final handleSearchCustomerRequestSuccess(Ljava/util/List;)Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/external/business/models/Customer;",
            ">;)",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;"
        }
    .end annotation

    .line 69
    check-cast p1, Ljava/lang/Iterable;

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/protos/capital/external/business/models/Customer;

    iget-object v2, v2, Lcom/squareup/protos/capital/external/business/models/Customer;->partner_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;->currentUnit:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    move-object v0, v1

    :goto_0
    check-cast v0, Lcom/squareup/protos/capital/external/business/models/Customer;

    if-eqz v0, :cond_a

    .line 73
    iget-object p1, v0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_active_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    if-eqz p1, :cond_2

    iget-object p1, p1, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->plan_id:Ljava/lang/String;

    goto :goto_1

    :cond_2
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 74
    new-instance p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$CurrentPlan;

    invoke-direct {p1, v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$CurrentPlan;-><init>(Lcom/squareup/protos/capital/external/business/models/Customer;)V

    check-cast p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;

    goto :goto_4

    .line 75
    :cond_3
    iget-object p1, v0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_financing_request_in_review:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    if-eqz p1, :cond_4

    iget-object p1, p1, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;->financing_request_id:Ljava/lang/String;

    goto :goto_2

    :cond_4
    move-object p1, v1

    :goto_2
    if-eqz p1, :cond_5

    .line 76
    new-instance p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$FinancingRequest;

    invoke-direct {p1, v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$FinancingRequest;-><init>(Lcom/squareup/protos/capital/external/business/models/Customer;)V

    check-cast p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;

    goto :goto_4

    .line 77
    :cond_5
    iget-object p1, v0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_main_offer:Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    if-eqz p1, :cond_6

    iget-object p1, p1, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->offer_set_id:Ljava/lang/String;

    goto :goto_3

    :cond_6
    move-object p1, v1

    :goto_3
    if-eqz p1, :cond_7

    .line 78
    new-instance p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;

    invoke-direct {p1, v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;-><init>(Lcom/squareup/protos/capital/external/business/models/Customer;)V

    check-cast p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;

    goto :goto_4

    .line 79
    :cond_7
    iget-object p1, v0, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_latest_completed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    if-eqz p1, :cond_8

    iget-object v1, p1, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->plan_id:Ljava/lang/String;

    :cond_8
    if-eqz v1, :cond_9

    .line 80
    new-instance p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$PreviousPlan;

    invoke-direct {p1, v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$PreviousPlan;-><init>(Lcom/squareup/protos/capital/external/business/models/Customer;)V

    check-cast p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;

    goto :goto_4

    .line 81
    :cond_9
    sget-object p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$NoOffer;->INSTANCE:Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$NoOffer;

    check-cast p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;

    :goto_4
    return-object p1

    .line 70
    :cond_a
    sget-object p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$NoOffer;->INSTANCE:Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$NoOffer;

    check-cast p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;

    return-object p1
.end method


# virtual methods
.method public fetchFlexLoanStatus()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;",
            ">;"
        }
    .end annotation

    .line 49
    new-instance v0, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$Builder;-><init>()V

    .line 50
    iget-object v1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;->currentUnit:Ljava/lang/String;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$Builder;->partner_ids(Ljava/util/List;)Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$Builder;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$Builder;->build()Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;

    move-result-object v0

    const-string v1, "SearchCustomersRequest.B\u2026ntUnit))\n        .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    iget-object v1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;->capitalService:Lcom/squareup/capital/flexloan/CapitalService;

    invoke-interface {v1, v0}, Lcom/squareup/capital/flexloan/CapitalService;->searchCustomers(Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 55
    new-instance v1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore$fetchFlexLoanStatus$1;

    invoke-direct {v1, p0}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore$fetchFlexLoanStatus$1;-><init>(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "capitalService.searchCus\u2026  }\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public fetchFlexPlan(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;",
            ">;"
        }
    .end annotation

    const-string v0, "planId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanRequest$Builder;-><init>()V

    .line 35
    invoke-virtual {v0, p1}, Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanRequest$Builder;->id(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanRequest$Builder;

    move-result-object p1

    .line 36
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanRequest$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanRequest;

    move-result-object p1

    .line 38
    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;->capitalService:Lcom/squareup/capital/flexloan/CapitalService;

    const-string v1, "retrievePlanRequest"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/capital/flexloan/CapitalService;->retrievePlan(Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanRequest;)Lcom/squareup/capital/flexloan/CapitalRetrievePlanResponse;

    move-result-object p1

    .line 39
    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/CapitalRetrievePlanResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 40
    new-instance v0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore$fetchFlexPlan$1;

    invoke-direct {v0, p0}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore$fetchFlexPlan$1;-><init>(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "capitalService.retrieveP\u2026re)\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
