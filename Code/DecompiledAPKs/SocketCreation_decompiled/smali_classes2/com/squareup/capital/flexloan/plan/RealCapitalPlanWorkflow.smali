.class public final Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealCapitalPlanWorkflow.kt"

# interfaces
.implements Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$Action;,
        Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;",
        "Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealCapitalPlanWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealCapitalPlanWorkflow.kt\ncom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 4 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,198:1\n41#2:199\n56#2,2:200\n276#3:202\n149#4,5:203\n149#4,5:208\n149#4,5:213\n*E\n*S KotlinDebug\n*F\n+ 1 RealCapitalPlanWorkflow.kt\ncom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow\n*L\n75#1:199\n75#1,2:200\n75#1:202\n79#1,5:203\n109#1,5:208\n119#1,5:213\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0088\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 32\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002:\u000223B7\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u00a2\u0006\u0002\u0010\u0017J\u0018\u0010\u0018\u001a\u00020\u00052\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001aH\u0002J\u001a\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u001d\u001a\u00020\u00032\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0016J\u001e\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\"0!2\u0006\u0010\u001d\u001a\u00020\u00032\u0006\u0010#\u001a\u00020$H\u0002J\u0016\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\"0!2\u0006\u0010\u001b\u001a\u00020\u001aH\u0002J\u0016\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\"0!2\u0006\u0010\u001b\u001a\u00020\u001aH\u0002J\u0016\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020\"0!2\u0006\u0010\u001b\u001a\u00020\u001aH\u0002J\u001e\u0010(\u001a\u0008\u0012\u0004\u0012\u00020\"0!2\u0006\u0010\u001b\u001a\u00020\u001a2\u0006\u0010)\u001a\u00020\u001aH\u0002JN\u0010*\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u001d\u001a\u00020\u00032\u0006\u0010+\u001a\u00020\u00042\u0012\u0010,\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050-H\u0016J\u0010\u0010.\u001a\u00020\u001f2\u0006\u0010+\u001a\u00020\u0004H\u0016J\u000c\u0010/\u001a\u000200*\u000201H\u0002R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00064"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;",
        "Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;",
        "Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState;",
        "",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "capitalFlexLoanDataRepository",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;",
        "capitalErrorWorkflow",
        "Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;",
        "capitalPlanScreenMapper",
        "Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;",
        "browserLauncher",
        "Lcom/squareup/util/BrowserLauncher;",
        "analytics",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;",
        "multipassOtkHelper",
        "Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;",
        "(Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;)V",
        "handleOtkFetchFailure",
        "targetUrl",
        "",
        "planId",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "logViewAction",
        "Lcom/squareup/workflow/Worker;",
        "",
        "planScreenData",
        "Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;",
        "logViewCapitalActivePlan",
        "logViewCapitalClosedPlan",
        "logViewCapitalPastDuePlan",
        "logViewCapitalStateError",
        "errorCode",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "toState",
        "Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$Action;",
        "Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;",
        "Action",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ANALYTICS_KEY:Ljava/lang/String; = "CapitalPlanWorkflowAnalyticsWorker"

.field public static final Companion:Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$Companion;


# instance fields
.field private final analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final capitalErrorWorkflow:Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;

.field private final capitalFlexLoanDataRepository:Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;

.field private final capitalPlanScreenMapper:Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;

.field private final multipassOtkHelper:Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->Companion:Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "capitalFlexLoanDataRepository"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capitalErrorWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capitalPlanScreenMapper"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "browserLauncher"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "multipassOtkHelper"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->capitalFlexLoanDataRepository:Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;

    iput-object p2, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->capitalErrorWorkflow:Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;

    iput-object p3, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->capitalPlanScreenMapper:Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;

    iput-object p4, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iput-object p5, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    iput-object p6, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->multipassOtkHelper:Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;)Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getBrowserLauncher$p(Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;)Lcom/squareup/util/BrowserLauncher;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-object p0
.end method

.method public static final synthetic access$getMultipassOtkHelper$p(Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;)Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->multipassOtkHelper:Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;

    return-object p0
.end method

.method public static final synthetic access$handleOtkFetchFailure(Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1, p2}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->handleOtkFetchFailure(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$toState(Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;)Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$Action;
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->toState(Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;)Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$Action;

    move-result-object p0

    return-object p0
.end method

.method private final handleOtkFetchFailure(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .line 131
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x5

    const/4 v5, 0x0

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics$DefaultImpls;->logViewCapitalSessionBridgeError$default(Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    .line 132
    iget-object p2, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    invoke-interface {p2, p1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    return-void
.end method

.method private final logViewAction(Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;)Lcom/squareup/workflow/Worker;
    .locals 2

    .line 185
    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;->getPlanType()Lcom/squareup/capital/flexloan/CapitalFlexPlanType;

    move-result-object v0

    sget-object v1, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/capital/flexloan/CapitalFlexPlanType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 p2, 0x2

    if-ne v0, p2, :cond_0

    .line 191
    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->logViewCapitalClosedPlan(Ljava/lang/String;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 186
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->isPastDue()Z

    move-result p2

    if-eqz p2, :cond_2

    .line 187
    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->logViewCapitalPastDuePlan(Ljava/lang/String;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    goto :goto_0

    .line 189
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->logViewCapitalActivePlan(Ljava/lang/String;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final logViewCapitalActivePlan(Ljava/lang/String;)Lcom/squareup/workflow/Worker;
    .locals 3

    .line 164
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v1, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$logViewCapitalActivePlan$1;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$logViewCapitalActivePlan$1;-><init>(Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    return-object p1
.end method

.method private final logViewCapitalClosedPlan(Ljava/lang/String;)Lcom/squareup/workflow/Worker;
    .locals 3

    .line 168
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v1, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$logViewCapitalClosedPlan$1;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$logViewCapitalClosedPlan$1;-><init>(Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    return-object p1
.end method

.method private final logViewCapitalPastDuePlan(Ljava/lang/String;)Lcom/squareup/workflow/Worker;
    .locals 3

    .line 172
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v1, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$logViewCapitalPastDuePlan$1;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$logViewCapitalPastDuePlan$1;-><init>(Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    return-object p1
.end method

.method private final logViewCapitalStateError(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/workflow/Worker;
    .locals 3

    .line 179
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v1, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$logViewCapitalStateError$1;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, p2, v2}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$logViewCapitalStateError$1;-><init>(Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    return-object p1
.end method

.method private final toState(Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;)Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$Action;
    .locals 1

    .line 136
    instance-of v0, p1, Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus$PlanData;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$Action$DisplayPlanDetails;

    check-cast p1, Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus$PlanData;

    invoke-direct {v0, p1}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$Action$DisplayPlanDetails;-><init>(Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus$PlanData;)V

    check-cast v0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$Action;

    goto :goto_0

    .line 137
    :cond_0
    instance-of v0, p1, Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus$Failure;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$Action$DisplayCapitalError;

    check-cast p1, Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus$Failure;

    invoke-direct {v0, p1}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$Action$DisplayCapitalError;-><init>(Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus$Failure;)V

    check-cast v0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$Action;

    goto :goto_0

    .line 138
    :cond_1
    sget-object p1, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$Action$DoNothing;->INSTANCE:Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$Action$DoNothing;

    move-object v0, p1

    check-cast v0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$Action;

    :goto_0
    return-object v0
.end method


# virtual methods
.method public initialState(Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState;
    .locals 0

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    sget-object p1, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState$FetchingPlanDetails;->INSTANCE:Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState$FetchingPlanDetails;

    check-cast p1, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState;

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->initialState(Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;

    check-cast p2, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->render(Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;",
            "Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    .line 73
    instance-of v1, p2, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState$FetchingPlanDetails;

    const-string v2, ""

    if-eqz v1, :cond_1

    .line 75
    iget-object p2, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->capitalFlexLoanDataRepository:Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;

    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;->fetchFlexPlan(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p1

    .line 199
    sget-object p2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    const-string p2, "this.toFlowable(BUFFER)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_0

    .line 201
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 202
    const-class p2, Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v4, v1

    check-cast v4, Lcom/squareup/workflow/Worker;

    const/4 v5, 0x0

    .line 76
    new-instance p1, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$1;

    invoke-direct {p1, p0}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$1;-><init>(Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v3, p3

    .line 74
    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 78
    new-instance p1, Lcom/squareup/capital/flexloan/CapitalLoadingScreen;

    new-instance p2, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$2;

    invoke-direct {p2, v0}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$2;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-direct {p1, p2}, Lcom/squareup/capital/flexloan/CapitalLoadingScreen;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 204
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 205
    const-class p3, Lcom/squareup/capital/flexloan/CapitalLoadingScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 206
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 204
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 80
    sget-object p1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 201
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 82
    :cond_1
    instance-of v1, p2, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState$DisplayingPlanDetails;

    if-eqz v1, :cond_2

    .line 83
    iget-object v1, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->capitalPlanScreenMapper:Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;

    .line 84
    check-cast p2, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState$DisplayingPlanDetails;

    invoke-virtual {p2}, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState$DisplayingPlanDetails;->getPlanDetails()Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus$PlanData;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus$PlanData;->getPlan()Lcom/squareup/protos/capital/servicing/plan/models/Plan;

    move-result-object p2

    .line 85
    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;->getPlanType()Lcom/squareup/capital/flexloan/CapitalFlexPlanType;

    move-result-object v3

    .line 83
    invoke-virtual {v1, p2, v3}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;->mapPlanToScreenData(Lcom/squareup/protos/capital/servicing/plan/models/Plan;Lcom/squareup/capital/flexloan/CapitalFlexPlanType;)Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;

    move-result-object p2

    .line 87
    invoke-direct {p0, p1, p2}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->logViewAction(Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;)Lcom/squareup/workflow/Worker;

    move-result-object v1

    const-string v3, "CapitalPlanWorkflowAnalyticsWorker.logPlanView"

    invoke-static {p3, v1, v3}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 89
    new-instance p3, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;

    .line 91
    new-instance v1, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$3;

    invoke-direct {v1, v0}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$3;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 92
    new-instance v0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4;-><init>(Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 89
    invoke-direct {p3, p2, v1, v0}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;-><init>(Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast p3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 209
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 210
    const-class p2, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    invoke-static {p2, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 211
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 209
    invoke-direct {p1, p2, p3, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 110
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 112
    :cond_2
    instance-of v0, p2, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState$DisplayingCapitalError;

    if-eqz v0, :cond_3

    .line 114
    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;->getId()Ljava/lang/String;

    move-result-object p1

    check-cast p2, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState$DisplayingCapitalError;

    invoke-virtual {p2}, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState$DisplayingCapitalError;->getFailure()Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus$Failure;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus$Failure;->getFailureData()Lcom/squareup/capital/flexloan/FailureData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/capital/flexloan/FailureData;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->logViewCapitalStateError(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    const-string v0, "CapitalPlanWorkflowAnalyticsWorker.logPlanError"

    .line 113
    invoke-static {p3, p1, v0}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 118
    iget-object p1, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->capitalErrorWorkflow:Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;

    move-object v4, p1

    check-cast v4, Lcom/squareup/workflow/Workflow;

    invoke-virtual {p2}, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState$DisplayingCapitalError;->getFailure()Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus$Failure;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus$Failure;->getFailureData()Lcom/squareup/capital/flexloan/FailureData;

    move-result-object v5

    const/4 v6, 0x0

    sget-object p1, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$5;->INSTANCE:Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$5;

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v3, p3

    invoke-static/range {v3 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 214
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 215
    const-class p3, Lcom/squareup/capital/flexloan/error/CapitalErrorScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 216
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 214
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 120
    sget-object p1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState;

    invoke-virtual {p0, p1}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->snapshotState(Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
