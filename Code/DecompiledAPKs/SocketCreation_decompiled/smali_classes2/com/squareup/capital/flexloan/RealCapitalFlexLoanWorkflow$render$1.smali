.class final Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealCapitalFlexLoanWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->render(Lkotlin/Unit;Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;",
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action;",
        "flexStatus",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$render$1;->this$0:Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;)Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action;
    .locals 1

    const-string v0, "flexStatus"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$render$1;->this$0:Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;

    invoke-static {v0, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->access$toState(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;)Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;

    invoke-virtual {p0, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$render$1;->invoke(Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;)Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action;

    move-result-object p1

    return-object p1
.end method
