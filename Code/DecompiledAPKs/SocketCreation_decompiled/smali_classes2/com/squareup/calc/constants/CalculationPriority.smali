.class public final enum Lcom/squareup/calc/constants/CalculationPriority;
.super Ljava/lang/Enum;
.source "CalculationPriority.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/calc/constants/CalculationPriority;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/calc/constants/CalculationPriority;

.field public static final enum HIGH:Lcom/squareup/calc/constants/CalculationPriority;

.field public static final enum NORMAL:Lcom/squareup/calc/constants/CalculationPriority;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 15
    new-instance v0, Lcom/squareup/calc/constants/CalculationPriority;

    const/4 v1, 0x0

    const-string v2, "HIGH"

    invoke-direct {v0, v2, v1}, Lcom/squareup/calc/constants/CalculationPriority;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/calc/constants/CalculationPriority;->HIGH:Lcom/squareup/calc/constants/CalculationPriority;

    .line 20
    new-instance v0, Lcom/squareup/calc/constants/CalculationPriority;

    const/4 v2, 0x1

    const-string v3, "NORMAL"

    invoke-direct {v0, v3, v2}, Lcom/squareup/calc/constants/CalculationPriority;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/calc/constants/CalculationPriority;->NORMAL:Lcom/squareup/calc/constants/CalculationPriority;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/calc/constants/CalculationPriority;

    .line 10
    sget-object v3, Lcom/squareup/calc/constants/CalculationPriority;->HIGH:Lcom/squareup/calc/constants/CalculationPriority;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/calc/constants/CalculationPriority;->NORMAL:Lcom/squareup/calc/constants/CalculationPriority;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/calc/constants/CalculationPriority;->$VALUES:[Lcom/squareup/calc/constants/CalculationPriority;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/calc/constants/CalculationPriority;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/calc/constants/CalculationPriority;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/calc/constants/CalculationPriority;

    return-object p0
.end method

.method public static values()[Lcom/squareup/calc/constants/CalculationPriority;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/calc/constants/CalculationPriority;->$VALUES:[Lcom/squareup/calc/constants/CalculationPriority;

    invoke-virtual {v0}, [Lcom/squareup/calc/constants/CalculationPriority;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/calc/constants/CalculationPriority;

    return-object v0
.end method
