.class public Lcom/squareup/calc/TaxAndDiscountAdjuster;
.super Lcom/squareup/calc/Adjuster;
.source "TaxAndDiscountAdjuster.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/calc/TaxAndDiscountAdjuster$SurchargeItem;,
        Lcom/squareup/calc/TaxAndDiscountAdjuster$AdjustmentMap;,
        Lcom/squareup/calc/TaxAndDiscountAdjuster$ZeroedMap;
    }
.end annotation


# instance fields
.field private final adjustedItems:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/calc/order/Item;",
            "Lcom/squareup/calc/AdjustedItem;",
            ">;"
        }
    .end annotation
.end field

.field private appliedDiscounts:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/calc/order/Adjustment;",
            ">;"
        }
    .end annotation
.end field

.field private appliedTaxes:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/calc/order/Adjustment;",
            ">;"
        }
    .end annotation
.end field

.field private calculated:Z

.field private final collectedPerDiscount:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field private final collectedPerSurcharge:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field private final collectedPerTax:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field private final items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/calc/order/Item;",
            ">;"
        }
    .end annotation
.end field

.field private final roundingType:Lcom/squareup/calc/constants/RoundingType;

.field private final surchargeItemMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/calc/order/Surcharge;",
            "Lcom/squareup/calc/TaxAndDiscountAdjuster$SurchargeItem;",
            ">;"
        }
    .end annotation
.end field

.field private final surcharges:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/calc/order/Surcharge;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/calc/constants/RoundingType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/calc/order/Item;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/calc/order/Surcharge;",
            ">;",
            "Lcom/squareup/calc/constants/RoundingType;",
            ")V"
        }
    .end annotation

    .line 113
    invoke-direct {p0}, Lcom/squareup/calc/Adjuster;-><init>()V

    .line 114
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->items:Ljava/util/List;

    .line 115
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->surcharges:Ljava/util/List;

    .line 116
    iput-object p3, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->roundingType:Lcom/squareup/calc/constants/RoundingType;

    .line 117
    new-instance p1, Lcom/squareup/calc/TaxAndDiscountAdjuster$AdjustmentMap;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Lcom/squareup/calc/TaxAndDiscountAdjuster$AdjustmentMap;-><init>(Lcom/squareup/calc/TaxAndDiscountAdjuster$1;)V

    iput-object p1, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->adjustedItems:Ljava/util/Map;

    .line 118
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->surchargeItemMap:Ljava/util/Map;

    .line 121
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->collectedPerDiscount:Ljava/util/LinkedHashMap;

    .line 122
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->collectedPerTax:Ljava/util/LinkedHashMap;

    .line 123
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->collectedPerSurcharge:Ljava/util/LinkedHashMap;

    return-void
.end method

.method private bubbleAdjustmentAcrossItems(Lcom/squareup/calc/constants/CalculationPhase;Lcom/squareup/calc/order/Adjustment;Ljava/math/BigDecimal;Ljava/math/BigDecimal;ZLjava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/calc/constants/CalculationPhase;",
            "Lcom/squareup/calc/order/Adjustment;",
            "Ljava/math/BigDecimal;",
            "Ljava/math/BigDecimal;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/calc/order/Item;",
            ">;)V"
        }
    .end annotation

    .line 398
    invoke-interface {p6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p6

    :goto_0
    invoke-interface {p6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/calc/order/Item;

    if-nez p5, :cond_0

    .line 402
    invoke-interface {v0}, Lcom/squareup/calc/order/Item;->appliedTaxes()Ljava/util/Map;

    move-result-object v1

    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->id()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    .line 408
    :cond_0
    invoke-interface {v0}, Lcom/squareup/calc/order/Item;->appliedDiscounts()Ljava/util/Map;

    move-result-object v1

    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->id()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    .line 413
    :cond_1
    iget-object v1, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->adjustedItems:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/calc/AdjustedItem;

    .line 414
    invoke-virtual {v0, p1}, Lcom/squareup/calc/AdjustedItem;->getBasisForPhase(Lcom/squareup/calc/constants/CalculationPhase;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 417
    sget-object v2, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-static {p3, v2}, Lcom/squareup/calc/util/AdjustmentComparator;->equalByCompare(Ljava/lang/Comparable;Ljava/lang/Comparable;)Z

    move-result v2

    if-nez v2, :cond_3

    sget-object v2, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-static {v1, v2}, Lcom/squareup/calc/util/AdjustmentComparator;->equalByCompare(Ljava/lang/Comparable;Ljava/lang/Comparable;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_1

    .line 421
    :cond_2
    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v1, p3, v2}, Lcom/squareup/calc/util/BigDecimalHelper;->divide(Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 422
    invoke-virtual {v2, p4}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    sget-object v3, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v2, v3}, Lcom/squareup/calc/util/BigDecimalHelper;->round(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v2

    goto :goto_2

    .line 418
    :cond_3
    :goto_1
    sget-object v2, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    :goto_2
    if-eqz p5, :cond_4

    .line 427
    invoke-virtual {v2}, Ljava/math/BigDecimal;->negate()Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v0, p2, v3}, Lcom/squareup/calc/AdjustedItem;->recordAdjustment(Lcom/squareup/calc/order/Adjustment;Ljava/math/BigDecimal;)V

    goto :goto_3

    .line 429
    :cond_4
    invoke-virtual {v0, p2, v2}, Lcom/squareup/calc/AdjustedItem;->recordAdjustment(Lcom/squareup/calc/order/Adjustment;Ljava/math/BigDecimal;)V

    .line 433
    :goto_3
    invoke-virtual {p4, v2}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p4

    .line 435
    invoke-virtual {p3, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p3

    goto :goto_0

    :cond_5
    return-void
.end method

.method private calculateDiscounts(Lcom/squareup/calc/constants/CalculationPhase;Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/calc/constants/CalculationPhase;",
            "Ljava/util/List<",
            "Lcom/squareup/calc/order/Item;",
            ">;)V"
        }
    .end annotation

    .line 241
    new-instance v0, Lcom/squareup/calc/TaxAndDiscountAdjuster$ZeroedMap;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/calc/TaxAndDiscountAdjuster$ZeroedMap;-><init>(Lcom/squareup/calc/TaxAndDiscountAdjuster$1;)V

    .line 243
    iget-object v1, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->appliedDiscounts:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v5, v2

    check-cast v5, Lcom/squareup/calc/order/Adjustment;

    .line 244
    invoke-interface {v5}, Lcom/squareup/calc/order/Adjustment;->phase()Lcom/squareup/calc/constants/CalculationPhase;

    move-result-object v2

    if-eq v2, p1, :cond_0

    goto :goto_0

    .line 248
    :cond_0
    invoke-interface {v5}, Lcom/squareup/calc/order/Adjustment;->amountAppliesPerItemQuantity()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 249
    invoke-direct {p0, p1, v5, p2}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->collectItemizedDiscount(Lcom/squareup/calc/constants/CalculationPhase;Lcom/squareup/calc/order/Adjustment;Ljava/util/List;)V

    goto :goto_0

    .line 253
    :cond_1
    invoke-interface {v5}, Lcom/squareup/calc/order/Adjustment;->id()Ljava/lang/String;

    move-result-object v2

    .line 254
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/calc/order/Item;

    .line 255
    invoke-interface {v4}, Lcom/squareup/calc/order/Item;->appliedDiscounts()Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 256
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/math/BigDecimal;

    iget-object v7, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->adjustedItems:Ljava/util/Map;

    invoke-interface {v7, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/calc/AdjustedItem;

    invoke-virtual {v4, p1}, Lcom/squareup/calc/AdjustedItem;->getBasisForPhase(Lcom/squareup/calc/constants/CalculationPhase;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-interface {v0, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 261
    :cond_3
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Ljava/math/BigDecimal;

    const/4 v7, 0x1

    move-object v3, p0

    move-object v4, p1

    move-object v8, p2

    invoke-direct/range {v3 .. v8}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->collectAdjustment(Lcom/squareup/calc/constants/CalculationPhase;Lcom/squareup/calc/order/Adjustment;Ljava/math/BigDecimal;ZLjava/util/List;)Ljava/math/BigDecimal;

    move-result-object v3

    .line 262
    iget-object v4, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->collectedPerDiscount:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/math/BigDecimal;->negate()Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_4
    return-void
.end method

.method private calculateSurcharges(Lcom/squareup/calc/constants/CalculationPhase;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/calc/constants/CalculationPhase;",
            "Ljava/util/List<",
            "Lcom/squareup/calc/order/Item;",
            ">;)V"
        }
    .end annotation

    .line 335
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 336
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/calc/order/Item;

    .line 337
    iget-object v3, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->adjustedItems:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/calc/AdjustedItem;

    .line 338
    invoke-virtual {v2, p1}, Lcom/squareup/calc/AdjustedItem;->getBasisForPhase(Lcom/squareup/calc/constants/CalculationPhase;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0

    .line 341
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->getSurcharges()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/calc/order/Surcharge;

    .line 342
    invoke-interface {v2}, Lcom/squareup/calc/order/Surcharge;->phase()Lcom/squareup/calc/constants/CalculationPhase;

    move-result-object v3

    if-eq v3, p1, :cond_1

    goto :goto_1

    .line 346
    :cond_1
    invoke-interface {v2}, Lcom/squareup/calc/order/Surcharge;->percentage()Ljava/math/BigDecimal;

    move-result-object v3

    if-nez v3, :cond_2

    .line 347
    new-instance v3, Ljava/math/BigDecimal;

    invoke-interface {v2}, Lcom/squareup/calc/order/Surcharge;->amount()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/math/BigDecimal;-><init>(J)V

    goto :goto_2

    .line 349
    :cond_2
    invoke-interface {v2}, Lcom/squareup/calc/order/Surcharge;->percentage()Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v3

    .line 351
    :goto_2
    invoke-direct {p0, v2, v3, p2}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->collectSurcharge(Lcom/squareup/calc/order/Surcharge;Ljava/math/BigDecimal;Ljava/util/List;)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method private calculateTaxes(Lcom/squareup/calc/constants/CalculationPhase;Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/calc/constants/CalculationPhase;",
            "Ljava/util/List<",
            "Lcom/squareup/calc/order/Item;",
            ">;)V"
        }
    .end annotation

    .line 304
    new-instance v0, Lcom/squareup/calc/TaxAndDiscountAdjuster$ZeroedMap;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/calc/TaxAndDiscountAdjuster$ZeroedMap;-><init>(Lcom/squareup/calc/TaxAndDiscountAdjuster$1;)V

    .line 306
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/calc/order/Item;

    .line 307
    invoke-interface {v2}, Lcom/squareup/calc/order/Item;->baseAmount()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_1

    goto :goto_0

    .line 311
    :cond_1
    invoke-interface {v2}, Lcom/squareup/calc/order/Item;->appliedTaxes()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/calc/order/Adjustment;

    .line 312
    invoke-interface {v4}, Lcom/squareup/calc/order/Adjustment;->phase()Lcom/squareup/calc/constants/CalculationPhase;

    move-result-object v5

    if-eq v5, p1, :cond_2

    goto :goto_1

    .line 316
    :cond_2
    iget-object v5, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->adjustedItems:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/calc/AdjustedItem;

    .line 317
    invoke-interface {v4}, Lcom/squareup/calc/order/Adjustment;->id()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4}, Lcom/squareup/calc/order/Adjustment;->id()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/math/BigDecimal;

    invoke-virtual {v5, p1}, Lcom/squareup/calc/AdjustedItem;->getBasisForPhase(Lcom/squareup/calc/constants/CalculationPhase;)Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-interface {v0, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 322
    :cond_3
    iget-object v1, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->appliedTaxes:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/calc/order/Adjustment;

    .line 323
    invoke-interface {v2}, Lcom/squareup/calc/order/Adjustment;->phase()Lcom/squareup/calc/constants/CalculationPhase;

    move-result-object v3

    if-eq v3, p1, :cond_4

    goto :goto_2

    .line 327
    :cond_4
    invoke-interface {v2}, Lcom/squareup/calc/order/Adjustment;->id()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Ljava/math/BigDecimal;

    const/4 v7, 0x0

    move-object v3, p0

    move-object v4, p1

    move-object v5, v2

    move-object v8, p2

    .line 328
    invoke-direct/range {v3 .. v8}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->collectAdjustment(Lcom/squareup/calc/constants/CalculationPhase;Lcom/squareup/calc/order/Adjustment;Ljava/math/BigDecimal;ZLjava/util/List;)Ljava/math/BigDecimal;

    move-result-object v3

    .line 329
    iget-object v4, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->collectedPerTax:Ljava/util/LinkedHashMap;

    invoke-interface {v2}, Lcom/squareup/calc/order/Adjustment;->id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_5
    return-void
.end method

.method private collectAdjustment(Lcom/squareup/calc/constants/CalculationPhase;Lcom/squareup/calc/order/Adjustment;Ljava/math/BigDecimal;ZLjava/util/List;)Ljava/math/BigDecimal;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/calc/constants/CalculationPhase;",
            "Lcom/squareup/calc/order/Adjustment;",
            "Ljava/math/BigDecimal;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/calc/order/Item;",
            ">;)",
            "Ljava/math/BigDecimal;"
        }
    .end annotation

    .line 363
    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->rate()Ljava/math/BigDecimal;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 364
    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->rate()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 365
    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->amount()Ljava/lang/Long;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 367
    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->amount()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/squareup/calc/util/BigDecimalHelper;->newBigDecimal(J)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->min(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0

    .line 370
    :cond_0
    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->amount()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/calc/util/BigDecimalHelper;->newBigDecimal(J)Ljava/math/BigDecimal;

    move-result-object v0

    .line 374
    :cond_1
    :goto_0
    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->maxAmount()Ljava/lang/Long;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 375
    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->maxAmount()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/squareup/calc/util/BigDecimalHelper;->newBigDecimal(J)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->min(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 379
    :cond_2
    invoke-virtual {v0, p3}, Ljava/math/BigDecimal;->min(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 382
    invoke-direct {p0, v0}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->round(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, v0

    move v6, p4

    move-object v7, p5

    .line 385
    invoke-direct/range {v1 .. v7}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->bubbleAdjustmentAcrossItems(Lcom/squareup/calc/constants/CalculationPhase;Lcom/squareup/calc/order/Adjustment;Ljava/math/BigDecimal;Ljava/math/BigDecimal;ZLjava/util/List;)V

    return-object v0
.end method

.method private collectItemizedDiscount(Lcom/squareup/calc/constants/CalculationPhase;Lcom/squareup/calc/order/Adjustment;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/calc/constants/CalculationPhase;",
            "Lcom/squareup/calc/order/Adjustment;",
            "Ljava/util/List<",
            "Lcom/squareup/calc/order/Item;",
            ">;)V"
        }
    .end annotation

    .line 269
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 271
    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->id()Ljava/lang/String;

    move-result-object v1

    .line 272
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_0
    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/calc/order/Item;

    .line 273
    invoke-interface {v2}, Lcom/squareup/calc/order/Item;->appliedDiscounts()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 274
    iget-object v3, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->adjustedItems:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/calc/AdjustedItem;

    .line 275
    invoke-direct {p0, p2}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->isFlatDiscount(Lcom/squareup/calc/order/Adjustment;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 276
    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->amount()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/squareup/calc/util/BigDecimalHelper;->newBigDecimal(J)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-interface {v2}, Lcom/squareup/calc/order/Item;->quantity()Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    goto :goto_1

    .line 277
    :cond_1
    invoke-virtual {v3, p1}, Lcom/squareup/calc/AdjustedItem;->getBasisForPhase(Lcom/squareup/calc/constants/CalculationPhase;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->rate()Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    .line 280
    :goto_1
    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->maxAmount()Ljava/lang/Long;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 282
    invoke-interface {p2}, Lcom/squareup/calc/order/Adjustment;->maxAmount()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/squareup/calc/util/BigDecimalHelper;->newBigDecimal(J)Ljava/math/BigDecimal;

    move-result-object v5

    invoke-interface {v2}, Lcom/squareup/calc/order/Item;->quantity()Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 281
    invoke-virtual {v4, v2}, Ljava/math/BigDecimal;->min(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    .line 288
    :cond_2
    invoke-virtual {v3, p1}, Lcom/squareup/calc/AdjustedItem;->getBasisForPhase(Lcom/squareup/calc/constants/CalculationPhase;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/math/BigDecimal;->min(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    sget-object v4, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v2, v4}, Ljava/math/BigDecimal;->max(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 291
    invoke-direct {p0, v2}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->round(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 294
    invoke-virtual {v2}, Ljava/math/BigDecimal;->negate()Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v3, p2, v4}, Lcom/squareup/calc/AdjustedItem;->recordAdjustment(Lcom/squareup/calc/order/Adjustment;Ljava/math/BigDecimal;)V

    .line 295
    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    goto/16 :goto_0

    .line 299
    :cond_3
    iget-object p1, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->collectedPerDiscount:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->negate()Ljava/math/BigDecimal;

    move-result-object p2

    invoke-virtual {p1, v1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private collectSurcharge(Lcom/squareup/calc/order/Surcharge;Ljava/math/BigDecimal;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/calc/order/Surcharge;",
            "Ljava/math/BigDecimal;",
            "Ljava/util/List<",
            "Lcom/squareup/calc/order/Item;",
            ">;)V"
        }
    .end annotation

    .line 441
    invoke-direct {p0, p2}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->round(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p2

    .line 442
    new-instance v0, Lcom/squareup/calc/TaxAndDiscountAdjuster$SurchargeItem;

    invoke-virtual {p2}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v1

    const/4 v3, 0x0

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/squareup/calc/TaxAndDiscountAdjuster$SurchargeItem;-><init>(Lcom/squareup/calc/order/Surcharge;JLcom/squareup/calc/TaxAndDiscountAdjuster$1;)V

    .line 444
    iget-object v1, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->collectedPerSurcharge:Ljava/util/LinkedHashMap;

    invoke-interface {p1}, Lcom/squareup/calc/order/Surcharge;->id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 445
    iget-object p2, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->surchargeItemMap:Ljava/util/Map;

    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 446
    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private collectTaxesAndDiscounts()V
    .locals 4

    .line 224
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->appliedTaxes:Ljava/util/LinkedHashMap;

    .line 225
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->appliedDiscounts:Ljava/util/LinkedHashMap;

    .line 227
    invoke-virtual {p0}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/calc/order/Item;

    .line 228
    iget-object v2, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->appliedTaxes:Ljava/util/LinkedHashMap;

    invoke-interface {v1}, Lcom/squareup/calc/order/Item;->appliedTaxes()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/LinkedHashMap;->putAll(Ljava/util/Map;)V

    .line 229
    iget-object v2, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->appliedDiscounts:Ljava/util/LinkedHashMap;

    invoke-interface {v1}, Lcom/squareup/calc/order/Item;->appliedDiscounts()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->putAll(Ljava/util/Map;)V

    goto :goto_0

    .line 231
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->getSurcharges()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/calc/order/Surcharge;

    .line 232
    iget-object v2, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->appliedTaxes:Ljava/util/LinkedHashMap;

    invoke-interface {v1}, Lcom/squareup/calc/order/Surcharge;->appliedTaxes()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->putAll(Ljava/util/Map;)V

    goto :goto_1

    .line 235
    :cond_1
    iget-object v0, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->appliedTaxes:Ljava/util/LinkedHashMap;

    invoke-static {v0}, Lcom/squareup/calc/util/AdjustmentComparator;->sortToCalculationOrder(Ljava/util/Map;)Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->appliedTaxes:Ljava/util/LinkedHashMap;

    .line 236
    iget-object v0, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->appliedDiscounts:Ljava/util/LinkedHashMap;

    invoke-static {v0}, Lcom/squareup/calc/util/AdjustmentComparator;->sortToCalculationOrder(Ljava/util/Map;)Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->appliedDiscounts:Ljava/util/LinkedHashMap;

    return-void
.end method

.method private isFlatDiscount(Lcom/squareup/calc/order/Adjustment;)Z
    .locals 1

    .line 461
    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->rate()Ljava/math/BigDecimal;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->amount()Ljava/lang/Long;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private round(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;
    .locals 2

    .line 450
    sget-object v0, Lcom/squareup/calc/TaxAndDiscountAdjuster$1;->$SwitchMap$com$squareup$calc$constants$RoundingType:[I

    iget-object v1, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->roundingType:Lcom/squareup/calc/constants/RoundingType;

    invoke-virtual {v1}, Lcom/squareup/calc/constants/RoundingType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 454
    sget-object v0, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    invoke-static {p1, v0}, Lcom/squareup/calc/util/BigDecimalHelper;->round(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object p1

    return-object p1

    .line 456
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unsupported rounding type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->roundingType:Lcom/squareup/calc/constants/RoundingType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 452
    :cond_1
    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {p1, v0}, Lcom/squareup/calc/util/BigDecimalHelper;->round(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public calculate()V
    .locals 2

    .line 156
    iget-boolean v0, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->calculated:Z

    if-eqz v0, :cond_0

    return-void

    .line 164
    :cond_0
    new-instance v0, Ljava/util/LinkedList;

    invoke-virtual {p0}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 166
    invoke-direct {p0}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->collectTaxesAndDiscounts()V

    .line 167
    sget-object v1, Lcom/squareup/calc/constants/CalculationPhase;->DISCOUNT_PERCENTAGE_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    invoke-direct {p0, v1, v0}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->calculateDiscounts(Lcom/squareup/calc/constants/CalculationPhase;Ljava/util/List;)V

    .line 168
    sget-object v1, Lcom/squareup/calc/constants/CalculationPhase;->DISCOUNT_AMOUNT_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    invoke-direct {p0, v1, v0}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->calculateDiscounts(Lcom/squareup/calc/constants/CalculationPhase;Ljava/util/List;)V

    .line 169
    sget-object v1, Lcom/squareup/calc/constants/CalculationPhase;->SURCHARGE_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    invoke-direct {p0, v1, v0}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->calculateSurcharges(Lcom/squareup/calc/constants/CalculationPhase;Ljava/util/List;)V

    .line 170
    sget-object v1, Lcom/squareup/calc/constants/CalculationPhase;->FEE_SUBTOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    invoke-direct {p0, v1, v0}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->calculateTaxes(Lcom/squareup/calc/constants/CalculationPhase;Ljava/util/List;)V

    .line 171
    sget-object v1, Lcom/squareup/calc/constants/CalculationPhase;->FEE_TOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    invoke-direct {p0, v1, v0}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->calculateTaxes(Lcom/squareup/calc/constants/CalculationPhase;Ljava/util/List;)V

    .line 172
    sget-object v1, Lcom/squareup/calc/constants/CalculationPhase;->SURCHARGE_TOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    invoke-direct {p0, v1, v0}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->calculateSurcharges(Lcom/squareup/calc/constants/CalculationPhase;Ljava/util/List;)V

    .line 174
    iget-object v0, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->adjustedItems:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 175
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/calc/AdjustedItem;

    invoke-virtual {v1}, Lcom/squareup/calc/AdjustedItem;->roundPreFeeAmount()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    .line 178
    iput-boolean v0, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->calculated:Z

    return-void
.end method

.method public getAdjustedItems()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/calc/order/Item;",
            "Lcom/squareup/calc/AdjustedItem;",
            ">;"
        }
    .end annotation

    .line 186
    invoke-virtual {p0}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->calculate()V

    .line 187
    iget-object v0, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->adjustedItems:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getAppliedDiscounts()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/calc/order/Adjustment;",
            ">;"
        }
    .end annotation

    .line 191
    invoke-virtual {p0}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->calculate()V

    .line 192
    iget-object v0, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->appliedDiscounts:Ljava/util/LinkedHashMap;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getAppliedTaxes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/calc/order/Adjustment;",
            ">;"
        }
    .end annotation

    .line 201
    invoke-virtual {p0}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->calculate()V

    .line 202
    iget-object v0, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->appliedTaxes:Ljava/util/LinkedHashMap;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getCollectedAmountPerDiscount()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation

    .line 196
    invoke-virtual {p0}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->calculate()V

    .line 197
    iget-object v0, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->collectedPerDiscount:Ljava/util/LinkedHashMap;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getCollectedAmountPerSurcharge()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation

    .line 219
    invoke-virtual {p0}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->calculate()V

    .line 220
    iget-object v0, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->collectedPerSurcharge:Ljava/util/LinkedHashMap;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getCollectedAmountPerTax()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation

    .line 206
    invoke-virtual {p0}, Lcom/squareup/calc/TaxAndDiscountAdjuster;->calculate()V

    .line 207
    iget-object v0, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->collectedPerTax:Ljava/util/LinkedHashMap;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/calc/order/Item;",
            ">;"
        }
    .end annotation

    .line 182
    iget-object v0, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->items:Ljava/util/List;

    return-object v0
.end method

.method public getSurchargeItemFor(Lcom/squareup/calc/order/Surcharge;)Lcom/squareup/calc/order/Item;
    .locals 1

    .line 211
    iget-object v0, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->surchargeItemMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/calc/order/Item;

    return-object p1
.end method

.method public getSurcharges()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/calc/order/Surcharge;",
            ">;"
        }
    .end annotation

    .line 215
    iget-object v0, p0, Lcom/squareup/calc/TaxAndDiscountAdjuster;->surcharges:Ljava/util/List;

    return-object v0
.end method
