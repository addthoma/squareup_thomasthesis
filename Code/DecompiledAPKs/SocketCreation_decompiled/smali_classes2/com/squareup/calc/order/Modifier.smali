.class public interface abstract Lcom/squareup/calc/order/Modifier;
.super Ljava/lang/Object;
.source "Modifier.java"


# virtual methods
.method public abstract id()Ljava/lang/String;
.end method

.method public abstract price()J
.end method

.method public abstract quantity()I
.end method

.method public abstract quantityTimesItemQuantity(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;
.end method

.method public abstract totalPrice(Ljava/math/BigDecimal;)J
.end method
