.class public interface abstract Lcom/squareup/calc/order/Surcharge;
.super Ljava/lang/Object;
.source "Surcharge.java"


# virtual methods
.method public abstract amount()Ljava/lang/Long;
.end method

.method public abstract appliedTaxes()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/calc/order/Adjustment;",
            ">;"
        }
    .end annotation
.end method

.method public abstract id()Ljava/lang/String;
.end method

.method public abstract percentage()Ljava/math/BigDecimal;
.end method

.method public abstract phase()Lcom/squareup/calc/constants/CalculationPhase;
.end method
