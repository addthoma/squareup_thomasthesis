.class final Lcom/squareup/BundleKey$2;
.super Lcom/squareup/BundleKey;
.source "BundleKey.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/BundleKey<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic val$gson:Lcom/google/gson/Gson;

.field final synthetic val$type:Ljava/lang/Class;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/Class;)V
    .locals 0

    .line 92
    iput-object p2, p0, Lcom/squareup/BundleKey$2;->val$gson:Lcom/google/gson/Gson;

    iput-object p3, p0, Lcom/squareup/BundleKey$2;->val$type:Ljava/lang/Class;

    const/4 p2, 0x0

    invoke-direct {p0, p1, p2}, Lcom/squareup/BundleKey;-><init>(Ljava/lang/String;Lcom/squareup/BundleKey$1;)V

    return-void
.end method


# virtual methods
.method public get(Landroid/content/Intent;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")TT;"
        }
    .end annotation

    .line 99
    iget-object v0, p0, Lcom/squareup/BundleKey$2;->key:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 100
    iget-object v0, p0, Lcom/squareup/BundleKey$2;->val$gson:Lcom/google/gson/Gson;

    iget-object v1, p0, Lcom/squareup/BundleKey$2;->val$type:Ljava/lang/Class;

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public get(Landroid/os/Bundle;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            ")TT;"
        }
    .end annotation

    .line 94
    iget-object v0, p0, Lcom/squareup/BundleKey$2;->key:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 95
    iget-object v0, p0, Lcom/squareup/BundleKey$2;->val$gson:Lcom/google/gson/Gson;

    iget-object v1, p0, Lcom/squareup/BundleKey$2;->val$type:Ljava/lang/Class;

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public put(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "TT;)V"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 111
    iget-object v0, p0, Lcom/squareup/BundleKey$2;->key:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/BundleKey$2;->val$gson:Lcom/google/gson/Gson;

    iget-object v2, p0, Lcom/squareup/BundleKey$2;->val$type:Ljava/lang/Class;

    invoke-virtual {v1, p2, v2}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method public put(Landroid/os/Bundle;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "TT;)V"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 105
    iget-object v0, p0, Lcom/squareup/BundleKey$2;->key:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/BundleKey$2;->val$gson:Lcom/google/gson/Gson;

    iget-object v2, p0, Lcom/squareup/BundleKey$2;->val$type:Ljava/lang/Class;

    invoke-virtual {v1, p2, v2}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
