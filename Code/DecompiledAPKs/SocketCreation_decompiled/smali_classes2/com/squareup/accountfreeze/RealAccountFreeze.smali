.class public final Lcom/squareup/accountfreeze/RealAccountFreeze;
.super Ljava/lang/Object;
.source "RealAccountFreeze.kt"

# interfaces
.implements Lcom/squareup/accountfreeze/AccountFreeze;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/accountfreeze/RealAccountFreeze$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealAccountFreeze.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealAccountFreeze.kt\ncom/squareup/accountfreeze/RealAccountFreeze\n+ 2 RxKotlin.kt\ncom/squareup/util/rx2/Observables\n*L\n1#1,107:1\n57#2,4:108\n148#2,4:112\n57#2,4:116\n*E\n*S KotlinDebug\n*F\n+ 1 RealAccountFreeze.kt\ncom/squareup/accountfreeze/RealAccountFreeze\n*L\n57#1,4:108\n71#1,4:112\n83#1,4:116\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u0000  2\u00020\u0001:\u0001 BE\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u000e\u0008\u0001\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e\u00a2\u0006\u0002\u0010\u0010J\u000e\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u0017H\u0016J\u000e\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u0017H\u0016J\u000e\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u0017H\u0016J\u000e\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u0017H\u0016J\u0008\u0010\u001c\u001a\u00020\u0018H\u0016J\u0008\u0010\u001d\u001a\u00020\u001eH\u0016J\u000c\u0010\u001f\u001a\u00020\u0018*\u00020\u000fH\u0002R\u001b\u0010\u0011\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0014\u0010\u0015\u001a\u0004\u0008\u0012\u0010\u0013R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/accountfreeze/RealAccountFreeze;",
        "Lcom/squareup/accountfreeze/AccountFreeze;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "systemPermissionsPresenter",
        "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
        "lazyBalanceAppletGateway",
        "Ldagger/Lazy;",
        "Lcom/squareup/ui/balance/BalanceAppletGateway;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "lastDismissedBanner",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Ldagger/Lazy;Lcom/squareup/util/Clock;Lcom/f2prateek/rx/preferences2/Preference;)V",
        "balanceAppletGateway",
        "getBalanceAppletGateway",
        "()Lcom/squareup/ui/balance/BalanceAppletGateway;",
        "balanceAppletGateway$delegate",
        "Ldagger/Lazy;",
        "canShowBadge",
        "Lio/reactivex/Observable;",
        "",
        "canShowBanner",
        "canShowNotification",
        "frozen",
        "isFrozen",
        "markBannerDismissed",
        "",
        "moreThanADayAgo",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field private static final CHECK_INTERVAL_MINUTES:J = 0x1L

.field public static final Companion:Lcom/squareup/accountfreeze/RealAccountFreeze$Companion;

.field private static final NOTIFICATION_INTERVAL:J

.field private static final NOTIFICATION_WINDOW:J


# instance fields
.field private final balanceAppletGateway$delegate:Ldagger/Lazy;

.field private final clock:Lcom/squareup/util/Clock;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final lastDismissedBanner:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final systemPermissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    const-class v2, Lcom/squareup/accountfreeze/RealAccountFreeze;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "balanceAppletGateway"

    const-string v4, "getBalanceAppletGateway()Lcom/squareup/ui/balance/BalanceAppletGateway;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/accountfreeze/RealAccountFreeze;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/squareup/accountfreeze/RealAccountFreeze$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/accountfreeze/RealAccountFreeze$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/accountfreeze/RealAccountFreeze;->Companion:Lcom/squareup/accountfreeze/RealAccountFreeze$Companion;

    .line 98
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v3

    sput-wide v3, Lcom/squareup/accountfreeze/RealAccountFreeze;->NOTIFICATION_INTERVAL:J

    .line 102
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/squareup/accountfreeze/RealAccountFreeze;->NOTIFICATION_WINDOW:J

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Ldagger/Lazy;Lcom/squareup/util/Clock;Lcom/f2prateek/rx/preferences2/Preference;)V
    .locals 1
    .param p6    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/accountfreeze/LastDismissedFreezeBanner;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/balance/BalanceAppletGateway;",
            ">;",
            "Lcom/squareup/util/Clock;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "settings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "systemPermissionsPresenter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lazyBalanceAppletGateway"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lastDismissedBanner"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/accountfreeze/RealAccountFreeze;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p2, p0, Lcom/squareup/accountfreeze/RealAccountFreeze;->features:Lcom/squareup/settings/server/Features;

    iput-object p3, p0, Lcom/squareup/accountfreeze/RealAccountFreeze;->systemPermissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    iput-object p5, p0, Lcom/squareup/accountfreeze/RealAccountFreeze;->clock:Lcom/squareup/util/Clock;

    iput-object p6, p0, Lcom/squareup/accountfreeze/RealAccountFreeze;->lastDismissedBanner:Lcom/f2prateek/rx/preferences2/Preference;

    .line 35
    iput-object p4, p0, Lcom/squareup/accountfreeze/RealAccountFreeze;->balanceAppletGateway$delegate:Ldagger/Lazy;

    return-void
.end method

.method public static final synthetic access$getNOTIFICATION_INTERVAL$cp()J
    .locals 2

    .line 24
    sget-wide v0, Lcom/squareup/accountfreeze/RealAccountFreeze;->NOTIFICATION_INTERVAL:J

    return-wide v0
.end method

.method public static final synthetic access$getNOTIFICATION_WINDOW$cp()J
    .locals 2

    .line 24
    sget-wide v0, Lcom/squareup/accountfreeze/RealAccountFreeze;->NOTIFICATION_WINDOW:J

    return-wide v0
.end method

.method public static final synthetic access$getSystemPermissionsPresenter$p(Lcom/squareup/accountfreeze/RealAccountFreeze;)Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/accountfreeze/RealAccountFreeze;->systemPermissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    return-object p0
.end method

.method public static final synthetic access$moreThanADayAgo(Lcom/squareup/accountfreeze/RealAccountFreeze;J)Z
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Lcom/squareup/accountfreeze/RealAccountFreeze;->moreThanADayAgo(J)Z

    move-result p0

    return p0
.end method

.method private final getBalanceAppletGateway()Lcom/squareup/ui/balance/BalanceAppletGateway;
    .locals 3

    iget-object v0, p0, Lcom/squareup/accountfreeze/RealAccountFreeze;->balanceAppletGateway$delegate:Ldagger/Lazy;

    sget-object v1, Lcom/squareup/accountfreeze/RealAccountFreeze;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/dagger/LazysKt;->getValue(Ldagger/Lazy;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/balance/BalanceAppletGateway;

    return-object v0
.end method

.method private final moreThanADayAgo(J)Z
    .locals 3

    .line 94
    iget-object v0, p0, Lcom/squareup/accountfreeze/RealAccountFreeze;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p1

    sget-wide p1, Lcom/squareup/accountfreeze/RealAccountFreeze;->NOTIFICATION_INTERVAL:J

    cmp-long v2, v0, p1

    if-lez v2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public canShowBadge()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 57
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    invoke-virtual {p0}, Lcom/squareup/accountfreeze/RealAccountFreeze;->frozen()Lio/reactivex/Observable;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/accountfreeze/RealAccountFreeze;->getBalanceAppletGateway()Lcom/squareup/ui/balance/BalanceAppletGateway;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/ui/balance/BalanceAppletGateway;->hasPermissionToActivate()Lio/reactivex/Observable;

    move-result-object v1

    .line 109
    check-cast v0, Lio/reactivex/ObservableSource;

    check-cast v1, Lio/reactivex/ObservableSource;

    .line 110
    new-instance v2, Lcom/squareup/accountfreeze/RealAccountFreeze$canShowBadge$$inlined$combineLatest$1;

    invoke-direct {v2}, Lcom/squareup/accountfreeze/RealAccountFreeze$canShowBadge$$inlined$combineLatest$1;-><init>()V

    check-cast v2, Lio/reactivex/functions/BiFunction;

    .line 108
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026ineFunction(t1, t2) }\n  )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public canShowBanner()Lio/reactivex/Observable;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/squareup/accountfreeze/RealAccountFreeze;->features:Lcom/squareup/settings/server/Features;

    .line 62
    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->IGNORE_SYSTEM_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v0

    .line 63
    new-instance v1, Lcom/squareup/accountfreeze/RealAccountFreeze$canShowBanner$hasRequiredSystemPermissions$1;

    invoke-direct {v1, p0}, Lcom/squareup/accountfreeze/RealAccountFreeze$canShowBanner$hasRequiredSystemPermissions$1;-><init>(Lcom/squareup/accountfreeze/RealAccountFreeze;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 71
    sget-object v1, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 72
    invoke-virtual {p0}, Lcom/squareup/accountfreeze/RealAccountFreeze;->frozen()Lio/reactivex/Observable;

    move-result-object v1

    .line 73
    iget-object v2, p0, Lcom/squareup/accountfreeze/RealAccountFreeze;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->ACCOUNT_FREEZE_BANNER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v2

    const-string v3, "features.featureEnabled(ACCOUNT_FREEZE_BANNER)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "hasRequiredSystemPermissions"

    .line 74
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    iget-object v3, p0, Lcom/squareup/accountfreeze/RealAccountFreeze;->lastDismissedBanner:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v3}, Lcom/f2prateek/rx/preferences2/Preference;->asObservable()Lio/reactivex/Observable;

    move-result-object v3

    const-string v4, "lastDismissedBanner.asObservable()"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v4, 0x0

    .line 76
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v4}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v4

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0x1

    invoke-static {v6, v7, v5}, Lio/reactivex/Observable;->interval(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v5

    check-cast v5, Lio/reactivex/ObservableSource;

    invoke-virtual {v4, v5}, Lio/reactivex/Observable;->concatWith(Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v4

    const-string v5, "just(0L).concatWith(inte\u2026TERVAL_MINUTES, MINUTES))"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    move-object v6, v1

    check-cast v6, Lio/reactivex/ObservableSource;

    move-object v7, v2

    check-cast v7, Lio/reactivex/ObservableSource;

    move-object v8, v0

    check-cast v8, Lio/reactivex/ObservableSource;

    move-object v9, v3

    check-cast v9, Lio/reactivex/ObservableSource;

    move-object v10, v4

    check-cast v10, Lio/reactivex/ObservableSource;

    .line 114
    new-instance v0, Lcom/squareup/accountfreeze/RealAccountFreeze$canShowBanner$$inlined$combineLatest$1;

    invoke-direct {v0, p0}, Lcom/squareup/accountfreeze/RealAccountFreeze$canShowBanner$$inlined$combineLatest$1;-><init>(Lcom/squareup/accountfreeze/RealAccountFreeze;)V

    move-object v11, v0

    check-cast v11, Lio/reactivex/functions/Function5;

    .line 112
    invoke-static/range {v6 .. v11}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function5;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026t1, t2, t3, t4, t5) }\n  )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public canShowNotification()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 83
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 84
    invoke-virtual {p0}, Lcom/squareup/accountfreeze/RealAccountFreeze;->frozen()Lio/reactivex/Observable;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/squareup/accountfreeze/RealAccountFreeze;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->ACCOUNT_FREEZE_NOTIFICATIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "features.featureEnabled(\u2026UNT_FREEZE_NOTIFICATIONS)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    check-cast v0, Lio/reactivex/ObservableSource;

    check-cast v1, Lio/reactivex/ObservableSource;

    .line 118
    new-instance v2, Lcom/squareup/accountfreeze/RealAccountFreeze$canShowNotification$$inlined$combineLatest$1;

    invoke-direct {v2}, Lcom/squareup/accountfreeze/RealAccountFreeze$canShowNotification$$inlined$combineLatest$1;-><init>()V

    check-cast v2, Lio/reactivex/functions/BiFunction;

    .line 116
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026ineFunction(t1, t2) }\n  )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public frozen()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/squareup/accountfreeze/RealAccountFreeze;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailableRx2()Lio/reactivex/Observable;

    move-result-object v0

    .line 53
    new-instance v1, Lcom/squareup/accountfreeze/RealAccountFreeze$frozen$1;

    invoke-direct {v1, p0}, Lcom/squareup/accountfreeze/RealAccountFreeze$frozen$1;-><init>(Lcom/squareup/accountfreeze/RealAccountFreeze;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "settings.settingsAvailab\u2026      .map { isFrozen() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public isFrozen()Z
    .locals 2

    .line 46
    iget-object v0, p0, Lcom/squareup/accountfreeze/RealAccountFreeze;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ACCOUNT_FREEZE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/accountfreeze/RealAccountFreeze;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->isAccountFrozen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    invoke-direct {p0}, Lcom/squareup/accountfreeze/RealAccountFreeze;->getBalanceAppletGateway()Lcom/squareup/ui/balance/BalanceAppletGateway;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/ui/balance/BalanceAppletGateway;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public markBannerDismissed()V
    .locals 3

    .line 91
    iget-object v0, p0, Lcom/squareup/accountfreeze/RealAccountFreeze;->lastDismissedBanner:Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v1, p0, Lcom/squareup/accountfreeze/RealAccountFreeze;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method
