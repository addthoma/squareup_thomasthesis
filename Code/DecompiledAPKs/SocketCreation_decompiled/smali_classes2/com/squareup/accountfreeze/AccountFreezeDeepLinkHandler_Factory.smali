.class public final Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler_Factory;
.super Ljava/lang/Object;
.source "AccountFreezeDeepLinkHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DeepLinks;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DeepLinks;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 26
    iput-object p3, p0, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DeepLinks;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;",
            ">;)",
            "Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler_Factory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ldagger/Lazy;Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;)Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/DeepLinks;",
            ">;",
            "Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;",
            "Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;",
            ")",
            "Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;-><init>(Ldagger/Lazy;Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;

    iget-object v2, p0, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;

    invoke-static {v0, v1, v2}, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler_Factory;->newInstance(Ldagger/Lazy;Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;)Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler_Factory;->get()Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;

    move-result-object v0

    return-object v0
.end method
