.class public final Lcom/squareup/activity/BillsList;
.super Ljava/lang/Object;
.source "BillsList.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBillsList.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BillsList.kt\ncom/squareup/activity/BillsList\n+ 2 _Sequences.kt\nkotlin/sequences/SequencesKt___SequencesKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,510:1\n1104#2,2:511\n704#3:513\n777#3,2:514\n1642#3,2:516\n1642#3,2:518\n*E\n*S KotlinDebug\n*F\n+ 1 BillsList.kt\ncom/squareup/activity/BillsList\n*L\n288#1,2:511\n320#1:513\n320#1,2:514\n375#1,2:516\n415#1,2:518\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u001e\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0000\n\u0002\u0010$\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0012\u0018\u00002\u00020\u0001B\u0019\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\t2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u001e\u0010\u0012\u001a\u00020\u000e2\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00132\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u000e\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u000f\u001a\u00020\tJ\u0018\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u000f\u001a\u00020\t2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0014\u0010\u0014\u001a\u00020\u00112\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0013J\u001e\u0010\u0014\u001a\u00020\u00112\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00132\u0006\u0010\u0016\u001a\u00020\u0011H\u0002J\u000e\u0010\u0017\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\tJ\u0018\u0010\u0017\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\t2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0014\u0010\u0017\u001a\u00020\u000e2\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0013J\u001e\u0010\u0017\u001a\u00020\u000e2\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00132\u0006\u0010\u0016\u001a\u00020\u0011H\u0002J\u0010\u0010\u0018\u001a\u00020\u00112\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0008\u0010\u001b\u001a\u00020\u000eH\u0007J\u0006\u0010\u001c\u001a\u00020\u000eJ\u0010\u0010\u001d\u001a\u0004\u0018\u00010\t2\u0006\u0010\u0019\u001a\u00020\u001aJ\u000c\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\t0\u001fJ$\u0010 \u001a\u00020\u00112\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\t0!2\u0006\u0010\u000f\u001a\u00020\tH\u0002J\u0010\u0010\"\u001a\u00020#2\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0006\u0010$\u001a\u00020\u0011J\u0014\u0010%\u001a\u00020\u000e2\u000c\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\t0\u001fJ\u0010\u0010\'\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\tH\u0002J\u0018\u0010(\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\t2\u0006\u0010)\u001a\u00020\u0011H\u0002J\u000e\u0010*\u001a\u00020\u00112\u0006\u0010\u000f\u001a\u00020\tJ\u0018\u0010*\u001a\u00020\u00112\u0006\u0010\u000f\u001a\u00020\t2\u0006\u0010)\u001a\u00020\u0011H\u0002J\u0014\u0010+\u001a\u00020\u000e2\u000c\u0010,\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0013J\u0008\u0010-\u001a\u00020\u000eH\u0002J\u0016\u0010.\u001a\u00020\u000e2\u0006\u0010/\u001a\u00020\u001a2\u0006\u00100\u001a\u00020\u001aJ\u0018\u00101\u001a\u00020\u000e2\u0006\u00102\u001a\u00020\t2\u0006\u00103\u001a\u00020\tH\u0002J\u000c\u00104\u001a\u00020\u000c*\u00020\tH\u0002J\u000c\u00104\u001a\u00020\u000c*\u00020\u001aH\u0002R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\t0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00065"
    }
    d2 = {
        "Lcom/squareup/activity/BillsList;",
        "",
        "mainThread",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "currentBill",
        "Lcom/squareup/activity/CurrentBill;",
        "(Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/activity/CurrentBill;)V",
        "bills",
        "",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "billsMap",
        "",
        "",
        "addBill",
        "",
        "bill",
        "sortAfterAdding",
        "",
        "addBills",
        "",
        "addIfNotPresent",
        "newBills",
        "sortAfterAddingEachBill",
        "addOrSafelyReplace",
        "alreadyPresent",
        "id",
        "Lcom/squareup/billhistory/model/BillHistoryId;",
        "clear",
        "clearNonPendingBills",
        "getBill",
        "getBills",
        "",
        "hasForwardedVersionOf",
        "",
        "indexOfBill",
        "",
        "isEmpty",
        "recordProcessedForwardedPayments",
        "forwardedPayments",
        "removeBill",
        "replaceBill",
        "sortAfterReplacing",
        "safelyReplaceIfPresent",
        "safelyReplacePendingBills",
        "pendingBills",
        "sortBillsList",
        "updateBillIdIfPresent",
        "oldId",
        "newId",
        "verifyStoreAndForward",
        "existingBill",
        "newBill",
        "asKey",
        "bill-history-ui_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bills:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;"
        }
    .end annotation
.end field

.field private final billsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;"
        }
    .end annotation
.end field

.field private final currentBill:Lcom/squareup/activity/CurrentBill;

.field private final mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;


# direct methods
.method public constructor <init>(Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/activity/CurrentBill;)V
    .locals 1
    .param p1    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "mainThread"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentBill"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/activity/BillsList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iput-object p2, p0, Lcom/squareup/activity/BillsList;->currentBill:Lcom/squareup/activity/CurrentBill;

    .line 52
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/activity/BillsList;->bills:Ljava/util/List;

    .line 56
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/squareup/activity/BillsList;->billsMap:Ljava/util/Map;

    return-void
.end method

.method public static final synthetic access$addIfNotPresent(Lcom/squareup/activity/BillsList;Lcom/squareup/billhistory/model/BillHistory;Z)Z
    .locals 0

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/squareup/activity/BillsList;->addIfNotPresent(Lcom/squareup/billhistory/model/BillHistory;Z)Z

    move-result p0

    return p0
.end method

.method private final addBill(Lcom/squareup/billhistory/model/BillHistory;Z)V
    .locals 2

    .line 403
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->bills:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 404
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->billsMap:Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/squareup/activity/BillsList;->asKey(Lcom/squareup/billhistory/model/BillHistory;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_0

    .line 406
    invoke-direct {p0}, Lcom/squareup/activity/BillsList;->sortBillsList()V

    :cond_0
    return-void
.end method

.method private final addBills(Ljava/util/Collection;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;Z)V"
        }
    .end annotation

    .line 414
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->bills:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {v0, p1}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 518
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/BillHistory;

    .line 416
    iget-object v1, p0, Lcom/squareup/activity/BillsList;->billsMap:Ljava/util/Map;

    invoke-direct {p0, v0}, Lcom/squareup/activity/BillsList;->asKey(Lcom/squareup/billhistory/model/BillHistory;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    .line 419
    invoke-direct {p0}, Lcom/squareup/activity/BillsList;->sortBillsList()V

    :cond_1
    return-void
.end method

.method private final addIfNotPresent(Lcom/squareup/billhistory/model/BillHistory;Z)Z
    .locals 2

    .line 384
    iget-object v0, p1, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    const-string v1, "bill.id"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/squareup/activity/BillsList;->alreadyPresent(Lcom/squareup/billhistory/model/BillHistoryId;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 387
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/activity/BillsList;->addBill(Lcom/squareup/billhistory/model/BillHistory;Z)V

    const/4 p1, 0x1

    return p1
.end method

.method private final addIfNotPresent(Ljava/util/Collection;Z)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;Z)Z"
        }
    .end annotation

    .line 395
    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object p1

    .line 396
    new-instance v0, Lcom/squareup/activity/BillsList$addIfNotPresent$2;

    invoke-direct {v0, p0, p2}, Lcom/squareup/activity/BillsList$addIfNotPresent$2;-><init>(Lcom/squareup/activity/BillsList;Z)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lkotlin/sequences/SequencesKt;->filter(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p1

    .line 397
    invoke-static {p1}, Lkotlin/sequences/SequencesKt;->count(Lkotlin/sequences/Sequence;)I

    move-result p1

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final addOrSafelyReplace(Lcom/squareup/billhistory/model/BillHistory;Z)V
    .locals 1

    .line 365
    invoke-direct {p0, p1, p2}, Lcom/squareup/activity/BillsList;->addIfNotPresent(Lcom/squareup/billhistory/model/BillHistory;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 368
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/activity/BillsList;->safelyReplaceIfPresent(Lcom/squareup/billhistory/model/BillHistory;Z)Z

    return-void
.end method

.method private final addOrSafelyReplace(Ljava/util/Collection;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;Z)V"
        }
    .end annotation

    .line 375
    check-cast p1, Ljava/lang/Iterable;

    .line 516
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/BillHistory;

    .line 376
    invoke-direct {p0, v0, p2}, Lcom/squareup/activity/BillsList;->addOrSafelyReplace(Lcom/squareup/billhistory/model/BillHistory;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private final alreadyPresent(Lcom/squareup/billhistory/model/BillHistoryId;)Z
    .locals 1

    .line 345
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->billsMap:Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/squareup/activity/BillsList;->asKey(Lcom/squareup/billhistory/model/BillHistoryId;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private final asKey(Lcom/squareup/billhistory/model/BillHistory;)Ljava/lang/String;
    .locals 1

    .line 488
    iget-object p1, p1, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/BillHistoryId;->getPaymentId(Z)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    return-object p1

    .line 489
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "At a minimum, client id should always be non-null"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final asKey(Lcom/squareup/billhistory/model/BillHistoryId;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 495
    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/BillHistoryId;->getPaymentId(Z)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    return-object p1

    .line 496
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "At a minimum, client id should always be non-null"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final hasForwardedVersionOf(Ljava/util/Map;Lcom/squareup/billhistory/model/BillHistory;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ")Z"
        }
    .end annotation

    .line 354
    invoke-direct {p0, p2}, Lcom/squareup/activity/BillsList;->asKey(Lcom/squareup/billhistory/model/BillHistory;)Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/BillHistory;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->getStoreAndForwardState()Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    sget-object p2, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->FORWARDED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    if-ne p1, p2, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return p1
.end method

.method private final indexOfBill(Lcom/squareup/billhistory/model/BillHistoryId;)I
    .locals 2

    .line 357
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->billsMap:Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/squareup/activity/BillsList;->asKey(Lcom/squareup/billhistory/model/BillHistoryId;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/BillHistory;

    if-eqz v0, :cond_0

    .line 358
    iget-object p1, p0, Lcom/squareup/activity/BillsList;->bills:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    return p1

    .line 357
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Bill with id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistoryId;->getId()Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " not in list"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final removeBill(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 3

    .line 470
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->bills:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    const-string v2, "bill.id"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/squareup/activity/BillsList;->indexOfBill(Lcom/squareup/billhistory/model/BillHistoryId;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 471
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->billsMap:Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/squareup/activity/BillsList;->asKey(Lcom/squareup/billhistory/model/BillHistory;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private final replaceBill(Lcom/squareup/billhistory/model/BillHistory;Z)V
    .locals 4

    .line 458
    invoke-direct {p0, p1}, Lcom/squareup/activity/BillsList;->asKey(Lcom/squareup/billhistory/model/BillHistory;)Ljava/lang/String;

    move-result-object v0

    .line 459
    iget-object v1, p0, Lcom/squareup/activity/BillsList;->bills:Ljava/util/List;

    iget-object v2, p1, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    const-string v3, "bill.id"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/squareup/activity/BillsList;->indexOfBill(Lcom/squareup/billhistory/model/BillHistoryId;)I

    move-result v2

    invoke-interface {v1, v2, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 460
    iget-object v1, p0, Lcom/squareup/activity/BillsList;->billsMap:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 461
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/CurrentBill;->isEqualTo(Lcom/squareup/billhistory/model/BillHistory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 462
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/CurrentBill;->safelyReplace(Lcom/squareup/billhistory/model/BillHistory;)V

    :cond_0
    if-eqz p2, :cond_1

    .line 465
    invoke-direct {p0}, Lcom/squareup/activity/BillsList;->sortBillsList()V

    :cond_1
    return-void
.end method

.method private final safelyReplaceIfPresent(Lcom/squareup/billhistory/model/BillHistory;Z)Z
    .locals 5

    .line 427
    iget-object v0, p1, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    const-string v1, "bill.id"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/squareup/activity/BillsList;->alreadyPresent(Lcom/squareup/billhistory/model/BillHistoryId;)Z

    move-result v0

    const/4 v2, 0x0

    if-nez v0, :cond_0

    return v2

    .line 432
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->getStoreAndForwardState()Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-result-object v0

    sget-object v3, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->FORWARDED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    const/4 v4, 0x1

    if-ne v0, v3, :cond_1

    .line 433
    invoke-direct {p0, p1, p2}, Lcom/squareup/activity/BillsList;->replaceBill(Lcom/squareup/billhistory/model/BillHistory;Z)V

    return v4

    .line 438
    :cond_1
    iget-object v0, p1, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/squareup/activity/BillsList;->getBill(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 441
    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistory;->getStoreAndForwardState()Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-result-object v1

    sget-object v3, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->FORWARDED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    if-ne v1, v3, :cond_2

    return v2

    .line 446
    :cond_2
    iget-boolean v1, p1, Lcom/squareup/billhistory/model/BillHistory;->pending:Z

    if-eqz v1, :cond_3

    iget-boolean v0, v0, Lcom/squareup/billhistory/model/BillHistory;->pending:Z

    if-nez v0, :cond_3

    return v2

    .line 450
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/squareup/activity/BillsList;->replaceBill(Lcom/squareup/billhistory/model/BillHistory;Z)V

    return v4

    .line 438
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Should never throw. See alreadyPresent(bill.id) above"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final sortBillsList()V
    .locals 3

    .line 475
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->bills:Ljava/util/List;

    sget-object v1, Lcom/squareup/billhistory/BillHistoryComparator;->COMPARATOR:Lcom/squareup/billhistory/BillHistoryComparator;

    const-string v2, "BillHistoryComparator.COMPARATOR"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/util/Comparator;

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->sortWith(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method private final verifyStoreAndForward(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 2

    .line 502
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->isStoreAndForward()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->isStoreAndForward()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    return-void

    .line 503
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Two pending bills share an id but somehow are not both store-and-forward bills. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Existing bill = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 504
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " (store-and-forward state = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 505
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->getStoreAndForwardState()Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "). New bill = "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " (store-and-forward "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "state = "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 506
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->getStoreAndForwardState()Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ")."

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 502
    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method


# virtual methods
.method public final addIfNotPresent(Lcom/squareup/billhistory/model/BillHistory;)Z
    .locals 1

    const-string v0, "bill"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 330
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    const/4 v0, 0x1

    .line 331
    invoke-direct {p0, p1, v0}, Lcom/squareup/activity/BillsList;->addIfNotPresent(Lcom/squareup/billhistory/model/BillHistory;Z)Z

    move-result p1

    return p1
.end method

.method public final addIfNotPresent(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "newBills"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 340
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    const/4 v0, 0x0

    .line 341
    invoke-direct {p0, p1, v0}, Lcom/squareup/activity/BillsList;->addIfNotPresent(Ljava/util/Collection;Z)Z

    move-result p1

    .line 342
    invoke-direct {p0}, Lcom/squareup/activity/BillsList;->sortBillsList()V

    return p1
.end method

.method public final addOrSafelyReplace(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 1

    const-string v0, "bill"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    const/4 v0, 0x1

    .line 100
    invoke-direct {p0, p1, v0}, Lcom/squareup/activity/BillsList;->addOrSafelyReplace(Lcom/squareup/billhistory/model/BillHistory;Z)V

    return-void
.end method

.method public final addOrSafelyReplace(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;)V"
        }
    .end annotation

    const-string v0, "bills"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    const/4 v0, 0x0

    .line 108
    invoke-direct {p0, p1, v0}, Lcom/squareup/activity/BillsList;->addOrSafelyReplace(Ljava/util/Collection;Z)V

    .line 109
    invoke-direct {p0}, Lcom/squareup/activity/BillsList;->sortBillsList()V

    return-void
.end method

.method public final clear()V
    .locals 1

    .line 479
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 480
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->bills:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 481
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->billsMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public final clearNonPendingBills()V
    .locals 4

    .line 319
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 320
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->bills:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 513
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 514
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/billhistory/model/BillHistory;

    .line 320
    iget-boolean v3, v3, Lcom/squareup/billhistory/model/BillHistory;->pending:Z

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 515
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 321
    invoke-virtual {p0}, Lcom/squareup/activity/BillsList;->clear()V

    .line 322
    check-cast v1, Ljava/util/Collection;

    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lcom/squareup/activity/BillsList;->addBills(Ljava/util/Collection;Z)V

    return-void
.end method

.method public final getBill(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 1

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 74
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->billsMap:Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/squareup/activity/BillsList;->asKey(Lcom/squareup/billhistory/model/BillHistoryId;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/BillHistory;

    return-object p1
.end method

.method public final getBills()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;"
        }
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 66
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->bills:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final isEmpty()Z
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 79
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->bills:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final recordProcessedForwardedPayments(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;)V"
        }
    .end annotation

    const-string v0, "forwardedPayments"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 278
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 281
    check-cast p1, Ljava/lang/Iterable;

    .line 282
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object p1

    .line 283
    new-instance v0, Lcom/squareup/activity/BillsList$recordProcessedForwardedPayments$1;

    invoke-direct {v0, p0}, Lcom/squareup/activity/BillsList$recordProcessedForwardedPayments$1;-><init>(Lcom/squareup/activity/BillsList;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lkotlin/sequences/SequencesKt;->filter(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p1

    .line 511
    invoke-interface {p1}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/BillHistory;

    const/4 v1, 0x0

    .line 288
    invoke-direct {p0, v0, v1}, Lcom/squareup/activity/BillsList;->replaceBill(Lcom/squareup/billhistory/model/BillHistory;Z)V

    goto :goto_0

    .line 512
    :cond_0
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 289
    invoke-direct {p0}, Lcom/squareup/activity/BillsList;->sortBillsList()V

    return-void
.end method

.method public final safelyReplaceIfPresent(Lcom/squareup/billhistory/model/BillHistory;)Z
    .locals 1

    const-string v0, "bill"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    const/4 v0, 0x1

    .line 134
    invoke-direct {p0, p1, v0}, Lcom/squareup/activity/BillsList;->safelyReplaceIfPresent(Lcom/squareup/billhistory/model/BillHistory;Z)Z

    move-result p1

    return p1
.end method

.method public final safelyReplacePendingBills(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;)V"
        }
    .end annotation

    const-string v0, "pendingBills"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 220
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v0, Ljava/util/Map;

    .line 221
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/BillHistory;

    .line 222
    invoke-direct {p0, v1}, Lcom/squareup/activity/BillsList;->asKey(Lcom/squareup/billhistory/model/BillHistory;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/billhistory/model/BillHistory;

    if-eqz v2, :cond_0

    .line 224
    invoke-direct {p0, v2, v1}, Lcom/squareup/activity/BillsList;->verifyStoreAndForward(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/BillHistory;)V

    .line 225
    invoke-virtual {v2}, Lcom/squareup/billhistory/model/BillHistory;->getStoreAndForwardState()Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-result-object v2

    sget-object v3, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->FORWARDED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    if-ne v2, v3, :cond_0

    goto :goto_0

    .line 229
    :cond_0
    invoke-direct {p0, v1}, Lcom/squareup/activity/BillsList;->asKey(Lcom/squareup/billhistory/model/BillHistory;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 235
    :cond_1
    iget-object p1, p0, Lcom/squareup/activity/BillsList;->bills:Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/BillHistory;

    .line 238
    iget-boolean v2, v1, Lcom/squareup/billhistory/model/BillHistory;->pending:Z

    if-nez v2, :cond_3

    goto :goto_1

    .line 243
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/billhistory/model/BillHistory;->getStoreAndForwardState()Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-result-object v2

    sget-object v3, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->STORED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    if-ne v2, v3, :cond_4

    .line 244
    invoke-direct {p0, v0, v1}, Lcom/squareup/activity/BillsList;->hasForwardedVersionOf(Ljava/util/Map;Lcom/squareup/billhistory/model/BillHistory;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 245
    invoke-direct {p0, v1}, Lcom/squareup/activity/BillsList;->removeBill(Lcom/squareup/billhistory/model/BillHistory;)V

    goto :goto_1

    .line 252
    :cond_4
    invoke-direct {p0, v1}, Lcom/squareup/activity/BillsList;->asKey(Lcom/squareup/billhistory/model/BillHistory;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 253
    invoke-direct {p0, v1}, Lcom/squareup/activity/BillsList;->removeBill(Lcom/squareup/billhistory/model/BillHistory;)V

    goto :goto_1

    .line 260
    :cond_5
    iget-object p1, p0, Lcom/squareup/activity/BillsList;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {p1}, Lcom/squareup/activity/CurrentBill;->get()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    if-eqz p1, :cond_6

    .line 262
    iget-boolean v1, p1, Lcom/squareup/billhistory/model/BillHistory;->pending:Z

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_6

    const-string v1, "it"

    .line 263
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/squareup/activity/BillsList;->asKey(Lcom/squareup/billhistory/model/BillHistory;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    :cond_6
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/squareup/activity/BillsList;->addOrSafelyReplace(Ljava/util/Collection;Z)V

    .line 270
    invoke-direct {p0}, Lcom/squareup/activity/BillsList;->sortBillsList()V

    return-void
.end method

.method public final updateBillIdIfPresent(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/billhistory/model/BillHistoryId;)V
    .locals 2

    const-string v0, "oldId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 301
    iget-object v0, p0, Lcom/squareup/activity/BillsList;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 302
    invoke-virtual {p0, p1}, Lcom/squareup/activity/BillsList;->getBill(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 303
    new-instance v1, Lcom/squareup/billhistory/model/BillHistory$Builder;

    invoke-direct {v1, v0}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistory;)V

    .line 304
    invoke-virtual {v1, p2}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setId(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p2

    .line 305
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p2

    .line 308
    invoke-direct {p0, p1}, Lcom/squareup/activity/BillsList;->alreadyPresent(Lcom/squareup/billhistory/model/BillHistoryId;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 309
    invoke-direct {p0, v0}, Lcom/squareup/activity/BillsList;->removeBill(Lcom/squareup/billhistory/model/BillHistory;)V

    const-string p1, "newBill"

    .line 310
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x1

    invoke-direct {p0, p2, p1}, Lcom/squareup/activity/BillsList;->addBill(Lcom/squareup/billhistory/model/BillHistory;Z)V

    :cond_0
    return-void
.end method
