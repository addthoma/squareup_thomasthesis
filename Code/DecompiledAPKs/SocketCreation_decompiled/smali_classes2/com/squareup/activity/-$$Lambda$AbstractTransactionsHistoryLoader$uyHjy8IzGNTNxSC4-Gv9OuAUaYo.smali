.class public final synthetic Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$uyHjy8IzGNTNxSC4-Gv9OuAUaYo;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Ljava/util/Comparator;


# static fields
.field public static final synthetic INSTANCE:Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$uyHjy8IzGNTNxSC4-Gv9OuAUaYo;


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$uyHjy8IzGNTNxSC4-Gv9OuAUaYo;

    invoke-direct {v0}, Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$uyHjy8IzGNTNxSC4-Gv9OuAUaYo;-><init>()V

    sput-object v0, Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$uyHjy8IzGNTNxSC4-Gv9OuAUaYo;->INSTANCE:Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$uyHjy8IzGNTNxSC4-Gv9OuAUaYo;

    return-void
.end method

.method private synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;

    check-cast p2, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;

    invoke-static {p1, p2}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->lambda$sortBillFamiliesInDescendingOrderByCompletedTime$7(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;)I

    move-result p1

    return p1
.end method
