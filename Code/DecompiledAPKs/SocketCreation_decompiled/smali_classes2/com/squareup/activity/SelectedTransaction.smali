.class public final Lcom/squareup/activity/SelectedTransaction;
.super Ljava/lang/Object;
.source "SelectedTransaction.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/activity/ActivityAppletScope;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectedTransaction.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectedTransaction.kt\ncom/squareup/activity/SelectedTransaction\n+ 2 HistoricalTransaction.kt\ncom/squareup/transactionhistory/historical/HistoricalTransactionKt\n*L\n1#1,429:1\n44#2:430\n44#2:431\n44#2:432\n44#2:433\n44#2:434\n44#2:435\n44#2:436\n*E\n*S KotlinDebug\n*F\n+ 1 SelectedTransaction.kt\ncom/squareup/activity/SelectedTransaction\n*L\n181#1:430\n182#1:431\n227#1:432\n227#1:433\n319#1:434\n348#1:435\n399#1:436\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u0001B+\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0006\u0010\u0013\u001a\u00020\u0014J\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u000cJ\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u000eJ\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018J\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u0018J\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u0018J\u0006\u0010\u001b\u001a\u00020\u001cJ\u0006\u0010\u001d\u001a\u00020\u001cJ\u0006\u0010\u001e\u001a\u00020\u001cJ\u0010\u0010\u001f\u001a\u00020\u001c2\u0008\u0010 \u001a\u0004\u0018\u00010\u000eJ\u0010\u0010\u001f\u001a\u00020\u001c2\u0008\u0010 \u001a\u0004\u0018\u00010\u000cJ\u000e\u0010!\u001a\u00020\u00142\u0006\u0010\u0012\u001a\u00020\u000eJ\u000e\u0010!\u001a\u00020\u00142\u0006\u0010\u000f\u001a\u00020\u000cJ\u000e\u0010\"\u001a\u00020\u00142\u0006\u0010#\u001a\u00020\u0018J\u0012\u0010\u000f\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000c0\u00110$J\u0012\u0010\u0012\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000e0\u00110$J\u000e\u0010%\u001a\u00020\u001c2\u0006\u0010&\u001a\u00020\u000eR\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000f\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000c0\u00110\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0012\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000e0\u00110\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/activity/SelectedTransaction;",
        "",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "mainThread",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "res",
        "Lcom/squareup/util/Res;",
        "perUnitFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;)V",
        "backingSummary",
        "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
        "backingTransaction",
        "Lcom/squareup/transactionhistory/historical/HistoricalTransaction;",
        "summary",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/util/Optional;",
        "transaction",
        "clearTenderId",
        "",
        "getBackingSummary",
        "getBackingTransaction",
        "getClientId",
        "",
        "getServerId",
        "getTenderId",
        "hasServerId",
        "",
        "hasTenderId",
        "isSet",
        "sameIdentityAs",
        "other",
        "set",
        "setTenderId",
        "tenderId",
        "Lio/reactivex/Observable;",
        "update",
        "updated",
        "bill-history-ui_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private backingSummary:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

.field private backingTransaction:Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private final perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final res:Lcom/squareup/util/Res;

.field private final summary:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ">;>;"
        }
    .end annotation
.end field

.field private final transaction:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransaction;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;)V
    .locals 1
    .param p1    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p2    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "mainScheduler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThread"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "perUnitFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/activity/SelectedTransaction;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p2, p0, Lcom/squareup/activity/SelectedTransaction;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iput-object p3, p0, Lcom/squareup/activity/SelectedTransaction;->res:Lcom/squareup/util/Res;

    iput-object p4, p0, Lcom/squareup/activity/SelectedTransaction;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 74
    invoke-static {}, Lcom/squareup/activity/SelectedTransactionKt;->getNO_BACKING_SUMMARY()Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefault(NO_BACKING_SUMMARY)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/activity/SelectedTransaction;->summary:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 81
    invoke-static {}, Lcom/squareup/activity/SelectedTransactionKt;->getNO_BACKING_TRANSACTION()Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefa\u2026t(NO_BACKING_TRANSACTION)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/activity/SelectedTransaction;->transaction:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method


# virtual methods
.method public final clearTenderId()V
    .locals 21

    move-object/from16 v0, p0

    .line 338
    iget-object v1, v0, Lcom/squareup/activity/SelectedTransaction;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 340
    iget-object v2, v0, Lcom/squareup/activity/SelectedTransaction;->backingSummary:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    if-eqz v2, :cond_0

    .line 341
    invoke-virtual {v2}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getTenderId()Ljava/lang/String;

    move-result-object v1

    const-string v3, ""

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const v19, 0xff7f

    const/16 v20, 0x0

    const-string v10, ""

    .line 342
    invoke-static/range {v2 .. v20}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->copy$default(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/transactionhistory/TransactionType;Lcom/squareup/transactionhistory/pending/StoreAndForwardState;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZZZZZZLjava/util/List;ILjava/lang/Object;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    move-result-object v1

    .line 343
    iput-object v1, v0, Lcom/squareup/activity/SelectedTransaction;->backingSummary:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    .line 344
    iget-object v2, v0, Lcom/squareup/activity/SelectedTransaction;->summary:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v3, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v3, v1}, Lcom/squareup/util/Optional$Companion;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 349
    :cond_0
    iget-object v1, v0, Lcom/squareup/activity/SelectedTransaction;->backingTransaction:Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    if-eqz v1, :cond_2

    .line 435
    invoke-virtual {v1}, Lcom/squareup/transactionhistory/historical/HistoricalTransaction;->getBillHistory()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    check-cast v1, Lcom/squareup/billhistory/model/BillHistory;

    if-eqz v1, :cond_2

    .line 350
    iget-object v2, v1, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    const-string v3, "it.id"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/billhistory/model/BillHistoryId;->getTenderId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 351
    new-instance v2, Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    .line 352
    new-instance v3, Lcom/squareup/billhistory/model/BillHistory$Builder;

    invoke-direct {v3, v1}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistory;)V

    .line 353
    iget-object v1, v1, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    invoke-virtual {v1}, Lcom/squareup/billhistory/model/BillHistoryId;->withoutTender()Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setId(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object v1

    .line 354
    invoke-virtual {v1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v1

    const-string v3, "BillHistory.Builder(it)\n\u2026                 .build()"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 351
    invoke-direct {v2, v1}, Lcom/squareup/transactionhistory/historical/HistoricalTransaction;-><init>(Ljava/lang/Object;)V

    .line 356
    iput-object v2, v0, Lcom/squareup/activity/SelectedTransaction;->backingTransaction:Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    .line 357
    iget-object v1, v0, Lcom/squareup/activity/SelectedTransaction;->transaction:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v3, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v3, v2}, Lcom/squareup/util/Optional$Companion;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 435
    :cond_1
    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type com.squareup.billhistory.model.BillHistory"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    :goto_0
    return-void
.end method

.method public final getBackingSummary()Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;
    .locals 1

    .line 366
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 367
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->backingSummary:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    return-object v0
.end method

.method public final getBackingTransaction()Lcom/squareup/transactionhistory/historical/HistoricalTransaction;
    .locals 1

    .line 376
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 377
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->backingTransaction:Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    return-object v0
.end method

.method public final getClientId()Ljava/lang/String;
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 254
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->backingSummary:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getClientId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final getServerId()Ljava/lang/String;
    .locals 1

    .line 273
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 274
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->backingSummary:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getServerId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final getTenderId()Ljava/lang/String;
    .locals 1

    .line 293
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 294
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->backingSummary:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getTenderId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final hasServerId()Z
    .locals 2

    .line 262
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 263
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->backingSummary:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    if-eqz v0, :cond_0

    .line 264
    invoke-virtual {v0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getServerId()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final hasTenderId()Z
    .locals 2

    .line 282
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 283
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->backingSummary:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    if-eqz v0, :cond_0

    .line 284
    invoke-virtual {v0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getTenderId()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isSet()Z
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 121
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->backingSummary:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final sameIdentityAs(Lcom/squareup/transactionhistory/historical/HistoricalTransaction;)Z
    .locals 3

    .line 394
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 399
    :cond_0
    iget-object v1, p0, Lcom/squareup/activity/SelectedTransaction;->backingSummary:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    if-eqz v1, :cond_2

    .line 400
    invoke-static {v1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummaryKt;->getTransactionIds(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Lcom/squareup/transactionhistory/TransactionIds;

    move-result-object v0

    .line 436
    invoke-virtual {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransaction;->getBillHistory()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_1

    check-cast p1, Lcom/squareup/billhistory/model/BillHistory;

    iget-object p1, p1, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    const-string v1, "other.asBillHistory<BillHistory>()\n          .id"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistoryId;->getId()Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    .line 403
    sget-object v1, Lcom/squareup/transactionhistory/TransactionIds;->Companion:Lcom/squareup/transactionhistory/TransactionIds$Companion;

    iget-object v2, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-virtual {v1, v2, p1}, Lcom/squareup/transactionhistory/TransactionIds$Companion;->newTransactionIds(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/transactionhistory/TransactionIds;

    move-result-object p1

    .line 404
    invoke-static {v0, p1}, Lcom/squareup/transactionhistory/TransactionIdsKt;->atLeastOneIdIsEqual(Lcom/squareup/transactionhistory/TransactionIds;Lcom/squareup/transactionhistory/TransactionIds;)Z

    move-result p1

    return p1

    .line 436
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.billhistory.model.BillHistory"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    return v0
.end method

.method public final sameIdentityAs(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z
    .locals 1

    .line 385
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 386
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->backingSummary:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    invoke-static {v0, p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummaryKt;->sameIdentities(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z

    move-result p1

    return p1
.end method

.method public final set(Lcom/squareup/transactionhistory/historical/HistoricalTransaction;)V
    .locals 3

    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 182
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->backingTransaction:Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    if-eq p1, v0, :cond_5

    .line 430
    invoke-virtual {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransaction;->getBillHistory()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.squareup.billhistory.model.BillHistory"

    if-eqz v0, :cond_4

    check-cast v0, Lcom/squareup/billhistory/model/BillHistory;

    iget-object v2, p0, Lcom/squareup/activity/SelectedTransaction;->backingTransaction:Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    if-eqz v2, :cond_1

    .line 431
    invoke-virtual {v2}, Lcom/squareup/transactionhistory/historical/HistoricalTransaction;->getBillHistory()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    check-cast v2, Lcom/squareup/billhistory/model/BillHistory;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    const/4 v2, 0x0

    :goto_0
    if-ne v0, v2, :cond_2

    goto :goto_1

    .line 187
    :cond_2
    iput-object p1, p0, Lcom/squareup/activity/SelectedTransaction;->backingTransaction:Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    .line 188
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->transaction:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/util/Optional$Companion;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 190
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/activity/SelectedTransaction;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-static {p1, v0, v1}, Lcom/squareup/transactionhistory/mappings/historical/HistoricalTransactionMappersKt;->toHistoricalTransactionSummary(Lcom/squareup/transactionhistory/historical/HistoricalTransaction;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    move-result-object p1

    .line 191
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->backingSummary:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    .line 192
    iput-object p1, p0, Lcom/squareup/activity/SelectedTransaction;->backingSummary:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    .line 193
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->summary:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/util/Optional$Companion;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_3
    return-void

    .line 430
    :cond_4
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    :goto_1
    return-void
.end method

.method public final set(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)V
    .locals 4

    const-string v0, "summary"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 137
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->backingSummary:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 141
    :cond_0
    iput-object p1, p0, Lcom/squareup/activity/SelectedTransaction;->backingSummary:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    .line 143
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->backingTransaction:Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    if-eqz v0, :cond_2

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/squareup/activity/SelectedTransaction;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/activity/SelectedTransaction;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-static {v0, v2, v3}, Lcom/squareup/transactionhistory/mappings/historical/HistoricalTransactionMappersKt;->toHistoricalTransactionSummary(Lcom/squareup/transactionhistory/historical/HistoricalTransaction;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    :goto_0
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 145
    check-cast v1, Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    iput-object v1, p0, Lcom/squareup/activity/SelectedTransaction;->backingTransaction:Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    .line 146
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->transaction:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {}, Lcom/squareup/activity/SelectedTransactionKt;->getNO_BACKING_TRANSACTION()Lcom/squareup/util/Optional;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 148
    :cond_2
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->summary:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/util/Optional$Companion;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final setTenderId(Ljava/lang/String;)V
    .locals 20

    move-object/from16 v0, p0

    move-object/from16 v9, p1

    const-string v1, "tenderId"

    invoke-static {v9, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 308
    iget-object v1, v0, Lcom/squareup/activity/SelectedTransaction;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 309
    move-object v1, v9

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_3

    .line 311
    iget-object v1, v0, Lcom/squareup/activity/SelectedTransaction;->backingSummary:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    if-eqz v1, :cond_0

    .line 312
    invoke-virtual {v1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getTenderId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v9, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const v18, 0xff7f

    const/16 v19, 0x0

    move-object/from16 v9, p1

    .line 313
    invoke-static/range {v1 .. v19}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->copy$default(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/transactionhistory/TransactionType;Lcom/squareup/transactionhistory/pending/StoreAndForwardState;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZZZZZZLjava/util/List;ILjava/lang/Object;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    move-result-object v1

    .line 314
    iput-object v1, v0, Lcom/squareup/activity/SelectedTransaction;->backingSummary:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    .line 315
    iget-object v2, v0, Lcom/squareup/activity/SelectedTransaction;->summary:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v3, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v3, v1}, Lcom/squareup/util/Optional$Companion;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 320
    :cond_0
    iget-object v1, v0, Lcom/squareup/activity/SelectedTransaction;->backingTransaction:Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    if-eqz v1, :cond_2

    .line 434
    invoke-virtual {v1}, Lcom/squareup/transactionhistory/historical/HistoricalTransaction;->getBillHistory()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    check-cast v1, Lcom/squareup/billhistory/model/BillHistory;

    if-eqz v1, :cond_2

    .line 321
    iget-object v2, v1, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    const-string v3, "it.id"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/billhistory/model/BillHistoryId;->getTenderId()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v3, p1

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    .line 322
    new-instance v2, Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    .line 323
    new-instance v4, Lcom/squareup/billhistory/model/BillHistory$Builder;

    invoke-direct {v4, v1}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistory;)V

    .line 324
    iget-object v1, v1, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    invoke-virtual {v1, v3}, Lcom/squareup/billhistory/model/BillHistoryId;->forTenderId(Ljava/lang/String;)Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setId(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object v1

    .line 325
    invoke-virtual {v1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v1

    const-string v3, "BillHistory.Builder(it)\n\u2026                 .build()"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 322
    invoke-direct {v2, v1}, Lcom/squareup/transactionhistory/historical/HistoricalTransaction;-><init>(Ljava/lang/Object;)V

    .line 327
    iput-object v2, v0, Lcom/squareup/activity/SelectedTransaction;->backingTransaction:Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    .line 328
    iget-object v1, v0, Lcom/squareup/activity/SelectedTransaction;->transaction:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v3, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v3, v2}, Lcom/squareup/util/Optional$Companion;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 434
    :cond_1
    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type com.squareup.billhistory.model.BillHistory"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    :goto_0
    return-void

    .line 309
    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Check failed."

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method

.method public final summary()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ">;>;"
        }
    .end annotation

    .line 103
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->summary:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/activity/SelectedTransaction;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "summary.observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final transaction()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransaction;",
            ">;>;"
        }
    .end annotation

    .line 114
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->transaction:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/activity/SelectedTransaction;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "transaction.observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final update(Lcom/squareup/transactionhistory/historical/HistoricalTransaction;)Z
    .locals 4

    const-string/jumbo v0, "updated"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 221
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 222
    invoke-virtual {p0, p1}, Lcom/squareup/activity/SelectedTransaction;->sameIdentityAs(Lcom/squareup/transactionhistory/historical/HistoricalTransaction;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->backingTransaction:Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    if-eq p1, v0, :cond_7

    .line 432
    invoke-virtual {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransaction;->getBillHistory()Ljava/lang/Object;

    move-result-object v0

    const-string v2, "null cannot be cast to non-null type com.squareup.billhistory.model.BillHistory"

    if-eqz v0, :cond_6

    check-cast v0, Lcom/squareup/billhistory/model/BillHistory;

    iget-object v3, p0, Lcom/squareup/activity/SelectedTransaction;->backingTransaction:Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    if-eqz v3, :cond_2

    .line 433
    invoke-virtual {v3}, Lcom/squareup/transactionhistory/historical/HistoricalTransaction;->getBillHistory()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    check-cast v3, Lcom/squareup/billhistory/model/BillHistory;

    goto :goto_0

    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    const/4 v3, 0x0

    :goto_0
    if-ne v0, v3, :cond_3

    goto :goto_1

    .line 232
    :cond_3
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->backingTransaction:Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    const/4 v2, 0x1

    if-eqz v0, :cond_4

    .line 233
    iput-object p1, p0, Lcom/squareup/activity/SelectedTransaction;->backingTransaction:Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    .line 234
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->transaction:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/util/Optional$Companion;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    const/4 v1, 0x1

    .line 238
    :cond_4
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/activity/SelectedTransaction;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-static {p1, v0, v3}, Lcom/squareup/transactionhistory/mappings/historical/HistoricalTransactionMappersKt;->toHistoricalTransactionSummary(Lcom/squareup/transactionhistory/historical/HistoricalTransaction;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    move-result-object p1

    .line 239
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->backingSummary:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v2

    if-eqz v0, :cond_5

    .line 240
    iput-object p1, p0, Lcom/squareup/activity/SelectedTransaction;->backingSummary:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    .line 241
    iget-object v0, p0, Lcom/squareup/activity/SelectedTransaction;->summary:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/util/Optional$Companion;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return v2

    :cond_5
    return v1

    .line 432
    :cond_6
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    :goto_1
    return v1
.end method
