.class public Lcom/squareup/activity/ActivitySearchInstrumentConverter;
.super Ljava/lang/Object;
.source "ActivitySearchInstrumentConverter.java"


# instance fields
.field private final cardConverter:Lcom/squareup/payment/CardConverter;


# direct methods
.method constructor <init>(Lcom/squareup/payment/CardConverter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/activity/ActivitySearchInstrumentConverter;->cardConverter:Lcom/squareup/payment/CardConverter;

    return-void
.end method

.method private entryMethodForCard(Lcom/squareup/Card;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .locals 0

    .line 47
    invoke-virtual {p1}, Lcom/squareup/Card;->isManual()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->KEYED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    :goto_0
    return-object p1
.end method

.method public static instrumentSearchForBytes(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;
    .locals 0

    .line 37
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/squareup/payment/CardConverterUtils;->createCardDataFromBytes(Lcom/squareup/protos/client/bills/CardData$ReaderType;[B)Lcom/squareup/protos/client/bills/CardData;

    move-result-object p0

    .line 38
    new-instance p1, Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch$Builder;-><init>()V

    .line 39
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch$Builder;->entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch$Builder;

    move-result-object p1

    new-instance p2, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;-><init>()V

    .line 41
    invoke-virtual {p2, p0}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->card_data(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;

    move-result-object p0

    .line 42
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->build()Lcom/squareup/protos/client/bills/PaymentInstrument;

    move-result-object p0

    .line 40
    invoke-virtual {p1, p0}, Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch$Builder;->payment_instrument(Lcom/squareup/protos/client/bills/PaymentInstrument;)Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch$Builder;

    move-result-object p0

    .line 43
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch$Builder;->build()Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method instrumentSearchForCard(Lcom/squareup/Card;)Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/squareup/activity/ActivitySearchInstrumentConverter;->cardConverter:Lcom/squareup/payment/CardConverter;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/CardConverter;->getCardData(Lcom/squareup/Card;)Lcom/squareup/protos/client/bills/CardData;

    move-result-object v0

    .line 27
    new-instance v1, Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch$Builder;-><init>()V

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/activity/ActivitySearchInstrumentConverter;->entryMethodForCard(Lcom/squareup/Card;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch$Builder;->entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch$Builder;

    move-result-object p1

    new-instance v1, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;-><init>()V

    .line 30
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->card_data(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->build()Lcom/squareup/protos/client/bills/PaymentInstrument;

    move-result-object v0

    .line 29
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch$Builder;->payment_instrument(Lcom/squareup/protos/client/bills/PaymentInstrument;)Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch$Builder;

    move-result-object p1

    .line 32
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch$Builder;->build()Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    move-result-object p1

    return-object p1
.end method
