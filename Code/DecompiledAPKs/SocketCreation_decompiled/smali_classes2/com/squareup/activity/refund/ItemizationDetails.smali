.class public final Lcom/squareup/activity/refund/ItemizationDetails;
.super Ljava/lang/Object;
.source "ItemizationDetails.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/activity/refund/ItemizationDetails$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008%\u0008\u0086\u0008\u0018\u0000 62\u00020\u0001:\u00016By\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0008\u0002\u0010\r\u001a\u00020\u000e\u0012\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u0010\u0012\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0002\u0010\u0013J\t\u0010$\u001a\u00020\u0003H\u00c6\u0003J\t\u0010%\u001a\u00020\u0010H\u00c6\u0003J\t\u0010&\u001a\u00020\u0012H\u00c6\u0003J\t\u0010\'\u001a\u00020\u0005H\u00c6\u0003J\t\u0010(\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010)\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010*\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\u000b\u0010+\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010,\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010-\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010.\u001a\u00020\u000eH\u00c6\u0003J\u0081\u0001\u0010/\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0002\u0010\r\u001a\u00020\u000e2\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u00102\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u0012H\u00c6\u0001J\u0013\u00100\u001a\u00020\u00102\u0008\u00101\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u00102\u001a\u00020\u000eH\u00d6\u0001J\u0006\u00103\u001a\u00020\u0010J\u0006\u00104\u001a\u00020\u0010J\t\u00105\u001a\u00020\u0003H\u00d6\u0001R\u0013\u0010\u000c\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0016R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0015R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0015R\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0015R\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0011\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001dR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u0015R\u0013\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010 R\u0013\u0010\n\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\u0015R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010#\u00a8\u00067"
    }
    d2 = {
        "Lcom/squareup/activity/refund/ItemizationDetails;",
        "",
        "name",
        "",
        "totalMoney",
        "Lcom/squareup/protos/common/Money;",
        "note",
        "sourceBillServerToken",
        "sourceItemizationTokenPair",
        "Lcom/squareup/protos/client/IdPair;",
        "sourceTipTenderToken",
        "merchantCatalogToken",
        "cogsId",
        "ordinal",
        "",
        "isInventoryTracked",
        "",
        "quantity",
        "Ljava/math/BigDecimal;",
        "(Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/math/BigDecimal;)V",
        "getCogsId",
        "()Ljava/lang/String;",
        "()Z",
        "getMerchantCatalogToken",
        "getName",
        "getNote",
        "getOrdinal",
        "()I",
        "getQuantity",
        "()Ljava/math/BigDecimal;",
        "getSourceBillServerToken",
        "getSourceItemizationTokenPair",
        "()Lcom/squareup/protos/client/IdPair;",
        "getSourceTipTenderToken",
        "getTotalMoney",
        "()Lcom/squareup/protos/common/Money;",
        "component1",
        "component10",
        "component11",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "other",
        "hashCode",
        "isItemization",
        "isTip",
        "toString",
        "Companion",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/activity/refund/ItemizationDetails$Companion;


# instance fields
.field private final cogsId:Ljava/lang/String;

.field private final isInventoryTracked:Z

.field private final merchantCatalogToken:Ljava/lang/String;

.field private final name:Ljava/lang/String;

.field private final note:Ljava/lang/String;

.field private final ordinal:I

.field private final quantity:Ljava/math/BigDecimal;

.field private final sourceBillServerToken:Ljava/lang/String;

.field private final sourceItemizationTokenPair:Lcom/squareup/protos/client/IdPair;

.field private final sourceTipTenderToken:Ljava/lang/String;

.field private final totalMoney:Lcom/squareup/protos/common/Money;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/activity/refund/ItemizationDetails$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/activity/refund/ItemizationDetails$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/activity/refund/ItemizationDetails;->Companion:Lcom/squareup/activity/refund/ItemizationDetails$Companion;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/math/BigDecimal;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "totalMoney"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "note"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "quantity"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/activity/refund/ItemizationDetails;->name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/activity/refund/ItemizationDetails;->totalMoney:Lcom/squareup/protos/common/Money;

    iput-object p3, p0, Lcom/squareup/activity/refund/ItemizationDetails;->note:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceBillServerToken:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceItemizationTokenPair:Lcom/squareup/protos/client/IdPair;

    iput-object p6, p0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceTipTenderToken:Ljava/lang/String;

    iput-object p7, p0, Lcom/squareup/activity/refund/ItemizationDetails;->merchantCatalogToken:Ljava/lang/String;

    iput-object p8, p0, Lcom/squareup/activity/refund/ItemizationDetails;->cogsId:Ljava/lang/String;

    iput p9, p0, Lcom/squareup/activity/refund/ItemizationDetails;->ordinal:I

    iput-boolean p10, p0, Lcom/squareup/activity/refund/ItemizationDetails;->isInventoryTracked:Z

    iput-object p11, p0, Lcom/squareup/activity/refund/ItemizationDetails;->quantity:Ljava/math/BigDecimal;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/math/BigDecimal;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 14

    move/from16 v0, p12

    and-int/lit8 v1, v0, 0x4

    if-eqz v1, :cond_0

    const-string v1, ""

    move-object v5, v1

    goto :goto_0

    :cond_0
    move-object/from16 v5, p3

    :goto_0
    and-int/lit8 v1, v0, 0x8

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 72
    move-object v1, v2

    check-cast v1, Ljava/lang/String;

    move-object v6, v1

    goto :goto_1

    :cond_1
    move-object/from16 v6, p4

    :goto_1
    and-int/lit8 v1, v0, 0x10

    if-eqz v1, :cond_2

    .line 73
    move-object v1, v2

    check-cast v1, Lcom/squareup/protos/client/IdPair;

    move-object v7, v1

    goto :goto_2

    :cond_2
    move-object/from16 v7, p5

    :goto_2
    and-int/lit8 v1, v0, 0x20

    if-eqz v1, :cond_3

    .line 74
    move-object v1, v2

    check-cast v1, Ljava/lang/String;

    move-object v8, v1

    goto :goto_3

    :cond_3
    move-object/from16 v8, p6

    :goto_3
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_4

    .line 75
    move-object v1, v2

    check-cast v1, Ljava/lang/String;

    move-object v9, v1

    goto :goto_4

    :cond_4
    move-object/from16 v9, p7

    :goto_4
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_5

    .line 76
    move-object v1, v2

    check-cast v1, Ljava/lang/String;

    move-object v10, v1

    goto :goto_5

    :cond_5
    move-object/from16 v10, p8

    :goto_5
    and-int/lit16 v1, v0, 0x100

    const/4 v2, 0x0

    if-eqz v1, :cond_6

    const/4 v11, 0x0

    goto :goto_6

    :cond_6
    move/from16 v11, p9

    :goto_6
    and-int/lit16 v1, v0, 0x200

    if-eqz v1, :cond_7

    const/4 v12, 0x0

    goto :goto_7

    :cond_7
    move/from16 v12, p10

    :goto_7
    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_8

    .line 79
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v1, "ZERO"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v13, v0

    goto :goto_8

    :cond_8
    move-object/from16 v13, p11

    :goto_8
    move-object v2, p0

    move-object v3, p1

    move-object/from16 v4, p2

    invoke-direct/range {v2 .. v13}, Lcom/squareup/activity/refund/ItemizationDetails;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/math/BigDecimal;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/activity/refund/ItemizationDetails;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/math/BigDecimal;ILjava/lang/Object;)Lcom/squareup/activity/refund/ItemizationDetails;
    .locals 12

    move-object v0, p0

    move/from16 v1, p12

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/activity/refund/ItemizationDetails;->name:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/activity/refund/ItemizationDetails;->totalMoney:Lcom/squareup/protos/common/Money;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/activity/refund/ItemizationDetails;->note:Ljava/lang/String;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceBillServerToken:Ljava/lang/String;

    goto :goto_3

    :cond_3
    move-object/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceItemizationTokenPair:Lcom/squareup/protos/client/IdPair;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceTipTenderToken:Ljava/lang/String;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/activity/refund/ItemizationDetails;->merchantCatalogToken:Ljava/lang/String;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/squareup/activity/refund/ItemizationDetails;->cogsId:Ljava/lang/String;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget v10, v0, Lcom/squareup/activity/refund/ItemizationDetails;->ordinal:I

    goto :goto_8

    :cond_8
    move/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget-boolean v11, v0, Lcom/squareup/activity/refund/ItemizationDetails;->isInventoryTracked:Z

    goto :goto_9

    :cond_9
    move/from16 v11, p10

    :goto_9
    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_a

    iget-object v1, v0, Lcom/squareup/activity/refund/ItemizationDetails;->quantity:Ljava/math/BigDecimal;

    goto :goto_a

    :cond_a
    move-object/from16 v1, p11

    :goto_a
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object/from16 p4, v5

    move-object/from16 p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move/from16 p9, v10

    move/from16 p10, v11

    move-object/from16 p11, v1

    invoke-virtual/range {p0 .. p11}, Lcom/squareup/activity/refund/ItemizationDetails;->copy(Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/math/BigDecimal;)Lcom/squareup/activity/refund/ItemizationDetails;

    move-result-object v0

    return-object v0
.end method

.method public static final of(Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/activity/refund/ItemizationDetails;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/activity/refund/ItemizationDetails;->Companion:Lcom/squareup/activity/refund/ItemizationDetails$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/activity/refund/ItemizationDetails$Companion;->of(Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/activity/refund/ItemizationDetails;

    move-result-object p0

    return-object p0
.end method

.method public static final ofRefundableItemization(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;Ljava/math/BigDecimal;Ljava/math/RoundingMode;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;",
            "Ljava/math/BigDecimal;",
            "Ljava/math/RoundingMode;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/ItemizationDetails;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/activity/refund/ItemizationDetails;->Companion:Lcom/squareup/activity/refund/ItemizationDetails$Companion;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/activity/refund/ItemizationDetails$Companion;->ofRefundableItemization(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;Ljava/math/BigDecimal;Ljava/math/RoundingMode;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final ofRefundableTip(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;Lcom/squareup/util/Res;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;",
            "Lcom/squareup/util/Res;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/ItemizationDetails;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/activity/refund/ItemizationDetails;->Companion:Lcom/squareup/activity/refund/ItemizationDetails$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/squareup/activity/refund/ItemizationDetails$Companion;->ofRefundableTip(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;Lcom/squareup/util/Res;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final ofRefundedItemization(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;Lcom/squareup/protos/client/bills/Itemization;Ljava/math/BigDecimal;Ljava/math/RoundingMode;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;",
            "Lcom/squareup/protos/client/bills/Itemization;",
            "Ljava/math/BigDecimal;",
            "Ljava/math/RoundingMode;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/ItemizationDetails;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/activity/refund/ItemizationDetails;->Companion:Lcom/squareup/activity/refund/ItemizationDetails$Companion;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/squareup/activity/refund/ItemizationDetails$Companion;->ofRefundedItemization(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;Lcom/squareup/protos/client/bills/Itemization;Ljava/math/BigDecimal;Ljava/math/RoundingMode;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final ofRefundedTip(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;Lcom/squareup/util/Res;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;",
            "Lcom/squareup/util/Res;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/ItemizationDetails;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/activity/refund/ItemizationDetails;->Companion:Lcom/squareup/activity/refund/ItemizationDetails$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/squareup/activity/refund/ItemizationDetails$Companion;->ofRefundedTip(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;Lcom/squareup/util/Res;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final component10()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->isInventoryTracked:Z

    return v0
.end method

.method public final component11()Ljava/math/BigDecimal;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->quantity:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->totalMoney:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->note:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceBillServerToken:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Lcom/squareup/protos/client/IdPair;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceItemizationTokenPair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public final component6()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceTipTenderToken:Ljava/lang/String;

    return-object v0
.end method

.method public final component7()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->merchantCatalogToken:Ljava/lang/String;

    return-object v0
.end method

.method public final component8()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->cogsId:Ljava/lang/String;

    return-object v0
.end method

.method public final component9()I
    .locals 1

    iget v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->ordinal:I

    return v0
.end method

.method public final copy(Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/math/BigDecimal;)Lcom/squareup/activity/refund/ItemizationDetails;
    .locals 13

    const-string v0, "name"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "totalMoney"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "note"

    move-object/from16 v4, p3

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "quantity"

    move-object/from16 v12, p11

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/activity/refund/ItemizationDetails;

    move-object v1, v0

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    invoke-direct/range {v1 .. v12}, Lcom/squareup/activity/refund/ItemizationDetails;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/math/BigDecimal;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/activity/refund/ItemizationDetails;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/activity/refund/ItemizationDetails;

    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/activity/refund/ItemizationDetails;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->totalMoney:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/activity/refund/ItemizationDetails;->totalMoney:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->note:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/activity/refund/ItemizationDetails;->note:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceBillServerToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/activity/refund/ItemizationDetails;->sourceBillServerToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceItemizationTokenPair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, p1, Lcom/squareup/activity/refund/ItemizationDetails;->sourceItemizationTokenPair:Lcom/squareup/protos/client/IdPair;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceTipTenderToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/activity/refund/ItemizationDetails;->sourceTipTenderToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->merchantCatalogToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/activity/refund/ItemizationDetails;->merchantCatalogToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->cogsId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/activity/refund/ItemizationDetails;->cogsId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->ordinal:I

    iget v1, p1, Lcom/squareup/activity/refund/ItemizationDetails;->ordinal:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->isInventoryTracked:Z

    iget-boolean v1, p1, Lcom/squareup/activity/refund/ItemizationDetails;->isInventoryTracked:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->quantity:Ljava/math/BigDecimal;

    iget-object p1, p1, Lcom/squareup/activity/refund/ItemizationDetails;->quantity:Ljava/math/BigDecimal;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCogsId()Ljava/lang/String;
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->cogsId:Ljava/lang/String;

    return-object v0
.end method

.method public final getMerchantCatalogToken()Ljava/lang/String;
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->merchantCatalogToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final getNote()Ljava/lang/String;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->note:Ljava/lang/String;

    return-object v0
.end method

.method public final getOrdinal()I
    .locals 1

    .line 77
    iget v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->ordinal:I

    return v0
.end method

.method public final getQuantity()Ljava/math/BigDecimal;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->quantity:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public final getSourceBillServerToken()Ljava/lang/String;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceBillServerToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getSourceItemizationTokenPair()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceItemizationTokenPair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public final getSourceTipTenderToken()Ljava/lang/String;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceTipTenderToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getTotalMoney()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->totalMoney:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->name:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/ItemizationDetails;->totalMoney:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/ItemizationDetails;->note:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceBillServerToken:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceItemizationTokenPair:Lcom/squareup/protos/client/IdPair;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceTipTenderToken:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/ItemizationDetails;->merchantCatalogToken:Ljava/lang/String;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/ItemizationDetails;->cogsId:Ljava/lang/String;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_7
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/activity/refund/ItemizationDetails;->ordinal:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/activity/refund/ItemizationDetails;->isInventoryTracked:Z

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    :cond_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/ItemizationDetails;->quantity:Ljava/math/BigDecimal;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_9
    add-int/2addr v0, v1

    return v0
.end method

.method public final isInventoryTracked()Z
    .locals 1

    .line 78
    iget-boolean v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->isInventoryTracked:Z

    return v0
.end method

.method public final isItemization()Z
    .locals 1

    .line 286
    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceItemizationTokenPair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isTip()Z
    .locals 1

    .line 288
    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceTipTenderToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ItemizationDetails(name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/ItemizationDetails;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", totalMoney="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/ItemizationDetails;->totalMoney:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", note="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/ItemizationDetails;->note:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", sourceBillServerToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceBillServerToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", sourceItemizationTokenPair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceItemizationTokenPair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", sourceTipTenderToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/ItemizationDetails;->sourceTipTenderToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", merchantCatalogToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/ItemizationDetails;->merchantCatalogToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", cogsId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/ItemizationDetails;->cogsId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", ordinal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/activity/refund/ItemizationDetails;->ordinal:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isInventoryTracked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/activity/refund/ItemizationDetails;->isInventoryTracked:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/ItemizationDetails;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
