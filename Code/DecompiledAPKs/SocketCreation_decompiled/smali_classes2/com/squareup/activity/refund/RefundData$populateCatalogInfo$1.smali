.class final Lcom/squareup/activity/refund/RefundData$populateCatalogInfo$1;
.super Ljava/lang/Object;
.source "RefundData.kt"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/activity/refund/RefundData;->populateCatalogInfo(Lcom/squareup/cogs/Cogs;)Lrx/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogTask<",
        "Lcom/squareup/activity/refund/RefundData;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRefundData.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RefundData.kt\ncom/squareup/activity/refund/RefundData$populateCatalogInfo$1\n+ 2 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,870:1\n67#2:871\n92#2,3:872\n1642#3,2:875\n*E\n*S KotlinDebug\n*F\n+ 1 RefundData.kt\ncom/squareup/activity/refund/RefundData$populateCatalogInfo$1\n*L\n789#1:871\n789#1,3:872\n798#1,2:875\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/activity/refund/RefundData;",
        "cogsLocal",
        "Lcom/squareup/shared/catalog/Catalog$Local;",
        "kotlin.jvm.PlatformType",
        "perform"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $cogsIds:Ljava/util/Set;

.field final synthetic this$0:Lcom/squareup/activity/refund/RefundData;


# direct methods
.method constructor <init>(Lcom/squareup/activity/refund/RefundData;Ljava/util/Set;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/activity/refund/RefundData$populateCatalogInfo$1;->this$0:Lcom/squareup/activity/refund/RefundData;

    iput-object p2, p0, Lcom/squareup/activity/refund/RefundData$populateCatalogInfo$1;->$cogsIds:Ljava/util/Set;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final perform(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/activity/refund/RefundData;
    .locals 40

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 784
    invoke-interface/range {p1 .. p1}, Lcom/squareup/shared/catalog/Catalog$Local;->readAppliedServerVersion()J

    move-result-wide v2

    .line 786
    const-class v4, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    iget-object v5, v0, Lcom/squareup/activity/refund/RefundData$populateCatalogInfo$1;->$cogsIds:Ljava/util/Set;

    .line 785
    invoke-interface {v1, v4, v5}, Lcom/squareup/shared/catalog/Catalog$Local;->findObjectsByIds(Ljava/lang/Class;Ljava/util/Set;)Ljava/util/Map;

    move-result-object v4

    const-string/jumbo v5, "variationsByIds"

    .line 789
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 871
    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v5, Ljava/util/Collection;

    .line 872
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    .line 790
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    const-string v9, "it.value"

    invoke-static {v8, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v8, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {v8}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object v8

    .line 792
    const-class v10, Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    invoke-static {v7, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v7, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {v7}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getItemId()Ljava/lang/String;

    move-result-object v7

    .line 791
    invoke-interface {v1, v10, v7}, Lcom/squareup/shared/catalog/Catalog$Local;->findById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v7

    const-string v9, "cogsLocal.findById(\n    \u2026ue.itemId\n              )"

    invoke-static {v7, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v7, Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-virtual {v7}, Lcom/squareup/shared/catalog/models/CatalogItem;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object v7

    .line 790
    invoke-static {v8, v7}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v7

    .line 793
    invoke-interface {v5, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 874
    :cond_0
    check-cast v5, Ljava/util/List;

    check-cast v5, Ljava/lang/Iterable;

    .line 795
    invoke-static {v5}, Lkotlin/collections/MapsKt;->toMap(Ljava/lang/Iterable;)Ljava/util/Map;

    move-result-object v23

    .line 796
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object v8, v1

    check-cast v8, Ljava/util/List;

    .line 797
    iget-object v1, v0, Lcom/squareup/activity/refund/RefundData$populateCatalogInfo$1;->this$0:Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {v1}, Lcom/squareup/activity/refund/RefundData;->getItemizationDetails()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 875
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v9, v5

    check-cast v9, Lcom/squareup/activity/refund/ItemizationDetails;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    .line 801
    invoke-virtual {v9}, Lcom/squareup/activity/refund/ItemizationDetails;->getCogsId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->isInventoryTracked()Z

    move-result v5

    move/from16 v19, v5

    goto :goto_2

    :cond_1
    const/4 v5, 0x0

    const/16 v19, 0x0

    :goto_2
    const/16 v17, 0x0

    const/16 v18, 0x0

    .line 802
    invoke-virtual {v9}, Lcom/squareup/activity/refund/ItemizationDetails;->getCogsId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    :cond_2
    const/4 v5, 0x0

    :goto_3
    move-object/from16 v16, v5

    const/16 v20, 0x0

    const/16 v21, 0x5bf

    const/16 v22, 0x0

    .line 800
    invoke-static/range {v9 .. v22}, Lcom/squareup/activity/refund/ItemizationDetails;->copy$default(Lcom/squareup/activity/refund/ItemizationDetails;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/math/BigDecimal;ILjava/lang/Object;)Lcom/squareup/activity/refund/ItemizationDetails;

    move-result-object v5

    .line 799
    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 806
    :cond_3
    iget-object v6, v0, Lcom/squareup/activity/refund/RefundData$populateCatalogInfo$1;->this$0:Lcom/squareup/activity/refund/RefundData;

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    .line 809
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v24

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const v38, 0x7ffcfffd

    const/16 v39, 0x0

    .line 806
    invoke-static/range {v6 .. v39}, Lcom/squareup/activity/refund/RefundData;->copy$default(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 0

    .line 82
    invoke-virtual {p0, p1}, Lcom/squareup/activity/refund/RefundData$populateCatalogInfo$1;->perform(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/activity/refund/RefundData;

    move-result-object p1

    return-object p1
.end method
