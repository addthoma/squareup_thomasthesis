.class public final Lcom/squareup/activity/refund/RefundCashDrawerShiftHelper$Companion;
.super Ljava/lang/Object;
.source "RefundCashDrawerShiftHelper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/refund/RefundCashDrawerShiftHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRefundCashDrawerShiftHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RefundCashDrawerShiftHelper.kt\ncom/squareup/activity/refund/RefundCashDrawerShiftHelper$Companion\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,79:1\n704#2:80\n777#2,2:81\n1360#2:83\n1429#2,3:84\n704#2:87\n777#2,2:88\n1550#2,2:90\n1550#2,3:92\n1552#2:95\n*E\n*S KotlinDebug\n*F\n+ 1 RefundCashDrawerShiftHelper.kt\ncom/squareup/activity/refund/RefundCashDrawerShiftHelper$Companion\n*L\n34#1:80\n34#1,2:81\n38#1:83\n38#1,3:84\n64#1:87\n64#1,2:88\n65#1,2:90\n65#1,3:92\n65#1:95\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0007J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u000cH\u0007\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RefundCashDrawerShiftHelper$Companion;",
        "",
        "()V",
        "addRefundTendersToManagedCashDrawer",
        "",
        "refundBillHistory",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "cashDrawerShiftManager",
        "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
        "shouldOpenDrawerForRefund",
        "",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Lcom/squareup/activity/refund/RefundCashDrawerShiftHelper$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final addRefundTendersToManagedCashDrawer(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;)V
    .locals 8
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "refundBillHistory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cashDrawerShiftManager"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-interface {p2}, Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;->cashManagementEnabledAndIsOpenShift()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 28
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->getSourceBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    .line 30
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->getRelatedRefundBills()Ljava/util/List;

    move-result-object v1

    const-string v2, "refundBillHistory.relatedRefundBills"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    .line 30
    check-cast v1, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 33
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->getTenders()Ljava/util/List;

    move-result-object p1

    const-string v2, "refundBillHistory.getTenders()"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 80
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 81
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v5, v3

    check-cast v5, Lcom/squareup/billhistory/model/TenderHistory;

    .line 35
    invoke-virtual {v1}, Lcom/squareup/server/payment/RelatedBillHistory;->getTendersRefundMoney()Ljava/util/Map;

    move-result-object v6

    iget-object v7, v5, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, v5, Lcom/squareup/billhistory/model/TenderHistory;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    sget-object v7, Lcom/squareup/billhistory/model/TenderHistory$Type;->CASH:Lcom/squareup/billhistory/model/TenderHistory$Type;

    if-eq v6, v7, :cond_3

    iget-object v5, v5, Lcom/squareup/billhistory/model/TenderHistory;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    sget-object v6, Lcom/squareup/billhistory/model/TenderHistory$Type;->OTHER:Lcom/squareup/billhistory/model/TenderHistory$Type;

    if-ne v5, v6, :cond_2

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :cond_3
    :goto_1
    if-eqz v4, :cond_1

    .line 36
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 82
    :cond_4
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 83
    new-instance p1, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {p1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 84
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 85
    check-cast v3, Lcom/squareup/billhistory/model/TenderHistory;

    .line 40
    iget-object v5, v3, Lcom/squareup/billhistory/model/TenderHistory;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    if-eqz v5, :cond_6

    sget-object v6, Lcom/squareup/activity/refund/RefundCashDrawerShiftHelper$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v5}, Lcom/squareup/billhistory/model/TenderHistory$Type;->ordinal()I

    move-result v5

    aget v5, v6, v5

    if-eq v5, v4, :cond_5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_6

    .line 42
    sget-object v5, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->OTHER_TENDER_REFUND:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    goto :goto_3

    .line 41
    :cond_5
    sget-object v5, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->CASH_TENDER_REFUND:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    .line 47
    :goto_3
    invoke-virtual {v1}, Lcom/squareup/server/payment/RelatedBillHistory;->getTendersRefundMoney()Ljava/util/Map;

    move-result-object v6

    iget-object v3, v3, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    .line 46
    invoke-interface {p2, v3, v5, v0}, Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;->addTenderWithId(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Ljava/lang/String;)V

    .line 49
    sget-object v3, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-interface {p1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 43
    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "We should have filtered out "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v3, Lcom/squareup/billhistory/model/TenderHistory;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 86
    :cond_7
    check-cast p1, Ljava/util/List;

    return-void
.end method

.method public final shouldOpenDrawerForRefund(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/settings/server/AccountStatusSettings;)Z
    .locals 7
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "refundBillHistory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->getRelatedRefundBills()Ljava/util/List;

    move-result-object v0

    const-string v1, "refundBillHistory.relatedRefundBills"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    .line 60
    check-cast v0, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 63
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->getTenders()Ljava/util/List;

    move-result-object p1

    const-string v1, "refundBillHistory.getTenders()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 87
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 88
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/billhistory/model/TenderHistory;

    .line 64
    invoke-virtual {v0}, Lcom/squareup/server/payment/RelatedBillHistory;->getTendersRefundMoney()Ljava/util/Map;

    move-result-object v4

    iget-object v3, v3, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-interface {v4, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 89
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 90
    instance-of p1, v1, Ljava/util/Collection;

    const/4 v0, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_2

    move-object p1, v1

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    goto/16 :goto_6

    .line 91
    :cond_2
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/TenderHistory;

    .line 67
    iget-object v3, v1, Lcom/squareup/billhistory/model/TenderHistory;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    if-nez v3, :cond_4

    goto :goto_1

    :cond_4
    sget-object v4, Lcom/squareup/activity/refund/RefundCashDrawerShiftHelper$Companion$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v3}, Lcom/squareup/billhistory/model/TenderHistory$Type;->ordinal()I

    move-result v3

    aget v3, v4, v3

    if-eq v3, v0, :cond_c

    const/4 v4, 0x2

    if-eq v3, v4, :cond_6

    :cond_5
    :goto_1
    const/4 v1, 0x0

    goto :goto_5

    .line 69
    :cond_6
    invoke-virtual {p2}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v3

    const-string v4, "accountStatusSettings.paymentSettings"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/settings/server/PaymentSettings;->getOtherTenderOptions()Ljava/util/List;

    move-result-object v3

    const-string v4, "accountStatusSettings.pa\u2026ttings.otherTenderOptions"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/Iterable;

    .line 92
    instance-of v4, v3, Ljava/util/Collection;

    if-eqz v4, :cond_7

    move-object v4, v3

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_7

    goto :goto_1

    .line 93
    :cond_7
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/server/account/protos/OtherTenderType;

    .line 71
    iget-object v5, v4, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    move-object v6, v1

    check-cast v6, Lcom/squareup/billhistory/model/OtherTenderHistory;

    iget v6, v6, Lcom/squareup/billhistory/model/OtherTenderHistory;->tenderType:I

    if-nez v5, :cond_9

    goto :goto_2

    :cond_9
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v5, v6, :cond_a

    iget-object v4, v4, Lcom/squareup/server/account/protos/OtherTenderType;->tender_opens_cash_drawer:Ljava/lang/Boolean;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    const/4 v4, 0x1

    goto :goto_3

    :cond_a
    :goto_2
    const/4 v4, 0x0

    :goto_3
    if-eqz v4, :cond_8

    goto :goto_4

    :cond_b
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.billhistory.model.OtherTenderHistory"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_c
    :goto_4
    const/4 v1, 0x1

    :goto_5
    if-eqz v1, :cond_3

    const/4 v2, 0x1

    :cond_d
    :goto_6
    return v2
.end method
