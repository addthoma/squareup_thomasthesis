.class public final Lcom/squareup/activity/refund/IssueRefundsRequests$Companion;
.super Ljava/lang/Object;
.source "IssueRefundsRequests.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/refund/IssueRefundsRequests;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nIssueRefundsRequests.kt\nKotlin\n*S Kotlin\n*F\n+ 1 IssueRefundsRequests.kt\ncom/squareup/activity/refund/IssueRefundsRequests$Companion\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n+ 4 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n*L\n1#1,568:1\n1360#2:569\n1429#2,3:570\n1288#2:573\n1313#2,3:574\n1316#2,3:584\n1360#2:597\n1429#2,3:598\n1866#2,7:601\n347#3,7:577\n501#3:587\n486#3,6:588\n67#4:594\n92#4,2:595\n94#4:608\n*E\n*S KotlinDebug\n*F\n+ 1 IssueRefundsRequests.kt\ncom/squareup/activity/refund/IssueRefundsRequests$Companion\n*L\n121#1:569\n121#1,3:570\n121#1:573\n121#1,3:574\n121#1,3:584\n121#1:597\n121#1,3:598\n121#1,7:601\n121#1,7:577\n121#1:587\n121#1,6:588\n121#1:594\n121#1,2:595\n121#1:608\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J*\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0007J\"\u0010\r\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00082\u0006\u0010\t\u001a\u00020\nH\u0007J \u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\nH\u0007\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/activity/refund/IssueRefundsRequests$Companion;",
        "",
        "()V",
        "createExchangeRequest",
        "Lcom/squareup/protos/client/bills/IssueRefundsRequest;",
        "userToken",
        "",
        "creatorDetails",
        "Lcom/squareup/protos/client/CreatorDetails;",
        "refundData",
        "Lcom/squareup/activity/refund/RefundData;",
        "order",
        "Lcom/squareup/payment/Order;",
        "createIssueRefundRequest",
        "createRestockRequest",
        "Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;",
        "refundBillToken",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 67
    invoke-direct {p0}, Lcom/squareup/activity/refund/IssueRefundsRequests$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final createExchangeRequest(Ljava/lang/String;Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/activity/refund/RefundData;Lcom/squareup/payment/Order;)Lcom/squareup/protos/client/bills/IssueRefundsRequest;
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string/jumbo v0, "userToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "refundData"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "order"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    invoke-static {p3}, Lcom/squareup/activity/refund/IssueRefundsRequestsKt;->access$createRefundsFromTenderDetails(Lcom/squareup/activity/refund/RefundData;)Ljava/util/List;

    move-result-object v0

    .line 99
    new-instance v1, Lcom/squareup/protos/client/bills/ReturnCartDetails$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/ReturnCartDetails$Builder;-><init>()V

    .line 100
    invoke-virtual {p4}, Lcom/squareup/payment/Order;->getCartProtoForExchange()Lcom/squareup/protos/client/bills/Cart;

    move-result-object p4

    invoke-virtual {v1, p4}, Lcom/squareup/protos/client/bills/ReturnCartDetails$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/bills/ReturnCartDetails$Builder;

    move-result-object p4

    .line 101
    invoke-virtual {p4}, Lcom/squareup/protos/client/bills/ReturnCartDetails$Builder;->build()Lcom/squareup/protos/client/bills/ReturnCartDetails;

    move-result-object p4

    .line 102
    new-instance v1, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;-><init>()V

    .line 104
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;

    .line 105
    invoke-virtual {v1, p2}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;

    .line 106
    invoke-virtual {p3}, Lcom/squareup/activity/refund/RefundData;->getClientToken()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->request_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;

    .line 107
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->refund_request(Ljava/util/List;)Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;

    .line 108
    invoke-virtual {v1, p4}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->return_cart_details(Lcom/squareup/protos/client/bills/ReturnCartDetails;)Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;

    .line 110
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->build()Lcom/squareup/protos/client/bills/IssueRefundsRequest;

    move-result-object p1

    const-string p2, "IssueRefundsRequest.Buil\u2026     }\n          .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final createIssueRefundRequest(Ljava/lang/String;Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/protos/client/bills/IssueRefundsRequest;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string/jumbo v0, "userToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "refundData"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-static {p3}, Lcom/squareup/activity/refund/IssueRefundsRequestsKt;->access$createRefundsFromTenderDetails(Lcom/squareup/activity/refund/RefundData;)Ljava/util/List;

    move-result-object v0

    .line 75
    invoke-static {p3}, Lcom/squareup/activity/refund/IssueRefundsRequestsKt;->access$createReturnCartDetailsOrNull(Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/protos/client/bills/ReturnCartDetails;

    move-result-object v1

    .line 76
    new-instance v2, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;-><init>()V

    .line 78
    invoke-virtual {v2, p1}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;

    .line 79
    invoke-virtual {v2, p2}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;

    .line 80
    invoke-virtual {p3}, Lcom/squareup/activity/refund/RefundData;->getClientToken()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->request_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;

    .line 81
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->refund_request(Ljava/util/List;)Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;

    .line 82
    invoke-virtual {v2, v1}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->return_cart_details(Lcom/squareup/protos/client/bills/ReturnCartDetails;)Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;

    .line 84
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->build()Lcom/squareup/protos/client/bills/IssueRefundsRequest;

    move-result-object p1

    const-string p2, "IssueRefundsRequest.Buil\u2026     }\n          .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final createRestockRequest(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;
    .locals 8
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string/jumbo v0, "userToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "refundBillToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "refundData"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    new-instance v0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;-><init>()V

    .line 122
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;->idempotency_token(Ljava/lang/String;)Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;

    .line 124
    invoke-virtual {p3}, Lcom/squareup/activity/refund/RefundData;->getRestockIndices()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 569
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 570
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 571
    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v4

    .line 125
    invoke-virtual {p3}, Lcom/squareup/activity/refund/RefundData;->getItemizationDetails()Ljava/util/List;

    move-result-object v5

    invoke-virtual {p3}, Lcom/squareup/activity/refund/RefundData;->getSelectedIndices()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v4

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/activity/refund/ItemizationDetails;

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 572
    :cond_0
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 573
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v1, Ljava/util/Map;

    .line 574
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 575
    move-object v5, v4

    check-cast v5, Lcom/squareup/activity/refund/ItemizationDetails;

    .line 126
    invoke-virtual {v5}, Lcom/squareup/activity/refund/ItemizationDetails;->getMerchantCatalogToken()Ljava/lang/String;

    move-result-object v5

    .line 577
    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_1

    .line 576
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 580
    invoke-interface {v1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 576
    :cond_1
    check-cast v6, Ljava/util/List;

    .line 584
    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 587
    :cond_2
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v2, Ljava/util/Map;

    .line 588
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 127
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_4

    const/4 v5, 0x1

    goto :goto_3

    :cond_4
    const/4 v5, 0x0

    :goto_3
    if-eqz v5, :cond_3

    .line 590
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v2, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 594
    :cond_5
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 595
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 129
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Iterable;

    .line 597
    new-instance v6, Ljava/util/ArrayList;

    invoke-static {v5, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v7

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v6, Ljava/util/Collection;

    .line 598
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    .line 599
    check-cast v7, Lcom/squareup/activity/refund/ItemizationDetails;

    .line 129
    invoke-virtual {v7}, Lcom/squareup/activity/refund/ItemizationDetails;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 600
    :cond_6
    check-cast v6, Ljava/util/List;

    check-cast v6, Ljava/lang/Iterable;

    .line 601
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 602
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 603
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 604
    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 605
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/math/BigDecimal;

    check-cast v6, Ljava/math/BigDecimal;

    .line 130
    invoke-virtual {v6, v7}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v6

    const-string v7, "this.add(other)"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_6

    .line 129
    :cond_7
    check-cast v6, Ljava/math/BigDecimal;

    .line 131
    new-instance v5, Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    invoke-direct {v5}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;-><init>()V

    .line 133
    invoke-virtual {v5, p1}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    .line 134
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->variation_token(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    .line 135
    invoke-virtual {v6}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->adjust_quantity_decimal(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    .line 136
    sget-object v4, Lcom/squareup/protos/client/InventoryAdjustmentReason;->RETURNED:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    invoke-virtual {v5, v4}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->reason(Lcom/squareup/protos/client/InventoryAdjustmentReason;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    .line 137
    invoke-virtual {v5, p2}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->refund_bill_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    .line 138
    invoke-virtual {p3}, Lcom/squareup/activity/refund/RefundData;->getCatalogVersion()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->catalog_version(Ljava/lang/Long;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    .line 140
    invoke-virtual {v5}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->build()Lcom/squareup/protos/client/InventoryAdjustment;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 602
    :cond_8
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "Empty collection can\'t be reduced."

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 608
    :cond_9
    check-cast v1, Ljava/util/List;

    .line 123
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;->adjustments(Ljava/util/List;)Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;

    .line 144
    invoke-virtual {v0}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;->build()Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;

    move-result-object p1

    const-string p2, "BatchAdjustVariationInve\u2026     }\n          .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
