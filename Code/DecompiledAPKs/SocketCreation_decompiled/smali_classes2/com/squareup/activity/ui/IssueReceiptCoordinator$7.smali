.class Lcom/squareup/activity/ui/IssueReceiptCoordinator$7;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "IssueReceiptCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/activity/ui/IssueReceiptCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/activity/ui/IssueReceiptCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/activity/ui/IssueReceiptCoordinator;)V
    .locals 0

    .line 211
    iput-object p1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator$7;->this$0:Lcom/squareup/activity/ui/IssueReceiptCoordinator;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x4

    if-ne p2, p1, :cond_0

    .line 214
    iget-object p1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator$7;->this$0:Lcom/squareup/activity/ui/IssueReceiptCoordinator;

    invoke-static {p1}, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->access$000(Lcom/squareup/activity/ui/IssueReceiptCoordinator;)Lcom/squareup/activity/ui/IssueReceiptCoordinator$EventHandler;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/activity/ui/IssueReceiptCoordinator$EventHandler;->sendEmailReceipt()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
