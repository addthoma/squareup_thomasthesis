.class public Lcom/squareup/activity/CurrentBill;
.super Ljava/lang/Object;
.source "CurrentBill.java"


# instance fields
.field private bill:Lcom/squareup/billhistory/model/BillHistory;

.field private final onChanged:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/activity/CurrentBill;->onChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method private equalIds(Lcom/squareup/billhistory/model/BillHistory;)Z
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/squareup/activity/CurrentBill;->bill:Lcom/squareup/billhistory/model/BillHistory;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    iget-object p1, p1, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    invoke-static {v0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public clearTenderId()V
    .locals 2

    .line 147
    iget-object v0, p0, Lcom/squareup/activity/CurrentBill;->bill:Lcom/squareup/billhistory/model/BillHistory;

    if-nez v0, :cond_0

    return-void

    .line 150
    :cond_0
    new-instance v1, Lcom/squareup/billhistory/model/BillHistory$Builder;

    invoke-direct {v1, v0}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistory;)V

    iget-object v0, p0, Lcom/squareup/activity/CurrentBill;->bill:Lcom/squareup/billhistory/model/BillHistory;

    iget-object v0, v0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    .line 151
    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistoryId;->withoutTender()Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setId(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object v0

    .line 152
    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/activity/CurrentBill;->bill:Lcom/squareup/billhistory/model/BillHistory;

    return-void
.end method

.method public get()Lcom/squareup/billhistory/model/BillHistory;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/activity/CurrentBill;->bill:Lcom/squareup/billhistory/model/BillHistory;

    return-object v0
.end method

.method public getId()Lcom/squareup/billhistory/model/BillHistoryId;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/activity/CurrentBill;->bill:Lcom/squareup/billhistory/model/BillHistory;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public hasTenderId()Z
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/squareup/activity/CurrentBill;->bill:Lcom/squareup/billhistory/model/BillHistory;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistoryId;->getTenderId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isEqualTo(Lcom/squareup/billhistory/model/BillHistory;)Z
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/squareup/activity/CurrentBill;->bill:Lcom/squareup/billhistory/model/BillHistory;

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 168
    :cond_0
    iget-object v0, v0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    iget-object p1, p1, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    invoke-static {v0, p1}, Lcom/squareup/billhistory/Bills;->idsMatch(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/billhistory/model/BillHistoryId;)Z

    move-result p1

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public isSet()Z
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/activity/CurrentBill;->bill:Lcom/squareup/billhistory/model/BillHistory;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method onBillIdUpdated(Lcom/squareup/billhistory/Bills$BillIdChanged;)V
    .locals 2

    .line 176
    iget-object v0, p0, Lcom/squareup/activity/CurrentBill;->bill:Lcom/squareup/billhistory/model/BillHistory;

    if-nez v0, :cond_0

    return-void

    .line 179
    :cond_0
    iget-object v0, v0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    iget-object v1, p1, Lcom/squareup/billhistory/Bills$BillIdChanged;->oldBillId:Lcom/squareup/billhistory/model/BillHistoryId;

    invoke-static {v0, v1}, Lcom/squareup/billhistory/Bills;->idsMatch(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/billhistory/model/BillHistoryId;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 180
    new-instance v0, Lcom/squareup/billhistory/model/BillHistory$Builder;

    iget-object v1, p0, Lcom/squareup/activity/CurrentBill;->bill:Lcom/squareup/billhistory/model/BillHistory;

    invoke-direct {v0, v1}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistory;)V

    iget-object p1, p1, Lcom/squareup/billhistory/Bills$BillIdChanged;->newBillId:Lcom/squareup/billhistory/model/BillHistoryId;

    .line 181
    invoke-virtual {v0, p1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setId(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    .line 182
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/activity/CurrentBill;->bill:Lcom/squareup/billhistory/model/BillHistory;

    :cond_1
    return-void
.end method

.method public onChanged()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/squareup/activity/CurrentBill;->onChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public registerOnBadBus(Lmortar/MortarScope;Lcom/squareup/badbus/BadBus;)V
    .locals 1

    .line 172
    const-class v0, Lcom/squareup/billhistory/Bills$BillIdChanged;

    invoke-virtual {p2, v0}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p2

    new-instance v0, Lcom/squareup/activity/-$$Lambda$GIKZxXg5hcl0qSS5TodvFZtpHmM;

    invoke-direct {v0, p0}, Lcom/squareup/activity/-$$Lambda$GIKZxXg5hcl0qSS5TodvFZtpHmM;-><init>(Lcom/squareup/activity/CurrentBill;)V

    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public safelyReplace(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 2

    const-string v0, "bill"

    .line 91
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 92
    iget-object v0, p1, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    const-string v1, "billId"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/activity/CurrentBill;->equalIds(Lcom/squareup/billhistory/model/BillHistory;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 97
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/activity/CurrentBill;->getId()Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistoryId;->getTenderId()Ljava/lang/String;

    move-result-object v0

    .line 98
    iput-object p1, p0, Lcom/squareup/activity/CurrentBill;->bill:Lcom/squareup/billhistory/model/BillHistory;

    if-eqz v0, :cond_1

    .line 100
    invoke-virtual {p0, v0}, Lcom/squareup/activity/CurrentBill;->setTenderId(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public set(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 2

    const-string v0, "bill"

    .line 70
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 71
    iget-object v0, p1, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    const-string v1, "billId"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 72
    invoke-direct {p0, p1}, Lcom/squareup/activity/CurrentBill;->equalIds(Lcom/squareup/billhistory/model/BillHistory;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 75
    :cond_0
    iput-object p1, p0, Lcom/squareup/activity/CurrentBill;->bill:Lcom/squareup/billhistory/model/BillHistory;

    .line 76
    iget-object p1, p0, Lcom/squareup/activity/CurrentBill;->onChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public setTenderId(Ljava/lang/String;)V
    .locals 2

    const-string v0, "tenderId"

    .line 127
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 128
    iget-object v0, p0, Lcom/squareup/activity/CurrentBill;->bill:Lcom/squareup/billhistory/model/BillHistory;

    if-nez v0, :cond_0

    return-void

    .line 131
    :cond_0
    new-instance v1, Lcom/squareup/billhistory/model/BillHistory$Builder;

    invoke-direct {v1, v0}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistory;)V

    iget-object v0, p0, Lcom/squareup/activity/CurrentBill;->bill:Lcom/squareup/billhistory/model/BillHistory;

    iget-object v0, v0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    .line 132
    invoke-virtual {v0, p1}, Lcom/squareup/billhistory/model/BillHistoryId;->forTenderId(Ljava/lang/String;)Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setId(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    .line 133
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/activity/CurrentBill;->bill:Lcom/squareup/billhistory/model/BillHistory;

    return-void
.end method
