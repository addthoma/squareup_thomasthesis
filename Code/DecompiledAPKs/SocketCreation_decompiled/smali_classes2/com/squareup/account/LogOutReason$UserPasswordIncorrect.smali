.class public final Lcom/squareup/account/LogOutReason$UserPasswordIncorrect;
.super Lcom/squareup/account/LogOutReason;
.source "LogOutReason.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/account/LogOutReason;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UserPasswordIncorrect"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/account/LogOutReason$UserPasswordIncorrect;",
        "Lcom/squareup/account/LogOutReason;",
        "()V",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/account/LogOutReason$UserPasswordIncorrect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 17
    new-instance v0, Lcom/squareup/account/LogOutReason$UserPasswordIncorrect;

    invoke-direct {v0}, Lcom/squareup/account/LogOutReason$UserPasswordIncorrect;-><init>()V

    sput-object v0, Lcom/squareup/account/LogOutReason$UserPasswordIncorrect;->INSTANCE:Lcom/squareup/account/LogOutReason$UserPasswordIncorrect;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const-string/jumbo v0, "user/pass incorrect"

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0, v0, v1}, Lcom/squareup/account/LogOutReason;-><init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
