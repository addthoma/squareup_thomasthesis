.class public final Lcom/squareup/account/FileBackedAuthenticator_Factory;
.super Ljava/lang/Object;
.source "FileBackedAuthenticator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/account/FileBackedAuthenticator;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/PersistentAccountService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/LogoutService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/PersistentAccountService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/LogoutService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/account/FileBackedAuthenticator_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/account/FileBackedAuthenticator_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/account/FileBackedAuthenticator_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/account/FileBackedAuthenticator_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p5, p0, Lcom/squareup/account/FileBackedAuthenticator_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p6, p0, Lcom/squareup/account/FileBackedAuthenticator_Factory;->arg5Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/account/FileBackedAuthenticator_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/PersistentAccountService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/LogoutService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ">;)",
            "Lcom/squareup/account/FileBackedAuthenticator_Factory;"
        }
    .end annotation

    .line 51
    new-instance v7, Lcom/squareup/account/FileBackedAuthenticator_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/account/FileBackedAuthenticator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/account/PersistentAccountService;Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/analytics/Analytics;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/server/account/LogoutService;Lcom/squareup/queue/CorruptQueueRecorder;)Lcom/squareup/account/FileBackedAuthenticator;
    .locals 8

    .line 57
    new-instance v7, Lcom/squareup/account/FileBackedAuthenticator;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/account/FileBackedAuthenticator;-><init>(Lcom/squareup/account/PersistentAccountService;Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/analytics/Analytics;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/server/account/LogoutService;Lcom/squareup/queue/CorruptQueueRecorder;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/account/FileBackedAuthenticator;
    .locals 7

    .line 43
    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/account/PersistentAccountService;

    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/accountstatus/PersistentAccountStatusService;

    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/log/OhSnapLogger;

    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/server/account/LogoutService;

    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/queue/CorruptQueueRecorder;

    invoke-static/range {v1 .. v6}, Lcom/squareup/account/FileBackedAuthenticator_Factory;->newInstance(Lcom/squareup/account/PersistentAccountService;Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/analytics/Analytics;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/server/account/LogoutService;Lcom/squareup/queue/CorruptQueueRecorder;)Lcom/squareup/account/FileBackedAuthenticator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/account/FileBackedAuthenticator_Factory;->get()Lcom/squareup/account/FileBackedAuthenticator;

    move-result-object v0

    return-object v0
.end method
