.class public interface abstract Lcom/squareup/account/LoggedInStatusProvider;
.super Ljava/lang/Object;
.source "LoggedInStatusProvider.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;,
        Lcom/squareup/account/LoggedInStatusProvider$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\u0006J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u000e\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005H&\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/account/LoggedInStatusProvider;",
        "",
        "lastLoggedInStatus",
        "Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;",
        "loggedInStatus",
        "Lio/reactivex/Observable;",
        "LoggedInStatus",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract lastLoggedInStatus()Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;
.end method

.method public abstract loggedInStatus()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;",
            ">;"
        }
    .end annotation
.end method
