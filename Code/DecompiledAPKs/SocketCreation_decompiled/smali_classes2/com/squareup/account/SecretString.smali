.class public final Lcom/squareup/account/SecretString;
.super Ljava/lang/Object;
.source "SecretString.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/account/SecretString$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSecretString.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SecretString.kt\ncom/squareup/account/SecretString\n*L\n1#1,42:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0013\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u0096\u0002J\u0008\u0010\r\u001a\u00020\u0008H\u0016J\u0008\u0010\u000e\u001a\u00020\u0003H\u0016J\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0008H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/account/SecretString;",
        "Landroid/os/Parcelable;",
        "secretValue",
        "",
        "(Ljava/lang/String;)V",
        "getSecretValue",
        "()Ljava/lang/String;",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/account/SecretString;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/account/SecretString$Companion;

.field private static final EMPTY:Lcom/squareup/account/SecretString;


# instance fields
.field private final secretValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/account/SecretString$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/account/SecretString$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/account/SecretString;->Companion:Lcom/squareup/account/SecretString$Companion;

    const-string v0, ""

    .line 31
    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/account/SecretStringKt;->toSecretString(Ljava/lang/CharSequence;)Lcom/squareup/account/SecretString;

    move-result-object v0

    sput-object v0, Lcom/squareup/account/SecretString;->EMPTY:Lcom/squareup/account/SecretString;

    .line 33
    new-instance v0, Lcom/squareup/account/SecretString$Companion$CREATOR$1;

    invoke-direct {v0}, Lcom/squareup/account/SecretString$Companion$CREATOR$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/account/SecretString;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const-string v0, "secretValue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/account/SecretString;->secretValue:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$getEMPTY$cp()Lcom/squareup/account/SecretString;
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/account/SecretString;->EMPTY:Lcom/squareup/account/SecretString;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 19
    move-object v0, p0

    check-cast v0, Lcom/squareup/account/SecretString;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    .line 20
    :cond_0
    instance-of v0, p1, Lcom/squareup/account/SecretString;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/account/SecretString;->secretValue:Ljava/lang/String;

    check-cast p1, Lcom/squareup/account/SecretString;

    iget-object p1, p1, Lcom/squareup/account/SecretString;->secretValue:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final getSecretValue()Ljava/lang/String;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/account/SecretString;->secretValue:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/account/SecretString;->secretValue:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/account/SecretString;->secretValue:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const-string v0, ""

    goto :goto_1

    :cond_1
    const-string v0, "<secret string>"

    :goto_1
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
