.class public abstract Lcom/squareup/account/PreferencesCacheLoggedInModule;
.super Ljava/lang/Object;
.source "PreferencesCacheLoggedInModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static providePreferencesInProgress(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;"
        }
    .end annotation

    .line 20
    const-class v0, Lcom/squareup/server/account/protos/PreferencesRequest;

    const-string v1, "preferences-in-progress.json"

    invoke-static {p0, v1, p1, v0}, Lcom/squareup/settings/GsonLocalSetting;->forClass(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/Class;)Lcom/squareup/settings/GsonLocalSetting;

    move-result-object p0

    return-object p0
.end method

.method static providePreferencesOnDeck(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;"
        }
    .end annotation

    .line 26
    const-class v0, Lcom/squareup/server/account/protos/PreferencesRequest;

    const-string v1, "preferences-on-deck.json"

    invoke-static {p0, v1, p1, v0}, Lcom/squareup/settings/GsonLocalSetting;->forClass(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/Class;)Lcom/squareup/settings/GsonLocalSetting;

    move-result-object p0

    return-object p0
.end method
