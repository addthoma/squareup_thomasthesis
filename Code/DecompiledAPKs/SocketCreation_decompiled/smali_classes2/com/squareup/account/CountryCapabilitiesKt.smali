.class public final Lcom/squareup/account/CountryCapabilitiesKt;
.super Ljava/lang/Object;
.source "CountryCapabilities.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0005\"\u0017\u0010\u0000\u001a\u00020\u0001*\u0004\u0018\u00010\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0017\u0010\u0005\u001a\u00020\u0001*\u0004\u0018\u00010\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0004\u00a8\u0006\u0007"
    }
    d2 = {
        "hasFreeReaderPromo",
        "",
        "Lcom/squareup/CountryCode;",
        "getHasFreeReaderPromo",
        "(Lcom/squareup/CountryCode;)Z",
        "hasPayments",
        "getHasPayments",
        "loggedout_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getHasFreeReaderPromo(Lcom/squareup/CountryCode;)Z
    .locals 1

    .line 22
    sget-object v0, Lcom/squareup/CountryCode;->CA:Lcom/squareup/CountryCode;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static final getHasPayments(Lcom/squareup/CountryCode;)Z
    .locals 0

    if-eqz p0, :cond_0

    .line 13
    iget-boolean p0, p0, Lcom/squareup/CountryCode;->hasPayments:Z

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
