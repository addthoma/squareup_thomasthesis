.class public Lcom/squareup/account/PendingPreferencesCache;
.super Lcom/squareup/account/DefaultLogInResponseCache;
.source "PendingPreferencesCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;,
        Lcom/squareup/account/PendingPreferencesCache$LoggedInDependenciesInjector;
    }
.end annotation


# instance fields
.field private appDelegate:Lcom/squareup/AppDelegate;

.field private loggedInDependencies:Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;


# direct methods
.method public constructor <init>(Lcom/squareup/AppDelegate;Ljava/io/File;Lcom/squareup/persistent/PersistentFactory;Landroid/content/SharedPreferences;Ljavax/inject/Provider;Landroid/app/Application;Lcom/squareup/util/PosBuild;Lcom/google/gson/Gson;Lcom/squareup/log/CrashReporter;Lcom/squareup/encryption/KeystoreEncryptor;Lcom/squareup/firebase/versions/PlayServicesVersions;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/AppDelegate;",
            "Ljava/io/File;",
            "Lcom/squareup/persistent/PersistentFactory;",
            "Landroid/content/SharedPreferences;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Landroid/app/Application;",
            "Lcom/squareup/util/PosBuild;",
            "Lcom/google/gson/Gson;",
            "Lcom/squareup/log/CrashReporter;",
            "Lcom/squareup/encryption/KeystoreEncryptor;",
            "Lcom/squareup/firebase/versions/PlayServicesVersions;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object/from16 v4, p10

    move-object/from16 v5, p6

    move-object/from16 v6, p5

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p11

    .line 54
    invoke-direct/range {v0 .. v10}, Lcom/squareup/account/DefaultLogInResponseCache;-><init>(Ljava/io/File;Lcom/squareup/persistent/PersistentFactory;Landroid/content/SharedPreferences;Lcom/squareup/encryption/KeystoreEncryptor;Landroid/app/Application;Ljavax/inject/Provider;Lcom/squareup/util/PosBuild;Lcom/google/gson/Gson;Lcom/squareup/log/CrashReporter;Lcom/squareup/firebase/versions/PlayServicesVersions;)V

    move-object v1, p1

    .line 56
    iput-object v1, v0, Lcom/squareup/account/PendingPreferencesCache;->appDelegate:Lcom/squareup/AppDelegate;

    return-void
.end method


# virtual methods
.method public clearCache()V
    .locals 1

    .line 98
    invoke-super {p0}, Lcom/squareup/account/DefaultLogInResponseCache;->clearCache()V

    const/4 v0, 0x0

    .line 99
    iput-object v0, p0, Lcom/squareup/account/PendingPreferencesCache;->loggedInDependencies:Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;

    return-void
.end method

.method public getCanonicalStatus()Lcom/squareup/server/account/protos/AccountStatusResponse;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 79
    invoke-super {p0}, Lcom/squareup/account/DefaultLogInResponseCache;->getCanonicalStatus()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    .line 80
    iget-object v1, p0, Lcom/squareup/account/PendingPreferencesCache;->loggedInDependencies:Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;

    if-nez v1, :cond_0

    return-object v0

    .line 83
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    if-eqz v0, :cond_1

    .line 84
    iget-object v1, p0, Lcom/squareup/account/PendingPreferencesCache;->loggedInDependencies:Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;

    iget-object v1, v1, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->userId:Ljava/lang/String;

    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->id:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache;->loggedInDependencies:Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;

    invoke-virtual {v0}, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->getOverlaidStatus()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    return-object v0

    .line 85
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "LoggedInDependencies for wrong user"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onLoggedIn()V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache;->loggedInDependencies:Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;

    if-eqz v0, :cond_0

    return-void

    .line 68
    :cond_0
    new-instance v0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;

    invoke-super {p0}, Lcom/squareup/account/DefaultLogInResponseCache;->getCanonicalStatus()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;-><init>(Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    .line 69
    iget-object v1, p0, Lcom/squareup/account/PendingPreferencesCache;->appDelegate:Lcom/squareup/AppDelegate;

    const-class v2, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependenciesInjector;

    .line 70
    invoke-interface {v1, v2}, Lcom/squareup/AppDelegate;->getLoggedInComponent(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependenciesInjector;

    .line 72
    invoke-interface {v1, v0}, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependenciesInjector;->inject(Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;)V

    .line 75
    iput-object v0, p0, Lcom/squareup/account/PendingPreferencesCache;->loggedInDependencies:Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;

    return-void
.end method

.method pendingPreferencesChanged()V
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache;->loggedInDependencies:Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->pendingPreferencesChanged()V

    :cond_0
    return-void
.end method

.method public replaceCache(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 93
    invoke-super {p0, p1, p2}, Lcom/squareup/account/DefaultLogInResponseCache;->replaceCache(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    .line 94
    iget-object p1, p0, Lcom/squareup/account/PendingPreferencesCache;->loggedInDependencies:Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;

    if-eqz p1, :cond_0

    invoke-static {p1, p2}, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->access$000(Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    :cond_0
    return-void
.end method

.method public setPreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/protos/Preferences;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 106
    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache;->loggedInDependencies:Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;

    if-nez v0, :cond_0

    .line 107
    invoke-super {p0, p1}, Lcom/squareup/account/DefaultLogInResponseCache;->setPreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    .line 110
    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->setPreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)V

    .line 111
    new-instance p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    .line 112
    invoke-virtual {p0}, Lcom/squareup/account/PendingPreferencesCache;->getCanonicalStatus()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPreferences(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Preferences;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;-><init>(Ljava/lang/Object;)V

    .line 111
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
