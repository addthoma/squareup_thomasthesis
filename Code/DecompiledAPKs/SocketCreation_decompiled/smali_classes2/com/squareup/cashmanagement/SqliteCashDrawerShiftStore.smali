.class public Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SqliteCashDrawerShiftStore.java"

# interfaces
.implements Lcom/squareup/cashmanagement/CashDrawerShiftStore;


# static fields
.field private static final CREATE:Ljava/lang/String;

.field private static final CREATE_START_DATE_INDEX:Ljava/lang/String;

.field private static final CREATE_STATE_INDEX:Ljava/lang/String;

.field public static final DATABASE_NAME:Ljava/lang/String; = "cash_drawer_shifts"

.field private static final DATABASE_VERSION:I = 0x1

.field static final DESCRIPTION:Ljava/lang/String; = "description"

.field private static final GET_OPEN_CASH_DRAWER_SHIFT:Ljava/lang/String;

.field public static final INDEX_START_DATE:Ljava/lang/String; = "index_start_date"

.field public static final INDEX_STATE:Ljava/lang/String; = "index_state"

.field private static final OPEN:I

.field private static final QUERY_CASH_DRAWER_SHIFTS_BY_STATE:Lcom/squareup/phrase/Phrase;

.field private static final RETRIEVE_CASH_DRAWER_SHIFT_BY_ID:Lcom/squareup/phrase/Phrase;

.field static final SHIFT_ID:Ljava/lang/String; = "shift_id"

.field static final SHIFT_PROTO_BLOB:Ljava/lang/String; = "data"

.field static final START_DATE:Ljava/lang/String; = "started_date"

.field static final STATE:Ljava/lang/String; = "state"

.field private static final TABLE_NAME:Ljava/lang/String; = "cash_drawer_shifts_table"

.field private static final WHERE_CLAUSE_FOR_STATE_AND_DATE:Lcom/squareup/phrase/Phrase;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 23
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->OPEN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    invoke-virtual {v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->getValue()I

    move-result v0

    sput v0, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->OPEN:I

    const-string v0, "CREATE TABLE {table} ( {id} STRING PRIMARY KEY, {start_date} LONG NOT NULL, {description} STRING, {state} INT NOT NULL, {proto_blob} BLOB )"

    .line 37
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "cash_drawer_shifts_table"

    const-string v2, "table"

    .line 46
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v3, "id"

    const-string v4, "shift_id"

    .line 47
    invoke-virtual {v0, v3, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v5, "start_date"

    const-string v6, "started_date"

    .line 48
    invoke-virtual {v0, v5, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v7, "description"

    .line 49
    invoke-virtual {v0, v7, v7}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v8, "state"

    .line 50
    invoke-virtual {v0, v8, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v9, "data"

    const-string v10, "proto_blob"

    .line 51
    invoke-virtual {v0, v10, v9}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 53
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->CREATE:Ljava/lang/String;

    const-string v0, "CREATE INDEX {index} ON {table} ({state})"

    .line 55
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v10, "index"

    const-string v11, "index_state"

    .line 58
    invoke-virtual {v0, v10, v11}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 59
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 60
    invoke-virtual {v0, v8, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 62
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->CREATE_STATE_INDEX:Ljava/lang/String;

    const-string v0, "CREATE INDEX {index} ON {table} ({start_date})"

    .line 64
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v11, "index_start_date"

    .line 67
    invoke-virtual {v0, v10, v11}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 68
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 69
    invoke-virtual {v0, v5, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 71
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->CREATE_START_DATE_INDEX:Ljava/lang/String;

    const-string v0, "SELECT {data} FROM {table} WHERE {state_column} IS {state}"

    .line 73
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 77
    invoke-virtual {v0, v9, v9}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 78
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v10, "state_column"

    .line 79
    invoke-virtual {v0, v10, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sget v11, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->OPEN:I

    .line 80
    invoke-virtual {v0, v8, v11}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 82
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->GET_OPEN_CASH_DRAWER_SHIFT:Ljava/lang/String;

    const-string v0, "SELECT {data} FROM {table} WHERE {id_column} IS \'{id_value}\'"

    .line 84
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 88
    invoke-virtual {v0, v9, v9}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 89
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v9, "id_column"

    .line 90
    invoke-virtual {v0, v9, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sput-object v0, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->RETRIEVE_CASH_DRAWER_SHIFT_BY_ID:Lcom/squareup/phrase/Phrase;

    const-string v0, "SELECT {id}, {description}, {start_date} FROM {table} WHERE {state_column} IS {state} ORDER BY {start_date} DESC"

    .line 92
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 97
    invoke-virtual {v0, v3, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 98
    invoke-virtual {v0, v7, v7}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 99
    invoke-virtual {v0, v5, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 100
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 101
    invoke-virtual {v0, v10, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sput-object v0, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->QUERY_CASH_DRAWER_SHIFTS_BY_STATE:Lcom/squareup/phrase/Phrase;

    const-string v0, " {state_column} IS {state} AND {date_column} < {started_date}"

    .line 104
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 107
    invoke-virtual {v0, v10, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "date_column"

    .line 108
    invoke-virtual {v0, v1, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    sput-object v0, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->WHERE_CLAUSE_FOR_STATE_AND_DATE:Lcom/squareup/phrase/Phrase;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/io/File;)V
    .locals 2

    .line 111
    new-instance v0, Ljava/io/File;

    const-string v1, "cash_drawer_shifts.db"

    invoke-direct {v0, p2, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    return-void
.end method

.method private static buildCashDrawerShiftContentValues(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)Landroid/content/ContentValues;
    .locals 3

    .line 247
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 248
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_cash_drawer_shift_id:Ljava/lang/String;

    const-string v2, "shift_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v1, v1, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Times;->requireIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "started_date"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 250
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->description:Ljava/lang/String;

    const-string v2, "description"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->state:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->getValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "state"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 252
    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v1, p0}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object p0

    const-string v1, "data"

    invoke-virtual {v0, v1, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    return-object v0
.end method

.method private protoFromByteArray([B)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;
    .locals 1

    .line 258
    :try_start_0
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 260
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 2

    monitor-enter p0

    :try_start_0
    const-string v0, "close()"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 242
    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 243
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public dropAllCashDrawerShifts()V
    .locals 3

    .line 231
    invoke-virtual {p0}, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 232
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    const-string v1, "cash_drawer_shifts_table"

    const/4 v2, 0x0

    .line 234
    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 235
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 238
    throw v1
.end method

.method public dropCashDrawerShifts(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;Ljava/util/Date;)V
    .locals 3

    .line 215
    invoke-virtual {p0}, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 216
    sget-object v1, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->WHERE_CLAUSE_FOR_STATE_AND_DATE:Lcom/squareup/phrase/Phrase;

    .line 217
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->getValue()I

    move-result p1

    const-string v2, "state"

    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 218
    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p2

    const-string v1, "started_date"

    invoke-virtual {p1, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 219
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 220
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 221
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    const-string p2, "cash_drawer_shifts_table"

    const/4 v1, 0x0

    .line 223
    invoke-virtual {v0, p2, p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 224
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 226
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 227
    throw p1
.end method

.method public getCashDrawerShift(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;
    .locals 3

    .line 169
    invoke-virtual {p0}, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 170
    sget-object v1, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->RETRIEVE_CASH_DRAWER_SHIFT_BY_ID:Lcom/squareup/phrase/Phrase;

    const-string v2, "id_value"

    .line 171
    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 172
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 173
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    .line 170
    invoke-virtual {v0, p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    .line 175
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    .line 180
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->protoFromByteArray([B)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object v0

    .line 181
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method public getCashDrawerShifts(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;)Lcom/squareup/util/ReadOnlyCursorList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;",
            ")",
            "Lcom/squareup/util/ReadOnlyCursorList<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftRow;",
            ">;"
        }
    .end annotation

    .line 130
    invoke-virtual {p0}, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 131
    sget-object v1, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->QUERY_CASH_DRAWER_SHIFTS_BY_STATE:Lcom/squareup/phrase/Phrase;

    .line 132
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->getValue()I

    move-result p1

    const-string v2, "state"

    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 133
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 134
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    .line 131
    invoke-virtual {v0, p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    .line 135
    new-instance v0, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    invoke-direct {v0, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;-><init>(Landroid/database/Cursor;)V

    return-object v0
.end method

.method public getOpenCashDrawerShift()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;
    .locals 7

    const-string v0, "Done, closing the cursor."

    .line 141
    invoke-virtual {p0}, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    .line 142
    sget-object v4, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->GET_OPEN_CASH_DRAWER_SHIFT:Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const-string v4, "getOpenCashDrawerShift() - Executing raw query:\n%s"

    invoke-static {v4, v3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v3, 0x0

    .line 146
    :try_start_0
    sget-object v4, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->GET_OPEN_CASH_DRAWER_SHIFT:Ljava/lang/String;

    invoke-virtual {v1, v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 147
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_1

    const-string v2, "No rows, closing cursor."

    new-array v4, v5, [Ljava/lang/Object;

    .line 148
    invoke-static {v2, v4}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    new-array v2, v5, [Ljava/lang/Object;

    .line 159
    invoke-static {v0, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz v1, :cond_0

    .line 161
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    return-object v3

    :cond_1
    :try_start_2
    const-string v3, "Got a Cursor for the query - isOpen = %s"

    new-array v4, v2, [Ljava/lang/Object;

    .line 152
    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v6

    if-nez v6, :cond_2

    const/4 v6, 0x1

    goto :goto_0

    :cond_2
    const/4 v6, 0x0

    :goto_0
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v3, "Found %d rows, there are %d total columns. Getting blob at column 0 (name=%s)."

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    .line 155
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-interface {v1}, Landroid/database/Cursor;->getColumnCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v2

    const/4 v2, 0x2

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v2

    .line 153
    invoke-static {v3, v4}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 157
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->protoFromByteArray([B)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    new-array v3, v5, [Ljava/lang/Object;

    .line 159
    invoke-static {v0, v3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz v1, :cond_3

    .line 161
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    return-object v2

    :catchall_0
    move-exception v2

    goto :goto_1

    :catchall_1
    move-exception v2

    move-object v1, v3

    :goto_1
    new-array v3, v5, [Ljava/lang/Object;

    .line 159
    invoke-static {v0, v3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz v1, :cond_4

    .line 161
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 163
    :cond_4
    throw v2
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .line 115
    sget-object v0, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->CREATE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 116
    sget-object v1, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->CREATE:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "onCreate() - created table with query:\n$s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 118
    sget-object v0, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->CREATE_STATE_INDEX:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 119
    sget-object v0, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->CREATE_START_DATE_INDEX:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .line 124
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public repopulateCashDrawerShifts(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
            ">;)V"
        }
    .end annotation

    .line 199
    invoke-virtual {p0}, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 201
    invoke-virtual {p0}, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->dropAllCashDrawerShifts()V

    .line 202
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 204
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    .line 205
    invoke-static {v1}, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->buildCashDrawerShiftContentValues(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "cash_drawer_shifts_table"

    const/4 v3, 0x0

    const/4 v4, 0x5

    .line 206
    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    goto :goto_0

    .line 208
    :cond_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 211
    throw p1
.end method

.method public saveCashDrawerShift(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
    .locals 4

    .line 186
    invoke-virtual {p0}, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 187
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 189
    :try_start_0
    invoke-static {p1}, Lcom/squareup/cashmanagement/SqliteCashDrawerShiftStore;->buildCashDrawerShiftContentValues(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)Landroid/content/ContentValues;

    move-result-object p1

    const-string v1, "cash_drawer_shifts_table"

    const/4 v2, 0x0

    const/4 v3, 0x5

    .line 191
    invoke-virtual {v0, v1, v2, p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 192
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 195
    throw p1
.end method
