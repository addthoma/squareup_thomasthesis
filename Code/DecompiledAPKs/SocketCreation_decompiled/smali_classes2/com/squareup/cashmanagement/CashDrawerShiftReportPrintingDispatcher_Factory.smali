.class public final Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher_Factory;
.super Ljava/lang/Object;
.source "CashDrawerShiftReportPrintingDispatcher_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;",
        ">;"
    }
.end annotation


# instance fields
.field private final cashReportPayloadFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashReportPayloadFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final printSpoolerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSpooler;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSpooler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashReportPayloadFactory;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher_Factory;->printSpoolerProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p3, p0, Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher_Factory;->cashReportPayloadFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSpooler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashReportPayloadFactory;",
            ">;)",
            "Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher_Factory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/PrinterStations;Lcom/squareup/cashmanagement/CashReportPayloadFactory;)Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;
    .locals 1

    .line 43
    new-instance v0, Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;-><init>(Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/PrinterStations;Lcom/squareup/cashmanagement/CashReportPayloadFactory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher_Factory;->printSpoolerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/PrintSpooler;

    iget-object v1, p0, Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrinterStations;

    iget-object v2, p0, Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher_Factory;->cashReportPayloadFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cashmanagement/CashReportPayloadFactory;

    invoke-static {v0, v1, v2}, Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher_Factory;->newInstance(Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/PrinterStations;Lcom/squareup/cashmanagement/CashReportPayloadFactory;)Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher_Factory;->get()Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;

    move-result-object v0

    return-object v0
.end method
