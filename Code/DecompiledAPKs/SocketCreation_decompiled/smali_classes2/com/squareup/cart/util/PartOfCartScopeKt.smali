.class public final Lcom/squareup/cart/util/PartOfCartScopeKt;
.super Ljava/lang/Object;
.source "PartOfCartScope.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "reachableFromCart",
        "",
        "Lflow/Traversal;",
        "cart-navigation_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final reachableFromCart(Lflow/Traversal;)Z
    .locals 2

    const-string v0, "$this$reachableFromCart"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    iget-object v0, p0, Lflow/Traversal;->origin:Lflow/History;

    const-string v1, "origin"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/container/Traversals;->topNonDialog(Lflow/History;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    .line 13
    const-class v1, Lcom/squareup/cart/util/PartOfCartScope;

    invoke-virtual {v0, v1}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result v0

    .line 14
    iget-object p0, p0, Lflow/Traversal;->destination:Lflow/History;

    const-string v1, "destination"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/container/Traversals;->topNonDialog(Lflow/History;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p0

    .line 15
    const-class v1, Lcom/squareup/cart/util/PartOfCartScope;

    invoke-virtual {p0, v1}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p0

    if-nez v0, :cond_1

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method
