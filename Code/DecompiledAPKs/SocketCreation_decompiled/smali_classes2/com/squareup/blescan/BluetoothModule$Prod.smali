.class public Lcom/squareup/blescan/BluetoothModule$Prod;
.super Ljava/lang/Object;
.source "BluetoothModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/blescan/BluetoothModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Prod"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static provideUtils(Landroid/app/Application;Landroid/bluetooth/BluetoothManager;)Lcom/squareup/cardreader/BluetoothUtils;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/blescan/RealBluetoothUtils;

    invoke-direct {v0, p0, p1}, Lcom/squareup/blescan/RealBluetoothUtils;-><init>(Landroid/app/Application;Landroid/bluetooth/BluetoothManager;)V

    return-object v0
.end method
