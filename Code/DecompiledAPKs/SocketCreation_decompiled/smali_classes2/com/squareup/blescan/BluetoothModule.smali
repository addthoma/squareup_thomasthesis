.class public Lcom/squareup/blescan/BluetoothModule;
.super Ljava/lang/Object;
.source "BluetoothModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/blescan/BluetoothModule$Prod;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static provideAdapter(Lcom/squareup/cardreader/BluetoothUtils;Landroid/bluetooth/BluetoothManager;)Landroid/bluetooth/BluetoothAdapter;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 65
    invoke-interface {p0}, Lcom/squareup/cardreader/BluetoothUtils;->supportsBluetooth()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 69
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothManager;->getAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object p0

    return-object p0

    .line 66
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Bluetooth not supported on this device."

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static provideBluetoothLeScanner(Landroid/bluetooth/BluetoothAdapter;)Landroid/bluetooth/le/BluetoothLeScanner;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 74
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothAdapter;->getBluetoothLeScanner()Landroid/bluetooth/le/BluetoothLeScanner;

    move-result-object p0

    return-object p0
.end method

.method public static provideBluetoothStatus(Landroid/app/Application;Lcom/squareup/cardreader/BluetoothUtils;)Lcom/squareup/blescan/BluetoothStatusStream;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/blescan/BluetoothStatusReceiver;

    invoke-direct {v0, p1}, Lcom/squareup/blescan/BluetoothStatusReceiver;-><init>(Lcom/squareup/cardreader/BluetoothUtils;)V

    .line 39
    invoke-virtual {v0, p0}, Lcom/squareup/blescan/BluetoothStatusReceiver;->initialize(Landroid/content/Context;)V

    return-object v0
.end method

.method public static provideWirelessSearcher(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/BluetoothUtils;Ljavax/inject/Provider;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/blescan/WirelessSearcher;
    .locals 7
    .param p3    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/le/BluetoothLeScanner;",
            ">;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ")",
            "Lcom/squareup/blescan/WirelessSearcher;"
        }
    .end annotation

    .line 49
    new-instance v6, Lcom/squareup/blescan/BleScanner;

    new-instance v3, Lcom/squareup/blescan/BleScanFilter;

    invoke-direct {v3}, Lcom/squareup/blescan/BleScanFilter;-><init>()V

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/blescan/BleScanner;-><init>(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/blescan/BleScanFilter;Ljavax/inject/Provider;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-object v6
.end method

.method public static providesBluetoothManager(Landroid/app/Application;)Landroid/bluetooth/BluetoothManager;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "bluetooth"

    .line 59
    invoke-virtual {p0, v0}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/bluetooth/BluetoothManager;

    return-object p0
.end method
