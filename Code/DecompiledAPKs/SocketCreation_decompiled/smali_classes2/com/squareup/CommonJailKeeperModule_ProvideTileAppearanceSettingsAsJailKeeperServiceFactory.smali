.class public final Lcom/squareup/CommonJailKeeperModule_ProvideTileAppearanceSettingsAsJailKeeperServiceFactory;
.super Ljava/lang/Object;
.source "CommonJailKeeperModule_ProvideTileAppearanceSettingsAsJailKeeperServiceFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/jailkeeper/JailKeeperService;",
        ">;"
    }
.end annotation


# instance fields
.field private final tileAppearanceSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/CommonJailKeeperModule_ProvideTileAppearanceSettingsAsJailKeeperServiceFactory;->tileAppearanceSettingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/CommonJailKeeperModule_ProvideTileAppearanceSettingsAsJailKeeperServiceFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;)",
            "Lcom/squareup/CommonJailKeeperModule_ProvideTileAppearanceSettingsAsJailKeeperServiceFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/CommonJailKeeperModule_ProvideTileAppearanceSettingsAsJailKeeperServiceFactory;

    invoke-direct {v0, p0}, Lcom/squareup/CommonJailKeeperModule_ProvideTileAppearanceSettingsAsJailKeeperServiceFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideTileAppearanceSettingsAsJailKeeperService(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)Lcom/squareup/jailkeeper/JailKeeperService;
    .locals 1

    .line 38
    invoke-static {p0}, Lcom/squareup/CommonJailKeeperModule;->provideTileAppearanceSettingsAsJailKeeperService(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)Lcom/squareup/jailkeeper/JailKeeperService;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/jailkeeper/JailKeeperService;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/jailkeeper/JailKeeperService;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/CommonJailKeeperModule_ProvideTileAppearanceSettingsAsJailKeeperServiceFactory;->tileAppearanceSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-static {v0}, Lcom/squareup/CommonJailKeeperModule_ProvideTileAppearanceSettingsAsJailKeeperServiceFactory;->provideTileAppearanceSettingsAsJailKeeperService(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)Lcom/squareup/jailkeeper/JailKeeperService;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/CommonJailKeeperModule_ProvideTileAppearanceSettingsAsJailKeeperServiceFactory;->get()Lcom/squareup/jailkeeper/JailKeeperService;

    move-result-object v0

    return-object v0
.end method
