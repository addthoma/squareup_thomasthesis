.class public final Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;
.super Ljava/lang/Object;
.source "PayGiftCardPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final activeCardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final customerManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final softInputPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final swipeHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final thirdPartyGiftCardStrategyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final x2ScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 76
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 77
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 78
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 79
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->swipeHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 80
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 81
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->giftCardsProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 82
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 83
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->busProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 84
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->softInputPresenterProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 85
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 86
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 87
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 88
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 89
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 90
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->tenderScopeRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 91
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 92
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->thirdPartyGiftCardStrategyProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;)",
            "Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 113
    new-instance v18, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v18
.end method

.method public static newInstance(Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/text/Formatter;Lcom/squareup/badbus/BadBus;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/tenderpayment/TenderScopeRunner;Lcom/squareup/crm/CustomerManagementSettings;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/giftcard/GiftCards;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/ui/SoftInputPresenter;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/payment/TenderInEdit;",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;)",
            "Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 124
    new-instance v18, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;-><init>(Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/text/Formatter;Lcom/squareup/badbus/BadBus;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/tenderpayment/TenderScopeRunner;Lcom/squareup/crm/CustomerManagementSettings;Ljavax/inject/Provider;)V

    return-object v18
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;
    .locals 19

    move-object/from16 v0, p0

    .line 97
    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/cardreader/dipper/ActiveCardReader;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->swipeHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/ui/payment/SwipeHandler;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->giftCardsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/giftcard/GiftCards;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/badbus/BadBus;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->softInputPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/ui/SoftInputPresenter;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/payment/TenderInEdit;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->tenderScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/tenderpayment/TenderScopeRunner;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/crm/CustomerManagementSettings;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->thirdPartyGiftCardStrategyProvider:Ljavax/inject/Provider;

    move-object/from16 v18, v1

    invoke-static/range {v2 .. v18}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->newInstance(Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/text/Formatter;Lcom/squareup/badbus/BadBus;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/tenderpayment/TenderScopeRunner;Lcom/squareup/crm/CustomerManagementSettings;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter_Factory;->get()Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;

    move-result-object v0

    return-object v0
.end method
