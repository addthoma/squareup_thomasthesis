.class public final Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;
.super Ljava/lang/Object;
.source "PayCreditCardPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;"
        }
    .end annotation
.end field

.field private final arg10Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final arg11Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg12Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;"
        }
    .end annotation
.end field

.field private final arg13Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final arg14Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;"
        }
    .end annotation
.end field

.field private final arg15Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg16Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final arg17Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field

.field private final arg18Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final arg19Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final arg20Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg21Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cnp/LinkSpanDataHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final arg22Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;"
        }
    .end annotation
.end field

.field private final arg23Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg8Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg9Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cnp/LinkSpanDataHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingStarter;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 98
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg0Provider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 99
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg1Provider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 100
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg2Provider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 101
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg3Provider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 102
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg4Provider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 103
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg5Provider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 104
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg6Provider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 105
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg7Provider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 106
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg8Provider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 107
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg9Provider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 108
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg10Provider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 109
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg11Provider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 110
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg12Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 111
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg13Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 112
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg14Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 113
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg15Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 114
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg16Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 115
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg17Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 116
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg18Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 117
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg19Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 118
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg20Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 119
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg21Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 120
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg22Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 121
    iput-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg23Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cnp/LinkSpanDataHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingStarter;",
            ">;)",
            "Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    .line 144
    new-instance v25, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;

    move-object/from16 v0, v25

    invoke-direct/range {v0 .. v24}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v25
.end method

.method public static newInstance(Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/badbus/BadBus;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/tenderpayment/TenderScopeRunner;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/cnp/LinkSpanDataHelper;Ljavax/inject/Provider;Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingStarter;)Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/giftcard/GiftCards;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/ui/SoftInputPresenter;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            "Lcom/squareup/api/ApiTransactionState;",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/payment/TenderInEdit;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            "Lcom/squareup/cnp/LinkSpanDataHelper;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;",
            "Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingStarter;",
            ")",
            "Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    .line 155
    new-instance v25, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;

    move-object/from16 v0, v25

    invoke-direct/range {v0 .. v24}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;-><init>(Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/badbus/BadBus;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/tenderpayment/TenderScopeRunner;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/cnp/LinkSpanDataHelper;Ljavax/inject/Provider;Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingStarter;)V

    return-object v25
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;
    .locals 26

    move-object/from16 v0, p0

    .line 126
    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/cardreader/dipper/ActiveCardReader;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/ui/payment/SwipeHandler;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/giftcard/GiftCards;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/badbus/BadBus;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/ui/SoftInputPresenter;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg8Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/settings/LocalSetting;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg9Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/settings/LocalSetting;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg10Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg11Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg12Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/api/ApiTransactionState;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg13Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/cardreader/DippedCardTracker;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg14Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/cardreader/dipper/ReaderHudManager;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg15Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg16Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/payment/TenderInEdit;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg17Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg18Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/tenderpayment/TenderScopeRunner;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg19Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/ui/main/errors/PaymentInputHandler;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg20Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg21Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/cnp/LinkSpanDataHelper;

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg22Provider:Ljavax/inject/Provider;

    move-object/from16 v24, v1

    iget-object v1, v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->arg23Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingStarter;

    invoke-static/range {v2 .. v25}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->newInstance(Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/badbus/BadBus;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/tenderpayment/TenderScopeRunner;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/cnp/LinkSpanDataHelper;Ljavax/inject/Provider;Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingStarter;)Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter_Factory;->get()Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;

    move-result-object v0

    return-object v0
.end method
