.class public interface abstract Lcom/squareup/checkoutflow/PaymentProcessingEventSink;
.super Ljava/lang/Object;
.source "PaymentProcessingEventSink.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0003H&J\u000e\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H&J\u0010\u0010\u0008\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\nH&J\u0008\u0010\u000b\u001a\u00020\u0003H&J\u0008\u0010\u000c\u001a\u00020\u0003H&\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
        "",
        "collectNextTender",
        "",
        "divertedToOnboarding",
        "onPaymentProcessingComplete",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/checkoutflow/PaymentProcessingResult;",
        "paymentCanceled",
        "reason",
        "Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;",
        "paymentCompleted",
        "paymentStarted",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract collectNextTender()V
.end method

.method public abstract divertedToOnboarding()V
.end method

.method public abstract onPaymentProcessingComplete()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/checkoutflow/PaymentProcessingResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract paymentCanceled(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
.end method

.method public abstract paymentCompleted()V
.end method

.method public abstract paymentStarted()V
.end method
