.class public abstract Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowModule;
.super Ljava/lang/Object;
.source "EmoneyWorkflowModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindEmoneyIntoBuyer(Lcom/squareup/checkoutflow/emoney/EmoneyViewFactory;)Lcom/squareup/workflow/WorkflowViewFactory;
    .annotation runtime Lcom/squareup/ui/buyer/ForBuyer;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract bindEmoneyIntoCheckoutflowTenderOptionFactories(Lcom/squareup/checkoutflow/emoney/EmoneyTenderOptionFactory;)Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract bindEmoneyIntoMainActivity(Lcom/squareup/checkoutflow/emoney/EmoneyViewFactory;)Lcom/squareup/workflow/WorkflowViewFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract provideEmoneyBrandSelectionWorkflow(Lcom/squareup/checkoutflow/emoney/brandselection/RealEmoneyBrandSelectionWorkflow;)Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideEmoneyCheckBalanceWorkflow(Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow;)Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideEmoneyMiryoWorkflow(Lcom/squareup/checkoutflow/emoney/miryo/RealEmoneyMiryoWorkflow;)Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideEmoneyPaymentProcessingWorkflow(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;)Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideEmoneyTenderOptionFactory(Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;)Lcom/squareup/checkoutflow/emoney/EmoneyTenderOptionFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideEmoneyWorkflow(Lcom/squareup/checkoutflow/emoney/RealEmoneyWorkflow;)Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
