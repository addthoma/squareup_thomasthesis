.class public final Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealEmoneyPaymentProcessingWorkflow.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealEmoneyPaymentProcessingWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealEmoneyPaymentProcessingWorkflow.kt\ncom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,927:1\n149#2,5:928\n150#2,4:933\n41#3:937\n56#3,2:938\n41#3:941\n56#3,2:942\n41#3:945\n56#3,2:946\n276#4:940\n276#4:944\n276#4:948\n*E\n*S KotlinDebug\n*F\n+ 1 RealEmoneyPaymentProcessingWorkflow.kt\ncom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow\n*L\n386#1,5:928\n408#1,4:933\n166#1:937\n166#1,2:938\n173#1:941\n173#1,2:942\n178#1:945\n178#1,2:946\n166#1:940\n173#1:944\n178#1:948\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0091\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0004*\u0001/\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002BQ\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0008\u0008\u0001\u0010\u0019\u001a\u00020\u001a\u0012\u0006\u0010\u001b\u001a\u00020\u001c\u00a2\u0006\u0002\u0010\u001dJ\u0082\u0001\u00101\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0012\u00102\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005032\u001c\u00104\u001a\u0018\u0012\u0004\u0012\u000205\u0012\u0004\u0012\u0002060\u0008j\u0008\u0012\u0004\u0012\u000205`72\u0006\u00108\u001a\u00020\u00042\u0006\u00109\u001a\u00020:2\u0014\u0008\u0002\u0010;\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050<H\u0002J.\u0010=\u001a\u0018\u0012\u0004\u0012\u000205\u0012\u0004\u0012\u0002060\u0008j\u0008\u0012\u0004\u0012\u000205`72\u0006\u0010>\u001a\u00020?2\u0006\u0010@\u001a\u00020\u0003H\u0002J:\u0010A\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010>\u001a\u00020?2\u0006\u0010@\u001a\u00020\u0003H\u0002J\u0016\u0010B\u001a\u00020:2\u000c\u0010C\u001a\u0008\u0012\u0004\u0012\u00020#0DH\u0002J\u0012\u0010E\u001a\u0004\u0018\u00010F2\u0006\u0010G\u001a\u00020\u0004H\u0002J$\u0010H\u001a\u00020,2\u0012\u00102\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005032\u0006\u0010@\u001a\u00020\u0003H\u0002J>\u0010I\u001a\u00020,2\u0012\u00102\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005032\u0006\u0010@\u001a\u00020\u00032\u0018\u0010J\u001a\u0014\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020#0\"\u0012\u0004\u0012\u00020\u00040\'H\u0002J,\u0010K\u001a\u00020,2\u0012\u00102\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005032\u0006\u0010G\u001a\u00020L2\u0006\u0010@\u001a\u00020\u0003H\u0002J,\u0010M\u001a\u00020,2\u0012\u00102\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005032\u0006\u0010@\u001a\u00020\u00032\u0006\u0010N\u001a\u00020OH\u0002J$\u0010P\u001a\u00020,2\u0012\u00102\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005032\u0006\u0010@\u001a\u00020\u0003H\u0002J2\u0010Q\u001a\u00020,2\u0012\u00102\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005032\u0006\u0010@\u001a\u00020\u00032\u000c\u0010R\u001a\u0008\u0012\u0004\u0012\u00020#0\"H\u0002J\u001a\u0010S\u001a\u00020T2\u0006\u0010@\u001a\u00020\u00032\u0008\u0010U\u001a\u0004\u0018\u00010VH\u0016J\u0010\u0010W\u001a\u00020O2\u0006\u0010X\u001a\u00020YH\u0002J\u001a\u0010Z\u001a\u0004\u0018\u00010F2\u0006\u0010[\u001a\u00020:2\u0006\u0010\\\u001a\u00020]H\u0002J$\u0010^\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050<2\u0006\u0010_\u001a\u00020\u00042\u0006\u0010`\u001a\u00020YH\u0002J*\u0010^\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050<2\u0006\u0010_\u001a\u00020\u00042\u000c\u0010a\u001a\u0008\u0012\u0004\u0012\u00020#0\"H\u0002J$\u0010^\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050<2\u0006\u0010_\u001a\u00020\u00042\u0006\u0010b\u001a\u00020:H\u0002JN\u0010c\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010@\u001a\u00020\u00032\u0006\u0010G\u001a\u00020\u00042\u0012\u00102\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000503H\u0016JJ\u0010d\u001a\u00020,2\u0012\u00102\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005032\u0006\u0010@\u001a\u00020\u00032\u0006\u0010G\u001a\u00020\u00042\u0006\u0010e\u001a\u00020f2\u0014\u0008\u0002\u0010g\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050<H\u0002J,\u0010h\u001a\u00020,2\u0006\u0010@\u001a\u00020\u00032\u0006\u0010G\u001a\u00020\u00042\u0012\u00102\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000503H\u0002J\u0010\u0010i\u001a\u00020V2\u0006\u0010G\u001a\u00020\u0004H\u0016R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020 0\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010!\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020#0\"0\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010$\u001a\u0008\u0012\u0004\u0012\u00020%0\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R-\u0010&\u001a!\u0012\u0013\u0012\u00110(\u00a2\u0006\u000c\u0008)\u0012\u0008\u0008*\u0012\u0004\u0008\u0008(+\u0012\u0004\u0012\u00020,0\'j\u0002`-X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010.\u001a\u00020/X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u00100R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006j"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "tmnWorkflow",
        "Lcom/squareup/tmn/TmnStarterWorkflow;",
        "miryoWorkflow",
        "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoWorkflow;",
        "tmnObservablesHelper",
        "Lcom/squareup/tmn/TmnObservablesHelper;",
        "failureMessageFactory",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "emoneyAddTenderProcessor",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;",
        "analytics",
        "Lcom/squareup/tmn/EmoneyAnalyticsLogger;",
        "readerRequestSink",
        "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
        "danglingMiryo",
        "Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
        "emoneyIconFactory",
        "Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;",
        "(Lcom/squareup/tmn/TmnStarterWorkflow;Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoWorkflow;Lcom/squareup/tmn/TmnObservablesHelper;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;Lcom/squareup/tmn/EmoneyAnalyticsLogger;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;)V",
        "authRequestWorker",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnAuthRequest;",
        "authResponseWorker",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/bills/AddTendersResponse;",
        "displayRequestWorker",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
        "emptyReaderRequestHandler",
        "Lkotlin/Function1;",
        "Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;",
        "Lkotlin/ParameterName;",
        "name",
        "issueScreen",
        "",
        "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestHandler;",
        "lifecycleWorker",
        "com/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$lifecycleWorker$1",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$lifecycleWorker$1;",
        "createDialogStack",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "body",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "keepState",
        "key",
        "",
        "dismissAction",
        "Lcom/squareup/workflow/WorkflowAction;",
        "createScreenBody",
        "data",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;",
        "input",
        "createSimpleScreen",
        "getAuthFailureMessage",
        "failure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;",
        "getBalanceFromState",
        "Lcom/squareup/protos/common/Money;",
        "state",
        "handleAuthDataFromReader",
        "handleAuthResponse",
        "stateFactory",
        "handleAuthResponseWithReaderResponse",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;",
        "handleDisplayRequestForBeforeTap",
        "showingCancelDialog",
        "",
        "handleDisplayRequestForProcessing",
        "handleDisplayRequestWithAuthResponse",
        "authResponse",
        "initialState",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "isTMNDisplayMessageRetryableError",
        "tmnMessage",
        "Lcom/squareup/cardreader/lcr/CrsTmnMessage;",
        "parseBalance",
        "balanceString",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "performEnterState",
        "newState",
        "displayRequest",
        "authResult",
        "transitionReason",
        "render",
        "renderTmnWorkflow",
        "action",
        "Lcom/squareup/tmn/Action;",
        "onCompletedResult",
        "runWorkersAndChildren",
        "snapshotState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/tmn/EmoneyAnalyticsLogger;

.field private final authRequestWorker:Lcom/squareup/workflow/Worker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnAuthRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final authResponseWorker:Lcom/squareup/workflow/Worker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/AddTendersResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final danglingMiryo:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

.field private final displayRequestWorker:Lcom/squareup/workflow/Worker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final emoneyAddTenderProcessor:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;

.field private final emoneyIconFactory:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;

.field private final emptyReaderRequestHandler:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final lifecycleWorker:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$lifecycleWorker$1;

.field private final miryoWorkflow:Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoWorkflow;

.field private final readerRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

.field private final tmnWorkflow:Lcom/squareup/tmn/TmnStarterWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/tmn/TmnStarterWorkflow;Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoWorkflow;Lcom/squareup/tmn/TmnObservablesHelper;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;Lcom/squareup/tmn/EmoneyAnalyticsLogger;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;)V
    .locals 1
    .param p8    # Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;
        .annotation runtime Lcom/squareup/checkoutflow/datamodels/payment/DanglingMiryo;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "tmnWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "miryoWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tmnObservablesHelper"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "failureMessageFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emoneyAddTenderProcessor"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "readerRequestSink"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "danglingMiryo"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emoneyIconFactory"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->tmnWorkflow:Lcom/squareup/tmn/TmnStarterWorkflow;

    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->miryoWorkflow:Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoWorkflow;

    iput-object p4, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    iput-object p5, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->emoneyAddTenderProcessor:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;

    iput-object p6, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->analytics:Lcom/squareup/tmn/EmoneyAnalyticsLogger;

    iput-object p7, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->readerRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    iput-object p8, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->danglingMiryo:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    iput-object p9, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->emoneyIconFactory:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;

    .line 148
    sget-object p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$emptyReaderRequestHandler$1;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$emptyReaderRequestHandler$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->emptyReaderRequestHandler:Lkotlin/jvm/functions/Function1;

    .line 150
    new-instance p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$lifecycleWorker$1;

    invoke-direct {p1, p0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$lifecycleWorker$1;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->lifecycleWorker:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$lifecycleWorker$1;

    .line 165
    invoke-interface {p3}, Lcom/squareup/tmn/TmnObservablesHelper;->getDisplayRequestObservable()Lio/reactivex/Observable;

    move-result-object p1

    .line 937
    sget-object p2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    const-string p2, "this.toFlowable(BUFFER)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    const-string p4, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    if-eqz p1, :cond_2

    .line 939
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 940
    const-class p5, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;

    invoke-static {p5}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p5

    new-instance p6, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {p6, p5, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast p6, Lcom/squareup/workflow/Worker;

    .line 937
    iput-object p6, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->displayRequestWorker:Lcom/squareup/workflow/Worker;

    .line 172
    invoke-interface {p3}, Lcom/squareup/tmn/TmnObservablesHelper;->getTmnAuthRequest()Lio/reactivex/Observable;

    move-result-object p1

    .line 941
    sget-object p3, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, p3}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_1

    .line 943
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 944
    const-class p3, Lcom/squareup/dipper/events/TmnEvent$OnTmnAuthRequest;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p3

    new-instance p5, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {p5, p3, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast p5, Lcom/squareup/workflow/Worker;

    .line 941
    iput-object p5, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->authRequestWorker:Lcom/squareup/workflow/Worker;

    .line 178
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->emoneyAddTenderProcessor:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;->getServerResponse()Lio/reactivex/Observable;

    move-result-object p1

    .line 945
    sget-object p3, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, p3}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_0

    .line 947
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 948
    const-class p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    sget-object p3, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class p4, Lcom/squareup/protos/client/bills/AddTendersResponse;

    invoke-static {p4}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p4

    invoke-virtual {p3, p4}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object p3

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance p3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {p3, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast p3, Lcom/squareup/workflow/Worker;

    .line 945
    iput-object p3, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->authResponseWorker:Lcom/squareup/workflow/Worker;

    return-void

    .line 947
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, p4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 943
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, p4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 939
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, p4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;)Lcom/squareup/tmn/EmoneyAnalyticsLogger;
    .locals 0

    .line 123
    iget-object p0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->analytics:Lcom/squareup/tmn/EmoneyAnalyticsLogger;

    return-object p0
.end method

.method public static final synthetic access$getAuthFailureMessage(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Ljava/lang/String;
    .locals 0

    .line 123
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->getAuthFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getBalanceFromState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 123
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->getBalanceFromState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getDanglingMiryo$p(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;)Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;
    .locals 0

    .line 123
    iget-object p0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->danglingMiryo:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    return-object p0
.end method

.method public static final synthetic access$getEmoneyAddTenderProcessor$p(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;)Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;
    .locals 0

    .line 123
    iget-object p0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->emoneyAddTenderProcessor:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyAddTenderProcessor;

    return-object p0
.end method

.method public static final synthetic access$getEmptyReaderRequestHandler$p(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;)Lkotlin/jvm/functions/Function1;
    .locals 0

    .line 123
    iget-object p0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->emptyReaderRequestHandler:Lkotlin/jvm/functions/Function1;

    return-object p0
.end method

.method public static final synthetic access$getReaderRequestSink$p(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;)Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;
    .locals 0

    .line 123
    iget-object p0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->readerRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    return-object p0
.end method

.method public static final synthetic access$isTMNDisplayMessageRetryableError(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/cardreader/lcr/CrsTmnMessage;)Z
    .locals 0

    .line 123
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->isTMNDisplayMessageRetryableError(Lcom/squareup/cardreader/lcr/CrsTmnMessage;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$parseBalance(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 123
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->parseBalance(Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/cardreader/lcr/CrsTmnMessage;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 123
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/cardreader/lcr/CrsTmnMessage;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 123
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 123
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final createDialogStack(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Ljava/lang/String;Lcom/squareup/workflow/WorkflowAction;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "-",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
            ">;",
            "Lcom/squareup/workflow/legacy/Screen;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "Ljava/lang/String;",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "+",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 400
    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 401
    new-instance v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelScreen;

    .line 402
    new-instance v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$createDialogStack$1;

    invoke-direct {v2, p5}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$createDialogStack$1;-><init>(Lcom/squareup/workflow/WorkflowAction;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p1, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p5

    .line 405
    new-instance v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$createDialogStack$2;

    invoke-direct {v2, p0, p3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$createDialogStack$2;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p1, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 401
    invoke-direct {v1, p5, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelScreen;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 933
    new-instance v4, Lcom/squareup/workflow/legacy/Screen;

    .line 934
    const-class p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelScreen;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    invoke-static {p1, p4}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p1

    .line 935
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 933
    invoke-direct {v4, p1, v1, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p2

    .line 400
    invoke-static/range {v0 .. v6}, Lcom/squareup/container/PosLayering$Companion;->dialogStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method static synthetic createDialogStack$default(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Ljava/lang/String;Lcom/squareup/workflow/WorkflowAction;ILjava/lang/Object;)Ljava/util/Map;
    .locals 6

    and-int/lit8 p6, p6, 0x10

    if-eqz p6, :cond_0

    .line 399
    sget-object p5, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object p6, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult$EmoneyAbort;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult$EmoneyAbort;

    invoke-virtual {p5, p6}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p5

    :cond_0
    move-object v5, p5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->createDialogStack(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Ljava/lang/String;Lcom/squareup/workflow/WorkflowAction;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method private final createScreenBody(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)Lcom/squareup/workflow/legacy/Screen;
    .locals 3

    .line 383
    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;

    .line 384
    new-instance v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType$Payment;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType$Payment;-><init>(Lcom/squareup/protos/common/Money;)V

    check-cast v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType;

    .line 385
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;->getBrand()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    move-result-object p2

    .line 383
    invoke-direct {v0, p1, v1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$TransactionType;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 929
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 930
    const-class p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 931
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 929
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method private final createSimpleScreen(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)Ljava/util/Map;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 378
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->createScreenBody(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final getAuthFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/client/bills/AddTendersResponse;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 803
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    .line 805
    sget v1, Lcom/squareup/common/strings/R$string;->error_default:I

    .line 806
    sget-object v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$getAuthFailureMessage$1;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$getAuthFailureMessage$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 803
    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final getBalanceFromState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;)Lcom/squareup/protos/common/Money;
    .locals 1

    .line 906
    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_0

    .line 907
    :cond_0
    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$ReceivedRetryableErrorMessage;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$ReceivedRetryableErrorMessage;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$ReceivedRetryableErrorMessage;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_0

    .line 908
    :cond_1
    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentComplete;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentComplete;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentComplete;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_0

    .line 909
    :cond_2
    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_0

    .line 910
    :cond_3
    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    if-eqz v0, :cond_4

    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_0

    :cond_4
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final handleAuthDataFromReader(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "-",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
            ">;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;",
            ")V"
        }
    .end annotation

    .line 571
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->authRequestWorker:Lcom/squareup/workflow/Worker;

    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthDataFromReader$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthDataFromReader$1;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)V

    move-object v3, v0

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final handleAuthResponse(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lkotlin/jvm/functions/Function1;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "-",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
            ">;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/AddTendersResponse;",
            ">;+",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            ">;)V"
        }
    .end annotation

    .line 703
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->authResponseWorker:Lcom/squareup/workflow/Worker;

    new-instance p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponse$1;

    invoke-direct {p2, p0, p3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponse$1;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lkotlin/jvm/functions/Function1;)V

    move-object v3, p2

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final handleAuthResponseWithReaderResponse(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "-",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
            ">;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;",
            ")V"
        }
    .end annotation

    .line 725
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->authResponseWorker:Lcom/squareup/workflow/Worker;

    new-instance p3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponseWithReaderResponse$1;

    invoke-direct {p3, p0, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponseWithReaderResponse$1;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;)V

    move-object v3, p3

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final handleDisplayRequestForBeforeTap(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "-",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
            ">;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;",
            "Z)V"
        }
    .end annotation

    .line 585
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->displayRequestWorker:Lcom/squareup/workflow/Worker;

    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestForBeforeTap$1;

    invoke-direct {v0, p0, p3, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestForBeforeTap$1;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;ZLcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)V

    move-object v3, v0

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final handleDisplayRequestForProcessing(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "-",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
            ">;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;",
            ")V"
        }
    .end annotation

    .line 610
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->displayRequestWorker:Lcom/squareup/workflow/Worker;

    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestForProcessing$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestForProcessing$1;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)V

    move-object v3, v0

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final handleDisplayRequestWithAuthResponse(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "-",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
            ">;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/AddTendersResponse;",
            ">;)V"
        }
    .end annotation

    .line 651
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->displayRequestWorker:Lcom/squareup/workflow/Worker;

    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestWithAuthResponse$1;

    invoke-direct {v0, p0, p3, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleDisplayRequestWithAuthResponse$1;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)V

    move-object v3, v0

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final isTMNDisplayMessageRetryableError(Lcom/squareup/cardreader/lcr/CrsTmnMessage;)Z
    .locals 1

    .line 754
    sget-object v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    const/4 p1, 0x0

    goto :goto_0

    :pswitch_0
    const/4 p1, 0x1

    :goto_0
    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private final parseBalance(Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;
    .locals 2

    .line 918
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/squareup/util/Strings;->removeNonDigits(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 919
    invoke-static {p1}, Lkotlin/text/StringsKt;->toLongOrNull(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 921
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1, p2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/cardreader/lcr/CrsTmnMessage;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "Lcom/squareup/cardreader/lcr/CrsTmnMessage;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
            ">;"
        }
    .end annotation

    .line 797
    sget-object v0, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    array-length p2, v0

    invoke-static {v0, p2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p2

    const-string v0, "Received TMN message: %s"

    invoke-static {v0, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    const-string v0, "java.lang.String.format(format, *args)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/AddTendersResponse;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
            ">;"
        }
    .end annotation

    .line 790
    instance-of p2, p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz p2, :cond_0

    const-string p2, "Auth Success"

    goto :goto_0

    :cond_0
    const-string p2, "Auth Failure"

    .line 791
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
            ">;"
        }
    .end annotation

    .line 777
    sget-object v0, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 778
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p2, v1, v2

    .line 777
    array-length p2, v1

    invoke-static {v1, p2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p2

    const-string v1, "EMoney Workflow State Transition: %s Reason: %s"

    invoke-static {v1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    const-string v1, "java.lang.String.format(format, *args)"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v1, v3, [Ljava/lang/Object;

    .line 781
    invoke-static {p2, v1}, Ltimber/log/Timber;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 782
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->analytics:Lcom/squareup/tmn/EmoneyAnalyticsLogger;

    new-instance v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyStateTransitionEvent;

    invoke-direct {v2, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyStateTransitionEvent;-><init>(Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-virtual {v1, v2}, Lcom/squareup/tmn/EmoneyAnalyticsLogger;->addEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 783
    sget-object p2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    const/4 v1, 0x0

    invoke-static {p2, p1, v1, v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final renderTmnWorkflow(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/tmn/Action;Lcom/squareup/workflow/WorkflowAction;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "-",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
            ">;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "Lcom/squareup/tmn/Action;",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "+",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
            ">;)V"
        }
    .end annotation

    .line 825
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->tmnWorkflow:Lcom/squareup/tmn/TmnStarterWorkflow;

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 826
    new-instance v0, Lcom/squareup/tmn/TmnInput;

    sget-object v4, Lcom/squareup/tmn/TmnTransactionType;->Payment:Lcom/squareup/tmn/TmnTransactionType;

    sget-object v1, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;->getBrand()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->getTmnBrandId(Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    move-result-object v5

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-string v3, "input.money.amount"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object v3, v0

    move-object v8, p4

    invoke-direct/range {v3 .. v8}, Lcom/squareup/tmn/TmnInput;-><init>(Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;JLcom/squareup/tmn/Action;)V

    .line 827
    new-instance p4, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$renderTmnWorkflow$1;

    invoke-direct {p4, p0, p2, p3, p5}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$renderTmnWorkflow$1;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/workflow/WorkflowAction;)V

    move-object v5, p4

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p1

    .line 824
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method static synthetic renderTmnWorkflow$default(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/tmn/Action;Lcom/squareup/workflow/WorkflowAction;ILjava/lang/Object;)V
    .locals 6

    and-int/lit8 p6, p6, 0x10

    if-eqz p6, :cond_0

    .line 822
    sget-object p5, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {p5}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p5

    :cond_0
    move-object v5, p5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->renderTmnWorkflow(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/tmn/Action;Lcom/squareup/workflow/WorkflowAction;)V

    return-void
.end method

.method private final runWorkersAndChildren(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/workflow/RenderContext;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "-",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v8, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v1, p3

    .line 420
    sget-object v0, Lcom/squareup/tmn/Action$NoOp;->INSTANCE:Lcom/squareup/tmn/Action$NoOp;

    move-object v4, v0

    check-cast v4, Lcom/squareup/tmn/Action;

    .line 422
    instance-of v0, v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle;

    if-eqz v0, :cond_2

    .line 423
    move-object v0, v3

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle;->getIdleType()Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle$EmoneyIdleType;

    move-result-object v5

    .line 424
    instance-of v6, v5, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle$EmoneyIdleType$ReaderNotReady;

    if-eqz v6, :cond_0

    .line 425
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle;->getShowingCancelDialog()Z

    move-result v0

    invoke-direct {v8, v1, v2, v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->handleDisplayRequestForBeforeTap(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Z)V

    .line 426
    sget-object v0, Lcom/squareup/tmn/Action$StartPayment;->INSTANCE:Lcom/squareup/tmn/Action$StartPayment;

    move-object v4, v0

    check-cast v4, Lcom/squareup/tmn/Action;

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-static/range {v0 .. v7}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->renderTmnWorkflow$default(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/tmn/Action;Lcom/squareup/workflow/WorkflowAction;ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 428
    :cond_0
    instance-of v6, v5, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle$EmoneyIdleType$ReaderDisconnected;

    if-eqz v6, :cond_1

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    .line 429
    invoke-static/range {v0 .. v7}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->renderTmnWorkflow$default(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/tmn/Action;Lcom/squareup/workflow/WorkflowAction;ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 431
    :cond_1
    instance-of v5, v5, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle$EmoneyIdleType$WaitingForTap;

    if-eqz v5, :cond_e

    .line 432
    invoke-direct {v8, v1, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->handleAuthDataFromReader(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)V

    .line 433
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle;->getShowingCancelDialog()Z

    move-result v0

    invoke-direct {v8, v1, v2, v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->handleDisplayRequestForBeforeTap(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Z)V

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    .line 434
    invoke-static/range {v0 .. v7}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->renderTmnWorkflow$default(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/tmn/Action;Lcom/squareup/workflow/WorkflowAction;ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 438
    :cond_2
    instance-of v0, v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$CancelRequested;

    if-eqz v0, :cond_3

    .line 439
    invoke-direct {v8, v1, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->handleAuthDataFromReader(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)V

    const/4 v0, 0x0

    .line 440
    invoke-direct {v8, v1, v2, v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->handleDisplayRequestForBeforeTap(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Z)V

    .line 442
    sget-object v0, Lcom/squareup/tmn/Action$CancelPaymentRequest;->INSTANCE:Lcom/squareup/tmn/Action$CancelPaymentRequest;

    move-object v4, v0

    check-cast v4, Lcom/squareup/tmn/Action;

    sget-object v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$1;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-static {v8, v6, v0, v5, v6}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    .line 441
    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->renderTmnWorkflow(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/tmn/Action;Lcom/squareup/workflow/WorkflowAction;)V

    goto/16 :goto_1

    .line 445
    :cond_3
    instance-of v0, v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;

    if-eqz v0, :cond_5

    .line 446
    move-object v0, v3

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;->getAuthResponse()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 448
    invoke-direct {v8, v1, v2, v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->handleDisplayRequestWithAuthResponse(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    goto :goto_0

    .line 450
    :cond_4
    invoke-direct {v8, v1, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->handleDisplayRequestForProcessing(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)V

    .line 452
    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$2;

    invoke-direct {v0, v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$2;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-direct {v8, v1, v2, v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->handleAuthResponse(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lkotlin/jvm/functions/Function1;)V

    :goto_0
    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    .line 456
    invoke-static/range {v0 .. v7}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->renderTmnWorkflow$default(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/tmn/Action;Lcom/squareup/workflow/WorkflowAction;ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 458
    :cond_5
    instance-of v0, v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$NoAuthOrReaderResponse;

    if-eqz v0, :cond_6

    .line 459
    invoke-direct {v8, v1, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->handleDisplayRequestForProcessing(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)V

    .line 460
    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$3;

    invoke-direct {v0, v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$3;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-direct {v8, v1, v2, v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->handleAuthResponse(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lkotlin/jvm/functions/Function1;)V

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    .line 463
    invoke-static/range {v0 .. v7}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->renderTmnWorkflow$default(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/tmn/Action;Lcom/squareup/workflow/WorkflowAction;ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 465
    :cond_6
    instance-of v0, v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;

    if-eqz v0, :cond_7

    .line 466
    iget-object v10, v8, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->displayRequestWorker:Lcom/squareup/workflow/Worker;

    const/4 v11, 0x0

    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$4;

    invoke-direct {v0, v8, v2, v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$4;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;)V

    move-object v12, v0

    check-cast v12, Lkotlin/jvm/functions/Function1;

    const/4 v13, 0x2

    const/4 v14, 0x0

    move-object/from16 v9, p3

    invoke-static/range {v9 .. v14}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 473
    move-object v0, v3

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;

    invoke-direct {v8, v1, v0, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->handleAuthResponseWithReaderResponse(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)V

    .line 474
    sget-object v0, Lcom/squareup/tmn/Action$NoOp;->INSTANCE:Lcom/squareup/tmn/Action$NoOp;

    move-object v4, v0

    check-cast v4, Lcom/squareup/tmn/Action;

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-static/range {v0 .. v7}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->renderTmnWorkflow$default(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/tmn/Action;Lcom/squareup/workflow/WorkflowAction;ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 476
    :cond_7
    instance-of v0, v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulAuthResponseOnly;

    if-eqz v0, :cond_8

    .line 477
    move-object v0, v3

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulAuthResponseOnly;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulAuthResponseOnly;->getAuthResponse()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object v0

    invoke-direct {v8, v1, v2, v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->handleDisplayRequestWithAuthResponse(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    .line 478
    invoke-static/range {v0 .. v7}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->renderTmnWorkflow$default(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/tmn/Action;Lcom/squareup/workflow/WorkflowAction;ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 480
    :cond_8
    instance-of v0, v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$ReceivedRetryableErrorMessage;

    if-eqz v0, :cond_9

    .line 488
    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;

    sget-object v5, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType$RetryablePaymentError;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType$RetryablePaymentError;

    move-object v10, v5

    check-cast v10, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType;

    move-object v5, v3

    check-cast v5, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$ReceivedRetryableErrorMessage;

    invoke-virtual {v5}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$ReceivedRetryableErrorMessage;->getErrorMessage()Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    move-result-object v11

    invoke-virtual {v5}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$ReceivedRetryableErrorMessage;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object v12

    const/4 v13, 0x0

    const/16 v14, 0x8

    const/4 v15, 0x0

    move-object v9, v0

    invoke-direct/range {v9 .. v15}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;Lcom/squareup/protos/common/Money;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    const-string v5, "TMN Workflow Finished"

    .line 487
    invoke-direct {v8, v0, v5}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    .line 485
    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->renderTmnWorkflow(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/tmn/Action;Lcom/squareup/workflow/WorkflowAction;)V

    goto/16 :goto_1

    .line 493
    :cond_9
    instance-of v0, v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentComplete;

    if-eqz v0, :cond_a

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    .line 494
    invoke-static/range {v0 .. v7}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->renderTmnWorkflow$default(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/tmn/Action;Lcom/squareup/workflow/WorkflowAction;ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 496
    :cond_a
    instance-of v0, v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;

    if-eqz v0, :cond_d

    .line 497
    move-object v0, v3

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;->getErrorType()Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType;

    move-result-object v0

    .line 498
    instance-of v5, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType$RetryablePaymentError;

    if-eqz v5, :cond_b

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-static/range {v0 .. v7}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->renderTmnWorkflow$default(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/tmn/Action;Lcom/squareup/workflow/WorkflowAction;ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 499
    :cond_b
    instance-of v5, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType$AuthOnlyRetryablePaymentError;

    if-eqz v5, :cond_c

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-static/range {v0 .. v7}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->renderTmnWorkflow$default(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/tmn/Action;Lcom/squareup/workflow/WorkflowAction;ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 502
    :cond_c
    instance-of v0, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType$FatalPaymentError;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/squareup/tmn/Action$CancelPayment;->INSTANCE:Lcom/squareup/tmn/Action$CancelPayment;

    move-object v4, v0

    check-cast v4, Lcom/squareup/tmn/Action;

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-static/range {v0 .. v7}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->renderTmnWorkflow$default(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/tmn/Action;Lcom/squareup/workflow/WorkflowAction;ILjava/lang/Object;)V

    goto :goto_1

    .line 505
    :cond_d
    instance-of v0, v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    if-eqz v0, :cond_e

    .line 506
    iget-object v10, v8, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->authRequestWorker:Lcom/squareup/workflow/Worker;

    const/4 v11, 0x0

    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$5;

    invoke-direct {v0, v8, v2, v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$5;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;)V

    move-object v12, v0

    check-cast v12, Lkotlin/jvm/functions/Function1;

    const/4 v13, 0x2

    const/4 v14, 0x0

    move-object/from16 v9, p3

    invoke-static/range {v9 .. v14}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 522
    iget-object v10, v8, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->displayRequestWorker:Lcom/squareup/workflow/Worker;

    sget-object v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$6;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$6;

    move-object v12, v0

    check-cast v12, Lkotlin/jvm/functions/Function1;

    invoke-static/range {v9 .. v14}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 527
    iget-object v0, v8, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->tmnWorkflow:Lcom/squareup/tmn/TmnStarterWorkflow;

    move-object v4, v0

    check-cast v4, Lcom/squareup/workflow/Workflow;

    .line 528
    new-instance v5, Lcom/squareup/tmn/TmnInput;

    .line 529
    sget-object v10, Lcom/squareup/tmn/TmnTransactionType;->Miryo:Lcom/squareup/tmn/TmnTransactionType;

    sget-object v0, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;->getBrand()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->getTmnBrandId(Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    move-result-object v11

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-string v2, "input.money.amount"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    move-object v0, v3

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;->getTmnWorkflowInputAction()Lcom/squareup/tmn/Action;

    move-result-object v14

    move-object v9, v5

    .line 528
    invoke-direct/range {v9 .. v14}, Lcom/squareup/tmn/TmnInput;-><init>(Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;JLcom/squareup/tmn/Action;)V

    const/4 v6, 0x0

    .line 531
    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$7;

    invoke-direct {v0, v8, v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$7;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;)V

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v9, 0x4

    const/4 v10, 0x0

    move-object/from16 v0, p3

    move-object v1, v4

    move-object v2, v5

    move-object v3, v6

    move-object v4, v7

    move v5, v9

    move-object v6, v10

    .line 526
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    :cond_e
    :goto_1
    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle;
    .locals 3

    const-string p2, "input"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    new-instance p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle;

    sget-object p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle$EmoneyIdleType$ReaderNotReady;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle$EmoneyIdleType$ReaderNotReady;

    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle$EmoneyIdleType;

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-direct {p1, p2, v0, v1, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle$EmoneyIdleType;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 123
    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->initialState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 123
    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;

    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->render(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "-",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->lifecycleWorker:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$lifecycleWorker$1;

    check-cast v0, Lcom/squareup/workflow/Worker;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p3, v0, v2, v1, v2}, Lcom/squareup/workflow/RenderContextKt;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;ILjava/lang/Object;)V

    .line 194
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->runWorkersAndChildren(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/workflow/RenderContext;)V

    .line 197
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle;

    if-eqz v0, :cond_5

    .line 198
    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle;->getIdleType()Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle$EmoneyIdleType;

    move-result-object v0

    .line 199
    instance-of v3, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle$EmoneyIdleType$ReaderNotReady;

    if-eqz v3, :cond_0

    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$StartingReader;

    .line 200
    new-instance v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$screenState$1;

    invoke-direct {v3, p0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$screenState$1;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v3}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v3

    .line 199
    invoke-direct {v0, v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$StartingReader;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    goto :goto_0

    .line 206
    :cond_0
    instance-of v3, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle$EmoneyIdleType$ReaderDisconnected;

    if-eqz v3, :cond_1

    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReaderNotConnected;

    .line 207
    new-instance v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$screenState$2;

    invoke-direct {v3, p0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$screenState$2;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v3}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v3

    .line 206
    invoke-direct {v0, v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReaderNotConnected;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    goto :goto_0

    .line 213
    :cond_1
    instance-of v0, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle$EmoneyIdleType$WaitingForTap;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;

    .line 214
    new-instance v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$screenState$3;

    invoke-direct {v3, p0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$screenState$3;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v3}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v3

    .line 219
    iget-object v4, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->emoneyIconFactory:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;

    .line 213
    invoke-direct {v0, v3, v4}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;-><init>(Lkotlin/jvm/functions/Function1;Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;)V

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    .line 223
    :goto_0
    invoke-direct {p0, v0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->createScreenBody(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v5

    .line 224
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle;->getShowingCancelDialog()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 225
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle;->getIdleType()Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle$EmoneyIdleType;

    move-result-object p1

    instance-of p1, p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle$EmoneyIdleType$WaitingForTap;

    if-eqz p1, :cond_2

    .line 226
    sget-object p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$CancelRequested;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$CancelRequested;

    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    const-string v0, "User clicked Cancel"

    invoke-direct {p0, p1, v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_1

    .line 228
    :cond_2
    sget-object p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$dismissAction$1;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$dismissAction$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    const/4 v0, 0x1

    invoke-static {p0, v2, p1, v0, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_1
    move-object v8, p1

    .line 231
    new-instance p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle;->getIdleType()Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle$EmoneyIdleType;

    move-result-object v0

    const/4 v3, 0x0

    invoke-direct {p1, v0, v3, v1, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle$EmoneyIdleType;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v6, p1

    check-cast v6, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle;->getIdleType()Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentIdle$EmoneyIdleType;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    const-string p1, "state.idleType.javaClass.simpleName"

    invoke-static {v7, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, p0

    move-object v4, p3

    .line 230
    invoke-direct/range {v3 .. v8}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->createDialogStack(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Ljava/lang/String;Lcom/squareup/workflow/WorkflowAction;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_5

    .line 235
    :cond_3
    sget-object p1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v5, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_5

    .line 213
    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 238
    :cond_5
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;

    if-eqz v0, :cond_6

    .line 239
    new-instance p3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Retap;

    .line 240
    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$WaitingForRetap;->getErrorMessage()Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    move-result-object p2

    .line 241
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->emoneyIconFactory:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;

    .line 239
    invoke-direct {p3, p2, v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Retap;-><init>(Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;)V

    check-cast p3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    .line 238
    invoke-direct {p0, p3, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->createSimpleScreen(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_5

    .line 245
    :cond_6
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$CancelRequested;

    if-eqz v0, :cond_7

    sget-object p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$Cancelling;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$Cancelling;

    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    invoke-direct {p0, p2, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->createSimpleScreen(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_5

    .line 246
    :cond_7
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$NoAuthOrReaderResponse;

    if-eqz v0, :cond_a

    .line 247
    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$NoAuthOrReaderResponse;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$NoAuthOrReaderResponse;->getTmnDisplayRequest()Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;

    move-result-object p2

    if-eqz p2, :cond_8

    invoke-virtual {p2}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->getDisplayRequest()Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    move-result-object v2

    :cond_8
    sget-object p2, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_ONLINE_PROCESSING:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    if-ne v2, p2, :cond_9

    .line 248
    sget-object p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$Online;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$Online;

    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing;

    goto :goto_2

    .line 250
    :cond_9
    sget-object p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$Generic;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$Generic;

    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing;

    .line 247
    :goto_2
    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    .line 246
    invoke-direct {p0, p2, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->createSimpleScreen(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_5

    .line 253
    :cond_a
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulAuthResponseOnly;

    if-eqz v0, :cond_d

    .line 254
    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulAuthResponseOnly;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulAuthResponseOnly;->getTmnDisplayRequest()Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;

    move-result-object p2

    if-eqz p2, :cond_b

    invoke-virtual {p2}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->getDisplayRequest()Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    move-result-object v2

    :cond_b
    sget-object p2, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_ONLINE_PROCESSING:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    if-ne v2, p2, :cond_c

    .line 255
    sget-object p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$Online;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$Online;

    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing;

    goto :goto_3

    .line 257
    :cond_c
    sget-object p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$Generic;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$Generic;

    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing;

    .line 254
    :goto_3
    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    .line 253
    invoke-direct {p0, p2, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->createSimpleScreen(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_5

    .line 260
    :cond_d
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;

    if-eqz v0, :cond_e

    sget-object p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$Authorizing;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$Authorizing;

    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    invoke-direct {p0, p2, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->createSimpleScreen(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_5

    .line 261
    :cond_e
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$ReceivedRetryableErrorMessage;

    if-eqz v0, :cond_f

    sget-object p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$ReceivingError;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Processing$ReceivingError;

    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    invoke-direct {p0, p2, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->createSimpleScreen(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_5

    .line 262
    :cond_f
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentComplete;

    if-eqz v0, :cond_10

    .line 263
    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Complete;

    check-cast p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentComplete;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentComplete;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 264
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentComplete;->getFromMiryo()Z

    move-result p2

    .line 265
    sget-object v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$1;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p3

    .line 263
    invoke-direct {v0, v1, p2, p3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$Complete;-><init>(Lcom/squareup/protos/common/Money;ZLkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    .line 262
    invoke-direct {p0, v0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->createSimpleScreen(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_5

    .line 269
    :cond_10
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;

    if-eqz v0, :cond_15

    .line 270
    move-object v0, p2

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;->getErrorType()Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType;

    move-result-object v1

    .line 271
    instance-of v2, v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType$RetryablePaymentError;

    if-eqz v2, :cond_11

    new-instance v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$RetryableError;

    .line 272
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 273
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;->getErrorMessage()Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    move-result-object v3

    .line 274
    new-instance v4, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$screenState$4;

    invoke-direct {v4, p0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$screenState$4;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v4}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v4

    .line 277
    new-instance v5, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$screenState$5;

    invoke-direct {v5, p0, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$screenState$5;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v5}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 271
    invoke-direct {v1, v2, v3, v4, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$RetryableError;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    goto :goto_4

    .line 286
    :cond_11
    instance-of v2, v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType$AuthOnlyRetryablePaymentError;

    if-eqz v2, :cond_12

    new-instance v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$RetryableError;

    .line 287
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 288
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;->getErrorMessage()Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    move-result-object v3

    .line 289
    new-instance v4, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$screenState$6;

    invoke-direct {v4, p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$screenState$6;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v4}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v4

    .line 293
    new-instance v5, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$screenState$7;

    invoke-direct {v5, p0, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$screenState$7;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v5}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 286
    invoke-direct {v1, v2, v3, v4, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$RetryableError;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    goto :goto_4

    .line 302
    :cond_12
    instance-of v1, v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType$FatalPaymentError;

    if-eqz v1, :cond_14

    new-instance v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$FatalError;

    .line 303
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 304
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;->getErrorMessage()Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    move-result-object v3

    .line 305
    new-instance v4, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$screenState$8;

    invoke-direct {v4, p0, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$screenState$8;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v4}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 302
    invoke-direct {v1, v2, v3, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$FatalError;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;

    .line 315
    :goto_4
    invoke-direct {p0, v1, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->createScreenBody(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v4

    .line 317
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;->getShowingCancelDialog()Z

    move-result p1

    if-eqz p1, :cond_13

    .line 319
    new-instance p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;->getErrorType()Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType;

    move-result-object v6

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;->getErrorMessage()Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    move-result-object v7

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object v8

    const/4 v9, 0x0

    const/16 v10, 0x8

    const/4 v11, 0x0

    move-object v5, p1

    invoke-direct/range {v5 .. v11}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;Lcom/squareup/protos/common/Money;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v5, p1

    check-cast v5, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    .line 320
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;->getErrorType()Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    const-string p1, "state.errorType.javaClass.simpleName"

    invoke-static {v6, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v7, 0x0

    const/16 v8, 0x10

    const/4 v9, 0x0

    move-object v2, p0

    move-object v3, p3

    .line 318
    invoke-static/range {v2 .. v9}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->createDialogStack$default(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Ljava/lang/String;Lcom/squareup/workflow/WorkflowAction;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    goto :goto_5

    .line 323
    :cond_13
    sget-object p1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v4, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_5

    .line 302
    :cond_14
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 326
    :cond_15
    instance-of v0, p2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    if-eqz v0, :cond_16

    .line 328
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->miryoWorkflow:Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoWorkflow;

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 329
    new-instance v0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;

    .line 330
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;->getBrand()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    move-result-object v5

    move-object v1, p2

    check-cast v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    invoke-virtual {v1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object v6

    invoke-virtual {v1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;->getShouldShowUnresolved()Z

    move-result v7

    .line 331
    invoke-virtual {v1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;->getHasValidReaderConnected()Z

    move-result v8

    move-object v3, v0

    .line 329
    invoke-direct/range {v3 .. v8}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;Lcom/squareup/protos/common/Money;ZZ)V

    const/4 v4, 0x0

    .line 333
    new-instance v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$render$2;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;)V

    move-object v5, v1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 327
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    :goto_5
    return-object p1

    :cond_16
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 185
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 123
    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->snapshotState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
