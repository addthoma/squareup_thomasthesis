.class final Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark$AnimationCallbackLooper;
.super Landroidx/vectordrawable/graphics/drawable/Animatable2Compat$AnimationCallback;
.source "EmoneyIconFactoryDark.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AnimationCallbackLooper"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark$AnimationCallbackLooper;",
        "Landroidx/vectordrawable/graphics/drawable/Animatable2Compat$AnimationCallback;",
        "(Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark;)V",
        "onAnimationEnd",
        "",
        "drawable",
        "Landroid/graphics/drawable/Drawable;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark;


# direct methods
.method public constructor <init>(Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 45
    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark$AnimationCallbackLooper;->this$0:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark;

    invoke-direct {p0}, Landroidx/vectordrawable/graphics/drawable/Animatable2Compat$AnimationCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    const-string v0, "drawable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-super {p0, p1}, Landroidx/vectordrawable/graphics/drawable/Animatable2Compat$AnimationCallback;->onAnimationEnd(Landroid/graphics/drawable/Drawable;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark$AnimationCallbackLooper;->this$0:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark;

    invoke-static {v0}, Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark;->access$getMainThread$p(Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark;)Lcom/squareup/thread/executor/MainThread;

    move-result-object v0

    new-instance v1, Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark$AnimationCallbackLooper$onAnimationEnd$1;

    invoke-direct {v1, p1}, Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark$AnimationCallbackLooper$onAnimationEnd$1;-><init>(Landroid/graphics/drawable/Drawable;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method
