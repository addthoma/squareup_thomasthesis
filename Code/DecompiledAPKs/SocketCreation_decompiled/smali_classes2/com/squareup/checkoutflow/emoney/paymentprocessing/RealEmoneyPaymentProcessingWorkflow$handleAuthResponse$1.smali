.class final Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponse$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEmoneyPaymentProcessingWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->handleAuthResponse(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "+",
        "Lcom/squareup/protos/client/bills/AddTendersResponse;",
        ">;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
        "+",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
        "authResult",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/bills/AddTendersResponse;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $stateFactory:Lkotlin/jvm/functions/Function1;

.field final synthetic this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponse$1;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponse$1;->$stateFactory:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/AddTendersResponse;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
            ">;"
        }
    .end annotation

    const-string v0, "authResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 705
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponse$1;->$stateFactory:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    goto :goto_0

    .line 706
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_2

    .line 707
    move-object v0, p1

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-virtual {v0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getRetryable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 708
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponse$1;->$stateFactory:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    goto :goto_0

    .line 711
    :cond_1
    new-instance v8, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;

    sget-object v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType$FatalPaymentError;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType$FatalPaymentError;

    move-object v2, v1

    check-cast v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType;

    new-instance v1, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ServerMessage;

    iget-object v3, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponse$1;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    invoke-static {v3, v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$getAuthFailureMessage(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ServerMessage;-><init>(Ljava/lang/String;)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v1, v8

    invoke-direct/range {v1 .. v7}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;Lcom/squareup/protos/common/Money;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v0, v8

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    .line 716
    :goto_0
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponse$1;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    invoke-static {v1, v0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    .line 707
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 123
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponse$1;->invoke(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
