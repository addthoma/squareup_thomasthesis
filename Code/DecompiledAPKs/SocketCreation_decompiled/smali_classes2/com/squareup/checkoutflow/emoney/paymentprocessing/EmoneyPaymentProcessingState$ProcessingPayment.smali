.class public abstract Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment;
.super Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;
.source "EmoneyPaymentProcessingState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ProcessingPayment"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$NoAuthOrReaderResponse;,
        Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulAuthResponseOnly;,
        Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;,
        Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$ReceivedRetryableErrorMessage;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0004\u0003\u0004\u0005\u0006B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0004\u0007\u0008\t\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
        "()V",
        "NoAuthOrReaderResponse",
        "ReceivedRetryableErrorMessage",
        "SuccessfulAuthResponseOnly",
        "SuccessfulReaderResponseOnly",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$NoAuthOrReaderResponse;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulAuthResponseOnly;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$ReceivedRetryableErrorMessage;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 60
    invoke-direct {p0, v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 60
    invoke-direct {p0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment;-><init>()V

    return-void
.end method
