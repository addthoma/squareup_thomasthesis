.class final Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$handleDisplayRequest$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEmoneyCheckBalanceWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow;->handleDisplayRequest(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;",
        "+",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceOutput;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealEmoneyCheckBalanceWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealEmoneyCheckBalanceWorkflow.kt\ncom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$handleDisplayRequest$1\n*L\n1#1,194:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceOutput;",
        "result",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $input:Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$handleDisplayRequest$1;->$input:Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;",
            "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->getDisplayRequest()Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    move-result-object v0

    sget-object v1, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    const/4 v2, 0x2

    if-eq v0, v1, :cond_4

    if-eq v0, v2, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 160
    new-instance v0, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$CheckBalanceError;

    new-instance v1, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ReaderMessage;

    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->getDisplayRequest()Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ReaderMessage;-><init>(Lcom/squareup/cardreader/lcr/CrsTmnMessage;)V

    check-cast v1, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$CheckBalanceError;-><init>(Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;)V

    check-cast v0, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;

    goto :goto_0

    .line 156
    :cond_0
    new-instance v0, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$WaitingForRetap;

    new-instance v1, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ReaderMessage;

    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->getDisplayRequest()Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ReaderMessage;-><init>(Lcom/squareup/cardreader/lcr/CrsTmnMessage;)V

    check-cast v1, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$WaitingForRetap;-><init>(Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;)V

    check-cast v0, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;

    goto :goto_0

    .line 146
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->getBalance()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/squareup/util/Strings;->removeNonDigits(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 147
    invoke-static {p1}, Lkotlin/text/StringsKt;->toLongOrNull(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 149
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$handleDisplayRequest$1;->$input:Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceInput;->getCurrency()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 153
    new-instance v0, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$CheckBalanceComplete;

    invoke-direct {v0, p1}, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$CheckBalanceComplete;-><init>(Lcom/squareup/protos/common/Money;)V

    check-cast v0, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;

    goto :goto_0

    .line 151
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Invalid balance value received for card"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 143
    :cond_3
    new-instance v0, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$CheckingBalance$NoAuthOrReaderResponse;

    invoke-direct {v0, p1}, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$CheckingBalance$NoAuthOrReaderResponse;-><init>(Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;)V

    check-cast v0, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;

    goto :goto_0

    .line 141
    :cond_4
    sget-object p1, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$WaitingForTap;->INSTANCE:Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$WaitingForTap;

    move-object v0, p1

    check-cast v0, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;

    .line 164
    :goto_0
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1, v2, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 55
    check-cast p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/emoney/checkbalance/RealEmoneyCheckBalanceWorkflow$handleDisplayRequest$1;->invoke(Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
