.class final Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelDialogFactory$create$1$1;
.super Ljava/lang/Object;
.source "EmoneyPaymentProcessingCancelDialogFactory.kt"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/content/DialogInterface;",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 1>",
        "",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/workflow/legacy/Screen;

.field final synthetic this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelDialogFactory$create$1;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelDialogFactory$create$1;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelDialogFactory$create$1$1;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelDialogFactory$create$1;

    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelDialogFactory$create$1$1;->$screen:Lcom/squareup/workflow/legacy/Screen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 35
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelDialogFactory$create$1$1;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelDialogFactory$create$1;

    iget-object p1, p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelDialogFactory$create$1;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelDialogFactory;

    invoke-static {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelDialogFactory;->access$getGatekeeper$p(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelDialogFactory;)Lcom/squareup/permissions/PermissionGatekeeper;

    move-result-object p1

    sget-object p2, Lcom/squareup/permissions/Permission;->CANCEL_BUYER_FLOW:Lcom/squareup/permissions/Permission;

    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelDialogFactory$create$1$1$1;

    invoke-direct {v0, p0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelDialogFactory$create$1$1$1;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelDialogFactory$create$1$1;)V

    check-cast v0, Lcom/squareup/permissions/PermissionGatekeeper$When;

    invoke-virtual {p1, p2, v0}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessExplicitlyGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method
