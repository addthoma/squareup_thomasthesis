.class public final Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark;
.super Ljava/lang/Object;
.source "EmoneyIconFactoryDark.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark$AnimationCallbackLooper;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEmoneyIconFactoryDark.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EmoneyIconFactoryDark.kt\ncom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark\n*L\n1#1,56:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\tB\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark;",
        "Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;",
        "mainThread",
        "Lcom/squareup/thread/executor/MainThread;",
        "(Lcom/squareup/thread/executor/MainThread;)V",
        "create",
        "Landroid/graphics/drawable/Drawable;",
        "context",
        "Landroid/content/Context;",
        "AnimationCallbackLooper",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final mainThread:Lcom/squareup/thread/executor/MainThread;


# direct methods
.method public constructor <init>(Lcom/squareup/thread/executor/MainThread;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "mainThread"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-void
.end method

.method public static final synthetic access$getMainThread$p(Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark;)Lcom/squareup/thread/executor/MainThread;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-object p0
.end method


# virtual methods
.method public create(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    sget v0, Lcom/squareup/checkoutflow/emoney/impl/R$drawable;->icon_contactless_animated_border_336:I

    .line 34
    invoke-static {p1, v0}, Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;->create(Landroid/content/Context;I)Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 40
    new-instance v0, Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark$AnimationCallbackLooper;

    invoke-direct {v0, p0}, Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark$AnimationCallbackLooper;-><init>(Lcom/squareup/checkoutflow/emoney/EmoneyIconFactoryDark;)V

    check-cast v0, Landroidx/vectordrawable/graphics/drawable/Animatable2Compat$AnimationCallback;

    invoke-virtual {p1, v0}, Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;->registerAnimationCallback(Landroidx/vectordrawable/graphics/drawable/Animatable2Compat$AnimationCallback;)V

    .line 41
    invoke-virtual {p1}, Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;->start()V

    .line 39
    check-cast p1, Landroid/graphics/drawable/Drawable;

    return-object p1

    .line 33
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
