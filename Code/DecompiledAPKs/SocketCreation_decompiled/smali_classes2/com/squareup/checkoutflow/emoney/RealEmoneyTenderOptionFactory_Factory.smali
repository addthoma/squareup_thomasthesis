.class public final Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory_Factory;
.super Ljava/lang/Object;
.source "RealEmoneyTenderOptionFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 44
    iput-object p7, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory_Factory;->arg6Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory_Factory;"
        }
    .end annotation

    .line 56
    new-instance v8, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/util/Res;Ldagger/Lazy;Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;)Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Ldagger/Lazy<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;",
            "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;"
        }
    .end annotation

    .line 62
    new-instance v8, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;-><init>(Lcom/squareup/util/Res;Ldagger/Lazy;Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;
    .locals 8

    .line 49
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/cardreader/CardReaderHub;

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/cardreader/CardReaderHubUtils;

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/settings/server/Features;

    invoke-static/range {v1 .. v7}, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory_Factory;->newInstance(Lcom/squareup/util/Res;Ldagger/Lazy;Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;)Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory_Factory;->get()Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;

    move-result-object v0

    return-object v0
.end method
