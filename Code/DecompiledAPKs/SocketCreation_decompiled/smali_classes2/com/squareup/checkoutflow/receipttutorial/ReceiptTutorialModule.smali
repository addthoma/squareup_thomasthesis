.class public abstract Lcom/squareup/checkoutflow/receipttutorial/ReceiptTutorialModule;
.super Ljava/lang/Object;
.source "ReceiptTutorialModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$provideReceiptTutorial$0(Lcom/squareup/register/tutorial/TutorialPresenter;)V
    .locals 0

    .line 10
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/TutorialPresenter;->onShowingThanks()V

    return-void
.end method

.method static provideReceiptTutorial(Lcom/squareup/register/tutorial/TutorialPresenter;)Lcom/squareup/checkoutflow/receipttutorial/ReceiptTutorial;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 10
    new-instance v0, Lcom/squareup/checkoutflow/receipttutorial/-$$Lambda$ReceiptTutorialModule$3m8V1tYDpmE32kQ2ZfuDJh4Yijk;

    invoke-direct {v0, p0}, Lcom/squareup/checkoutflow/receipttutorial/-$$Lambda$ReceiptTutorialModule$3m8V1tYDpmE32kQ2ZfuDJh4Yijk;-><init>(Lcom/squareup/register/tutorial/TutorialPresenter;)V

    return-object v0
.end method
