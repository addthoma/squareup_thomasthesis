.class public final Lcom/squareup/checkoutflow/RealBlockedByBuyerFacingDisplayViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "RealBlockedByBuyerFacingDisplayViewFactory.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/RealBlockedByBuyerFacingDisplayViewFactory;",
        "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "()V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 10
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 11
    const-class v2, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayDialog;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    .line 12
    sget-object v3, Lcom/squareup/checkoutflow/RealBlockedByBuyerFacingDisplayViewFactory$1;->INSTANCE:Lcom/squareup/checkoutflow/RealBlockedByBuyerFacingDisplayViewFactory$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 10
    invoke-virtual {v1, v2, v3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 9
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
