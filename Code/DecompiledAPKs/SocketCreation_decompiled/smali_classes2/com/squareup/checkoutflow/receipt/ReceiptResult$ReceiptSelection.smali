.class public abstract Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;
.super Ljava/lang/Object;
.source "ReceiptResult.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/receipt/ReceiptResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ReceiptSelection"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DeclineReceipt;,
        Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$PaperReceipt;,
        Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$FormalPaperReceipt;,
        Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0004\u0007\u0008\t\nB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0004\u000b\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;",
        "Landroid/os/Parcelable;",
        "receiptSelectionType",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;",
        "(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V",
        "getReceiptSelectionType",
        "()Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;",
        "DeclineReceipt",
        "DigitalReceipt",
        "FormalPaperReceipt",
        "PaperReceipt",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DeclineReceipt;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$PaperReceipt;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$FormalPaperReceipt;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final receiptSelectionType:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;


# direct methods
.method private constructor <init>(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;->receiptSelectionType:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V

    return-void
.end method


# virtual methods
.method public final getReceiptSelectionType()Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;->receiptSelectionType:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    return-object v0
.end method
