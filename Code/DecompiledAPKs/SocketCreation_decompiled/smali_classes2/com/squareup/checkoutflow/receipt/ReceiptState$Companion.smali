.class public final Lcom/squareup/checkoutflow/receipt/ReceiptState$Companion;
.super Ljava/lang/Object;
.source "ReceiptState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/receipt/ReceiptState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReceiptState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReceiptState.kt\ncom/squareup/checkoutflow/receipt/ReceiptState$Companion\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n*L\n1#1,95:1\n32#2,12:96\n*E\n*S KotlinDebug\n*F\n+ 1 ReceiptState.kt\ncom/squareup/checkoutflow/receipt/ReceiptState$Companion\n*L\n84#1,12:96\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\u00020\u00042\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/ReceiptState$Companion;",
        "",
        "()V",
        "start",
        "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "shouldSkipReceiptSelection",
        "",
        "defaultEmail",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 78
    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/ReceiptState$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final start(Lcom/squareup/workflow/Snapshot;ZLjava/lang/String;)Lcom/squareup/checkoutflow/receipt/ReceiptState;
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_4

    .line 96
    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result v1

    const/4 v2, 0x0

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v0

    :goto_1
    if-eqz p1, :cond_3

    .line 101
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v1, "Parcel.obtain()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 103
    array-length v1, p1

    invoke-virtual {v0, p1, v2, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 104
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 105
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p1

    if-nez p1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    goto :goto_2

    :cond_3
    move-object p1, v0

    .line 107
    :goto_2
    move-object v0, p1

    check-cast v0, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    :cond_4
    if-eqz v0, :cond_5

    .line 86
    new-instance p1, Lcom/squareup/checkoutflow/receipt/ReceiptState;

    invoke-direct {p1, v0}, Lcom/squareup/checkoutflow/receipt/ReceiptState;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;)V

    goto :goto_3

    :cond_5
    if-eqz p2, :cond_6

    .line 88
    new-instance p1, Lcom/squareup/checkoutflow/receipt/ReceiptState;

    sget-object p2, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;->Companion:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$Companion;

    invoke-virtual {p2, p3}, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$Companion;->getReceiptCompleteState$impl_release(Ljava/lang/String;)Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;

    move-result-object p2

    check-cast p2, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    invoke-direct {p1, p2}, Lcom/squareup/checkoutflow/receipt/ReceiptState;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;)V

    goto :goto_3

    .line 90
    :cond_6
    new-instance p1, Lcom/squareup/checkoutflow/receipt/ReceiptState;

    sget-object p2, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptSelectionState;->INSTANCE:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptSelectionState;

    check-cast p2, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    invoke-direct {p1, p2}, Lcom/squareup/checkoutflow/receipt/ReceiptState;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;)V

    :goto_3
    return-object p1
.end method
