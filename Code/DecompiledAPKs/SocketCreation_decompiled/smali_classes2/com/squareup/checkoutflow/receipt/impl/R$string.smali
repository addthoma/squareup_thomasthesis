.class public final Lcom/squareup/checkoutflow/receipt/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/receipt/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final buyer_receipt_add_customer:I = 0x7f120247

.field public static final buyer_receipt_email:I = 0x7f120248

.field public static final buyer_receipt_email_hint:I = 0x7f120249

.field public static final buyer_receipt_invalid_email:I = 0x7f12024a

.field public static final buyer_receipt_invalid_input:I = 0x7f12024b

.field public static final buyer_receipt_invalid_sms:I = 0x7f12024c

.field public static final buyer_receipt_print:I = 0x7f12024e

.field public static final buyer_receipt_print_formal:I = 0x7f12024f

.field public static final buyer_receipt_printed:I = 0x7f120250

.field public static final buyer_receipt_send:I = 0x7f120251

.field public static final buyer_receipt_sent:I = 0x7f120252

.field public static final buyer_receipt_text:I = 0x7f120253

.field public static final buyer_receipt_text_hint:I = 0x7f120254

.field public static final buyer_receipt_view_customer:I = 0x7f120256

.field public static final sms_marketing_check_your_texts:I = 0x7f121828

.field public static final sms_marketing_input_detail:I = 0x7f121829

.field public static final sms_marketing_input_enter_phone_number:I = 0x7f12182a

.field public static final sms_marketing_input_is_this_correct:I = 0x7f12182b

.field public static final sms_marketing_input_submit:I = 0x7f12182c

.field public static final sms_marketing_no_thanks:I = 0x7f12182d

.field public static final sms_marketing_receipt_sent_to:I = 0x7f12182e

.field public static final sms_marketing_sign_up:I = 0x7f12182f

.field public static final sms_marketing_transaction_complete:I = 0x7f121830

.field public static final sms_marketing_youre_all_set:I = 0x7f121831


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
