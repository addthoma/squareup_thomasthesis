.class final Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$attach$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ReceiptInputCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012<\u0010\u0002\u001a8\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005 \u0007*\u001c\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0003j\n\u0012\u0004\u0012\u00020\u0004\u0018\u0001`\u00060\u0003j\u0008\u0012\u0004\u0012\u00020\u0004`\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$attach$1;->this$0:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 44
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$attach$1;->invoke(Lcom/squareup/workflow/legacy/Screen;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 3

    const-string v0, "it"

    .line 79
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/workflow/legacy/ScreenKt;->getUnwrapV2Screen(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/workflow/legacy/V2Screen;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->getScreenState()Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;

    move-result-object p1

    instance-of p1, p1, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState$SmsInput;

    if-eqz p1, :cond_0

    .line 80
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$attach$1;->this$0:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;

    invoke-static {p1}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->access$getInputField$p(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;)Lcom/squareup/noho/NohoEditText;

    move-result-object p1

    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$attach$1$1;

    .line 81
    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$attach$1;->this$0:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;

    invoke-static {v1}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->access$getScrubber$p(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;)Lcom/squareup/text/InsertingScrubber;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$attach$1;->this$0:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;

    invoke-static {v2}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;->access$getInputField$p(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator;)Lcom/squareup/noho/NohoEditText;

    move-result-object v2

    check-cast v2, Lcom/squareup/text/HasSelectableText;

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$attach$1$1;-><init>(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$attach$1;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    check-cast v0, Landroid/text/TextWatcher;

    .line 80
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_0
    return-void
.end method
