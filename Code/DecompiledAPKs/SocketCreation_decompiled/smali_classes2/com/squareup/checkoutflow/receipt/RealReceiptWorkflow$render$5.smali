.class final Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$5;
.super Lkotlin/jvm/internal/Lambda;
.source "RealReceiptWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->render(Lcom/squareup/checkoutflow/receipt/ReceiptInput;Lcom/squareup/checkoutflow/receipt/ReceiptState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
        "+",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
        "it",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screenState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

.field final synthetic $state:Lcom/squareup/checkoutflow/receipt/ReceiptState;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;Lcom/squareup/checkoutflow/receipt/ReceiptState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$5;->$screenState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$5;->$state:Lcom/squareup/checkoutflow/receipt/ReceiptState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
            "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 245
    sget-object v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$NoSmsMarketing;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$NoSmsMarketing;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 246
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 247
    new-instance v0, Lcom/squareup/checkoutflow/receipt/ReceiptState;

    .line 248
    new-instance v3, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;

    .line 249
    new-instance v4, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt;

    iget-object v5, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$5;->$screenState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    check-cast v5, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingState;

    invoke-virtual {v5}, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingState;->getSmsDestination()Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$SmsDestination;

    move-result-object v5

    check-cast v5, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination;

    invoke-direct {v4, v5}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination;)V

    check-cast v4, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;

    sget-object v5, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;->NO_SMS_MARKETING:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;

    .line 248
    invoke-direct {v3, v4, v5}, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;)V

    check-cast v3, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    .line 250
    iget-object v4, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$5;->$state:Lcom/squareup/checkoutflow/receipt/ReceiptState;

    invoke-virtual {v4}, Lcom/squareup/checkoutflow/receipt/ReceiptState;->getRequestCreator()Lcom/squareup/picasso/RequestCreator;

    move-result-object v4

    .line 247
    invoke-direct {v0, v3, v4}, Lcom/squareup/checkoutflow/receipt/ReceiptState;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;Lcom/squareup/picasso/RequestCreator;)V

    .line 246
    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 253
    :cond_0
    sget-object v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$SmsMarketingDeclined;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$SmsMarketingDeclined;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 254
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 255
    new-instance v0, Lcom/squareup/checkoutflow/receipt/ReceiptState;

    .line 256
    new-instance v3, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;

    .line 257
    new-instance v4, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt;

    iget-object v5, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$5;->$screenState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    check-cast v5, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingState;

    invoke-virtual {v5}, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingState;->getSmsDestination()Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$SmsDestination;

    move-result-object v5

    check-cast v5, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination;

    invoke-direct {v4, v5}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination;)V

    check-cast v4, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;

    sget-object v5, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;->SMS_MARKETING_DECLINED:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;

    .line 256
    invoke-direct {v3, v4, v5}, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;)V

    check-cast v3, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    .line 259
    iget-object v4, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$5;->$state:Lcom/squareup/checkoutflow/receipt/ReceiptState;

    invoke-virtual {v4}, Lcom/squareup/checkoutflow/receipt/ReceiptState;->getRequestCreator()Lcom/squareup/picasso/RequestCreator;

    move-result-object v4

    .line 255
    invoke-direct {v0, v3, v4}, Lcom/squareup/checkoutflow/receipt/ReceiptState;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;Lcom/squareup/picasso/RequestCreator;)V

    .line 254
    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 262
    :cond_1
    sget-object v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$SmsMarketingAccepted;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$SmsMarketingAccepted;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 263
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 264
    new-instance v0, Lcom/squareup/checkoutflow/receipt/ReceiptState;

    .line 265
    new-instance v3, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;

    .line 266
    new-instance v4, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt;

    iget-object v5, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$5;->$screenState:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    check-cast v5, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingState;

    invoke-virtual {v5}, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingState;->getSmsDestination()Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$SmsDestination;

    move-result-object v5

    check-cast v5, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination;

    invoke-direct {v4, v5}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination;)V

    check-cast v4, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;

    sget-object v5, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;->SMS_MARKETING_ACCEPTED:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;

    .line 265
    invoke-direct {v3, v4, v5}, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;)V

    check-cast v3, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    .line 268
    iget-object v4, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$5;->$state:Lcom/squareup/checkoutflow/receipt/ReceiptState;

    invoke-virtual {v4}, Lcom/squareup/checkoutflow/receipt/ReceiptState;->getRequestCreator()Lcom/squareup/picasso/RequestCreator;

    move-result-object v4

    .line 264
    invoke-direct {v0, v3, v4}, Lcom/squareup/checkoutflow/receipt/ReceiptState;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;Lcom/squareup/picasso/RequestCreator;)V

    .line 263
    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 271
    :cond_2
    sget-object v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$BuyerLanguageClicked;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$BuyerLanguageClicked;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/receipt/ReceiptResult$BuyerLanguageClicked;->INSTANCE:Lcom/squareup/checkoutflow/receipt/ReceiptResult$BuyerLanguageClicked;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 272
    :cond_3
    sget-object v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$UpdateCustomerClicked;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$UpdateCustomerClicked;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/receipt/ReceiptResult$UpdateCustomerClicked;->INSTANCE:Lcom/squareup/checkoutflow/receipt/ReceiptResult$UpdateCustomerClicked;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 273
    :cond_4
    sget-object v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$AddCardClicked;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$AddCardClicked;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/receipt/ReceiptResult$AddCardClicked;->INSTANCE:Lcom/squareup/checkoutflow/receipt/ReceiptResult$AddCardClicked;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 274
    :cond_5
    instance-of p1, p1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$ExitedManually;

    if-eqz p1, :cond_6

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v0, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptExited$ExitedManually;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptExited$ExitedManually;-><init>(Z)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 115
    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$5;->invoke(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
