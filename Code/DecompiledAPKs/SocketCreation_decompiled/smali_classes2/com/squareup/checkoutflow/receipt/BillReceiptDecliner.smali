.class public final Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner;
.super Ljava/lang/Object;
.source "BillReceiptDecliner.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/receipt/ReceiptDecliner;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u000b\u001a\u00020\u000cH\u0016R\u0014\u0010\u0007\u001a\u00020\u00088BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptDecliner;",
        "receiptForLastPayment",
        "Lcom/squareup/payment/ReceiptForLastPayment;",
        "receiptSender",
        "Lcom/squareup/receipt/ReceiptSender;",
        "(Lcom/squareup/payment/ReceiptForLastPayment;Lcom/squareup/receipt/ReceiptSender;)V",
        "receipt",
        "Lcom/squareup/payment/PaymentReceipt;",
        "getReceipt",
        "()Lcom/squareup/payment/PaymentReceipt;",
        "declineReceipt",
        "",
        "impl-bill_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final receiptForLastPayment:Lcom/squareup/payment/ReceiptForLastPayment;

.field private final receiptSender:Lcom/squareup/receipt/ReceiptSender;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/ReceiptForLastPayment;Lcom/squareup/receipt/ReceiptSender;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "receiptForLastPayment"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "receiptSender"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner;->receiptForLastPayment:Lcom/squareup/payment/ReceiptForLastPayment;

    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    return-void
.end method

.method private final getReceipt()Lcom/squareup/payment/PaymentReceipt;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner;->receiptForLastPayment:Lcom/squareup/payment/ReceiptForLastPayment;

    invoke-interface {v0}, Lcom/squareup/payment/ReceiptForLastPayment;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public declineReceipt()V
    .locals 2

    .line 17
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/receipt/ReceiptSender;->receiptDeclined(Lcom/squareup/payment/PaymentReceipt;)V

    return-void
.end method
