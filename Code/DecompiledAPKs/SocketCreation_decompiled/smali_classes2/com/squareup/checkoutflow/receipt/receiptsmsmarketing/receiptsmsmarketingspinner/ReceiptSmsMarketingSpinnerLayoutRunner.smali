.class public final Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner;
.super Ljava/lang/Object;
.source "ReceiptSmsMarketingSpinnerLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerScreen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\rB\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\u000cH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "buyerActionBar",
        "Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private final view:Landroid/view/View;


# direct methods
.method private constructor <init>(Landroid/view/View;)V
    .locals 3

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner;->view:Landroid/view/View;

    .line 32
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/marin/R$id;->buyer_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 35
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/checkout/R$id;->glyph_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_1

    check-cast p1, Landroid/view/ViewGroup;

    .line 37
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/checkout/R$layout;->auth_spinner_glyph:I

    const/4 v2, 0x0

    .line 36
    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    check-cast v0, Landroid/view/View;

    .line 35
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void

    .line 36
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.marin.widgets.MarinSpinnerGlyph"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 35
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public synthetic constructor <init>(Landroid/view/View;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 3

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner;->view:Landroid/view/View;

    sget-object v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$showRendering$1;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$showRendering$1;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 48
    iget-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 49
    new-instance v0, Lcom/squareup/util/ViewString$TextString;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/checkout/R$string;->new_sale:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "view.context.getString(c\u2026eckout.R.string.new_sale)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    invoke-direct {v0, v1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v0, Lcom/squareup/util/ViewString;

    .line 50
    new-instance v1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$showRendering$2;

    invoke-direct {v1, p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$showRendering$2;-><init>(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerScreen;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 48
    invoke-virtual {p2, v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setUpLeftButton(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V

    .line 52
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerScreen;->getAddCardState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    move-result-object p2

    .line 53
    sget-object v0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState$Hidden;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState$Hidden;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    invoke-virtual {p2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->hideLeftGlyphButton()V

    goto :goto_0

    .line 54
    :cond_0
    instance-of v0, p2, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState$Showing;

    if-eqz v0, :cond_1

    .line 55
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SAVE_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    new-instance v2, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$showRendering$3;

    invoke-direct {v2, p2}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$showRendering$3;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setLeftGlyphButton(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V

    .line 61
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerScreen;->getUpdateCustomerState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    move-result-object p1

    .line 62
    instance-of p2, p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$AddCustomer;

    if-eqz p2, :cond_2

    iget-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 63
    new-instance v0, Lcom/squareup/util/ViewString$ResourceString;

    sget v1, Lcom/squareup/checkoutflow/receipt/impl/R$string;->buyer_receipt_add_customer:I

    invoke-direct {v0, v1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v0, Lcom/squareup/util/ViewString;

    .line 64
    new-instance v1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$showRendering$4;

    invoke-direct {v1, p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$showRendering$4;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 62
    invoke-virtual {p2, v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setUpRightButton(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V

    goto :goto_1

    .line 66
    :cond_2
    instance-of p2, p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$ViewCustomer;

    if-eqz p2, :cond_3

    iget-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 67
    new-instance v0, Lcom/squareup/util/ViewString$ResourceString;

    sget v1, Lcom/squareup/checkoutflow/receipt/impl/R$string;->buyer_receipt_view_customer:I

    invoke-direct {v0, v1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v0, Lcom/squareup/util/ViewString;

    .line 68
    new-instance v1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$showRendering$5;

    invoke-direct {v1, p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$showRendering$5;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 66
    invoke-virtual {p2, v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setUpRightButton(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V

    goto :goto_1

    .line 70
    :cond_3
    instance-of p1, p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$Hidden;

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->hideRightButton()V

    :cond_4
    :goto_1
    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner;->showRendering(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
