.class public final Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SelectTenderCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001:\u0001\u001bB9\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0010\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0018\u0010\u001a\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u000e\u001a\u00020\u0005H\u0002R\u0014\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/selecttender/SelectTenderScreenData;",
        "",
        "Lcom/squareup/checkoutflow/selecttender/SelectTenderScreen;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V",
        "data",
        "splitTenderButton",
        "Lcom/squareup/marketfont/MarketButton;",
        "title",
        "Lcom/squareup/marketfont/MarketTextView;",
        "upButton",
        "Lcom/squareup/glyph/SquareGlyphView;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "update",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final data:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/checkoutflow/selecttender/SelectTenderScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private splitTenderButton:Lcom/squareup/marketfont/MarketButton;

.field private title:Lcom/squareup/marketfont/MarketTextView;

.field private upButton:Lcom/squareup/glyph/SquareGlyphView;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p2, p0, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator;->res:Lcom/squareup/util/Res;

    .line 34
    sget-object p2, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator$data$1;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator$data$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "screens.map { it.data }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator;->data:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator;Landroid/view/View;Lcom/squareup/checkoutflow/selecttender/SelectTenderScreenData;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator;->update(Landroid/view/View;Lcom/squareup/checkoutflow/selecttender/SelectTenderScreenData;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 75
    sget v0, Lcom/squareup/checkoutflow/selecttender/impl/R$id;->title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.title)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator;->title:Lcom/squareup/marketfont/MarketTextView;

    .line 76
    sget v0, Lcom/squareup/checkoutflow/selecttender/impl/R$id;->select_payment_up_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.select_payment_up_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator;->upButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 77
    sget v0, Lcom/squareup/checkoutflow/selecttender/impl/R$id;->split_tender_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.split_tender_button)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator;->splitTenderButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/checkoutflow/selecttender/SelectTenderScreenData;)V
    .locals 2

    .line 51
    new-instance v0, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator$update$1;

    invoke-direct {v0, p2}, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator$update$1;-><init>(Lcom/squareup/checkoutflow/selecttender/SelectTenderScreenData;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 55
    iget-object p1, p0, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator;->upButton:Lcom/squareup/glyph/SquareGlyphView;

    if-nez p1, :cond_0

    const-string v0, "upButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator$update$2;

    invoke-direct {v0, p2}, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator$update$2;-><init>(Lcom/squareup/checkoutflow/selecttender/SelectTenderScreenData;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    iget-object p1, p0, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator;->splitTenderButton:Lcom/squareup/marketfont/MarketButton;

    const-string v0, "splitTenderButton"

    if-nez p1, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/selecttender/SelectTenderScreenData;->getSplitTenderEnabled()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketButton;->setEnabled(Z)V

    .line 62
    iget-object p1, p0, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator;->splitTenderButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p1, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v0, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator$update$3;

    invoke-direct {v0, p2}, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator$update$3;-><init>(Lcom/squareup/checkoutflow/selecttender/SelectTenderScreenData;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    iget-object p1, p0, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator;->title:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_3

    const-string v0, "title"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 68
    :cond_3
    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkoutflow/selecttender/impl/R$string;->select_tender_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/selecttender/SelectTenderScreenData;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p2

    invoke-interface {v1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    const-string v1, "amount"

    invoke-virtual {v0, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 70
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    .line 71
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator;->bindViews(Landroid/view/View;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator;->data:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator$attach$1;-><init>(Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "data.subscribe { update(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
