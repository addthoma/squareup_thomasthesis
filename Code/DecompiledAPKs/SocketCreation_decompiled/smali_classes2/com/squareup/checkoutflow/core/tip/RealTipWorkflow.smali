.class public final Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealTipWorkflow.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/core/tip/TipWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/checkoutflow/core/tip/TipProps;",
        "Lcom/squareup/checkoutflow/core/tip/TipState;",
        "Lcom/squareup/checkoutflow/core/tip/TipOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;",
        "Lcom/squareup/checkoutflow/core/tip/TipWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealTipWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealTipWorkflow.kt\ncom/squareup/checkoutflow/core/tip/RealTipWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,97:1\n32#2,12:98\n149#3,5:110\n149#3,5:115\n*E\n*S KotlinDebug\n*F\n+ 1 RealTipWorkflow.kt\ncom/squareup/checkoutflow/core/tip/RealTipWorkflow\n*L\n44#1,12:98\n73#1,5:110\n92#1,5:115\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012,\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012\u0016\u0012\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00070\u0006j\u0006\u0012\u0002\u0008\u0003`\u00080\u0002B/\u0008\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0002\u0010\u0013J\u001a\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00032\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016J>\u0010\u0019\u001a\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00070\u0006j\u0006\u0012\u0002\u0008\u0003`\u00082\u0006\u0010\u0016\u001a\u00020\u00032\u0006\u0010\u001a\u001a\u00020\u00042\u0012\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001cH\u0016J\u0010\u0010\u001d\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\u0004H\u0016R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;",
        "Lcom/squareup/checkoutflow/core/tip/TipWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/checkoutflow/core/tip/TipProps;",
        "Lcom/squareup/checkoutflow/core/tip/TipState;",
        "Lcom/squareup/checkoutflow/core/tip/TipOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "moneyLocaleHelper",
        "Lcom/squareup/money/MoneyLocaleHelper;",
        "device",
        "Lcom/squareup/util/Device;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;)V",
        "initialState",
        "Lcom/squareup/checkoutflow/core/tip/TipState$QuickTip;",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final device:Lcom/squareup/util/Device;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "currencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyLocaleHelper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p2, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    iput-object p3, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;->device:Lcom/squareup/util/Device;

    iput-object p4, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;->features:Lcom/squareup/settings/server/Features;

    iput-object p5, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getCurrencyCode$p(Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method

.method public static final synthetic access$getMoneyLocaleHelper$p(Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;)Lcom/squareup/money/MoneyLocaleHelper;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    return-object p0
.end method


# virtual methods
.method public initialState(Lcom/squareup/checkoutflow/core/tip/TipProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/core/tip/TipState$QuickTip;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 98
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 103
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 105
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 106
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 107
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 109
    :cond_3
    check-cast v1, Lcom/squareup/checkoutflow/core/tip/TipState$QuickTip;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 44
    :cond_4
    sget-object v1, Lcom/squareup/checkoutflow/core/tip/TipState$QuickTip;->INSTANCE:Lcom/squareup/checkoutflow/core/tip/TipState$QuickTip;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/checkoutflow/core/tip/TipProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;->initialState(Lcom/squareup/checkoutflow/core/tip/TipProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/core/tip/TipState$QuickTip;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/checkoutflow/core/tip/TipProps;Lcom/squareup/checkoutflow/core/tip/TipState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/core/tip/TipProps;",
            "Lcom/squareup/checkoutflow/core/tip/TipState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/core/tip/TipState;",
            "-",
            "Lcom/squareup/checkoutflow/core/tip/TipOutput;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    instance-of v0, p2, Lcom/squareup/checkoutflow/core/tip/TipState$QuickTip;

    const-string v1, ""

    if-eqz v0, :cond_0

    .line 55
    new-instance p2, Lcom/squareup/checkoutflow/core/tip/TipScreen;

    .line 56
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/tip/TipProps;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 57
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/tip/TipProps;->getHasAutoGratuity()Z

    move-result v4

    .line 58
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTabletAtLeast10Inches()Z

    move-result v5

    .line 59
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/tip/TipProps;->isUsingCustomAmounts()Z

    move-result v6

    .line 60
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_BUYER_LANGUAGE_SELECTION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v7

    .line 61
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/tip/TipProps;->getTipOptions()Ljava/util/List;

    move-result-object v8

    .line 62
    new-instance p1, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow$render$1;

    invoke-direct {p1, p0}, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow$render$1;-><init>(Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, p1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v9

    move-object v2, p2

    .line 55
    invoke-direct/range {v2 .. v9}, Lcom/squareup/checkoutflow/core/tip/TipScreen;-><init>(Lcom/squareup/protos/common/Money;ZZZZLjava/util/List;Lkotlin/jvm/functions/Function1;)V

    check-cast p2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 111
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 112
    const-class p3, Lcom/squareup/checkoutflow/core/tip/TipScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 113
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 111
    invoke-direct {p1, p3, p2, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    goto :goto_0

    .line 75
    :cond_0
    instance-of p2, p2, Lcom/squareup/checkoutflow/core/tip/TipState$CustomTip;

    if-eqz p2, :cond_1

    .line 76
    new-instance p2, Lcom/squareup/checkoutflow/core/tip/TipInputScreen;

    .line 77
    iget-object v3, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 78
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/tip/TipProps;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v4

    .line 79
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/tip/TipProps;->getCustomTipMaxMoney()Lcom/squareup/protos/common/Money;

    move-result-object v5

    .line 80
    sget-object v0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow$render$2;->INSTANCE:Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow$render$2;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v0}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v6

    .line 81
    new-instance v0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow$render$3;

    invoke-direct {v0, p0, p1}, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow$render$3;-><init>(Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;Lcom/squareup/checkoutflow/core/tip/TipProps;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v0}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v7

    move-object v2, p2

    .line 76
    invoke-direct/range {v2 .. v7}, Lcom/squareup/checkoutflow/core/tip/TipInputScreen;-><init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    check-cast p2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 116
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 117
    const-class p3, Lcom/squareup/checkoutflow/core/tip/TipInputScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 118
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 116
    invoke-direct {p1, p3, p2, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    :goto_0
    return-object p1

    .line 119
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/checkoutflow/core/tip/TipProps;

    check-cast p2, Lcom/squareup/checkoutflow/core/tip/TipState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;->render(Lcom/squareup/checkoutflow/core/tip/TipProps;Lcom/squareup/checkoutflow/core/tip/TipState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/checkoutflow/core/tip/TipState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/checkoutflow/core/tip/TipState;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;->snapshotState(Lcom/squareup/checkoutflow/core/tip/TipState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
