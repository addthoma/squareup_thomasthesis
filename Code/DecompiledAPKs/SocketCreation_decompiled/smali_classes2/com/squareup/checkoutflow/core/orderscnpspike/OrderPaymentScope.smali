.class public final Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "OrderPaymentScope.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentScope$ParentComponent;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderPaymentScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderPaymentScope.kt\ncom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n+ 3 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,21:1\n63#2:22\n24#3,4:23\n*E\n*S KotlinDebug\n*F\n+ 1 OrderPaymentScope.kt\ncom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentScope\n*L\n11#1:22\n19#1,4:23\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001:\u0001\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0016R\u001c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00048\u0006X\u0087\u0004\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u0005\u0010\u0002\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentScope;",
        "Lcom/squareup/ui/main/InMainActivityScope;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "CREATOR$annotations",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "ParentComponent",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentScope;

    invoke-direct {v0}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentScope;-><init>()V

    sput-object v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentScope;->INSTANCE:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentScope;

    .line 23
    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentScope$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentScope$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 26
    sput-object v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method

.method public static synthetic CREATOR$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-super {p0, p1}, Lcom/squareup/ui/main/InMainActivityScope;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object v0

    .line 22
    const-class v1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentScope$ParentComponent;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->componentInParent(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentScope$ParentComponent;

    .line 11
    invoke-interface {p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentScope$ParentComponent;->orderPaymentWorkflowRunner()Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    const-string p1, "super.buildScope(parentS\u2026)::registerServices\n    )"

    .line 10
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
