.class final Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderPaymentWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1;->invoke(Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;",
        "-",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentOutput;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $it:Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput;

.field final synthetic this$0:Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1;Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1$2;->this$0:Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1;

    iput-object p2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1$2;->$it:Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1$2;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;",
            "-",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;

    .line 77
    iget-object v1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1$2;->$it:Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput;

    check-cast v1, Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput$Error$Rejected;

    invoke-virtual {v1}, Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput$Error$Rejected;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 78
    iget-object v2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1$2;->$it:Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput;

    check-cast v2, Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput$Error$Rejected;

    invoke-virtual {v2}, Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput$Error$Rejected;->getLocalizedDescription()Ljava/lang/String;

    move-result-object v2

    .line 79
    iget-object v3, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1$2;->this$0:Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1;

    iget-object v3, v3, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1;->$state:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;

    check-cast v3, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;

    invoke-virtual {v3}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;->getTipAmount()Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 76
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method
