.class final Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderPaymentWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;->render(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;",
        "+",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentOutput;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderPaymentWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderPaymentWorkflow.kt\ncom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1\n*L\n1#1,131:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentOutput;",
        "it",
        "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;

.field final synthetic this$0:Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1;->this$0:Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;

    iput-object p2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1;->$state:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    sget-object v0, Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput$Success;->INSTANCE:Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput$Success;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1;->this$0:Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;

    sget-object v0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1$1;->INSTANCE:Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v2, v0, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 73
    :cond_0
    sget-object v0, Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput$Error$RemoteError;->INSTANCE:Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput$Error$RemoteError;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const-string v3, "An operation is not implemented: "

    if-nez v0, :cond_3

    .line 74
    instance-of v0, p1, Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput$Error$ClientError;

    if-nez v0, :cond_2

    .line 75
    instance-of v0, p1, Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput$Error$Rejected;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1;->this$0:Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;

    new-instance v3, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1$2;

    invoke-direct {v3, p0, p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1$2;-><init>(Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1;Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v2, v3, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 74
    :cond_2
    new-instance p1, Lkotlin/NotImplementedError;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "MPLAT-2549 Handle client error from CardProcessing"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 73
    :cond_3
    new-instance p1, Lkotlin/NotImplementedError;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "MPLAT-2548 handle remote error from CardProcessing"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1;->invoke(Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
