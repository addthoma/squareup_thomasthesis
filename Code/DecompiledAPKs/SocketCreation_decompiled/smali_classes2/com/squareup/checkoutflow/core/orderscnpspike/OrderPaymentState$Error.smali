.class public final Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;
.super Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;
.source "OrderPaymentState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Error"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u000f\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J)\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u00c6\u0001J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0003H\u00d6\u0001J\u0019\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u0012H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\t\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;",
        "title",
        "",
        "message",
        "tipAmount",
        "Lcom/squareup/protos/common/Money;",
        "(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V",
        "getMessage",
        "()Ljava/lang/String;",
        "getTipAmount",
        "()Lcom/squareup/protos/common/Money;",
        "getTitle",
        "component1",
        "component2",
        "component3",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final message:Ljava/lang/String;

.field private final tipAmount:Lcom/squareup/protos/common/Money;

.field private final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error$Creator;

    invoke-direct {v0}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error$Creator;-><init>()V

    sput-object v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V
    .locals 1

    const-string v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 24
    invoke-direct {p0, v0}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->title:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->message:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->tipAmount:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 23
    check-cast p3, Lcom/squareup/protos/common/Money;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->title:Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->message:Ljava/lang/String;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->tipAmount:Lcom/squareup/protos/common/Money;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->copy(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->message:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->tipAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;
    .locals 1

    const-string v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;

    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->title:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->title:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->message:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->message:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->tipAmount:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->tipAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getMessage()Ljava/lang/String;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->message:Ljava/lang/String;

    return-object v0
.end method

.method public final getTipAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->tipAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->title:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->title:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->message:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->tipAmount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error(title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", tipAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->tipAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->title:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->message:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->tipAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method
