.class public final Lcom/squareup/checkoutflow/core/tip/TipScreen;
.super Ljava/lang/Object;
.source "TipScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/core/tip/TipScreen$Event;,
        Lcom/squareup/checkoutflow/core/tip/TipScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0014\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 )2\u00020\u0001:\u0002)*BO\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\u0008\u001a\u00020\u0005\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u0012\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\r\u00a2\u0006\u0002\u0010\u0010J\t\u0010\u001a\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0005H\u00c6\u0003J\u000f\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u00c6\u0003J\u0015\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rH\u00c6\u0003Ja\u0010!\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00052\u000e\u0008\u0002\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0014\u0008\u0002\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rH\u00c6\u0001J\u0013\u0010\"\u001a\u00020\u00052\u0008\u0010#\u001a\u0004\u0018\u00010$H\u00d6\u0003J\t\u0010%\u001a\u00020&H\u00d6\u0001J\t\u0010\'\u001a\u00020(H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0012R\u0011\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0012R\u001d\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u0008\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0012R\u0017\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/tip/TipScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "totalAmount",
        "Lcom/squareup/protos/common/Money;",
        "hasAutoGratuity",
        "",
        "isTablet10Inch",
        "isUsingCustomTip",
        "showLanguageSelection",
        "tipOptions",
        "",
        "Lcom/squareup/protos/common/tipping/TipOption;",
        "onEvent",
        "Lkotlin/Function1;",
        "Lcom/squareup/checkoutflow/core/tip/TipScreen$Event;",
        "",
        "(Lcom/squareup/protos/common/Money;ZZZZLjava/util/List;Lkotlin/jvm/functions/Function1;)V",
        "getHasAutoGratuity",
        "()Z",
        "getOnEvent",
        "()Lkotlin/jvm/functions/Function1;",
        "getShowLanguageSelection",
        "getTipOptions",
        "()Ljava/util/List;",
        "getTotalAmount",
        "()Lcom/squareup/protos/common/Money;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "Companion",
        "Event",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/checkoutflow/core/tip/TipScreen$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;

.field public static final SHOWN:Ljava/lang/String; = "Shown TipScreen"


# instance fields
.field private final hasAutoGratuity:Z

.field private final isTablet10Inch:Z

.field private final isUsingCustomTip:Z

.field private final onEvent:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/checkoutflow/core/tip/TipScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final showLanguageSelection:Z

.field private final tipOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation
.end field

.field private final totalAmount:Lcom/squareup/protos/common/Money;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/checkoutflow/core/tip/TipScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/core/tip/TipScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->Companion:Lcom/squareup/checkoutflow/core/tip/TipScreen$Companion;

    .line 37
    const-class v0, Lcom/squareup/checkoutflow/core/tip/TipScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;ZZZZLjava/util/List;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "ZZZZ",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/checkoutflow/core/tip/TipScreen$Event;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "totalAmount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tipOptions"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->totalAmount:Lcom/squareup/protos/common/Money;

    iput-boolean p2, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->hasAutoGratuity:Z

    iput-boolean p3, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->isTablet10Inch:Z

    iput-boolean p4, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->isUsingCustomTip:Z

    iput-boolean p5, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->showLanguageSelection:Z

    iput-object p6, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->tipOptions:Ljava/util/List;

    iput-object p7, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/core/tip/TipScreen;Lcom/squareup/protos/common/Money;ZZZZLjava/util/List;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/checkoutflow/core/tip/TipScreen;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->totalAmount:Lcom/squareup/protos/common/Money;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-boolean p2, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->hasAutoGratuity:Z

    :cond_1
    move p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->isTablet10Inch:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->isUsingCustomTip:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->showLanguageSelection:Z

    :cond_4
    move v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->tipOptions:Ljava/util/List;

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-object p7, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    :cond_6
    move-object v4, p7

    move-object p2, p0

    move-object p3, p1

    move p4, p9

    move p5, v0

    move p6, v1

    move p7, v2

    move-object p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/checkoutflow/core/tip/TipScreen;->copy(Lcom/squareup/protos/common/Money;ZZZZLjava/util/List;Lkotlin/jvm/functions/Function1;)Lcom/squareup/checkoutflow/core/tip/TipScreen;

    move-result-object p0

    return-object p0
.end method

.method public static final getKEY()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    sget-object v0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->Companion:Lcom/squareup/checkoutflow/core/tip/TipScreen$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static final isTipScreen(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->Companion:Lcom/squareup/checkoutflow/core/tip/TipScreen$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/checkoutflow/core/tip/TipScreen$Companion;->isTipScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->totalAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->hasAutoGratuity:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->isTablet10Inch:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->isUsingCustomTip:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->showLanguageSelection:Z

    return v0
.end method

.method public final component6()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->tipOptions:Ljava/util/List;

    return-object v0
.end method

.method public final component7()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/checkoutflow/core/tip/TipScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/common/Money;ZZZZLjava/util/List;Lkotlin/jvm/functions/Function1;)Lcom/squareup/checkoutflow/core/tip/TipScreen;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "ZZZZ",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/checkoutflow/core/tip/TipScreen$Event;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/checkoutflow/core/tip/TipScreen;"
        }
    .end annotation

    const-string v0, "totalAmount"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tipOptions"

    move-object v7, p6

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkoutflow/core/tip/TipScreen;

    move-object v1, v0

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v8}, Lcom/squareup/checkoutflow/core/tip/TipScreen;-><init>(Lcom/squareup/protos/common/Money;ZZZZLjava/util/List;Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/core/tip/TipScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/core/tip/TipScreen;

    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->totalAmount:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/checkoutflow/core/tip/TipScreen;->totalAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->hasAutoGratuity:Z

    iget-boolean v1, p1, Lcom/squareup/checkoutflow/core/tip/TipScreen;->hasAutoGratuity:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->isTablet10Inch:Z

    iget-boolean v1, p1, Lcom/squareup/checkoutflow/core/tip/TipScreen;->isTablet10Inch:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->isUsingCustomTip:Z

    iget-boolean v1, p1, Lcom/squareup/checkoutflow/core/tip/TipScreen;->isUsingCustomTip:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->showLanguageSelection:Z

    iget-boolean v1, p1, Lcom/squareup/checkoutflow/core/tip/TipScreen;->showLanguageSelection:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->tipOptions:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/checkoutflow/core/tip/TipScreen;->tipOptions:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/checkoutflow/core/tip/TipScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getHasAutoGratuity()Z
    .locals 1

    .line 15
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->hasAutoGratuity:Z

    return v0
.end method

.method public final getOnEvent()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/checkoutflow/core/tip/TipScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getShowLanguageSelection()Z
    .locals 1

    .line 18
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->showLanguageSelection:Z

    return v0
.end method

.method public final getTipOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->tipOptions:Ljava/util/List;

    return-object v0
.end method

.method public final getTotalAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->totalAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->totalAmount:Lcom/squareup/protos/common/Money;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->hasAutoGratuity:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->isTablet10Inch:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->isUsingCustomTip:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->showLanguageSelection:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->tipOptions:Ljava/util/List;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_5
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public final isTablet10Inch()Z
    .locals 1

    .line 16
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->isTablet10Inch:Z

    return v0
.end method

.method public final isUsingCustomTip()Z
    .locals 1

    .line 17
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->isUsingCustomTip:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TipScreen(totalAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->totalAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", hasAutoGratuity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->hasAutoGratuity:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isTablet10Inch="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->isTablet10Inch:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isUsingCustomTip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->isUsingCustomTip:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showLanguageSelection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->showLanguageSelection:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", tipOptions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->tipOptions:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onEvent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/core/tip/TipScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
