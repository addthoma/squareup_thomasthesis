.class public final Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;
.super Ljava/lang/Object;
.source "InstallmentsSmsTask.kt"

# interfaces
.implements Lcom/squareup/queue/LoggedInTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/queue/LoggedInTask<",
        "Lcom/squareup/checkoutflow/installments/InstallmentsComponent;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0000\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0016\u0010\u0015\u001a\u00020\u00162\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0018H\u0016J\u0010\u0010\u001a\u001a\u00020\u00162\u0006\u0010\u001b\u001a\u00020\u0002H\u0016J\u0008\u0010\u001c\u001a\u00020\u001dH\u0016R\u001e\u0010\t\u001a\u00020\n8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000eR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u000f\u001a\u00020\u00108\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\"\u0004\u0008\u0013\u0010\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;",
        "Lcom/squareup/queue/LoggedInTask;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsComponent;",
        "phoneNumber",
        "",
        "token",
        "cart",
        "Lcom/squareup/protos/client/bills/Cart;",
        "(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;)V",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "getAnalytics",
        "()Lcom/squareup/analytics/Analytics;",
        "setAnalytics",
        "(Lcom/squareup/analytics/Analytics;)V",
        "installmentsService",
        "Lcom/squareup/checkoutflow/installments/InstallmentsService;",
        "getInstallmentsService",
        "()Lcom/squareup/checkoutflow/installments/InstallmentsService;",
        "setInstallmentsService",
        "(Lcom/squareup/checkoutflow/installments/InstallmentsService;)V",
        "execute",
        "",
        "callback",
        "Lcom/squareup/server/SquareCallback;",
        "Lcom/squareup/server/SimpleResponse;",
        "inject",
        "component",
        "secureCopyWithoutPIIForLogs",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public transient analytics:Lcom/squareup/analytics/Analytics;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final cart:Lcom/squareup/protos/client/bills/Cart;

.field public transient installmentsService:Lcom/squareup/checkoutflow/installments/InstallmentsService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final phoneNumber:Ljava/lang/String;

.field private final token:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;)V
    .locals 1

    const-string v0, "phoneNumber"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "token"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cart"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;->phoneNumber:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;->token:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-void
.end method


# virtual methods
.method public execute(Lcom/squareup/server/SquareCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    const-string v0, "callback"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;->installmentsService:Lcom/squareup/checkoutflow/installments/InstallmentsService;

    if-nez v0, :cond_0

    const-string v1, "installmentsService"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 25
    :cond_0
    new-instance v1, Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest$Builder;-><init>()V

    .line 26
    iget-object v2, p0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest$Builder;->phone_number(Ljava/lang/String;)Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest$Builder;

    move-result-object v1

    .line 27
    iget-object v2, p0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;->token:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest$Builder;->idempotence_token(Ljava/lang/String;)Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest$Builder;

    move-result-object v1

    .line 28
    iget-object v2, p0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest$Builder;

    move-result-object v1

    .line 29
    invoke-virtual {v1}, Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest$Builder;->build()Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest;

    move-result-object v1

    const-string v2, "SendInstallmentsSMSReque\u2026art)\n            .build()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-interface {v0, v1}, Lcom/squareup/checkoutflow/installments/InstallmentsService;->sendSms(Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 32
    new-instance v1, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask$execute$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask$execute$1;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;Lcom/squareup/server/SquareCallback;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public bridge synthetic execute(Ljava/lang/Object;)V
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/server/SquareCallback;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;->execute(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public final getAnalytics()Lcom/squareup/analytics/Analytics;
    .locals 2

    .line 21
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;->analytics:Lcom/squareup/analytics/Analytics;

    if-nez v0, :cond_0

    const-string v1, "analytics"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getInstallmentsService()Lcom/squareup/checkoutflow/installments/InstallmentsService;
    .locals 2

    .line 20
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;->installmentsService:Lcom/squareup/checkoutflow/installments/InstallmentsService;

    if-nez v0, :cond_0

    const-string v1, "installmentsService"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public inject(Lcom/squareup/checkoutflow/installments/InstallmentsComponent;)V
    .locals 1

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-interface {p1, p0}, Lcom/squareup/checkoutflow/installments/InstallmentsComponent;->inject(Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/checkoutflow/installments/InstallmentsComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;->inject(Lcom/squareup/checkoutflow/installments/InstallmentsComponent;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;->token:Ljava/lang/String;

    return-object v0
.end method

.method public final setAnalytics(Lcom/squareup/analytics/Analytics;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public final setInstallmentsService(Lcom/squareup/checkoutflow/installments/InstallmentsService;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;->installmentsService:Lcom/squareup/checkoutflow/installments/InstallmentsService;

    return-void
.end method
