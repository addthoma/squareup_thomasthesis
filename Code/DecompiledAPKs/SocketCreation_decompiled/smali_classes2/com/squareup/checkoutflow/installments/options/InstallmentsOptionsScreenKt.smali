.class public final Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsScreenKt;
.super Ljava/lang/Object;
.source "InstallmentsOptionsScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u001a\u001e\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0002`\u00042\u0006\u0010\u0005\u001a\u00020\u0002*\"\u0010\u0006\"\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001\u00a8\u0006\u0007"
    }
    d2 = {
        "createInstallmentsOptionsScreen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData;",
        "",
        "Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsScreen;",
        "screenData",
        "InstallmentsOptionsScreen",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final createInstallmentsOptionsScreen(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData;)Lcom/squareup/workflow/legacy/Screen;
    .locals 3

    const-string v0, "screenData"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    sget-object v1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData;->Companion:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData$Companion;

    invoke-virtual {v1}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object v0
.end method
