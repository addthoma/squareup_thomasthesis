.class public final Lcom/squareup/salesreport/RealSalesReportWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealSalesReportWorkflow.kt"

# interfaces
.implements Lcom/squareup/salesreport/SalesReportWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/RealSalesReportWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/salesreport/SalesReportProps;",
        "Lcom/squareup/salesreport/SalesReportState;",
        "Lcom/squareup/salesreport/SalesReportOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/salesreport/SalesReportWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSalesReportWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSalesReportWorkflow.kt\ncom/squareup/salesreport/RealSalesReportWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 6 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n*L\n1#1,526:1\n149#2,5:527\n85#3:532\n240#4:533\n276#5:534\n32#6,12:535\n*E\n*S KotlinDebug\n*F\n+ 1 RealSalesReportWorkflow.kt\ncom/squareup/salesreport/RealSalesReportWorkflow\n*L\n129#1,5:527\n165#1:532\n165#1:533\n165#1:534\n493#1,12:535\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00b8\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 <2\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002:\u0001<Bq\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u000c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0012\u0012\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0012\u0012\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0012\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u0012\u0006\u0010\u001e\u001a\u00020\u001f\u0012\u0006\u0010 \u001a\u00020!\u00a2\u0006\u0002\u0010\"J\u0008\u0010#\u001a\u00020$H\u0007J\u0012\u0010%\u001a\u0004\u0018\u00010&2\u0006\u0010\'\u001a\u00020(H\u0002J$\u0010)\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050*2\u0006\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020.H\u0002J4\u0010/\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050*2\u0006\u0010\'\u001a\u00020(2\u0006\u0010+\u001a\u0002002\u0006\u0010-\u001a\u00020\u00042\u0006\u00101\u001a\u000202H\u0002J\u001a\u00103\u001a\u00020\u00042\u0006\u00104\u001a\u00020\u00032\u0008\u00105\u001a\u0004\u0018\u000106H\u0016JN\u00107\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u00104\u001a\u00020\u00032\u0006\u0010-\u001a\u00020\u00042\u0012\u00108\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000509H\u0016J\u0010\u0010:\u001a\u0002062\u0006\u0010-\u001a\u00020\u0004H\u0016J\u000c\u0010;\u001a\u000202*\u00020\u0003H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006="
    }
    d2 = {
        "Lcom/squareup/salesreport/RealSalesReportWorkflow;",
        "Lcom/squareup/salesreport/SalesReportWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/salesreport/SalesReportProps;",
        "Lcom/squareup/salesreport/SalesReportState;",
        "Lcom/squareup/salesreport/SalesReportOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "initialSalesReportStateProvider",
        "Lcom/squareup/salesreport/InitialSalesReportStateProvider;",
        "basicSalesReportConfigRepository",
        "Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;",
        "newBasicReportConfigFactory",
        "Lcom/squareup/salesreport/basic/NewBasicReportConfigFactory;",
        "customizeReportWorkflowProvider",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/salesreport/customize/CustomizeReportWorkflow;",
        "exportReportWorkflowProvider",
        "Lcom/squareup/salesreport/export/ExportReportWorkflow;",
        "localeProvider",
        "Ljava/util/Locale;",
        "salesReportRepository",
        "Lcom/squareup/customreport/data/SalesReportRepository;",
        "salesReportAnalytics",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalytics;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "feeTutorial",
        "Lcom/squareup/feetutorial/FeeTutorial;",
        "(Lcom/squareup/salesreport/InitialSalesReportStateProvider;Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;Lcom/squareup/salesreport/basic/NewBasicReportConfigFactory;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/customreport/data/SalesReportRepository;Lcom/squareup/salesreport/analytics/SalesReportAnalytics;Lcom/squareup/time/CurrentTime;Lcom/squareup/settings/server/Features;Lcom/squareup/feetutorial/FeeTutorial;)V",
        "canShowFees",
        "",
        "earliestSelectableDate",
        "Lorg/threeten/bp/LocalDate;",
        "detailLevel",
        "Lcom/squareup/api/salesreport/DetailLevel;",
        "handleFeeDialogEvent",
        "Lcom/squareup/workflow/WorkflowAction;",
        "event",
        "Lcom/squareup/salesreport/FeeDetailDialogScreen$Event;",
        "state",
        "Lcom/squareup/salesreport/SalesReportState$ViewingFeesNote;",
        "handleSalesReportEvent",
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent;",
        "uiDisplayState",
        "Lcom/squareup/salesreport/UiDisplayState;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "toUiDisplayState",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/salesreport/RealSalesReportWorkflow$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MAXIMUM_NUMBERS_OF_DAY_BACK_BASIC_MODE:J = 0x5aL


# instance fields
.field private final basicSalesReportConfigRepository:Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;

.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final customizeReportWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/customize/CustomizeReportWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final exportReportWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/export/ExportReportWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private final feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;

.field private final initialSalesReportStateProvider:Lcom/squareup/salesreport/InitialSalesReportStateProvider;

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final newBasicReportConfigFactory:Lcom/squareup/salesreport/basic/NewBasicReportConfigFactory;

.field private final salesReportAnalytics:Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

.field private final salesReportRepository:Lcom/squareup/customreport/data/SalesReportRepository;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/salesreport/RealSalesReportWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/salesreport/RealSalesReportWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->Companion:Lcom/squareup/salesreport/RealSalesReportWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/salesreport/InitialSalesReportStateProvider;Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;Lcom/squareup/salesreport/basic/NewBasicReportConfigFactory;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/customreport/data/SalesReportRepository;Lcom/squareup/salesreport/analytics/SalesReportAnalytics;Lcom/squareup/time/CurrentTime;Lcom/squareup/settings/server/Features;Lcom/squareup/feetutorial/FeeTutorial;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/InitialSalesReportStateProvider;",
            "Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;",
            "Lcom/squareup/salesreport/basic/NewBasicReportConfigFactory;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/customize/CustomizeReportWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/export/ExportReportWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/customreport/data/SalesReportRepository;",
            "Lcom/squareup/salesreport/analytics/SalesReportAnalytics;",
            "Lcom/squareup/time/CurrentTime;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/feetutorial/FeeTutorial;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "initialSalesReportStateProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "basicSalesReportConfigRepository"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newBasicReportConfigFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customizeReportWorkflowProvider"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "exportReportWorkflowProvider"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "salesReportRepository"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "salesReportAnalytics"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "feeTutorial"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->initialSalesReportStateProvider:Lcom/squareup/salesreport/InitialSalesReportStateProvider;

    iput-object p2, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->basicSalesReportConfigRepository:Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;

    iput-object p3, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->newBasicReportConfigFactory:Lcom/squareup/salesreport/basic/NewBasicReportConfigFactory;

    iput-object p4, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->customizeReportWorkflowProvider:Ljavax/inject/Provider;

    iput-object p5, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->exportReportWorkflowProvider:Ljavax/inject/Provider;

    iput-object p6, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->localeProvider:Ljavax/inject/Provider;

    iput-object p7, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->salesReportRepository:Lcom/squareup/customreport/data/SalesReportRepository;

    iput-object p8, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    iput-object p9, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->currentTime:Lcom/squareup/time/CurrentTime;

    iput-object p10, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->features:Lcom/squareup/settings/server/Features;

    iput-object p11, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;

    return-void
.end method

.method public static final synthetic access$getBasicSalesReportConfigRepository$p(Lcom/squareup/salesreport/RealSalesReportWorkflow;)Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;
    .locals 0

    .line 92
    iget-object p0, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->basicSalesReportConfigRepository:Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;

    return-object p0
.end method

.method public static final synthetic access$getSalesReportAnalytics$p(Lcom/squareup/salesreport/RealSalesReportWorkflow;)Lcom/squareup/salesreport/analytics/SalesReportAnalytics;
    .locals 0

    .line 92
    iget-object p0, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    return-object p0
.end method

.method public static final synthetic access$handleFeeDialogEvent(Lcom/squareup/salesreport/RealSalesReportWorkflow;Lcom/squareup/salesreport/FeeDetailDialogScreen$Event;Lcom/squareup/salesreport/SalesReportState$ViewingFeesNote;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 92
    invoke-direct {p0, p1, p2}, Lcom/squareup/salesreport/RealSalesReportWorkflow;->handleFeeDialogEvent(Lcom/squareup/salesreport/FeeDetailDialogScreen$Event;Lcom/squareup/salesreport/SalesReportState$ViewingFeesNote;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleSalesReportEvent(Lcom/squareup/salesreport/RealSalesReportWorkflow;Lcom/squareup/api/salesreport/DetailLevel;Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent;Lcom/squareup/salesreport/SalesReportState;Lcom/squareup/salesreport/UiDisplayState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 92
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/salesreport/RealSalesReportWorkflow;->handleSalesReportEvent(Lcom/squareup/api/salesreport/DetailLevel;Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent;Lcom/squareup/salesreport/SalesReportState;Lcom/squareup/salesreport/UiDisplayState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final earliestSelectableDate(Lcom/squareup/api/salesreport/DetailLevel;)Lorg/threeten/bp/LocalDate;
    .locals 2

    .line 504
    sget-object v0, Lcom/squareup/api/salesreport/DetailLevel$Detailed;->INSTANCE:Lcom/squareup/api/salesreport/DetailLevel$Detailed;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 505
    :cond_0
    sget-object v0, Lcom/squareup/api/salesreport/DetailLevel$Basic;->INSTANCE:Lcom/squareup/api/salesreport/DetailLevel$Basic;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {p1}, Lcom/squareup/time/CurrentTime;->localDate()Lorg/threeten/bp/LocalDate;

    move-result-object p1

    const-wide/16 v0, 0x5a

    invoke-virtual {p1, v0, v1}, Lorg/threeten/bp/LocalDate;->minusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final handleFeeDialogEvent(Lcom/squareup/salesreport/FeeDetailDialogScreen$Event;Lcom/squareup/salesreport/SalesReportState$ViewingFeesNote;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/FeeDetailDialogScreen$Event;",
            "Lcom/squareup/salesreport/SalesReportState$ViewingFeesNote;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/salesreport/SalesReportState;",
            "Lcom/squareup/salesreport/SalesReportOutput;",
            ">;"
        }
    .end annotation

    .line 216
    instance-of v0, p1, Lcom/squareup/salesreport/FeeDetailDialogScreen$Event$Back;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/salesreport/RealSalesReportWorkflow$handleFeeDialogEvent$1;

    invoke-direct {p1, p2}, Lcom/squareup/salesreport/RealSalesReportWorkflow$handleFeeDialogEvent$1;-><init>(Lcom/squareup/salesreport/SalesReportState$ViewingFeesNote;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, p1, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 217
    :cond_0
    instance-of p1, p1, Lcom/squareup/salesreport/FeeDetailDialogScreen$Event$LearnMore;

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/salesreport/RealSalesReportWorkflow$handleFeeDialogEvent$2;

    invoke-direct {p1, p0, p2}, Lcom/squareup/salesreport/RealSalesReportWorkflow$handleFeeDialogEvent$2;-><init>(Lcom/squareup/salesreport/RealSalesReportWorkflow;Lcom/squareup/salesreport/SalesReportState$ViewingFeesNote;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, p1, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final handleSalesReportEvent(Lcom/squareup/api/salesreport/DetailLevel;Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent;Lcom/squareup/salesreport/SalesReportState;Lcom/squareup/salesreport/UiDisplayState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/salesreport/DetailLevel;",
            "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent;",
            "Lcom/squareup/salesreport/SalesReportState;",
            "Lcom/squareup/salesreport/UiDisplayState;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/salesreport/SalesReportState;",
            "Lcom/squareup/salesreport/SalesReportOutput;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    .line 233
    instance-of v3, v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$CustomizeReport;

    const/4 v4, 0x2

    const/4 v5, 0x0

    if-eqz v3, :cond_1

    .line 234
    sget-object v1, Lcom/squareup/api/salesreport/DetailLevel$Basic;->INSTANCE:Lcom/squareup/api/salesreport/DetailLevel$Basic;

    move-object/from16 v3, p1

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 235
    iget-object v1, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->newBasicReportConfigFactory:Lcom/squareup/salesreport/basic/NewBasicReportConfigFactory;

    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/squareup/salesreport/basic/NewBasicReportConfigFactory;->create(Lcom/squareup/customreport/data/ReportConfig;)Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v1

    goto :goto_0

    .line 237
    :cond_0
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v1

    .line 239
    :goto_0
    sget-object v3, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 240
    new-instance v6, Lcom/squareup/salesreport/SalesReportState$CustomizingReport;

    .line 243
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v7

    .line 240
    invoke-direct {v6, v2, v1, v7}, Lcom/squareup/salesreport/SalesReportState$CustomizingReport;-><init>(Lcom/squareup/salesreport/SalesReportState;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;)V

    .line 239
    invoke-static {v3, v6, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 247
    :cond_1
    instance-of v3, v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$BackPress;

    if-eqz v3, :cond_2

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v2, Lcom/squareup/salesreport/SalesReportOutput;->EXIT:Lcom/squareup/salesreport/SalesReportOutput;

    invoke-virtual {v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 248
    :cond_2
    instance-of v3, v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ExportReport;

    if-eqz v3, :cond_4

    .line 249
    instance-of v1, v2, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    if-eqz v1, :cond_3

    .line 250
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 251
    new-instance v3, Lcom/squareup/salesreport/SalesReportState$ExportingReport;

    .line 252
    move-object v6, v2

    check-cast v6, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    invoke-virtual {v6}, Lcom/squareup/salesreport/SalesReportState$ViewingReport;->getSalesReport()Lcom/squareup/customreport/data/WithSalesReport;

    move-result-object v6

    .line 253
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v7

    .line 254
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v2

    .line 251
    invoke-direct {v3, v6, v7, v2}, Lcom/squareup/salesreport/SalesReportState$ExportingReport;-><init>(Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;)V

    .line 250
    invoke-static {v1, v3, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 258
    :cond_3
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 261
    :cond_4
    instance-of v3, v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ReloadReport;

    if-eqz v3, :cond_5

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 262
    new-instance v3, Lcom/squareup/salesreport/SalesReportState$LoadingReport;

    .line 263
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v6

    .line 264
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v2

    .line 262
    invoke-direct {v3, v6, v2}, Lcom/squareup/salesreport/SalesReportState$LoadingReport;-><init>(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;)V

    .line 261
    invoke-static {v1, v3, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 267
    :cond_5
    instance-of v3, v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectPredefinedRange;

    const-string v6, "Check failed."

    const/4 v7, 0x1

    if-eqz v3, :cond_8

    .line 268
    invoke-virtual/range {p4 .. p4}, Lcom/squareup/salesreport/UiDisplayState;->getShowQuickDateSelections()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 269
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object v3

    check-cast v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectPredefinedRange;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectPredefinedRange;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;

    move-result-object v6

    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    xor-int/2addr v3, v7

    if-eqz v3, :cond_6

    .line 270
    iget-object v3, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectPredefinedRange;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;

    move-result-object v6

    invoke-interface {v3, v6}, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;->logPresetTimeSelection(Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;)V

    .line 271
    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectPredefinedRange;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;

    move-result-object v3

    iget-object v6, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    const-string v8, "localeProvider.get()"

    invoke-static {v6, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v6, Ljava/util/Locale;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectPredefinedRange;->getCurrentDate()Lorg/threeten/bp/LocalDate;

    move-result-object v9

    invoke-static {v3, v6, v9}, Lcom/squareup/customreport/data/util/RangeSelectionsKt;->endDate(Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;Ljava/util/Locale;Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object v12

    .line 272
    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectPredefinedRange;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;

    move-result-object v3

    iget-object v6, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v6, Ljava/util/Locale;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectPredefinedRange;->getCurrentDate()Lorg/threeten/bp/LocalDate;

    move-result-object v8

    invoke-static {v3, v6, v8}, Lcom/squareup/customreport/data/util/RangeSelectionsKt;->startDate(Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;Ljava/util/Locale;Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object v11

    .line 273
    sget-object v3, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 274
    new-instance v6, Lcom/squareup/salesreport/SalesReportState$LoadingReport;

    .line 275
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v10

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    .line 278
    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectPredefinedRange;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;

    move-result-object v8

    move-object/from16 v18, v8

    check-cast v18, Lcom/squareup/customreport/data/RangeSelection;

    .line 279
    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectPredefinedRange;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;

    move-result-object v1

    check-cast v1, Lcom/squareup/customreport/data/RangeSelection;

    invoke-static {v1}, Lcom/squareup/salesreport/util/RangeSelectionsKt;->getDefaultComparisonRange(Lcom/squareup/customreport/data/RangeSelection;)Lcom/squareup/customreport/data/ComparisonRange;

    move-result-object v19

    const/16 v20, 0x7c

    const/16 v21, 0x0

    .line 275
    invoke-static/range {v10 .. v21}, Lcom/squareup/customreport/data/ReportConfig;->copy$default(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;ZZLcom/squareup/customreport/data/EmployeeFiltersSelection;Lcom/squareup/customreport/data/RangeSelection;Lcom/squareup/customreport/data/ComparisonRange;ILjava/lang/Object;)Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v1

    .line 280
    invoke-static {v1, v7}, Lcom/squareup/customreport/data/util/ReportConfigsKt;->withAllDay(Lcom/squareup/customreport/data/ReportConfig;Z)Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v1

    .line 281
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v2

    .line 274
    invoke-direct {v6, v1, v2}, Lcom/squareup/salesreport/SalesReportState$LoadingReport;-><init>(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;)V

    .line 273
    invoke-static {v3, v6, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 286
    :cond_6
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 268
    :cond_7
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 289
    :cond_8
    instance-of v3, v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectComparisonRange;

    if-eqz v3, :cond_b

    .line 290
    invoke-virtual/range {p4 .. p4}, Lcom/squareup/salesreport/UiDisplayState;->getShowCompareTo()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 291
    instance-of v3, v2, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    if-eqz v3, :cond_9

    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/customreport/data/ReportConfig;->getComparisonRange()Lcom/squareup/customreport/data/ComparisonRange;

    move-result-object v3

    check-cast v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectComparisonRange;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectComparisonRange;->getComparisonRange()Lcom/squareup/customreport/data/ComparisonRange;

    move-result-object v6

    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    xor-int/2addr v3, v7

    if-eqz v3, :cond_9

    .line 292
    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectComparisonRange;->getComparisonRange()Lcom/squareup/customreport/data/ComparisonRange;

    move-result-object v15

    .line 293
    iget-object v1, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    invoke-interface {v1, v15}, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;->logComparisonRangeChanged(Lcom/squareup/customreport/data/ComparisonRange;)V

    .line 294
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 295
    new-instance v3, Lcom/squareup/salesreport/SalesReportState$LoadingReport;

    .line 296
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0xff

    const/16 v17, 0x0

    invoke-static/range {v6 .. v17}, Lcom/squareup/customreport/data/ReportConfig;->copy$default(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;ZZLcom/squareup/customreport/data/EmployeeFiltersSelection;Lcom/squareup/customreport/data/RangeSelection;Lcom/squareup/customreport/data/ComparisonRange;ILjava/lang/Object;)Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v6

    .line 297
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v2

    .line 295
    invoke-direct {v3, v6, v2}, Lcom/squareup/salesreport/SalesReportState$LoadingReport;-><init>(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;)V

    .line 294
    invoke-static {v1, v3, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 301
    :cond_9
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 290
    :cond_a
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 304
    :cond_b
    instance-of v3, v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ToggleOverviewDetails;

    if-eqz v3, :cond_e

    .line 305
    invoke-virtual/range {p4 .. p4}, Lcom/squareup/salesreport/UiDisplayState;->getShowSalesOverview()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 306
    instance-of v1, v2, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    if-eqz v1, :cond_c

    .line 307
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getTopSectionState()Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/salesreport/RealSalesReportWorkflowKt;->access$toggle(Lcom/squareup/salesreport/SalesReportState$TopSectionState;)Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    move-result-object v7

    .line 308
    iget-object v1, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    invoke-interface {v1, v7}, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;->logOverviewDetailsToggled(Lcom/squareup/salesreport/SalesReportState$TopSectionState;)V

    .line 309
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 310
    move-object v3, v2

    check-cast v3, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    const/16 v17, 0x0

    const/16 v18, 0x0

    .line 311
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v6

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xfe

    const/16 v16, 0x0

    invoke-static/range {v6 .. v16}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->copy$default(Lcom/squareup/salesreport/SalesReportState$UiSelectionState;Lcom/squareup/salesreport/SalesReportState$TopSectionState;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;Ljava/util/Set;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v11

    const/4 v12, 0x3

    move-object v8, v3

    move-object/from16 v9, v17

    move-object/from16 v10, v18

    .line 310
    invoke-static/range {v8 .. v13}, Lcom/squareup/salesreport/SalesReportState$ViewingReport;->copy$default(Lcom/squareup/salesreport/SalesReportState$ViewingReport;Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    move-result-object v2

    .line 309
    invoke-static {v1, v2, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 317
    :cond_c
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 305
    :cond_d
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 320
    :cond_e
    instance-of v3, v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectItemGrossCountTab;

    if-eqz v3, :cond_10

    .line 321
    instance-of v3, v2, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    if-eqz v3, :cond_f

    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getItemGrossCountSelection()Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    move-result-object v3

    check-cast v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectItemGrossCountTab;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectItemGrossCountTab;->getSelection()Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    move-result-object v6

    if-eq v3, v6, :cond_f

    .line 324
    iget-object v3, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectItemGrossCountTab;->getSelection()Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    move-result-object v6

    invoke-interface {v3, v6}, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;->logItemGrossCountToggled(Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;)V

    .line 325
    sget-object v3, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 326
    move-object v6, v2

    check-cast v6, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 327
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v9

    const/4 v10, 0x0

    .line 328
    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectItemGrossCountTab;->getSelection()Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0xfd

    const/16 v19, 0x0

    .line 327
    invoke-static/range {v9 .. v19}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->copy$default(Lcom/squareup/salesreport/SalesReportState$UiSelectionState;Lcom/squareup/salesreport/SalesReportState$TopSectionState;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;Ljava/util/Set;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v9

    const/4 v10, 0x3

    const/4 v11, 0x0

    .line 326
    invoke-static/range {v6 .. v11}, Lcom/squareup/salesreport/SalesReportState$ViewingReport;->copy$default(Lcom/squareup/salesreport/SalesReportState$ViewingReport;Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    move-result-object v1

    .line 325
    invoke-static {v3, v1, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 333
    :cond_f
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 335
    :cond_10
    instance-of v3, v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectCategoryAmountCountTab;

    if-eqz v3, :cond_12

    .line 336
    instance-of v3, v2, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    if-eqz v3, :cond_11

    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getCategoryGrossCountSelection()Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    move-result-object v3

    check-cast v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectCategoryAmountCountTab;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectCategoryAmountCountTab;->getSelection()Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    move-result-object v6

    if-eq v3, v6, :cond_11

    .line 339
    iget-object v3, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectCategoryAmountCountTab;->getSelection()Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    move-result-object v6

    invoke-interface {v3, v6}, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;->logCategoryGrossCountToggled(Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;)V

    .line 340
    sget-object v3, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 341
    move-object v6, v2

    check-cast v6, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 342
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 343
    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectCategoryAmountCountTab;->getSelection()Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0xfb

    const/16 v19, 0x0

    .line 342
    invoke-static/range {v9 .. v19}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->copy$default(Lcom/squareup/salesreport/SalesReportState$UiSelectionState;Lcom/squareup/salesreport/SalesReportState$TopSectionState;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;Ljava/util/Set;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v9

    const/4 v10, 0x3

    .line 341
    invoke-static/range {v6 .. v11}, Lcom/squareup/salesreport/SalesReportState$ViewingReport;->copy$default(Lcom/squareup/salesreport/SalesReportState$ViewingReport;Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    move-result-object v1

    .line 340
    invoke-static {v3, v1, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 348
    :cond_11
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 350
    :cond_12
    instance-of v3, v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectChartSalesType;

    if-eqz v3, :cond_14

    instance-of v3, v2, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    if-eqz v3, :cond_13

    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getChartSalesSelection()Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    move-result-object v3

    check-cast v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectChartSalesType;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectChartSalesType;->getSelection()Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    move-result-object v6

    if-eq v3, v6, :cond_13

    .line 353
    iget-object v3, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectChartSalesType;->getSelection()Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    move-result-object v6

    invoke-interface {v3, v6}, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;->logChartSalesSelectionToggled(Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;)V

    .line 354
    sget-object v3, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 355
    move-object v6, v2

    check-cast v6, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 356
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 357
    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectChartSalesType;->getSelection()Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0xf7

    const/16 v19, 0x0

    .line 356
    invoke-static/range {v9 .. v19}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->copy$default(Lcom/squareup/salesreport/SalesReportState$UiSelectionState;Lcom/squareup/salesreport/SalesReportState$TopSectionState;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;Ljava/util/Set;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v9

    const/4 v10, 0x3

    .line 355
    invoke-static/range {v6 .. v11}, Lcom/squareup/salesreport/SalesReportState$ViewingReport;->copy$default(Lcom/squareup/salesreport/SalesReportState$ViewingReport;Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    move-result-object v1

    .line 354
    invoke-static {v3, v1, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 362
    :cond_13
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 364
    :cond_14
    instance-of v3, v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ChangeCategoryViewCount;

    if-eqz v3, :cond_16

    instance-of v3, v2, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    if-eqz v3, :cond_15

    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getCategoryViewCount()Lcom/squareup/salesreport/SalesReportState$ViewCount;

    move-result-object v3

    check-cast v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ChangeCategoryViewCount;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ChangeCategoryViewCount;->getViewCount()Lcom/squareup/salesreport/SalesReportState$ViewCount;

    move-result-object v6

    if-eq v3, v6, :cond_15

    .line 367
    iget-object v3, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ChangeCategoryViewCount;->getViewCount()Lcom/squareup/salesreport/SalesReportState$ViewCount;

    move-result-object v6

    invoke-interface {v3, v6}, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;->logCategoryViewCountChanged(Lcom/squareup/salesreport/SalesReportState$ViewCount;)V

    .line 368
    sget-object v3, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 369
    move-object v6, v2

    check-cast v6, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 370
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ChangeCategoryViewCount;->getViewCount()Lcom/squareup/salesreport/SalesReportState$ViewCount;

    move-result-object v15

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0xdf

    const/16 v19, 0x0

    invoke-static/range {v9 .. v19}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->copy$default(Lcom/squareup/salesreport/SalesReportState$UiSelectionState;Lcom/squareup/salesreport/SalesReportState$TopSectionState;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;Ljava/util/Set;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v9

    const/4 v10, 0x3

    .line 369
    invoke-static/range {v6 .. v11}, Lcom/squareup/salesreport/SalesReportState$ViewingReport;->copy$default(Lcom/squareup/salesreport/SalesReportState$ViewingReport;Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    move-result-object v1

    .line 368
    invoke-static {v3, v1, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 374
    :cond_15
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 376
    :cond_16
    instance-of v3, v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ChangeItemViewCount;

    if-eqz v3, :cond_18

    instance-of v3, v2, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    if-eqz v3, :cond_17

    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getItemViewCount()Lcom/squareup/salesreport/SalesReportState$ViewCount;

    move-result-object v3

    check-cast v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ChangeItemViewCount;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ChangeItemViewCount;->getViewCount()Lcom/squareup/salesreport/SalesReportState$ViewCount;

    move-result-object v6

    if-eq v3, v6, :cond_17

    .line 379
    iget-object v3, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ChangeItemViewCount;->getViewCount()Lcom/squareup/salesreport/SalesReportState$ViewCount;

    move-result-object v6

    invoke-interface {v3, v6}, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;->logItemViewCountChanged(Lcom/squareup/salesreport/SalesReportState$ViewCount;)V

    .line 380
    sget-object v3, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 381
    move-object v6, v2

    check-cast v6, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 382
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    .line 383
    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ChangeItemViewCount;->getViewCount()Lcom/squareup/salesreport/SalesReportState$ViewCount;

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0xef

    const/16 v19, 0x0

    .line 382
    invoke-static/range {v9 .. v19}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->copy$default(Lcom/squareup/salesreport/SalesReportState$UiSelectionState;Lcom/squareup/salesreport/SalesReportState$TopSectionState;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;Ljava/util/Set;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v9

    const/4 v10, 0x3

    .line 381
    invoke-static/range {v6 .. v11}, Lcom/squareup/salesreport/SalesReportState$ViewingReport;->copy$default(Lcom/squareup/salesreport/SalesReportState$ViewingReport;Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    move-result-object v1

    .line 380
    invoke-static {v3, v1, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 388
    :cond_17
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 390
    :cond_18
    instance-of v3, v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ToggleShowingItemsForCategory;

    if-eqz v3, :cond_1b

    instance-of v3, v2, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    if-eqz v3, :cond_1a

    .line 392
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getCategoriesWithItemsShown()Ljava/util/Set;

    move-result-object v3

    check-cast v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ToggleShowingItemsForCategory;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ToggleShowingItemsForCategory;->getCategoryIndex()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 393
    iget-object v6, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ToggleShowingItemsForCategory;->getCategoryIndex()I

    move-result v7

    xor-int/lit8 v8, v3, 0x1

    invoke-interface {v6, v7, v8}, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;->logCategoryItemsShownToggled(IZ)V

    .line 394
    sget-object v6, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 395
    move-object v7, v2

    check-cast v7, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 396
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    if-eqz v3, :cond_19

    .line 398
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getCategoriesWithItemsShown()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ToggleShowingItemsForCategory;->getCategoryIndex()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2, v1}, Lkotlin/collections/SetsKt;->minus(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    goto :goto_1

    .line 400
    :cond_19
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getCategoriesWithItemsShown()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ToggleShowingItemsForCategory;->getCategoryIndex()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2, v1}, Lkotlin/collections/SetsKt;->plus(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    :goto_1
    move-object/from16 v17, v1

    const/16 v18, 0x0

    const/16 v19, 0xbf

    const/16 v20, 0x0

    .line 396
    invoke-static/range {v10 .. v20}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->copy$default(Lcom/squareup/salesreport/SalesReportState$UiSelectionState;Lcom/squareup/salesreport/SalesReportState$TopSectionState;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;Ljava/util/Set;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v10

    const/4 v11, 0x3

    const/4 v12, 0x0

    .line 395
    invoke-static/range {v7 .. v12}, Lcom/squareup/salesreport/SalesReportState$ViewingReport;->copy$default(Lcom/squareup/salesreport/SalesReportState$ViewingReport;Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    move-result-object v1

    .line 394
    invoke-static {v6, v1, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 406
    :cond_1a
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 408
    :cond_1b
    instance-of v3, v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ToggleShowingVariationsForItem;

    if-eqz v3, :cond_1e

    instance-of v3, v2, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    if-eqz v3, :cond_1d

    .line 410
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getItemsWithVariationsShown()Ljava/util/Set;

    move-result-object v3

    check-cast v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ToggleShowingVariationsForItem;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ToggleShowingVariationsForItem;->getItemIndex()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 411
    iget-object v6, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ToggleShowingVariationsForItem;->getItemIndex()I

    move-result v7

    xor-int/lit8 v8, v3, 0x1

    invoke-interface {v6, v7, v8}, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;->logItemVariationsShownToggled(IZ)V

    .line 412
    sget-object v6, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 413
    move-object v7, v2

    check-cast v7, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 414
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    if-eqz v3, :cond_1c

    .line 416
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getItemsWithVariationsShown()Ljava/util/Set;

    move-result-object v2

    .line 417
    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ToggleShowingVariationsForItem;->getItemIndex()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 416
    invoke-static {v2, v1}, Lkotlin/collections/SetsKt;->minus(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    goto :goto_2

    .line 420
    :cond_1c
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getItemsWithVariationsShown()Ljava/util/Set;

    move-result-object v2

    .line 421
    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ToggleShowingVariationsForItem;->getItemIndex()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 420
    invoke-static {v2, v1}, Lkotlin/collections/SetsKt;->plus(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    :goto_2
    move-object/from16 v18, v1

    const/16 v19, 0x7f

    const/16 v20, 0x0

    .line 414
    invoke-static/range {v10 .. v20}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->copy$default(Lcom/squareup/salesreport/SalesReportState$UiSelectionState;Lcom/squareup/salesreport/SalesReportState$TopSectionState;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;Ljava/util/Set;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v10

    const/4 v11, 0x3

    const/4 v12, 0x0

    .line 413
    invoke-static/range {v7 .. v12}, Lcom/squareup/salesreport/SalesReportState$ViewingReport;->copy$default(Lcom/squareup/salesreport/SalesReportState$ViewingReport;Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    move-result-object v1

    .line 412
    invoke-static {v6, v1, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 428
    :cond_1d
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 430
    :cond_1e
    instance-of v3, v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ExpandAllItems;

    const/4 v6, 0x0

    if-eqz v3, :cond_20

    instance-of v1, v2, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    if-eqz v1, :cond_1f

    .line 431
    iget-object v1, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    sget-object v3, Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Item;->INSTANCE:Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Item;

    check-cast v3, Lcom/squareup/salesreport/SalesReportState$ItemOrCategory;

    invoke-interface {v1, v3}, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;->logExpandAllDetails(Lcom/squareup/salesreport/SalesReportState$ItemOrCategory;)V

    .line 432
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 433
    move-object v7, v2

    check-cast v7, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 434
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    .line 435
    invoke-virtual {v7}, Lcom/squareup/salesreport/SalesReportState$ViewingReport;->getSalesReport()Lcom/squareup/customreport/data/WithSalesReport;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/salesreport/util/WithSalesReportsKt;->getItemCount(Lcom/squareup/customreport/data/WithSalesReport;)I

    move-result v2

    invoke-static {v6, v2}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v18

    const/16 v19, 0x7f

    const/16 v20, 0x0

    .line 434
    invoke-static/range {v10 .. v20}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->copy$default(Lcom/squareup/salesreport/SalesReportState$UiSelectionState;Lcom/squareup/salesreport/SalesReportState$TopSectionState;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;Ljava/util/Set;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v10

    const/4 v11, 0x3

    .line 433
    invoke-static/range {v7 .. v12}, Lcom/squareup/salesreport/SalesReportState$ViewingReport;->copy$default(Lcom/squareup/salesreport/SalesReportState$ViewingReport;Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    move-result-object v2

    .line 432
    invoke-static {v1, v2, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 440
    :cond_1f
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 442
    :cond_20
    instance-of v3, v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$CollapseAllItems;

    if-eqz v3, :cond_22

    instance-of v1, v2, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    if-eqz v1, :cond_21

    .line 443
    iget-object v1, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    sget-object v3, Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Item;->INSTANCE:Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Item;

    check-cast v3, Lcom/squareup/salesreport/SalesReportState$ItemOrCategory;

    invoke-interface {v1, v3}, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;->logCollapseAllDetails(Lcom/squareup/salesreport/SalesReportState$ItemOrCategory;)V

    .line 444
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 445
    move-object v6, v2

    check-cast v6, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 446
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    .line 447
    invoke-static {}, Lkotlin/collections/SetsKt;->emptySet()Ljava/util/Set;

    move-result-object v17

    const/16 v18, 0x7f

    const/16 v19, 0x0

    .line 446
    invoke-static/range {v9 .. v19}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->copy$default(Lcom/squareup/salesreport/SalesReportState$UiSelectionState;Lcom/squareup/salesreport/SalesReportState$TopSectionState;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;Ljava/util/Set;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v9

    const/4 v10, 0x3

    .line 445
    invoke-static/range {v6 .. v11}, Lcom/squareup/salesreport/SalesReportState$ViewingReport;->copy$default(Lcom/squareup/salesreport/SalesReportState$ViewingReport;Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    move-result-object v2

    .line 444
    invoke-static {v1, v2, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 452
    :cond_21
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_3

    .line 454
    :cond_22
    instance-of v3, v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ExpandAllCategories;

    if-eqz v3, :cond_24

    instance-of v1, v2, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    if-eqz v1, :cond_23

    .line 455
    iget-object v1, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    sget-object v3, Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Category;->INSTANCE:Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Category;

    check-cast v3, Lcom/squareup/salesreport/SalesReportState$ItemOrCategory;

    invoke-interface {v1, v3}, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;->logExpandAllDetails(Lcom/squareup/salesreport/SalesReportState$ItemOrCategory;)V

    .line 456
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 457
    move-object v7, v2

    check-cast v7, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 458
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    .line 459
    invoke-virtual {v7}, Lcom/squareup/salesreport/SalesReportState$ViewingReport;->getSalesReport()Lcom/squareup/customreport/data/WithSalesReport;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/salesreport/util/WithSalesReportsKt;->getCategoryCount(Lcom/squareup/customreport/data/WithSalesReport;)I

    move-result v2

    invoke-static {v6, v2}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v17

    const/16 v18, 0x0

    const/16 v19, 0xbf

    const/16 v20, 0x0

    .line 458
    invoke-static/range {v10 .. v20}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->copy$default(Lcom/squareup/salesreport/SalesReportState$UiSelectionState;Lcom/squareup/salesreport/SalesReportState$TopSectionState;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;Ljava/util/Set;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v10

    const/4 v11, 0x3

    .line 457
    invoke-static/range {v7 .. v12}, Lcom/squareup/salesreport/SalesReportState$ViewingReport;->copy$default(Lcom/squareup/salesreport/SalesReportState$ViewingReport;Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    move-result-object v2

    .line 456
    invoke-static {v1, v2, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_3

    .line 464
    :cond_23
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_3

    .line 466
    :cond_24
    instance-of v3, v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$CollapseAllCategories;

    if-eqz v3, :cond_26

    instance-of v1, v2, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    if-eqz v1, :cond_25

    .line 467
    iget-object v1, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    sget-object v3, Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Category;->INSTANCE:Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Category;

    check-cast v3, Lcom/squareup/salesreport/SalesReportState$ItemOrCategory;

    invoke-interface {v1, v3}, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;->logCollapseAllDetails(Lcom/squareup/salesreport/SalesReportState$ItemOrCategory;)V

    .line 468
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 469
    move-object v6, v2

    check-cast v6, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 470
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    .line 471
    invoke-static {}, Lkotlin/collections/SetsKt;->emptySet()Ljava/util/Set;

    move-result-object v16

    const/16 v17, 0x0

    const/16 v18, 0xbf

    const/16 v19, 0x0

    .line 470
    invoke-static/range {v9 .. v19}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->copy$default(Lcom/squareup/salesreport/SalesReportState$UiSelectionState;Lcom/squareup/salesreport/SalesReportState$TopSectionState;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;Ljava/util/Set;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v9

    const/4 v10, 0x3

    .line 469
    invoke-static/range {v6 .. v11}, Lcom/squareup/salesreport/SalesReportState$ViewingReport;->copy$default(Lcom/squareup/salesreport/SalesReportState$ViewingReport;Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    move-result-object v2

    .line 468
    invoke-static {v1, v2, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_3

    .line 476
    :cond_25
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_3

    .line 478
    :cond_26
    instance-of v1, v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ViewFeesNote;

    if-eqz v1, :cond_28

    instance-of v1, v2, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    if-eqz v1, :cond_27

    .line 479
    iget-object v1, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    invoke-interface {v1}, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;->logViewFeesNote()V

    .line 480
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 481
    new-instance v3, Lcom/squareup/salesreport/SalesReportState$ViewingFeesNote;

    check-cast v2, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    invoke-direct {v3, v2}, Lcom/squareup/salesreport/SalesReportState$ViewingFeesNote;-><init>(Lcom/squareup/salesreport/SalesReportState$ViewingReport;)V

    .line 480
    invoke-static {v1, v3, v5, v4, v5}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_3

    .line 484
    :cond_27
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    :goto_3
    return-object v1

    .line 478
    :cond_28
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method private final toUiDisplayState(Lcom/squareup/salesreport/SalesReportProps;)Lcom/squareup/salesreport/UiDisplayState;
    .locals 4

    .line 508
    new-instance v0, Lcom/squareup/salesreport/UiDisplayState;

    .line 509
    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportProps;->getDetailLevel()Lcom/squareup/api/salesreport/DetailLevel;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/salesreport/util/DetailLevelsKt;->isComparisonEnabled(Lcom/squareup/api/salesreport/DetailLevel;)Z

    move-result v1

    .line 510
    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportProps;->getDetailLevel()Lcom/squareup/api/salesreport/DetailLevel;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/salesreport/util/DetailLevelsKt;->isComparisonEnabled(Lcom/squareup/api/salesreport/DetailLevel;)Z

    move-result v2

    .line 511
    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportProps;->getDetailLevel()Lcom/squareup/api/salesreport/DetailLevel;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/salesreport/util/DetailLevelsKt;->isOverviewEnabled(Lcom/squareup/api/salesreport/DetailLevel;)Z

    move-result v3

    .line 512
    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportProps;->getDetailLevel()Lcom/squareup/api/salesreport/DetailLevel;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/salesreport/RealSalesReportWorkflow;->earliestSelectableDate(Lcom/squareup/api/salesreport/DetailLevel;)Lorg/threeten/bp/LocalDate;

    move-result-object p1

    .line 508
    invoke-direct {v0, v1, v2, v3, p1}, Lcom/squareup/salesreport/UiDisplayState;-><init>(ZZZLorg/threeten/bp/LocalDate;)V

    return-object v0
.end method


# virtual methods
.method public final canShowFees()Z
    .locals 2

    .line 499
    iget-object v0, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_CP_PRICING_CHANGE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;

    invoke-interface {v0}, Lcom/squareup/feetutorial/FeeTutorial;->getCanShow()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public initialState(Lcom/squareup/salesreport/SalesReportProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/salesreport/SalesReportState;
    .locals 3

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 535
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v2

    :goto_1
    if-eqz p2, :cond_3

    .line 540
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v2, "Parcel.obtain()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 541
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 542
    array-length v2, p2

    invoke-virtual {v0, p2, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 543
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 544
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p2, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 545
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 546
    :cond_3
    check-cast v2, Lcom/squareup/salesreport/SalesReportState;

    if-eqz v2, :cond_4

    goto :goto_2

    .line 494
    :cond_4
    iget-object p2, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->initialSalesReportStateProvider:Lcom/squareup/salesreport/InitialSalesReportStateProvider;

    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportProps;->getDetailLevel()Lcom/squareup/api/salesreport/DetailLevel;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/salesreport/InitialSalesReportStateProvider;->get(Lcom/squareup/api/salesreport/DetailLevel;)Lcom/squareup/salesreport/SalesReportState;

    move-result-object v2

    :goto_2
    return-object v2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 92
    check-cast p1, Lcom/squareup/salesreport/SalesReportProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/salesreport/RealSalesReportWorkflow;->initialState(Lcom/squareup/salesreport/SalesReportProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/salesreport/SalesReportState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 92
    check-cast p1, Lcom/squareup/salesreport/SalesReportProps;

    check-cast p2, Lcom/squareup/salesreport/SalesReportState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/salesreport/RealSalesReportWorkflow;->render(Lcom/squareup/salesreport/SalesReportProps;Lcom/squareup/salesreport/SalesReportState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/salesreport/SalesReportProps;Lcom/squareup/salesreport/SalesReportState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/SalesReportProps;",
            "Lcom/squareup/salesreport/SalesReportState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/salesreport/SalesReportState;",
            "-",
            "Lcom/squareup/salesreport/SalesReportOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move-object v8, p2

    move-object/from16 v9, p3

    const-string v2, "props"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "state"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "context"

    invoke-static {v9, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-direct {p0, p1}, Lcom/squareup/salesreport/RealSalesReportWorkflow;->toUiDisplayState(Lcom/squareup/salesreport/SalesReportProps;)Lcom/squareup/salesreport/UiDisplayState;

    move-result-object v10

    .line 114
    invoke-virtual {v10}, Lcom/squareup/salesreport/UiDisplayState;->getEarliestSelectableDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/squareup/salesreport/SalesReportState;->verifyState(Lorg/threeten/bp/LocalDate;)V

    .line 116
    new-instance v11, Lcom/squareup/salesreport/SalesReportScreen;

    .line 118
    new-instance v2, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$body$1;

    invoke-direct {v2, p0, p1, p2, v10}, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$body$1;-><init>(Lcom/squareup/salesreport/RealSalesReportWorkflow;Lcom/squareup/salesreport/SalesReportProps;Lcom/squareup/salesreport/SalesReportState;Lcom/squareup/salesreport/UiDisplayState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {v9, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v5

    .line 126
    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportProps;->getDetailLevel()Lcom/squareup/api/salesreport/DetailLevel;

    move-result-object v2

    sget-object v3, Lcom/squareup/api/salesreport/DetailLevel$Detailed;->INSTANCE:Lcom/squareup/api/salesreport/DetailLevel$Detailed;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    .line 127
    invoke-virtual {p0}, Lcom/squareup/salesreport/RealSalesReportWorkflow;->canShowFees()Z

    move-result v7

    move-object v2, v11

    move-object v3, p2

    move-object v4, v10

    .line 116
    invoke-direct/range {v2 .. v7}, Lcom/squareup/salesreport/SalesReportScreen;-><init>(Lcom/squareup/salesreport/SalesReportState;Lcom/squareup/salesreport/UiDisplayState;Lkotlin/jvm/functions/Function1;ZZ)V

    check-cast v11, Lcom/squareup/workflow/legacy/V2Screen;

    .line 528
    new-instance v12, Lcom/squareup/workflow/legacy/Screen;

    .line 529
    const-class v2, Lcom/squareup/salesreport/SalesReportScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, ""

    invoke-static {v2, v3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 530
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 528
    invoke-direct {v12, v2, v11, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 132
    instance-of v2, v8, Lcom/squareup/salesreport/SalesReportState$CustomizingReport;

    if-eqz v2, :cond_0

    .line 135
    iget-object v2, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->customizeReportWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    const-string v3, "customizeReportWorkflowProvider.get()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 136
    new-instance v3, Lcom/squareup/salesreport/customize/CustomizeReportInput;

    invoke-virtual {p2}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v4

    invoke-direct {v3, v4, v10}, Lcom/squareup/salesreport/customize/CustomizeReportInput;-><init>(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/UiDisplayState;)V

    const/4 v4, 0x0

    .line 137
    new-instance v5, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$customizeReportCard$1;

    invoke-direct {v5, p0, p1, p2}, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$customizeReportCard$1;-><init>(Lcom/squareup/salesreport/RealSalesReportWorkflow;Lcom/squareup/salesreport/SalesReportProps;Lcom/squareup/salesreport/SalesReportState;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object/from16 v1, p3

    .line 134
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 133
    check-cast v1, Ljava/util/Map;

    .line 156
    sget-object v2, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    invoke-virtual {v2, v12}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    return-object v1

    .line 158
    :cond_0
    instance-of v1, v8, Lcom/squareup/salesreport/SalesReportState$LoadingReport;

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    instance-of v1, v8, Lcom/squareup/salesreport/SalesReportState$InitialState;

    if-eqz v1, :cond_3

    .line 159
    :goto_0
    instance-of v1, v8, Lcom/squareup/salesreport/SalesReportState$InitialState;

    if-eqz v1, :cond_2

    .line 160
    iget-object v1, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    invoke-interface {v1}, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;->logInitialReportLoad()V

    .line 163
    :cond_2
    iget-object v1, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->salesReportRepository:Lcom/squareup/customreport/data/SalesReportRepository;

    .line 164
    invoke-virtual {p2}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/customreport/data/SalesReportRepository;->salesReport(Lcom/squareup/customreport/data/ReportConfig;)Lio/reactivex/Single;

    move-result-object v1

    .line 532
    sget-object v2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v2, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$$inlined$asWorker$1;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 533
    invoke-static {v2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v1

    .line 534
    const-class v2, Lcom/squareup/customreport/data/SalesReportRepository$Result;

    sget-object v3, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v4, Lcom/squareup/customreport/data/SalesReport;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v2

    new-instance v3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v3, v2, v1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v3, Lcom/squareup/workflow/Worker;

    .line 166
    invoke-static {}, Lorg/threeten/bp/LocalDateTime;->now()Lorg/threeten/bp/LocalDateTime;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDateTime;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "LocalDateTime.now().toString()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    new-instance v2, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$1;

    invoke-direct {v2, p2}, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$1;-><init>(Lcom/squareup/salesreport/SalesReportState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 162
    invoke-interface {v9, v3, v1, v2}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 188
    sget-object v1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    invoke-virtual {v1, v12}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_2

    .line 190
    :cond_3
    instance-of v1, v8, Lcom/squareup/salesreport/SalesReportState$EmptyReport;

    if-eqz v1, :cond_4

    goto :goto_1

    :cond_4
    instance-of v1, v8, Lcom/squareup/salesreport/SalesReportState$NetworkFailure;

    if-eqz v1, :cond_5

    goto :goto_1

    :cond_5
    instance-of v1, v8, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    if-eqz v1, :cond_6

    :goto_1
    sget-object v1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    invoke-virtual {v1, v12}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v1

    goto :goto_2

    .line 191
    :cond_6
    instance-of v1, v8, Lcom/squareup/salesreport/SalesReportState$ViewingFeesNote;

    if-eqz v1, :cond_7

    sget-object v1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 193
    new-instance v2, Lcom/squareup/salesreport/FeeDetailDialogScreen;

    .line 194
    new-instance v5, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$2;

    invoke-direct {v5, p0, p2}, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$2;-><init>(Lcom/squareup/salesreport/RealSalesReportWorkflow;Lcom/squareup/salesreport/SalesReportState;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    invoke-interface {v9, v5}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v5

    .line 193
    invoke-direct {v2, v5}, Lcom/squareup/salesreport/FeeDetailDialogScreen;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 195
    invoke-virtual {v2}, Lcom/squareup/salesreport/FeeDetailDialogScreen;->toLegacyScreen()Lcom/squareup/workflow/legacy/Screen;

    move-result-object v5

    const/4 v6, 0x6

    const/4 v7, 0x0

    move-object v2, v12

    .line 191
    invoke-static/range {v1 .. v7}, Lcom/squareup/container/PosLayering$Companion;->dialogStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    goto :goto_2

    .line 197
    :cond_7
    instance-of v1, v8, Lcom/squareup/salesreport/SalesReportState$ExportingReport;

    if-eqz v1, :cond_8

    .line 198
    iget-object v1, v0, Lcom/squareup/salesreport/RealSalesReportWorkflow;->exportReportWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    const-string v2, "exportReportWorkflowProvider.get()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, v1

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 199
    new-instance v3, Lcom/squareup/salesreport/export/ExportReportProps;

    move-object v1, v8

    check-cast v1, Lcom/squareup/salesreport/SalesReportState$ExportingReport;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$ExportingReport;->getSalesReport()Lcom/squareup/customreport/data/WithSalesReport;

    move-result-object v1

    invoke-virtual {p2}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Lcom/squareup/salesreport/export/ExportReportProps;-><init>(Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;)V

    const/4 v4, 0x0

    .line 200
    new-instance v1, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$3;

    invoke-direct {v1, p2}, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$3;-><init>(Lcom/squareup/salesreport/SalesReportState;)V

    move-object v5, v1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object/from16 v1, p3

    .line 197
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 208
    sget-object v2, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    invoke-virtual {v2, v12}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    :goto_2
    return-object v1

    :cond_8
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public snapshotState(Lcom/squareup/salesreport/SalesReportState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 496
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 92
    check-cast p1, Lcom/squareup/salesreport/SalesReportState;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/RealSalesReportWorkflow;->snapshotState(Lcom/squareup/salesreport/SalesReportState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
