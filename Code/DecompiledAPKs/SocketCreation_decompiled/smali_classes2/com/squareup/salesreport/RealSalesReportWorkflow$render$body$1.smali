.class final Lcom/squareup/salesreport/RealSalesReportWorkflow$render$body$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSalesReportWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/RealSalesReportWorkflow;->render(Lcom/squareup/salesreport/SalesReportProps;Lcom/squareup/salesreport/SalesReportState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/salesreport/SalesReportState;",
        "+",
        "Lcom/squareup/salesreport/SalesReportOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/salesreport/SalesReportState;",
        "Lcom/squareup/salesreport/SalesReportOutput;",
        "it",
        "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/salesreport/SalesReportProps;

.field final synthetic $state:Lcom/squareup/salesreport/SalesReportState;

.field final synthetic $uiDisplayState:Lcom/squareup/salesreport/UiDisplayState;

.field final synthetic this$0:Lcom/squareup/salesreport/RealSalesReportWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/RealSalesReportWorkflow;Lcom/squareup/salesreport/SalesReportProps;Lcom/squareup/salesreport/SalesReportState;Lcom/squareup/salesreport/UiDisplayState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$body$1;->this$0:Lcom/squareup/salesreport/RealSalesReportWorkflow;

    iput-object p2, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$body$1;->$props:Lcom/squareup/salesreport/SalesReportProps;

    iput-object p3, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$body$1;->$state:Lcom/squareup/salesreport/SalesReportState;

    iput-object p4, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$body$1;->$uiDisplayState:Lcom/squareup/salesreport/UiDisplayState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent;)Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/salesreport/SalesReportState;",
            "Lcom/squareup/salesreport/SalesReportOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$body$1;->this$0:Lcom/squareup/salesreport/RealSalesReportWorkflow;

    .line 120
    iget-object v1, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$body$1;->$props:Lcom/squareup/salesreport/SalesReportProps;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportProps;->getDetailLevel()Lcom/squareup/api/salesreport/DetailLevel;

    move-result-object v1

    .line 122
    iget-object v2, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$body$1;->$state:Lcom/squareup/salesreport/SalesReportState;

    .line 123
    iget-object v3, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$body$1;->$uiDisplayState:Lcom/squareup/salesreport/UiDisplayState;

    .line 119
    invoke-static {v0, v1, p1, v2, v3}, Lcom/squareup/salesreport/RealSalesReportWorkflow;->access$handleSalesReportEvent(Lcom/squareup/salesreport/RealSalesReportWorkflow;Lcom/squareup/api/salesreport/DetailLevel;Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent;Lcom/squareup/salesreport/SalesReportState;Lcom/squareup/salesreport/UiDisplayState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 92
    check-cast p1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$body$1;->invoke(Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
