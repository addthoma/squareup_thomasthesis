.class public final Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;
.super Ljava/lang/Object;
.source "SalesReportCoordinator_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/salesreport/SalesReportCoordinator$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final arg10Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg11Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg12Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg13Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final arg14Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg15Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private final arg16Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg17Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final arg18Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg19Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg20Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/Current24HourClockMode;",
            ">;"
        }
    .end annotation
.end field

.field private final arg21Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/util/PercentageChangeFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg8Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg9Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/util/PercentageChangeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/Current24HourClockMode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 88
    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg0Provider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 89
    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg1Provider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 90
    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg2Provider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 91
    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg3Provider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 92
    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg4Provider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 93
    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg5Provider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 94
    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg6Provider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 95
    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg7Provider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 96
    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg8Provider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 97
    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg9Provider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 98
    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg10Provider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 99
    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg11Provider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 100
    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg12Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 101
    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg13Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 102
    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg14Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 103
    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg15Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 104
    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg16Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 105
    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg17Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 106
    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg18Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 107
    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg19Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 108
    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg20Provider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 109
    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg21Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/util/PercentageChangeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/Current24HourClockMode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;",
            ">;)",
            "Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    .line 130
    new-instance v23, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;

    move-object/from16 v0, v23

    invoke-direct/range {v0 .. v22}, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v23
.end method

.method public static newInstance(Lcom/squareup/util/Device;Lio/reactivex/Scheduler;Lcom/squareup/salesreport/util/PercentageChangeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lcom/squareup/time/CurrentTime;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Landroid/content/res/Resources;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/time/Current24HourClockMode;Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;)Lcom/squareup/salesreport/SalesReportCoordinator$Factory;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Device;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/salesreport/util/PercentageChangeFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            "Lcom/squareup/time/CurrentTime;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            "Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/util/Res;",
            "Landroid/content/res/Resources;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;",
            "Lcom/squareup/settings/server/Features;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/time/Current24HourClockMode;",
            "Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;",
            ")",
            "Lcom/squareup/salesreport/SalesReportCoordinator$Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    .line 140
    new-instance v23, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;

    move-object/from16 v0, v23

    invoke-direct/range {v0 .. v22}, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;-><init>(Lcom/squareup/util/Device;Lio/reactivex/Scheduler;Lcom/squareup/salesreport/util/PercentageChangeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lcom/squareup/time/CurrentTime;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Landroid/content/res/Resources;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/time/Current24HourClockMode;Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;)V

    return-object v23
.end method


# virtual methods
.method public get()Lcom/squareup/salesreport/SalesReportCoordinator$Factory;
    .locals 24

    move-object/from16 v0, p0

    .line 114
    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lio/reactivex/Scheduler;

    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/salesreport/util/PercentageChangeFormatter;

    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg8Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/salesreport/util/LocalTimeFormatter;

    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg9Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/time/CurrentTime;

    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg10Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/recycler/RecyclerFactory;

    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg11Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg12Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg13Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg14Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg15Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Landroid/content/res/Resources;

    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg16Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg17Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;

    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg18Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg19Provider:Ljavax/inject/Provider;

    move-object/from16 v21, v1

    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg20Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/time/Current24HourClockMode;

    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->arg21Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;

    invoke-static/range {v2 .. v23}, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->newInstance(Lcom/squareup/util/Device;Lio/reactivex/Scheduler;Lcom/squareup/salesreport/util/PercentageChangeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lcom/squareup/time/CurrentTime;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Landroid/content/res/Resources;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/time/Current24HourClockMode;Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;)Lcom/squareup/salesreport/SalesReportCoordinator$Factory;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportCoordinator_Factory_Factory;->get()Lcom/squareup/salesreport/SalesReportCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method
