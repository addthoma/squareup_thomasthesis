.class public final Lcom/squareup/salesreport/SalesReportState$UiSelectionState;
.super Ljava/lang/Object;
.source "SalesReportState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/SalesReportState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UiSelectionState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/SalesReportState$UiSelectionState$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\"\n\u0002\u0010\u0008\n\u0002\u0008\u001a\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001Ba\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008\u0012\u0008\u0008\u0002\u0010\t\u001a\u00020\n\u0012\u0008\u0008\u0002\u0010\u000b\u001a\u00020\n\u0012\u000e\u0008\u0002\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u0012\u000e\u0008\u0002\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0002\u0010\u0010J\t\u0010\u001e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0005H\u00c6\u0003J\t\u0010 \u001a\u00020\u0005H\u00c6\u0003J\t\u0010!\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\"\u001a\u00020\nH\u00c6\u0003J\t\u0010#\u001a\u00020\nH\u00c6\u0003J\u000f\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rH\u00c6\u0003J\u000f\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rH\u00c6\u0003Je\u0010&\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\n2\u000e\u0008\u0002\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r2\u000e\u0008\u0002\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rH\u00c6\u0001J\t\u0010\'\u001a\u00020\u000eH\u00d6\u0001J\u0013\u0010(\u001a\u00020)2\u0008\u0010*\u001a\u0004\u0018\u00010+H\u00d6\u0003J\t\u0010,\u001a\u00020\u000eH\u00d6\u0001J\t\u0010-\u001a\u00020.H\u00d6\u0001J\u0019\u0010/\u001a\u0002002\u0006\u00101\u001a\u0002022\u0006\u00103\u001a\u00020\u000eH\u00d6\u0001R\u0017\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u000b\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0014R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0016R\u0017\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001d\u00a8\u00064"
    }
    d2 = {
        "Lcom/squareup/salesreport/SalesReportState$UiSelectionState;",
        "Landroid/os/Parcelable;",
        "topSectionState",
        "Lcom/squareup/salesreport/SalesReportState$TopSectionState;",
        "itemGrossCountSelection",
        "Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;",
        "categoryGrossCountSelection",
        "chartSalesSelection",
        "Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;",
        "itemViewCount",
        "Lcom/squareup/salesreport/SalesReportState$ViewCount;",
        "categoryViewCount",
        "categoriesWithItemsShown",
        "",
        "",
        "itemsWithVariationsShown",
        "(Lcom/squareup/salesreport/SalesReportState$TopSectionState;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;Ljava/util/Set;)V",
        "getCategoriesWithItemsShown",
        "()Ljava/util/Set;",
        "getCategoryGrossCountSelection",
        "()Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;",
        "getCategoryViewCount",
        "()Lcom/squareup/salesreport/SalesReportState$ViewCount;",
        "getChartSalesSelection",
        "()Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;",
        "getItemGrossCountSelection",
        "getItemViewCount",
        "getItemsWithVariationsShown",
        "getTopSectionState",
        "()Lcom/squareup/salesreport/SalesReportState$TopSectionState;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "copy",
        "describeContents",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final categoriesWithItemsShown:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final categoryGrossCountSelection:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

.field private final categoryViewCount:Lcom/squareup/salesreport/SalesReportState$ViewCount;

.field private final chartSalesSelection:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

.field private final itemGrossCountSelection:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

.field private final itemViewCount:Lcom/squareup/salesreport/SalesReportState$ViewCount;

.field private final itemsWithVariationsShown:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final topSectionState:Lcom/squareup/salesreport/SalesReportState$TopSectionState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState$Creator;

    invoke-direct {v0}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState$Creator;-><init>()V

    sput-object v0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 11

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xff

    const/4 v10, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;-><init>(Lcom/squareup/salesreport/SalesReportState$TopSectionState;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;Ljava/util/Set;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/salesreport/SalesReportState$TopSectionState;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/SalesReportState$TopSectionState;",
            "Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;",
            "Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;",
            "Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;",
            "Lcom/squareup/salesreport/SalesReportState$ViewCount;",
            "Lcom/squareup/salesreport/SalesReportState$ViewCount;",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    const-string v0, "topSectionState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemGrossCountSelection"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "categoryGrossCountSelection"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chartSalesSelection"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemViewCount"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "categoryViewCount"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "categoriesWithItemsShown"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemsWithVariationsShown"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->topSectionState:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    iput-object p2, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemGrossCountSelection:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    iput-object p3, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoryGrossCountSelection:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    iput-object p4, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->chartSalesSelection:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    iput-object p5, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemViewCount:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    iput-object p6, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoryViewCount:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    iput-object p7, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoriesWithItemsShown:Ljava/util/Set;

    iput-object p8, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemsWithVariationsShown:Ljava/util/Set;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/salesreport/SalesReportState$TopSectionState;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;Ljava/util/Set;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 8

    move/from16 v0, p9

    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_0

    .line 141
    sget-object v1, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->SALES_OVERVIEW:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    goto :goto_0

    :cond_0
    move-object v1, p1

    :goto_0
    and-int/lit8 v2, v0, 0x2

    if-eqz v2, :cond_1

    .line 142
    sget-object v2, Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;->GROSS:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    goto :goto_1

    :cond_1
    move-object v2, p2

    :goto_1
    and-int/lit8 v3, v0, 0x4

    if-eqz v3, :cond_2

    .line 143
    sget-object v3, Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;->GROSS:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    goto :goto_2

    :cond_2
    move-object v3, p3

    :goto_2
    and-int/lit8 v4, v0, 0x8

    if-eqz v4, :cond_3

    .line 144
    sget-object v4, Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;->GROSS_SALES:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    goto :goto_3

    :cond_3
    move-object v4, p4

    :goto_3
    and-int/lit8 v5, v0, 0x10

    if-eqz v5, :cond_4

    .line 145
    sget-object v5, Lcom/squareup/salesreport/SalesReportState$ViewCount;->VIEW_COUNT_FIVE:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    goto :goto_4

    :cond_4
    move-object v5, p5

    :goto_4
    and-int/lit8 v6, v0, 0x20

    if-eqz v6, :cond_5

    .line 146
    sget-object v6, Lcom/squareup/salesreport/SalesReportState$ViewCount;->VIEW_COUNT_FIVE:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    goto :goto_5

    :cond_5
    move-object v6, p6

    :goto_5
    and-int/lit8 v7, v0, 0x40

    if-eqz v7, :cond_6

    .line 147
    invoke-static {}, Lkotlin/collections/SetsKt;->emptySet()Ljava/util/Set;

    move-result-object v7

    goto :goto_6

    :cond_6
    move-object v7, p7

    :goto_6
    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_7

    .line 148
    invoke-static {}, Lkotlin/collections/SetsKt;->emptySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_7

    :cond_7
    move-object/from16 v0, p8

    :goto_7
    move-object p1, p0

    move-object p2, v1

    move-object p3, v2

    move-object p4, v3

    move-object p5, v4

    move-object p6, v5

    move-object p7, v6

    move-object/from16 p8, v7

    move-object/from16 p9, v0

    invoke-direct/range {p1 .. p9}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;-><init>(Lcom/squareup/salesreport/SalesReportState$TopSectionState;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;Ljava/util/Set;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/salesreport/SalesReportState$UiSelectionState;Lcom/squareup/salesreport/SalesReportState$TopSectionState;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;Ljava/util/Set;ILjava/lang/Object;)Lcom/squareup/salesreport/SalesReportState$UiSelectionState;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->topSectionState:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemGrossCountSelection:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoryGrossCountSelection:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->chartSalesSelection:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemViewCount:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoryViewCount:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    goto :goto_5

    :cond_5
    move-object v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoriesWithItemsShown:Ljava/util/Set;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemsWithVariationsShown:Ljava/util/Set;

    goto :goto_7

    :cond_7
    move-object/from16 v1, p8

    :goto_7
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move-object p5, v6

    move-object p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->copy(Lcom/squareup/salesreport/SalesReportState$TopSectionState;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;Ljava/util/Set;)Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/squareup/salesreport/SalesReportState$TopSectionState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->topSectionState:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    return-object v0
.end method

.method public final component2()Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemGrossCountSelection:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    return-object v0
.end method

.method public final component3()Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoryGrossCountSelection:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    return-object v0
.end method

.method public final component4()Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->chartSalesSelection:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    return-object v0
.end method

.method public final component5()Lcom/squareup/salesreport/SalesReportState$ViewCount;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemViewCount:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    return-object v0
.end method

.method public final component6()Lcom/squareup/salesreport/SalesReportState$ViewCount;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoryViewCount:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    return-object v0
.end method

.method public final component7()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoriesWithItemsShown:Ljava/util/Set;

    return-object v0
.end method

.method public final component8()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemsWithVariationsShown:Ljava/util/Set;

    return-object v0
.end method

.method public final copy(Lcom/squareup/salesreport/SalesReportState$TopSectionState;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;Ljava/util/Set;)Lcom/squareup/salesreport/SalesReportState$UiSelectionState;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/SalesReportState$TopSectionState;",
            "Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;",
            "Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;",
            "Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;",
            "Lcom/squareup/salesreport/SalesReportState$ViewCount;",
            "Lcom/squareup/salesreport/SalesReportState$ViewCount;",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/squareup/salesreport/SalesReportState$UiSelectionState;"
        }
    .end annotation

    const-string v0, "topSectionState"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemGrossCountSelection"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "categoryGrossCountSelection"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chartSalesSelection"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemViewCount"

    move-object v6, p5

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "categoryViewCount"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "categoriesWithItemsShown"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemsWithVariationsShown"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;-><init>(Lcom/squareup/salesreport/SalesReportState$TopSectionState;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;Ljava/util/Set;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->topSectionState:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    iget-object v1, p1, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->topSectionState:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemGrossCountSelection:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    iget-object v1, p1, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemGrossCountSelection:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoryGrossCountSelection:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    iget-object v1, p1, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoryGrossCountSelection:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->chartSalesSelection:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    iget-object v1, p1, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->chartSalesSelection:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemViewCount:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    iget-object v1, p1, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemViewCount:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoryViewCount:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    iget-object v1, p1, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoryViewCount:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoriesWithItemsShown:Ljava/util/Set;

    iget-object v1, p1, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoriesWithItemsShown:Ljava/util/Set;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemsWithVariationsShown:Ljava/util/Set;

    iget-object p1, p1, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemsWithVariationsShown:Ljava/util/Set;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCategoriesWithItemsShown()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 147
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoriesWithItemsShown:Ljava/util/Set;

    return-object v0
.end method

.method public final getCategoryGrossCountSelection()Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoryGrossCountSelection:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    return-object v0
.end method

.method public final getCategoryViewCount()Lcom/squareup/salesreport/SalesReportState$ViewCount;
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoryViewCount:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    return-object v0
.end method

.method public final getChartSalesSelection()Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->chartSalesSelection:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    return-object v0
.end method

.method public final getItemGrossCountSelection()Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemGrossCountSelection:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    return-object v0
.end method

.method public final getItemViewCount()Lcom/squareup/salesreport/SalesReportState$ViewCount;
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemViewCount:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    return-object v0
.end method

.method public final getItemsWithVariationsShown()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 148
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemsWithVariationsShown:Ljava/util/Set;

    return-object v0
.end method

.method public final getTopSectionState()Lcom/squareup/salesreport/SalesReportState$TopSectionState;
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->topSectionState:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->topSectionState:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemGrossCountSelection:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoryGrossCountSelection:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->chartSalesSelection:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemViewCount:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoryViewCount:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoriesWithItemsShown:Ljava/util/Set;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemsWithVariationsShown:Ljava/util/Set;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_7
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UiSelectionState(topSectionState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->topSectionState:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", itemGrossCountSelection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemGrossCountSelection:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", categoryGrossCountSelection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoryGrossCountSelection:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", chartSalesSelection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->chartSalesSelection:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", itemViewCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemViewCount:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", categoryViewCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoryViewCount:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", categoriesWithItemsShown="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoriesWithItemsShown:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", itemsWithVariationsShown="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemsWithVariationsShown:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->topSectionState:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemGrossCountSelection:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoryGrossCountSelection:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->chartSalesSelection:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemViewCount:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoryViewCount:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->categoriesWithItemsShown:Ljava/util/Set;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->itemsWithVariationsShown:Ljava/util/Set;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1

    :cond_1
    return-void
.end method
