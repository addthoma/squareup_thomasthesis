.class final Lcom/squareup/salesreport/SalesReportCoordinator$onScreen$3;
.super Lkotlin/jvm/internal/Lambda;
.source "SalesReportCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/SalesReportCoordinator;->onScreen(Lcom/squareup/salesreport/SalesReportScreen;ZZLandroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/salesreport/SalesReportScreen;

.field final synthetic this$0:Lcom/squareup/salesreport/SalesReportCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/SalesReportCoordinator;Lcom/squareup/salesreport/SalesReportScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator$onScreen$3;->this$0:Lcom/squareup/salesreport/SalesReportCoordinator;

    iput-object p2, p0, Lcom/squareup/salesreport/SalesReportCoordinator$onScreen$3;->$screen:Lcom/squareup/salesreport/SalesReportScreen;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 177
    check-cast p1, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$onScreen$3;->invoke(Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;)V
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1093
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator$onScreen$3;->$screen:Lcom/squareup/salesreport/SalesReportScreen;

    invoke-virtual {v0}, Lcom/squareup/salesreport/SalesReportScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    new-instance v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectPredefinedRange;

    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportCoordinator$onScreen$3;->this$0:Lcom/squareup/salesreport/SalesReportCoordinator;

    invoke-static {v2}, Lcom/squareup/salesreport/SalesReportCoordinator;->access$getCurrentDate(Lcom/squareup/salesreport/SalesReportCoordinator;)Lorg/threeten/bp/LocalDate;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$SelectPredefinedRange;-><init>(Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;Lorg/threeten/bp/LocalDate;)V

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
