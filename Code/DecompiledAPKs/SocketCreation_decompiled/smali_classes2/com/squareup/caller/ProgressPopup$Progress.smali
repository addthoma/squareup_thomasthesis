.class public Lcom/squareup/caller/ProgressPopup$Progress;
.super Ljava/lang/Object;
.source "ProgressPopup.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/caller/ProgressPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Progress"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/caller/ProgressPopup$Progress;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final complete:Z

.field public final message:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 55
    new-instance v0, Lcom/squareup/caller/ProgressPopup$Progress$1;

    invoke-direct {v0}, Lcom/squareup/caller/ProgressPopup$Progress$1;-><init>()V

    sput-object v0, Lcom/squareup/caller/ProgressPopup$Progress;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 38
    invoke-direct {p0, p1, v0}, Lcom/squareup/caller/ProgressPopup$Progress;-><init>(Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/caller/ProgressPopup$Progress;->message:Ljava/lang/String;

    .line 43
    iput-boolean p2, p0, Lcom/squareup/caller/ProgressPopup$Progress;->complete:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 51
    iget-object p2, p0, Lcom/squareup/caller/ProgressPopup$Progress;->message:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 52
    iget-boolean p2, p0, Lcom/squareup/caller/ProgressPopup$Progress;->complete:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
