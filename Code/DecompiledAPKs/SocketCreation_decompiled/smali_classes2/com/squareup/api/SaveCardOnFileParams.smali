.class public Lcom/squareup/api/SaveCardOnFileParams;
.super Ljava/lang/Object;
.source "SaveCardOnFileParams.java"


# instance fields
.field public final customerId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/api/SaveCardOnFileParams;->customerId:Ljava/lang/String;

    return-void
.end method

.method public static fromNativeRequest(Landroid/content/Intent;)Lcom/squareup/api/SaveCardOnFileParams;
    .locals 1

    const-string v0, "com.squareup.pos.CUSTOMER_ID"

    .line 15
    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 16
    new-instance v0, Lcom/squareup/api/SaveCardOnFileParams;

    invoke-direct {v0, p0}, Lcom/squareup/api/SaveCardOnFileParams;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public copyToIntent(Landroid/content/Intent;)V
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/squareup/api/SaveCardOnFileParams;->customerId:Ljava/lang/String;

    const-string v1, "com.squareup.pos.CUSTOMER_ID"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-void
.end method
