.class public Lcom/squareup/api/ApiTransactionController;
.super Ljava/lang/Object;
.source "ApiTransactionController.java"


# static fields
.field private static final BROWSER_APPLICATION_ID_KEY:Ljava/lang/String; = "BROWSER_APPLICATION_ID"

.field private static final REQUEST_STATE_KEY:Ljava/lang/String; = "REQUEST_STATE"

.field private static final WEB_CALLBACK_URI_KEY:Ljava/lang/String; = "WEB_CALLBACK_URI"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final apiRequestController:Lcom/squareup/api/ApiRequestController;

.field private final apiState:Lcom/squareup/api/ApiTransactionState;

.field private browserApplicationId:Ljava/lang/String;

.field private final clock:Lcom/squareup/util/Clock;

.field private final home:Lcom/squareup/ui/main/Home;

.field private final isReaderSdk:Z

.field private loaded:Z

.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

.field private paymentInFlight:Lcom/squareup/payment/BillPayment;

.field private final resources:Landroid/content/res/Resources;

.field private state:Ljava/lang/String;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final userToken:Ljava/lang/String;

.field private webCallbackUri:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/util/Clock;Lcom/squareup/payment/Transaction;Landroid/content/res/Resources;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/api/ApiRequestController;Lcom/squareup/analytics/Analytics;ZLjava/lang/String;Lcom/squareup/ui/main/Home;Lcom/squareup/api/ApiTransactionState;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput-object p1, p0, Lcom/squareup/api/ApiTransactionController;->clock:Lcom/squareup/util/Clock;

    .line 125
    iput-object p2, p0, Lcom/squareup/api/ApiTransactionController;->transaction:Lcom/squareup/payment/Transaction;

    .line 126
    iput-object p3, p0, Lcom/squareup/api/ApiTransactionController;->resources:Landroid/content/res/Resources;

    .line 127
    iput-object p4, p0, Lcom/squareup/api/ApiTransactionController;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    .line 128
    iput-object p5, p0, Lcom/squareup/api/ApiTransactionController;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    .line 129
    iput-object p6, p0, Lcom/squareup/api/ApiTransactionController;->analytics:Lcom/squareup/analytics/Analytics;

    .line 130
    iput-boolean p7, p0, Lcom/squareup/api/ApiTransactionController;->isReaderSdk:Z

    .line 131
    iput-object p8, p0, Lcom/squareup/api/ApiTransactionController;->userToken:Ljava/lang/String;

    .line 132
    iput-object p9, p0, Lcom/squareup/api/ApiTransactionController;->home:Lcom/squareup/ui/main/Home;

    .line 133
    iput-object p10, p0, Lcom/squareup/api/ApiTransactionController;->apiState:Lcom/squareup/api/ApiTransactionState;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/api/ApiTransactionController;)Lcom/squareup/api/ApiRequestController;
    .locals 0

    .line 79
    iget-object p0, p0, Lcom/squareup/api/ApiTransactionController;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/api/ApiTransactionController;)Z
    .locals 0

    .line 79
    iget-boolean p0, p0, Lcom/squareup/api/ApiTransactionController;->loaded:Z

    return p0
.end method

.method static synthetic access$102(Lcom/squareup/api/ApiTransactionController;Z)Z
    .locals 0

    .line 79
    iput-boolean p1, p0, Lcom/squareup/api/ApiTransactionController;->loaded:Z

    return p1
.end method

.method static synthetic access$200(Lcom/squareup/api/ApiTransactionController;)Ljava/lang/String;
    .locals 0

    .line 79
    iget-object p0, p0, Lcom/squareup/api/ApiTransactionController;->browserApplicationId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$202(Lcom/squareup/api/ApiTransactionController;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 79
    iput-object p1, p0, Lcom/squareup/api/ApiTransactionController;->browserApplicationId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/squareup/api/ApiTransactionController;)Ljava/lang/String;
    .locals 0

    .line 79
    iget-object p0, p0, Lcom/squareup/api/ApiTransactionController;->webCallbackUri:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$302(Lcom/squareup/api/ApiTransactionController;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 79
    iput-object p1, p0, Lcom/squareup/api/ApiTransactionController;->webCallbackUri:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/squareup/api/ApiTransactionController;)Ljava/lang/String;
    .locals 0

    .line 79
    iget-object p0, p0, Lcom/squareup/api/ApiTransactionController;->state:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$402(Lcom/squareup/api/ApiTransactionController;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 79
    iput-object p1, p0, Lcom/squareup/api/ApiTransactionController;->state:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/squareup/api/ApiTransactionController;)Lcom/squareup/api/ApiTransactionState;
    .locals 0

    .line 79
    iget-object p0, p0, Lcom/squareup/api/ApiTransactionController;->apiState:Lcom/squareup/api/ApiTransactionState;

    return-object p0
.end method

.method private createCheckoutResult()Lcom/squareup/sdk/reader/checkout/CheckoutResult;
    .locals 13

    .line 412
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->paymentInFlight:Lcom/squareup/payment/BillPayment;

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    .line 413
    iget-object v1, p0, Lcom/squareup/api/ApiTransactionController;->paymentInFlight:Lcom/squareup/payment/BillPayment;

    invoke-virtual {v1}, Lcom/squareup/payment/BillPayment;->getFlushedTenders()Ljava/util/Set;

    move-result-object v1

    .line 416
    iget-object v2, p0, Lcom/squareup/api/ApiTransactionController;->paymentInFlight:Lcom/squareup/payment/BillPayment;

    invoke-virtual {v2}, Lcom/squareup/payment/BillPayment;->getCompleteBill()Lcom/squareup/queue/bills/CompleteBill;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/queue/bills/CompleteBill;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    .line 417
    iget-object v3, v2, Lcom/squareup/protos/client/bills/CompleteBillRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    .line 418
    iget-object v4, v3, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v4, v4, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 421
    iget-object v5, p0, Lcom/squareup/api/ApiTransactionController;->userToken:Ljava/lang/String;

    iget-object v6, v3, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    .line 422
    invoke-static {v6}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->from(Lcom/squareup/protos/common/Money;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v6

    iget-object v7, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-static {v5, v6, v7}, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->newBuilder(Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/Money;Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;

    move-result-object v5

    .line 424
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v6, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/payment/tender/BaseTender;

    .line 425
    invoke-virtual {v7}, Lcom/squareup/payment/tender/BaseTender;->requireTenderForHistory()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v8

    .line 426
    invoke-virtual {v7}, Lcom/squareup/payment/tender/BaseTender;->requireTender()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v9

    iget-object v9, v9, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 427
    invoke-virtual {v7}, Lcom/squareup/payment/tender/BaseTender;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v10

    .line 428
    invoke-virtual {v7}, Lcom/squareup/payment/tender/BaseTender;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v7

    invoke-static {v7}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->from(Lcom/squareup/protos/common/Money;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v7

    .line 431
    iget-object v11, v8, Lcom/squareup/protos/client/bills/Tender;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-static {v11}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->from(Lcom/squareup/protos/client/bills/Tender$Type;)Lcom/squareup/sdk/reader/checkout/Tender$Type;

    move-result-object v11

    .line 433
    sget-object v12, Lcom/squareup/sdk/reader/checkout/Tender$Type;->CARD:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    if-ne v11, v12, :cond_0

    .line 434
    iget-object v6, v8, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v6, v6, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    .line 436
    iget-object v11, v6, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    .line 438
    invoke-static {v11}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->from(Lcom/squareup/protos/client/bills/CardTender$Card;)Lcom/squareup/sdk/reader/checkout/Card;

    move-result-object v11

    iget-object v12, v6, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v12, v12, Lcom/squareup/protos/client/bills/CardTender$Card;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-static {v12}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->from(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    move-result-object v12

    invoke-static {v6}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->from(Lcom/squareup/protos/client/bills/CardTender;)Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;

    move-result-object v6

    .line 437
    invoke-static {v11, v12, v6}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->newBuilder(Lcom/squareup/sdk/reader/checkout/Card;Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;)Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;

    move-result-object v6

    .line 439
    invoke-virtual {v6}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;->build()Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

    move-result-object v6

    .line 440
    iget-object v9, v9, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 441
    invoke-static {v9, v7, v6}, Lcom/squareup/sdk/reader/checkout/Tender;->newCardTenderBuilder(Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/Money;Lcom/squareup/sdk/reader/checkout/TenderCardDetails;)Lcom/squareup/sdk/reader/checkout/Tender$Builder;

    move-result-object v6

    const/4 v7, 0x1

    move-object v7, v6

    const/4 v6, 0x1

    goto :goto_1

    .line 444
    :cond_0
    sget-object v9, Lcom/squareup/sdk/reader/checkout/Tender$Type;->CASH:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    if-ne v11, v9, :cond_1

    .line 445
    iget-object v9, v8, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v9, v9, Lcom/squareup/protos/client/bills/Tender$Method;->cash_tender:Lcom/squareup/protos/client/bills/CashTender;

    .line 447
    iget-object v11, v9, Lcom/squareup/protos/client/bills/CashTender;->amounts:Lcom/squareup/protos/client/bills/CashTender$Amounts;

    iget-object v11, v11, Lcom/squareup/protos/client/bills/CashTender$Amounts;->buyer_tendered_money:Lcom/squareup/protos/common/Money;

    .line 448
    invoke-static {v11}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->from(Lcom/squareup/protos/common/Money;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v11

    invoke-static {v11}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;->newBuilder(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;

    move-result-object v11

    iget-object v9, v9, Lcom/squareup/protos/client/bills/CashTender;->amounts:Lcom/squareup/protos/client/bills/CashTender$Amounts;

    iget-object v9, v9, Lcom/squareup/protos/client/bills/CashTender$Amounts;->change_back_money:Lcom/squareup/protos/common/Money;

    .line 449
    invoke-static {v9}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->from(Lcom/squareup/protos/common/Money;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v9

    invoke-virtual {v11, v9}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;->changeBackMoney(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;

    move-result-object v9

    .line 450
    invoke-virtual {v9}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;->build()Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

    move-result-object v9

    .line 452
    invoke-static {v7, v9}, Lcom/squareup/sdk/reader/checkout/Tender;->newCashTenderBuilder(Lcom/squareup/sdk/reader/checkout/Money;Lcom/squareup/sdk/reader/checkout/TenderCashDetails;)Lcom/squareup/sdk/reader/checkout/Tender$Builder;

    move-result-object v7

    goto :goto_1

    .line 455
    :cond_1
    invoke-static {v7}, Lcom/squareup/sdk/reader/checkout/Tender;->newOtherTenderBuilder(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/checkout/Tender$Builder;

    move-result-object v7

    .line 458
    :goto_1
    iget-object v8, v8, Lcom/squareup/protos/client/bills/Tender;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 459
    invoke-static {v8}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->from(Lcom/squareup/protos/client/ISO8601Date;)Ljava/util/Date;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->createdAt(Ljava/util/Date;)Lcom/squareup/sdk/reader/checkout/Tender$Builder;

    if-eqz v10, :cond_2

    .line 461
    invoke-static {v10, v4}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->from(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->tipMoney(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/checkout/Tender$Builder;

    .line 463
    :cond_2
    invoke-virtual {v7}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->build()Lcom/squareup/sdk/reader/checkout/Tender;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->addTender(Lcom/squareup/sdk/reader/checkout/Tender;)Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;

    goto/16 :goto_0

    .line 466
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/ApiTransactionController;->userToken:Ljava/lang/String;

    .line 467
    invoke-virtual {v5, v1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->locationId(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;

    move-result-object v1

    iget-object v2, v2, Lcom/squareup/protos/client/bills/CompleteBillRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Bill$Dates;->completed_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 468
    invoke-static {v2}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->from(Lcom/squareup/protos/client/ISO8601Date;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->createdAt(Ljava/util/Date;)Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;

    move-result-object v1

    iget-object v2, v3, Lcom/squareup/protos/client/bills/Cart$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    .line 469
    invoke-static {v2, v4}, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils;->from(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->totalTipMoney(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;

    if-eqz v6, :cond_4

    .line 472
    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-virtual {v5, v0}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->transactionId(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;

    .line 474
    :cond_4
    invoke-virtual {v5}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->build()Lcom/squareup/sdk/reader/checkout/CheckoutResult;

    move-result-object v0

    return-object v0
.end method

.method private createErrorResultIntent(Lcom/squareup/api/ApiErrorResult;)Landroid/content/Intent;
    .locals 8

    .line 306
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->resources:Landroid/content/res/Resources;

    iget v1, p1, Lcom/squareup/api/ApiErrorResult;->errorDescriptionResourceId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 307
    iget-object v5, p0, Lcom/squareup/api/ApiTransactionController;->webCallbackUri:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v4, p0, Lcom/squareup/api/ApiTransactionController;->browserApplicationId:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/api/ApiTransactionController;->state:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    .line 309
    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->getActivity()Landroid/app/Activity;

    move-result-object v7

    move-object v2, p1

    .line 308
    invoke-static/range {v2 .. v7}, Lcom/squareup/api/ApiTransactionController;->createWebErrorResultIntent(Lcom/squareup/api/ApiErrorResult;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->state:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/squareup/api/ApiTransactionController;->isReaderSdk:Z

    .line 310
    invoke-static {p1, v3, v0, v1}, Lcom/squareup/api/ApiTransactionController;->createNativeErrorResultIntent(Lcom/squareup/api/ApiErrorResult;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method static createNativeErrorResultIntent(Lcom/squareup/api/ApiErrorResult;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 1

    .line 315
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    if-eqz p3, :cond_0

    .line 316
    invoke-virtual {p0}, Lcom/squareup/api/ApiErrorResult;->name()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/squareup/api/ApiErrorResult;->errorCode:Ljava/lang/String;

    :goto_0
    const-string p3, "com.squareup.pos.ERROR_CODE"

    .line 317
    invoke-virtual {v0, p3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p0, "com.squareup.pos.ERROR_DESCRIPTION"

    .line 318
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p2, :cond_1

    const-string p0, "com.squareup.pos.STATE"

    .line 320
    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    return-object v0
.end method

.method private createPosApiSuccessResultIntent(Lcom/squareup/protos/client/IdPair;)Landroid/content/Intent;
    .locals 3

    .line 292
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 293
    iget-object v1, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    const-string v2, "com.squareup.pos.CLIENT_TRANSACTION_ID"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 295
    iget-object v1, p1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 296
    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    const-string v1, "com.squareup.pos.SERVER_TRANSACTION_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 299
    :cond_0
    iget-object p1, p0, Lcom/squareup/api/ApiTransactionController;->state:Ljava/lang/String;

    if-eqz p1, :cond_1

    const-string v1, "com.squareup.pos.STATE"

    .line 300
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    return-object v0
.end method

.method private createReaderSdkSuccessResultIntent(Lcom/squareup/sdk/reader/checkout/CheckoutResult;)Landroid/content/Intent;
    .locals 1

    .line 286
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 287
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/checkout/CheckoutResultParcelable;->writeCheckoutResultToIntent(Lcom/squareup/sdk/reader/checkout/CheckoutResult;Landroid/content/Intent;)V

    return-object v0
.end method

.method private createSuccessResultIntent()Landroid/content/Intent;
    .locals 1

    .line 251
    iget-boolean v0, p0, Lcom/squareup/api/ApiTransactionController;->isReaderSdk:Z

    if-eqz v0, :cond_0

    .line 252
    invoke-direct {p0}, Lcom/squareup/api/ApiTransactionController;->createCheckoutResult()Lcom/squareup/sdk/reader/checkout/CheckoutResult;

    move-result-object v0

    .line 253
    invoke-direct {p0, v0}, Lcom/squareup/api/ApiTransactionController;->createReaderSdkSuccessResultIntent(Lcom/squareup/sdk/reader/checkout/CheckoutResult;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->webCallbackUri:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 255
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->paymentInFlight:Lcom/squareup/payment/BillPayment;

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/api/ApiTransactionController;->createWebSuccessResultIntent(Lcom/squareup/protos/client/IdPair;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 257
    :cond_1
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->paymentInFlight:Lcom/squareup/payment/BillPayment;

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/api/ApiTransactionController;->createPosApiSuccessResultIntent(Lcom/squareup/protos/client/IdPair;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static createWebErrorResultIntent(Lcom/squareup/api/ApiErrorResult;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;)Landroid/content/Intent;
    .locals 3

    .line 327
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 328
    sget-object v1, Lcom/squareup/api/WebApiStrings;->WEB_ERROR_KEY_MAP:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    const-string v1, "unexpected_web_error"

    .line 329
    invoke-static {p0, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 331
    invoke-virtual {p5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget p1, Lcom/squareup/common/strings/R$string;->devplat_error_unknown_error:I

    .line 332
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string p0, "unexpected"

    .line 334
    :cond_0
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p3

    .line 335
    invoke-virtual {p3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object p3

    const-string v1, "success"

    const-string v2, "false"

    .line 336
    invoke-virtual {p3, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p3

    const-string v1, "error_code"

    .line 337
    invoke-virtual {p3, v1, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p0

    const-string p3, "debug_description"

    .line 338
    invoke-virtual {p0, p3, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p0

    if-eqz p4, :cond_1

    const-string p1, "state"

    .line 341
    invoke-virtual {p0, p1, p4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 343
    :cond_1
    invoke-virtual {p0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 345
    invoke-virtual {p5}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    invoke-static {v0, p2, p0}, Lcom/squareup/api/ApiTransactionController;->setPackageIfResolvable(Landroid/content/Intent;Ljava/lang/String;Landroid/content/pm/PackageManager;)V

    return-object v0
.end method

.method private createWebSuccessResultIntent(Lcom/squareup/protos/client/IdPair;)Landroid/content/Intent;
    .locals 4

    .line 262
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 263
    iget-object v1, p0, Lcom/squareup/api/ApiTransactionController;->webCallbackUri:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "success"

    const-string v3, "true"

    .line 265
    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 266
    iget-object v2, p0, Lcom/squareup/api/ApiTransactionController;->userToken:Ljava/lang/String;

    const-string v3, "location_id"

    invoke-virtual {v1, v3, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 267
    iget-object v2, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    const-string v3, "client_transaction_id"

    invoke-virtual {v1, v3, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 269
    iget-object v2, p1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 270
    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    const-string v2, "server_transaction_id"

    invoke-virtual {v1, v2, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 273
    :cond_0
    iget-object p1, p0, Lcom/squareup/api/ApiTransactionController;->state:Ljava/lang/String;

    if-eqz p1, :cond_1

    const-string v2, "state"

    .line 274
    invoke-virtual {v1, v2, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 277
    :cond_1
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 279
    iget-object p1, p0, Lcom/squareup/api/ApiTransactionController;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {p1}, Lcom/squareup/api/ApiRequestController;->getActivity()Landroid/app/Activity;

    move-result-object p1

    .line 281
    iget-object v1, p0, Lcom/squareup/api/ApiTransactionController;->browserApplicationId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/api/ApiTransactionController;->setPackageIfResolvable(Landroid/content/Intent;Ljava/lang/String;Landroid/content/pm/PackageManager;)V

    return-object v0
.end method

.method private logError(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/api/ApiErrorResult;)V
    .locals 10

    .line 361
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->sequenceUuid()Ljava/lang/String;

    move-result-object v4

    .line 362
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->requestStartTime()J

    move-result-wide v7

    .line 364
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->API:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Register API error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/squareup/api/ApiErrorResult;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 365
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->clientId()Ljava/lang/String;

    move-result-object v5

    .line 366
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v9, Lcom/squareup/api/TransactionFailureEvent;

    iget-object v2, p0, Lcom/squareup/api/ApiTransactionController;->clock:Lcom/squareup/util/Clock;

    move-object v1, v9

    move-object v3, p1

    move-object v6, p2

    invoke-direct/range {v1 .. v8}, Lcom/squareup/api/TransactionFailureEvent;-><init>(Lcom/squareup/util/Clock;Lcom/squareup/container/ContainerTreeKey;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/ApiErrorResult;J)V

    invoke-interface {v0, v9}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private logSuccess(Lcom/squareup/protos/client/IdPair;Lcom/squareup/api/TransactionSuccessEvent$Reason;)V
    .locals 10

    .line 351
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->sequenceUuid()Ljava/lang/String;

    move-result-object v3

    .line 352
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->requestStartTime()J

    move-result-wide v7

    .line 354
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->API:Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v2, "Register API success"

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 355
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->clientId()Ljava/lang/String;

    move-result-object v4

    .line 356
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v9, Lcom/squareup/api/TransactionSuccessEvent;

    iget-object v2, p0, Lcom/squareup/api/ApiTransactionController;->clock:Lcom/squareup/util/Clock;

    move-object v1, v9

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v1 .. v8}, Lcom/squareup/api/TransactionSuccessEvent;-><init>(Lcom/squareup/util/Clock;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Lcom/squareup/api/TransactionSuccessEvent$Reason;J)V

    invoke-interface {v0, v9}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private prepareErrorResult(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/api/ApiErrorResult;)V
    .locals 1

    .line 233
    invoke-direct {p0, p1, p2}, Lcom/squareup/api/ApiTransactionController;->logError(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/api/ApiErrorResult;)V

    .line 234
    invoke-direct {p0, p2}, Lcom/squareup/api/ApiTransactionController;->createErrorResultIntent(Lcom/squareup/api/ApiErrorResult;)Landroid/content/Intent;

    move-result-object p1

    .line 235
    iget-object p2, p0, Lcom/squareup/api/ApiTransactionController;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {p2}, Lcom/squareup/api/ApiRequestController;->getActivity()Landroid/app/Activity;

    move-result-object p2

    const/4 v0, 0x0

    invoke-direct {p0, v0, p2, p1}, Lcom/squareup/api/ApiTransactionController;->respondWithResult(ILandroid/app/Activity;Landroid/content/Intent;)V

    return-void
.end method

.method private prepareSuccessResult(Lcom/squareup/api/TransactionSuccessEvent$Reason;)V
    .locals 2

    .line 225
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->paymentInFlight:Lcom/squareup/payment/BillPayment;

    const-string v1, "paymentInFlight"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 226
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->paymentInFlight:Lcom/squareup/payment/BillPayment;

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/squareup/api/ApiTransactionController;->logSuccess(Lcom/squareup/protos/client/IdPair;Lcom/squareup/api/TransactionSuccessEvent$Reason;)V

    .line 227
    invoke-direct {p0}, Lcom/squareup/api/ApiTransactionController;->createSuccessResultIntent()Landroid/content/Intent;

    move-result-object p1

    .line 228
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-direct {p0, v1, v0, p1}, Lcom/squareup/api/ApiTransactionController;->respondWithResult(ILandroid/app/Activity;Landroid/content/Intent;)V

    return-void
.end method

.method private respondWithResult(ILandroid/app/Activity;Landroid/content/Intent;)V
    .locals 1

    .line 239
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->webCallbackUri:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 240
    iget-object p1, p0, Lcom/squareup/api/ApiTransactionController;->apiState:Lcom/squareup/api/ApiTransactionState;

    .line 241
    invoke-virtual {p1}, Lcom/squareup/api/ApiTransactionState;->getApiVersion()Lcom/squareup/api/ApiVersion;

    move-result-object p1

    invoke-static {p3, p1}, Lcom/squareup/api/ApiVersion;->downgradeWebResponseIntent(Landroid/content/Intent;Lcom/squareup/api/ApiVersion;)Landroid/content/Intent;

    move-result-object p1

    .line 242
    invoke-virtual {p2, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->apiState:Lcom/squareup/api/ApiTransactionState;

    .line 245
    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionState;->getApiVersion()Lcom/squareup/api/ApiVersion;

    move-result-object v0

    invoke-static {p3, v0}, Lcom/squareup/api/ApiVersion;->downgradeResponseIntent(Landroid/content/Intent;Lcom/squareup/api/ApiVersion;)Landroid/content/Intent;

    move-result-object p3

    .line 246
    invoke-virtual {p2, p1, p3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    :goto_0
    return-void
.end method

.method private static setPackageIfResolvable(Landroid/content/Intent;Ljava/lang/String;Landroid/content/pm/PackageManager;)V
    .locals 1

    const-string v0, "com.android.browser.application_id"

    .line 479
    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 480
    invoke-virtual {p0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 481
    invoke-static {p0, p2}, Lcom/squareup/util/Intents;->isIntentAvailable(Landroid/content/Intent;Landroid/content/pm/PackageManager;)Z

    move-result p2

    if-nez p2, :cond_0

    const/4 p2, 0x0

    .line 483
    invoke-virtual {p0, p2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object p1, p0, p2

    const-string p1, "Cannot send result intent to %s"

    .line 484
    invoke-static {p1, p0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public abortOnEscapeFromApiTransaction(Lflow/Traversal;)Lcom/squareup/container/DispatchStep$Result;
    .locals 3

    .line 214
    iget-object v0, p1, Lflow/Traversal;->origin:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    .line 215
    iget-object v1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v1

    .line 216
    iget-object v2, p0, Lcom/squareup/api/ApiTransactionController;->home:Lcom/squareup/ui/main/Home;

    iget-object p1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-static {v2, v1, p1}, Lcom/squareup/ui/main/HomeKt;->contains(Lcom/squareup/ui/main/Home;Ljava/lang/Object;Lflow/History;)Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/api/ApiErrorResult;->UNEXPECTED_HOME_SCREEN:Lcom/squareup/api/ApiErrorResult;

    .line 217
    invoke-virtual {p0, v0, p1}, Lcom/squareup/api/ApiTransactionController;->handleApiTransactionCanceled(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/api/ApiErrorResult;)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "abortOnEscapeFromApiTransaction"

    .line 218
    invoke-static {p1}, Lcom/squareup/container/DispatchStep$Result;->stop(Ljava/lang/String;)Lcom/squareup/container/DispatchStep$Result;

    move-result-object p1

    return-object p1

    .line 221
    :cond_0
    invoke-static {}, Lcom/squareup/container/DispatchStep$Result;->go()Lcom/squareup/container/DispatchStep$Result;

    move-result-object p1

    return-object p1
.end method

.method public buyerFlowStarted()V
    .locals 6

    .line 146
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->apiState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionState;->isApiTransactionRequest()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/ApiTransactionController;->paymentInFlight:Lcom/squareup/payment/BillPayment;

    .line 150
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->clientId()Ljava/lang/String;

    move-result-object v0

    .line 151
    iget-object v1, p0, Lcom/squareup/api/ApiTransactionController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/api/BuyerFlowStartedEvent;

    iget-object v3, p0, Lcom/squareup/api/ApiTransactionController;->clock:Lcom/squareup/util/Clock;

    iget-object v4, p0, Lcom/squareup/api/ApiTransactionController;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    .line 152
    invoke-virtual {v4}, Lcom/squareup/api/ApiRequestController;->requestStartTime()J

    move-result-wide v4

    invoke-direct {v2, v3, v0, v4, v5}, Lcom/squareup/api/BuyerFlowStartedEvent;-><init>(Lcom/squareup/util/Clock;Ljava/lang/String;J)V

    .line 151
    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public getBundler()Lmortar/bundler/Bundler;
    .locals 2

    .line 372
    new-instance v0, Lcom/squareup/api/ApiTransactionController$1;

    const-class v1, Lcom/squareup/api/ApiTransactionController;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/api/ApiTransactionController$1;-><init>(Lcom/squareup/api/ApiTransactionController;Ljava/lang/String;)V

    return-object v0
.end method

.method public handleApiTransactionCanceled(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/api/ApiErrorResult;)Z
    .locals 1

    const-string v0, "apiError"

    .line 174
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 176
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->apiState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionState;->isApiTransactionRequest()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 180
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/api/ApiTransactionController;->prepareErrorResult(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/api/ApiErrorResult;)V

    .line 181
    iget-object p1, p0, Lcom/squareup/api/ApiTransactionController;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {p1}, Lcom/squareup/api/ApiRequestController;->finish()V

    .line 182
    iget-object p1, p0, Lcom/squareup/api/ApiTransactionController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->resetReceiptScreenStrategy()V

    .line 183
    iget-object p1, p0, Lcom/squareup/api/ApiTransactionController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->reset()V

    const/4 p1, 0x1

    return p1
.end method

.method public handleBuyerFlowCompleted()Z
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->apiState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionState;->isApiTransactionRequest()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 191
    :cond_0
    sget-object v0, Lcom/squareup/api/TransactionSuccessEvent$Reason;->BUYER_FLOW_COMPLETED:Lcom/squareup/api/TransactionSuccessEvent$Reason;

    invoke-direct {p0, v0}, Lcom/squareup/api/ApiTransactionController;->prepareSuccessResult(Lcom/squareup/api/TransactionSuccessEvent$Reason;)V

    .line 192
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->resetReceiptScreenStrategy()V

    .line 193
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->finish()V

    const/4 v0, 0x1

    return v0
.end method

.method public handleDivertToOnboarding()Z
    .locals 2

    .line 160
    sget-object v0, Lcom/squareup/api/ApiErrorResult;->USER_NOT_ACTIVATED:Lcom/squareup/api/ApiErrorResult;

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/squareup/api/ApiTransactionController;->handleApiTransactionCanceled(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/api/ApiErrorResult;)Z

    move-result v0

    return v0
.end method

.method public handleTenderFlowCanceled()Z
    .locals 2

    .line 156
    sget-object v0, Lcom/squareup/api/ApiErrorResult;->TENDER_FLOW_CANCELED:Lcom/squareup/api/ApiErrorResult;

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/squareup/api/ApiTransactionController;->handleApiTransactionCanceled(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/api/ApiErrorResult;)Z

    move-result v0

    return v0
.end method

.method public handleTenderFlowCompleted(Lcom/squareup/payment/Payment;)Z
    .locals 1

    .line 199
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionController;->apiState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionState;->isApiTransactionRequest()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 202
    :cond_0
    check-cast p1, Lcom/squareup/payment/BillPayment;

    iput-object p1, p0, Lcom/squareup/api/ApiTransactionController;->paymentInFlight:Lcom/squareup/payment/BillPayment;

    .line 203
    sget-object p1, Lcom/squareup/api/TransactionSuccessEvent$Reason;->TENDER_FLOW_COMPLETED:Lcom/squareup/api/TransactionSuccessEvent$Reason;

    invoke-direct {p0, p1}, Lcom/squareup/api/ApiTransactionController;->prepareSuccessResult(Lcom/squareup/api/TransactionSuccessEvent$Reason;)V

    .line 204
    iget-object p1, p0, Lcom/squareup/api/ApiTransactionController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->resetReceiptScreenStrategy()V

    .line 205
    iget-object p1, p0, Lcom/squareup/api/ApiTransactionController;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {p1}, Lcom/squareup/api/ApiRequestController;->finish()V

    const/4 p1, 0x1

    return p1
.end method

.method public startTransaction(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 138
    iput-object p5, p0, Lcom/squareup/api/ApiTransactionController;->browserApplicationId:Ljava/lang/String;

    .line 139
    iput-object p6, p0, Lcom/squareup/api/ApiTransactionController;->webCallbackUri:Ljava/lang/String;

    .line 140
    iput-object p7, p0, Lcom/squareup/api/ApiTransactionController;->state:Ljava/lang/String;

    .line 141
    iget-object p5, p0, Lcom/squareup/api/ApiTransactionController;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {p5, p1, p2, p3, p4}, Lcom/squareup/api/ApiRequestController;->startApiSession(Ljava/lang/String;Ljava/lang/String;J)V

    .line 142
    iget-object p2, p0, Lcom/squareup/api/ApiTransactionController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance p5, Lcom/squareup/api/TenderFlowStartedEvent;

    iget-object p6, p0, Lcom/squareup/api/ApiTransactionController;->clock:Lcom/squareup/util/Clock;

    invoke-direct {p5, p6, p1, p3, p4}, Lcom/squareup/api/TenderFlowStartedEvent;-><init>(Lcom/squareup/util/Clock;Ljava/lang/String;J)V

    invoke-interface {p2, p5}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method
