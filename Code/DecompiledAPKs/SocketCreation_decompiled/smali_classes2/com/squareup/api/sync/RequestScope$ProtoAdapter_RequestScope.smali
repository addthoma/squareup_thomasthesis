.class final Lcom/squareup/api/sync/RequestScope$ProtoAdapter_RequestScope;
.super Lcom/squareup/wire/ProtoAdapter;
.source "RequestScope.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/sync/RequestScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_RequestScope"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/api/sync/RequestScope;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 393
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/api/sync/RequestScope;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/sync/RequestScope;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 430
    new-instance v0, Lcom/squareup/api/sync/RequestScope$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/RequestScope$Builder;-><init>()V

    .line 431
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 432
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/16 v4, 0x3e8

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 460
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 458
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/RequestScope$Builder;->supports_confirmations(Ljava/lang/Boolean;)Lcom/squareup/api/sync/RequestScope$Builder;

    goto :goto_0

    .line 457
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/RequestScope$Builder;->supports_advanced_availability(Ljava/lang/Boolean;)Lcom/squareup/api/sync/RequestScope$Builder;

    goto :goto_0

    .line 456
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/RequestScope$Builder;->supports_pagination(Ljava/lang/Boolean;)Lcom/squareup/api/sync/RequestScope$Builder;

    goto :goto_0

    .line 455
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/RequestScope$Builder;->supports_carts_in_appointments(Ljava/lang/Boolean;)Lcom/squareup/api/sync/RequestScope$Builder;

    goto :goto_0

    .line 454
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/RequestScope$Builder;->supports_cancelled_appointments(Ljava/lang/Boolean;)Lcom/squareup/api/sync/RequestScope$Builder;

    goto :goto_0

    .line 453
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/agenda/UnitIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/agenda/UnitIdentifier;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/RequestScope$Builder;->unit_identifier(Lcom/squareup/protos/agenda/UnitIdentifier;)Lcom/squareup/api/sync/RequestScope$Builder;

    goto :goto_0

    .line 447
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/tickets/TicketsRole;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/tickets/TicketsRole;

    invoke-virtual {v0, v4}, Lcom/squareup/api/sync/RequestScope$Builder;->tickets_role(Lcom/squareup/protos/tickets/TicketsRole;)Lcom/squareup/api/sync/RequestScope$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 449
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/api/sync/RequestScope$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 439
    :cond_1
    :try_start_1
    iget-object v4, v0, Lcom/squareup/api/sync/RequestScope$Builder;->supported_features:Ljava/util/List;

    sget-object v5, Lcom/squareup/api/sync/CatalogFeature;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v5, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v4

    .line 441
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/api/sync/RequestScope$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 436
    :cond_2
    sget-object v3, Lcom/squareup/api/sync/VisibleTypeSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/sync/VisibleTypeSet;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/RequestScope$Builder;->visible_type_set(Lcom/squareup/api/sync/VisibleTypeSet;)Lcom/squareup/api/sync/RequestScope$Builder;

    goto/16 :goto_0

    .line 435
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/RequestScope$Builder;->user_token(Ljava/lang/String;)Lcom/squareup/api/sync/RequestScope$Builder;

    goto/16 :goto_0

    .line 434
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/api/sync/RequestScope$Builder;->user_id(Ljava/lang/Integer;)Lcom/squareup/api/sync/RequestScope$Builder;

    goto/16 :goto_0

    .line 464
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/RequestScope$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 465
    invoke-virtual {v0}, Lcom/squareup/api/sync/RequestScope$Builder;->build()Lcom/squareup/api/sync/RequestScope;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x3ea
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 391
    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/RequestScope$ProtoAdapter_RequestScope;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/sync/RequestScope;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/sync/RequestScope;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 414
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/RequestScope;->user_token:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 415
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/RequestScope;->user_id:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 416
    sget-object v0, Lcom/squareup/api/sync/VisibleTypeSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/RequestScope;->visible_type_set:Lcom/squareup/api/sync/VisibleTypeSet;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 417
    sget-object v0, Lcom/squareup/api/sync/CatalogFeature;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asPacked()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/api/sync/RequestScope;->supported_features:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 418
    sget-object v0, Lcom/squareup/protos/tickets/TicketsRole;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/RequestScope;->tickets_role:Lcom/squareup/protos/tickets/TicketsRole;

    const/16 v2, 0x3e8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 419
    sget-object v0, Lcom/squareup/protos/agenda/UnitIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/RequestScope;->unit_identifier:Lcom/squareup/protos/agenda/UnitIdentifier;

    const/16 v2, 0x3ea

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 420
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/RequestScope;->supports_cancelled_appointments:Ljava/lang/Boolean;

    const/16 v2, 0x3eb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 421
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/RequestScope;->supports_carts_in_appointments:Ljava/lang/Boolean;

    const/16 v2, 0x3ec

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 422
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/RequestScope;->supports_pagination:Ljava/lang/Boolean;

    const/16 v2, 0x3ed

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 423
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/RequestScope;->supports_advanced_availability:Ljava/lang/Boolean;

    const/16 v2, 0x3ee

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 424
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/RequestScope;->supports_confirmations:Ljava/lang/Boolean;

    const/16 v2, 0x3ef

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 425
    invoke-virtual {p2}, Lcom/squareup/api/sync/RequestScope;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 391
    check-cast p2, Lcom/squareup/api/sync/RequestScope;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/api/sync/RequestScope$ProtoAdapter_RequestScope;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/sync/RequestScope;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/api/sync/RequestScope;)I
    .locals 4

    .line 398
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/RequestScope;->user_token:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/RequestScope;->user_id:Ljava/lang/Integer;

    const/4 v3, 0x1

    .line 399
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/sync/VisibleTypeSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/RequestScope;->visible_type_set:Lcom/squareup/api/sync/VisibleTypeSet;

    const/4 v3, 0x3

    .line 400
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/sync/CatalogFeature;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 401
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asPacked()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/api/sync/RequestScope;->supported_features:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/tickets/TicketsRole;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/RequestScope;->tickets_role:Lcom/squareup/protos/tickets/TicketsRole;

    const/16 v3, 0x3e8

    .line 402
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/agenda/UnitIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/RequestScope;->unit_identifier:Lcom/squareup/protos/agenda/UnitIdentifier;

    const/16 v3, 0x3ea

    .line 403
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/RequestScope;->supports_cancelled_appointments:Ljava/lang/Boolean;

    const/16 v3, 0x3eb

    .line 404
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/RequestScope;->supports_carts_in_appointments:Ljava/lang/Boolean;

    const/16 v3, 0x3ec

    .line 405
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/RequestScope;->supports_pagination:Ljava/lang/Boolean;

    const/16 v3, 0x3ed

    .line 406
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/RequestScope;->supports_advanced_availability:Ljava/lang/Boolean;

    const/16 v3, 0x3ee

    .line 407
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/RequestScope;->supports_confirmations:Ljava/lang/Boolean;

    const/16 v3, 0x3ef

    .line 408
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 409
    invoke-virtual {p1}, Lcom/squareup/api/sync/RequestScope;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 391
    check-cast p1, Lcom/squareup/api/sync/RequestScope;

    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/RequestScope$ProtoAdapter_RequestScope;->encodedSize(Lcom/squareup/api/sync/RequestScope;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/api/sync/RequestScope;)Lcom/squareup/api/sync/RequestScope;
    .locals 2

    .line 470
    invoke-virtual {p1}, Lcom/squareup/api/sync/RequestScope;->newBuilder()Lcom/squareup/api/sync/RequestScope$Builder;

    move-result-object p1

    .line 471
    iget-object v0, p1, Lcom/squareup/api/sync/RequestScope$Builder;->visible_type_set:Lcom/squareup/api/sync/VisibleTypeSet;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/api/sync/VisibleTypeSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/RequestScope$Builder;->visible_type_set:Lcom/squareup/api/sync/VisibleTypeSet;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/VisibleTypeSet;

    iput-object v0, p1, Lcom/squareup/api/sync/RequestScope$Builder;->visible_type_set:Lcom/squareup/api/sync/VisibleTypeSet;

    .line 472
    :cond_0
    iget-object v0, p1, Lcom/squareup/api/sync/RequestScope$Builder;->unit_identifier:Lcom/squareup/protos/agenda/UnitIdentifier;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/agenda/UnitIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/RequestScope$Builder;->unit_identifier:Lcom/squareup/protos/agenda/UnitIdentifier;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/agenda/UnitIdentifier;

    iput-object v0, p1, Lcom/squareup/api/sync/RequestScope$Builder;->unit_identifier:Lcom/squareup/protos/agenda/UnitIdentifier;

    .line 473
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/api/sync/RequestScope$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 474
    invoke-virtual {p1}, Lcom/squareup/api/sync/RequestScope$Builder;->build()Lcom/squareup/api/sync/RequestScope;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 391
    check-cast p1, Lcom/squareup/api/sync/RequestScope;

    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/RequestScope$ProtoAdapter_RequestScope;->redact(Lcom/squareup/api/sync/RequestScope;)Lcom/squareup/api/sync/RequestScope;

    move-result-object p1

    return-object p1
.end method
