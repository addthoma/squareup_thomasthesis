.class public final Lcom/squareup/api/sync/GetResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/sync/GetResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/sync/GetResponse;",
        "Lcom/squareup/api/sync/GetResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public catalog_object_count:Ljava/lang/Integer;

.field public current_bazaar_version:Ljava/lang/Long;

.field public current_server_version:Ljava/lang/Long;

.field public objects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectWrapper;",
            ">;"
        }
    .end annotation
.end field

.field public pagination_token:Ljava/lang/String;

.field public sync_again_immediately:Ljava/lang/Boolean;

.field public sync_token:Ljava/lang/String;

.field public total_object_count:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 212
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 213
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/sync/GetResponse$Builder;->objects:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/sync/GetResponse;
    .locals 11

    .line 276
    new-instance v10, Lcom/squareup/api/sync/GetResponse;

    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse$Builder;->current_server_version:Ljava/lang/Long;

    iget-object v2, p0, Lcom/squareup/api/sync/GetResponse$Builder;->objects:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/api/sync/GetResponse$Builder;->total_object_count:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/api/sync/GetResponse$Builder;->catalog_object_count:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/api/sync/GetResponse$Builder;->pagination_token:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/api/sync/GetResponse$Builder;->sync_token:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/api/sync/GetResponse$Builder;->sync_again_immediately:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/squareup/api/sync/GetResponse$Builder;->current_bazaar_version:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/api/sync/GetResponse;-><init>(Ljava/lang/Long;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 195
    invoke-virtual {p0}, Lcom/squareup/api/sync/GetResponse$Builder;->build()Lcom/squareup/api/sync/GetResponse;

    move-result-object v0

    return-object v0
.end method

.method public catalog_object_count(Ljava/lang/Integer;)Lcom/squareup/api/sync/GetResponse$Builder;
    .locals 0

    .line 243
    iput-object p1, p0, Lcom/squareup/api/sync/GetResponse$Builder;->catalog_object_count:Ljava/lang/Integer;

    return-object p0
.end method

.method public current_bazaar_version(Ljava/lang/Long;)Lcom/squareup/api/sync/GetResponse$Builder;
    .locals 0

    .line 270
    iput-object p1, p0, Lcom/squareup/api/sync/GetResponse$Builder;->current_bazaar_version:Ljava/lang/Long;

    return-object p0
.end method

.method public current_server_version(Ljava/lang/Long;)Lcom/squareup/api/sync/GetResponse$Builder;
    .locals 0

    .line 217
    iput-object p1, p0, Lcom/squareup/api/sync/GetResponse$Builder;->current_server_version:Ljava/lang/Long;

    return-object p0
.end method

.method public objects(Ljava/util/List;)Lcom/squareup/api/sync/GetResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectWrapper;",
            ">;)",
            "Lcom/squareup/api/sync/GetResponse$Builder;"
        }
    .end annotation

    .line 226
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 227
    iput-object p1, p0, Lcom/squareup/api/sync/GetResponse$Builder;->objects:Ljava/util/List;

    return-object p0
.end method

.method public pagination_token(Ljava/lang/String;)Lcom/squareup/api/sync/GetResponse$Builder;
    .locals 0

    .line 248
    iput-object p1, p0, Lcom/squareup/api/sync/GetResponse$Builder;->pagination_token:Ljava/lang/String;

    return-object p0
.end method

.method public sync_again_immediately(Ljava/lang/Boolean;)Lcom/squareup/api/sync/GetResponse$Builder;
    .locals 0

    .line 265
    iput-object p1, p0, Lcom/squareup/api/sync/GetResponse$Builder;->sync_again_immediately:Ljava/lang/Boolean;

    return-object p0
.end method

.method public sync_token(Ljava/lang/String;)Lcom/squareup/api/sync/GetResponse$Builder;
    .locals 0

    .line 256
    iput-object p1, p0, Lcom/squareup/api/sync/GetResponse$Builder;->sync_token:Ljava/lang/String;

    return-object p0
.end method

.method public total_object_count(Ljava/lang/Integer;)Lcom/squareup/api/sync/GetResponse$Builder;
    .locals 0

    .line 235
    iput-object p1, p0, Lcom/squareup/api/sync/GetResponse$Builder;->total_object_count:Ljava/lang/Integer;

    return-object p0
.end method
