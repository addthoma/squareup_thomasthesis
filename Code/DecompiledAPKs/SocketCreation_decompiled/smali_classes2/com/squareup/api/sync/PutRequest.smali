.class public final Lcom/squareup/api/sync/PutRequest;
.super Lcom/squareup/wire/Message;
.source "PutRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/sync/PutRequest$ProtoAdapter_PutRequest;,
        Lcom/squareup/api/sync/PutRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/sync/PutRequest;",
        "Lcom/squareup/api/sync/PutRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/sync/PutRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_OBSOLETE_SKIP_CLIENT_STATE_VALIDATION:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final OBSOLETE_skip_client_state_validation:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3e8
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final client_state:Lcom/squareup/api/sync/WritableSessionState;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.WritableSessionState#ADAPTER"
        tag = 0x1
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final objects:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectWrapper#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectWrapper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/api/sync/PutRequest$ProtoAdapter_PutRequest;

    invoke-direct {v0}, Lcom/squareup/api/sync/PutRequest$ProtoAdapter_PutRequest;-><init>()V

    sput-object v0, Lcom/squareup/api/sync/PutRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 31
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/sync/PutRequest;->DEFAULT_OBSOLETE_SKIP_CLIENT_STATE_VALIDATION:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/api/sync/WritableSessionState;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectWrapper;",
            ">;",
            "Lcom/squareup/api/sync/WritableSessionState;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 73
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/api/sync/PutRequest;-><init>(Ljava/util/List;Lcom/squareup/api/sync/WritableSessionState;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/api/sync/WritableSessionState;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectWrapper;",
            ">;",
            "Lcom/squareup/api/sync/WritableSessionState;",
            "Ljava/lang/Boolean;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 78
    sget-object v0, Lcom/squareup/api/sync/PutRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p4, "objects"

    .line 79
    invoke-static {p4, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/api/sync/PutRequest;->objects:Ljava/util/List;

    .line 80
    iput-object p2, p0, Lcom/squareup/api/sync/PutRequest;->client_state:Lcom/squareup/api/sync/WritableSessionState;

    .line 81
    iput-object p3, p0, Lcom/squareup/api/sync/PutRequest;->OBSOLETE_skip_client_state_validation:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 97
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/sync/PutRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 98
    :cond_1
    check-cast p1, Lcom/squareup/api/sync/PutRequest;

    .line 99
    invoke-virtual {p0}, Lcom/squareup/api/sync/PutRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/sync/PutRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/PutRequest;->objects:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/api/sync/PutRequest;->objects:Ljava/util/List;

    .line 100
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/PutRequest;->client_state:Lcom/squareup/api/sync/WritableSessionState;

    iget-object v3, p1, Lcom/squareup/api/sync/PutRequest;->client_state:Lcom/squareup/api/sync/WritableSessionState;

    .line 101
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/PutRequest;->OBSOLETE_skip_client_state_validation:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/api/sync/PutRequest;->OBSOLETE_skip_client_state_validation:Ljava/lang/Boolean;

    .line 102
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 107
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 109
    invoke-virtual {p0}, Lcom/squareup/api/sync/PutRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 110
    iget-object v1, p0, Lcom/squareup/api/sync/PutRequest;->objects:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 111
    iget-object v1, p0, Lcom/squareup/api/sync/PutRequest;->client_state:Lcom/squareup/api/sync/WritableSessionState;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/api/sync/WritableSessionState;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 112
    iget-object v1, p0, Lcom/squareup/api/sync/PutRequest;->OBSOLETE_skip_client_state_validation:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 113
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/sync/PutRequest$Builder;
    .locals 2

    .line 86
    new-instance v0, Lcom/squareup/api/sync/PutRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/PutRequest$Builder;-><init>()V

    .line 87
    iget-object v1, p0, Lcom/squareup/api/sync/PutRequest;->objects:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/api/sync/PutRequest$Builder;->objects:Ljava/util/List;

    .line 88
    iget-object v1, p0, Lcom/squareup/api/sync/PutRequest;->client_state:Lcom/squareup/api/sync/WritableSessionState;

    iput-object v1, v0, Lcom/squareup/api/sync/PutRequest$Builder;->client_state:Lcom/squareup/api/sync/WritableSessionState;

    .line 89
    iget-object v1, p0, Lcom/squareup/api/sync/PutRequest;->OBSOLETE_skip_client_state_validation:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/sync/PutRequest$Builder;->OBSOLETE_skip_client_state_validation:Ljava/lang/Boolean;

    .line 90
    invoke-virtual {p0}, Lcom/squareup/api/sync/PutRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/PutRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/api/sync/PutRequest;->newBuilder()Lcom/squareup/api/sync/PutRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 121
    iget-object v1, p0, Lcom/squareup/api/sync/PutRequest;->objects:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", objects="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/PutRequest;->objects:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 122
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/sync/PutRequest;->client_state:Lcom/squareup/api/sync/WritableSessionState;

    if-eqz v1, :cond_1

    const-string v1, ", client_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/PutRequest;->client_state:Lcom/squareup/api/sync/WritableSessionState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 123
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/sync/PutRequest;->OBSOLETE_skip_client_state_validation:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", OBSOLETE_skip_client_state_validation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/PutRequest;->OBSOLETE_skip_client_state_validation:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PutRequest{"

    .line 124
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
