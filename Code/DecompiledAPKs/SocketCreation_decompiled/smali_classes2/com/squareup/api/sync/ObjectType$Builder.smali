.class public final Lcom/squareup/api/sync/ObjectType$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ObjectType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/sync/ObjectType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/sync/ObjectType;",
        "Lcom/squareup/api/sync/ObjectType$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public agenda_type:Lcom/squareup/protos/agenda/AgendaType;

.field public external_type:Lcom/squareup/api/items/ExternalType;

.field public type:Lcom/squareup/api/items/Type;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 125
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public agenda_type(Lcom/squareup/protos/agenda/AgendaType;)Lcom/squareup/api/sync/ObjectType$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectType$Builder;->agenda_type:Lcom/squareup/protos/agenda/AgendaType;

    return-object p0
.end method

.method public build()Lcom/squareup/api/sync/ObjectType;
    .locals 5

    .line 145
    new-instance v0, Lcom/squareup/api/sync/ObjectType;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectType$Builder;->agenda_type:Lcom/squareup/protos/agenda/AgendaType;

    iget-object v2, p0, Lcom/squareup/api/sync/ObjectType$Builder;->type:Lcom/squareup/api/items/Type;

    iget-object v3, p0, Lcom/squareup/api/sync/ObjectType$Builder;->external_type:Lcom/squareup/api/items/ExternalType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/api/sync/ObjectType;-><init>(Lcom/squareup/protos/agenda/AgendaType;Lcom/squareup/api/items/Type;Lcom/squareup/api/items/ExternalType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 118
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectType$Builder;->build()Lcom/squareup/api/sync/ObjectType;

    move-result-object v0

    return-object v0
.end method

.method public external_type(Lcom/squareup/api/items/ExternalType;)Lcom/squareup/api/sync/ObjectType$Builder;
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectType$Builder;->external_type:Lcom/squareup/api/items/ExternalType;

    return-object p0
.end method

.method public type(Lcom/squareup/api/items/Type;)Lcom/squareup/api/sync/ObjectType$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectType$Builder;->type:Lcom/squareup/api/items/Type;

    return-object p0
.end method
