.class public Lcom/squareup/api/ClientSettingsValidator;
.super Ljava/lang/Object;
.source "ClientSettingsValidator.java"


# static fields
.field private static final SANDBOX_CLIENT_ID_PREFIX:Ljava/lang/String; = "sandbox-"


# instance fields
.field private final clientSettingsCache:Lcom/squareup/api/ClientSettingsCache;

.field private final connectService:Lcom/squareup/server/api/ConnectService;

.field private final fileScheduler:Lio/reactivex/Scheduler;

.field private final fingerprintVerifier:Lcom/squareup/api/FingerprintVerifier;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private queryClientSettings:Lrx/functions/Func1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Func1<",
            "Lcom/squareup/api/ClientInfo;",
            "Lrx/Observable<",
            "Lkotlin/Pair<",
            "Lcom/squareup/api/ClientInfo;",
            "Lcom/squareup/server/api/ClientSettings;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final resources:Landroid/content/res/Resources;

.field private final validateClientSettings:Lrx/functions/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Action1<",
            "Lkotlin/Pair<",
            "Lcom/squareup/api/ClientInfo;",
            "Lcom/squareup/server/api/ClientSettings;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/server/api/ConnectService;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/api/FingerprintVerifier;Lcom/squareup/api/ClientSettingsCache;Landroid/content/res/Resources;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Lcom/squareup/api/ClientSettingsValidator$1;

    invoke-direct {v0, p0}, Lcom/squareup/api/ClientSettingsValidator$1;-><init>(Lcom/squareup/api/ClientSettingsValidator;)V

    iput-object v0, p0, Lcom/squareup/api/ClientSettingsValidator;->queryClientSettings:Lrx/functions/Func1;

    .line 96
    new-instance v0, Lcom/squareup/api/ClientSettingsValidator$2;

    invoke-direct {v0, p0}, Lcom/squareup/api/ClientSettingsValidator$2;-><init>(Lcom/squareup/api/ClientSettingsValidator;)V

    iput-object v0, p0, Lcom/squareup/api/ClientSettingsValidator;->validateClientSettings:Lrx/functions/Action1;

    .line 123
    iput-object p1, p0, Lcom/squareup/api/ClientSettingsValidator;->connectService:Lcom/squareup/server/api/ConnectService;

    .line 124
    iput-object p2, p0, Lcom/squareup/api/ClientSettingsValidator;->mainScheduler:Lio/reactivex/Scheduler;

    .line 125
    iput-object p3, p0, Lcom/squareup/api/ClientSettingsValidator;->fileScheduler:Lio/reactivex/Scheduler;

    .line 126
    iput-object p4, p0, Lcom/squareup/api/ClientSettingsValidator;->fingerprintVerifier:Lcom/squareup/api/FingerprintVerifier;

    .line 127
    iput-object p5, p0, Lcom/squareup/api/ClientSettingsValidator;->clientSettingsCache:Lcom/squareup/api/ClientSettingsCache;

    .line 128
    iput-object p6, p0, Lcom/squareup/api/ClientSettingsValidator;->resources:Landroid/content/res/Resources;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/api/ClientSettingsValidator;)Lcom/squareup/api/ClientSettingsCache;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/api/ClientSettingsValidator;->clientSettingsCache:Lcom/squareup/api/ClientSettingsCache;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/api/ClientSettingsValidator;)Lio/reactivex/Scheduler;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/api/ClientSettingsValidator;->mainScheduler:Lio/reactivex/Scheduler;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/api/ClientSettingsValidator;)Lio/reactivex/Scheduler;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/api/ClientSettingsValidator;->fileScheduler:Lio/reactivex/Scheduler;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/api/ClientSettingsValidator;)Lcom/squareup/server/api/ConnectService;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/api/ClientSettingsValidator;->connectService:Lcom/squareup/server/api/ConnectService;

    return-object p0
.end method

.method static synthetic access$400(Ljava/lang/Object;)Lrx/functions/Func1;
    .locals 0

    .line 38
    invoke-static {p0}, Lcom/squareup/api/ClientSettingsValidator;->pairWithFirst(Ljava/lang/Object;)Lrx/functions/Func1;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$500(Ljava/lang/String;Lcom/squareup/server/api/ClientSettings;)Z
    .locals 0

    .line 38
    invoke-static {p0, p1}, Lcom/squareup/api/ClientSettingsValidator;->packageInSettings(Ljava/lang/String;Lcom/squareup/server/api/ClientSettings;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$600(Lcom/squareup/api/ClientSettingsValidator;Lcom/squareup/api/ClientInfo;Lcom/squareup/api/ApiErrorResult;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2}, Lcom/squareup/api/ClientSettingsValidator;->throwInvalidAppError(Lcom/squareup/api/ClientInfo;Lcom/squareup/api/ApiErrorResult;)V

    return-void
.end method

.method static synthetic access$700(Lcom/squareup/api/ClientSettingsValidator;)Lcom/squareup/api/FingerprintVerifier;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/api/ClientSettingsValidator;->fingerprintVerifier:Lcom/squareup/api/FingerprintVerifier;

    return-object p0
.end method

.method static synthetic access$800(Ljava/lang/String;Lcom/squareup/server/api/ClientSettings;)Z
    .locals 0

    .line 38
    invoke-static {p0, p1}, Lcom/squareup/api/ClientSettingsValidator;->callbackUriInSettings(Ljava/lang/String;Lcom/squareup/server/api/ClientSettings;)Z

    move-result p0

    return p0
.end method

.method private static callbackUriInSettings(Ljava/lang/String;Lcom/squareup/server/api/ClientSettings;)Z
    .locals 1

    .line 137
    iget-object p1, p1, Lcom/squareup/server/api/ClientSettings;->commerce_api:Lcom/squareup/server/api/ClientSettings$CommerceApi;

    iget-object p1, p1, Lcom/squareup/server/api/ClientSettings$CommerceApi;->web_callback_url_bases:Ljava/util/List;

    .line 138
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 139
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method static synthetic lambda$pairWithFirst$1(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;
    .locals 1

    .line 160
    new-instance v0, Lkotlin/Pair;

    invoke-direct {v0, p0, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method private static packageInSettings(Ljava/lang/String;Lcom/squareup/server/api/ClientSettings;)Z
    .locals 2

    .line 147
    iget-object p1, p1, Lcom/squareup/server/api/ClientSettings;->commerce_api:Lcom/squareup/server/api/ClientSettings$CommerceApi;

    iget-object p1, p1, Lcom/squareup/server/api/ClientSettings$CommerceApi;->android_packages:Ljava/util/List;

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 151
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/api/ClientSettings$AndroidPackage;

    .line 152
    iget-object v1, v1, Lcom/squareup/server/api/ClientSettings$AndroidPackage;->package_name:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 p0, 0x1

    return p0

    :cond_2
    return v0
.end method

.method private static pairWithFirst(Ljava/lang/Object;)Lrx/functions/Func1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<F:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(TF;)",
            "Lrx/functions/Func1<",
            "TS;",
            "Lkotlin/Pair<",
            "TF;TS;>;>;"
        }
    .end annotation

    .line 160
    new-instance v0, Lcom/squareup/api/-$$Lambda$ClientSettingsValidator$EpmDTpzf935GOdpz7zv6lVGQcZ4;

    invoke-direct {v0, p0}, Lcom/squareup/api/-$$Lambda$ClientSettingsValidator$EpmDTpzf935GOdpz7zv6lVGQcZ4;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method private throwInvalidAppError(Lcom/squareup/api/ClientInfo;Lcom/squareup/api/ApiErrorResult;)V
    .locals 5

    .line 164
    iget-object v0, p1, Lcom/squareup/api/ClientInfo;->packageName:Ljava/lang/String;

    .line 167
    :try_start_0
    iget-object v1, p0, Lcom/squareup/api/ClientSettingsValidator;->fingerprintVerifier:Lcom/squareup/api/FingerprintVerifier;

    invoke-virtual {v1, v0}, Lcom/squareup/api/FingerprintVerifier;->computeAppFingerprints(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1
    :try_end_0
    .catch Lcom/squareup/api/ApiValidationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 169
    :catch_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 174
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const/4 v2, 0x0

    .line 175
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 176
    iget-object v2, p0, Lcom/squareup/api/ClientSettingsValidator;->resources:Landroid/content/res/Resources;

    sget v3, Lcom/squareup/common/strings/R$string;->devplat_error_dev_portal_error_help:I

    invoke-static {v2, v3}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/api/ClientSettingsValidator;->resources:Landroid/content/res/Resources;

    iget v4, p2, Lcom/squareup/api/ApiErrorResult;->errorDescriptionResourceId:I

    .line 178
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "error_description"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    const-string v3, "package_name"

    .line 179
    invoke-virtual {v2, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v2, "fingerprint"

    .line 180
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/api/ClientInfo;->clientId:Ljava/lang/String;

    const-string v1, "client_id"

    .line 181
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 182
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 183
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 184
    new-instance v0, Lcom/squareup/api/ApiValidationException;

    invoke-direct {v0, p2, p1}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;Ljava/lang/String;)V

    throw v0

    .line 186
    :cond_0
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    invoke-direct {p1, p2}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1
.end method


# virtual methods
.method public synthetic lambda$validateClientSettings$0$ClientSettingsValidator(Lrx/Observable;)Lrx/Observable;
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/api/ClientSettingsValidator;->queryClientSettings:Lrx/functions/Func1;

    invoke-virtual {p1, v0}, Lrx/Observable;->flatMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/api/ClientSettingsValidator;->validateClientSettings:Lrx/functions/Action1;

    .line 133
    invoke-virtual {p1, v0}, Lrx/Observable;->doOnNext(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public validateClientSettings()Lrx/Observable$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable$Transformer<",
            "Lcom/squareup/api/ClientInfo;",
            "Lkotlin/Pair<",
            "Lcom/squareup/api/ClientInfo;",
            "Lcom/squareup/server/api/ClientSettings;",
            ">;>;"
        }
    .end annotation

    .line 132
    new-instance v0, Lcom/squareup/api/-$$Lambda$ClientSettingsValidator$W0Udi_NvBCIU1XZZyADCTPmvCe0;

    invoke-direct {v0, p0}, Lcom/squareup/api/-$$Lambda$ClientSettingsValidator$W0Udi_NvBCIU1XZZyADCTPmvCe0;-><init>(Lcom/squareup/api/ClientSettingsValidator;)V

    return-object v0
.end method
