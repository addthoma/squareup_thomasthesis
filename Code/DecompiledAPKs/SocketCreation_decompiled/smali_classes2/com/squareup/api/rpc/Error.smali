.class public final Lcom/squareup/api/rpc/Error;
.super Lcom/squareup/wire/Message;
.source "Error.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/rpc/Error$ProtoAdapter_Error;,
        Lcom/squareup/api/rpc/Error$ServerErrorCode;,
        Lcom/squareup/api/rpc/Error$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/rpc/Error;",
        "Lcom/squareup/api/rpc/Error$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/rpc/Error;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_FIELD_VALIDATION_ERROR_CODE:Lcom/squareup/api/sync/FieldValidationErrorCode;

.field public static final DEFAULT_INVALID_FIELD_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_SERVER_ERROR_CODE:Lcom/squareup/api/rpc/Error$ServerErrorCode;

.field public static final DEFAULT_SYNC_ERROR_CODE:Lcom/squareup/api/sync/SyncErrorCode;

.field private static final serialVersionUID:J


# instance fields
.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final field_validation_error_code:Lcom/squareup/api/sync/FieldValidationErrorCode;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.FieldValidationErrorCode#ADAPTER"
        tag = 0x3fd
    .end annotation
.end field

.field public final invalid_field_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3ff
    .end annotation
.end field

.field public final invalid_object_id:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0x3fe
    .end annotation
.end field

.field public final server_error_code:Lcom/squareup/api/rpc/Error$ServerErrorCode;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.rpc.Error$ServerErrorCode#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final sync_error_code:Lcom/squareup/api/sync/SyncErrorCode;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.SyncErrorCode#ADAPTER"
        tag = 0x3fc
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/api/rpc/Error$ProtoAdapter_Error;

    invoke-direct {v0}, Lcom/squareup/api/rpc/Error$ProtoAdapter_Error;-><init>()V

    sput-object v0, Lcom/squareup/api/rpc/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 31
    sget-object v0, Lcom/squareup/api/rpc/Error$ServerErrorCode;->INTERNAL_SERVER_ERROR:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    sput-object v0, Lcom/squareup/api/rpc/Error;->DEFAULT_SERVER_ERROR_CODE:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    .line 33
    sget-object v0, Lcom/squareup/api/sync/SyncErrorCode;->SESSION_EXPIRED:Lcom/squareup/api/sync/SyncErrorCode;

    sput-object v0, Lcom/squareup/api/rpc/Error;->DEFAULT_SYNC_ERROR_CODE:Lcom/squareup/api/sync/SyncErrorCode;

    .line 35
    sget-object v0, Lcom/squareup/api/sync/FieldValidationErrorCode;->FIELD_MISSING:Lcom/squareup/api/sync/FieldValidationErrorCode;

    sput-object v0, Lcom/squareup/api/rpc/Error;->DEFAULT_FIELD_VALIDATION_ERROR_CODE:Lcom/squareup/api/sync/FieldValidationErrorCode;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/api/rpc/Error$ServerErrorCode;Lcom/squareup/api/sync/SyncErrorCode;Lcom/squareup/api/sync/FieldValidationErrorCode;Lcom/squareup/api/sync/ObjectId;Ljava/lang/String;)V
    .locals 8

    .line 100
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/api/rpc/Error;-><init>(Ljava/lang/String;Lcom/squareup/api/rpc/Error$ServerErrorCode;Lcom/squareup/api/sync/SyncErrorCode;Lcom/squareup/api/sync/FieldValidationErrorCode;Lcom/squareup/api/sync/ObjectId;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/api/rpc/Error$ServerErrorCode;Lcom/squareup/api/sync/SyncErrorCode;Lcom/squareup/api/sync/FieldValidationErrorCode;Lcom/squareup/api/sync/ObjectId;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 106
    sget-object v0, Lcom/squareup/api/rpc/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 107
    iput-object p1, p0, Lcom/squareup/api/rpc/Error;->description:Ljava/lang/String;

    .line 108
    iput-object p2, p0, Lcom/squareup/api/rpc/Error;->server_error_code:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    .line 109
    iput-object p3, p0, Lcom/squareup/api/rpc/Error;->sync_error_code:Lcom/squareup/api/sync/SyncErrorCode;

    .line 110
    iput-object p4, p0, Lcom/squareup/api/rpc/Error;->field_validation_error_code:Lcom/squareup/api/sync/FieldValidationErrorCode;

    .line 111
    iput-object p5, p0, Lcom/squareup/api/rpc/Error;->invalid_object_id:Lcom/squareup/api/sync/ObjectId;

    .line 112
    iput-object p6, p0, Lcom/squareup/api/rpc/Error;->invalid_field_name:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 131
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/rpc/Error;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 132
    :cond_1
    check-cast p1, Lcom/squareup/api/rpc/Error;

    .line 133
    invoke-virtual {p0}, Lcom/squareup/api/rpc/Error;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/rpc/Error;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/rpc/Error;->description:Ljava/lang/String;

    .line 134
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->server_error_code:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    iget-object v3, p1, Lcom/squareup/api/rpc/Error;->server_error_code:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    .line 135
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->sync_error_code:Lcom/squareup/api/sync/SyncErrorCode;

    iget-object v3, p1, Lcom/squareup/api/rpc/Error;->sync_error_code:Lcom/squareup/api/sync/SyncErrorCode;

    .line 136
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->field_validation_error_code:Lcom/squareup/api/sync/FieldValidationErrorCode;

    iget-object v3, p1, Lcom/squareup/api/rpc/Error;->field_validation_error_code:Lcom/squareup/api/sync/FieldValidationErrorCode;

    .line 137
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->invalid_object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p1, Lcom/squareup/api/rpc/Error;->invalid_object_id:Lcom/squareup/api/sync/ObjectId;

    .line 138
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->invalid_field_name:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/api/rpc/Error;->invalid_field_name:Ljava/lang/String;

    .line 139
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 144
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 146
    invoke-virtual {p0}, Lcom/squareup/api/rpc/Error;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 147
    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->description:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 148
    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->server_error_code:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/api/rpc/Error$ServerErrorCode;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 149
    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->sync_error_code:Lcom/squareup/api/sync/SyncErrorCode;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/api/sync/SyncErrorCode;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 150
    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->field_validation_error_code:Lcom/squareup/api/sync/FieldValidationErrorCode;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/api/sync/FieldValidationErrorCode;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 151
    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->invalid_object_id:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 152
    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->invalid_field_name:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 153
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/rpc/Error$Builder;
    .locals 2

    .line 117
    new-instance v0, Lcom/squareup/api/rpc/Error$Builder;

    invoke-direct {v0}, Lcom/squareup/api/rpc/Error$Builder;-><init>()V

    .line 118
    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/rpc/Error$Builder;->description:Ljava/lang/String;

    .line 119
    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->server_error_code:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    iput-object v1, v0, Lcom/squareup/api/rpc/Error$Builder;->server_error_code:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    .line 120
    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->sync_error_code:Lcom/squareup/api/sync/SyncErrorCode;

    iput-object v1, v0, Lcom/squareup/api/rpc/Error$Builder;->sync_error_code:Lcom/squareup/api/sync/SyncErrorCode;

    .line 121
    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->field_validation_error_code:Lcom/squareup/api/sync/FieldValidationErrorCode;

    iput-object v1, v0, Lcom/squareup/api/rpc/Error$Builder;->field_validation_error_code:Lcom/squareup/api/sync/FieldValidationErrorCode;

    .line 122
    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->invalid_object_id:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/api/rpc/Error$Builder;->invalid_object_id:Lcom/squareup/api/sync/ObjectId;

    .line 123
    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->invalid_field_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/rpc/Error$Builder;->invalid_field_name:Ljava/lang/String;

    .line 124
    invoke-virtual {p0}, Lcom/squareup/api/rpc/Error;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/rpc/Error$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/api/rpc/Error;->newBuilder()Lcom/squareup/api/rpc/Error$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 161
    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->description:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->server_error_code:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    if-eqz v1, :cond_1

    const-string v1, ", server_error_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->server_error_code:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 163
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->sync_error_code:Lcom/squareup/api/sync/SyncErrorCode;

    if-eqz v1, :cond_2

    const-string v1, ", sync_error_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->sync_error_code:Lcom/squareup/api/sync/SyncErrorCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 164
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->field_validation_error_code:Lcom/squareup/api/sync/FieldValidationErrorCode;

    if-eqz v1, :cond_3

    const-string v1, ", field_validation_error_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->field_validation_error_code:Lcom/squareup/api/sync/FieldValidationErrorCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 165
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->invalid_object_id:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_4

    const-string v1, ", invalid_object_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->invalid_object_id:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 166
    :cond_4
    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->invalid_field_name:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", invalid_field_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/rpc/Error;->invalid_field_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Error{"

    .line 167
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
