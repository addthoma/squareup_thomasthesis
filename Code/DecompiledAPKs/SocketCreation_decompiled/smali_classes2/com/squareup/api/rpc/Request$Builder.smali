.class public final Lcom/squareup/api/rpc/Request$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Request.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/rpc/Request;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/rpc/Request;",
        "Lcom/squareup/api/rpc/Request$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public api_version:Lcom/squareup/api/sync/ApiVersion;

.field public create_session_request:Lcom/squareup/api/sync/CreateSessionRequest;

.field public get_request:Lcom/squareup/api/sync/GetRequest;

.field public id:Ljava/lang/Long;

.field public inventory_adjust_request:Lcom/squareup/protos/inventory/InventoryAdjustment;

.field public method_name:Ljava/lang/String;

.field public put_request:Lcom/squareup/api/sync/PutRequest;

.field public request_enqueued_at_millis:Ljava/lang/Long;

.field public scope:Lcom/squareup/api/sync/RequestScope;

.field public service_name:Ljava/lang/String;

.field public skip_writable_session_state_validation:Ljava/lang/Boolean;

.field public writable_session_state:Lcom/squareup/api/sync/WritableSessionState;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 313
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public api_version(Lcom/squareup/api/sync/ApiVersion;)Lcom/squareup/api/rpc/Request$Builder;
    .locals 0

    .line 367
    iput-object p1, p0, Lcom/squareup/api/rpc/Request$Builder;->api_version:Lcom/squareup/api/sync/ApiVersion;

    return-object p0
.end method

.method public build()Lcom/squareup/api/rpc/Request;
    .locals 15

    .line 428
    new-instance v14, Lcom/squareup/api/rpc/Request;

    iget-object v1, p0, Lcom/squareup/api/rpc/Request$Builder;->id:Ljava/lang/Long;

    iget-object v2, p0, Lcom/squareup/api/rpc/Request$Builder;->service_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/api/rpc/Request$Builder;->method_name:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/api/rpc/Request$Builder;->scope:Lcom/squareup/api/sync/RequestScope;

    iget-object v5, p0, Lcom/squareup/api/rpc/Request$Builder;->api_version:Lcom/squareup/api/sync/ApiVersion;

    iget-object v6, p0, Lcom/squareup/api/rpc/Request$Builder;->writable_session_state:Lcom/squareup/api/sync/WritableSessionState;

    iget-object v7, p0, Lcom/squareup/api/rpc/Request$Builder;->create_session_request:Lcom/squareup/api/sync/CreateSessionRequest;

    iget-object v8, p0, Lcom/squareup/api/rpc/Request$Builder;->get_request:Lcom/squareup/api/sync/GetRequest;

    iget-object v9, p0, Lcom/squareup/api/rpc/Request$Builder;->put_request:Lcom/squareup/api/sync/PutRequest;

    iget-object v10, p0, Lcom/squareup/api/rpc/Request$Builder;->skip_writable_session_state_validation:Ljava/lang/Boolean;

    iget-object v11, p0, Lcom/squareup/api/rpc/Request$Builder;->request_enqueued_at_millis:Ljava/lang/Long;

    iget-object v12, p0, Lcom/squareup/api/rpc/Request$Builder;->inventory_adjust_request:Lcom/squareup/protos/inventory/InventoryAdjustment;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v13

    move-object v0, v14

    invoke-direct/range {v0 .. v13}, Lcom/squareup/api/rpc/Request;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/sync/RequestScope;Lcom/squareup/api/sync/ApiVersion;Lcom/squareup/api/sync/WritableSessionState;Lcom/squareup/api/sync/CreateSessionRequest;Lcom/squareup/api/sync/GetRequest;Lcom/squareup/api/sync/PutRequest;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/squareup/protos/inventory/InventoryAdjustment;Lokio/ByteString;)V

    return-object v14
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 288
    invoke-virtual {p0}, Lcom/squareup/api/rpc/Request$Builder;->build()Lcom/squareup/api/rpc/Request;

    move-result-object v0

    return-object v0
.end method

.method public create_session_request(Lcom/squareup/api/sync/CreateSessionRequest;)Lcom/squareup/api/rpc/Request$Builder;
    .locals 0

    .line 386
    iput-object p1, p0, Lcom/squareup/api/rpc/Request$Builder;->create_session_request:Lcom/squareup/api/sync/CreateSessionRequest;

    return-object p0
.end method

.method public get_request(Lcom/squareup/api/sync/GetRequest;)Lcom/squareup/api/rpc/Request$Builder;
    .locals 0

    .line 391
    iput-object p1, p0, Lcom/squareup/api/rpc/Request$Builder;->get_request:Lcom/squareup/api/sync/GetRequest;

    return-object p0
.end method

.method public id(Ljava/lang/Long;)Lcom/squareup/api/rpc/Request$Builder;
    .locals 0

    .line 322
    iput-object p1, p0, Lcom/squareup/api/rpc/Request$Builder;->id:Ljava/lang/Long;

    return-object p0
.end method

.method public inventory_adjust_request(Lcom/squareup/protos/inventory/InventoryAdjustment;)Lcom/squareup/api/rpc/Request$Builder;
    .locals 0

    .line 422
    iput-object p1, p0, Lcom/squareup/api/rpc/Request$Builder;->inventory_adjust_request:Lcom/squareup/protos/inventory/InventoryAdjustment;

    return-object p0
.end method

.method public method_name(Ljava/lang/String;)Lcom/squareup/api/rpc/Request$Builder;
    .locals 0

    .line 338
    iput-object p1, p0, Lcom/squareup/api/rpc/Request$Builder;->method_name:Ljava/lang/String;

    return-object p0
.end method

.method public put_request(Lcom/squareup/api/sync/PutRequest;)Lcom/squareup/api/rpc/Request$Builder;
    .locals 0

    .line 396
    iput-object p1, p0, Lcom/squareup/api/rpc/Request$Builder;->put_request:Lcom/squareup/api/sync/PutRequest;

    return-object p0
.end method

.method public request_enqueued_at_millis(Ljava/lang/Long;)Lcom/squareup/api/rpc/Request$Builder;
    .locals 0

    .line 417
    iput-object p1, p0, Lcom/squareup/api/rpc/Request$Builder;->request_enqueued_at_millis:Ljava/lang/Long;

    return-object p0
.end method

.method public scope(Lcom/squareup/api/sync/RequestScope;)Lcom/squareup/api/rpc/Request$Builder;
    .locals 0

    .line 354
    iput-object p1, p0, Lcom/squareup/api/rpc/Request$Builder;->scope:Lcom/squareup/api/sync/RequestScope;

    return-object p0
.end method

.method public service_name(Ljava/lang/String;)Lcom/squareup/api/rpc/Request$Builder;
    .locals 0

    .line 330
    iput-object p1, p0, Lcom/squareup/api/rpc/Request$Builder;->service_name:Ljava/lang/String;

    return-object p0
.end method

.method public skip_writable_session_state_validation(Ljava/lang/Boolean;)Lcom/squareup/api/rpc/Request$Builder;
    .locals 0

    .line 408
    iput-object p1, p0, Lcom/squareup/api/rpc/Request$Builder;->skip_writable_session_state_validation:Ljava/lang/Boolean;

    return-object p0
.end method

.method public writable_session_state(Lcom/squareup/api/sync/WritableSessionState;)Lcom/squareup/api/rpc/Request$Builder;
    .locals 0

    .line 378
    iput-object p1, p0, Lcom/squareup/api/rpc/Request$Builder;->writable_session_state:Lcom/squareup/api/sync/WritableSessionState;

    return-object p0
.end method
