.class Lcom/squareup/api/ClientSettingsValidator$1;
.super Ljava/lang/Object;
.source "ClientSettingsValidator.java"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/ClientSettingsValidator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "Lcom/squareup/api/ClientInfo;",
        "Lrx/Observable<",
        "Lkotlin/Pair<",
        "Lcom/squareup/api/ClientInfo;",
        "Lcom/squareup/server/api/ClientSettings;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/api/ClientSettingsValidator;


# direct methods
.method constructor <init>(Lcom/squareup/api/ClientSettingsValidator;)V
    .locals 0

    .line 50
    iput-object p1, p0, Lcom/squareup/api/ClientSettingsValidator$1;->this$0:Lcom/squareup/api/ClientSettingsValidator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$call$0(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/server/api/ClientSettings;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 69
    instance-of v0, p0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 70
    check-cast p0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/api/ClientSettings;

    return-object p0

    .line 72
    :cond_0
    check-cast p0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    .line 73
    invoke-virtual {p0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p0

    .line 75
    instance-of v0, p0, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_1

    .line 76
    check-cast p0, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {p0}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/api/ClientSettings;

    return-object p0

    .line 77
    :cond_1
    instance-of v0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-nez v0, :cond_4

    .line 79
    instance-of v0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-nez v0, :cond_3

    .line 81
    instance-of p0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz p0, :cond_2

    .line 82
    new-instance p0, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->SETTINGS_NO_SERVER:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p0, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p0

    .line 84
    :cond_2
    new-instance p0, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->SETTINGS_UNKNOWN_ERROR:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p0, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p0

    .line 80
    :cond_3
    new-instance p0, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->SETTINGS_INVALID_CLIENT_ID:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p0, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p0

    .line 78
    :cond_4
    new-instance p0, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->SETTINGS_NO_NETWORK:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p0, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p0
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 50
    check-cast p1, Lcom/squareup/api/ClientInfo;

    invoke-virtual {p0, p1}, Lcom/squareup/api/ClientSettingsValidator$1;->call(Lcom/squareup/api/ClientInfo;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public call(Lcom/squareup/api/ClientInfo;)Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/ClientInfo;",
            ")",
            "Lrx/Observable<",
            "Lkotlin/Pair<",
            "Lcom/squareup/api/ClientInfo;",
            "Lcom/squareup/server/api/ClientSettings;",
            ">;>;"
        }
    .end annotation

    .line 53
    iget-object v0, p1, Lcom/squareup/api/ClientInfo;->clientId:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v1, "sandbox-"

    .line 59
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 64
    iget-object v1, p0, Lcom/squareup/api/ClientSettingsValidator$1;->this$0:Lcom/squareup/api/ClientSettingsValidator;

    invoke-static {v1}, Lcom/squareup/api/ClientSettingsValidator;->access$000(Lcom/squareup/api/ClientSettingsValidator;)Lcom/squareup/api/ClientSettingsCache;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/api/ClientSettingsCache;->get(Ljava/lang/String;)Lrx/Observable;

    move-result-object v1

    .line 66
    iget-object v2, p0, Lcom/squareup/api/ClientSettingsValidator$1;->this$0:Lcom/squareup/api/ClientSettingsValidator;

    invoke-static {v2}, Lcom/squareup/api/ClientSettingsValidator;->access$300(Lcom/squareup/api/ClientSettingsValidator;)Lcom/squareup/server/api/ConnectService;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/squareup/server/api/ConnectService;->getClientSettings(Ljava/lang/String;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v2

    .line 67
    invoke-virtual {v2}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v2

    sget-object v3, Lcom/squareup/api/-$$Lambda$ClientSettingsValidator$1$1Yglp4_ujVOJuY5DynH1COvM6T8;->INSTANCE:Lcom/squareup/api/-$$Lambda$ClientSettingsValidator$1$1Yglp4_ujVOJuY5DynH1COvM6T8;

    .line 68
    invoke-virtual {v2, v3}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/api/ClientSettingsValidator$1;->this$0:Lcom/squareup/api/ClientSettingsValidator;

    .line 86
    invoke-static {v3}, Lcom/squareup/api/ClientSettingsValidator;->access$200(Lcom/squareup/api/ClientSettingsValidator;)Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v2

    new-instance v3, Lcom/squareup/api/-$$Lambda$ClientSettingsValidator$1$cWVawkFSsL8UwpyN9jN_VCbB-LQ;

    invoke-direct {v3, p0, v0}, Lcom/squareup/api/-$$Lambda$ClientSettingsValidator$1$cWVawkFSsL8UwpyN9jN_VCbB-LQ;-><init>(Lcom/squareup/api/ClientSettingsValidator$1;Ljava/lang/String;)V

    .line 87
    invoke-virtual {v2, v3}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/api/ClientSettingsValidator$1;->this$0:Lcom/squareup/api/ClientSettingsValidator;

    .line 88
    invoke-static {v2}, Lcom/squareup/api/ClientSettingsValidator;->access$100(Lcom/squareup/api/ClientSettingsValidator;)Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v0, v2}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 90
    invoke-static {v0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Single;->toObservable()Lrx/Observable;

    move-result-object v0

    invoke-static {v1, v0}, Lrx/Observable;->concat(Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lrx/Observable;->first()Lrx/Observable;

    move-result-object v0

    .line 92
    invoke-static {p1}, Lcom/squareup/api/ClientSettingsValidator;->access$400(Ljava/lang/Object;)Lrx/functions/Func1;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 60
    :cond_0
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_SANDBOX_CLIENT_ID:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1

    .line 56
    :cond_1
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->MISSING_CLIENT_ID:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1
.end method

.method public synthetic lambda$call$1$ClientSettingsValidator$1(Ljava/lang/String;Lcom/squareup/server/api/ClientSettings;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 87
    iget-object v0, p0, Lcom/squareup/api/ClientSettingsValidator$1;->this$0:Lcom/squareup/api/ClientSettingsValidator;

    invoke-static {v0}, Lcom/squareup/api/ClientSettingsValidator;->access$000(Lcom/squareup/api/ClientSettingsValidator;)Lcom/squareup/api/ClientSettingsCache;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/squareup/api/ClientSettingsCache;->put(Ljava/lang/String;Lcom/squareup/server/api/ClientSettings;)V

    return-void
.end method
