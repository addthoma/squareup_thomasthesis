.class public final enum Lcom/squareup/api/items/ItemsVisibleTypeSet;
.super Ljava/lang/Enum;
.source "ItemsVisibleTypeSet.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/ItemsVisibleTypeSet$ProtoAdapter_ItemsVisibleTypeSet;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/items/ItemsVisibleTypeSet;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/items/ItemsVisibleTypeSet;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/ItemsVisibleTypeSet;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum LEGACY:Lcom/squareup/api/items/ItemsVisibleTypeSet;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 15
    new-instance v0, Lcom/squareup/api/items/ItemsVisibleTypeSet;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "LEGACY"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/api/items/ItemsVisibleTypeSet;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/ItemsVisibleTypeSet;->LEGACY:Lcom/squareup/api/items/ItemsVisibleTypeSet;

    new-array v0, v1, [Lcom/squareup/api/items/ItemsVisibleTypeSet;

    .line 10
    sget-object v1, Lcom/squareup/api/items/ItemsVisibleTypeSet;->LEGACY:Lcom/squareup/api/items/ItemsVisibleTypeSet;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/api/items/ItemsVisibleTypeSet;->$VALUES:[Lcom/squareup/api/items/ItemsVisibleTypeSet;

    .line 17
    new-instance v0, Lcom/squareup/api/items/ItemsVisibleTypeSet$ProtoAdapter_ItemsVisibleTypeSet;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemsVisibleTypeSet$ProtoAdapter_ItemsVisibleTypeSet;-><init>()V

    sput-object v0, Lcom/squareup/api/items/ItemsVisibleTypeSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    iput p3, p0, Lcom/squareup/api/items/ItemsVisibleTypeSet;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/items/ItemsVisibleTypeSet;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 30
    :cond_0
    sget-object p0, Lcom/squareup/api/items/ItemsVisibleTypeSet;->LEGACY:Lcom/squareup/api/items/ItemsVisibleTypeSet;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/items/ItemsVisibleTypeSet;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/api/items/ItemsVisibleTypeSet;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/items/ItemsVisibleTypeSet;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/items/ItemsVisibleTypeSet;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/api/items/ItemsVisibleTypeSet;->$VALUES:[Lcom/squareup/api/items/ItemsVisibleTypeSet;

    invoke-virtual {v0}, [Lcom/squareup/api/items/ItemsVisibleTypeSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/items/ItemsVisibleTypeSet;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 37
    iget v0, p0, Lcom/squareup/api/items/ItemsVisibleTypeSet;->value:I

    return v0
.end method
