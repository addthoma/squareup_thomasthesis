.class final Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$ProtoAdapter_AutoGratuitySettings;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Surcharge.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AutoGratuitySettings"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 487
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 506
    new-instance v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;-><init>()V

    .line 507
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 508
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 520
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 513
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    invoke-virtual {v0, v4}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;->auto_enable_type(Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;)Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 515
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 510
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;->minimum_seat_count(Ljava/lang/Integer;)Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;

    goto :goto_0

    .line 524
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 525
    invoke-virtual {v0}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;->build()Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 485
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$ProtoAdapter_AutoGratuitySettings;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 499
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->minimum_seat_count:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 500
    sget-object v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->auto_enable_type:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 501
    invoke-virtual {p2}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 485
    check-cast p2, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$ProtoAdapter_AutoGratuitySettings;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;)I
    .locals 4

    .line 492
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->minimum_seat_count:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->auto_enable_type:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    const/4 v3, 0x2

    .line 493
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 494
    invoke-virtual {p1}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 485
    check-cast p1, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    invoke-virtual {p0, p1}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$ProtoAdapter_AutoGratuitySettings;->encodedSize(Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;)Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;
    .locals 0

    .line 530
    invoke-virtual {p1}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->newBuilder()Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;

    move-result-object p1

    .line 531
    invoke-virtual {p1}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 532
    invoke-virtual {p1}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;->build()Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 485
    check-cast p1, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    invoke-virtual {p0, p1}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$ProtoAdapter_AutoGratuitySettings;->redact(Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;)Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    move-result-object p1

    return-object p1
.end method
