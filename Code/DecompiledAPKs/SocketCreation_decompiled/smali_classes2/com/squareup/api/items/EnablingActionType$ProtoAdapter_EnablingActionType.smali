.class final Lcom/squareup/api/items/EnablingActionType$ProtoAdapter_EnablingActionType;
.super Lcom/squareup/wire/EnumAdapter;
.source "EnablingActionType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/EnablingActionType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_EnablingActionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/api/items/EnablingActionType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 44
    const-class v0, Lcom/squareup/api/items/EnablingActionType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/api/items/EnablingActionType;
    .locals 0

    .line 49
    invoke-static {p1}, Lcom/squareup/api/items/EnablingActionType;->fromValue(I)Lcom/squareup/api/items/EnablingActionType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 42
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/EnablingActionType$ProtoAdapter_EnablingActionType;->fromValue(I)Lcom/squareup/api/items/EnablingActionType;

    move-result-object p1

    return-object p1
.end method
