.class public final Lcom/squareup/api/items/Configuration;
.super Lcom/squareup/wire/Message;
.source "Configuration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/Configuration$ProtoAdapter_Configuration;,
        Lcom/squareup/api/items/Configuration$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/Configuration;",
        "Lcom/squareup/api/items/Configuration$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/Configuration;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_PAGE_LAYOUT:Lcom/squareup/api/items/PageLayout;

.field private static final serialVersionUID:J


# instance fields
.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final page_layout:Lcom/squareup/api/items/PageLayout;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.PageLayout#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final void_reasons:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.VoidReason#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/VoidReason;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/api/items/Configuration$ProtoAdapter_Configuration;

    invoke-direct {v0}, Lcom/squareup/api/items/Configuration$ProtoAdapter_Configuration;-><init>()V

    sput-object v0, Lcom/squareup/api/items/Configuration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 38
    sget-object v0, Lcom/squareup/api/items/PageLayout;->UNKNOWN_PAGE_LAYOUT:Lcom/squareup/api/items/PageLayout;

    sput-object v0, Lcom/squareup/api/items/Configuration;->DEFAULT_PAGE_LAYOUT:Lcom/squareup/api/items/PageLayout;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/api/items/PageLayout;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/api/items/PageLayout;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/VoidReason;",
            ">;)V"
        }
    .end annotation

    .line 67
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/api/items/Configuration;-><init>(Ljava/lang/String;Lcom/squareup/api/items/PageLayout;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/api/items/PageLayout;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/api/items/PageLayout;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/VoidReason;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 72
    sget-object v0, Lcom/squareup/api/items/Configuration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 73
    iput-object p1, p0, Lcom/squareup/api/items/Configuration;->id:Ljava/lang/String;

    .line 74
    iput-object p2, p0, Lcom/squareup/api/items/Configuration;->page_layout:Lcom/squareup/api/items/PageLayout;

    const-string/jumbo p1, "void_reasons"

    .line 75
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/api/items/Configuration;->void_reasons:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 91
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/Configuration;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 92
    :cond_1
    check-cast p1, Lcom/squareup/api/items/Configuration;

    .line 93
    invoke-virtual {p0}, Lcom/squareup/api/items/Configuration;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/Configuration;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Configuration;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Configuration;->id:Ljava/lang/String;

    .line 94
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Configuration;->page_layout:Lcom/squareup/api/items/PageLayout;

    iget-object v3, p1, Lcom/squareup/api/items/Configuration;->page_layout:Lcom/squareup/api/items/PageLayout;

    .line 95
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Configuration;->void_reasons:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/api/items/Configuration;->void_reasons:Ljava/util/List;

    .line 96
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 101
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 103
    invoke-virtual {p0}, Lcom/squareup/api/items/Configuration;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 104
    iget-object v1, p0, Lcom/squareup/api/items/Configuration;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 105
    iget-object v1, p0, Lcom/squareup/api/items/Configuration;->page_layout:Lcom/squareup/api/items/PageLayout;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/api/items/PageLayout;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 106
    iget-object v1, p0, Lcom/squareup/api/items/Configuration;->void_reasons:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 107
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/Configuration$Builder;
    .locals 2

    .line 80
    new-instance v0, Lcom/squareup/api/items/Configuration$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Configuration$Builder;-><init>()V

    .line 81
    iget-object v1, p0, Lcom/squareup/api/items/Configuration;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Configuration$Builder;->id:Ljava/lang/String;

    .line 82
    iget-object v1, p0, Lcom/squareup/api/items/Configuration;->page_layout:Lcom/squareup/api/items/PageLayout;

    iput-object v1, v0, Lcom/squareup/api/items/Configuration$Builder;->page_layout:Lcom/squareup/api/items/PageLayout;

    .line 83
    iget-object v1, p0, Lcom/squareup/api/items/Configuration;->void_reasons:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/api/items/Configuration$Builder;->void_reasons:Ljava/util/List;

    .line 84
    invoke-virtual {p0}, Lcom/squareup/api/items/Configuration;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Configuration$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/squareup/api/items/Configuration;->newBuilder()Lcom/squareup/api/items/Configuration$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 115
    iget-object v1, p0, Lcom/squareup/api/items/Configuration;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Configuration;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/Configuration;->page_layout:Lcom/squareup/api/items/PageLayout;

    if-eqz v1, :cond_1

    const-string v1, ", page_layout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Configuration;->page_layout:Lcom/squareup/api/items/PageLayout;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 117
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/Configuration;->void_reasons:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", void_reasons="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Configuration;->void_reasons:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Configuration{"

    .line 118
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
