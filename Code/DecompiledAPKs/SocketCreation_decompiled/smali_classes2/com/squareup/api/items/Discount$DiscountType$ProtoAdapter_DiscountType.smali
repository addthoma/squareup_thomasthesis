.class final Lcom/squareup/api/items/Discount$DiscountType$ProtoAdapter_DiscountType;
.super Lcom/squareup/wire/EnumAdapter;
.source "Discount.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Discount$DiscountType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_DiscountType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/api/items/Discount$DiscountType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 473
    const-class v0, Lcom/squareup/api/items/Discount$DiscountType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/api/items/Discount$DiscountType;
    .locals 0

    .line 478
    invoke-static {p1}, Lcom/squareup/api/items/Discount$DiscountType;->fromValue(I)Lcom/squareup/api/items/Discount$DiscountType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 471
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/Discount$DiscountType$ProtoAdapter_DiscountType;->fromValue(I)Lcom/squareup/api/items/Discount$DiscountType;

    move-result-object p1

    return-object p1
.end method
