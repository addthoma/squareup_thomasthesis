.class public final Lcom/squareup/api/items/AdditionalItemImage$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AdditionalItemImage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/AdditionalItemImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/AdditionalItemImage;",
        "Lcom/squareup/api/items/AdditionalItemImage$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public caption:Ljava/lang/String;

.field public center_point:Lcom/squareup/api/items/Point;

.field public id:Ljava/lang/String;

.field public item:Lcom/squareup/api/sync/ObjectId;

.field public item_variation:Lcom/squareup/api/sync/ObjectId;

.field public ordinal:Ljava/lang/Integer;

.field public url:Ljava/lang/String;

.field public zoom_factor:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 203
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/AdditionalItemImage;
    .locals 11

    .line 268
    new-instance v10, Lcom/squareup/api/items/AdditionalItemImage;

    iget-object v1, p0, Lcom/squareup/api/items/AdditionalItemImage$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/AdditionalItemImage$Builder;->url:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/api/items/AdditionalItemImage$Builder;->item:Lcom/squareup/api/sync/ObjectId;

    iget-object v4, p0, Lcom/squareup/api/items/AdditionalItemImage$Builder;->item_variation:Lcom/squareup/api/sync/ObjectId;

    iget-object v5, p0, Lcom/squareup/api/items/AdditionalItemImage$Builder;->caption:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/api/items/AdditionalItemImage$Builder;->center_point:Lcom/squareup/api/items/Point;

    iget-object v7, p0, Lcom/squareup/api/items/AdditionalItemImage$Builder;->zoom_factor:Ljava/lang/Integer;

    iget-object v8, p0, Lcom/squareup/api/items/AdditionalItemImage$Builder;->ordinal:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/api/items/AdditionalItemImage;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/sync/ObjectId;Ljava/lang/String;Lcom/squareup/api/items/Point;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 186
    invoke-virtual {p0}, Lcom/squareup/api/items/AdditionalItemImage$Builder;->build()Lcom/squareup/api/items/AdditionalItemImage;

    move-result-object v0

    return-object v0
.end method

.method public caption(Ljava/lang/String;)Lcom/squareup/api/items/AdditionalItemImage$Builder;
    .locals 0

    .line 237
    iput-object p1, p0, Lcom/squareup/api/items/AdditionalItemImage$Builder;->caption:Ljava/lang/String;

    return-object p0
.end method

.method public center_point(Lcom/squareup/api/items/Point;)Lcom/squareup/api/items/AdditionalItemImage$Builder;
    .locals 0

    .line 246
    iput-object p1, p0, Lcom/squareup/api/items/AdditionalItemImage$Builder;->center_point:Lcom/squareup/api/items/Point;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/AdditionalItemImage$Builder;
    .locals 0

    .line 207
    iput-object p1, p0, Lcom/squareup/api/items/AdditionalItemImage$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public item(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/AdditionalItemImage$Builder;
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/api/items/AdditionalItemImage$Builder;->item:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public item_variation(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/AdditionalItemImage$Builder;
    .locals 0

    .line 229
    iput-object p1, p0, Lcom/squareup/api/items/AdditionalItemImage$Builder;->item_variation:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/AdditionalItemImage$Builder;
    .locals 0

    .line 262
    iput-object p1, p0, Lcom/squareup/api/items/AdditionalItemImage$Builder;->ordinal:Ljava/lang/Integer;

    return-object p0
.end method

.method public url(Ljava/lang/String;)Lcom/squareup/api/items/AdditionalItemImage$Builder;
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/api/items/AdditionalItemImage$Builder;->url:Ljava/lang/String;

    return-object p0
.end method

.method public zoom_factor(Ljava/lang/Integer;)Lcom/squareup/api/items/AdditionalItemImage$Builder;
    .locals 0

    .line 254
    iput-object p1, p0, Lcom/squareup/api/items/AdditionalItemImage$Builder;->zoom_factor:Ljava/lang/Integer;

    return-object p0
.end method
