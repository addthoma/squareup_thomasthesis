.class public final Lcom/squareup/api/items/ItemModifierOptionOverride;
.super Lcom/squareup/wire/Message;
.source "ItemModifierOptionOverride.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/ItemModifierOptionOverride$ProtoAdapter_ItemModifierOptionOverride;,
        Lcom/squareup/api/items/ItemModifierOptionOverride$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/ItemModifierOptionOverride;",
        "Lcom/squareup/api/items/ItemModifierOptionOverride$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/ItemModifierOptionOverride;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ON_BY_DEFAULT:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final on_by_default:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/api/items/ItemModifierOptionOverride$ProtoAdapter_ItemModifierOptionOverride;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemModifierOptionOverride$ProtoAdapter_ItemModifierOptionOverride;-><init>()V

    sput-object v0, Lcom/squareup/api/items/ItemModifierOptionOverride;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 28
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/ItemModifierOptionOverride;->DEFAULT_ON_BY_DEFAULT:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;)V
    .locals 1

    .line 37
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/api/items/ItemModifierOptionOverride;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/api/items/ItemModifierOptionOverride;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 42
    iput-object p1, p0, Lcom/squareup/api/items/ItemModifierOptionOverride;->on_by_default:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 56
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/ItemModifierOptionOverride;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 57
    :cond_1
    check-cast p1, Lcom/squareup/api/items/ItemModifierOptionOverride;

    .line 58
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemModifierOptionOverride;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/ItemModifierOptionOverride;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOptionOverride;->on_by_default:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/api/items/ItemModifierOptionOverride;->on_by_default:Ljava/lang/Boolean;

    .line 59
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 64
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 66
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemModifierOptionOverride;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 67
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOptionOverride;->on_by_default:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 68
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/ItemModifierOptionOverride$Builder;
    .locals 2

    .line 47
    new-instance v0, Lcom/squareup/api/items/ItemModifierOptionOverride$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemModifierOptionOverride$Builder;-><init>()V

    .line 48
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOptionOverride;->on_by_default:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/ItemModifierOptionOverride$Builder;->on_by_default:Ljava/lang/Boolean;

    .line 49
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemModifierOptionOverride;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemModifierOptionOverride$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemModifierOptionOverride;->newBuilder()Lcom/squareup/api/items/ItemModifierOptionOverride$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 76
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOptionOverride;->on_by_default:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", on_by_default="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOptionOverride;->on_by_default:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ItemModifierOptionOverride{"

    .line 77
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
