.class public final enum Lcom/squareup/api/items/PricingRule$ExcludeStrategy;
.super Ljava/lang/Enum;
.source "PricingRule.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/PricingRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ExcludeStrategy"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/PricingRule$ExcludeStrategy$ProtoAdapter_ExcludeStrategy;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/items/PricingRule$ExcludeStrategy;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/PricingRule$ExcludeStrategy;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum LEAST_EXPENSIVE:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

.field public static final enum MOST_EXPENSIVE:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 557
    new-instance v0, Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "LEAST_EXPENSIVE"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/api/items/PricingRule$ExcludeStrategy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/PricingRule$ExcludeStrategy;->LEAST_EXPENSIVE:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    .line 563
    new-instance v0, Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    const/4 v3, 0x2

    const-string v4, "MOST_EXPENSIVE"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/api/items/PricingRule$ExcludeStrategy;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/PricingRule$ExcludeStrategy;->MOST_EXPENSIVE:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    new-array v0, v3, [Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    .line 552
    sget-object v3, Lcom/squareup/api/items/PricingRule$ExcludeStrategy;->LEAST_EXPENSIVE:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/api/items/PricingRule$ExcludeStrategy;->MOST_EXPENSIVE:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/api/items/PricingRule$ExcludeStrategy;->$VALUES:[Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    .line 565
    new-instance v0, Lcom/squareup/api/items/PricingRule$ExcludeStrategy$ProtoAdapter_ExcludeStrategy;

    invoke-direct {v0}, Lcom/squareup/api/items/PricingRule$ExcludeStrategy$ProtoAdapter_ExcludeStrategy;-><init>()V

    sput-object v0, Lcom/squareup/api/items/PricingRule$ExcludeStrategy;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 569
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 570
    iput p3, p0, Lcom/squareup/api/items/PricingRule$ExcludeStrategy;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/items/PricingRule$ExcludeStrategy;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 579
    :cond_0
    sget-object p0, Lcom/squareup/api/items/PricingRule$ExcludeStrategy;->MOST_EXPENSIVE:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    return-object p0

    .line 578
    :cond_1
    sget-object p0, Lcom/squareup/api/items/PricingRule$ExcludeStrategy;->LEAST_EXPENSIVE:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/items/PricingRule$ExcludeStrategy;
    .locals 1

    .line 552
    const-class v0, Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/items/PricingRule$ExcludeStrategy;
    .locals 1

    .line 552
    sget-object v0, Lcom/squareup/api/items/PricingRule$ExcludeStrategy;->$VALUES:[Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    invoke-virtual {v0}, [Lcom/squareup/api/items/PricingRule$ExcludeStrategy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 586
    iget v0, p0, Lcom/squareup/api/items/PricingRule$ExcludeStrategy;->value:I

    return v0
.end method
