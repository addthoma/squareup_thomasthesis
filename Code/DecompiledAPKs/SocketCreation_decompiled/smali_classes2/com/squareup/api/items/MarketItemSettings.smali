.class public final Lcom/squareup/api/items/MarketItemSettings;
.super Lcom/squareup/wire/Message;
.source "MarketItemSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/MarketItemSettings$ProtoAdapter_MarketItemSettings;,
        Lcom/squareup/api/items/MarketItemSettings$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/MarketItemSettings;",
        "Lcom/squareup/api/items/MarketItemSettings$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/MarketItemSettings;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_IS_IN_STOCK:Ljava/lang/Boolean;

.field public static final DEFAULT_IS_UNIQUE:Ljava/lang/Boolean;

.field public static final DEFAULT_SLUG:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final is_in_stock:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final is_unique:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final item:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final shipping_cost:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final slug:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/api/items/MarketItemSettings$ProtoAdapter_MarketItemSettings;

    invoke-direct {v0}, Lcom/squareup/api/items/MarketItemSettings$ProtoAdapter_MarketItemSettings;-><init>()V

    sput-object v0, Lcom/squareup/api/items/MarketItemSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 31
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/MarketItemSettings;->DEFAULT_IS_UNIQUE:Ljava/lang/Boolean;

    .line 33
    sput-object v0, Lcom/squareup/api/items/MarketItemSettings;->DEFAULT_IS_IN_STOCK:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;Lcom/squareup/api/sync/ObjectId;)V
    .locals 8

    .line 73
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/api/items/MarketItemSettings;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;Lcom/squareup/api/sync/ObjectId;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;Lcom/squareup/api/sync/ObjectId;Lokio/ByteString;)V
    .locals 1

    .line 78
    sget-object v0, Lcom/squareup/api/items/MarketItemSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 79
    iput-object p1, p0, Lcom/squareup/api/items/MarketItemSettings;->id:Ljava/lang/String;

    .line 80
    iput-object p2, p0, Lcom/squareup/api/items/MarketItemSettings;->slug:Ljava/lang/String;

    .line 81
    iput-object p3, p0, Lcom/squareup/api/items/MarketItemSettings;->is_unique:Ljava/lang/Boolean;

    .line 82
    iput-object p4, p0, Lcom/squareup/api/items/MarketItemSettings;->is_in_stock:Ljava/lang/Boolean;

    .line 83
    iput-object p5, p0, Lcom/squareup/api/items/MarketItemSettings;->shipping_cost:Lcom/squareup/protos/common/Money;

    .line 84
    iput-object p6, p0, Lcom/squareup/api/items/MarketItemSettings;->item:Lcom/squareup/api/sync/ObjectId;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 103
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/MarketItemSettings;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 104
    :cond_1
    check-cast p1, Lcom/squareup/api/items/MarketItemSettings;

    .line 105
    invoke-virtual {p0}, Lcom/squareup/api/items/MarketItemSettings;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/MarketItemSettings;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/MarketItemSettings;->id:Ljava/lang/String;

    .line 106
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->slug:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/MarketItemSettings;->slug:Ljava/lang/String;

    .line 107
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->is_unique:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/items/MarketItemSettings;->is_unique:Ljava/lang/Boolean;

    .line 108
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->is_in_stock:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/items/MarketItemSettings;->is_in_stock:Ljava/lang/Boolean;

    .line 109
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->shipping_cost:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/api/items/MarketItemSettings;->shipping_cost:Lcom/squareup/protos/common/Money;

    .line 110
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->item:Lcom/squareup/api/sync/ObjectId;

    iget-object p1, p1, Lcom/squareup/api/items/MarketItemSettings;->item:Lcom/squareup/api/sync/ObjectId;

    .line 111
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 116
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 118
    invoke-virtual {p0}, Lcom/squareup/api/items/MarketItemSettings;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 119
    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 120
    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->slug:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 121
    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->is_unique:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 122
    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->is_in_stock:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 123
    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->shipping_cost:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 124
    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->item:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 125
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/MarketItemSettings$Builder;
    .locals 2

    .line 89
    new-instance v0, Lcom/squareup/api/items/MarketItemSettings$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/MarketItemSettings$Builder;-><init>()V

    .line 90
    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/MarketItemSettings$Builder;->id:Ljava/lang/String;

    .line 91
    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->slug:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/MarketItemSettings$Builder;->slug:Ljava/lang/String;

    .line 92
    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->is_unique:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/MarketItemSettings$Builder;->is_unique:Ljava/lang/Boolean;

    .line 93
    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->is_in_stock:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/MarketItemSettings$Builder;->is_in_stock:Ljava/lang/Boolean;

    .line 94
    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->shipping_cost:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/api/items/MarketItemSettings$Builder;->shipping_cost:Lcom/squareup/protos/common/Money;

    .line 95
    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->item:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/api/items/MarketItemSettings$Builder;->item:Lcom/squareup/api/sync/ObjectId;

    .line 96
    invoke-virtual {p0}, Lcom/squareup/api/items/MarketItemSettings;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/MarketItemSettings$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/api/items/MarketItemSettings;->newBuilder()Lcom/squareup/api/items/MarketItemSettings$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 133
    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->slug:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", slug="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->slug:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->is_unique:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", is_unique="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->is_unique:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 136
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->is_in_stock:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", is_in_stock="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->is_in_stock:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 137
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->shipping_cost:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", shipping_cost="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->shipping_cost:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 138
    :cond_4
    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->item:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_5

    const-string v1, ", item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings;->item:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "MarketItemSettings{"

    .line 139
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
