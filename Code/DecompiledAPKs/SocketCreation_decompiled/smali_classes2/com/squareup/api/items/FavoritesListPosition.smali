.class public final Lcom/squareup/api/items/FavoritesListPosition;
.super Lcom/squareup/wire/Message;
.source "FavoritesListPosition.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/FavoritesListPosition$ProtoAdapter_FavoritesListPosition;,
        Lcom/squareup/api/items/FavoritesListPosition$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/FavoritesListPosition;",
        "Lcom/squareup/api/items/FavoritesListPosition$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/FavoritesListPosition;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_ORDINAL:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final favorite_object:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final ordinal:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/api/items/FavoritesListPosition$ProtoAdapter_FavoritesListPosition;

    invoke-direct {v0}, Lcom/squareup/api/items/FavoritesListPosition$ProtoAdapter_FavoritesListPosition;-><init>()V

    sput-object v0, Lcom/squareup/api/items/FavoritesListPosition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 28
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/FavoritesListPosition;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Ljava/lang/Integer;)V
    .locals 1

    .line 57
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/api/items/FavoritesListPosition;-><init>(Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 62
    sget-object v0, Lcom/squareup/api/items/FavoritesListPosition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 63
    iput-object p1, p0, Lcom/squareup/api/items/FavoritesListPosition;->id:Ljava/lang/String;

    .line 64
    iput-object p2, p0, Lcom/squareup/api/items/FavoritesListPosition;->favorite_object:Lcom/squareup/api/sync/ObjectId;

    .line 65
    iput-object p3, p0, Lcom/squareup/api/items/FavoritesListPosition;->ordinal:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 81
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/FavoritesListPosition;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 82
    :cond_1
    check-cast p1, Lcom/squareup/api/items/FavoritesListPosition;

    .line 83
    invoke-virtual {p0}, Lcom/squareup/api/items/FavoritesListPosition;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/FavoritesListPosition;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/FavoritesListPosition;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/FavoritesListPosition;->id:Ljava/lang/String;

    .line 84
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/FavoritesListPosition;->favorite_object:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p1, Lcom/squareup/api/items/FavoritesListPosition;->favorite_object:Lcom/squareup/api/sync/ObjectId;

    .line 85
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/FavoritesListPosition;->ordinal:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/api/items/FavoritesListPosition;->ordinal:Ljava/lang/Integer;

    .line 86
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 91
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 93
    invoke-virtual {p0}, Lcom/squareup/api/items/FavoritesListPosition;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 94
    iget-object v1, p0, Lcom/squareup/api/items/FavoritesListPosition;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 95
    iget-object v1, p0, Lcom/squareup/api/items/FavoritesListPosition;->favorite_object:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 96
    iget-object v1, p0, Lcom/squareup/api/items/FavoritesListPosition;->ordinal:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 97
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/FavoritesListPosition$Builder;
    .locals 2

    .line 70
    new-instance v0, Lcom/squareup/api/items/FavoritesListPosition$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/FavoritesListPosition$Builder;-><init>()V

    .line 71
    iget-object v1, p0, Lcom/squareup/api/items/FavoritesListPosition;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/FavoritesListPosition$Builder;->id:Ljava/lang/String;

    .line 72
    iget-object v1, p0, Lcom/squareup/api/items/FavoritesListPosition;->favorite_object:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/api/items/FavoritesListPosition$Builder;->favorite_object:Lcom/squareup/api/sync/ObjectId;

    .line 73
    iget-object v1, p0, Lcom/squareup/api/items/FavoritesListPosition;->ordinal:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/items/FavoritesListPosition$Builder;->ordinal:Ljava/lang/Integer;

    .line 74
    invoke-virtual {p0}, Lcom/squareup/api/items/FavoritesListPosition;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/FavoritesListPosition$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/api/items/FavoritesListPosition;->newBuilder()Lcom/squareup/api/items/FavoritesListPosition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 105
    iget-object v1, p0, Lcom/squareup/api/items/FavoritesListPosition;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/FavoritesListPosition;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/FavoritesListPosition;->favorite_object:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_1

    const-string v1, ", favorite_object="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/FavoritesListPosition;->favorite_object:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 107
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/FavoritesListPosition;->ordinal:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", ordinal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/FavoritesListPosition;->ordinal:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FavoritesListPosition{"

    .line 108
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
