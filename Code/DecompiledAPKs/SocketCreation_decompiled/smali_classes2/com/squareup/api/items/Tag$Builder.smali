.class public final Lcom/squareup/api/items/Tag$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Tag.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Tag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/Tag;",
        "Lcom/squareup/api/items/Tag$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public color:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public tag_membership:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation
.end field

.field public type:Lcom/squareup/api/items/Tag$Type;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 155
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 156
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/items/Tag$Builder;->tag_membership:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/Tag;
    .locals 8

    .line 194
    new-instance v7, Lcom/squareup/api/items/Tag;

    iget-object v1, p0, Lcom/squareup/api/items/Tag$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/Tag$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/api/items/Tag$Builder;->type:Lcom/squareup/api/items/Tag$Type;

    iget-object v4, p0, Lcom/squareup/api/items/Tag$Builder;->tag_membership:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/api/items/Tag$Builder;->color:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/api/items/Tag;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Tag$Type;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 144
    invoke-virtual {p0}, Lcom/squareup/api/items/Tag$Builder;->build()Lcom/squareup/api/items/Tag;

    move-result-object v0

    return-object v0
.end method

.method public color(Ljava/lang/String;)Lcom/squareup/api/items/Tag$Builder;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/api/items/Tag$Builder;->color:Ljava/lang/String;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/Tag$Builder;
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/api/items/Tag$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/api/items/Tag$Builder;
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/squareup/api/items/Tag$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public tag_membership(Ljava/util/List;)Lcom/squareup/api/items/Tag$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;)",
            "Lcom/squareup/api/items/Tag$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 179
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 180
    iput-object p1, p0, Lcom/squareup/api/items/Tag$Builder;->tag_membership:Ljava/util/List;

    return-object p0
.end method

.method public type(Lcom/squareup/api/items/Tag$Type;)Lcom/squareup/api/items/Tag$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/api/items/Tag$Builder;->type:Lcom/squareup/api/items/Tag$Type;

    return-object p0
.end method
