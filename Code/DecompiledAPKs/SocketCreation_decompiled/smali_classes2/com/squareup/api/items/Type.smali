.class public final enum Lcom/squareup/api/items/Type;
.super Ljava/lang/Enum;
.source "Type.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/Type$ProtoAdapter_Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/items/Type;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/items/Type;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/Type;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ADDITIONAL_ITEM_IMAGE:Lcom/squareup/api/items/Type;

.field public static final enum CONFIGURATION:Lcom/squareup/api/items/Type;

.field public static final enum DINING_OPTION:Lcom/squareup/api/items/Type;

.field public static final enum DISCOUNT:Lcom/squareup/api/items/Type;

.field public static final enum FAVORITES_LIST_POSITION:Lcom/squareup/api/items/Type;

.field public static final enum FEE:Lcom/squareup/api/items/Type;

.field public static final enum FLOOR_PLAN:Lcom/squareup/api/items/Type;

.field public static final enum FLOOR_PLAN_TILE:Lcom/squareup/api/items/Type;

.field public static final enum INVENTORY_INFO:Lcom/squareup/api/items/Type;

.field public static final enum ITEM:Lcom/squareup/api/items/Type;

.field public static final enum ITEM_FEE_MEMBERSHIP:Lcom/squareup/api/items/Type;

.field public static final enum ITEM_IMAGE:Lcom/squareup/api/items/Type;

.field public static final enum ITEM_ITEM_MODIFIER_LIST_MEMBERSHIP:Lcom/squareup/api/items/Type;

.field public static final enum ITEM_MODIFIER_LIST:Lcom/squareup/api/items/Type;

.field public static final enum ITEM_MODIFIER_OPTION:Lcom/squareup/api/items/Type;

.field public static final enum ITEM_PAGE_MEMBERSHIP:Lcom/squareup/api/items/Type;

.field public static final enum ITEM_VARIATION:Lcom/squareup/api/items/Type;

.field public static final enum MARKET_ITEM_SETTINGS:Lcom/squareup/api/items/Type;

.field public static final enum MENU:Lcom/squareup/api/items/Type;

.field public static final enum MENU_CATEGORY:Lcom/squareup/api/items/Type;

.field public static final enum MENU_GROUP_MEMBERSHIP:Lcom/squareup/api/items/Type;

.field public static final enum OBSOLETE_TENDER_FEE:Lcom/squareup/api/items/Type;

.field public static final enum PAGE:Lcom/squareup/api/items/Type;

.field public static final enum PLACEHOLDER:Lcom/squareup/api/items/Type;

.field public static final enum PRICING_RULE:Lcom/squareup/api/items/Type;

.field public static final enum PRODUCT_SET:Lcom/squareup/api/items/Type;

.field public static final enum PROMO:Lcom/squareup/api/items/Type;

.field public static final enum SURCHARGE:Lcom/squareup/api/items/Type;

.field public static final enum SURCHARGE_FEE_MEMBERSHIP:Lcom/squareup/api/items/Type;

.field public static final enum TAG:Lcom/squareup/api/items/Type;

.field public static final enum TAX_RULE:Lcom/squareup/api/items/Type;

.field public static final enum TICKET_GROUP:Lcom/squareup/api/items/Type;

.field public static final enum TICKET_TEMPLATE:Lcom/squareup/api/items/Type;

.field public static final enum TIME_PERIOD:Lcom/squareup/api/items/Type;

.field public static final enum VOID_REASON:Lcom/squareup/api/items/Type;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 11
    new-instance v0, Lcom/squareup/api/items/Type;

    const/4 v1, 0x0

    const-string v2, "ITEM"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->ITEM:Lcom/squareup/api/items/Type;

    .line 13
    new-instance v0, Lcom/squareup/api/items/Type;

    const/4 v2, 0x1

    const-string v3, "PAGE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->PAGE:Lcom/squareup/api/items/Type;

    .line 15
    new-instance v0, Lcom/squareup/api/items/Type;

    const/4 v3, 0x2

    const-string v4, "ITEM_IMAGE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->ITEM_IMAGE:Lcom/squareup/api/items/Type;

    .line 17
    new-instance v0, Lcom/squareup/api/items/Type;

    const/4 v4, 0x3

    const-string v5, "ITEM_PAGE_MEMBERSHIP"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->ITEM_PAGE_MEMBERSHIP:Lcom/squareup/api/items/Type;

    .line 19
    new-instance v0, Lcom/squareup/api/items/Type;

    const/4 v5, 0x4

    const-string v6, "MENU_CATEGORY"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->MENU_CATEGORY:Lcom/squareup/api/items/Type;

    .line 21
    new-instance v0, Lcom/squareup/api/items/Type;

    const/4 v6, 0x5

    const-string v7, "ITEM_VARIATION"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->ITEM_VARIATION:Lcom/squareup/api/items/Type;

    .line 23
    new-instance v0, Lcom/squareup/api/items/Type;

    const/4 v7, 0x6

    const-string v8, "FEE"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->FEE:Lcom/squareup/api/items/Type;

    .line 25
    new-instance v0, Lcom/squareup/api/items/Type;

    const/4 v8, 0x7

    const-string v9, "PLACEHOLDER"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->PLACEHOLDER:Lcom/squareup/api/items/Type;

    .line 27
    new-instance v0, Lcom/squareup/api/items/Type;

    const/16 v9, 0x8

    const-string v10, "DISCOUNT"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->DISCOUNT:Lcom/squareup/api/items/Type;

    .line 29
    new-instance v0, Lcom/squareup/api/items/Type;

    const/16 v10, 0x9

    const-string v11, "ITEM_FEE_MEMBERSHIP"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->ITEM_FEE_MEMBERSHIP:Lcom/squareup/api/items/Type;

    .line 31
    new-instance v0, Lcom/squareup/api/items/Type;

    const/16 v11, 0xa

    const-string v12, "ITEM_MODIFIER_LIST"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->ITEM_MODIFIER_LIST:Lcom/squareup/api/items/Type;

    .line 33
    new-instance v0, Lcom/squareup/api/items/Type;

    const/16 v12, 0xb

    const-string v13, "ITEM_ITEM_MODIFIER_LIST_MEMBERSHIP"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->ITEM_ITEM_MODIFIER_LIST_MEMBERSHIP:Lcom/squareup/api/items/Type;

    .line 35
    new-instance v0, Lcom/squareup/api/items/Type;

    const/16 v13, 0xc

    const-string v14, "ITEM_MODIFIER_OPTION"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->ITEM_MODIFIER_OPTION:Lcom/squareup/api/items/Type;

    .line 37
    new-instance v0, Lcom/squareup/api/items/Type;

    const/16 v14, 0xd

    const-string v15, "MARKET_ITEM_SETTINGS"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->MARKET_ITEM_SETTINGS:Lcom/squareup/api/items/Type;

    .line 39
    new-instance v0, Lcom/squareup/api/items/Type;

    const/16 v15, 0xe

    const-string v14, "ADDITIONAL_ITEM_IMAGE"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->ADDITIONAL_ITEM_IMAGE:Lcom/squareup/api/items/Type;

    .line 41
    new-instance v0, Lcom/squareup/api/items/Type;

    const-string v14, "PROMO"

    const/16 v15, 0xf

    const/16 v13, 0xf

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->PROMO:Lcom/squareup/api/items/Type;

    .line 43
    new-instance v0, Lcom/squareup/api/items/Type;

    const-string v13, "INVENTORY_INFO"

    const/16 v14, 0x10

    const/16 v15, 0x10

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->INVENTORY_INFO:Lcom/squareup/api/items/Type;

    .line 45
    new-instance v0, Lcom/squareup/api/items/Type;

    const-string v13, "OBSOLETE_TENDER_FEE"

    const/16 v14, 0x11

    const/16 v15, 0x11

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->OBSOLETE_TENDER_FEE:Lcom/squareup/api/items/Type;

    .line 47
    new-instance v0, Lcom/squareup/api/items/Type;

    const-string v13, "DINING_OPTION"

    const/16 v14, 0x12

    const/16 v15, 0x12

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->DINING_OPTION:Lcom/squareup/api/items/Type;

    .line 49
    new-instance v0, Lcom/squareup/api/items/Type;

    const-string v13, "TAX_RULE"

    const/16 v14, 0x13

    const/16 v15, 0x13

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->TAX_RULE:Lcom/squareup/api/items/Type;

    .line 51
    new-instance v0, Lcom/squareup/api/items/Type;

    const-string v13, "CONFIGURATION"

    const/16 v14, 0x14

    const/16 v15, 0x14

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->CONFIGURATION:Lcom/squareup/api/items/Type;

    .line 53
    new-instance v0, Lcom/squareup/api/items/Type;

    const-string v13, "TICKET_GROUP"

    const/16 v14, 0x15

    const/16 v15, 0x15

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->TICKET_GROUP:Lcom/squareup/api/items/Type;

    .line 55
    new-instance v0, Lcom/squareup/api/items/Type;

    const-string v13, "TICKET_TEMPLATE"

    const/16 v14, 0x16

    const/16 v15, 0x16

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->TICKET_TEMPLATE:Lcom/squareup/api/items/Type;

    .line 57
    new-instance v0, Lcom/squareup/api/items/Type;

    const-string v13, "VOID_REASON"

    const/16 v14, 0x17

    const/16 v15, 0x17

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->VOID_REASON:Lcom/squareup/api/items/Type;

    .line 59
    new-instance v0, Lcom/squareup/api/items/Type;

    const-string v13, "MENU"

    const/16 v14, 0x18

    const/16 v15, 0x18

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->MENU:Lcom/squareup/api/items/Type;

    .line 61
    new-instance v0, Lcom/squareup/api/items/Type;

    const-string v13, "TAG"

    const/16 v14, 0x19

    const/16 v15, 0x19

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->TAG:Lcom/squareup/api/items/Type;

    .line 63
    new-instance v0, Lcom/squareup/api/items/Type;

    const-string v13, "FLOOR_PLAN"

    const/16 v14, 0x1a

    const/16 v15, 0x1a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->FLOOR_PLAN:Lcom/squareup/api/items/Type;

    .line 65
    new-instance v0, Lcom/squareup/api/items/Type;

    const-string v13, "FLOOR_PLAN_TILE"

    const/16 v14, 0x1b

    const/16 v15, 0x1b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->FLOOR_PLAN_TILE:Lcom/squareup/api/items/Type;

    .line 67
    new-instance v0, Lcom/squareup/api/items/Type;

    const-string v13, "FAVORITES_LIST_POSITION"

    const/16 v14, 0x1c

    const/16 v15, 0x1c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->FAVORITES_LIST_POSITION:Lcom/squareup/api/items/Type;

    .line 69
    new-instance v0, Lcom/squareup/api/items/Type;

    const-string v13, "MENU_GROUP_MEMBERSHIP"

    const/16 v14, 0x1d

    const/16 v15, 0x1d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->MENU_GROUP_MEMBERSHIP:Lcom/squareup/api/items/Type;

    .line 71
    new-instance v0, Lcom/squareup/api/items/Type;

    const-string v13, "SURCHARGE"

    const/16 v14, 0x1e

    const/16 v15, 0x1e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->SURCHARGE:Lcom/squareup/api/items/Type;

    .line 73
    new-instance v0, Lcom/squareup/api/items/Type;

    const-string v13, "PRICING_RULE"

    const/16 v14, 0x1f

    const/16 v15, 0x1f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->PRICING_RULE:Lcom/squareup/api/items/Type;

    .line 75
    new-instance v0, Lcom/squareup/api/items/Type;

    const-string v13, "PRODUCT_SET"

    const/16 v14, 0x20

    const/16 v15, 0x20

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->PRODUCT_SET:Lcom/squareup/api/items/Type;

    .line 77
    new-instance v0, Lcom/squareup/api/items/Type;

    const-string v13, "TIME_PERIOD"

    const/16 v14, 0x21

    const/16 v15, 0x21

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->TIME_PERIOD:Lcom/squareup/api/items/Type;

    .line 79
    new-instance v0, Lcom/squareup/api/items/Type;

    const-string v13, "SURCHARGE_FEE_MEMBERSHIP"

    const/16 v14, 0x22

    const/16 v15, 0x22

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Type;->SURCHARGE_FEE_MEMBERSHIP:Lcom/squareup/api/items/Type;

    const/16 v0, 0x23

    new-array v0, v0, [Lcom/squareup/api/items/Type;

    .line 10
    sget-object v13, Lcom/squareup/api/items/Type;->ITEM:Lcom/squareup/api/items/Type;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/api/items/Type;->PAGE:Lcom/squareup/api/items/Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->ITEM_IMAGE:Lcom/squareup/api/items/Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/api/items/Type;->ITEM_PAGE_MEMBERSHIP:Lcom/squareup/api/items/Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/api/items/Type;->MENU_CATEGORY:Lcom/squareup/api/items/Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/api/items/Type;->ITEM_VARIATION:Lcom/squareup/api/items/Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/api/items/Type;->FEE:Lcom/squareup/api/items/Type;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/api/items/Type;->PLACEHOLDER:Lcom/squareup/api/items/Type;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/api/items/Type;->DISCOUNT:Lcom/squareup/api/items/Type;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/api/items/Type;->ITEM_FEE_MEMBERSHIP:Lcom/squareup/api/items/Type;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/api/items/Type;->ITEM_MODIFIER_LIST:Lcom/squareup/api/items/Type;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/api/items/Type;->ITEM_ITEM_MODIFIER_LIST_MEMBERSHIP:Lcom/squareup/api/items/Type;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/api/items/Type;->ITEM_MODIFIER_OPTION:Lcom/squareup/api/items/Type;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->MARKET_ITEM_SETTINGS:Lcom/squareup/api/items/Type;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->ADDITIONAL_ITEM_IMAGE:Lcom/squareup/api/items/Type;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->PROMO:Lcom/squareup/api/items/Type;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->INVENTORY_INFO:Lcom/squareup/api/items/Type;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->OBSOLETE_TENDER_FEE:Lcom/squareup/api/items/Type;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->DINING_OPTION:Lcom/squareup/api/items/Type;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->TAX_RULE:Lcom/squareup/api/items/Type;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->CONFIGURATION:Lcom/squareup/api/items/Type;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->TICKET_GROUP:Lcom/squareup/api/items/Type;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->TICKET_TEMPLATE:Lcom/squareup/api/items/Type;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->VOID_REASON:Lcom/squareup/api/items/Type;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->MENU:Lcom/squareup/api/items/Type;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->TAG:Lcom/squareup/api/items/Type;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->FLOOR_PLAN:Lcom/squareup/api/items/Type;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->FLOOR_PLAN_TILE:Lcom/squareup/api/items/Type;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->FAVORITES_LIST_POSITION:Lcom/squareup/api/items/Type;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->MENU_GROUP_MEMBERSHIP:Lcom/squareup/api/items/Type;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->SURCHARGE:Lcom/squareup/api/items/Type;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->PRICING_RULE:Lcom/squareup/api/items/Type;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->PRODUCT_SET:Lcom/squareup/api/items/Type;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->TIME_PERIOD:Lcom/squareup/api/items/Type;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->SURCHARGE_FEE_MEMBERSHIP:Lcom/squareup/api/items/Type;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/api/items/Type;->$VALUES:[Lcom/squareup/api/items/Type;

    .line 81
    new-instance v0, Lcom/squareup/api/items/Type$ProtoAdapter_Type;

    invoke-direct {v0}, Lcom/squareup/api/items/Type$ProtoAdapter_Type;-><init>()V

    sput-object v0, Lcom/squareup/api/items/Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 85
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 86
    iput p3, p0, Lcom/squareup/api/items/Type;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/items/Type;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 128
    :pswitch_0
    sget-object p0, Lcom/squareup/api/items/Type;->SURCHARGE_FEE_MEMBERSHIP:Lcom/squareup/api/items/Type;

    return-object p0

    .line 127
    :pswitch_1
    sget-object p0, Lcom/squareup/api/items/Type;->TIME_PERIOD:Lcom/squareup/api/items/Type;

    return-object p0

    .line 126
    :pswitch_2
    sget-object p0, Lcom/squareup/api/items/Type;->PRODUCT_SET:Lcom/squareup/api/items/Type;

    return-object p0

    .line 125
    :pswitch_3
    sget-object p0, Lcom/squareup/api/items/Type;->PRICING_RULE:Lcom/squareup/api/items/Type;

    return-object p0

    .line 124
    :pswitch_4
    sget-object p0, Lcom/squareup/api/items/Type;->SURCHARGE:Lcom/squareup/api/items/Type;

    return-object p0

    .line 123
    :pswitch_5
    sget-object p0, Lcom/squareup/api/items/Type;->MENU_GROUP_MEMBERSHIP:Lcom/squareup/api/items/Type;

    return-object p0

    .line 122
    :pswitch_6
    sget-object p0, Lcom/squareup/api/items/Type;->FAVORITES_LIST_POSITION:Lcom/squareup/api/items/Type;

    return-object p0

    .line 121
    :pswitch_7
    sget-object p0, Lcom/squareup/api/items/Type;->FLOOR_PLAN_TILE:Lcom/squareup/api/items/Type;

    return-object p0

    .line 120
    :pswitch_8
    sget-object p0, Lcom/squareup/api/items/Type;->FLOOR_PLAN:Lcom/squareup/api/items/Type;

    return-object p0

    .line 119
    :pswitch_9
    sget-object p0, Lcom/squareup/api/items/Type;->TAG:Lcom/squareup/api/items/Type;

    return-object p0

    .line 118
    :pswitch_a
    sget-object p0, Lcom/squareup/api/items/Type;->MENU:Lcom/squareup/api/items/Type;

    return-object p0

    .line 117
    :pswitch_b
    sget-object p0, Lcom/squareup/api/items/Type;->VOID_REASON:Lcom/squareup/api/items/Type;

    return-object p0

    .line 116
    :pswitch_c
    sget-object p0, Lcom/squareup/api/items/Type;->TICKET_TEMPLATE:Lcom/squareup/api/items/Type;

    return-object p0

    .line 115
    :pswitch_d
    sget-object p0, Lcom/squareup/api/items/Type;->TICKET_GROUP:Lcom/squareup/api/items/Type;

    return-object p0

    .line 114
    :pswitch_e
    sget-object p0, Lcom/squareup/api/items/Type;->CONFIGURATION:Lcom/squareup/api/items/Type;

    return-object p0

    .line 113
    :pswitch_f
    sget-object p0, Lcom/squareup/api/items/Type;->TAX_RULE:Lcom/squareup/api/items/Type;

    return-object p0

    .line 112
    :pswitch_10
    sget-object p0, Lcom/squareup/api/items/Type;->DINING_OPTION:Lcom/squareup/api/items/Type;

    return-object p0

    .line 111
    :pswitch_11
    sget-object p0, Lcom/squareup/api/items/Type;->OBSOLETE_TENDER_FEE:Lcom/squareup/api/items/Type;

    return-object p0

    .line 110
    :pswitch_12
    sget-object p0, Lcom/squareup/api/items/Type;->INVENTORY_INFO:Lcom/squareup/api/items/Type;

    return-object p0

    .line 109
    :pswitch_13
    sget-object p0, Lcom/squareup/api/items/Type;->PROMO:Lcom/squareup/api/items/Type;

    return-object p0

    .line 108
    :pswitch_14
    sget-object p0, Lcom/squareup/api/items/Type;->ADDITIONAL_ITEM_IMAGE:Lcom/squareup/api/items/Type;

    return-object p0

    .line 107
    :pswitch_15
    sget-object p0, Lcom/squareup/api/items/Type;->MARKET_ITEM_SETTINGS:Lcom/squareup/api/items/Type;

    return-object p0

    .line 106
    :pswitch_16
    sget-object p0, Lcom/squareup/api/items/Type;->ITEM_MODIFIER_OPTION:Lcom/squareup/api/items/Type;

    return-object p0

    .line 105
    :pswitch_17
    sget-object p0, Lcom/squareup/api/items/Type;->ITEM_ITEM_MODIFIER_LIST_MEMBERSHIP:Lcom/squareup/api/items/Type;

    return-object p0

    .line 104
    :pswitch_18
    sget-object p0, Lcom/squareup/api/items/Type;->ITEM_MODIFIER_LIST:Lcom/squareup/api/items/Type;

    return-object p0

    .line 103
    :pswitch_19
    sget-object p0, Lcom/squareup/api/items/Type;->ITEM_FEE_MEMBERSHIP:Lcom/squareup/api/items/Type;

    return-object p0

    .line 102
    :pswitch_1a
    sget-object p0, Lcom/squareup/api/items/Type;->DISCOUNT:Lcom/squareup/api/items/Type;

    return-object p0

    .line 101
    :pswitch_1b
    sget-object p0, Lcom/squareup/api/items/Type;->PLACEHOLDER:Lcom/squareup/api/items/Type;

    return-object p0

    .line 100
    :pswitch_1c
    sget-object p0, Lcom/squareup/api/items/Type;->FEE:Lcom/squareup/api/items/Type;

    return-object p0

    .line 99
    :pswitch_1d
    sget-object p0, Lcom/squareup/api/items/Type;->ITEM_VARIATION:Lcom/squareup/api/items/Type;

    return-object p0

    .line 98
    :pswitch_1e
    sget-object p0, Lcom/squareup/api/items/Type;->MENU_CATEGORY:Lcom/squareup/api/items/Type;

    return-object p0

    .line 97
    :pswitch_1f
    sget-object p0, Lcom/squareup/api/items/Type;->ITEM_PAGE_MEMBERSHIP:Lcom/squareup/api/items/Type;

    return-object p0

    .line 96
    :pswitch_20
    sget-object p0, Lcom/squareup/api/items/Type;->ITEM_IMAGE:Lcom/squareup/api/items/Type;

    return-object p0

    .line 95
    :pswitch_21
    sget-object p0, Lcom/squareup/api/items/Type;->PAGE:Lcom/squareup/api/items/Type;

    return-object p0

    .line 94
    :pswitch_22
    sget-object p0, Lcom/squareup/api/items/Type;->ITEM:Lcom/squareup/api/items/Type;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/items/Type;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/api/items/Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/items/Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/items/Type;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/api/items/Type;->$VALUES:[Lcom/squareup/api/items/Type;

    invoke-virtual {v0}, [Lcom/squareup/api/items/Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/items/Type;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 135
    iget v0, p0, Lcom/squareup/api/items/Type;->value:I

    return v0
.end method
