.class public final Lcom/squareup/api/items/ItemModifierOptionOverride$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ItemModifierOptionOverride.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/ItemModifierOptionOverride;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/ItemModifierOptionOverride;",
        "Lcom/squareup/api/items/ItemModifierOptionOverride$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public on_by_default:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 83
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/ItemModifierOptionOverride;
    .locals 3

    .line 93
    new-instance v0, Lcom/squareup/api/items/ItemModifierOptionOverride;

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOptionOverride$Builder;->on_by_default:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/api/items/ItemModifierOptionOverride;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 80
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemModifierOptionOverride$Builder;->build()Lcom/squareup/api/items/ItemModifierOptionOverride;

    move-result-object v0

    return-object v0
.end method

.method public on_by_default(Ljava/lang/Boolean;)Lcom/squareup/api/items/ItemModifierOptionOverride$Builder;
    .locals 0

    .line 87
    iput-object p1, p0, Lcom/squareup/api/items/ItemModifierOptionOverride$Builder;->on_by_default:Ljava/lang/Boolean;

    return-object p0
.end method
