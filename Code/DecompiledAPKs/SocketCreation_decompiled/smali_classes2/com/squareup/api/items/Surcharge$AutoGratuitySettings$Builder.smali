.class public final Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Surcharge.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;",
        "Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public auto_enable_type:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

.field public minimum_seat_count:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 411
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public auto_enable_type(Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;)Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;
    .locals 0

    .line 424
    iput-object p1, p0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;->auto_enable_type:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    return-object p0
.end method

.method public build()Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;
    .locals 4

    .line 430
    new-instance v0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;->minimum_seat_count:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;->auto_enable_type:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;-><init>(Ljava/lang/Integer;Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$AutoEnableType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 406
    invoke-virtual {p0}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;->build()Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    move-result-object v0

    return-object v0
.end method

.method public minimum_seat_count(Ljava/lang/Integer;)Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;
    .locals 0

    .line 419
    iput-object p1, p0, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings$Builder;->minimum_seat_count:Ljava/lang/Integer;

    return-object p0
.end method
