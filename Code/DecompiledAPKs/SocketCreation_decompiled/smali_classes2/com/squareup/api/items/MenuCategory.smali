.class public final Lcom/squareup/api/items/MenuCategory;
.super Lcom/squareup/wire/Message;
.source "MenuCategory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/MenuCategory$ProtoAdapter_MenuCategory;,
        Lcom/squareup/api/items/MenuCategory$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/MenuCategory;",
        "Lcom/squareup/api/items/MenuCategory$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/MenuCategory;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ABBREVIATION:Ljava/lang/String; = ""

.field public static final DEFAULT_COLOR:Ljava/lang/String; = ""

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_ORDINAL:Ljava/lang/Integer;

.field public static final DEFAULT_STRAIGHT_FIRE:Lcom/squareup/api/items/StraightFireType;

.field public static final DEFAULT_V2_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final abbreviation:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.MerchantCatalogObjectReference#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final color:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final ordinal:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3
    .end annotation
.end field

.field public final straight_fire:Lcom/squareup/api/items/StraightFireType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.StraightFireType#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final v2_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/api/items/MenuCategory$ProtoAdapter_MenuCategory;

    invoke-direct {v0}, Lcom/squareup/api/items/MenuCategory$ProtoAdapter_MenuCategory;-><init>()V

    sput-object v0, Lcom/squareup/api/items/MenuCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 29
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/MenuCategory;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    .line 31
    sget-object v0, Lcom/squareup/api/items/StraightFireType;->UNKNOWN_STRAIGHT_FIRE:Lcom/squareup/api/items/StraightFireType;

    sput-object v0, Lcom/squareup/api/items/MenuCategory;->DEFAULT_STRAIGHT_FIRE:Lcom/squareup/api/items/StraightFireType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/api/items/MerchantCatalogObjectReference;Lcom/squareup/api/items/StraightFireType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .line 109
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/api/items/MenuCategory;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/api/items/MerchantCatalogObjectReference;Lcom/squareup/api/items/StraightFireType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/api/items/MerchantCatalogObjectReference;Lcom/squareup/api/items/StraightFireType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 115
    sget-object v0, Lcom/squareup/api/items/MenuCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 116
    iput-object p1, p0, Lcom/squareup/api/items/MenuCategory;->id:Ljava/lang/String;

    .line 117
    iput-object p2, p0, Lcom/squareup/api/items/MenuCategory;->name:Ljava/lang/String;

    .line 118
    iput-object p3, p0, Lcom/squareup/api/items/MenuCategory;->ordinal:Ljava/lang/Integer;

    .line 119
    iput-object p4, p0, Lcom/squareup/api/items/MenuCategory;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 120
    iput-object p5, p0, Lcom/squareup/api/items/MenuCategory;->straight_fire:Lcom/squareup/api/items/StraightFireType;

    .line 121
    iput-object p6, p0, Lcom/squareup/api/items/MenuCategory;->color:Ljava/lang/String;

    .line 122
    iput-object p7, p0, Lcom/squareup/api/items/MenuCategory;->abbreviation:Ljava/lang/String;

    .line 123
    iput-object p8, p0, Lcom/squareup/api/items/MenuCategory;->v2_id:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 144
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/MenuCategory;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 145
    :cond_1
    check-cast p1, Lcom/squareup/api/items/MenuCategory;

    .line 146
    invoke-virtual {p0}, Lcom/squareup/api/items/MenuCategory;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/MenuCategory;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/MenuCategory;->id:Ljava/lang/String;

    .line 147
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/MenuCategory;->name:Ljava/lang/String;

    .line 148
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->ordinal:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/items/MenuCategory;->ordinal:Ljava/lang/Integer;

    .line 149
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v3, p1, Lcom/squareup/api/items/MenuCategory;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 150
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->straight_fire:Lcom/squareup/api/items/StraightFireType;

    iget-object v3, p1, Lcom/squareup/api/items/MenuCategory;->straight_fire:Lcom/squareup/api/items/StraightFireType;

    .line 151
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->color:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/MenuCategory;->color:Ljava/lang/String;

    .line 152
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->abbreviation:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/MenuCategory;->abbreviation:Ljava/lang/String;

    .line 153
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->v2_id:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/api/items/MenuCategory;->v2_id:Ljava/lang/String;

    .line 154
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 159
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_8

    .line 161
    invoke-virtual {p0}, Lcom/squareup/api/items/MenuCategory;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 162
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 163
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 164
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->ordinal:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 165
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/api/items/MerchantCatalogObjectReference;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 166
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->straight_fire:Lcom/squareup/api/items/StraightFireType;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/api/items/StraightFireType;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 167
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->color:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 168
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->abbreviation:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 169
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->v2_id:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    .line 170
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/MenuCategory$Builder;
    .locals 2

    .line 128
    new-instance v0, Lcom/squareup/api/items/MenuCategory$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/MenuCategory$Builder;-><init>()V

    .line 129
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/MenuCategory$Builder;->id:Ljava/lang/String;

    .line 130
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/MenuCategory$Builder;->name:Ljava/lang/String;

    .line 131
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->ordinal:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/items/MenuCategory$Builder;->ordinal:Ljava/lang/Integer;

    .line 132
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iput-object v1, v0, Lcom/squareup/api/items/MenuCategory$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 133
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->straight_fire:Lcom/squareup/api/items/StraightFireType;

    iput-object v1, v0, Lcom/squareup/api/items/MenuCategory$Builder;->straight_fire:Lcom/squareup/api/items/StraightFireType;

    .line 134
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->color:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/MenuCategory$Builder;->color:Ljava/lang/String;

    .line 135
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->abbreviation:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/MenuCategory$Builder;->abbreviation:Ljava/lang/String;

    .line 136
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->v2_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/MenuCategory$Builder;->v2_id:Ljava/lang/String;

    .line 137
    invoke-virtual {p0}, Lcom/squareup/api/items/MenuCategory;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/MenuCategory$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/api/items/MenuCategory;->newBuilder()Lcom/squareup/api/items/MenuCategory$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 178
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->ordinal:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", ordinal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->ordinal:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 181
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-eqz v1, :cond_3

    const-string v1, ", catalog_object_reference="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 182
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->straight_fire:Lcom/squareup/api/items/StraightFireType;

    if-eqz v1, :cond_4

    const-string v1, ", straight_fire="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->straight_fire:Lcom/squareup/api/items/StraightFireType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 183
    :cond_4
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->color:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", color="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->color:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    :cond_5
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->abbreviation:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", abbreviation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->abbreviation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    :cond_6
    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->v2_id:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", v2_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory;->v2_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "MenuCategory{"

    .line 186
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
