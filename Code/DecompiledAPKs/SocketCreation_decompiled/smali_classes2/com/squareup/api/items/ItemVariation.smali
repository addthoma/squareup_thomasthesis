.class public final Lcom/squareup/api/items/ItemVariation;
.super Lcom/squareup/wire/Message;
.source "ItemVariation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/ItemVariation$ProtoAdapter_ItemVariation;,
        Lcom/squareup/api/items/ItemVariation$InventoryAlertType;,
        Lcom/squareup/api/items/ItemVariation$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/ItemVariation;",
        "Lcom/squareup/api/items/ItemVariation$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/ItemVariation;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_AVAILABLE_ON_ONLINE_BOOKING_SITE:Ljava/lang/Boolean;

.field public static final DEFAULT_BUYER_FACING_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_GTIN:Ljava/lang/String; = ""

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_INVENTORY_ALERT_THRESHOLD:Ljava/lang/Long;

.field public static final DEFAULT_INVENTORY_ALERT_TYPE:Lcom/squareup/api/items/ItemVariation$InventoryAlertType;

.field public static final DEFAULT_MEASUREMENT_UNIT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_ORDINAL:Ljava/lang/Integer;

.field public static final DEFAULT_PRICE_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_PRICING_TYPE:Lcom/squareup/api/items/PricingType;

.field public static final DEFAULT_SERVICE_DURATION:Ljava/lang/Long;

.field public static final DEFAULT_SKU:Ljava/lang/String; = ""

.field public static final DEFAULT_TRACK_INVENTORY:Ljava/lang/Boolean;

.field public static final DEFAULT_TRANSITION_TIME:Ljava/lang/Long;

.field public static final DEFAULT_USER_DATA:Ljava/lang/String; = ""

.field public static final DEFAULT_V2_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final available_on_online_booking_site:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x17
    .end annotation
.end field

.field public final buyer_facing_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x11
    .end annotation
.end field

.field public final catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.MerchantCatalogObjectReference#ADAPTER"
        tag = 0x13
    .end annotation
.end field

.field public final custom_attribute_values:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogCustomAttributeValue#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1e
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;",
            ">;"
        }
    .end annotation
.end field

.field public final employee_tokens:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x18
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final gtin:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xb
    .end annotation
.end field

.field public final intermissions:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Intermission#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1c
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Intermission;",
            ">;"
        }
    .end annotation
.end field

.field public final inventory_alert_threshold:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0xe
    .end annotation
.end field

.field public final inventory_alert_type:Lcom/squareup/api/items/ItemVariation$InventoryAlertType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ItemVariation$InventoryAlertType#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final item:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final item_option_values:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ItemOptionValueForItemVariation#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1b
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/ItemOptionValueForItemVariation;",
            ">;"
        }
    .end annotation
.end field

.field public final measurement_unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x19
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final no_show_fee_money:Lcom/squareup/protos/common/dinero/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.dinero.Money#ADAPTER"
        tag = 0x16
    .end annotation
.end field

.field public final ordinal:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x5
    .end annotation
.end field

.field public final price:Lcom/squareup/protos/common/dinero/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.dinero.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final price_description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x14
    .end annotation
.end field

.field public final pricing_type:Lcom/squareup/api/items/PricingType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.PricingType#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final resource_tokens:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1f
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final service_duration:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x12
    .end annotation
.end field

.field public final sku:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final track_inventory:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xf
    .end annotation
.end field

.field public final transition_time:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x15
    .end annotation
.end field

.field public final user_data:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x10
    .end annotation
.end field

.field public final v2_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1a
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 29
    new-instance v0, Lcom/squareup/api/items/ItemVariation$ProtoAdapter_ItemVariation;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemVariation$ProtoAdapter_ItemVariation;-><init>()V

    sput-object v0, Lcom/squareup/api/items/ItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 43
    sget-object v0, Lcom/squareup/api/items/PricingType;->FIXED_PRICING:Lcom/squareup/api/items/PricingType;

    sput-object v0, Lcom/squareup/api/items/ItemVariation;->DEFAULT_PRICING_TYPE:Lcom/squareup/api/items/PricingType;

    const/4 v0, 0x0

    .line 45
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/ItemVariation;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    .line 47
    sget-object v0, Lcom/squareup/api/items/ItemVariation$InventoryAlertType;->NONE:Lcom/squareup/api/items/ItemVariation$InventoryAlertType;

    sput-object v0, Lcom/squareup/api/items/ItemVariation;->DEFAULT_INVENTORY_ALERT_TYPE:Lcom/squareup/api/items/ItemVariation$InventoryAlertType;

    const-wide/16 v2, 0x0

    .line 49
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/ItemVariation;->DEFAULT_INVENTORY_ALERT_THRESHOLD:Ljava/lang/Long;

    .line 51
    sput-object v1, Lcom/squareup/api/items/ItemVariation;->DEFAULT_TRACK_INVENTORY:Ljava/lang/Boolean;

    .line 55
    sput-object v0, Lcom/squareup/api/items/ItemVariation;->DEFAULT_SERVICE_DURATION:Ljava/lang/Long;

    .line 59
    sput-object v0, Lcom/squareup/api/items/ItemVariation;->DEFAULT_TRANSITION_TIME:Ljava/lang/Long;

    .line 61
    sput-object v1, Lcom/squareup/api/items/ItemVariation;->DEFAULT_AVAILABLE_ON_ONLINE_BOOKING_SITE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/ItemVariation$Builder;Lokio/ByteString;)V
    .locals 1

    .line 288
    sget-object v0, Lcom/squareup/api/items/ItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 289
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->id:Ljava/lang/String;

    .line 290
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->name:Ljava/lang/String;

    .line 291
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->buyer_facing_name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->buyer_facing_name:Ljava/lang/String;

    .line 292
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->sku:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->sku:Ljava/lang/String;

    .line 293
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->gtin:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->gtin:Ljava/lang/String;

    .line 294
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->price:Lcom/squareup/protos/common/dinero/Money;

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->price:Lcom/squareup/protos/common/dinero/Money;

    .line 295
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->pricing_type:Lcom/squareup/api/items/PricingType;

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->pricing_type:Lcom/squareup/api/items/PricingType;

    .line 296
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->ordinal:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->ordinal:Ljava/lang/Integer;

    .line 297
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->item:Lcom/squareup/api/sync/ObjectId;

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->item:Lcom/squareup/api/sync/ObjectId;

    .line 298
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->inventory_alert_type:Lcom/squareup/api/items/ItemVariation$InventoryAlertType;

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->inventory_alert_type:Lcom/squareup/api/items/ItemVariation$InventoryAlertType;

    .line 299
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->inventory_alert_threshold:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->inventory_alert_threshold:Ljava/lang/Long;

    .line 300
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->track_inventory:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->track_inventory:Ljava/lang/Boolean;

    .line 301
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->user_data:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->user_data:Ljava/lang/String;

    .line 302
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->service_duration:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->service_duration:Ljava/lang/Long;

    .line 303
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->price_description:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->price_description:Ljava/lang/String;

    .line 304
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->transition_time:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->transition_time:Ljava/lang/Long;

    .line 305
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->no_show_fee_money:Lcom/squareup/protos/common/dinero/Money;

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->no_show_fee_money:Lcom/squareup/protos/common/dinero/Money;

    .line 306
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->available_on_online_booking_site:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->available_on_online_booking_site:Ljava/lang/Boolean;

    .line 307
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->employee_tokens:Ljava/util/List;

    const-string v0, "employee_tokens"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->employee_tokens:Ljava/util/List;

    .line 308
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 309
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->measurement_unit_token:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->measurement_unit_token:Ljava/lang/String;

    .line 310
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->v2_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->v2_id:Ljava/lang/String;

    .line 311
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->item_option_values:Ljava/util/List;

    const-string v0, "item_option_values"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->item_option_values:Ljava/util/List;

    .line 312
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->intermissions:Ljava/util/List;

    const-string v0, "intermissions"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->intermissions:Ljava/util/List;

    .line 313
    iget-object p2, p1, Lcom/squareup/api/items/ItemVariation$Builder;->custom_attribute_values:Ljava/util/List;

    const-string v0, "custom_attribute_values"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/api/items/ItemVariation;->custom_attribute_values:Ljava/util/List;

    .line 314
    iget-object p1, p1, Lcom/squareup/api/items/ItemVariation$Builder;->resource_tokens:Ljava/util/List;

    const-string p2, "resource_tokens"

    invoke-static {p2, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/api/items/ItemVariation;->resource_tokens:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 353
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/ItemVariation;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 354
    :cond_1
    check-cast p1, Lcom/squareup/api/items/ItemVariation;

    .line 355
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemVariation;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/ItemVariation;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->id:Ljava/lang/String;

    .line 356
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->name:Ljava/lang/String;

    .line 357
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->buyer_facing_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->buyer_facing_name:Ljava/lang/String;

    .line 358
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->sku:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->sku:Ljava/lang/String;

    .line 359
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->gtin:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->gtin:Ljava/lang/String;

    .line 360
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->price:Lcom/squareup/protos/common/dinero/Money;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->price:Lcom/squareup/protos/common/dinero/Money;

    .line 361
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->pricing_type:Lcom/squareup/api/items/PricingType;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->pricing_type:Lcom/squareup/api/items/PricingType;

    .line 362
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->ordinal:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->ordinal:Ljava/lang/Integer;

    .line 363
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->item:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->item:Lcom/squareup/api/sync/ObjectId;

    .line 364
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->inventory_alert_type:Lcom/squareup/api/items/ItemVariation$InventoryAlertType;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->inventory_alert_type:Lcom/squareup/api/items/ItemVariation$InventoryAlertType;

    .line 365
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->inventory_alert_threshold:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->inventory_alert_threshold:Ljava/lang/Long;

    .line 366
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->track_inventory:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->track_inventory:Ljava/lang/Boolean;

    .line 367
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->user_data:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->user_data:Ljava/lang/String;

    .line 368
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->service_duration:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->service_duration:Ljava/lang/Long;

    .line 369
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->price_description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->price_description:Ljava/lang/String;

    .line 370
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->transition_time:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->transition_time:Ljava/lang/Long;

    .line 371
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->no_show_fee_money:Lcom/squareup/protos/common/dinero/Money;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->no_show_fee_money:Lcom/squareup/protos/common/dinero/Money;

    .line 372
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->available_on_online_booking_site:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->available_on_online_booking_site:Ljava/lang/Boolean;

    .line 373
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->employee_tokens:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->employee_tokens:Ljava/util/List;

    .line 374
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 375
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->measurement_unit_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->measurement_unit_token:Ljava/lang/String;

    .line 376
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->v2_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->v2_id:Ljava/lang/String;

    .line 377
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->item_option_values:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->item_option_values:Ljava/util/List;

    .line 378
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->intermissions:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->intermissions:Ljava/util/List;

    .line 379
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->custom_attribute_values:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/api/items/ItemVariation;->custom_attribute_values:Ljava/util/List;

    .line 380
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->resource_tokens:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/api/items/ItemVariation;->resource_tokens:Ljava/util/List;

    .line 381
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 386
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_15

    .line 388
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemVariation;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 389
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 390
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 391
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->buyer_facing_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 392
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->sku:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 393
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->gtin:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 394
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->price:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/common/dinero/Money;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 395
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->pricing_type:Lcom/squareup/api/items/PricingType;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/api/items/PricingType;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 396
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->ordinal:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 397
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->item:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 398
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->inventory_alert_type:Lcom/squareup/api/items/ItemVariation$InventoryAlertType;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemVariation$InventoryAlertType;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 399
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->inventory_alert_threshold:Ljava/lang/Long;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 400
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->track_inventory:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 401
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->user_data:Ljava/lang/String;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 402
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->service_duration:Ljava/lang/Long;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 403
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->price_description:Ljava/lang/String;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 404
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->transition_time:Ljava/lang/Long;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 405
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->no_show_fee_money:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/squareup/protos/common/dinero/Money;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 406
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->available_on_online_booking_site:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 407
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->employee_tokens:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 408
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Lcom/squareup/api/items/MerchantCatalogObjectReference;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 409
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->measurement_unit_token:Ljava/lang/String;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 410
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->v2_id:Ljava/lang/String;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_14
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 411
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->item_option_values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 412
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->intermissions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 413
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->custom_attribute_values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 414
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->resource_tokens:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 415
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_15
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/ItemVariation$Builder;
    .locals 2

    .line 319
    new-instance v0, Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemVariation$Builder;-><init>()V

    .line 320
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->id:Ljava/lang/String;

    .line 321
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->name:Ljava/lang/String;

    .line 322
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->buyer_facing_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->buyer_facing_name:Ljava/lang/String;

    .line 323
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->sku:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->sku:Ljava/lang/String;

    .line 324
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->gtin:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->gtin:Ljava/lang/String;

    .line 325
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->price:Lcom/squareup/protos/common/dinero/Money;

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->price:Lcom/squareup/protos/common/dinero/Money;

    .line 326
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->pricing_type:Lcom/squareup/api/items/PricingType;

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->pricing_type:Lcom/squareup/api/items/PricingType;

    .line 327
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->ordinal:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->ordinal:Ljava/lang/Integer;

    .line 328
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->item:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->item:Lcom/squareup/api/sync/ObjectId;

    .line 329
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->inventory_alert_type:Lcom/squareup/api/items/ItemVariation$InventoryAlertType;

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->inventory_alert_type:Lcom/squareup/api/items/ItemVariation$InventoryAlertType;

    .line 330
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->inventory_alert_threshold:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->inventory_alert_threshold:Ljava/lang/Long;

    .line 331
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->track_inventory:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->track_inventory:Ljava/lang/Boolean;

    .line 332
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->user_data:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->user_data:Ljava/lang/String;

    .line 333
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->service_duration:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->service_duration:Ljava/lang/Long;

    .line 334
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->price_description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->price_description:Ljava/lang/String;

    .line 335
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->transition_time:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->transition_time:Ljava/lang/Long;

    .line 336
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->no_show_fee_money:Lcom/squareup/protos/common/dinero/Money;

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->no_show_fee_money:Lcom/squareup/protos/common/dinero/Money;

    .line 337
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->available_on_online_booking_site:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->available_on_online_booking_site:Ljava/lang/Boolean;

    .line 338
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->employee_tokens:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->employee_tokens:Ljava/util/List;

    .line 339
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 340
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->measurement_unit_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->measurement_unit_token:Ljava/lang/String;

    .line 341
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->v2_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->v2_id:Ljava/lang/String;

    .line 342
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->item_option_values:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->item_option_values:Ljava/util/List;

    .line 343
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->intermissions:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->intermissions:Ljava/util/List;

    .line 344
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->custom_attribute_values:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->custom_attribute_values:Ljava/util/List;

    .line 345
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->resource_tokens:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/api/items/ItemVariation$Builder;->resource_tokens:Ljava/util/List;

    .line 346
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemVariation;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemVariation$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemVariation;->newBuilder()Lcom/squareup/api/items/ItemVariation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 422
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 423
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 424
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 425
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->buyer_facing_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", buyer_facing_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->buyer_facing_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 426
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->sku:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", sku="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->sku:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 427
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->gtin:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", gtin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->gtin:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 428
    :cond_4
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->price:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v1, :cond_5

    const-string v1, ", price="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->price:Lcom/squareup/protos/common/dinero/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 429
    :cond_5
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->pricing_type:Lcom/squareup/api/items/PricingType;

    if-eqz v1, :cond_6

    const-string v1, ", pricing_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->pricing_type:Lcom/squareup/api/items/PricingType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 430
    :cond_6
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->ordinal:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    const-string v1, ", ordinal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->ordinal:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 431
    :cond_7
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->item:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_8

    const-string v1, ", item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->item:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 432
    :cond_8
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->inventory_alert_type:Lcom/squareup/api/items/ItemVariation$InventoryAlertType;

    if-eqz v1, :cond_9

    const-string v1, ", inventory_alert_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->inventory_alert_type:Lcom/squareup/api/items/ItemVariation$InventoryAlertType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 433
    :cond_9
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->inventory_alert_threshold:Ljava/lang/Long;

    if-eqz v1, :cond_a

    const-string v1, ", inventory_alert_threshold="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->inventory_alert_threshold:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 434
    :cond_a
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->track_inventory:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    const-string v1, ", track_inventory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->track_inventory:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 435
    :cond_b
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->user_data:Ljava/lang/String;

    if-eqz v1, :cond_c

    const-string v1, ", user_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->user_data:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 436
    :cond_c
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->service_duration:Ljava/lang/Long;

    if-eqz v1, :cond_d

    const-string v1, ", service_duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->service_duration:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 437
    :cond_d
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->price_description:Ljava/lang/String;

    if-eqz v1, :cond_e

    const-string v1, ", price_description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->price_description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 438
    :cond_e
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->transition_time:Ljava/lang/Long;

    if-eqz v1, :cond_f

    const-string v1, ", transition_time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->transition_time:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 439
    :cond_f
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->no_show_fee_money:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v1, :cond_10

    const-string v1, ", no_show_fee_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->no_show_fee_money:Lcom/squareup/protos/common/dinero/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 440
    :cond_10
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->available_on_online_booking_site:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    const-string v1, ", available_on_online_booking_site="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->available_on_online_booking_site:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 441
    :cond_11
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->employee_tokens:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_12

    const-string v1, ", employee_tokens="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->employee_tokens:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 442
    :cond_12
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-eqz v1, :cond_13

    const-string v1, ", catalog_object_reference="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 443
    :cond_13
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->measurement_unit_token:Ljava/lang/String;

    if-eqz v1, :cond_14

    const-string v1, ", measurement_unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->measurement_unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 444
    :cond_14
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->v2_id:Ljava/lang/String;

    if-eqz v1, :cond_15

    const-string v1, ", v2_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->v2_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    :cond_15
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->item_option_values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_16

    const-string v1, ", item_option_values="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->item_option_values:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 446
    :cond_16
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->intermissions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_17

    const-string v1, ", intermissions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->intermissions:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 447
    :cond_17
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->custom_attribute_values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_18

    const-string v1, ", custom_attribute_values="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->custom_attribute_values:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 448
    :cond_18
    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->resource_tokens:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_19

    const-string v1, ", resource_tokens="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemVariation;->resource_tokens:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_19
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ItemVariation{"

    .line 449
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
