.class public final Lcom/squareup/api/items/PricingRule$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PricingRule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/PricingRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/PricingRule;",
        "Lcom/squareup/api/items/PricingRule$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public application_mode:Lcom/squareup/api/items/PricingRule$ApplicationMode;

.field public apply_products:Lcom/squareup/api/sync/ObjectId;

.field public discount_id:Lcom/squareup/api/sync/ObjectId;

.field public discount_target_scope:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

.field public exclude_products:Lcom/squareup/api/sync/ObjectId;

.field public exclude_strategy:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

.field public id:Ljava/lang/String;

.field public match_products:Lcom/squareup/api/sync/ObjectId;

.field public max_applications_per_attachment:Ljava/lang/Integer;

.field public name:Ljava/lang/String;

.field public stackable:Lcom/squareup/api/items/PricingRule$Stackable;

.field public valid_from:Ljava/lang/String;

.field public valid_until:Ljava/lang/String;

.field public validity:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 344
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 345
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/items/PricingRule$Builder;->validity:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public application_mode(Lcom/squareup/api/items/PricingRule$ApplicationMode;)Lcom/squareup/api/items/PricingRule$Builder;
    .locals 0

    .line 455
    iput-object p1, p0, Lcom/squareup/api/items/PricingRule$Builder;->application_mode:Lcom/squareup/api/items/PricingRule$ApplicationMode;

    return-object p0
.end method

.method public apply_products(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/PricingRule$Builder;
    .locals 0

    .line 392
    iput-object p1, p0, Lcom/squareup/api/items/PricingRule$Builder;->apply_products:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public build()Lcom/squareup/api/items/PricingRule;
    .locals 18

    move-object/from16 v0, p0

    .line 487
    new-instance v17, Lcom/squareup/api/items/PricingRule;

    iget-object v2, v0, Lcom/squareup/api/items/PricingRule$Builder;->id:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/api/items/PricingRule$Builder;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/squareup/api/items/PricingRule$Builder;->validity:Ljava/util/List;

    iget-object v5, v0, Lcom/squareup/api/items/PricingRule$Builder;->match_products:Lcom/squareup/api/sync/ObjectId;

    iget-object v6, v0, Lcom/squareup/api/items/PricingRule$Builder;->apply_products:Lcom/squareup/api/sync/ObjectId;

    iget-object v7, v0, Lcom/squareup/api/items/PricingRule$Builder;->stackable:Lcom/squareup/api/items/PricingRule$Stackable;

    iget-object v8, v0, Lcom/squareup/api/items/PricingRule$Builder;->valid_from:Ljava/lang/String;

    iget-object v9, v0, Lcom/squareup/api/items/PricingRule$Builder;->valid_until:Ljava/lang/String;

    iget-object v10, v0, Lcom/squareup/api/items/PricingRule$Builder;->exclude_products:Lcom/squareup/api/sync/ObjectId;

    iget-object v11, v0, Lcom/squareup/api/items/PricingRule$Builder;->exclude_strategy:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    iget-object v12, v0, Lcom/squareup/api/items/PricingRule$Builder;->application_mode:Lcom/squareup/api/items/PricingRule$ApplicationMode;

    iget-object v13, v0, Lcom/squareup/api/items/PricingRule$Builder;->discount_target_scope:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    iget-object v14, v0, Lcom/squareup/api/items/PricingRule$Builder;->max_applications_per_attachment:Ljava/lang/Integer;

    iget-object v15, v0, Lcom/squareup/api/items/PricingRule$Builder;->discount_id:Lcom/squareup/api/sync/ObjectId;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v16

    move-object/from16 v1, v17

    invoke-direct/range {v1 .. v16}, Lcom/squareup/api/items/PricingRule;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/items/PricingRule$Stackable;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/items/PricingRule$ExcludeStrategy;Lcom/squareup/api/items/PricingRule$ApplicationMode;Lcom/squareup/api/items/PricingRule$DiscountTargetScope;Ljava/lang/Integer;Lcom/squareup/api/sync/ObjectId;Lokio/ByteString;)V

    return-object v17
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 315
    invoke-virtual {p0}, Lcom/squareup/api/items/PricingRule$Builder;->build()Lcom/squareup/api/items/PricingRule;

    move-result-object v0

    return-object v0
.end method

.method public discount_id(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/PricingRule$Builder;
    .locals 0

    .line 481
    iput-object p1, p0, Lcom/squareup/api/items/PricingRule$Builder;->discount_id:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public discount_target_scope(Lcom/squareup/api/items/PricingRule$DiscountTargetScope;)Lcom/squareup/api/items/PricingRule$Builder;
    .locals 0

    .line 464
    iput-object p1, p0, Lcom/squareup/api/items/PricingRule$Builder;->discount_target_scope:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    return-object p0
.end method

.method public exclude_products(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/PricingRule$Builder;
    .locals 0

    .line 437
    iput-object p1, p0, Lcom/squareup/api/items/PricingRule$Builder;->exclude_products:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public exclude_strategy(Lcom/squareup/api/items/PricingRule$ExcludeStrategy;)Lcom/squareup/api/items/PricingRule$Builder;
    .locals 0

    .line 446
    iput-object p1, p0, Lcom/squareup/api/items/PricingRule$Builder;->exclude_strategy:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/PricingRule$Builder;
    .locals 0

    .line 349
    iput-object p1, p0, Lcom/squareup/api/items/PricingRule$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public match_products(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/PricingRule$Builder;
    .locals 0

    .line 380
    iput-object p1, p0, Lcom/squareup/api/items/PricingRule$Builder;->match_products:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public max_applications_per_attachment(Ljava/lang/Integer;)Lcom/squareup/api/items/PricingRule$Builder;
    .locals 0

    .line 473
    iput-object p1, p0, Lcom/squareup/api/items/PricingRule$Builder;->max_applications_per_attachment:Ljava/lang/Integer;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/api/items/PricingRule$Builder;
    .locals 0

    .line 357
    iput-object p1, p0, Lcom/squareup/api/items/PricingRule$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public stackable(Lcom/squareup/api/items/PricingRule$Stackable;)Lcom/squareup/api/items/PricingRule$Builder;
    .locals 0

    .line 401
    iput-object p1, p0, Lcom/squareup/api/items/PricingRule$Builder;->stackable:Lcom/squareup/api/items/PricingRule$Stackable;

    return-object p0
.end method

.method public valid_from(Ljava/lang/String;)Lcom/squareup/api/items/PricingRule$Builder;
    .locals 0

    .line 415
    iput-object p1, p0, Lcom/squareup/api/items/PricingRule$Builder;->valid_from:Ljava/lang/String;

    return-object p0
.end method

.method public valid_until(Ljava/lang/String;)Lcom/squareup/api/items/PricingRule$Builder;
    .locals 0

    .line 425
    iput-object p1, p0, Lcom/squareup/api/items/PricingRule$Builder;->valid_until:Ljava/lang/String;

    return-object p0
.end method

.method public validity(Ljava/util/List;)Lcom/squareup/api/items/PricingRule$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;)",
            "Lcom/squareup/api/items/PricingRule$Builder;"
        }
    .end annotation

    .line 367
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 368
    iput-object p1, p0, Lcom/squareup/api/items/PricingRule$Builder;->validity:Ljava/util/List;

    return-object p0
.end method
