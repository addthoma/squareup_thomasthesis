.class public final enum Lcom/squareup/api/items/PricingRule$ApplicationMode;
.super Ljava/lang/Enum;
.source "PricingRule.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/PricingRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ApplicationMode"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/PricingRule$ApplicationMode$ProtoAdapter_ApplicationMode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/items/PricingRule$ApplicationMode;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/items/PricingRule$ApplicationMode;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/PricingRule$ApplicationMode;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ATTACHED:Lcom/squareup/api/items/PricingRule$ApplicationMode;

.field public static final enum AUTOMATIC:Lcom/squareup/api/items/PricingRule$ApplicationMode;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 605
    new-instance v0, Lcom/squareup/api/items/PricingRule$ApplicationMode;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "AUTOMATIC"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/api/items/PricingRule$ApplicationMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/PricingRule$ApplicationMode;->AUTOMATIC:Lcom/squareup/api/items/PricingRule$ApplicationMode;

    .line 610
    new-instance v0, Lcom/squareup/api/items/PricingRule$ApplicationMode;

    const/4 v3, 0x2

    const-string v4, "ATTACHED"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/api/items/PricingRule$ApplicationMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/PricingRule$ApplicationMode;->ATTACHED:Lcom/squareup/api/items/PricingRule$ApplicationMode;

    new-array v0, v3, [Lcom/squareup/api/items/PricingRule$ApplicationMode;

    .line 601
    sget-object v3, Lcom/squareup/api/items/PricingRule$ApplicationMode;->AUTOMATIC:Lcom/squareup/api/items/PricingRule$ApplicationMode;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/api/items/PricingRule$ApplicationMode;->ATTACHED:Lcom/squareup/api/items/PricingRule$ApplicationMode;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/api/items/PricingRule$ApplicationMode;->$VALUES:[Lcom/squareup/api/items/PricingRule$ApplicationMode;

    .line 612
    new-instance v0, Lcom/squareup/api/items/PricingRule$ApplicationMode$ProtoAdapter_ApplicationMode;

    invoke-direct {v0}, Lcom/squareup/api/items/PricingRule$ApplicationMode$ProtoAdapter_ApplicationMode;-><init>()V

    sput-object v0, Lcom/squareup/api/items/PricingRule$ApplicationMode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 616
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 617
    iput p3, p0, Lcom/squareup/api/items/PricingRule$ApplicationMode;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/items/PricingRule$ApplicationMode;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 626
    :cond_0
    sget-object p0, Lcom/squareup/api/items/PricingRule$ApplicationMode;->ATTACHED:Lcom/squareup/api/items/PricingRule$ApplicationMode;

    return-object p0

    .line 625
    :cond_1
    sget-object p0, Lcom/squareup/api/items/PricingRule$ApplicationMode;->AUTOMATIC:Lcom/squareup/api/items/PricingRule$ApplicationMode;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/items/PricingRule$ApplicationMode;
    .locals 1

    .line 601
    const-class v0, Lcom/squareup/api/items/PricingRule$ApplicationMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/items/PricingRule$ApplicationMode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/items/PricingRule$ApplicationMode;
    .locals 1

    .line 601
    sget-object v0, Lcom/squareup/api/items/PricingRule$ApplicationMode;->$VALUES:[Lcom/squareup/api/items/PricingRule$ApplicationMode;

    invoke-virtual {v0}, [Lcom/squareup/api/items/PricingRule$ApplicationMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/items/PricingRule$ApplicationMode;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 633
    iget v0, p0, Lcom/squareup/api/items/PricingRule$ApplicationMode;->value:I

    return v0
.end method
