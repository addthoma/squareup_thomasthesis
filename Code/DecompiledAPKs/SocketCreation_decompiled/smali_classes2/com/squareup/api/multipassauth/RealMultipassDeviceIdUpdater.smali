.class public final Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater;
.super Ljava/lang/Object;
.source "RealMultipassDeviceIdUpdater.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0008\u0010\r\u001a\u00020\nH\u0016J\u0014\u0010\u000e\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00110\u00100\u000fH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater;",
        "Lmortar/Scoped;",
        "connectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "multipassService",
        "Lcom/squareup/api/multipassauth/MultipassService;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/api/multipassauth/MultipassService;Lcom/squareup/settings/server/Features;)V",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "updateDeviceIdIfNeeded",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/SimpleResponse;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final multipassService:Lcom/squareup/api/multipassauth/MultipassService;


# direct methods
.method public constructor <init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/api/multipassauth/MultipassService;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "connectivityMonitor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "multipassService"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    iput-object p2, p0, Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater;->multipassService:Lcom/squareup/api/multipassauth/MultipassService;

    iput-object p3, p0, Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static final synthetic access$updateDeviceIdIfNeeded(Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater;)Lio/reactivex/Single;
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater;->updateDeviceIdIfNeeded()Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private final updateDeviceIdIfNeeded()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;>;"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater;->multipassService:Lcom/squareup/api/multipassauth/MultipassService;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/squareup/api/multipassauth/MultipassService$DefaultImpls;->updateDeviceIdIfNeeded$default(Lcom/squareup/api/multipassauth/MultipassService;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/server/SimpleStandardResponse;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Lcom/squareup/server/SimpleStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ATTEMPT_DEVICE_ID_UPDATE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v0}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v0

    .line 32
    sget-object v1, Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater$onEnterScope$1;->INSTANCE:Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater$onEnterScope$1;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 33
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 34
    new-instance v1, Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater$onEnterScope$2;-><init>(Lcom/squareup/api/multipassauth/RealMultipassDeviceIdUpdater;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "connectivityMonitor.inte\u2026pdateDeviceIdIfNeeded() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 35
    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith$default(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
