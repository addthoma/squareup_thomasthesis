.class public abstract enum Lcom/squareup/api/ApiVersion;
.super Ljava/lang/Enum;
.source "ApiVersion.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/ApiVersion;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/ApiVersion;

.field static final COM_SQUAREUP_POS:Ljava/lang/String; = "com.squareup.pos."

.field static final COM_SQUAREUP_REGISTER:Ljava/lang/String; = "com.squareup.register."

.field public static final enum INVALID_VERSION:Lcom/squareup/api/ApiVersion;

.field public static final enum NO_VERSION:Lcom/squareup/api/ApiVersion;

.field public static final enum UNSUPPORTED_VERSION:Lcom/squareup/api/ApiVersion;

.field public static final enum V1_0:Lcom/squareup/api/ApiVersion;

.field public static final enum V1_1:Lcom/squareup/api/ApiVersion;

.field public static final enum V1_2:Lcom/squareup/api/ApiVersion;

.field public static final enum V1_3:Lcom/squareup/api/ApiVersion;

.field public static final enum V2_0:Lcom/squareup/api/ApiVersion;

.field public static final enum V3_0:Lcom/squareup/api/ApiVersion;


# instance fields
.field public final versionString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 63
    new-instance v0, Lcom/squareup/api/ApiVersion$1;

    const/4 v1, 0x0

    const-string v2, "UNSUPPORTED_VERSION"

    invoke-direct {v0, v2, v1, v2}, Lcom/squareup/api/ApiVersion$1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/api/ApiVersion;->UNSUPPORTED_VERSION:Lcom/squareup/api/ApiVersion;

    .line 67
    new-instance v0, Lcom/squareup/api/ApiVersion$2;

    const/4 v2, 0x1

    const-string v3, "NO_VERSION"

    const-string v4, "NO_API_VERSION"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/api/ApiVersion$2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/api/ApiVersion;->NO_VERSION:Lcom/squareup/api/ApiVersion;

    .line 72
    new-instance v0, Lcom/squareup/api/ApiVersion$3;

    const/4 v3, 0x2

    const-string v4, "INVALID_VERSION"

    const-string v5, "INVALID_API_VERSION"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/api/ApiVersion$3;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/api/ApiVersion;->INVALID_VERSION:Lcom/squareup/api/ApiVersion;

    .line 77
    new-instance v0, Lcom/squareup/api/ApiVersion$4;

    const/4 v4, 0x3

    const-string v5, "V1_0"

    const-string v6, "1.0"

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/api/ApiVersion$4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/api/ApiVersion;->V1_0:Lcom/squareup/api/ApiVersion;

    .line 90
    new-instance v0, Lcom/squareup/api/ApiVersion$5;

    const/4 v5, 0x4

    const-string v6, "V1_1"

    const-string/jumbo v7, "v1.1"

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/api/ApiVersion$5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/api/ApiVersion;->V1_1:Lcom/squareup/api/ApiVersion;

    .line 96
    new-instance v0, Lcom/squareup/api/ApiVersion$6;

    const/4 v6, 0x5

    const-string v7, "V1_2"

    const-string/jumbo v8, "v1.2"

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/api/ApiVersion$6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/api/ApiVersion;->V1_2:Lcom/squareup/api/ApiVersion;

    .line 102
    new-instance v0, Lcom/squareup/api/ApiVersion$7;

    const/4 v7, 0x6

    const-string v8, "V1_3"

    const-string/jumbo v9, "v1.3"

    invoke-direct {v0, v8, v7, v9}, Lcom/squareup/api/ApiVersion$7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/api/ApiVersion;->V1_3:Lcom/squareup/api/ApiVersion;

    .line 113
    new-instance v0, Lcom/squareup/api/ApiVersion$8;

    const/4 v8, 0x7

    const-string v9, "V2_0"

    const-string/jumbo v10, "v2.0"

    invoke-direct {v0, v9, v8, v10}, Lcom/squareup/api/ApiVersion$8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/api/ApiVersion;->V2_0:Lcom/squareup/api/ApiVersion;

    .line 129
    new-instance v0, Lcom/squareup/api/ApiVersion$9;

    const/16 v9, 0x8

    const-string v10, "V3_0"

    const-string/jumbo v11, "v3.0"

    invoke-direct {v0, v10, v9, v11}, Lcom/squareup/api/ApiVersion$9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/api/ApiVersion;->V3_0:Lcom/squareup/api/ApiVersion;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/api/ApiVersion;

    .line 57
    sget-object v10, Lcom/squareup/api/ApiVersion;->UNSUPPORTED_VERSION:Lcom/squareup/api/ApiVersion;

    aput-object v10, v0, v1

    sget-object v1, Lcom/squareup/api/ApiVersion;->NO_VERSION:Lcom/squareup/api/ApiVersion;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiVersion;->INVALID_VERSION:Lcom/squareup/api/ApiVersion;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/api/ApiVersion;->V1_0:Lcom/squareup/api/ApiVersion;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/api/ApiVersion;->V1_1:Lcom/squareup/api/ApiVersion;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/api/ApiVersion;->V1_2:Lcom/squareup/api/ApiVersion;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/api/ApiVersion;->V1_3:Lcom/squareup/api/ApiVersion;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/api/ApiVersion;->V2_0:Lcom/squareup/api/ApiVersion;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/api/ApiVersion;->V3_0:Lcom/squareup/api/ApiVersion;

    aput-object v1, v0, v9

    sput-object v0, Lcom/squareup/api/ApiVersion;->$VALUES:[Lcom/squareup/api/ApiVersion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 144
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 145
    iput-object p3, p0, Lcom/squareup/api/ApiVersion;->versionString:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/api/ApiVersion$1;)V
    .locals 0

    .line 57
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/api/ApiVersion;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Landroid/content/Intent;Lcom/squareup/api/ApiVersion;Lcom/squareup/api/ApiVersion;Z)Landroid/content/Intent;
    .locals 0

    .line 57
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/api/ApiVersion;->replaceIntentKeys(Landroid/content/Intent;Lcom/squareup/api/ApiVersion;Lcom/squareup/api/ApiVersion;Z)Landroid/content/Intent;

    move-result-object p0

    return-object p0
.end method

.method private static downgradeNativeResponseIntentToV2(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2

    .line 215
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string v1, "com.squareup.pos.STATE"

    .line 217
    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 219
    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    const-string v1, "com.squareup.pos.REQUEST_METADATA"

    .line 220
    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-object v0
.end method

.method public static downgradeResponseIntent(Landroid/content/Intent;Lcom/squareup/api/ApiVersion;)Landroid/content/Intent;
    .locals 2

    if-eqz p1, :cond_0

    .line 205
    invoke-virtual {p1}, Lcom/squareup/api/ApiVersion;->ordinal()I

    move-result v0

    sget-object v1, Lcom/squareup/api/ApiVersion;->V3_0:Lcom/squareup/api/ApiVersion;

    invoke-virtual {v1}, Lcom/squareup/api/ApiVersion;->ordinal()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 206
    invoke-static {p0}, Lcom/squareup/api/ApiVersion;->downgradeNativeResponseIntentToV2(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object p0

    :cond_0
    if-eqz p1, :cond_1

    .line 208
    invoke-virtual {p1}, Lcom/squareup/api/ApiVersion;->ordinal()I

    move-result p1

    sget-object v0, Lcom/squareup/api/ApiVersion;->V2_0:Lcom/squareup/api/ApiVersion;

    invoke-virtual {v0}, Lcom/squareup/api/ApiVersion;->ordinal()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 209
    sget-object p1, Lcom/squareup/api/ApiVersion;->V2_0:Lcom/squareup/api/ApiVersion;

    sget-object v0, Lcom/squareup/api/ApiVersion;->V1_3:Lcom/squareup/api/ApiVersion;

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/squareup/api/ApiVersion;->replaceIntentKeys(Landroid/content/Intent;Lcom/squareup/api/ApiVersion;Lcom/squareup/api/ApiVersion;Z)Landroid/content/Intent;

    move-result-object p0

    :cond_1
    return-object p0
.end method

.method public static downgradeResponseQueryParametersToV1(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 5

    .line 262
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 263
    sget-object p0, Lcom/squareup/api/ApiVersion;->V2_0:Lcom/squareup/api/ApiVersion;

    sget-object v1, Lcom/squareup/api/ApiVersion;->V1_3:Lcom/squareup/api/ApiVersion;

    const/4 v2, 0x1

    invoke-static {v0, p0, v1, v2}, Lcom/squareup/api/ApiVersion;->replaceIntentKeys(Landroid/content/Intent;Lcom/squareup/api/ApiVersion;Lcom/squareup/api/ApiVersion;Z)Landroid/content/Intent;

    move-result-object p0

    .line 264
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 265
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->clearQuery()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "com.squareup.pos.CLIENT_TRANSACTION_ID"

    .line 268
    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v3, "com.squareup.register.CLIENT_TRANSACTION_ID"

    .line 270
    invoke-virtual {v1, v3, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    const-string v2, "com.squareup.pos.SERVER_TRANSACTION_ID"

    .line 274
    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v3, "com.squareup.register.SERVER_TRANSACTION_ID"

    .line 276
    invoke-virtual {v1, v3, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    const-string v2, "com.squareup.pos.ERROR_CODE"

    .line 280
    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    const-string v3, "com.squareup.pos."

    const-string v4, "com.squareup.register."

    .line 284
    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.squareup.register.ERROR_CODE"

    .line 283
    invoke-virtual {v1, v3, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_2
    const-string v2, "com.squareup.pos.ERROR_DESCRIPTION"

    .line 286
    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    const-string v3, "com.squareup.register.ERROR_DESCRIPTION"

    .line 288
    invoke-virtual {v1, v3, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_3
    const-string v2, "com.squareup.pos.REQUEST_METADATA"

    .line 291
    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v2, "com.squareup.register.REQUEST_METADATA"

    .line 293
    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 296
    :cond_4
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    return-object p0
.end method

.method public static downgradeWebResponseIntent(Landroid/content/Intent;Lcom/squareup/api/ApiVersion;)Landroid/content/Intent;
    .locals 2

    if-eqz p1, :cond_0

    .line 227
    invoke-virtual {p1}, Lcom/squareup/api/ApiVersion;->ordinal()I

    move-result v0

    sget-object v1, Lcom/squareup/api/ApiVersion;->V3_0:Lcom/squareup/api/ApiVersion;

    invoke-virtual {v1}, Lcom/squareup/api/ApiVersion;->ordinal()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 228
    invoke-static {p0}, Lcom/squareup/api/ApiVersion;->downgradeWebResponseToV2(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object p0

    :cond_0
    if-eqz p1, :cond_1

    .line 230
    invoke-virtual {p1}, Lcom/squareup/api/ApiVersion;->ordinal()I

    move-result p1

    sget-object v0, Lcom/squareup/api/ApiVersion;->V2_0:Lcom/squareup/api/ApiVersion;

    invoke-virtual {v0}, Lcom/squareup/api/ApiVersion;->ordinal()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 231
    invoke-static {p0}, Lcom/squareup/api/ApiVersion;->downgradeResponseQueryParametersToV1(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object p0

    :cond_1
    return-object p0
.end method

.method public static downgradeWebResponseToV2(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 7

    .line 237
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 238
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p0

    .line 239
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->clearQuery()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 241
    invoke-virtual {p0}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 242
    sget-object v4, Lcom/squareup/api/WebApiStrings;->WEB_RESPONSE_KEYS_V3_TO_V2:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 246
    invoke-virtual {p0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    const-string v6, "error_code"

    .line 248
    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 250
    sget-object v5, Lcom/squareup/api/WebApiStrings;->WEB_RESPONSE_KEYS_V3_TO_V2:Ljava/util/Map;

    invoke-virtual {p0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v5, v3

    check-cast v5, Ljava/lang/String;

    .line 252
    :cond_1
    invoke-virtual {v1, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 257
    :cond_2
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    return-object v0
.end method

.method private static getNewIntentKey(Ljava/lang/String;Lcom/squareup/api/ApiVersion;Lcom/squareup/api/ApiVersion;Z)Ljava/lang/String;
    .locals 2

    .line 303
    sget-object v0, Lcom/squareup/api/ApiVersion$10;->$SwitchMap$com$squareup$api$ApiVersion:[I

    invoke-virtual {p2}, Lcom/squareup/api/ApiVersion;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const-string v0, "com.squareup.register."

    const-string v1, "com.squareup.pos."

    packed-switch p2, :pswitch_data_0

    const/4 p1, 0x0

    goto :goto_0

    :pswitch_0
    if-eqz p3, :cond_0

    .line 321
    sget-object p1, Lcom/squareup/api/WebApiStrings;->WEB_REQUEST_KEYS_V2_TO_V3:Ljava/util/Map;

    invoke-interface {p1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/api/LegacyApiKeys;->NATIVE_REQUEST_KEYS_V2_TO_V3:Ljava/util/Map;

    .line 322
    invoke-interface {p1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    goto :goto_0

    .line 311
    :pswitch_1
    invoke-virtual {p1}, Lcom/squareup/api/ApiVersion;->ordinal()I

    move-result p1

    sget-object p2, Lcom/squareup/api/ApiVersion;->V2_0:Lcom/squareup/api/ApiVersion;

    invoke-virtual {p2}, Lcom/squareup/api/ApiVersion;->ordinal()I

    move-result p2

    if-ge p1, p2, :cond_1

    .line 313
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    move-object p1, p0

    goto :goto_0

    .line 308
    :pswitch_2
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    if-nez p1, :cond_2

    goto :goto_1

    :cond_2
    move-object p0, p1

    :goto_1
    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getVersion(Landroid/content/Intent;)Lcom/squareup/api/ApiVersion;
    .locals 2

    const-string v0, "api_version"

    .line 189
    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 190
    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/api/ApiVersion;->parse(Ljava/lang/String;)Lcom/squareup/api/ApiVersion;

    move-result-object p0

    return-object p0

    :cond_0
    const-string v0, "com.squareup.pos.API_VERSION"

    .line 191
    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 192
    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/api/ApiVersion;->parse(Ljava/lang/String;)Lcom/squareup/api/ApiVersion;

    move-result-object p0

    return-object p0

    :cond_1
    const-string v0, "com.squareup.register.API_VERSION"

    .line 193
    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 195
    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/api/ApiVersion;->parse(Ljava/lang/String;)Lcom/squareup/api/ApiVersion;

    move-result-object p0

    .line 198
    sget-object v0, Lcom/squareup/api/ApiVersion;->V2_0:Lcom/squareup/api/ApiVersion;

    invoke-virtual {p0, v0}, Lcom/squareup/api/ApiVersion;->isAtLeast(Lcom/squareup/api/ApiVersion;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object p0, Lcom/squareup/api/ApiVersion;->INVALID_VERSION:Lcom/squareup/api/ApiVersion;

    :cond_2
    return-object p0

    .line 200
    :cond_3
    sget-object p0, Lcom/squareup/api/ApiVersion;->NO_VERSION:Lcom/squareup/api/ApiVersion;

    return-object p0
.end method

.method public static latest()Lcom/squareup/api/ApiVersion;
    .locals 2

    .line 184
    invoke-static {}, Lcom/squareup/api/ApiVersion;->values()[Lcom/squareup/api/ApiVersion;

    move-result-object v0

    .line 185
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public static parse(Ljava/lang/String;)Lcom/squareup/api/ApiVersion;
    .locals 5

    .line 175
    invoke-static {}, Lcom/squareup/api/ApiVersion;->values()[Lcom/squareup/api/ApiVersion;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 176
    iget-object v4, v3, Lcom/squareup/api/ApiVersion;->versionString:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 180
    :cond_1
    sget-object p0, Lcom/squareup/api/ApiVersion;->UNSUPPORTED_VERSION:Lcom/squareup/api/ApiVersion;

    return-object p0
.end method

.method private static replaceIntentKeys(Landroid/content/Intent;Lcom/squareup/api/ApiVersion;Lcom/squareup/api/ApiVersion;Z)Landroid/content/Intent;
    .locals 7

    .line 333
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 334
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p0

    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 335
    invoke-static {v1, p1, p2, p3}, Lcom/squareup/api/ApiVersion;->getNewIntentKey(Ljava/lang/String;Lcom/squareup/api/ApiVersion;Lcom/squareup/api/ApiVersion;Z)Ljava/lang/String;

    move-result-object v2

    .line 336
    invoke-virtual {p2}, Lcom/squareup/api/ApiVersion;->ordinal()I

    move-result v3

    sget-object v4, Lcom/squareup/api/ApiVersion;->V2_0:Lcom/squareup/api/ApiVersion;

    invoke-virtual {v4}, Lcom/squareup/api/ApiVersion;->ordinal()I

    move-result v4

    if-le v3, v4, :cond_1

    move-object v3, v1

    goto :goto_1

    :cond_1
    const-string v3, "com.squareup.register."

    const-string v4, "com.squareup.pos."

    .line 337
    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    const/4 v4, -0x1

    .line 338
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    goto/16 :goto_2

    :sswitch_0
    const-string v5, "com.squareup.pos.SERVER_TRANSACTION_ID"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v4, 0x3

    goto/16 :goto_2

    :sswitch_1
    const-string v5, "com.squareup.pos.CLIENT_ID"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0xa

    goto/16 :goto_2

    :sswitch_2
    const-string v5, "com.squareup.pos.action.CHARGE"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v4, 0x7

    goto/16 :goto_2

    :sswitch_3
    const-string v5, "com.squareup.pos.ERROR_INSUFFICIENT_CARD_BALANCE"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x18

    goto/16 :goto_2

    :sswitch_4
    const-string v5, "com.squareup.pos.ERROR_ILLEGAL_LOCATION_ID"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x17

    goto/16 :goto_2

    :sswitch_5
    const-string v5, "com.squareup.pos.ERROR_INVALID_CUSTOMER_ID"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x23

    goto/16 :goto_2

    :sswitch_6
    const-string v5, "com.squareup.pos.UNSUPPORTED_API_VERSION"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x1f

    goto/16 :goto_2

    :sswitch_7
    const-string v5, "com.squareup.pos.ERROR_INVALID_REQUEST"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x15

    goto/16 :goto_2

    :sswitch_8
    const-string v5, "com.squareup.pos.ERROR_UNEXPECTED"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x1e

    goto/16 :goto_2

    :sswitch_9
    const-string v5, "com.squareup.pos.TENDER_CARD_ON_FILE"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x10

    goto/16 :goto_2

    :sswitch_a
    const-string v5, "com.squareup.pos.CLIENT_TRANSACTION_ID"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v4, 0x2

    goto/16 :goto_2

    :sswitch_b
    const-string v5, "com.squareup.pos.TOTAL_AMOUNT"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x24

    goto/16 :goto_2

    :sswitch_c
    const-string v5, "com.squareup.pos.ERROR_TRANSACTION_ALREADY_IN_PROGRESS"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x1c

    goto/16 :goto_2

    :sswitch_d
    const-string v5, "com.squareup.pos.ERROR_GIFT_CARDS_NOT_SUPPORTED"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x14

    goto/16 :goto_2

    :sswitch_e
    const-string v5, "com.squareup.pos.ERROR_USER_NOT_LOGGED_IN"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x20

    goto/16 :goto_2

    :sswitch_f
    const-string v5, "com.squareup.pos.TENDER_TYPES"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v4, 0x0

    goto/16 :goto_2

    :sswitch_10
    const-string v5, "com.squareup.pos.TENDER_OTHER"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x12

    goto/16 :goto_2

    :sswitch_11
    const-string v5, "com.squareup.pos.LOCATION_ID"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0xd

    goto/16 :goto_2

    :sswitch_12
    const-string v5, "com.squareup.pos.ERROR_NO_RESULT"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x19

    goto/16 :goto_2

    :sswitch_13
    const-string v5, "com.squareup.pos.REQUEST_METADATA"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v4, 0x5

    goto/16 :goto_2

    :sswitch_14
    const-string v5, "com.squareup.pos.SDK_VERSION"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x9

    goto/16 :goto_2

    :sswitch_15
    const-string v5, "com.squareup.pos.ERROR_UNAUTHORIZED_CLIENT_ID"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x1d

    goto/16 :goto_2

    :sswitch_16
    const-string v5, "com.squareup.pos.ERROR_TRANSACTION_CANCELED"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x1b

    goto/16 :goto_2

    :sswitch_17
    const-string v5, "com.squareup.pos.WEB_CALLBACK_URI"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v4, 0x4

    goto/16 :goto_2

    :sswitch_18
    const-string v5, "com.squareup.pos.TENDER_CASH"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x11

    goto/16 :goto_2

    :sswitch_19
    const-string v5, "com.squareup.pos.TENDER_CARD"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v4, 0x6

    goto/16 :goto_2

    :sswitch_1a
    const-string v5, "com.squareup.pos.ERROR_NO_NETWORK"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x1a

    goto/16 :goto_2

    :sswitch_1b
    const-string v5, "com.squareup.pos.API_VERSION"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x8

    goto/16 :goto_2

    :sswitch_1c
    const-string v5, "com.squareup.pos.ERROR_NO_EMPLOYEE_LOGGED_IN"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x13

    goto/16 :goto_2

    :sswitch_1d
    const-string v5, "com.squareup.pos.NOTE"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0xc

    goto :goto_2

    :sswitch_1e
    const-string v5, "com.squareup.pos.ERROR_USER_NOT_ACTIVATED"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x21

    goto :goto_2

    :sswitch_1f
    const-string v5, "com.squareup.pos.AUTO_RETURN_TIMEOUT_MS"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x26

    goto :goto_2

    :sswitch_20
    const-string v5, "com.squareup.pos.ERROR_CUSTOMER_MANAGEMENT_NOT_SUPPORTED"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x22

    goto :goto_2

    :sswitch_21
    const-string v5, "com.squareup.pos.CURRENCY_CODE"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0xb

    goto :goto_2

    :sswitch_22
    const-string v5, "com.squareup.pos.ERROR_CODE"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v4, 0x1

    goto :goto_2

    :sswitch_23
    const-string v5, "com.squareup.pos.CUSTOMER_ID"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0xe

    goto :goto_2

    :sswitch_24
    const-string v5, "com.squareup.pos.ERROR_DESCRIPTION"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0xf

    goto :goto_2

    :sswitch_25
    const-string v5, "com.squareup.pos.ERROR_DISABLED"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x16

    goto :goto_2

    :sswitch_26
    const-string v5, "com.squareup.pos.TOTAL_AMOUNT_LONG"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v4, 0x25

    :cond_2
    :goto_2
    packed-switch v4, :pswitch_data_0

    goto/16 :goto_5

    :pswitch_0
    const-wide/16 v3, 0x0

    .line 410
    invoke-virtual {v0, v1, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    .line 409
    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto/16 :goto_5

    :pswitch_1
    const-wide/high16 v3, -0x8000000000000000L

    .line 406
    invoke-virtual {v0, v1, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto/16 :goto_5

    :pswitch_2
    const/high16 v3, -0x80000000

    .line 402
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_5

    .line 398
    :pswitch_3
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_5

    .line 361
    :pswitch_4
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1, p2, p3}, Lcom/squareup/api/ApiVersion;->getNewIntentKey(Ljava/lang/String;Lcom/squareup/api/ApiVersion;Lcom/squareup/api/ApiVersion;Z)Ljava/lang/String;

    move-result-object v3

    .line 360
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_5

    .line 342
    :pswitch_5
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 344
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 345
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 346
    invoke-static {v5, p1, p2, p3}, Lcom/squareup/api/ApiVersion;->getNewIntentKey(Ljava/lang/String;Lcom/squareup/api/ApiVersion;Lcom/squareup/api/ApiVersion;Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 348
    :cond_3
    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    goto :goto_5

    .line 350
    :cond_4
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ","

    .line 351
    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 352
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 353
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 354
    invoke-static {v6, p1, p2, p3}, Lcom/squareup/api/ApiVersion;->getNewIntentKey(Ljava/lang/String;Lcom/squareup/api/ApiVersion;Lcom/squareup/api/ApiVersion;Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 356
    :cond_5
    invoke-static {v4, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 413
    :goto_5
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 414
    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    return-object v0

    :sswitch_data_0
    .sparse-switch
        -0x7fd2c69b -> :sswitch_26
        -0x79fa73ea -> :sswitch_25
        -0x77f1e7fe -> :sswitch_24
        -0x75001127 -> :sswitch_23
        -0x73d21b79 -> :sswitch_22
        -0x73603428 -> :sswitch_21
        -0x69889813 -> :sswitch_20
        -0x62d9149a -> :sswitch_1f
        -0x60fe2135 -> :sswitch_1e
        -0x6044732b -> :sswitch_1d
        -0x565d6f6b -> :sswitch_1c
        -0x52dbe6d0 -> :sswitch_1b
        -0x4cdd5756 -> :sswitch_1a
        -0x40df30a8 -> :sswitch_19
        -0x40df3085 -> :sswitch_18
        -0x3a1a0a20 -> :sswitch_17
        -0x352a406c -> :sswitch_16
        -0x32df6b62 -> :sswitch_15
        -0x25afc4b0 -> :sswitch_14
        -0x258c6c9e -> :sswitch_13
        0xc9a3601 -> :sswitch_12
        0x158b2ba2 -> :sswitch_11
        0x25aab3a8 -> :sswitch_10
        0x25f38d11 -> :sswitch_f
        0x2878021c -> :sswitch_e
        0x29193e11 -> :sswitch_d
        0x3f6ff4a5 -> :sswitch_c
        0x470e9fb6 -> :sswitch_b
        0x55315aad -> :sswitch_a
        0x56798df5 -> :sswitch_9
        0x62e8fb6b -> :sswitch_8
        0x630e592d -> :sswitch_7
        0x64f50286 -> :sswitch_6
        0x66a0d4da -> :sswitch_5
        0x6b8deca2 -> :sswitch_4
        0x6e51c8ef -> :sswitch_3
        0x7657dfe9 -> :sswitch_2
        0x770e1f0c -> :sswitch_1
        0x78e4c035 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static upgradeIntent(Landroid/content/Intent;Lcom/squareup/api/ApiVersion;)Landroid/content/Intent;
    .locals 2

    if-nez p1, :cond_0

    return-object p0

    .line 166
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 167
    invoke-static {}, Lcom/squareup/api/ApiVersion;->values()[Lcom/squareup/api/ApiVersion;

    move-result-object p0

    .line 168
    invoke-virtual {p1}, Lcom/squareup/api/ApiVersion;->ordinal()I

    move-result p1

    :goto_0
    array-length v1, p0

    if-ge p1, v1, :cond_1

    .line 169
    aget-object v1, p0, p1

    invoke-virtual {v1, v0}, Lcom/squareup/api/ApiVersion;->upgrade(Landroid/content/Intent;)V

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/ApiVersion;
    .locals 1

    .line 57
    const-class v0, Lcom/squareup/api/ApiVersion;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/ApiVersion;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/ApiVersion;
    .locals 1

    .line 57
    sget-object v0, Lcom/squareup/api/ApiVersion;->$VALUES:[Lcom/squareup/api/ApiVersion;

    invoke-virtual {v0}, [Lcom/squareup/api/ApiVersion;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/ApiVersion;

    return-object v0
.end method


# virtual methods
.method public isAtLeast(Lcom/squareup/api/ApiVersion;)Z
    .locals 1

    .line 149
    invoke-virtual {p0}, Lcom/squareup/api/ApiVersion;->ordinal()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/api/ApiVersion;->ordinal()I

    move-result p1

    if-lt v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method abstract upgrade(Landroid/content/Intent;)V
.end method
