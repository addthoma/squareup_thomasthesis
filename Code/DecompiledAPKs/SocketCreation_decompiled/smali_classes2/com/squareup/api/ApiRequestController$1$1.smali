.class Lcom/squareup/api/ApiRequestController$1$1;
.super Lcom/squareup/pauses/PausesAndResumes$Adapter;
.source "ApiRequestController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/api/ApiRequestController$1;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/api/ApiRequestController$1;


# direct methods
.method constructor <init>(Lcom/squareup/api/ApiRequestController$1;)V
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/squareup/api/ApiRequestController$1$1;->this$1:Lcom/squareup/api/ApiRequestController$1;

    invoke-direct {p0}, Lcom/squareup/pauses/PausesAndResumes$Adapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onResume()V
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController$1$1;->this$1:Lcom/squareup/api/ApiRequestController$1;

    iget-object v0, v0, Lcom/squareup/api/ApiRequestController$1;->this$0:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->isApiRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/api/ApiRequestController$1$1;->this$1:Lcom/squareup/api/ApiRequestController$1;

    iget-object v0, v0, Lcom/squareup/api/ApiRequestController$1;->this$0:Lcom/squareup/api/ApiRequestController;

    invoke-static {v0}, Lcom/squareup/api/ApiRequestController;->access$500(Lcom/squareup/api/ApiRequestController;)Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/api/ApiRequestController$1$1;->this$1:Lcom/squareup/api/ApiRequestController$1;

    iget-object v1, v1, Lcom/squareup/api/ApiRequestController$1;->this$0:Lcom/squareup/api/ApiRequestController;

    invoke-static {v1}, Lcom/squareup/api/ApiRequestController;->access$200(Lcom/squareup/api/ApiRequestController;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController$1$1;->this$1:Lcom/squareup/api/ApiRequestController$1;

    iget-object v0, v0, Lcom/squareup/api/ApiRequestController$1;->this$0:Lcom/squareup/api/ApiRequestController;

    invoke-static {v0}, Lcom/squareup/api/ApiRequestController;->access$300(Lcom/squareup/api/ApiRequestController;)Lcom/squareup/api/ApiSessionLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/api/ApiRequestController$1$1;->this$1:Lcom/squareup/api/ApiRequestController$1;

    iget-object v1, v1, Lcom/squareup/api/ApiRequestController$1;->this$0:Lcom/squareup/api/ApiRequestController;

    invoke-static {v1}, Lcom/squareup/api/ApiRequestController;->access$200(Lcom/squareup/api/ApiRequestController;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/ApiSessionLogger;->setApiLogProperty(Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController$1$1;->this$1:Lcom/squareup/api/ApiRequestController$1;

    iget-object v0, v0, Lcom/squareup/api/ApiRequestController$1;->this$0:Lcom/squareup/api/ApiRequestController;

    invoke-static {v0}, Lcom/squareup/api/ApiRequestController;->access$600(Lcom/squareup/api/ApiRequestController;)Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
