.class public abstract Lcom/squareup/accountstatus/AccountStatusModule;
.super Ljava/lang/Object;
.source "AccountStatusModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCountryCode(Lcom/squareup/accountstatus/PersistentAccountStatusService;)Lcom/squareup/CountryCode;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 62
    invoke-virtual {p0}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->getStatus()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    .line 63
    invoke-static {p0}, Lcom/squareup/server/account/UserUtils;->getCountryCodeOrNull(Lcom/squareup/server/account/protos/User;)Lcom/squareup/CountryCode;

    move-result-object p0

    if-nez p0, :cond_0

    .line 64
    sget-object p0, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    :cond_0
    return-object p0
.end method

.method static provideMerchantToken(Lcom/squareup/accountstatus/PersistentAccountStatusService;)Ljava/lang/String;
    .locals 0
    .annotation runtime Lcom/squareup/user/MaybeMerchantToken;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 49
    invoke-virtual {p0}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->getStatus()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    if-eqz p0, :cond_0

    .line 50
    iget-object p0, p0, Lcom/squareup/server/account/protos/User;->merchant_token:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method static provideStatus(Lcom/squareup/accountstatus/PersistentAccountStatusService;)Lcom/squareup/server/account/protos/AccountStatusResponse;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 56
    invoke-virtual {p0}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->getStatus()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object p0

    return-object p0
.end method

.method static provideUserId(Lcom/squareup/accountstatus/PersistentAccountStatusService;)Ljava/lang/String;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 35
    invoke-virtual {p0}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->getStatus()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    if-eqz p0, :cond_0

    .line 36
    iget-object p0, p0, Lcom/squareup/server/account/protos/User;->id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method static provideUserToken(Lcom/squareup/accountstatus/PersistentAccountStatusService;)Ljava/lang/String;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 42
    invoke-virtual {p0}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->getStatus()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    if-eqz p0, :cond_0

    .line 43
    iget-object p0, p0, Lcom/squareup/server/account/protos/User;->token:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method


# virtual methods
.method abstract provideAccountStatus(Lcom/squareup/accountstatus/LegacyAccountStatusProvider;)Lcom/squareup/accountstatus/AccountStatusProvider;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideQuietServerPreferences(Lcom/squareup/accountstatus/PersistentAccountStatusService;)Lcom/squareup/accountstatus/QuietServerPreferences;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTransitionalAccountStatusResponseCache(Lcom/squareup/accountstatus/LegacyAccountStatusResponseCache;)Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
