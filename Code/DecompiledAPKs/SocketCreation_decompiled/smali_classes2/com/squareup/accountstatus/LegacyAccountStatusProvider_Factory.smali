.class public final Lcom/squareup/accountstatus/LegacyAccountStatusProvider_Factory;
.super Ljava/lang/Object;
.source "LegacyAccountStatusProvider_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/accountstatus/LegacyAccountStatusProvider;",
        ">;"
    }
.end annotation


# instance fields
.field private final delegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;"
        }
    .end annotation
.end field

.field private final loggedInStatusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LoggedInStatusProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionTokenProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/SessionIdPIIProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/SessionIdPIIProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LoggedInStatusProvider;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/accountstatus/LegacyAccountStatusProvider_Factory;->delegateProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/accountstatus/LegacyAccountStatusProvider_Factory;->sessionTokenProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p3, p0, Lcom/squareup/accountstatus/LegacyAccountStatusProvider_Factory;->loggedInStatusProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/accountstatus/LegacyAccountStatusProvider_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/SessionIdPIIProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LoggedInStatusProvider;",
            ">;)",
            "Lcom/squareup/accountstatus/LegacyAccountStatusProvider_Factory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/accountstatus/LegacyAccountStatusProvider_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/accountstatus/LegacyAccountStatusProvider_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/account/SessionIdPIIProvider;Lcom/squareup/account/LoggedInStatusProvider;)Lcom/squareup/accountstatus/LegacyAccountStatusProvider;
    .locals 1

    .line 43
    new-instance v0, Lcom/squareup/accountstatus/LegacyAccountStatusProvider;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/accountstatus/LegacyAccountStatusProvider;-><init>(Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/account/SessionIdPIIProvider;Lcom/squareup/account/LoggedInStatusProvider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/accountstatus/LegacyAccountStatusProvider;
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/squareup/accountstatus/LegacyAccountStatusProvider_Factory;->delegateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accountstatus/PersistentAccountStatusService;

    iget-object v1, p0, Lcom/squareup/accountstatus/LegacyAccountStatusProvider_Factory;->sessionTokenProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/account/SessionIdPIIProvider;

    iget-object v2, p0, Lcom/squareup/accountstatus/LegacyAccountStatusProvider_Factory;->loggedInStatusProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/account/LoggedInStatusProvider;

    invoke-static {v0, v1, v2}, Lcom/squareup/accountstatus/LegacyAccountStatusProvider_Factory;->newInstance(Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/account/SessionIdPIIProvider;Lcom/squareup/account/LoggedInStatusProvider;)Lcom/squareup/accountstatus/LegacyAccountStatusProvider;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/accountstatus/LegacyAccountStatusProvider_Factory;->get()Lcom/squareup/accountstatus/LegacyAccountStatusProvider;

    move-result-object v0

    return-object v0
.end method
