.class public abstract Lcom/squareup/CommonPosApp;
.super Landroid/app/Application;
.source "CommonPosApp.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract createAppDelegate()Lcom/squareup/app/HasOnCreate;
.end method

.method public onCreate()V
    .locals 1

    .line 26
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 27
    invoke-virtual {p0}, Lcom/squareup/CommonPosApp;->createAppDelegate()Lcom/squareup/app/HasOnCreate;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/app/HasOnCreate;->onCreate()V

    return-void
.end method
