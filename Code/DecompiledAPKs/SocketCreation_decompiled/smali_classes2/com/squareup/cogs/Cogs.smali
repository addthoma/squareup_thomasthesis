.class public interface abstract Lcom/squareup/cogs/Cogs;
.super Ljava/lang/Object;
.source "Cogs.java"

# interfaces
.implements Lcom/squareup/shared/catalog/Catalog;


# virtual methods
.method public abstract asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/CatalogTask<",
            "TT;>;)",
            "Lrx/Single<",
            "TT;>;"
        }
    .end annotation
.end method

.method public abstract asSingleOnline(Lcom/squareup/shared/catalog/CatalogOnlineTask;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/CatalogOnlineTask<",
            "TT;>;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "TT;>;>;"
        }
    .end annotation
.end method

.method public abstract asSingleOnlineThenLocal(Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask<",
            "TT;TS;>;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "TS;>;>;"
        }
    .end annotation
.end method

.method public abstract cogsSyncProgress()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/CatalogTask<",
            "TT;>;",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "TT;>;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract executeOnline(Lcom/squareup/shared/catalog/CatalogOnlineTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/CatalogOnlineTask<",
            "TT;>;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "TT;>;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract executeOnlineThenLocal(Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask<",
            "TT;TS;>;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "TS;>;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract foregroundSyncAsSingle(Lcom/squareup/shared/catalog/utils/ElapsedTime;J)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/utils/ElapsedTime;",
            "J)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract shouldForegroundSyncAsSingle(Lcom/squareup/shared/catalog/utils/ElapsedTime;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/utils/ElapsedTime;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/CatalogResult<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract syncAsSingle()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation
.end method
