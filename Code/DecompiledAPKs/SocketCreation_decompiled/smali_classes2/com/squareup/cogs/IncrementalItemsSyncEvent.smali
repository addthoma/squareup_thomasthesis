.class public Lcom/squareup/cogs/IncrementalItemsSyncEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "IncrementalItemsSyncEvent.java"


# instance fields
.field public final number_of_objects:I


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;)V
    .locals 2

    .line 14
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->INCREMENTAL_ITEMS_SYNC:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 15
    iget v0, p1, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->numberOfObjects:I

    const/16 v1, 0x3e8

    if-lt v0, v1, :cond_0

    .line 19
    iget p1, p1, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;->numberOfObjects:I

    iput p1, p0, Lcom/squareup/cogs/IncrementalItemsSyncEvent;->number_of_objects:I

    return-void

    .line 16
    :cond_0
    new-instance p1, Ljava/security/InvalidParameterException;

    const-string v0, "Only logged if number_of_objects is not smaller than 1000"

    invoke-direct {p1, v0}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
