.class public Lcom/squareup/cogs/CatalogUpdateEvent;
.super Ljava/lang/Object;
.source "CatalogUpdateEvent.java"


# instance fields
.field private final deleted:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final hasMultipleUpdateBatches:Z

.field private final isItemSuggestionDeleted:Z

.field private final updated:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final updatedTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Collection;Ljava/util/Collection;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;Z)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/cogs/CatalogUpdateEvent;->updated:Ljava/util/Collection;

    .line 33
    iput-object p2, p0, Lcom/squareup/cogs/CatalogUpdateEvent;->deleted:Ljava/util/Collection;

    .line 34
    iput-boolean p3, p0, Lcom/squareup/cogs/CatalogUpdateEvent;->hasMultipleUpdateBatches:Z

    .line 35
    new-instance p3, Ljava/util/HashSet;

    invoke-direct {p3}, Ljava/util/HashSet;-><init>()V

    iput-object p3, p0, Lcom/squareup/cogs/CatalogUpdateEvent;->updatedTypes:Ljava/util/Set;

    .line 36
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/shared/catalog/models/CatalogObject;

    .line 37
    iget-object v0, p0, Lcom/squareup/cogs/CatalogUpdateEvent;->updatedTypes:Ljava/util/Set;

    invoke-virtual {p3}, Lcom/squareup/shared/catalog/models/CatalogObject;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object p3

    invoke-interface {v0, p3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 39
    :cond_0
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogObject;

    .line 40
    iget-object p3, p0, Lcom/squareup/cogs/CatalogUpdateEvent;->updatedTypes:Ljava/util/Set;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogObject;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object p2

    invoke-interface {p3, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    .line 42
    iput-boolean p1, p0, Lcom/squareup/cogs/CatalogUpdateEvent;->isItemSuggestionDeleted:Z

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cogs/CatalogUpdateEvent;->updated:Ljava/util/Collection;

    .line 47
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cogs/CatalogUpdateEvent;->deleted:Ljava/util/Collection;

    const/4 v0, 0x0

    .line 48
    iput-boolean v0, p0, Lcom/squareup/cogs/CatalogUpdateEvent;->hasMultipleUpdateBatches:Z

    .line 49
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cogs/CatalogUpdateEvent;->updatedTypes:Ljava/util/Set;

    .line 50
    iput-boolean p1, p0, Lcom/squareup/cogs/CatalogUpdateEvent;->isItemSuggestionDeleted:Z

    return-void
.end method


# virtual methods
.method public getDeleted()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;"
        }
    .end annotation

    .line 74
    iget-boolean v0, p0, Lcom/squareup/cogs/CatalogUpdateEvent;->hasMultipleUpdateBatches:Z

    if-nez v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/squareup/cogs/CatalogUpdateEvent;->deleted:Ljava/util/Collection;

    return-object v0

    .line 75
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "deleted is undefined when hasMultipleUpdateBatches is true."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getUpdated()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;"
        }
    .end annotation

    .line 66
    iget-boolean v0, p0, Lcom/squareup/cogs/CatalogUpdateEvent;->hasMultipleUpdateBatches:Z

    if-nez v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/squareup/cogs/CatalogUpdateEvent;->updated:Ljava/util/Collection;

    return-object v0

    .line 67
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "updated is undefined when hasMultipleUpdateBatches is true."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public hasMultipleUpdateBatches()Z
    .locals 1

    .line 82
    iget-boolean v0, p0, Lcom/squareup/cogs/CatalogUpdateEvent;->hasMultipleUpdateBatches:Z

    return v0
.end method

.method public varargs hasOneOf([Lcom/squareup/shared/catalog/models/CatalogObjectType;)Z
    .locals 6

    .line 54
    iget-boolean v0, p0, Lcom/squareup/cogs/CatalogUpdateEvent;->hasMultipleUpdateBatches:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 57
    :cond_0
    array-length v0, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_2

    aget-object v4, p1, v3

    .line 58
    iget-object v5, p0, Lcom/squareup/cogs/CatalogUpdateEvent;->updatedTypes:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    return v1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    return v2
.end method

.method public isItemSuggestionDeleted()Z
    .locals 1

    .line 86
    iget-boolean v0, p0, Lcom/squareup/cogs/CatalogUpdateEvent;->isItemSuggestionDeleted:Z

    return v0
.end method
