.class public final Lcom/squareup/cogs/CogsPurger;
.super Ljava/lang/Object;
.source "CogsPurger.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\u0008\u0010\u000b\u001a\u00020\u0008H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/cogs/CogsPurger;",
        "Lmortar/Scoped;",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "(Lcom/squareup/cogs/Cogs;)V",
        "getCogs",
        "()Lcom/squareup/cogs/Cogs;",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "cogs_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cogs:Lcom/squareup/cogs/Cogs;


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/Cogs;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cogs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cogs/CogsPurger;->cogs:Lcom/squareup/cogs/Cogs;

    return-void
.end method


# virtual methods
.method public final getCogs()Lcom/squareup/cogs/Cogs;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/cogs/CogsPurger;->cogs:Lcom/squareup/cogs/Cogs;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/cogs/CogsPurger;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-interface {v0}, Lcom/squareup/cogs/Cogs;->purge()V

    return-void
.end method
