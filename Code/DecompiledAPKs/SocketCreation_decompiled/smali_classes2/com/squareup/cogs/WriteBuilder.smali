.class public Lcom/squareup/cogs/WriteBuilder;
.super Ljava/lang/Object;
.source "WriteBuilder.java"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogTask<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final deleted:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final updated:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/cogs/WriteBuilder;->updated:Ljava/util/Collection;

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/cogs/WriteBuilder;->deleted:Ljava/util/Collection;

    return-void
.end method


# virtual methods
.method public delete(Lcom/squareup/shared/catalog/models/CatalogObject;)Lcom/squareup/cogs/WriteBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;)",
            "Lcom/squareup/cogs/WriteBuilder;"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/squareup/cogs/WriteBuilder;->deleted:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public delete(Ljava/util/Collection;)Lcom/squareup/cogs/WriteBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;)",
            "Lcom/squareup/cogs/WriteBuilder;"
        }
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/squareup/cogs/WriteBuilder;->deleted:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public bridge synthetic perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 0

    .line 11
    invoke-virtual {p0, p1}, Lcom/squareup/cogs/WriteBuilder;->perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Void;
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/cogs/WriteBuilder;->updated:Ljava/util/Collection;

    iget-object v1, p0, Lcom/squareup/cogs/WriteBuilder;->deleted:Ljava/util/Collection;

    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->write(Ljava/util/Collection;Ljava/util/Collection;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public update(Lcom/squareup/shared/catalog/models/CatalogObject;)Lcom/squareup/cogs/WriteBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;)",
            "Lcom/squareup/cogs/WriteBuilder;"
        }
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/squareup/cogs/WriteBuilder;->updated:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public update(Ljava/util/Collection;)Lcom/squareup/cogs/WriteBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;)",
            "Lcom/squareup/cogs/WriteBuilder;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/cogs/WriteBuilder;->updated:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method
