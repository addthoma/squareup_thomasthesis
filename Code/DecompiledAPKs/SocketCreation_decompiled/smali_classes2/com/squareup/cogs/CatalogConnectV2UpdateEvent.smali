.class public final Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;
.super Ljava/lang/Object;
.source "CatalogConnectV2UpdateEvent.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cogs/CatalogConnectV2UpdateEvent$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCatalogConnectV2UpdateEvent.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CatalogConnectV2UpdateEvent.kt\ncom/squareup/cogs/CatalogConnectV2UpdateEvent\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,86:1\n1360#2:87\n1429#2,3:88\n1360#2:91\n1429#2,3:92\n*E\n*S KotlinDebug\n*F\n+ 1 CatalogConnectV2UpdateEvent.kt\ncom/squareup/cogs/CatalogConnectV2UpdateEvent\n*L\n31#1:87\n31#1,3:88\n31#1:91\n31#1,3:92\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0011\n\u0002\u0008\u0003\u0018\u0000 \u00172\u00020\u0001:\u0001\u0017B+\u0008\u0012\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u001f\u0010\u0013\u001a\u00020\u00072\u0012\u0010\u0014\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00110\u0015\"\u00020\u0011\u00a2\u0006\u0002\u0010\u0016R\u0016\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0016\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u000c\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;",
        "",
        "updated",
        "",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
        "deleted",
        "hasMultipleUpdateBatches",
        "",
        "(Ljava/util/Collection;Ljava/util/Collection;Z)V",
        "_deleted",
        "_updated",
        "getDeleted",
        "()Ljava/util/Collection;",
        "getHasMultipleUpdateBatches",
        "()Z",
        "typesInEvent",
        "",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;",
        "getUpdated",
        "hasOneOf",
        "types",
        "",
        "([Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;)Z",
        "Companion",
        "cogs_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/cogs/CatalogConnectV2UpdateEvent$Companion;


# instance fields
.field private final _deleted:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;"
        }
    .end annotation
.end field

.field private final _updated:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;"
        }
    .end annotation
.end field

.field private final hasMultipleUpdateBatches:Z

.field private final typesInEvent:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;->Companion:Lcom/squareup/cogs/CatalogConnectV2UpdateEvent$Companion;

    return-void
.end method

.method private constructor <init>(Ljava/util/Collection;Ljava/util/Collection;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;Z)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p3, p0, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;->hasMultipleUpdateBatches:Z

    if-nez p3, :cond_2

    .line 29
    iput-object p1, p0, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;->_updated:Ljava/util/Collection;

    .line 30
    iput-object p2, p0, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;->_deleted:Ljava/util/Collection;

    .line 31
    iget-object p1, p0, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;->_updated:Ljava/util/Collection;

    check-cast p1, Ljava/lang/Iterable;

    .line 87
    new-instance p2, Ljava/util/ArrayList;

    const/16 p3, 0xa

    invoke-static {p1, p3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-direct {p2, v0}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p2, Ljava/util/Collection;

    .line 88
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 89
    check-cast v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    .line 31
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;->getType()Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 90
    :cond_0
    check-cast p2, Ljava/util/List;

    check-cast p2, Ljava/lang/Iterable;

    .line 31
    iget-object p1, p0, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;->_deleted:Ljava/util/Collection;

    check-cast p1, Ljava/lang/Iterable;

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1, p3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result p3

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 92
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    .line 93
    check-cast p3, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    .line 31
    invoke-virtual {p3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;->getType()Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;

    move-result-object p3

    check-cast p3, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    invoke-interface {v0, p3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 94
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 31
    invoke-static {p2, v0}, Lkotlin/collections/CollectionsKt;->union(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;->typesInEvent:Ljava/util/Set;

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    .line 33
    move-object p2, p1

    check-cast p2, Ljava/util/Collection;

    iput-object p2, p0, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;->_updated:Ljava/util/Collection;

    .line 34
    iput-object p2, p0, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;->_deleted:Ljava/util/Collection;

    .line 35
    check-cast p1, Ljava/util/Set;

    iput-object p1, p0, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;->typesInEvent:Ljava/util/Set;

    :goto_2
    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/Collection;Ljava/util/Collection;ZLkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;-><init>(Ljava/util/Collection;Ljava/util/Collection;Z)V

    return-void
.end method

.method public static final of(Ljava/util/Collection;Ljava/util/Collection;Z)Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;Z)",
            "Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;->Companion:Lcom/squareup/cogs/CatalogConnectV2UpdateEvent$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent$Companion;->of(Ljava/util/Collection;Ljava/util/Collection;Z)Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final getDeleted()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;"
        }
    .end annotation

    .line 56
    iget-boolean v0, p0, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;->hasMultipleUpdateBatches:Z

    if-nez v0, :cond_1

    .line 59
    iget-object v0, p0, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;->_deleted:Ljava/util/Collection;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    return-object v0

    .line 57
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "deleted is undefined when hasMultipleUpdateBatches is true."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final getHasMultipleUpdateBatches()Z
    .locals 1

    .line 19
    iget-boolean v0, p0, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;->hasMultipleUpdateBatches:Z

    return v0
.end method

.method public final getUpdated()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;"
        }
    .end annotation

    .line 45
    iget-boolean v0, p0, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;->hasMultipleUpdateBatches:Z

    if-nez v0, :cond_1

    .line 48
    iget-object v0, p0, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;->_updated:Ljava/util/Collection;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    return-object v0

    .line 46
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "updated is undefined when hasMultipleUpdateBatches is true."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final varargs hasOneOf([Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;)Z
    .locals 6

    const-string v0, "types"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iget-boolean v0, p0, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;->hasMultipleUpdateBatches:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 68
    :cond_0
    array-length v0, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_3

    aget-object v4, p1, v3

    .line 69
    iget-object v5, p0, Lcom/squareup/cogs/CatalogConnectV2UpdateEvent;->typesInEvent:Ljava/util/Set;

    if-nez v5, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-interface {v5, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    return v1

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    return v2
.end method
