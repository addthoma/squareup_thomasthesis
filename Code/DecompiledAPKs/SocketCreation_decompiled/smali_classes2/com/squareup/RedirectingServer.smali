.class public Lcom/squareup/RedirectingServer;
.super Lcom/squareup/http/Server;
.source "RedirectingServer.java"


# instance fields
.field private final accountStatusSettingsApiUrl:Lcom/squareup/settings/server/AccountStatusSettingsApiUrl;

.field private final urlRedirectSetting:Lcom/squareup/http/UrlRedirectSetting;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/settings/server/AccountStatusSettingsApiUrl;Lcom/squareup/http/UrlRedirectSetting;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/http/Server;-><init>(Ljava/lang/String;)V

    .line 21
    iput-object p2, p0, Lcom/squareup/RedirectingServer;->accountStatusSettingsApiUrl:Lcom/squareup/settings/server/AccountStatusSettingsApiUrl;

    .line 22
    iput-object p3, p0, Lcom/squareup/RedirectingServer;->urlRedirectSetting:Lcom/squareup/http/UrlRedirectSetting;

    return-void
.end method


# virtual methods
.method public getUrl()Ljava/lang/String;
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/squareup/RedirectingServer;->urlRedirectSetting:Lcom/squareup/http/UrlRedirectSetting;

    invoke-interface {v0}, Lcom/squareup/http/UrlRedirectSetting;->isUrlRedirectEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 27
    iget-object v0, p0, Lcom/squareup/RedirectingServer;->accountStatusSettingsApiUrl:Lcom/squareup/settings/server/AccountStatusSettingsApiUrl;

    invoke-interface {v0}, Lcom/squareup/settings/server/AccountStatusSettingsApiUrl;->getApiUrl()Ljava/lang/String;

    move-result-object v0

    .line 28
    iget-object v1, p0, Lcom/squareup/RedirectingServer;->accountStatusSettingsApiUrl:Lcom/squareup/settings/server/AccountStatusSettingsApiUrl;

    invoke-interface {v1}, Lcom/squareup/settings/server/AccountStatusSettingsApiUrl;->getApiUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    return-object v0

    .line 33
    :cond_0
    invoke-super {p0}, Lcom/squareup/http/Server;->getUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
