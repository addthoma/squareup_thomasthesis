.class public final Lcom/squareup/blueprint/mosaic/BlueprintUiModelKt;
.super Ljava/lang/Object;
.source "BlueprintUiModel.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBlueprintUiModel.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BlueprintUiModel.kt\ncom/squareup/blueprint/mosaic/BlueprintUiModelKt\n*L\n1#1,51:1\n21#1,7:52\n26#1:59\n22#1,7:60\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001aP\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u0008\u0012\u0004\u0012\u0002H\u00020\u00042\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00062\u001d\u0010\u0008\u001a\u0019\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00020\n\u0012\u0004\u0012\u00020\u00010\t\u00a2\u0006\u0002\u0008\u000bH\u0086\u0008\u00a8\u0006\u000c"
    }
    d2 = {
        "blueprint",
        "",
        "P",
        "",
        "Lcom/squareup/mosaic/core/UiModelContext;",
        "extendHorizontally",
        "",
        "extendVertically",
        "block",
        "Lkotlin/Function1;",
        "Lcom/squareup/blueprint/mosaic/BlueprintUiModel;",
        "Lkotlin/ExtensionFunctionType;",
        "blueprint-mosaic_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final blueprint(Lcom/squareup/mosaic/core/UiModelContext;ZZLkotlin/jvm/functions/Function1;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/mosaic/core/UiModelContext<",
            "TP;>;ZZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/blueprint/mosaic/BlueprintUiModel<",
            "TP;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$blueprint"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    new-instance v0, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;

    .line 22
    invoke-interface {p0}, Lcom/squareup/mosaic/core/UiModelContext;->createParams()Ljava/lang/Object;

    move-result-object v2

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v1, v0

    move v3, p1

    move v4, p2

    .line 21
    invoke-direct/range {v1 .. v7}, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;-><init>(Ljava/lang/Object;ZZLcom/squareup/blueprint/Block;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 26
    invoke-interface {p3, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    check-cast v0, Lcom/squareup/mosaic/core/UiModel;

    invoke-interface {p0, v0}, Lcom/squareup/mosaic/core/UiModelContext;->add(Lcom/squareup/mosaic/core/UiModel;)V

    return-void
.end method

.method public static synthetic blueprint$default(Lcom/squareup/mosaic/core/UiModelContext;ZZLkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    .locals 8

    and-int/lit8 p5, p4, 0x1

    const/4 v0, 0x1

    if-eqz p5, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    move v3, p1

    :goto_0
    and-int/lit8 p1, p4, 0x2

    if-eqz p1, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    move v4, p2

    :goto_1
    const-string p1, "$this$blueprint"

    .line 18
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "block"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    new-instance p1, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;

    .line 60
    invoke-interface {p0}, Lcom/squareup/mosaic/core/UiModelContext;->createParams()Ljava/lang/Object;

    move-result-object v2

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v1, p1

    .line 52
    invoke-direct/range {v1 .. v7}, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;-><init>(Ljava/lang/Object;ZZLcom/squareup/blueprint/Block;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 59
    invoke-interface {p3, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    check-cast p1, Lcom/squareup/mosaic/core/UiModel;

    invoke-interface {p0, p1}, Lcom/squareup/mosaic/core/UiModelContext;->add(Lcom/squareup/mosaic/core/UiModel;)V

    return-void
.end method
