.class public abstract Lcom/squareup/blueprint/LinearBlock;
.super Lcom/squareup/blueprint/Block;
.source "LinearBlock.kt"

# interfaces
.implements Lcom/squareup/blueprint/BlueprintContext;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/blueprint/LinearBlock$Params;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C:",
        "Lcom/squareup/blueprint/UpdateContext;",
        "P:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/blueprint/Block<",
        "TC;TP;>;",
        "Lcom/squareup/blueprint/BlueprintContext<",
        "TC;",
        "Lcom/squareup/blueprint/LinearBlock$Params;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLinearBlock.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LinearBlock.kt\ncom/squareup/blueprint/LinearBlock\n+ 2 _Sequences.kt\nkotlin/sequences/SequencesKt___SequencesKt\n*L\n1#1,93:1\n1003#2,2:94\n*E\n*S KotlinDebug\n*F\n+ 1 LinearBlock.kt\ncom/squareup/blueprint/LinearBlock\n*L\n57#1,2:94\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0008&\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00042\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00030\u00052\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u00020\u00070\u0006:\u0001\u001bB\u0005\u00a2\u0006\u0002\u0010\u0008J\u001c\u0010\u000f\u001a\u00020\u00102\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00070\u0005H\u0016J\u0008\u0010\u0012\u001a\u00020\u0007H\u0016J+\u0010\u0013\u001a\u00020\u00102#\u0010\u0014\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0004\u0012\u00020\u00100\u0015\u00a2\u0006\u0002\u0008\u0016J\u0018\u0010\u0017\u001a\u00020\u00182\u000e\u0010\u0014\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0005H\u0016J\u000e\u0010\u0019\u001a\u00020\u00102\u0006\u0010\u001a\u001a\u00020\nR\u000e\u0010\t\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R$\u0010\u000b\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00070\u00050\u000cX\u00a4\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/blueprint/LinearBlock;",
        "C",
        "Lcom/squareup/blueprint/UpdateContext;",
        "P",
        "",
        "Lcom/squareup/blueprint/Block;",
        "Lcom/squareup/blueprint/BlueprintContext;",
        "Lcom/squareup/blueprint/LinearBlock$Params;",
        "()V",
        "currentSpacing",
        "Lcom/squareup/resources/DimenModel;",
        "elements",
        "",
        "getElements",
        "()Ljava/util/List;",
        "add",
        "",
        "element",
        "createParams",
        "extend",
        "block",
        "Lkotlin/Function1;",
        "Lkotlin/ExtensionFunctionType;",
        "isSameLayoutAs",
        "",
        "spacing",
        "value",
        "Params",
        "blueprint-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private currentSpacing:Lcom/squareup/resources/DimenModel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 44
    invoke-direct {p0}, Lcom/squareup/blueprint/Block;-><init>()V

    const/4 v0, 0x0

    .line 53
    invoke-static {v0}, Lcom/squareup/resources/DimenModelsKt;->getDp(I)Lcom/squareup/resources/FixedDimen;

    move-result-object v0

    check-cast v0, Lcom/squareup/resources/DimenModel;

    iput-object v0, p0, Lcom/squareup/blueprint/LinearBlock;->currentSpacing:Lcom/squareup/resources/DimenModel;

    return-void
.end method


# virtual methods
.method public add(Lcom/squareup/blueprint/Block;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lcom/squareup/blueprint/LinearBlock$Params;",
            ">;)V"
        }
    .end annotation

    const-string v0, "element"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-virtual {p1}, Lcom/squareup/blueprint/Block;->getParams()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/blueprint/LinearBlock$Params;

    iget-object v1, p0, Lcom/squareup/blueprint/LinearBlock;->currentSpacing:Lcom/squareup/resources/DimenModel;

    invoke-virtual {v0, v1}, Lcom/squareup/blueprint/LinearBlock$Params;->setSpacing$blueprint_core_release(Lcom/squareup/resources/DimenModel;)V

    .line 73
    invoke-virtual {p0}, Lcom/squareup/blueprint/LinearBlock;->getElements()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    const/4 p1, 0x0

    .line 74
    invoke-static {p1}, Lcom/squareup/resources/DimenModelsKt;->getDp(I)Lcom/squareup/resources/FixedDimen;

    move-result-object p1

    check-cast p1, Lcom/squareup/resources/DimenModel;

    iput-object p1, p0, Lcom/squareup/blueprint/LinearBlock;->currentSpacing:Lcom/squareup/resources/DimenModel;

    return-void
.end method

.method public createParams()Lcom/squareup/blueprint/LinearBlock$Params;
    .locals 7

    .line 69
    new-instance v6, Lcom/squareup/blueprint/LinearBlock$Params;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/blueprint/LinearBlock$Params;-><init>(ZZLcom/squareup/resources/DimenModel;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v6
.end method

.method public bridge synthetic createParams()Ljava/lang/Object;
    .locals 1

    .line 43
    invoke-virtual {p0}, Lcom/squareup/blueprint/LinearBlock;->createParams()Lcom/squareup/blueprint/LinearBlock$Params;

    move-result-object v0

    return-object v0
.end method

.method public final extend(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "TC;",
            "Lcom/squareup/blueprint/LinearBlock$Params;",
            ">;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    new-instance v0, Lcom/squareup/blueprint/LinearBlock$extend$1;

    invoke-direct {v0, p0}, Lcom/squareup/blueprint/LinearBlock$extend$1;-><init>(Lcom/squareup/blueprint/LinearBlock;)V

    .line 82
    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method protected abstract getElements()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lcom/squareup/blueprint/LinearBlock$Params;",
            ">;>;"
        }
    .end annotation
.end method

.method public isSameLayoutAs(Lcom/squareup/blueprint/Block;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blueprint/Block<",
            "**>;)Z"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-super {p0, p1}, Lcom/squareup/blueprint/Block;->isSameLayoutAs(Lcom/squareup/blueprint/Block;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    check-cast p1, Lcom/squareup/blueprint/LinearBlock;

    .line 59
    invoke-virtual {p0}, Lcom/squareup/blueprint/LinearBlock;->getElements()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/LinearBlock;->getElements()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v0, v3, :cond_2

    invoke-virtual {p0}, Lcom/squareup/blueprint/LinearBlock;->getElements()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/LinearBlock;->getElements()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/sequences/SequencesKt;->zip(Lkotlin/sequences/Sequence;Lkotlin/sequences/Sequence;)Lkotlin/sequences/Sequence;

    move-result-object p1

    .line 94
    invoke-interface {p1}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/Pair;

    invoke-virtual {v0}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/blueprint/Block;

    invoke-virtual {v0}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/blueprint/Block;

    .line 60
    invoke-virtual {v3, v0}, Lcom/squareup/blueprint/Block;->isSameLayoutAs(Lcom/squareup/blueprint/Block;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    :goto_0
    if-eqz p1, :cond_2

    const/4 p1, 0x1

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_3

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    return v1
.end method

.method public final spacing(Lcom/squareup/resources/DimenModel;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iput-object p1, p0, Lcom/squareup/blueprint/LinearBlock;->currentSpacing:Lcom/squareup/resources/DimenModel;

    return-void
.end method
