.class public final Lcom/squareup/blueprint/ExistingViewBlock;
.super Lcom/squareup/blueprint/ViewBlock;
.source "ExistingViewBlock.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/blueprint/ViewBlock<",
        "Lcom/squareup/blueprint/ViewsUpdateContext;",
        "TP;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0010\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u0002H\u00010\u0003B\u0017\u0012\u0006\u0010\u0005\u001a\u00028\u0000\u0012\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J \u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u00042\u0006\u0010\u001c\u001a\u00020\u00072\u0006\u0010\u001d\u001a\u00020\u0007H\u0016J\u000e\u0010\u001e\u001a\u00028\u0000H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000eJ\t\u0010\u001f\u001a\u00020\u0007H\u00c6\u0003J(\u0010 \u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00002\u0008\u0008\u0002\u0010\u0005\u001a\u00028\u00002\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001\u00a2\u0006\u0002\u0010!J\u0013\u0010\"\u001a\u00020\u00112\u0008\u0010#\u001a\u0004\u0018\u00010\u0002H\u00d6\u0003J\t\u0010$\u001a\u00020\u0007H\u00d6\u0001J\t\u0010%\u001a\u00020&H\u00d6\u0001R\u001a\u0010\u0006\u001a\u00020\u0007X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\t\u0010\n\"\u0004\u0008\u000b\u0010\u000cR\u0016\u0010\u0005\u001a\u00028\u0000X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u000f\u001a\u0004\u0008\r\u0010\u000eR\u001a\u0010\u0010\u001a\u00020\u0011X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\"\u0004\u0008\u0014\u0010\u0015R\u001a\u0010\u0016\u001a\u00020\u0011X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0017\u0010\u0013\"\u0004\u0008\u0018\u0010\u0015\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/blueprint/ExistingViewBlock;",
        "P",
        "",
        "Lcom/squareup/blueprint/ViewBlock;",
        "Lcom/squareup/blueprint/ViewsUpdateContext;",
        "params",
        "id",
        "",
        "(Ljava/lang/Object;I)V",
        "getId",
        "()I",
        "setId",
        "(I)V",
        "getParams",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "wrapHorizontally",
        "",
        "getWrapHorizontally",
        "()Z",
        "setWrapHorizontally",
        "(Z)V",
        "wrapVertically",
        "getWrapVertically",
        "setWrapVertically",
        "buildView",
        "",
        "updateContext",
        "width",
        "height",
        "component1",
        "component2",
        "copy",
        "(Ljava/lang/Object;I)Lcom/squareup/blueprint/ExistingViewBlock;",
        "equals",
        "other",
        "hashCode",
        "toString",
        "",
        "blueprint-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private id:I

.field private final params:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field

.field private wrapHorizontally:Z

.field private wrapVertically:Z


# direct methods
.method public constructor <init>(Ljava/lang/Object;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;I)V"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Lcom/squareup/blueprint/ViewBlock;-><init>()V

    iput-object p1, p0, Lcom/squareup/blueprint/ExistingViewBlock;->params:Ljava/lang/Object;

    iput p2, p0, Lcom/squareup/blueprint/ExistingViewBlock;->id:I

    const/4 p1, 0x1

    .line 17
    iput-boolean p1, p0, Lcom/squareup/blueprint/ExistingViewBlock;->wrapHorizontally:Z

    .line 18
    iput-boolean p1, p0, Lcom/squareup/blueprint/ExistingViewBlock;->wrapVertically:Z

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, -0x1

    .line 14
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/blueprint/ExistingViewBlock;-><init>(Ljava/lang/Object;I)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/blueprint/ExistingViewBlock;Ljava/lang/Object;IILjava/lang/Object;)Lcom/squareup/blueprint/ExistingViewBlock;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/squareup/blueprint/ExistingViewBlock;->getParams()Ljava/lang/Object;

    move-result-object p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    invoke-virtual {p0}, Lcom/squareup/blueprint/ExistingViewBlock;->getId()I

    move-result p2

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/blueprint/ExistingViewBlock;->copy(Ljava/lang/Object;I)Lcom/squareup/blueprint/ExistingViewBlock;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public bridge synthetic buildView(Lcom/squareup/blueprint/UpdateContext;II)V
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/blueprint/ViewsUpdateContext;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/blueprint/ExistingViewBlock;->buildView(Lcom/squareup/blueprint/ViewsUpdateContext;II)V

    return-void
.end method

.method public buildView(Lcom/squareup/blueprint/ViewsUpdateContext;II)V
    .locals 1

    const-string v0, "updateContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-virtual {p0}, Lcom/squareup/blueprint/ExistingViewBlock;->getId()I

    move-result v0

    invoke-virtual {p1, v0, p2, p3}, Lcom/squareup/blueprint/ViewsUpdateContext;->addView(III)V

    return-void
.end method

.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/blueprint/ExistingViewBlock;->getParams()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final component2()I
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/blueprint/ExistingViewBlock;->getId()I

    move-result v0

    return v0
.end method

.method public final copy(Ljava/lang/Object;I)Lcom/squareup/blueprint/ExistingViewBlock;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;I)",
            "Lcom/squareup/blueprint/ExistingViewBlock<",
            "TP;>;"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/blueprint/ExistingViewBlock;

    invoke-direct {v0, p1, p2}, Lcom/squareup/blueprint/ExistingViewBlock;-><init>(Ljava/lang/Object;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/blueprint/ExistingViewBlock;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/blueprint/ExistingViewBlock;

    invoke-virtual {p0}, Lcom/squareup/blueprint/ExistingViewBlock;->getParams()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/ExistingViewBlock;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/blueprint/ExistingViewBlock;->getId()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/ExistingViewBlock;->getId()I

    move-result p1

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getId()I
    .locals 1

    .line 14
    iget v0, p0, Lcom/squareup/blueprint/ExistingViewBlock;->id:I

    return v0
.end method

.method public getParams()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .line 13
    iget-object v0, p0, Lcom/squareup/blueprint/ExistingViewBlock;->params:Ljava/lang/Object;

    return-object v0
.end method

.method public getWrapHorizontally()Z
    .locals 1

    .line 17
    iget-boolean v0, p0, Lcom/squareup/blueprint/ExistingViewBlock;->wrapHorizontally:Z

    return v0
.end method

.method public getWrapVertically()Z
    .locals 1

    .line 18
    iget-boolean v0, p0, Lcom/squareup/blueprint/ExistingViewBlock;->wrapVertically:Z

    return v0
.end method

.method public hashCode()I
    .locals 2

    invoke-virtual {p0}, Lcom/squareup/blueprint/ExistingViewBlock;->getParams()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/blueprint/ExistingViewBlock;->getId()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public setId(I)V
    .locals 0

    .line 14
    iput p1, p0, Lcom/squareup/blueprint/ExistingViewBlock;->id:I

    return-void
.end method

.method public setWrapHorizontally(Z)V
    .locals 0

    .line 17
    iput-boolean p1, p0, Lcom/squareup/blueprint/ExistingViewBlock;->wrapHorizontally:Z

    return-void
.end method

.method public setWrapVertically(Z)V
    .locals 0

    .line 18
    iput-boolean p1, p0, Lcom/squareup/blueprint/ExistingViewBlock;->wrapVertically:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ExistingViewBlock(params="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/blueprint/ExistingViewBlock;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/blueprint/ExistingViewBlock;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
