.class final Lcom/squareup/blueprint/IdAndMargin$hookForTheSameSide$2;
.super Lkotlin/jvm/internal/Lambda;
.source "IdsAndMargins.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/blueprint/IdAndMargin;->hookForTheSameSide(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;)Lcom/squareup/blueprint/IdAndMargin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "",
        "dependentMargin",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $canSubstractMargin:Lkotlin/jvm/internal/Ref$BooleanRef;

.field final synthetic this$0:Lcom/squareup/blueprint/IdAndMargin;


# direct methods
.method constructor <init>(Lcom/squareup/blueprint/IdAndMargin;Lkotlin/jvm/internal/Ref$BooleanRef;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/blueprint/IdAndMargin$hookForTheSameSide$2;->this$0:Lcom/squareup/blueprint/IdAndMargin;

    iput-object p2, p0, Lcom/squareup/blueprint/IdAndMargin$hookForTheSameSide$2;->$canSubstractMargin:Lkotlin/jvm/internal/Ref$BooleanRef;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/blueprint/IdAndMargin$hookForTheSameSide$2;->invoke(II)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(II)V
    .locals 0

    .line 90
    iget-object p1, p0, Lcom/squareup/blueprint/IdAndMargin$hookForTheSameSide$2;->this$0:Lcom/squareup/blueprint/IdAndMargin;

    invoke-virtual {p1}, Lcom/squareup/blueprint/IdAndMargin;->getMargin()I

    move-result p1

    if-ge p2, p1, :cond_0

    iget-object p1, p0, Lcom/squareup/blueprint/IdAndMargin$hookForTheSameSide$2;->$canSubstractMargin:Lkotlin/jvm/internal/Ref$BooleanRef;

    const/4 p2, 0x0

    iput-boolean p2, p1, Lkotlin/jvm/internal/Ref$BooleanRef;->element:Z

    :cond_0
    return-void
.end method
