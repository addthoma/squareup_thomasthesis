.class public final Lcom/squareup/blueprint/BlockKt;
.super Ljava/lang/Object;
.source "Block.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u001a\u001c\u0010\u0000\u001a\u00020\u0001*\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00022\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "dimen",
        "Lcom/squareup/resources/DimenModel;",
        "Lcom/squareup/blueprint/Block;",
        "id",
        "",
        "blueprint-core_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final dimen(Lcom/squareup/blueprint/Block;I)Lcom/squareup/resources/DimenModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blueprint/Block<",
            "**>;I)",
            "Lcom/squareup/resources/DimenModel;"
        }
    .end annotation

    const-string v0, "$this$dimen"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 211
    new-instance p0, Lcom/squareup/resources/ResourceDimen;

    invoke-direct {p0, p1}, Lcom/squareup/resources/ResourceDimen;-><init>(I)V

    check-cast p0, Lcom/squareup/resources/DimenModel;

    return-object p0
.end method
