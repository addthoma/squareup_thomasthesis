.class final Lcom/squareup/blueprint/IdAndMarginCollection$withExtraMargin$$inlined$also$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "IdsAndMargins.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/blueprint/IdAndMarginCollection;->withExtraMargin(I)Lcom/squareup/blueprint/IdAndMarginCollection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "id",
        "",
        "margin",
        "invoke",
        "com/squareup/blueprint/IdAndMarginCollection$withExtraMargin$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $delta$inlined:I

.field final synthetic $result:Lcom/squareup/blueprint/IdAndMarginCollection;

.field final synthetic this$0:Lcom/squareup/blueprint/IdAndMarginCollection;


# direct methods
.method constructor <init>(Lcom/squareup/blueprint/IdAndMarginCollection;Lcom/squareup/blueprint/IdAndMarginCollection;I)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/blueprint/IdAndMarginCollection$withExtraMargin$$inlined$also$lambda$1;->$result:Lcom/squareup/blueprint/IdAndMarginCollection;

    iput-object p2, p0, Lcom/squareup/blueprint/IdAndMarginCollection$withExtraMargin$$inlined$also$lambda$1;->this$0:Lcom/squareup/blueprint/IdAndMarginCollection;

    iput p3, p0, Lcom/squareup/blueprint/IdAndMarginCollection$withExtraMargin$$inlined$also$lambda$1;->$delta$inlined:I

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 172
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/blueprint/IdAndMarginCollection$withExtraMargin$$inlined$also$lambda$1;->invoke(II)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(II)V
    .locals 2

    .line 199
    iget-object v0, p0, Lcom/squareup/blueprint/IdAndMarginCollection$withExtraMargin$$inlined$also$lambda$1;->$result:Lcom/squareup/blueprint/IdAndMarginCollection;

    iget v1, p0, Lcom/squareup/blueprint/IdAndMarginCollection$withExtraMargin$$inlined$also$lambda$1;->$delta$inlined:I

    add-int/2addr p2, v1

    invoke-virtual {v0, p1, p2}, Lcom/squareup/blueprint/IdAndMarginCollection;->add(II)V

    return-void
.end method
