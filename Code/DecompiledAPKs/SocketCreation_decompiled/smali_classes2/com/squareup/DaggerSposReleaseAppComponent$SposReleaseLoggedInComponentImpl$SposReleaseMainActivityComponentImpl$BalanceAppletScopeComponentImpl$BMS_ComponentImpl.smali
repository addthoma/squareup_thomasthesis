.class final Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl$BMS_ComponentImpl;
.super Ljava/lang/Object;
.source "DaggerSposReleaseAppComponent.java"

# interfaces
.implements Lcom/squareup/ui/balance/BalanceMasterScreen$Component;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BMS_ComponentImpl"
.end annotation


# instance fields
.field private badgePresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;


# direct methods
.method private constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;)V
    .locals 0

    .line 15041
    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl$BMS_ComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15043
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl$BMS_ComponentImpl;->initialize()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V
    .locals 0

    .line 15038
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl$BMS_ComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;)V

    return-void
.end method

.method private initialize()V
    .locals 2

    .line 15048
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl$BMS_ComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$51300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;->create()Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/applet/BadgePresenter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/applet/BadgePresenter_Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl$BMS_ComponentImpl;->badgePresenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private injectAppletMasterView(Lcom/squareup/applet/AppletMasterView;)Lcom/squareup/applet/AppletMasterView;
    .locals 1

    .line 15077
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl$BMS_ComponentImpl;->badgePresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/BadgePresenter;

    invoke-static {p1, v0}, Lcom/squareup/applet/AppletMasterView_MembersInjector;->injectBadgePresenter(Lcom/squareup/applet/AppletMasterView;Lcom/squareup/applet/BadgePresenter;)V

    .line 15078
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl$BMS_ComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->access$51500(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletMasterViewPresenter;

    invoke-static {p1, v0}, Lcom/squareup/applet/AppletMasterView_MembersInjector;->injectPresenter(Lcom/squareup/applet/AppletMasterView;Lcom/squareup/applet/AppletMasterViewPresenter;)V

    return-object p1
.end method

.method private injectAppletSectionsListView(Lcom/squareup/applet/AppletSectionsListView;)Lcom/squareup/applet/AppletSectionsListView;
    .locals 1

    .line 15072
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl$BMS_ComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->access$51400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletSectionsListPresenter;

    invoke-static {p1, v0}, Lcom/squareup/applet/AppletSectionsListView_MembersInjector;->injectPresenter(Lcom/squareup/applet/AppletSectionsListView;Lcom/squareup/applet/AppletSectionsListPresenter;)V

    return-object p1
.end method

.method private injectBalanceAppletSectionsListView(Lcom/squareup/ui/balance/BalanceAppletSectionsListView;)Lcom/squareup/ui/balance/BalanceAppletSectionsListView;
    .locals 1

    .line 15065
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl$BMS_ComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->access$51400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletSectionsListPresenter;

    invoke-static {p1, v0}, Lcom/squareup/applet/AppletSectionsListView_MembersInjector;->injectPresenter(Lcom/squareup/applet/AppletSectionsListView;Lcom/squareup/applet/AppletSectionsListPresenter;)V

    .line 15066
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl$BMS_ComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;->access$51400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/balance/BalanceAppletSectionsListView_MembersInjector;->injectPresenter(Lcom/squareup/ui/balance/BalanceAppletSectionsListView;Lcom/squareup/ui/balance/BalanceAppletSectionsListPresenter;)V

    return-object p1
.end method


# virtual methods
.method public inject(Lcom/squareup/applet/AppletMasterView;)V
    .locals 0

    .line 15061
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl$BMS_ComponentImpl;->injectAppletMasterView(Lcom/squareup/applet/AppletMasterView;)Lcom/squareup/applet/AppletMasterView;

    return-void
.end method

.method public inject(Lcom/squareup/applet/AppletSectionsListView;)V
    .locals 0

    .line 15057
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl$BMS_ComponentImpl;->injectAppletSectionsListView(Lcom/squareup/applet/AppletSectionsListView;)Lcom/squareup/applet/AppletSectionsListView;

    return-void
.end method

.method public inject(Lcom/squareup/ui/balance/BalanceAppletSectionsListView;)V
    .locals 0

    .line 15053
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BalanceAppletScopeComponentImpl$BMS_ComponentImpl;->injectBalanceAppletSectionsListView(Lcom/squareup/ui/balance/BalanceAppletSectionsListView;)Lcom/squareup/ui/balance/BalanceAppletSectionsListView;

    return-void
.end method
