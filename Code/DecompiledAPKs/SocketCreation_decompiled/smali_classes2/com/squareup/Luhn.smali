.class public Lcom/squareup/Luhn;
.super Ljava/lang/Object;
.source "Luhn.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fixCheckDigit(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 3

    .line 66
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x30

    if-ne v0, v1, :cond_0

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {p0, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0}, Lcom/squareup/Luhn;->generateCheckDigit(Ljava/lang/CharSequence;)C

    move-result p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 67
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Last digit was not a 0."

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static generateCheckDigit(Ljava/lang/CharSequence;)C
    .locals 1

    .line 74
    invoke-static {p0}, Lcom/squareup/Luhn;->luhnSum(Ljava/lang/CharSequence;)I

    move-result p0

    const/4 v0, -0x1

    if-eq p0, v0, :cond_0

    .line 80
    rem-int/lit8 p0, p0, 0xa

    rsub-int/lit8 p0, p0, 0xa

    rem-int/lit8 p0, p0, 0xa

    add-int/lit8 p0, p0, 0x30

    int-to-char p0, p0

    return p0

    .line 77
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Sequence must only contain digits and spaces."

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static luhnSum(Ljava/lang/CharSequence;)I
    .locals 5

    .line 22
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ltz v0, :cond_5

    .line 23
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    const/16 v4, 0x20

    if-ne v3, v4, :cond_0

    goto :goto_1

    :cond_0
    const/16 v4, 0x30

    if-lt v3, v4, :cond_4

    const/16 v4, 0x39

    if-le v3, v4, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v3, v3, -0x30

    if-eqz v2, :cond_2

    mul-int/lit8 v3, v3, 0x2

    :cond_2
    const/16 v4, 0x9

    if-le v3, v4, :cond_3

    .line 43
    rem-int/lit8 v3, v3, 0xa

    add-int/lit8 v3, v3, 0x1

    :cond_3
    add-int/2addr v1, v3

    xor-int/lit8 v2, v2, 0x1

    :goto_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_4
    :goto_2
    const/4 p0, -0x1

    return p0

    :cond_5
    return v1
.end method

.method public static validate(Ljava/lang/CharSequence;)Z
    .locals 0

    .line 54
    invoke-static {p0}, Lcom/squareup/Luhn;->luhnSum(Ljava/lang/CharSequence;)I

    move-result p0

    rem-int/lit8 p0, p0, 0xa

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
