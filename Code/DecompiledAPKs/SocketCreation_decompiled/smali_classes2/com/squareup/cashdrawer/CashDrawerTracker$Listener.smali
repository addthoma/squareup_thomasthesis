.class public interface abstract Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;
.super Ljava/lang/Object;
.source "CashDrawerTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cashdrawer/CashDrawerTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract cashDrawerConnected(Lcom/squareup/cashdrawer/CashDrawer;)V
.end method

.method public abstract cashDrawerDisconnected(Lcom/squareup/cashdrawer/CashDrawer;Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;)V
.end method

.method public abstract cashDrawersOpened()V
.end method
