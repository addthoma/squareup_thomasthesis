.class public Lcom/squareup/cashdrawer/CashDrawersEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "CashDrawersEvent.java"


# instance fields
.field public final possiblePrinterCashDrawers:I

.field public final usbCashDrawersCount:I


# direct methods
.method private constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;II)V
    .locals 0

    .line 16
    invoke-direct {p0, p1, p2}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 17
    iput p3, p0, Lcom/squareup/cashdrawer/CashDrawersEvent;->usbCashDrawersCount:I

    .line 18
    iput p4, p0, Lcom/squareup/cashdrawer/CashDrawersEvent;->possiblePrinterCashDrawers:I

    return-void
.end method

.method public static forCashDrawersOpened(II)Lcom/squareup/cashdrawer/CashDrawersEvent;
    .locals 3

    .line 23
    new-instance v0, Lcom/squareup/cashdrawer/CashDrawersEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->CASH_DRAWERS_OPENED:Lcom/squareup/analytics/RegisterActionName;

    iget-object v2, v2, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/squareup/cashdrawer/CashDrawersEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;II)V

    return-object v0
.end method

.method public static forUsbCashDrawerConnected(II)Lcom/squareup/cashdrawer/CashDrawersEvent;
    .locals 3

    .line 29
    new-instance v0, Lcom/squareup/cashdrawer/CashDrawersEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->CASH_DRAWER_CONNECTED:Lcom/squareup/analytics/RegisterActionName;

    iget-object v2, v2, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/squareup/cashdrawer/CashDrawersEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;II)V

    return-object v0
.end method

.method public static forUsbCashDrawerDisconnected(IILcom/squareup/cashdrawer/CashDrawer$DisconnectReason;)Lcom/squareup/cashdrawer/CashDrawersEvent;
    .locals 2

    .line 35
    new-instance v0, Lcom/squareup/cashdrawer/CashDrawersEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    iget-object p2, p2, Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;->value:Ljava/lang/String;

    invoke-direct {v0, v1, p2, p0, p1}, Lcom/squareup/cashdrawer/CashDrawersEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;II)V

    return-object v0
.end method
