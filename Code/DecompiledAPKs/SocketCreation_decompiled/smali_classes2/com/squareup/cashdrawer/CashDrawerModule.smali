.class public final Lcom/squareup/cashdrawer/CashDrawerModule;
.super Ljava/lang/Object;
.source "CashDrawerModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0007\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/cashdrawer/CashDrawerModule;",
        "",
        "()V",
        "provideCashDrawerExecutor",
        "Ljava/util/concurrent/Executor;",
        "hardware_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final provideCashDrawerExecutor()Ljava/util/concurrent/Executor;
    .locals 2
    .annotation runtime Lcom/squareup/cashdrawer/CashDrawerExecutor;
    .end annotation

    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string/jumbo v0, "usb_cash_drawers"

    .line 14
    invoke-static {v0}, Lcom/squareup/thread/executor/Executors;->newSingleThreadExecutor(Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    const-string v1, "Executors.newSingleThrea\u2026cutor(\"usb_cash_drawers\")"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/concurrent/Executor;

    return-object v0
.end method
