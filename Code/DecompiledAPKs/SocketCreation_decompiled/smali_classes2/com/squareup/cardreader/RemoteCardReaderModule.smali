.class public abstract Lcom/squareup/cardreader/RemoteCardReaderModule;
.super Ljava/lang/Object;
.source "RemoteCardReaderModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCardReaderId()Lcom/squareup/cardreader/CardReaderId;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 19
    new-instance v0, Lcom/squareup/cardreader/CardReaderId;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/CardReaderId;-><init>(I)V

    return-object v0
.end method

.method static provideCardReaderInfoForRemoteReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReaderInfo;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 15
    new-instance v0, Lcom/squareup/cardreader/CardReaderInfo;

    sget-object v1, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->AUDIO:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2, v2}, Lcom/squareup/cardreader/CardReaderInfo;-><init>(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
