.class public final Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionListenerFactory;
.super Ljava/lang/Object;
.source "GlobalHeadsetModule_ProvideHeadsetConnectionListenerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final listenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/cardreader/GlobalHeadsetModule;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/GlobalHeadsetModule;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/GlobalHeadsetModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionListenerFactory;->module:Lcom/squareup/cardreader/GlobalHeadsetModule;

    .line 26
    iput-object p2, p0, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionListenerFactory;->listenerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/GlobalHeadsetModule;Ljavax/inject/Provider;)Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionListenerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/GlobalHeadsetModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;",
            ">;)",
            "Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionListenerFactory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionListenerFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionListenerFactory;-><init>(Lcom/squareup/cardreader/GlobalHeadsetModule;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideHeadsetConnectionListener(Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;)Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;
    .locals 0

    .line 41
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/GlobalHeadsetModule;->provideHeadsetConnectionListener(Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;)Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionListenerFactory;->module:Lcom/squareup/cardreader/GlobalHeadsetModule;

    iget-object v1, p0, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionListenerFactory;->listenerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionListenerFactory;->provideHeadsetConnectionListener(Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;)Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetConnectionListenerFactory;->get()Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

    move-result-object v0

    return-object v0
.end method
