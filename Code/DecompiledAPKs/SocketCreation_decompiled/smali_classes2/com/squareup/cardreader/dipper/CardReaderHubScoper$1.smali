.class Lcom/squareup/cardreader/dipper/CardReaderHubScoper$1;
.super Ljava/lang/Object;
.source "CardReaderHubScoper.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cardreader/dipper/CardReaderHubScoper;->scopeAttachListener(Lmortar/MortarScope;Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/dipper/CardReaderHubScoper;

.field final synthetic val$handler:Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/dipper/CardReaderHubScoper;Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V
    .locals 0

    .line 21
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/CardReaderHubScoper$1;->this$0:Lcom/squareup/cardreader/dipper/CardReaderHubScoper;

    iput-object p2, p0, Lcom/squareup/cardreader/dipper/CardReaderHubScoper$1;->val$handler:Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 23
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/CardReaderHubScoper$1;->this$0:Lcom/squareup/cardreader/dipper/CardReaderHubScoper;

    invoke-static {p1}, Lcom/squareup/cardreader/dipper/CardReaderHubScoper;->access$000(Lcom/squareup/cardreader/dipper/CardReaderHubScoper;)Lcom/squareup/cardreader/CardReaderHub;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/CardReaderHubScoper$1;->val$handler:Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/CardReaderHub;->addCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 27
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/CardReaderHubScoper$1;->this$0:Lcom/squareup/cardreader/dipper/CardReaderHubScoper;

    invoke-static {v0}, Lcom/squareup/cardreader/dipper/CardReaderHubScoper;->access$000(Lcom/squareup/cardreader/dipper/CardReaderHubScoper;)Lcom/squareup/cardreader/CardReaderHub;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/CardReaderHubScoper$1;->val$handler:Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderHub;->removeCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    return-void
.end method
