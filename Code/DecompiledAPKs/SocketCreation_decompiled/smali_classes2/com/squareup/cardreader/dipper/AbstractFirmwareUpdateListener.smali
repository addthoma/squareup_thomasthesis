.class abstract Lcom/squareup/cardreader/dipper/AbstractFirmwareUpdateListener;
.super Ljava/lang/Object;
.source "AbstractFirmwareUpdateListener.java"

# interfaces
.implements Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFirmwareManifestServerResponseFailure(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onFirmwareManifestServerResponseSuccess(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onFirmwareUpdateAborted(Lcom/squareup/cardreader/CardReaderInfo;Z)V
    .locals 0

    return-void
.end method

.method public onFirmwareUpdateComplete(Lcom/squareup/cardreader/CardReaderInfo;Z)V
    .locals 0

    return-void
.end method

.method public onFirmwareUpdateError(Lcom/squareup/cardreader/CardReaderInfo;ZLcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)V
    .locals 0

    return-void
.end method

.method public onFirmwareUpdateProgress(Lcom/squareup/cardreader/CardReaderInfo;ZIIZ)V
    .locals 0

    return-void
.end method

.method public onFirmwareUpdateStarted(Lcom/squareup/cardreader/CardReaderInfo;Z)V
    .locals 0

    return-void
.end method

.method public onReaderFailedToConnectAfterRebootingFwup(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onSendFirmwareManifestToServer(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method
