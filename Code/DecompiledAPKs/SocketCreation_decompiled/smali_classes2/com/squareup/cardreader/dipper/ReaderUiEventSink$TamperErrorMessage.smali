.class final enum Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;
.super Ljava/lang/Enum;
.source "ReaderUiEventSink.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/dipper/ReaderUiEventSink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "TamperErrorMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;

.field public static final enum DEFAULT:Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;

.field public static final enum FOR_R12:Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;


# instance fields
.field private final messageRes:I

.field private final titleRes:I

.field private final url:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 655
    new-instance v6, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;

    sget v4, Lcom/squareup/cardreader/ui/R$string;->emv_reader_tampered_title:I

    sget v5, Lcom/squareup/cardreader/ui/R$string;->emv_reader_error_msg:I

    const-string v1, "FOR_R12"

    const/4 v2, 0x0

    const-string v3, "/shop/reader-contactless-chip/warranty"

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v6, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;->FOR_R12:Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;

    .line 659
    new-instance v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;

    sget v11, Lcom/squareup/cardreader/ui/R$string;->emv_reader_tampered_title:I

    sget v12, Lcom/squareup/cardreader/ui/R$string;->emv_reader_error_msg:I

    const-string v8, "DEFAULT"

    const/4 v9, 0x1

    const-string v10, "/shop/reader/warranty"

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;->DEFAULT:Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;

    .line 654
    sget-object v1, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;->FOR_R12:Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;->DEFAULT:Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;->$VALUES:[Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)V"
        }
    .end annotation

    .line 668
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 669
    iput-object p3, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;->url:Ljava/lang/String;

    .line 670
    iput p4, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;->titleRes:I

    .line 671
    iput p5, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;->messageRes:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;
    .locals 0

    .line 654
    invoke-static {p0}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;->forReader(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;)Ljava/lang/String;
    .locals 0

    .line 654
    iget-object p0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;->url:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;)I
    .locals 0

    .line 654
    iget p0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;->titleRes:I

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;)I
    .locals 0

    .line 654
    iget p0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;->messageRes:I

    return p0
.end method

.method private static forReader(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;
    .locals 1

    .line 675
    sget-object v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$1;->$SwitchMap$com$squareup$cardreader$lcr$CrCardreaderType:[I

    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInfo;->getLcrReaderType()Lcom/squareup/cardreader/lcr/CrCardreaderType;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/cardreader/lcr/CrCardreaderType;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    .line 680
    sget-object p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;->DEFAULT:Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;

    return-object p0

    .line 678
    :cond_0
    sget-object p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;->FOR_R12:Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;
    .locals 1

    .line 654
    const-class v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;
    .locals 1

    .line 654
    sget-object v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;->$VALUES:[Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;

    return-object v0
.end method
