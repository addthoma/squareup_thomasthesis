.class public final Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSinkKt;
.super Ljava/lang/Object;
.source "ReaderIssueScreenRequestSink.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000*@\u0010\u0000\"\u001d\u0012\u0013\u0012\u00110\u0002\u00a2\u0006\u000c\u0008\u0003\u0012\u0008\u0008\u0004\u0012\u0004\u0008\u0008(\u0005\u0012\u0004\u0012\u00020\u00060\u00012\u001d\u0012\u0013\u0012\u00110\u0002\u00a2\u0006\u000c\u0008\u0003\u0012\u0008\u0008\u0004\u0012\u0004\u0008\u0008(\u0005\u0012\u0004\u0012\u00020\u00060\u0001\u00a8\u0006\u0007"
    }
    d2 = {
        "ReaderIssueScreenRequestHandler",
        "Lkotlin/Function1;",
        "Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;",
        "Lkotlin/ParameterName;",
        "name",
        "issueScreen",
        "",
        "cardreader-ui_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation
