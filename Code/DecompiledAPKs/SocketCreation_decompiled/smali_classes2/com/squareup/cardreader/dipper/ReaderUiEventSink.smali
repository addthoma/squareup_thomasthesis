.class public Lcom/squareup/cardreader/dipper/ReaderUiEventSink;
.super Ljava/lang/Object;
.source "ReaderUiEventSink.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderStatusListener;
.implements Lcom/squareup/cardreader/MagSwipeListener;
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;
    }
.end annotation


# static fields
.field static final FALLBACK_WARRANTY_URL_R12:Ljava/lang/String; = "/shop/reader-contactless-chip/warranty"

.field static final FALLBACK_WARRANTY_URL_R6:Ljava/lang/String; = "/shop/reader/warranty"


# instance fields
.field private final accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field private final apiTransactionState:Lcom/squareup/api/ApiTransactionState;

.field private final badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final damagedReaderService:Lcom/squareup/server/DamagedReaderService;

.field private final dipperEventHandler:Lcom/squareup/cardreader/dipper/DipperEventHandler;

.field private final dipperUiErrorDisplayTypeSelector:Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;

.field private final emvDipScreenHandler:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final emvSwipePassthroughEnabler:Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

.field private final firmwareUpdateScreenHandler:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final lazySmartPaymentFlowStarter:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final localCardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private final offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

.field private final onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

.field private final r12TutorialLauncher:Lcom/squareup/ui/main/R12ForceableContentLauncher;

.field private final r6VideoLauncher:Lcom/squareup/ui/main/R6ForceableContentLauncher;

.field private final readerConnectionEventLogger:Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;

.field private final readerHudConnectionEventHandler:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final readerHudManager:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;"
        }
    .end annotation
.end field

.field private final readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

.field private final reportCoredumpRequestDisposable:Lio/reactivex/disposables/CompositeDisposable;

.field private final reportCoredumpService:Lcom/squareup/server/ReportCoredumpService;

.field private final res:Lcom/squareup/util/Res;

.field private final sendRequestDisposable:Lio/reactivex/disposables/CompositeDisposable;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private silenceReaderHuds:Z

.field private final speRestartChecker:Lcom/squareup/cardreader/squid/common/SpeRestartChecker;

.field private final swipeBus:Lcom/squareup/wavpool/swipe/SwipeBus;

.field private final tenderStarter:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/accessibility/AccessibilityManager;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/dipper/DipperEventHandler;Lcom/squareup/server/DamagedReaderService;Ldagger/Lazy;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/wavpool/swipe/SwipeBus;Lcom/squareup/settings/server/Features;Ldagger/Lazy;Lcom/squareup/hudtoaster/HudToaster;Ldagger/Lazy;Lcom/squareup/server/ReportCoredumpService;Lcom/squareup/util/Res;Ldagger/Lazy;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/CardReaderListeners;Ldagger/Lazy;Lcom/squareup/ui/main/R6ForceableContentLauncher;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/onboarding/OnboardingDiverter;Ldagger/Lazy;Lcom/squareup/ui/main/R12ForceableContentLauncher;Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/cardreader/squid/common/SpeRestartChecker;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/accessibility/AccessibilityManager;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            "Lcom/squareup/cardreader/dipper/DipperEventHandler;",
            "Lcom/squareup/server/DamagedReaderService;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            "Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;",
            "Lcom/squareup/wavpool/swipe/SwipeBus;",
            "Lcom/squareup/settings/server/Features;",
            "Ldagger/Lazy<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;",
            ">;",
            "Lcom/squareup/hudtoaster/HudToaster;",
            "Ldagger/Lazy<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;",
            "Lcom/squareup/server/ReportCoredumpService;",
            "Lcom/squareup/util/Res;",
            "Ldagger/Lazy<",
            "Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;",
            ">;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Lcom/squareup/ui/main/R6ForceableContentLauncher;",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            "Ldagger/Lazy<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;",
            "Lcom/squareup/ui/main/R12ForceableContentLauncher;",
            "Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;",
            "Lcom/squareup/api/ApiTransactionState;",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            "Lcom/squareup/cardreader/squid/common/SpeRestartChecker;",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            "Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    new-instance v1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->reportCoredumpRequestDisposable:Lio/reactivex/disposables/CompositeDisposable;

    .line 134
    new-instance v1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->sendRequestDisposable:Lio/reactivex/disposables/CompositeDisposable;

    move-object v1, p1

    .line 159
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    move-object v1, p2

    .line 160
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p3

    .line 161
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    move-object v1, p4

    .line 162
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->dipperEventHandler:Lcom/squareup/cardreader/dipper/DipperEventHandler;

    move-object v1, p5

    .line 163
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->damagedReaderService:Lcom/squareup/server/DamagedReaderService;

    move-object v1, p6

    .line 164
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->lazySmartPaymentFlowStarter:Ldagger/Lazy;

    move-object v1, p8

    .line 165
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->emvSwipePassthroughEnabler:Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

    move-object v1, p9

    .line 166
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->swipeBus:Lcom/squareup/wavpool/swipe/SwipeBus;

    move-object v1, p10

    .line 167
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->features:Lcom/squareup/settings/server/Features;

    move-object v1, p7

    .line 168
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    move-object v1, p11

    .line 169
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->firmwareUpdateScreenHandler:Ldagger/Lazy;

    move-object v1, p12

    .line 170
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    move-object/from16 v1, p17

    .line 171
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    move-object/from16 v1, p18

    .line 172
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->localCardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    move-object/from16 v1, p16

    .line 173
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerHudConnectionEventHandler:Ldagger/Lazy;

    move-object v1, p13

    .line 174
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerHudManager:Ldagger/Lazy;

    move-object/from16 v1, p14

    .line 175
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->reportCoredumpService:Lcom/squareup/server/ReportCoredumpService;

    move-object/from16 v1, p15

    .line 176
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->res:Lcom/squareup/util/Res;

    move-object/from16 v1, p19

    .line 177
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->tenderStarter:Ldagger/Lazy;

    move-object/from16 v1, p20

    .line 178
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->r6VideoLauncher:Lcom/squareup/ui/main/R6ForceableContentLauncher;

    move-object/from16 v1, p25

    .line 179
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->r12TutorialLauncher:Lcom/squareup/ui/main/R12ForceableContentLauncher;

    move-object/from16 v1, p21

    .line 180
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-object/from16 v1, p23

    .line 181
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    move-object/from16 v1, p22

    .line 182
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    move-object/from16 v1, p24

    .line 183
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->emvDipScreenHandler:Ldagger/Lazy;

    move-object/from16 v1, p26

    .line 184
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerConnectionEventLogger:Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;

    move-object/from16 v1, p27

    .line 185
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    move-object/from16 v1, p28

    .line 186
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    move-object/from16 v1, p29

    .line 187
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->speRestartChecker:Lcom/squareup/cardreader/squid/common/SpeRestartChecker;

    move-object/from16 v1, p30

    .line 188
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    move-object/from16 v1, p31

    .line 189
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->dipperUiErrorDisplayTypeSelector:Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;

    return-void
.end method

.method private getMerchantToken()Ljava/lang/String;
    .locals 1

    .line 568
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private hasInvalidTmsForEmv(Lcom/squareup/cardreader/CardReaderInfo;)Z
    .locals 1

    .line 584
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->supportsTmsCountryCodeCheck()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 585
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->hasValidTmsCountryCode()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private hasInvalidTmsForSwipe(Lcom/squareup/cardreader/CardReaderInfo;)Z
    .locals 1

    .line 579
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->supportsTmsCountryCodeCheck()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 580
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->hasValidTmsCountryCode()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private isSecureSessionRequiredForSwipe(Lcom/squareup/cardreader/CardReaderInfo;)Z
    .locals 2

    .line 573
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REQUIRE_SECURE_SESSION_FOR_R6_SWIPE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 574
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R6:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v0, v1, :cond_0

    .line 575
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->hasSecureSession()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method static synthetic lambda$null$3(Lcom/squareup/cardreader/CardReader;Lcom/squareup/protos/client/tarkin/ReportCoredumpResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 309
    invoke-interface {p0}, Lcom/squareup/cardreader/CardReader;->onCoreDumpDataSent()V

    return-void
.end method

.method static synthetic lambda$null$4(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v0, "Send CoreDump"

    .line 310
    invoke-static {p0, v0}, Lcom/squareup/receiving/SuccessOrFailureLogger;->logFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$onCoreDump$5(Lcom/squareup/cardreader/CardReader;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 308
    new-instance v0, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$wWiTSenWTcyt0y74oYhmVHb8QzU;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$wWiTSenWTcyt0y74oYhmVHb8QzU;-><init>(Lcom/squareup/cardreader/CardReader;)V

    sget-object p0, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$yxho7DKJmSHJT31ePeY8-hhQ2ms;->INSTANCE:Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$yxho7DKJmSHJT31ePeY8-hhQ2ms;

    invoke-virtual {p1, v0, p0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method private showDefaultTamperErrorScreen(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 4

    .line 535
    invoke-static {p1}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;->access$000(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;

    move-result-object v0

    .line 536
    invoke-static {v0}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;->access$100(Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->res:Lcom/squareup/util/Res;

    invoke-static {v0}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;->access$200(Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;)I

    move-result v3

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->res:Lcom/squareup/util/Res;

    .line 537
    invoke-static {v0}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;->access$300(Lcom/squareup/cardreader/dipper/ReaderUiEventSink$TamperErrorMessage;)I

    move-result v0

    invoke-interface {v3, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 536
    invoke-direct {p0, p1, v1, v2, v0}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->showTamperErrorScreen(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private showInvalidTmsScreen(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 648
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getTmsCountryCode()Lcom/squareup/CountryCode;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/cardreader/InvalidTmsException;->remoteLog(Lcom/squareup/CountryCode;)V

    .line 650
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowConcreteWarningScreen;

    sget-object v1, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$InvalidTmsErrorScreen;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$InvalidTmsErrorScreen;

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowConcreteWarningScreen;-><init>(Lcom/squareup/container/LayoutScreen;)V

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    return-void
.end method

.method private showTamperErrorScreen(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 542
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->dipperUiErrorDisplayTypeSelector:Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;

    invoke-interface {v0}, Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;->getTamperErrorDisplayType()Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector$DisplayType;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector$DisplayType;->CONCRETE_WARNING_SCREEN:Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector$DisplayType;

    if-ne v0, v1, :cond_2

    .line 543
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getTamperStatus()Lcom/squareup/cardreader/lcr/CrTamperStatus;

    move-result-object p1

    sget-object p2, Lcom/squareup/cardreader/lcr/CrTamperStatus;->CR_TAMPER_STATUS_TAMPERED:Lcom/squareup/cardreader/lcr/CrTamperStatus;

    const/4 p3, 0x1

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    xor-int/2addr p1, p3

    .line 545
    iget-object p2, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance p3, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowConcreteWarningScreen;

    if-eqz p1, :cond_1

    .line 547
    invoke-static {}, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DarkTamperErrorScreen;->cancellableDarkTamperErrorScreen()Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DarkTamperErrorScreen;

    move-result-object p1

    goto :goto_1

    .line 548
    :cond_1
    invoke-static {}, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DarkTamperErrorScreen;->noncancellableDarkTamperErrorScreen()Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DarkTamperErrorScreen;

    move-result-object p1

    :goto_1
    invoke-direct {p3, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowConcreteWarningScreen;-><init>(Lcom/squareup/container/LayoutScreen;)V

    .line 545
    invoke-virtual {p2, p3}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    goto :goto_2

    .line 550
    :cond_2
    new-instance v0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CHIP_CARD_NOT_USABLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-direct {v0, v1, p3, p4}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    new-instance p3, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {p3}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object p4, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->TAMPER_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 554
    invoke-virtual {p3, p4}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p3

    iget p4, v0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->messageId:I

    .line 555
    invoke-virtual {p3, p4}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p3

    iget-object p4, v0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->localizedMessage:Ljava/lang/String;

    .line 556
    invoke-virtual {p3, p4}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedMessage(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p3

    iget p4, v0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->titleId:I

    .line 557
    invoke-virtual {p3, p4}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p3

    iget-object p4, v0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->localizedTitle:Ljava/lang/String;

    .line 558
    invoke-virtual {p3, p4}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedTitle(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p3

    iget-object p4, v0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 559
    invoke-virtual {p3, p4}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p3

    .line 560
    invoke-virtual {p3, p2}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->url(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p2

    .line 561
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 562
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 563
    iget-object p2, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance p3, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowErrorScreen;

    invoke-direct {p3, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowErrorScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {p2, p3}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    :goto_2
    return-void
.end method

.method private static translateReaderType(Lcom/squareup/cardreader/lcr/CrCardreaderType;)Lcom/squareup/protos/client/tarkin/ReaderType;
    .locals 3

    .line 591
    sget-object v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$1;->$SwitchMap$com$squareup$cardreader$lcr$CrCardreaderType:[I

    invoke-virtual {p0}, Lcom/squareup/cardreader/lcr/CrCardreaderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 605
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ReaderType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, " not supported by Tarkin protos"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 603
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/tarkin/ReaderType;->T2B:Lcom/squareup/protos/client/tarkin/ReaderType;

    return-object p0

    .line 601
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/tarkin/ReaderType;->T2:Lcom/squareup/protos/client/tarkin/ReaderType;

    return-object p0

    .line 599
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/tarkin/ReaderType;->X2:Lcom/squareup/protos/client/tarkin/ReaderType;

    return-object p0

    .line 597
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/tarkin/ReaderType;->R6:Lcom/squareup/protos/client/tarkin/ReaderType;

    return-object p0

    .line 595
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/tarkin/ReaderType;->R12C:Lcom/squareup/protos/client/tarkin/ReaderType;

    return-object p0

    .line 593
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/tarkin/ReaderType;->R12:Lcom/squareup/protos/client/tarkin/ReaderType;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public initializingSecureSession(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public synthetic lambda$null$0$ReaderUiEventSink(Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 269
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->onTamperDataSent()V

    .line 271
    invoke-virtual {p2}, Lcom/squareup/cardreader/CardReaderInfo;->getTamperStatus()Lcom/squareup/cardreader/lcr/CrTamperStatus;

    move-result-object p1

    sget-object v0, Lcom/squareup/cardreader/lcr/CrTamperStatus;->CR_TAMPER_STATUS_TAMPERED:Lcom/squareup/cardreader/lcr/CrTamperStatus;

    if-eq p1, v0, :cond_0

    return-void

    .line 275
    :cond_0
    iget-object p1, p3, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 276
    iget-object p1, p3, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse;->warranty_url:Ljava/lang/String;

    .line 277
    iget-object v0, p3, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    .line 278
    iget-object p3, p3, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p3, p3, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    .line 279
    invoke-direct {p0, p2, p1, v0, p3}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->showTamperErrorScreen(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 281
    :cond_1
    invoke-direct {p0, p2}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->showDefaultTamperErrorScreen(Lcom/squareup/cardreader/CardReaderInfo;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$1$ReaderUiEventSink(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 284
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getTamperStatus()Lcom/squareup/cardreader/lcr/CrTamperStatus;

    move-result-object p2

    sget-object v0, Lcom/squareup/cardreader/lcr/CrTamperStatus;->CR_TAMPER_STATUS_TAMPERED:Lcom/squareup/cardreader/lcr/CrTamperStatus;

    if-ne p2, v0, :cond_0

    .line 285
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->showDefaultTamperErrorScreen(Lcom/squareup/cardreader/CardReaderInfo;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onTamperData$2$ReaderUiEventSink(Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 268
    new-instance v0, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$7UZfqGuY6UNiGaxxeSWc3KuZsxM;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$7UZfqGuY6UNiGaxxeSWc3KuZsxM;-><init>(Lcom/squareup/cardreader/dipper/ReaderUiEventSink;Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReaderInfo;)V

    new-instance p1, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$TMbeSfW4TIvFEWcZT8rGM9VoI0U;

    invoke-direct {p1, p0, p2}, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$TMbeSfW4TIvFEWcZT8rGM9VoI0U;-><init>(Lcom/squareup/cardreader/dipper/ReaderUiEventSink;Lcom/squareup/cardreader/CardReaderInfo;)V

    invoke-virtual {p3, v0, p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public onAudioReaderFailedToConnect(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 205
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isAudio()Z

    move-result p1

    const-string v0, "non-headset reader called an audio API!"

    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 207
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_MAGSTRIPE_ONLY_READERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    .line 209
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowConcreteWarningScreen;

    sget-object v1, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DeadReaderBattery;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DeadReaderBattery;

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowConcreteWarningScreen;-><init>(Lcom/squareup/container/LayoutScreen;)V

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    :cond_0
    return-void
.end method

.method public onBatteryDead(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 251
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerHudManager:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/dipper/ReaderHudManager;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->toastBatteryStatus(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onBatteryUpdate(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 3

    .line 482
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionState;->cardSupported()Z

    move-result v0

    if-nez v0, :cond_0

    .line 484
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    return-void

    .line 488
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    invoke-interface {v0}, Lcom/squareup/onboarding/OnboardingDiverter;->shouldDivertToOnboarding()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 489
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    sget-object v0, Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;->PROMPT:Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    invoke-interface {p1, v0}, Lcom/squareup/onboarding/OnboardingDiverter;->divertToOnboarding(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;)V

    return-void

    .line 493
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->hasSecureSession()Z

    move-result v0

    .line 494
    iget-object v1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {v1}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v1}, Lcom/squareup/connectivity/ConnectivityMonitor;->isConnected()Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_4

    .line 497
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v1, Lcom/squareup/cardreader/ui/R$string;->unable_to_process_chip_cards:I

    sget v2, Lcom/squareup/cardreader/ui/R$string;->please_restore_internet_connectivity:I

    invoke-interface {p1, v0, v1, v2}, Lcom/squareup/hudtoaster/HudToaster;->toastLongHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)Z

    return-void

    .line 502
    :cond_4
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->hasInvalidTmsForEmv(Lcom/squareup/cardreader/CardReaderInfo;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 503
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->showInvalidTmsScreen(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void

    :cond_5
    if-nez v0, :cond_6

    .line 508
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerHudManager:Ldagger/Lazy;

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/dipper/ReaderHudManager;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->secure_session_required_for_dip_title:I

    .line 509
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->toastReaderStillConnecting(I)V

    return-void

    .line 513
    :cond_6
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->lazySmartPaymentFlowStarter:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_INSERTED_AWAITING_FW_UPDATE:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkFirmwareUpdateNotBlocking(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;)Z

    move-result v0

    if-nez v0, :cond_7

    return-void

    .line 518
    :cond_7
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->setActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-nez v0, :cond_8

    return-void

    .line 520
    :cond_8
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->emvDipScreenHandler:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onCardReaderBackendInitialized(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    return-void
.end method

.method public onCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 525
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionState;->cardSupported()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 527
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->emvDipScreenHandler:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onCoreDump(Lcom/squareup/cardreader/CardReader;[B[B)V
    .locals 2

    .line 295
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    .line 297
    new-instance v1, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;-><init>()V

    .line 298
    invoke-static {p2}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->coredump_key(Lokio/ByteString;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;

    move-result-object p2

    .line 299
    invoke-static {p3}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->coredump_data(Lokio/ByteString;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;

    move-result-object p2

    .line 300
    invoke-direct {p0}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->getMerchantToken()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;

    move-result-object p2

    .line 301
    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getLcrReaderType()Lcom/squareup/cardreader/lcr/CrCardreaderType;

    move-result-object p3

    invoke-static {p3}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->translateReaderType(Lcom/squareup/cardreader/lcr/CrCardreaderType;)Lcom/squareup/protos/client/tarkin/ReaderType;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->reader_type(Lcom/squareup/protos/client/tarkin/ReaderType;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;

    move-result-object p2

    .line 302
    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getHardwareSerialNumber()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->reader_serial_number(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;

    move-result-object p2

    .line 303
    invoke-virtual {p2}, Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest$Builder;->build()Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;

    move-result-object p2

    .line 305
    iget-object p3, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->reportCoredumpRequestDisposable:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->reportCoredumpService:Lcom/squareup/server/ReportCoredumpService;

    .line 306
    invoke-interface {v0, p2}, Lcom/squareup/server/ReportCoredumpService;->send(Lcom/squareup/protos/client/tarkin/ReportCoredumpRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p2

    .line 307
    invoke-virtual {p2}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p2

    new-instance v0, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$1xvVP2573cb_zCl2ASjgTmpX0WM;

    invoke-direct {v0, p1}, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$1xvVP2573cb_zCl2ASjgTmpX0WM;-><init>(Lcom/squareup/cardreader/CardReader;)V

    .line 308
    invoke-virtual {p2, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 305
    invoke-virtual {p3, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public onDeviceUnsupported(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 196
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->DEVICE_UNSUPPORTED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 198
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    .line 199
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 200
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 201
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowErrorScreen;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowErrorScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 611
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerHudConnectionEventHandler:Ldagger/Lazy;

    .line 612
    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;

    .line 613
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {v0}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v0

    if-nez v0, :cond_0

    .line 614
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->localCardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderListeners;->setLibraryLoadErrorListener(Lcom/squareup/cardreader/LibraryLoadErrorListener;)V

    .line 617
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->dipperEventHandler:Lcom/squareup/cardreader/dipper/DipperEventHandler;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/dipper/DipperEventHandler;->setCardReaderStatusListener(Lcom/squareup/cardreader/CardReaderStatusListener;)V

    .line 618
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->dipperEventHandler:Lcom/squareup/cardreader/dipper/DipperEventHandler;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/dipper/DipperEventHandler;->setMagSwipeListener(Lcom/squareup/cardreader/MagSwipeListener;)V

    .line 619
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderHub;->addCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    .line 620
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerConnectionEventLogger:Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/CardReaderHub;->addCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    .line 621
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->firmwareUpdateScreenHandler:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->addFirmwareUpdateListener(Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 625
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {v0}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v0

    if-nez v0, :cond_0

    .line 626
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->localCardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->unsetLibraryLoadErrorListener()V

    .line 629
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->dipperEventHandler:Lcom/squareup/cardreader/dipper/DipperEventHandler;

    sget-object v1, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/DipperEventHandler;->setCardReaderStatusListener(Lcom/squareup/cardreader/CardReaderStatusListener;)V

    .line 630
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->dipperEventHandler:Lcom/squareup/cardreader/dipper/DipperEventHandler;

    sget-object v1, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/DipperEventHandler;->setMagSwipeListener(Lcom/squareup/cardreader/MagSwipeListener;)V

    .line 631
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerHudConnectionEventHandler:Ldagger/Lazy;

    invoke-interface {v1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderHub;->removeCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    .line 632
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerConnectionEventLogger:Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderHub;->removeCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    .line 633
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->firmwareUpdateScreenHandler:Ldagger/Lazy;

    invoke-interface {v1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->removeFirmwareUpdateListener(Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;)V

    .line 634
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->reportCoredumpRequestDisposable:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 635
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->sendRequestDisposable:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method

.method public onFullCommsEstablished(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 231
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->emvSwipePassthroughEnabler:Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

    invoke-virtual {v0}, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;->refreshSwipePassthrough()V

    .line 233
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 234
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->requiresTalkBackWarning()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    .line 238
    new-instance p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->TALKBACK_ENABLED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 240
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 241
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 242
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    :cond_1
    return-void
.end method

.method public onInitFirmwareUpdateRequired(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onInitRegisterUpdateRequired(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 222
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->UPDATE_REGISTER:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 224
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    .line 225
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 226
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 227
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowErrorScreen;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowErrorScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    return-void
.end method

.method public onInitializationComplete(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 376
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->hasRecentRebootAssetCompleted(Lcom/squareup/cardreader/CardReaderInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 380
    :cond_0
    sget-object v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 389
    :cond_1
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->r12TutorialLauncher:Lcom/squareup/ui/main/R12ForceableContentLauncher;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/main/R12ForceableContentLauncher;->attemptToShowContent(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->tenderStarter:Ldagger/Lazy;

    .line 390
    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/TenderStarter;

    invoke-interface {v0}, Lcom/squareup/ui/tender/TenderStarter;->warnIfNfcEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    return-void

    .line 382
    :cond_3
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->r6VideoLauncher:Lcom/squareup/ui/main/R6ForceableContentLauncher;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/ui/main/R6ForceableContentLauncher;->attemptToShowContent(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    return-void

    .line 396
    :cond_4
    :goto_0
    iget-boolean v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->silenceReaderHuds:Z

    if-nez v0, :cond_5

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->hasBattery()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 397
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerHudManager:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/dipper/ReaderHudManager;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->toastBatteryStatus(Lcom/squareup/cardreader/CardReaderInfo;)V

    :cond_5
    return-void
.end method

.method public onMagSwipeFailed(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 2

    .line 456
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->lazySmartPaymentFlowStarter:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_SWIPED_AWAITING_FW_UPDATE:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkFirmwareUpdateNotBlocking(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 461
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->hasInvalidTmsForSwipe(Lcom/squareup/cardreader/CardReaderInfo;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 462
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->showInvalidTmsScreen(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void

    .line 466
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->isSecureSessionRequiredForSwipe(Lcom/squareup/cardreader/CardReaderInfo;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 467
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerHudManager:Ldagger/Lazy;

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/dipper/ReaderHudManager;

    sget p2, Lcom/squareup/cardreader/ui/R$string;->secure_session_required_for_swipe_title:I

    .line 468
    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->toastReaderStillConnecting(I)V

    return-void

    .line 472
    :cond_2
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->swipeBus:Lcom/squareup/wavpool/swipe/SwipeBus;

    invoke-virtual {p1, p2}, Lcom/squareup/wavpool/swipe/SwipeBus;->post(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V

    return-void
.end method

.method public onMagSwipePassthrough(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 0

    .line 477
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->swipeBus:Lcom/squareup/wavpool/swipe/SwipeBus;

    invoke-virtual {p1, p2}, Lcom/squareup/wavpool/swipe/SwipeBus;->post(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    return-void
.end method

.method public onMagSwipeSuccess(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 2

    .line 436
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->lazySmartPaymentFlowStarter:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_SWIPED_AWAITING_FW_UPDATE:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkFirmwareUpdateNotBlocking(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 441
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->hasInvalidTmsForSwipe(Lcom/squareup/cardreader/CardReaderInfo;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 442
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->showInvalidTmsScreen(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void

    .line 446
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->isSecureSessionRequiredForSwipe(Lcom/squareup/cardreader/CardReaderInfo;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 447
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerHudManager:Ldagger/Lazy;

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/dipper/ReaderHudManager;

    sget p2, Lcom/squareup/cardreader/ui/R$string;->secure_session_required_for_swipe_title:I

    .line 448
    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->toastReaderStillConnecting(I)V

    return-void

    .line 452
    :cond_2
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->swipeBus:Lcom/squareup/wavpool/swipe/SwipeBus;

    invoke-virtual {p1, p2}, Lcom/squareup/wavpool/swipe/SwipeBus;->post(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    return-void
.end method

.method public onReaderError(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    .line 291
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->showDefaultTamperErrorScreen(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onReaderReady()V
    .locals 1

    .line 531
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->speRestartChecker:Lcom/squareup/cardreader/squid/common/SpeRestartChecker;

    invoke-interface {v0}, Lcom/squareup/cardreader/squid/common/SpeRestartChecker;->onReaderReady()V

    return-void
.end method

.method public onSecureSessionAborted(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;)V
    .locals 2

    .line 322
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->SECURE_SESSION_FAILED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 324
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 325
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/cardreader/ui/R$string;->reader_failed_to_connect:I

    .line 326
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    .line 327
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 329
    sget-object v0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink$1;->$SwitchMap$com$squareup$cardreader$CardReader$AbortSecureSessionReason:[I

    invoke-virtual {p2}, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_2

    const/4 v0, 0x2

    if-eq p2, v0, :cond_1

    const/4 v0, 0x3

    if-eq p2, v0, :cond_0

    goto :goto_0

    .line 342
    :cond_0
    sget p2, Lcom/squareup/cardreader/ui/R$string;->please_try_again_or_contact_support:I

    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    .line 343
    iget-object p2, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {p2, v0}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    goto :goto_0

    .line 337
    :cond_1
    sget p2, Lcom/squareup/cardreader/ui/R$string;->please_check_your_network_connection:I

    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    .line 338
    iget-object p2, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {p2, v0}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    goto :goto_0

    .line 332
    :cond_2
    sget p2, Lcom/squareup/cardreader/ui/R$string;->please_contact_support:I

    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    .line 333
    iget-object p2, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {p2, v0}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    :goto_0
    return-void
.end method

.method public onSecureSessionDenied(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V
    .locals 0

    .line 353
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isFirmwareUpdateBlocking()Z

    move-result p1

    if-nez p1, :cond_0

    .line 354
    new-instance p1, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$SecureSessionFlipperDenied;

    invoke-direct {p1, p2, p3, p4}, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$SecureSessionFlipperDenied;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V

    .line 356
    iget-object p2, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance p3, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowConcreteWarningScreen;

    invoke-direct {p3, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowConcreteWarningScreen;-><init>(Lcom/squareup/container/LayoutScreen;)V

    invoke-virtual {p2, p3}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    :cond_0
    return-void
.end method

.method public onSecureSessionError(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V
    .locals 0

    return-void
.end method

.method public onSecureSessionInvalid(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onSecureSessionValid(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public onTamperData(Lcom/squareup/cardreader/CardReader;[B)V
    .locals 3

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Received tamper data.  Requesting warranty flow url."

    .line 255
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 256
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    .line 258
    new-instance v1, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;-><init>()V

    .line 259
    invoke-direct {p0}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->getMerchantToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;

    move-result-object v1

    .line 260
    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getLcrReaderType()Lcom/squareup/cardreader/lcr/CrCardreaderType;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->translateReaderType(Lcom/squareup/cardreader/lcr/CrCardreaderType;)Lcom/squareup/protos/client/tarkin/ReaderType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;->reader_type(Lcom/squareup/protos/client/tarkin/ReaderType;)Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;

    move-result-object v1

    .line 261
    invoke-static {p2}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;->tamper_data_message_bytes(Lokio/ByteString;)Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;

    move-result-object p2

    .line 262
    invoke-virtual {p2}, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;->build()Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;

    move-result-object p2

    .line 264
    iget-object v1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->sendRequestDisposable:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->damagedReaderService:Lcom/squareup/server/DamagedReaderService;

    .line 265
    invoke-interface {v2, p2}, Lcom/squareup/server/DamagedReaderService;->send(Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p2

    .line 266
    invoke-virtual {p2}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p2

    new-instance v2, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$FZRiQKL44bGAL7emsd7O5U5vYrM;

    invoke-direct {v2, p0, p1, v0}, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$FZRiQKL44bGAL7emsd7O5U5vYrM;-><init>(Lcom/squareup/cardreader/dipper/ReaderUiEventSink;Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 267
    invoke-virtual {p2, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 264
    invoke-virtual {v1, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public onTmsCountryCode(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 369
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->hasValidTmsCountryCode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SPE_TMS_LOGIN_CHECK:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->showInvalidTmsScreen(Lcom/squareup/cardreader/CardReaderInfo;)V

    :cond_0
    return-void
.end method

.method public onUseChipCard(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 402
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->emvDipScreenHandler:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-interface {v0}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->shouldDisableEmvDips()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 407
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->lazySmartPaymentFlowStarter:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_SWIPED_AWAITING_FW_UPDATE:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkFirmwareUpdateNotBlocking(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 412
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->hasInvalidTmsForSwipe(Lcom/squareup/cardreader/CardReaderInfo;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 413
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->showInvalidTmsScreen(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void

    .line 417
    :cond_2
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->isSecureSessionRequiredForSwipe(Lcom/squareup/cardreader/CardReaderInfo;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 418
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerHudManager:Ldagger/Lazy;

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/dipper/ReaderHudManager;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->secure_session_required_for_swipe_title:I

    .line 419
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->toastReaderStillConnecting(I)V

    return-void

    .line 423
    :cond_3
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->dipperUiErrorDisplayTypeSelector:Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;

    invoke-interface {p1}, Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector;->getUseChipCardDisplayType()Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector$DisplayType;

    move-result-object p1

    sget-object v0, Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector$DisplayType;->CONCRETE_WARNING_SCREEN:Lcom/squareup/cardreader/ui/api/DipperUiErrorDisplayTypeSelector$DisplayType;

    if-ne p1, v0, :cond_4

    .line 424
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowConcreteWarningScreen;

    sget-object v1, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$CardNotRead;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$CardNotRead;

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowConcreteWarningScreen;-><init>(Lcom/squareup/container/LayoutScreen;)V

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    goto :goto_0

    .line 427
    :cond_4
    new-instance p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->DIP_REQUIRED_ROOT_SCREEN:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 429
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 430
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 431
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    :goto_0
    return-void
.end method

.method public sendSecureSessionMessageToServer(Lcom/squareup/cardreader/CardReader;[B)V
    .locals 0

    return-void
.end method

.method public silenceReaderHuds(Z)V
    .locals 1

    .line 639
    iput-boolean p1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->silenceReaderHuds:Z

    if-eqz p1, :cond_0

    .line 641
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerHudConnectionEventHandler:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/CardReaderHub;->removeCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    goto :goto_0

    .line 643
    :cond_0
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->readerHudConnectionEventHandler:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/CardReaderHub;->addCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    :goto_0
    return-void
.end method
