.class public interface abstract Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;
.super Ljava/lang/Object;
.source "EmvCardInsertRemoveProcessor.java"


# virtual methods
.method public abstract processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method
