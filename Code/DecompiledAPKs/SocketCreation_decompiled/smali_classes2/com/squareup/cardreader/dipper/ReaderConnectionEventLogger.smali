.class public Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;
.super Ljava/lang/Object;
.source "ReaderConnectionEventLogger.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;


# instance fields
.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;


# direct methods
.method public constructor <init>(Lcom/squareup/log/OhSnapLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    return-void
.end method

.method private log(Lcom/squareup/cardreader/CardReaderInfo;Z)V
    .locals 7

    if-eqz p2, :cond_0

    const-string p2, "Connected"

    goto :goto_0

    :cond_0
    const-string p2, "Disconnected"

    .line 32
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v0

    .line 33
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->name()Ljava/lang/String;

    move-result-object v1

    .line 34
    sget-object v2, Lcom/squareup/protos/client/bills/CardData$ReaderType;->UNKNOWN:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v0, v2, :cond_1

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isAudio()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":AUDIO"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 37
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isWireless()Z

    move-result v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    if-eqz v0, :cond_2

    .line 38
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v5, Lcom/squareup/log/OhSnapLogger$EventType;->READER:Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p2, v6, v3

    aput-object v1, v6, v2

    .line 39
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v6, v4

    const-string p1, "CardReader %s (%s) address: %s"

    .line 38
    invoke-static {p1, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v5, p1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    goto :goto_1

    .line 41
    :cond_2
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->READER:Lcom/squareup/log/OhSnapLogger$EventType;

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p2, v4, v3

    aput-object v1, v4, v2

    const-string p2, "CardReader %s (%s)"

    invoke-static {p2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, v0, p2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    :goto_1
    return-void
.end method


# virtual methods
.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 1

    .line 23
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;->log(Lcom/squareup/cardreader/CardReaderInfo;Z)V

    return-void
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 1

    .line 27
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/dipper/ReaderConnectionEventLogger;->log(Lcom/squareup/cardreader/CardReaderInfo;Z)V

    return-void
.end method
