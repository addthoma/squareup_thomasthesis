.class abstract Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;
.super Ljava/lang/Object;
.source "ReaderBatteryStatusHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "HudBatteryHelper"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;


# direct methods
.method private constructor <init>(Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;)V
    .locals 0

    .line 211
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;->this$0:Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$1;)V
    .locals 0

    .line 211
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;-><init>(Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;)V

    return-void
.end method


# virtual methods
.method public getBatteryDeadText()Ljava/lang/CharSequence;
    .locals 2

    .line 232
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;->this$0:Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;

    invoke-static {v0}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->access$000(Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;)Lcom/squareup/util/Res;

    move-result-object v0

    sget v1, Lcom/squareup/cardreader/ui/R$string;->hud_charge_reader_dead_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBatteryLowText()Ljava/lang/CharSequence;
    .locals 2

    .line 228
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;->this$0:Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;

    invoke-static {v0}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->access$000(Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;)Lcom/squareup/util/Res;

    move-result-object v0

    sget v1, Lcom/squareup/cardreader/ui/R$string;->hud_charge_reader_low_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract getBatteryNormalText()Ljava/lang/CharSequence;
.end method

.method abstract getCharging()I
.end method

.method public getChargingText()Ljava/lang/CharSequence;
    .locals 2

    .line 236
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;->this$0:Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;

    invoke-static {v0}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->access$000(Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;)Lcom/squareup/util/Res;

    move-result-object v0

    sget v1, Lcom/squareup/cardreader/ui/R$string;->hud_charge_reader_charging_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method abstract getErrorScreenType()Lcom/squareup/cardreader/ui/api/ReaderWarningType;
.end method

.method abstract getFullBattery()I
.end method

.method abstract getHighBattery()I
.end method

.method abstract getLowBattery()I
.end method

.method abstract getMidBattery()I
.end method
