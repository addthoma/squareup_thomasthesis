.class public interface abstract Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;
.super Ljava/lang/Object;
.source "FirmwareUpdateListener.java"


# virtual methods
.method public abstract onFirmwareManifestServerResponseFailure(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onFirmwareManifestServerResponseSuccess(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onFirmwareUpdateAborted(Lcom/squareup/cardreader/CardReaderInfo;Z)V
.end method

.method public abstract onFirmwareUpdateComplete(Lcom/squareup/cardreader/CardReaderInfo;Z)V
.end method

.method public abstract onFirmwareUpdateError(Lcom/squareup/cardreader/CardReaderInfo;ZLcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)V
.end method

.method public abstract onFirmwareUpdateProgress(Lcom/squareup/cardreader/CardReaderInfo;ZIIZ)V
.end method

.method public abstract onFirmwareUpdateStarted(Lcom/squareup/cardreader/CardReaderInfo;Z)V
.end method

.method public abstract onReaderFailedToConnectAfterRebootingFwup(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onSendFirmwareManifestToServer(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method
