.class public abstract Lcom/squareup/cardreader/audio/AudioCardReaderContextComponent$SharedModule;
.super Ljava/lang/Object;
.source "AudioCardReaderContextComponent.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/wavpool/swipe/AndroidAudioModule;,
        Lcom/squareup/cardreader/CardReaderModule;,
        Lcom/squareup/cardreader/CardReaderModule$AllExceptDipper;,
        Lcom/squareup/cardreader/CardReaderModule$Prod;,
        Lcom/squareup/cardreader/LocalCardReaderModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/audio/AudioCardReaderContextComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "SharedModule"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideCardReaderGraphInitializer(Lcom/squareup/wavpool/swipe/AudioGraphInitializer;)Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
