.class public Lcom/squareup/cardreader/PaymentTimings;
.super Ljava/lang/Object;
.source "PaymentTimings.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/PaymentTimings$PaymentTimingsIterator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable<",
        "Lcom/squareup/cardreader/PaymentTiming;",
        ">;"
    }
.end annotation


# static fields
.field public static final EMPTY:Lcom/squareup/cardreader/PaymentTimings;


# instance fields
.field private paymentTimings:[Lcom/squareup/cardreader/PaymentTiming;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 11
    new-instance v0, Lcom/squareup/cardreader/PaymentTimings;

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/squareup/cardreader/PaymentTiming;

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/PaymentTimings;-><init>([Lcom/squareup/cardreader/PaymentTiming;)V

    sput-object v0, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    return-void
.end method

.method public constructor <init>([Lcom/squareup/cardreader/PaymentTiming;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/cardreader/PaymentTimings;->paymentTimings:[Lcom/squareup/cardreader/PaymentTiming;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/cardreader/PaymentTimings;)[Lcom/squareup/cardreader/PaymentTiming;
    .locals 0

    .line 10
    iget-object p0, p0, Lcom/squareup/cardreader/PaymentTimings;->paymentTimings:[Lcom/squareup/cardreader/PaymentTiming;

    return-object p0
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/squareup/cardreader/PaymentTiming;",
            ">;"
        }
    .end annotation

    .line 20
    new-instance v0, Lcom/squareup/cardreader/PaymentTimings$PaymentTimingsIterator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/cardreader/PaymentTimings$PaymentTimingsIterator;-><init>(Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/cardreader/PaymentTimings$1;)V

    return-object v0
.end method
