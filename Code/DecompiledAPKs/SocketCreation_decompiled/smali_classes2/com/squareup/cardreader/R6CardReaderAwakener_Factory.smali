.class public final Lcom/squareup/cardreader/R6CardReaderAwakener_Factory;
.super Ljava/lang/Object;
.source "R6CardReaderAwakener_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/R6CardReaderAwakener;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderConstantsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderConstants;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderDispatchProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderConstants;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/cardreader/R6CardReaderAwakener_Factory;->cardReaderDispatchProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/cardreader/R6CardReaderAwakener_Factory;->clockProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p3, p0, Lcom/squareup/cardreader/R6CardReaderAwakener_Factory;->cardReaderConstantsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/R6CardReaderAwakener_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderConstants;",
            ">;)",
            "Lcom/squareup/cardreader/R6CardReaderAwakener_Factory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/cardreader/R6CardReaderAwakener_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cardreader/R6CardReaderAwakener_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/cardreader/CardReaderDispatch;Lcom/squareup/util/Clock;Lcom/squareup/cardreader/CardReaderConstants;)Lcom/squareup/cardreader/R6CardReaderAwakener;
    .locals 1

    .line 43
    new-instance v0, Lcom/squareup/cardreader/R6CardReaderAwakener;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cardreader/R6CardReaderAwakener;-><init>(Lcom/squareup/cardreader/CardReaderDispatch;Lcom/squareup/util/Clock;Lcom/squareup/cardreader/CardReaderConstants;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/R6CardReaderAwakener;
    .locals 3

    .line 32
    iget-object v0, p0, Lcom/squareup/cardreader/R6CardReaderAwakener_Factory;->cardReaderDispatchProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    iget-object v1, p0, Lcom/squareup/cardreader/R6CardReaderAwakener_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Clock;

    iget-object v2, p0, Lcom/squareup/cardreader/R6CardReaderAwakener_Factory;->cardReaderConstantsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/CardReaderConstants;

    invoke-static {v0, v1, v2}, Lcom/squareup/cardreader/R6CardReaderAwakener_Factory;->newInstance(Lcom/squareup/cardreader/CardReaderDispatch;Lcom/squareup/util/Clock;Lcom/squareup/cardreader/CardReaderConstants;)Lcom/squareup/cardreader/R6CardReaderAwakener;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/R6CardReaderAwakener_Factory;->get()Lcom/squareup/cardreader/R6CardReaderAwakener;

    move-result-object v0

    return-object v0
.end method
