.class public interface abstract Lcom/squareup/cardreader/SystemFeatureLegacy$SystemFeatureListener;
.super Ljava/lang/Object;
.source "SystemFeatureLegacy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/SystemFeatureLegacy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SystemFeatureListener"
.end annotation


# virtual methods
.method public abstract onCapabilitiesReceived(ZLjava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrsCapability;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onChargeCycleCountReceived(I)V
.end method

.method public abstract onFirmwareVersionReceived(Ljava/lang/String;)V
.end method

.method public abstract onHardwareSerialNumberReceived(Ljava/lang/String;)V
.end method

.method public abstract onReaderError(Lcom/squareup/cardreader/lcr/CrsReaderError;)V
.end method
