.class public Lcom/squareup/cardreader/GlobalCardReaderModule;
.super Ljava/lang/Object;
.source "GlobalCardReaderModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;,
        Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$provideLCRExecutor$0(Ljava/lang/Runnable;)Ljava/lang/Thread;
    .locals 2

    .line 80
    new-instance v0, Lcom/squareup/cardreader/GlobalCardReaderModule$1;

    const-string v1, "Sq-lcr_runner"

    invoke-direct {v0, p0, v1}, Lcom/squareup/cardreader/GlobalCardReaderModule$1;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic lambda$provideSquidInterfaceScheduler$1(Ljava/lang/Runnable;)Ljava/lang/Thread;
    .locals 2

    .line 100
    new-instance v0, Ljava/lang/Thread;

    const-string v1, "Sq-squid_interface_runner"

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    return-object v0
.end method

.method static provideLCRExecutor()Ljava/util/concurrent/ExecutorService;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 79
    sget-object v0, Lcom/squareup/cardreader/-$$Lambda$GlobalCardReaderModule$fcjMyFaYz4RkgalHMM8zkfm-cDE;->INSTANCE:Lcom/squareup/cardreader/-$$Lambda$GlobalCardReaderModule$fcjMyFaYz4RkgalHMM8zkfm-cDE;

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method

.method static provideSquidInterfaceScheduler()Lio/reactivex/Scheduler;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 99
    sget-object v0, Lcom/squareup/cardreader/-$$Lambda$GlobalCardReaderModule$KVDyeUuU-IPIMsEChAEBXMVSsGo;->INSTANCE:Lcom/squareup/cardreader/-$$Lambda$GlobalCardReaderModule$KVDyeUuU-IPIMsEChAEBXMVSsGo;

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/schedulers/Schedulers;->from(Ljava/util/concurrent/Executor;)Lio/reactivex/Scheduler;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method cardReaderPauseAndResumer()Lcom/squareup/cardreader/CardReaderPauseAndResumer;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 70
    new-instance v0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    invoke-direct {v0}, Lcom/squareup/cardreader/CardReaderPauseAndResumer;-><init>()V

    return-object v0
.end method

.method provideCardReaderListeners(Lcom/squareup/cardreader/RealCardReaderListeners;)Lcom/squareup/cardreader/CardReaderListeners;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    return-object p1
.end method

.method provideNativeBinaries()Lcom/squareup/cardreader/NativeBinaries;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 28
    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "hodor"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    .line 29
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "Square"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v0, :cond_0

    .line 32
    sget-object v0, Lcom/squareup/cardreader/NativeBinaries;->NONE:Lcom/squareup/cardreader/NativeBinaries;

    return-object v0

    :cond_0
    if-eqz v1, :cond_1

    .line 34
    sget-object v0, Lcom/squareup/cardreader/NativeBinaries;->SQUID:Lcom/squareup/cardreader/NativeBinaries;

    return-object v0

    .line 36
    :cond_1
    sget-object v0, Lcom/squareup/cardreader/NativeBinaries;->POS:Lcom/squareup/cardreader/NativeBinaries;

    return-object v0
.end method

.method provideRealCardReaderListeners()Lcom/squareup/cardreader/RealCardReaderListeners;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 66
    new-instance v0, Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-direct {v0}, Lcom/squareup/cardreader/RealCardReaderListeners;-><init>()V

    return-object v0
.end method

.method provideRunning(Lcom/squareup/cardreader/CardReaderPauseAndResumer;)Ljava/lang/Boolean;
    .locals 0
    .annotation runtime Lcom/squareup/cardreader/CardReaderPauseAndResumer$Running;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 74
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderPauseAndResumer;->isRunning()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
