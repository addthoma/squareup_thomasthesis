.class Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;
.super Ljava/lang/Object;
.source "CardReaderPowerMonitor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderPowerMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PowerRequester"
.end annotation


# static fields
.field private static final MIN_POWER_REQUEST_INTERVAL_MS:J = 0xdbba0L


# instance fields
.field private final cardReader:Lcom/squareup/cardreader/CardReader;

.field private haveRequestedPowerOnce:Z

.field private lastPowerRequestTime:J

.field final synthetic this$0:Lcom/squareup/cardreader/CardReaderPowerMonitor;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/CardReaderPowerMonitor;Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;->this$0:Lcom/squareup/cardreader/CardReaderPowerMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    iput-object p2, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;->cardReader:Lcom/squareup/cardreader/CardReader;

    const-wide/16 p1, 0x0

    .line 124
    iput-wide p1, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;->lastPowerRequestTime:J

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;)Lcom/squareup/cardreader/CardReader;
    .locals 0

    .line 115
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;->cardReader:Lcom/squareup/cardreader/CardReader;

    return-object p0
.end method


# virtual methods
.method public getCardReaderId()Lcom/squareup/cardreader/CardReaderId;
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;->cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    return-object v0
.end method

.method requestCardReaderPowerStatus()Z
    .locals 6

    .line 145
    iget-boolean v0, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;->haveRequestedPowerOnce:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;->this$0:Lcom/squareup/cardreader/CardReaderPowerMonitor;

    .line 146
    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderPowerMonitor;->access$100(Lcom/squareup/cardreader/CardReaderPowerMonitor;)Lcom/squareup/util/Clock;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;->lastPowerRequestTime:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0xdbba0

    cmp-long v0, v2, v4

    if-gez v0, :cond_0

    return v1

    :cond_0
    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "Requesting power status"

    .line 149
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 150
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;->cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->requestPowerStatus()V

    const/4 v0, 0x1

    .line 151
    iput-boolean v0, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;->haveRequestedPowerOnce:Z

    .line 152
    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;->this$0:Lcom/squareup/cardreader/CardReaderPowerMonitor;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderPowerMonitor;->access$100(Lcom/squareup/cardreader/CardReaderPowerMonitor;)Lcom/squareup/util/Clock;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;->lastPowerRequestTime:J

    return v0
.end method

.method public run()V
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;->this$0:Lcom/squareup/cardreader/CardReaderPowerMonitor;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;->cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderPowerMonitor;->requestPowerStatus(Lcom/squareup/cardreader/CardReaderId;)Z

    return-void
.end method
