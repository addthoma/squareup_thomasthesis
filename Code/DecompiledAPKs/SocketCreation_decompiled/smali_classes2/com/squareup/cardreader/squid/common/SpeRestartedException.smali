.class public final Lcom/squareup/cardreader/squid/common/SpeRestartedException;
.super Ljava/lang/RuntimeException;
.source "SpeRestartedException.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00060\u0001j\u0002`\u0002B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "Lcom/squareup/cardreader/squid/common/SpeRestartedException;",
        "Ljava/lang/RuntimeException;",
        "Lkotlin/RuntimeException;",
        "()V",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/cardreader/squid/common/SpeRestartedException;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 3
    new-instance v0, Lcom/squareup/cardreader/squid/common/SpeRestartedException;

    invoke-direct {v0}, Lcom/squareup/cardreader/squid/common/SpeRestartedException;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/squid/common/SpeRestartedException;->INSTANCE:Lcom/squareup/cardreader/squid/common/SpeRestartedException;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const-string v0, "SPE CardReader already reported reader ready. SPE may have crashed or restarted unexpectedly."

    .line 3
    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    return-void
.end method
