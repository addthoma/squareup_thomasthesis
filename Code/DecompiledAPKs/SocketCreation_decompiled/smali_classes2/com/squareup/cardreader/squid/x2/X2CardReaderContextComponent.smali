.class public interface abstract Lcom/squareup/cardreader/squid/x2/X2CardReaderContextComponent;
.super Ljava/lang/Object;
.source "X2CardReaderContextComponent.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderContextComponent;


# annotations
.annotation runtime Ldagger/Component;
    dependencies = {
        Lcom/squareup/cardreader/CardReaderContextParent;
    }
    modules = {
        Lcom/squareup/cardreader/squid/common/SpeCardReaderModule;,
        Lcom/squareup/cardreader/CardReaderModule;,
        Lcom/squareup/cardreader/CardReaderModule$AllExceptDipper;,
        Lcom/squareup/cardreader/CardReaderModule$Prod;,
        Lcom/squareup/cardreader/squid/x2/X2LocalCardReaderModule;
    }
.end annotation
