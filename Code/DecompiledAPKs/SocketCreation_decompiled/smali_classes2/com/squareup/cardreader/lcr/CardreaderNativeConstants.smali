.class public interface abstract Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;
.super Ljava/lang/Object;
.source "CardreaderNativeConstants.java"


# static fields
.field public static final CRS_APP_PROTOCOL_VERSION:I

.field public static final CRS_EP_PROTOCOL_VERSION:I

.field public static final CRS_TRANSPORT_PAUSE_TIMEOUT_IN_MS:I

.field public static final CRS_TRANSPORT_PROTOCOL_VERSION:I

.field public static final CRS_TRANSPORT_PROTOCOL_VERSION_STAND:I

.field public static final CRS_TRANSPORT_SMALL_FRAME_THRESHOLD:I

.field public static final CR_CARDHOLDER_VERIFICATION_PERFORMED_FLAG_CVM_FALLTHROUGH:I

.field public static final CR_CARDHOLDER_VERIFICATION_PERFORMED_FLAG_RFU_BIT8:I

.field public static final CR_PAYMENT_ACCOUNT_TYPE_MAX:I

.field public static final CR_PAYMENT_APP_ADF_NAME_MAX:I

.field public static final CR_PAYMENT_APP_LABEL_MAX:I

.field public static final CR_PAYMENT_APP_PREFNAME_MAX:I

.field public static final CR_PAYMENT_LAST4_LENGTH:I

.field public static final CR_PAYMENT_MAGSWIPE_DUPLICATE_TIMEOUT_MS:I

.field public static final CR_PAYMENT_MAGSWIPE_MAX_TIME_BETWEEN_M1_MESSAGES_MS:I

.field public static final CR_PAYMENT_MAX_ICC_FAILURES:I

.field public static final CR_PAYMENT_MAX_TIMINGS:I

.field public static final CR_PAYMENT_NAME_MAX_LENGTH:I

.field public static final CR_PAYMENT_PAN_IIN_PREFIX_LENGTH:I

.field public static final CR_PAYMENT_PIN_ENTRY_TIMEOUT_MS:I

.field public static final CR_PAYMENT_TIMING_LABEL_SIZE:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 12
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->CRS_APP_PROTOCOL_VERSION_get()I

    move-result v0

    sput v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CRS_APP_PROTOCOL_VERSION:I

    .line 13
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->CRS_EP_PROTOCOL_VERSION_get()I

    move-result v0

    sput v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CRS_EP_PROTOCOL_VERSION:I

    .line 14
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->CRS_TRANSPORT_PROTOCOL_VERSION_get()I

    move-result v0

    sput v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CRS_TRANSPORT_PROTOCOL_VERSION:I

    .line 15
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->CRS_TRANSPORT_PAUSE_TIMEOUT_IN_MS_get()I

    move-result v0

    sput v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CRS_TRANSPORT_PAUSE_TIMEOUT_IN_MS:I

    .line 16
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->CR_PAYMENT_MAX_ICC_FAILURES_get()I

    move-result v0

    sput v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CR_PAYMENT_MAX_ICC_FAILURES:I

    .line 17
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->CR_PAYMENT_MAGSWIPE_DUPLICATE_TIMEOUT_MS_get()I

    move-result v0

    sput v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CR_PAYMENT_MAGSWIPE_DUPLICATE_TIMEOUT_MS:I

    .line 18
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->CR_PAYMENT_MAGSWIPE_MAX_TIME_BETWEEN_M1_MESSAGES_MS_get()I

    move-result v0

    sput v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CR_PAYMENT_MAGSWIPE_MAX_TIME_BETWEEN_M1_MESSAGES_MS:I

    .line 19
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->CR_PAYMENT_PIN_ENTRY_TIMEOUT_MS_get()I

    move-result v0

    sput v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CR_PAYMENT_PIN_ENTRY_TIMEOUT_MS:I

    .line 20
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->CR_PAYMENT_TIMING_LABEL_SIZE_get()I

    move-result v0

    sput v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CR_PAYMENT_TIMING_LABEL_SIZE:I

    .line 21
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->CR_PAYMENT_MAX_TIMINGS_get()I

    move-result v0

    sput v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CR_PAYMENT_MAX_TIMINGS:I

    .line 23
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->CR_PAYMENT_APP_ADF_NAME_MAX_get()I

    move-result v0

    sput v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CR_PAYMENT_APP_ADF_NAME_MAX:I

    .line 24
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->CR_PAYMENT_APP_LABEL_MAX_get()I

    move-result v0

    sput v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CR_PAYMENT_APP_LABEL_MAX:I

    .line 25
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->CR_PAYMENT_APP_PREFNAME_MAX_get()I

    move-result v0

    sput v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CR_PAYMENT_APP_PREFNAME_MAX:I

    .line 26
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->CR_PAYMENT_LAST4_LENGTH_get()I

    move-result v0

    sput v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CR_PAYMENT_LAST4_LENGTH:I

    .line 27
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->CR_PAYMENT_NAME_MAX_LENGTH_get()I

    move-result v0

    sput v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CR_PAYMENT_NAME_MAX_LENGTH:I

    .line 28
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->CR_PAYMENT_PAN_IIN_PREFIX_LENGTH_get()I

    move-result v0

    sput v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CR_PAYMENT_PAN_IIN_PREFIX_LENGTH:I

    .line 29
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->CR_PAYMENT_ACCOUNT_TYPE_MAX_get()I

    move-result v0

    sput v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CR_PAYMENT_ACCOUNT_TYPE_MAX:I

    .line 31
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->CR_CARDHOLDER_VERIFICATION_PERFORMED_FLAG_CVM_FALLTHROUGH_get()I

    move-result v0

    sput v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CR_CARDHOLDER_VERIFICATION_PERFORMED_FLAG_CVM_FALLTHROUGH:I

    .line 32
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->CR_CARDHOLDER_VERIFICATION_PERFORMED_FLAG_RFU_BIT8_get()I

    move-result v0

    sput v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CR_CARDHOLDER_VERIFICATION_PERFORMED_FLAG_RFU_BIT8:I

    .line 34
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->CRS_TRANSPORT_PROTOCOL_VERSION_STAND_get()I

    move-result v0

    sput v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CRS_TRANSPORT_PROTOCOL_VERSION_STAND:I

    .line 35
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNativeJNI;->CRS_TRANSPORT_SMALL_FRAME_THRESHOLD_get()I

    move-result v0

    sput v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CRS_TRANSPORT_SMALL_FRAME_THRESHOLD:I

    return-void
.end method
