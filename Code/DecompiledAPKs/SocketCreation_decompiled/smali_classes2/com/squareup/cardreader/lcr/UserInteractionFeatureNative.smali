.class public Lcom/squareup/cardreader/lcr/UserInteractionFeatureNative;
.super Ljava/lang/Object;
.source "UserInteractionFeatureNative.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cr_user_interaction_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)Lcom/squareup/cardreader/lcr/CrUserInteractionResult;
    .locals 2

    .line 17
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeJNI;->cr_user_interaction_free(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrUserInteractionResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrUserInteractionResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_user_interaction_identify_reader(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)Lcom/squareup/cardreader/lcr/CrUserInteractionResult;
    .locals 2

    .line 21
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeJNI;->cr_user_interaction_identify_reader(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrUserInteractionResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrUserInteractionResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_user_interaction_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)Lcom/squareup/cardreader/lcr/CrUserInteractionResult;
    .locals 2

    .line 13
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeJNI;->cr_user_interaction_term(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrUserInteractionResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrUserInteractionResult;

    move-result-object p0

    return-object p0
.end method

.method public static user_interaction_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;
    .locals 4

    .line 25
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeJNI;->user_interaction_initialize(J)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p0, v0, v2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 26
    :cond_0
    new-instance p0, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;-><init>(JZ)V

    :goto_0
    return-object p0
.end method
