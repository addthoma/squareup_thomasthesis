.class public Lcom/squareup/cardreader/lcr/PaymentFeatureNativeJNI;
.super Ljava/lang/Object;
.source "PaymentFeatureNativeJNI.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final native cr_payment_cancel_payment(J)I
.end method

.method public static final native cr_payment_enable_swipe_passthrough(JZ)I
.end method

.method public static final native cr_payment_free(J)I
.end method

.method public static final native cr_payment_request_card_presence(J)I
.end method

.method public static final native cr_payment_term(J)I
.end method

.method public static final native payment_initialize(JLjava/lang/Object;II)J
.end method

.method public static final native payment_process_server_response(J[B)I
.end method

.method public static final native payment_select_account_type(JI)I
.end method

.method public static final native payment_select_application(J[B)I
.end method

.method public static final native payment_send_powerup_hint(JI)I
.end method

.method public static final native payment_start_payment(JJIIIIIIII)I
.end method

.method public static final native payment_tmn_cancel_request(J)I
.end method

.method public static final native payment_tmn_send_bytes_to_reader(J[B)I
.end method

.method public static final native payment_tmn_start_miryo(J[B)I
.end method

.method public static final native payment_tmn_start_transaction(JILjava/lang/String;IJ)I
.end method

.method public static final native payment_tmn_write_notify_ack(J)I
.end method
