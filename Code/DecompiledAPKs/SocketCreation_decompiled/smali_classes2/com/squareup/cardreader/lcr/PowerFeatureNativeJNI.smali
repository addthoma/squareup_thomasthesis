.class public Lcom/squareup/cardreader/lcr/PowerFeatureNativeJNI;
.super Ljava/lang/Object;
.source "PowerFeatureNativeJNI.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final native cr_power_free(J)I
.end method

.method public static final native cr_power_get_battery_voltage(J)I
.end method

.method public static final native cr_power_off(J)I
.end method

.method public static final native cr_power_term(J)I
.end method

.method public static final native power_initialize(JLjava/lang/Object;)J
.end method
