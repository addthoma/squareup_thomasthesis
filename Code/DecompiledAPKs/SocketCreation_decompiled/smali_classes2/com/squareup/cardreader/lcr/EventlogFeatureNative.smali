.class public Lcom/squareup/cardreader/lcr/EventlogFeatureNative;
.super Ljava/lang/Object;
.source "EventlogFeatureNative.java"

# interfaces
.implements Lcom/squareup/cardreader/lcr/EventlogFeatureNativeConstants;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cr_eventlog_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;)Lcom/squareup/cardreader/lcr/CrEventLogResult;
    .locals 2

    .line 17
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/EventlogFeatureNativeJNI;->cr_eventlog_free(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrEventLogResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrEventLogResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_eventlog_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;)Lcom/squareup/cardreader/lcr/CrEventLogResult;
    .locals 2

    .line 13
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/EventlogFeatureNativeJNI;->cr_eventlog_term(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrEventLogResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrEventLogResult;

    move-result-object p0

    return-object p0
.end method

.method public static eventlog_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;
    .locals 3

    .line 21
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/squareup/cardreader/lcr/EventlogFeatureNativeJNI;->eventlog_initialize(JLjava/lang/Object;)J

    move-result-wide p0

    const-wide/16 v0, 0x0

    cmp-long v2, p0, v0

    if-nez v2, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 22
    :cond_0
    new-instance v0, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;-><init>(JZ)V

    move-object p0, v0

    :goto_0
    return-object p0
.end method
