.class public final enum Lcom/squareup/cardreader/lcr/CrsBatteryMode;
.super Ljava/lang/Enum;
.source "CrsBatteryMode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/lcr/CrsBatteryMode$SwigNext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/lcr/CrsBatteryMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/lcr/CrsBatteryMode;

.field public static final enum CRS_BATTERY_MODE_CHARGED:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

.field public static final enum CRS_BATTERY_MODE_CHARGING:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

.field public static final enum CRS_BATTERY_MODE_DISCHARGING:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

.field public static final enum CRS_BATTERY_MODE_LOW_CRITICAL:Lcom/squareup/cardreader/lcr/CrsBatteryMode;


# instance fields
.field private final swigValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 12
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    const/4 v1, 0x0

    const-string v2, "CRS_BATTERY_MODE_DISCHARGING"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/cardreader/lcr/CrsBatteryMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->CRS_BATTERY_MODE_DISCHARGING:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    const/4 v2, 0x1

    const-string v3, "CRS_BATTERY_MODE_CHARGING"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardreader/lcr/CrsBatteryMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->CRS_BATTERY_MODE_CHARGING:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    .line 14
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    const/4 v3, 0x2

    const-string v4, "CRS_BATTERY_MODE_CHARGED"

    invoke-direct {v0, v4, v3}, Lcom/squareup/cardreader/lcr/CrsBatteryMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->CRS_BATTERY_MODE_CHARGED:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    .line 15
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    const/4 v4, 0x3

    const-string v5, "CRS_BATTERY_MODE_LOW_CRITICAL"

    invoke-direct {v0, v5, v4}, Lcom/squareup/cardreader/lcr/CrsBatteryMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->CRS_BATTERY_MODE_LOW_CRITICAL:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    .line 11
    sget-object v5, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->CRS_BATTERY_MODE_DISCHARGING:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->CRS_BATTERY_MODE_CHARGING:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->CRS_BATTERY_MODE_CHARGED:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->CRS_BATTERY_MODE_LOW_CRITICAL:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->$VALUES:[Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 33
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrsBatteryMode$SwigNext;->access$008()I

    move-result p1

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->swigValue:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    iput p3, p0, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->swigValue:I

    add-int/lit8 p3, p3, 0x1

    .line 39
    invoke-static {p3}, Lcom/squareup/cardreader/lcr/CrsBatteryMode$SwigNext;->access$002(I)I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/cardreader/lcr/CrsBatteryMode;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/lcr/CrsBatteryMode;",
            ")V"
        }
    .end annotation

    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 44
    iget p1, p3, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->swigValue:I

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->swigValue:I

    .line 45
    iget p1, p0, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->swigValue:I

    add-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrsBatteryMode$SwigNext;->access$002(I)I

    return-void
.end method

.method public static swigToEnum(I)Lcom/squareup/cardreader/lcr/CrsBatteryMode;
    .locals 6

    .line 22
    const-class v0, Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    .line 23
    array-length v2, v1

    if-ge p0, v2, :cond_0

    if-ltz p0, :cond_0

    aget-object v2, v1, p0

    iget v2, v2, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->swigValue:I

    if-ne v2, p0, :cond_0

    .line 24
    aget-object p0, v1, p0

    return-object p0

    .line 25
    :cond_0
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    .line 26
    iget v5, v4, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->swigValue:I

    if-ne v5, p0, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 28
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No enum "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " with value "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/lcr/CrsBatteryMode;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/lcr/CrsBatteryMode;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->$VALUES:[Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/lcr/CrsBatteryMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    return-object v0
.end method


# virtual methods
.method public final swigValue()I
    .locals 1

    .line 18
    iget v0, p0, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->swigValue:I

    return v0
.end method
