.class public Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;
.super Ljava/lang/Object;
.source "cr_secure_touch_mode_feature_event_api_t.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 111
    invoke-static {}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->new_cr_secure_touch_mode_feature_event_api_t()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;-><init>(JZ)V

    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCMemOwn:Z

    .line 17
    iput-wide p1, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCPtr:J

    return-void
.end method

.method protected static getCPtr(Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)J
    .locals 2

    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    .line 21
    :cond_0
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCPtr:J

    :goto_0
    return-wide v0
.end method


# virtual methods
.method public declared-synchronized delete()V
    .locals 5

    monitor-enter p0

    .line 29
    :try_start_0
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCPtr:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 31
    iput-boolean v0, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->delete_cr_secure_touch_mode_feature_event_api_t(J)V

    .line 34
    :cond_0
    iput-wide v2, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCPtr:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .locals 0

    .line 25
    invoke-virtual {p0}, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->delete()V

    return-void
.end method

.method public getContext()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_void;
    .locals 5

    .line 106
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_event_api_t_context_get(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 107
    :cond_0
    new-instance v2, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_void;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_void;-><init>(JZ)V

    move-object v0, v2

    :goto_0
    return-object v0
.end method

.method public getDisable_squid_touch_driver_request()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t__void;
    .locals 5

    .line 43
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_event_api_t_disable_squid_touch_driver_request_get(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 44
    :cond_0
    new-instance v2, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t__void;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t__void;-><init>(JZ)V

    move-object v0, v2

    :goto_0
    return-object v0
.end method

.method public getEnable_squid_touch_driver_request()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t__void;
    .locals 5

    .line 52
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_event_api_t_enable_squid_touch_driver_request_get(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 53
    :cond_0
    new-instance v2, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t__void;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t__void;-><init>(JZ)V

    move-object v0, v2

    :goto_0
    return-object v0
.end method

.method public getHide_pin_pad_request()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t__void;
    .locals 5

    .line 88
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_event_api_t_hide_pin_pad_request_get(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 89
    :cond_0
    new-instance v2, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t__void;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t__void;-><init>(JZ)V

    move-object v0, v2

    :goto_0
    return-object v0
.end method

.method public getOn_keepalive_failed()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t_enum_cr_secure_touch_mode_feature_keepalive_error_t__void;
    .locals 5

    .line 70
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_event_api_t_on_keepalive_failed_get(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 71
    :cond_0
    new-instance v2, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t_enum_cr_secure_touch_mode_feature_keepalive_error_t__void;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t_enum_cr_secure_touch_mode_feature_keepalive_error_t__void;-><init>(JZ)V

    move-object v0, v2

    :goto_0
    return-object v0
.end method

.method public getOn_pin_pad_center_point()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t_p_struct_crs_stm_pin_pad_center_point_t__void;
    .locals 5

    .line 97
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_event_api_t_on_pin_pad_center_point_get(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 98
    :cond_0
    new-instance v2, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t_p_struct_crs_stm_pin_pad_center_point_t__void;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t_p_struct_crs_stm_pin_pad_center_point_t__void;-><init>(JZ)V

    move-object v0, v2

    :goto_0
    return-object v0
.end method

.method public getOn_pin_pad_event()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t_enum_crs_stm_pin_pad_event_id_t__void;
    .locals 5

    .line 79
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_event_api_t_on_pin_pad_event_get(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 80
    :cond_0
    new-instance v2, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t_enum_crs_stm_pin_pad_event_id_t__void;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t_enum_crs_stm_pin_pad_event_id_t__void;-><init>(JZ)V

    move-object v0, v2

    :goto_0
    return-object v0
.end method

.method public getShow_pin_pad_request()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_q_const__cr_card_info_t_enum_cr_secure_touch_mode_feature_pin_try_t_p_struct_cr_secure_touch_mode_feature_t__void;
    .locals 5

    .line 61
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_event_api_t_show_pin_pad_request_get(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 62
    :cond_0
    new-instance v2, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_q_const__cr_card_info_t_enum_cr_secure_touch_mode_feature_pin_try_t_p_struct_cr_secure_touch_mode_feature_t__void;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_q_const__cr_card_info_t_enum_cr_secure_touch_mode_feature_pin_try_t_p_struct_cr_secure_touch_mode_feature_t__void;-><init>(JZ)V

    move-object v0, v2

    :goto_0
    return-object v0
.end method

.method public setContext(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_void;)V
    .locals 4

    .line 102
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCPtr:J

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_void;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_void;)J

    move-result-wide v2

    invoke-static {v0, v1, p0, v2, v3}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_event_api_t_context_set(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;J)V

    return-void
.end method

.method public setDisable_squid_touch_driver_request(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t__void;)V
    .locals 4

    .line 39
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCPtr:J

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t__void;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t__void;)J

    move-result-wide v2

    invoke-static {v0, v1, p0, v2, v3}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_event_api_t_disable_squid_touch_driver_request_set(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;J)V

    return-void
.end method

.method public setEnable_squid_touch_driver_request(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t__void;)V
    .locals 4

    .line 48
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCPtr:J

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t__void;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t__void;)J

    move-result-wide v2

    invoke-static {v0, v1, p0, v2, v3}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_event_api_t_enable_squid_touch_driver_request_set(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;J)V

    return-void
.end method

.method public setHide_pin_pad_request(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t__void;)V
    .locals 4

    .line 84
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCPtr:J

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t__void;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t__void;)J

    move-result-wide v2

    invoke-static {v0, v1, p0, v2, v3}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_event_api_t_hide_pin_pad_request_set(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;J)V

    return-void
.end method

.method public setOn_keepalive_failed(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t_enum_cr_secure_touch_mode_feature_keepalive_error_t__void;)V
    .locals 4

    .line 66
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCPtr:J

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t_enum_cr_secure_touch_mode_feature_keepalive_error_t__void;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t_enum_cr_secure_touch_mode_feature_keepalive_error_t__void;)J

    move-result-wide v2

    invoke-static {v0, v1, p0, v2, v3}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_event_api_t_on_keepalive_failed_set(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;J)V

    return-void
.end method

.method public setOn_pin_pad_center_point(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t_p_struct_crs_stm_pin_pad_center_point_t__void;)V
    .locals 4

    .line 93
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCPtr:J

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t_p_struct_crs_stm_pin_pad_center_point_t__void;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t_p_struct_crs_stm_pin_pad_center_point_t__void;)J

    move-result-wide v2

    invoke-static {v0, v1, p0, v2, v3}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_event_api_t_on_pin_pad_center_point_set(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;J)V

    return-void
.end method

.method public setOn_pin_pad_event(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t_enum_crs_stm_pin_pad_event_id_t__void;)V
    .locals 4

    .line 75
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCPtr:J

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t_enum_crs_stm_pin_pad_event_id_t__void;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_struct_cr_secure_touch_mode_feature_t_enum_crs_stm_pin_pad_event_id_t__void;)J

    move-result-wide v2

    invoke-static {v0, v1, p0, v2, v3}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_event_api_t_on_pin_pad_event_set(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;J)V

    return-void
.end method

.method public setShow_pin_pad_request(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_q_const__cr_card_info_t_enum_cr_secure_touch_mode_feature_pin_try_t_p_struct_cr_secure_touch_mode_feature_t__void;)V
    .locals 4

    .line 57
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;->swigCPtr:J

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_q_const__cr_card_info_t_enum_cr_secure_touch_mode_feature_pin_try_t_p_struct_cr_secure_touch_mode_feature_t__void;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_f_p_void_p_q_const__cr_card_info_t_enum_cr_secure_touch_mode_feature_pin_try_t_p_struct_cr_secure_touch_mode_feature_t__void;)J

    move-result-wide v2

    invoke-static {v0, v1, p0, v2, v3}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->cr_secure_touch_mode_feature_event_api_t_show_pin_pad_request_set(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;J)V

    return-void
.end method
