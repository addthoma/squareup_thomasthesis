.class public Lcom/squareup/cardreader/lcr/TamperFeatureNativeJNI;
.super Ljava/lang/Object;
.source "TamperFeatureNativeJNI.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final native cr_tamper_free(J)I
.end method

.method public static final native cr_tamper_get_tamper_data(J)I
.end method

.method public static final native cr_tamper_get_tamper_status(J)I
.end method

.method public static final native cr_tamper_init(JJJ)I
.end method

.method public static final native cr_tamper_reset_tag(J)I
.end method

.method public static final native cr_tamper_term(J)I
.end method

.method public static final native tamper_initialize(JLjava/lang/Object;)J
.end method
