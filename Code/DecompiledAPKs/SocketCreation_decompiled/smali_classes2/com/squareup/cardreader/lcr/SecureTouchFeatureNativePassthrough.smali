.class public Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativePassthrough;
.super Ljava/lang/Object;
.source "SecureTouchFeatureNativePassthrough.java"

# interfaces
.implements Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cr_secure_touch_mode_feature_alloc()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;
    .locals 1

    .line 54
    invoke-static {}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNative;->cr_secure_touch_mode_feature_alloc()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

    move-result-object v0

    return-object v0
.end method

.method public cr_secure_touch_mode_feature_disable_squid_touch_driver_result(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;)V
    .locals 0

    .line 11
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNative;->cr_secure_touch_mode_feature_disable_squid_touch_driver_result(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;)V

    return-void
.end method

.method public cr_secure_touch_mode_feature_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureTouchResult;
    .locals 0

    .line 60
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNative;->cr_secure_touch_mode_feature_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_secure_touch_mode_feature_init(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)Lcom/squareup/cardreader/lcr/CrSecureTouchResult;
    .locals 0

    .line 67
    invoke-static {p1, p2, p3}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNative;->cr_secure_touch_mode_feature_init(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_secure_touch_mode_feature_regular_set_button_location(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)V
    .locals 0

    .line 17
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNative;->cr_secure_touch_mode_feature_regular_set_button_location(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)V

    return-void
.end method

.method public cr_secure_touch_mode_feature_sent_pinpad_configs(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmPinPadConfigType;)V
    .locals 0

    .line 24
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNative;->cr_secure_touch_mode_feature_sent_pinpad_configs(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmPinPadConfigType;)V

    return-void
.end method

.method public cr_secure_touch_mode_feature_set_accessibility_configs(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;)V
    .locals 0

    .line 43
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNative;->cr_secure_touch_mode_feature_set_accessibility_configs(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;)V

    return-void
.end method

.method public cr_secure_touch_mode_feature_start_secure_touch(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)V
    .locals 0

    .line 30
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNative;->cr_secure_touch_mode_feature_start_secure_touch(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)V

    return-void
.end method

.method public cr_secure_touch_mode_feature_stop_secure_touch(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)V
    .locals 0

    .line 36
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNative;->cr_secure_touch_mode_feature_stop_secure_touch(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)V

    return-void
.end method

.method public cr_secure_touch_mode_feature_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureTouchResult;
    .locals 0

    .line 73
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNative;->cr_secure_touch_mode_feature_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_secure_touch_mode_pin_pad_is_hidden(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmHidePinPadResult;)V
    .locals 0

    .line 49
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNative;->cr_secure_touch_mode_pin_pad_is_hidden(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmHidePinPadResult;)V

    return-void
.end method

.method public secure_touch_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;
    .locals 0

    .line 79
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNative;->secure_touch_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

    move-result-object p1

    return-object p1
.end method
