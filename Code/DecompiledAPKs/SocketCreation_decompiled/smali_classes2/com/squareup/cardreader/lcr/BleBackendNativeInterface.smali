.class public interface abstract Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;
.super Ljava/lang/Object;
.source "BleBackendNativeInterface.java"


# virtual methods
.method public abstract ble_received_data_from_characteristic_ack_vector(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;I)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;
.end method

.method public abstract ble_received_data_from_characteristic_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;[B)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;
.end method

.method public abstract ble_received_data_from_characteristic_mtu(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;I)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;
.end method

.method public abstract cr_comms_backend_ble_alloc()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;
.end method

.method public abstract cr_comms_backend_ble_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;
.end method

.method public abstract cr_comms_backend_ble_shutdown(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;)V
.end method

.method public abstract initialize_backend_ble(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;[BLjava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;
.end method
