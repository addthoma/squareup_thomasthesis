.class public Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativePassthrough;
.super Ljava/lang/Object;
.source "FirmwareUpdateFeatureNativePassthrough.java"

# interfaces
.implements Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cr_firmware_update_feature_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;
    .locals 0

    .line 16
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNative;->cr_firmware_update_feature_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_firmware_update_feature_get_manifest(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;
    .locals 0

    .line 22
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNative;->cr_firmware_update_feature_get_manifest(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_firmware_update_feature_stop_sending_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;
    .locals 0

    .line 28
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNative;->cr_firmware_update_feature_stop_sending_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_firmware_update_feature_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;
    .locals 0

    .line 10
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNative;->cr_firmware_update_feature_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    move-result-object p1

    return-object p1
.end method

.method public firmware_send_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;[B[B[B)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;
    .locals 0

    .line 41
    invoke-static {p1, p2, p3, p4}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNative;->firmware_send_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;[B[B[B)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    move-result-object p1

    return-object p1
.end method

.method public firmware_update_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;
    .locals 0

    .line 34
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNative;->firmware_update_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

    move-result-object p1

    return-object p1
.end method
