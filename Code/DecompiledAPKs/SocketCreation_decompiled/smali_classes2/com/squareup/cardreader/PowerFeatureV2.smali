.class public final Lcom/squareup/cardreader/PowerFeatureV2;
.super Ljava/lang/Object;
.source "PowerFeatureV2.kt"

# interfaces
.implements Lcom/squareup/cardreader/CanReset;
.implements Lcom/squareup/cardreader/PowerFeature;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u00012\u00020\u0002B\u001b\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u000e\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eJ\u0008\u0010\u000f\u001a\u00020\u000cH\u0002J8\u0010\u0010\u001a\u00020\u000c2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0012H\u0016J\u0010\u0010\u0019\u001a\n \u001b*\u0004\u0018\u00010\u001a0\u001aH\u0002J\u0010\u0010\u001c\u001a\n \u001b*\u0004\u0018\u00010\u001a0\u001aH\u0002J\u0008\u0010\u001d\u001a\u00020\u000cH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/cardreader/PowerFeatureV2;",
        "Lcom/squareup/cardreader/CanReset;",
        "Lcom/squareup/cardreader/PowerFeature;",
        "posSender",
        "Lcom/squareup/cardreader/SendsToPos;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput;",
        "cardreaderProvider",
        "Lcom/squareup/cardreader/CardreaderPointerProvider;",
        "(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/CardreaderPointerProvider;)V",
        "featurePointer",
        "Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;",
        "handleMessage",
        "",
        "message",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PowerFeatureMessage;",
        "initialize",
        "onPowerStatus",
        "percentage",
        "",
        "current",
        "voltage",
        "temperature",
        "isCritical",
        "",
        "batteryStatus",
        "powerOff",
        "Lcom/squareup/cardreader/lcr/CrPowerResult;",
        "kotlin.jvm.PlatformType",
        "requestPowerStatus",
        "resetIfInitilized",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

.field private featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;

.field private final posSender:Lcom/squareup/cardreader/SendsToPos;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/CardreaderPointerProvider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput;",
            ">;",
            "Lcom/squareup/cardreader/CardreaderPointerProvider;",
            ")V"
        }
    .end annotation

    const-string v0, "posSender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardreaderProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/PowerFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    iput-object p2, p0, Lcom/squareup/cardreader/PowerFeatureV2;->cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

    return-void
.end method

.method public static final synthetic access$getFeaturePointer$p(Lcom/squareup/cardreader/PowerFeatureV2;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;
    .locals 1

    .line 22
    iget-object p0, p0, Lcom/squareup/cardreader/PowerFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;

    if-nez p0, :cond_0

    const-string v0, "featurePointer"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setFeaturePointer$p(Lcom/squareup/cardreader/PowerFeatureV2;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;)V
    .locals 0

    .line 22
    iput-object p1, p0, Lcom/squareup/cardreader/PowerFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;

    return-void
.end method

.method private final initialize()V
    .locals 2

    .line 38
    iget-object v0, p0, Lcom/squareup/cardreader/PowerFeatureV2;->cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardreaderPointerProvider;->cardreaderPointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/squareup/cardreader/lcr/PowerFeatureNative;->power_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;

    move-result-object v0

    const-string v1, "PowerFeatureNative.power\u2026ardreaderPointer(), this)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/cardreader/PowerFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;

    return-void
.end method

.method private final powerOff()Lcom/squareup/cardreader/lcr/CrPowerResult;
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/cardreader/PowerFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/PowerFeatureNative;->cr_power_off(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;)Lcom/squareup/cardreader/lcr/CrPowerResult;

    move-result-object v0

    return-object v0
.end method

.method private final requestPowerStatus()Lcom/squareup/cardreader/lcr/CrPowerResult;
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/cardreader/PowerFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/PowerFeatureNative;->cr_power_get_battery_voltage(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;)Lcom/squareup/cardreader/lcr/CrPowerResult;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final handleMessage(Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PowerFeatureMessage;)V
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PowerFeatureMessage$InitializePowerFeature;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/cardreader/PowerFeatureV2;->initialize()V

    goto :goto_0

    .line 31
    :cond_0
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PowerFeatureMessage$RequestPowerStatus;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/squareup/cardreader/PowerFeatureV2;->requestPowerStatus()Lcom/squareup/cardreader/lcr/CrPowerResult;

    goto :goto_0

    .line 32
    :cond_1
    instance-of p1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PowerFeatureMessage$PowerOff;

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/squareup/cardreader/PowerFeatureV2;->powerOff()Lcom/squareup/cardreader/lcr/CrPowerResult;

    :cond_2
    :goto_0
    return-void
.end method

.method public onPowerStatus(IIIIZI)V
    .locals 8

    .line 72
    invoke-static {p6}, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    move-result-object p6

    const-string v0, "CrsBatteryMode.swigToEnum(batteryStatus)"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p6}, Lcom/squareup/cardreader/PowerFeatureV2Kt;->access$toBatteryMode(Lcom/squareup/cardreader/lcr/CrsBatteryMode;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;

    move-result-object v7

    .line 73
    iget-object p6, p0, Lcom/squareup/cardreader/PowerFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    .line 74
    new-instance v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;

    move-object v1, v0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;-><init>(IIIIZLcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;)V

    check-cast v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    .line 73
    invoke-interface {p6, v0}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public resetIfInitilized()V
    .locals 2

    .line 42
    move-object v0, p0

    check-cast v0, Lcom/squareup/cardreader/PowerFeatureV2;

    iget-object v0, v0, Lcom/squareup/cardreader/PowerFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;

    if-eqz v0, :cond_2

    .line 43
    iget-object v0, p0, Lcom/squareup/cardreader/PowerFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;

    const-string v1, "featurePointer"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/PowerFeatureNative;->cr_power_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;)Lcom/squareup/cardreader/lcr/CrPowerResult;

    .line 44
    iget-object v0, p0, Lcom/squareup/cardreader/PowerFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/PowerFeatureNative;->cr_power_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;)Lcom/squareup/cardreader/lcr/CrPowerResult;

    :cond_2
    return-void
.end method
