.class public final Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public card_reader_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6964
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;
    .locals 3

    .line 6974
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$Builder;->card_reader_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;-><init>(Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 6961
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    move-result-object v0

    return-object v0
.end method

.method public card_reader_info(Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;)Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$Builder;
    .locals 0

    .line 6968
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$Builder;->card_reader_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;

    return-object p0
.end method
