.class final Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad$ProtoAdapter_OnLibrariesFailedToLoad;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_OnLibrariesFailedToLoad"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 7121
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7136
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad$Builder;-><init>()V

    .line 7137
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 7138
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 7141
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 7145
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 7146
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7119
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad$ProtoAdapter_OnLibrariesFailedToLoad;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7131
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7119
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad$ProtoAdapter_OnLibrariesFailedToLoad;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;)I
    .locals 0

    .line 7126
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    return p1
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 7119
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad$ProtoAdapter_OnLibrariesFailedToLoad;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;)Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;
    .locals 0

    .line 7151
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad$Builder;

    move-result-object p1

    .line 7152
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 7153
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 7119
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad$ProtoAdapter_OnLibrariesFailedToLoad;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;)Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    move-result-object p1

    return-object p1
.end method
