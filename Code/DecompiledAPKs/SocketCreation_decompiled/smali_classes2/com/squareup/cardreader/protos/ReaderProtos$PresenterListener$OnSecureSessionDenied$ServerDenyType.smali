.class public final enum Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;
.super Ljava/lang/Enum;
.source "ReaderProtos.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ServerDenyType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType$ProtoAdapter_ServerDenyType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum APP_DAMAGED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

.field public static final enum APP_TOO_OLD:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

.field public static final enum DEVICE_FIRMWARE_DAMAGED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

.field public static final enum DEVICE_FIRMWARE_MODIFIED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

.field public static final enum DEVICE_FIRMWARE_NOT_SUPPORTED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

.field public static final enum DEVICE_FIRMWARE_TOO_OLD:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

.field public static final enum DEVICE_HARDWARE_NOT_SUPPORTED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

.field public static final enum DEVICE_IN_ACCESSIBILITY_MODE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

.field public static final enum DEVICE_IN_DEVELOPER_MODE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

.field public static final enum GENERIC_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

.field public static final enum INVALID_REQUEST:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

.field public static final enum NONE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

.field public static final enum NOT_DENIED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

.field public static final enum PARSE_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

.field public static final enum SELLER_NOT_ACTIVATED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

.field public static final enum SELLER_NOT_ELIGIBLE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

.field public static final enum SELLER_SUSPENDED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

.field public static final enum UNKNOWN_DENY_REASON:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 5109
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const/4 v1, 0x0

    const-string v2, "GENERIC_ERROR"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->GENERIC_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    .line 5111
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const/4 v2, 0x1

    const-string v3, "PARSE_ERROR"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->PARSE_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    .line 5113
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const/4 v3, 0x2

    const-string v4, "UNKNOWN_DENY_REASON"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->UNKNOWN_DENY_REASON:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    .line 5115
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const/4 v4, 0x3

    const-string v5, "NOT_DENIED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->NOT_DENIED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    .line 5117
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const/4 v5, 0x4

    const-string v6, "SELLER_SUSPENDED"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->SELLER_SUSPENDED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    .line 5119
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const/4 v6, 0x5

    const-string v7, "SELLER_NOT_ELIGIBLE"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->SELLER_NOT_ELIGIBLE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    .line 5121
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const/4 v7, 0x6

    const-string v8, "DEVICE_HARDWARE_NOT_SUPPORTED"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->DEVICE_HARDWARE_NOT_SUPPORTED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    .line 5123
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const/4 v8, 0x7

    const-string v9, "DEVICE_FIRMWARE_TOO_OLD"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->DEVICE_FIRMWARE_TOO_OLD:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    .line 5125
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const/16 v9, 0x8

    const-string v10, "DEVICE_FIRMWARE_NOT_SUPPORTED"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->DEVICE_FIRMWARE_NOT_SUPPORTED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    .line 5127
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const/16 v10, 0x9

    const-string v11, "DEVICE_FIRMWARE_MODIFIED"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->DEVICE_FIRMWARE_MODIFIED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    .line 5129
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const/16 v11, 0xa

    const-string v12, "DEVICE_FIRMWARE_DAMAGED"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->DEVICE_FIRMWARE_DAMAGED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    .line 5131
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const/16 v12, 0xb

    const-string v13, "DEVICE_IN_DEVELOPER_MODE"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->DEVICE_IN_DEVELOPER_MODE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    .line 5133
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const/16 v13, 0xc

    const-string v14, "APP_TOO_OLD"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->APP_TOO_OLD:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    .line 5135
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const/16 v14, 0xd

    const-string v15, "APP_DAMAGED"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->APP_DAMAGED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    .line 5137
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const/16 v15, 0xe

    const-string v14, "INVALID_REQUEST"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->INVALID_REQUEST:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    .line 5139
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const-string v14, "DEVICE_IN_ACCESSIBILITY_MODE"

    const/16 v15, 0xf

    const/16 v13, 0xf

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->DEVICE_IN_ACCESSIBILITY_MODE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    .line 5141
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const-string v13, "SELLER_NOT_ACTIVATED"

    const/16 v14, 0x10

    const/16 v15, 0x10

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->SELLER_NOT_ACTIVATED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    .line 5143
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const-string v13, "NONE"

    const/16 v14, 0x11

    const/16 v15, 0x11

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->NONE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const/16 v0, 0x12

    new-array v0, v0, [Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    .line 5108
    sget-object v13, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->GENERIC_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->PARSE_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->UNKNOWN_DENY_REASON:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->NOT_DENIED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->SELLER_SUSPENDED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->SELLER_NOT_ELIGIBLE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->DEVICE_HARDWARE_NOT_SUPPORTED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->DEVICE_FIRMWARE_TOO_OLD:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->DEVICE_FIRMWARE_NOT_SUPPORTED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->DEVICE_FIRMWARE_MODIFIED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->DEVICE_FIRMWARE_DAMAGED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->DEVICE_IN_DEVELOPER_MODE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->APP_TOO_OLD:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->APP_DAMAGED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->INVALID_REQUEST:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->DEVICE_IN_ACCESSIBILITY_MODE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->SELLER_NOT_ACTIVATED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->NONE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->$VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    .line 5145
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType$ProtoAdapter_ServerDenyType;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType$ProtoAdapter_ServerDenyType;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 5149
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 5150
    iput p3, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 5175
    :pswitch_0
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->NONE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-object p0

    .line 5174
    :pswitch_1
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->SELLER_NOT_ACTIVATED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-object p0

    .line 5173
    :pswitch_2
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->DEVICE_IN_ACCESSIBILITY_MODE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-object p0

    .line 5172
    :pswitch_3
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->INVALID_REQUEST:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-object p0

    .line 5171
    :pswitch_4
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->APP_DAMAGED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-object p0

    .line 5170
    :pswitch_5
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->APP_TOO_OLD:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-object p0

    .line 5169
    :pswitch_6
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->DEVICE_IN_DEVELOPER_MODE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-object p0

    .line 5168
    :pswitch_7
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->DEVICE_FIRMWARE_DAMAGED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-object p0

    .line 5167
    :pswitch_8
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->DEVICE_FIRMWARE_MODIFIED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-object p0

    .line 5166
    :pswitch_9
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->DEVICE_FIRMWARE_NOT_SUPPORTED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-object p0

    .line 5165
    :pswitch_a
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->DEVICE_FIRMWARE_TOO_OLD:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-object p0

    .line 5164
    :pswitch_b
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->DEVICE_HARDWARE_NOT_SUPPORTED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-object p0

    .line 5163
    :pswitch_c
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->SELLER_NOT_ELIGIBLE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-object p0

    .line 5162
    :pswitch_d
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->SELLER_SUSPENDED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-object p0

    .line 5161
    :pswitch_e
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->NOT_DENIED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-object p0

    .line 5160
    :pswitch_f
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->UNKNOWN_DENY_REASON:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-object p0

    .line 5159
    :pswitch_10
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->PARSE_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-object p0

    .line 5158
    :pswitch_11
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->GENERIC_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;
    .locals 1

    .line 5108
    const-class v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;
    .locals 1

    .line 5108
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->$VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 5182
    iget v0, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->value:I

    return v0
.end method
