.class public final Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;
.super Lcom/squareup/wire/Message;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Reset"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset$ProtoAdapter_Reset;,
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 292
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset$ProtoAdapter_Reset;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset$ProtoAdapter_Reset;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 297
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;-><init>(Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;)V
    .locals 1

    .line 301
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p1}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p1, p0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 314
    :cond_0
    instance-of v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 315
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    .line 316
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 321
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    return v0
.end method

.method public newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset$Builder;
    .locals 2

    .line 306
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset$Builder;-><init>()V

    .line 307
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 291
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 326
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Reset{"

    .line 327
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
