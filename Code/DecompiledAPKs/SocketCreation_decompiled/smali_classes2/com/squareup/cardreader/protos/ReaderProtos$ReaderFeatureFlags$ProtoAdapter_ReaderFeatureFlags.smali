.class final Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$ProtoAdapter_ReaderFeatureFlags;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ReaderFeatureFlags"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 8602
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8631
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;-><init>()V

    .line 8632
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 8633
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 8643
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 8641
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->felica_notification(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;

    goto :goto_0

    .line 8640
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->sonic_branding(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;

    goto :goto_0

    .line 8639
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->common_debit_support(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;

    goto :goto_0

    .line 8638
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->spoc_prng_seed(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;

    goto :goto_0

    .line 8637
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->account_type_selection(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;

    goto :goto_0

    .line 8636
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->pinblock_format_v2(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;

    goto :goto_0

    .line 8635
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->quickchip_fw209030(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;

    goto :goto_0

    .line 8647
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 8648
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8600
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$ProtoAdapter_ReaderFeatureFlags;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8619
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->quickchip_fw209030:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8620
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->pinblock_format_v2:Ljava/lang/Boolean;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8621
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->account_type_selection:Ljava/lang/Boolean;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8622
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->spoc_prng_seed:Ljava/lang/Boolean;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8623
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->common_debit_support:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8624
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->sonic_branding:Ljava/lang/Boolean;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8625
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->felica_notification:Ljava/lang/Boolean;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8626
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8600
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$ProtoAdapter_ReaderFeatureFlags;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)I
    .locals 4

    .line 8607
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->quickchip_fw209030:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->pinblock_format_v2:Ljava/lang/Boolean;

    const/4 v3, 0x2

    .line 8608
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->account_type_selection:Ljava/lang/Boolean;

    const/4 v3, 0x3

    .line 8609
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->spoc_prng_seed:Ljava/lang/Boolean;

    const/4 v3, 0x4

    .line 8610
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->common_debit_support:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 8611
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->sonic_branding:Ljava/lang/Boolean;

    const/4 v3, 0x6

    .line 8612
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->felica_notification:Ljava/lang/Boolean;

    const/4 v3, 0x7

    .line 8613
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8614
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 8600
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$ProtoAdapter_ReaderFeatureFlags;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;
    .locals 0

    .line 8653
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;

    move-result-object p1

    .line 8654
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 8655
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 8600
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$ProtoAdapter_ReaderFeatureFlags;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    move-result-object p1

    return-object p1
.end method
