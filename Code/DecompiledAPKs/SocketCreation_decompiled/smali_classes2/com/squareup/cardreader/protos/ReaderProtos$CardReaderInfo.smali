.class public final Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;
.super Lcom/squareup/wire/Message;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CardReaderInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ProtoAdapter_CardReaderInfo;,
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;,
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ConnectionType;,
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;,
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;,
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ADDRESS:Ljava/lang/String; = ""

.field public static final DEFAULT_CARD_PRESENCE:Ljava/lang/Boolean;

.field public static final DEFAULT_CHARGE_CYCLE_COUNT:Ljava/lang/Integer;

.field public static final DEFAULT_COMMS_STATUS:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

.field public static final DEFAULT_CONNECTED:Ljava/lang/Boolean;

.field public static final DEFAULT_CONNECTION_TYPE:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ConnectionType;

.field public static final DEFAULT_FIRMWARE_UPDATE_IN_PROGRESS:Ljava/lang/Boolean;

.field public static final DEFAULT_FIRMWARE_VERSION:Ljava/lang/String; = ""

.field public static final DEFAULT_HARDWARE_SERIAL_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_ID:Ljava/lang/Integer;

.field public static final DEFAULT_READER_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_READER_REQUIRES_FIRMWARE_UPDATE:Ljava/lang/Boolean;

.field public static final DEFAULT_READER_TYPE:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

.field public static final DEFAULT_SECURE_SESSION:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

.field public static final DEFAULT_SERVER_REQUIRES_FIRMWARE_UPDATE:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final address:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x10
    .end annotation
.end field

.field public final battery_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReaderBatteryInfo#ADAPTER"
        tag = 0x15
    .end annotation
.end field

.field public final card_presence:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final charge_cycle_count:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x7
    .end annotation
.end field

.field public final comms_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReaderInfo$CommsVersionResult#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final connected:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x13
    .end annotation
.end field

.field public final connection_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ConnectionType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReaderInfo$ConnectionType#ADAPTER"
        tag = 0xf
    .end annotation
.end field

.field public final firmware_update_in_progress:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field

.field public final firmware_version:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final hardware_serial_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final id:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xe
    .end annotation
.end field

.field public final reader_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x14
    .end annotation
.end field

.field public final reader_requires_firmware_update:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final reader_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReaderInfo$ReaderType#ADAPTER"
        tag = 0x11
    .end annotation
.end field

.field public final secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReaderInfo$SecureSessionState#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final server_requires_firmware_update:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 6213
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ProtoAdapter_CardReaderInfo;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ProtoAdapter_CardReaderInfo;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 6217
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->DEFAULT_ID:Ljava/lang/Integer;

    .line 6219
    sget-object v2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ConnectionType;->AUDIO:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ConnectionType;

    sput-object v2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->DEFAULT_CONNECTION_TYPE:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ConnectionType;

    .line 6225
    sget-object v2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->UNKNOWN:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    sput-object v2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->DEFAULT_READER_TYPE:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    .line 6227
    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->DEFAULT_CONNECTED:Ljava/lang/Boolean;

    .line 6229
    sget-object v2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;->OK:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    sput-object v2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->DEFAULT_COMMS_STATUS:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    .line 6231
    sget-object v2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;->INVALID:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    sput-object v2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->DEFAULT_SECURE_SESSION:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    .line 6233
    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->DEFAULT_CARD_PRESENCE:Ljava/lang/Boolean;

    .line 6239
    sput-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->DEFAULT_CHARGE_CYCLE_COUNT:Ljava/lang/Integer;

    .line 6241
    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->DEFAULT_FIRMWARE_UPDATE_IN_PROGRESS:Ljava/lang/Boolean;

    .line 6243
    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->DEFAULT_READER_REQUIRES_FIRMWARE_UPDATE:Ljava/lang/Boolean;

    .line 6245
    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->DEFAULT_SERVER_REQUIRES_FIRMWARE_UPDATE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;Lokio/ByteString;)V
    .locals 1

    .line 6344
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 6345
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->id:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->id:Ljava/lang/Integer;

    .line 6346
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->connection_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ConnectionType;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->connection_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ConnectionType;

    .line 6347
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->address:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->address:Ljava/lang/String;

    .line 6348
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->reader_name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_name:Ljava/lang/String;

    .line 6349
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->reader_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    .line 6350
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->connected:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->connected:Ljava/lang/Boolean;

    .line 6351
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->comms_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->comms_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    .line 6352
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    .line 6353
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->card_presence:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->card_presence:Ljava/lang/Boolean;

    .line 6354
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->firmware_version:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->firmware_version:Ljava/lang/String;

    .line 6355
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->hardware_serial_number:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->hardware_serial_number:Ljava/lang/String;

    .line 6356
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->charge_cycle_count:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->charge_cycle_count:Ljava/lang/Integer;

    .line 6357
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->battery_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->battery_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    .line 6358
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->firmware_update_in_progress:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->firmware_update_in_progress:Ljava/lang/Boolean;

    .line 6359
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->reader_requires_firmware_update:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_requires_firmware_update:Ljava/lang/Boolean;

    .line 6360
    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->server_requires_firmware_update:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->server_requires_firmware_update:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 6389
    :cond_0
    instance-of v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 6390
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;

    .line 6391
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->id:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->id:Ljava/lang/Integer;

    .line 6392
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->connection_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ConnectionType;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->connection_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ConnectionType;

    .line 6393
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->address:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->address:Ljava/lang/String;

    .line 6394
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_name:Ljava/lang/String;

    .line 6395
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    .line 6396
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->connected:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->connected:Ljava/lang/Boolean;

    .line 6397
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->comms_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->comms_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    .line 6398
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    .line 6399
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->card_presence:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->card_presence:Ljava/lang/Boolean;

    .line 6400
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->firmware_version:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->firmware_version:Ljava/lang/String;

    .line 6401
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->hardware_serial_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->hardware_serial_number:Ljava/lang/String;

    .line 6402
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->charge_cycle_count:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->charge_cycle_count:Ljava/lang/Integer;

    .line 6403
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->battery_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->battery_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    .line 6404
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->firmware_update_in_progress:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->firmware_update_in_progress:Ljava/lang/Boolean;

    .line 6405
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_requires_firmware_update:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_requires_firmware_update:Ljava/lang/Boolean;

    .line 6406
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->server_requires_firmware_update:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->server_requires_firmware_update:Ljava/lang/Boolean;

    .line 6407
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 6412
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_10

    .line 6414
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 6415
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->id:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6416
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->connection_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ConnectionType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ConnectionType;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6417
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->address:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6418
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_name:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6419
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6420
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->connected:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6421
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->comms_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6422
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6423
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->card_presence:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6424
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->firmware_version:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6425
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->hardware_serial_number:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6426
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->charge_cycle_count:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6427
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->battery_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6428
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->firmware_update_in_progress:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6429
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_requires_firmware_update:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6430
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->server_requires_firmware_update:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_f
    add-int/2addr v0, v2

    .line 6431
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_10
    return v0
.end method

.method public newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;
    .locals 2

    .line 6365
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;-><init>()V

    .line 6366
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->id:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->id:Ljava/lang/Integer;

    .line 6367
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->connection_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ConnectionType;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->connection_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ConnectionType;

    .line 6368
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->address:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->address:Ljava/lang/String;

    .line 6369
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->reader_name:Ljava/lang/String;

    .line 6370
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->reader_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    .line 6371
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->connected:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->connected:Ljava/lang/Boolean;

    .line 6372
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->comms_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->comms_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    .line 6373
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    .line 6374
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->card_presence:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->card_presence:Ljava/lang/Boolean;

    .line 6375
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->firmware_version:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->firmware_version:Ljava/lang/String;

    .line 6376
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->hardware_serial_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->hardware_serial_number:Ljava/lang/String;

    .line 6377
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->charge_cycle_count:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->charge_cycle_count:Ljava/lang/Integer;

    .line 6378
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->battery_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->battery_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    .line 6379
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->firmware_update_in_progress:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->firmware_update_in_progress:Ljava/lang/Boolean;

    .line 6380
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_requires_firmware_update:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->reader_requires_firmware_update:Ljava/lang/Boolean;

    .line 6381
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->server_requires_firmware_update:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->server_requires_firmware_update:Ljava/lang/Boolean;

    .line 6382
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 6212
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 6438
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 6439
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->id:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->id:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6440
    :cond_0
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->connection_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ConnectionType;

    if-eqz v1, :cond_1

    const-string v1, ", connection_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->connection_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ConnectionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6441
    :cond_1
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->address:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", address="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->address:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6442
    :cond_2
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_name:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", reader_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6443
    :cond_3
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    if-eqz v1, :cond_4

    const-string v1, ", reader_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6444
    :cond_4
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->connected:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", connected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->connected:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6445
    :cond_5
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->comms_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    if-eqz v1, :cond_6

    const-string v1, ", comms_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->comms_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6446
    :cond_6
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    if-eqz v1, :cond_7

    const-string v1, ", secure_session="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6447
    :cond_7
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->card_presence:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", card_presence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->card_presence:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6448
    :cond_8
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->firmware_version:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", firmware_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->firmware_version:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6449
    :cond_9
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->hardware_serial_number:Ljava/lang/String;

    if-eqz v1, :cond_a

    const-string v1, ", hardware_serial_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->hardware_serial_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6450
    :cond_a
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->charge_cycle_count:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    const-string v1, ", charge_cycle_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->charge_cycle_count:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6451
    :cond_b
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->battery_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    if-eqz v1, :cond_c

    const-string v1, ", battery_info="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->battery_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6452
    :cond_c
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->firmware_update_in_progress:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    const-string v1, ", firmware_update_in_progress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->firmware_update_in_progress:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6453
    :cond_d
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_requires_firmware_update:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    const-string v1, ", reader_requires_firmware_update="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_requires_firmware_update:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6454
    :cond_e
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->server_requires_firmware_update:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    const-string v1, ", server_requires_firmware_update="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->server_requires_firmware_update:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_f
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CardReaderInfo{"

    .line 6455
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
