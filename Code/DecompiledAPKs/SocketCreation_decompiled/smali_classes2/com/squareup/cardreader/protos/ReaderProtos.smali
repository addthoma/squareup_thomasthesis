.class public final Lcom/squareup/cardreader/protos/ReaderProtos;
.super Lcom/squareup/wire/Message;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$ProtoAdapter_ReaderProtos;,
        Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;,
        Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;,
        Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;,
        Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener;,
        Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;,
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;,
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;,
        Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;,
        Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener;,
        Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;,
        Lcom/squareup/cardreader/protos/ReaderProtos$Asset;,
        Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;,
        Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;,
        Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;,
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReader;,
        Lcom/squareup/cardreader/protos/ReaderProtos$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/cardreader/protos/ReaderProtos;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProtoAdapter_ReaderProtos;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$ProtoAdapter_ReaderProtos;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 35
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, v0}, Lcom/squareup/cardreader/protos/ReaderProtos;-><init>(Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;)V
    .locals 1

    .line 39
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p1}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p1, p0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 52
    :cond_0
    instance-of v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 53
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos;

    .line 54
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 59
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    return v0
.end method

.method public newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$Builder;
    .locals 2

    .line 44
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$Builder;-><init>()V

    .line 45
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ReaderProtos{"

    .line 65
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
