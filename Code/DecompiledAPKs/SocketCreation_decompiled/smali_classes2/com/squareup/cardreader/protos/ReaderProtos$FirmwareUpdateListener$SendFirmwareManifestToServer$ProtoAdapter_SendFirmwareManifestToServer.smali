.class final Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$ProtoAdapter_SendFirmwareManifestToServer;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SendFirmwareManifestToServer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 2888
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2908
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;-><init>()V

    .line 2909
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 2910
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 2915
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 2913
    :cond_0
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;->libcardreader_comms_version(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;

    goto :goto_0

    .line 2912
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;->manifest(Lokio/ByteString;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;

    goto :goto_0

    .line 2919
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 2920
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2886
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$ProtoAdapter_SendFirmwareManifestToServer;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2901
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;->manifest:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2902
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;->libcardreader_comms_version:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2903
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2886
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$ProtoAdapter_SendFirmwareManifestToServer;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;)I
    .locals 4

    .line 2893
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;->manifest:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;->libcardreader_comms_version:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;

    const/4 v3, 0x2

    .line 2894
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2895
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 2886
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$ProtoAdapter_SendFirmwareManifestToServer;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;
    .locals 2

    .line 2925
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;

    move-result-object p1

    .line 2926
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;->libcardreader_comms_version:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;->libcardreader_comms_version:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;->libcardreader_comms_version:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;

    .line 2927
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 2928
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2886
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$ProtoAdapter_SendFirmwareManifestToServer;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    move-result-object p1

    return-object p1
.end method
