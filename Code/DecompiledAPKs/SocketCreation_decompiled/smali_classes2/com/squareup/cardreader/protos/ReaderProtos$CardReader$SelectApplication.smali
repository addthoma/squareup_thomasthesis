.class public final Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;
.super Lcom/squareup/wire/Message;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SelectApplication"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$ProtoAdapter_SelectApplication;,
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final emv_application:Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$EmvApplication#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1539
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$ProtoAdapter_SelectApplication;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$ProtoAdapter_SelectApplication;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;)V
    .locals 1

    .line 1550
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;-><init>(Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;Lokio/ByteString;)V
    .locals 1

    .line 1554
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1555
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->emv_application:Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1569
    :cond_0
    instance-of v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1570
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    .line 1571
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->emv_application:Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->emv_application:Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    .line 1572
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 1577
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 1579
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1580
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->emv_application:Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 1581
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;
    .locals 2

    .line 1560
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;-><init>()V

    .line 1561
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->emv_application:Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;->emv_application:Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    .line 1562
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1538
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1588
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1589
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->emv_application:Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    if-eqz v1, :cond_0

    const-string v1, ", emv_application="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->emv_application:Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SelectApplication{"

    .line 1590
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
