.class public final Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public data_data:Lokio/ByteString;

.field public key_data:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4510
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;
    .locals 4

    .line 4525
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump$Builder;->key_data:Lokio/ByteString;

    iget-object v2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump$Builder;->data_data:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;-><init>(Lokio/ByteString;Lokio/ByteString;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 4505
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;

    move-result-object v0

    return-object v0
.end method

.method public data_data(Lokio/ByteString;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump$Builder;
    .locals 0

    .line 4519
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump$Builder;->data_data:Lokio/ByteString;

    return-object p0
.end method

.method public key_data(Lokio/ByteString;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump$Builder;
    .locals 0

    .line 4514
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump$Builder;->key_data:Lokio/ByteString;

    return-object p0
.end method
