.class final Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$ProtoAdapter_SelectApplication;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SelectApplication"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1612
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1629
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;-><init>()V

    .line 1630
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1631
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 1635
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1633
    :cond_0
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;->emv_application(Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;

    goto :goto_0

    .line 1639
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1640
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1610
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$ProtoAdapter_SelectApplication;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1623
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->emv_application:Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1624
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1610
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$ProtoAdapter_SelectApplication;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;)I
    .locals 3

    .line 1617
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->emv_application:Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 1618
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1610
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$ProtoAdapter_SelectApplication;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;
    .locals 2

    .line 1645
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;

    move-result-object p1

    .line 1646
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;->emv_application:Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;->emv_application:Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;->emv_application:Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    .line 1647
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1648
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1610
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$ProtoAdapter_SelectApplication;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    move-result-object p1

    return-object p1
.end method
