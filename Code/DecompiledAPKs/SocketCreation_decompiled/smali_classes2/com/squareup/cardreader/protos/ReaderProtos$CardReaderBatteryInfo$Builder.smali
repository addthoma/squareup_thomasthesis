.class public final Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public battery_mode:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

.field public current:Ljava/lang/Integer;

.field public is_critical:Ljava/lang/Boolean;

.field public percentage:Ljava/lang/Integer;

.field public temperature:Ljava/lang/Integer;

.field public voltage:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6109
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public battery_mode(Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;
    .locals 0

    .line 6138
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->battery_mode:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    return-object p0
.end method

.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;
    .locals 9

    .line 6144
    new-instance v8, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->percentage:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->current:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->voltage:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->temperature:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->is_critical:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->battery_mode:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 6096
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    move-result-object v0

    return-object v0
.end method

.method public current(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;
    .locals 0

    .line 6118
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->current:Ljava/lang/Integer;

    return-object p0
.end method

.method public is_critical(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;
    .locals 0

    .line 6133
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->is_critical:Ljava/lang/Boolean;

    return-object p0
.end method

.method public percentage(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;
    .locals 0

    .line 6113
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->percentage:Ljava/lang/Integer;

    return-object p0
.end method

.method public temperature(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;
    .locals 0

    .line 6128
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->temperature:Ljava/lang/Integer;

    return-object p0
.end method

.method public voltage(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;
    .locals 0

    .line 6123
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->voltage:Ljava/lang/Integer;

    return-object p0
.end method
