.class public final Lcom/squareup/cardreader/PaymentFeatureMessageWrapper;
.super Ljava/lang/Object;
.source "PaymentFeatureDelegateSender.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/cardreader/PaymentFeatureMessageWrapper;",
        "",
        "cardReaderId",
        "Lcom/squareup/cardreader/CardReaderId;",
        "message",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;",
        "(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;)V",
        "getCardReaderId",
        "()Lcom/squareup/cardreader/CardReaderId;",
        "getMessage",
        "()Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field private final message:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;)V
    .locals 1

    const-string v0, "cardReaderId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureMessageWrapper;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    iput-object p2, p0, Lcom/squareup/cardreader/PaymentFeatureMessageWrapper;->message:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;

    return-void
.end method


# virtual methods
.method public final getCardReaderId()Lcom/squareup/cardreader/CardReaderId;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureMessageWrapper;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return-object v0
.end method

.method public final getMessage()Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureMessageWrapper;->message:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;

    return-object v0
.end method
