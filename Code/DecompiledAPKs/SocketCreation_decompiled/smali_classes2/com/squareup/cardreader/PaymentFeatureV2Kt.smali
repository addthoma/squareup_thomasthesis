.class public final Lcom/squareup/cardreader/PaymentFeatureV2Kt;
.super Ljava/lang/Object;
.source "PaymentFeatureV2.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0002\u001a\u000c\u0010\u0003\u001a\u00020\u0004*\u00020\u0005H\u0002\u001a\u000c\u0010\u0003\u001a\u00020\u0006*\u00020\u0007H\u0002\u001a\u000c\u0010\u0003\u001a\u00020\u0008*\u00020\tH\u0002\u001a\u000c\u0010\u0003\u001a\u00020\n*\u00020\u000bH\u0002\u001a\n\u0010\u000c\u001a\u00020\u0005*\u00020\u0004\u001a\n\u0010\u000c\u001a\u00020\r*\u00020\u000e\u001a\u000c\u0010\u000c\u001a\u00020\u000f*\u00020\u0010H\u0002\u00a8\u0006\u0011"
    }
    d2 = {
        "toCardDescription",
        "Lcom/squareup/cardreader/CardDescription;",
        "Lcom/squareup/cardreader/CardInfo;",
        "toPosEnum",
        "Lcom/squareup/cardreader/PaymentAccountType;",
        "Lcom/squareup/cardreader/lcr/CrPaymentAccountType;",
        "Lcom/squareup/cardreader/TmnAudio;",
        "Lcom/squareup/cardreader/lcr/CrsTmnAudio;",
        "Lcom/squareup/cardreader/TmnMessage;",
        "Lcom/squareup/cardreader/lcr/CrsTmnMessage;",
        "Lcom/squareup/cardreader/TmnTransactionResult;",
        "Lcom/squareup/cardreader/lcr/TmnTransactionResult;",
        "toSwigEnum",
        "Lcom/squareup/cardreader/lcr/CrsTmnBrandId;",
        "Lcom/squareup/cardreader/TmnBrandId;",
        "Lcom/squareup/cardreader/lcr/CrsTmnRequestType;",
        "Lcom/squareup/cardreader/TmnRequestType;",
        "cardreader-features_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$toCardDescription(Lcom/squareup/cardreader/CardInfo;)Lcom/squareup/cardreader/CardDescription;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/cardreader/PaymentFeatureV2Kt;->toCardDescription(Lcom/squareup/cardreader/CardInfo;)Lcom/squareup/cardreader/CardDescription;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toPosEnum(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)Lcom/squareup/cardreader/PaymentAccountType;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/cardreader/PaymentFeatureV2Kt;->toPosEnum(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)Lcom/squareup/cardreader/PaymentAccountType;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toPosEnum(Lcom/squareup/cardreader/lcr/CrsTmnAudio;)Lcom/squareup/cardreader/TmnAudio;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/cardreader/PaymentFeatureV2Kt;->toPosEnum(Lcom/squareup/cardreader/lcr/CrsTmnAudio;)Lcom/squareup/cardreader/TmnAudio;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toPosEnum(Lcom/squareup/cardreader/lcr/CrsTmnMessage;)Lcom/squareup/cardreader/TmnMessage;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/cardreader/PaymentFeatureV2Kt;->toPosEnum(Lcom/squareup/cardreader/lcr/CrsTmnMessage;)Lcom/squareup/cardreader/TmnMessage;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toPosEnum(Lcom/squareup/cardreader/lcr/TmnTransactionResult;)Lcom/squareup/cardreader/TmnTransactionResult;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/cardreader/PaymentFeatureV2Kt;->toPosEnum(Lcom/squareup/cardreader/lcr/TmnTransactionResult;)Lcom/squareup/cardreader/TmnTransactionResult;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toSwigEnum(Lcom/squareup/cardreader/TmnRequestType;)Lcom/squareup/cardreader/lcr/CrsTmnRequestType;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/cardreader/PaymentFeatureV2Kt;->toSwigEnum(Lcom/squareup/cardreader/TmnRequestType;)Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    move-result-object p0

    return-object p0
.end method

.method private static final toCardDescription(Lcom/squareup/cardreader/CardInfo;)Lcom/squareup/cardreader/CardDescription;
    .locals 3

    .line 598
    new-instance v0, Lcom/squareup/cardreader/CardDescription;

    invoke-virtual {p0}, Lcom/squareup/cardreader/CardInfo;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/cardreader/CardInfo;->getLast4()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/squareup/cardreader/CardInfo;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/cardreader/CardDescription;-><init>(Lcom/squareup/Card$Brand;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private static final toPosEnum(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)Lcom/squareup/cardreader/PaymentAccountType;
    .locals 1

    .line 705
    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$6:[I

    invoke-virtual {p0}, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    .line 710
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "invalid value"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 709
    :cond_1
    sget-object p0, Lcom/squareup/cardreader/PaymentAccountType;->PAYMENT_ACCOUNT_TYPE_CREDIT:Lcom/squareup/cardreader/PaymentAccountType;

    goto :goto_0

    .line 708
    :cond_2
    sget-object p0, Lcom/squareup/cardreader/PaymentAccountType;->PAYMENT_ACCOUNT_TYPE_DEBIT:Lcom/squareup/cardreader/PaymentAccountType;

    goto :goto_0

    .line 707
    :cond_3
    sget-object p0, Lcom/squareup/cardreader/PaymentAccountType;->PAYMENT_ACCOUNT_TYPE_SAVINGS:Lcom/squareup/cardreader/PaymentAccountType;

    goto :goto_0

    .line 706
    :cond_4
    sget-object p0, Lcom/squareup/cardreader/PaymentAccountType;->PAYMENT_ACCOUNT_TYPE_DEFAULT:Lcom/squareup/cardreader/PaymentAccountType;

    :goto_0
    return-object p0
.end method

.method private static final toPosEnum(Lcom/squareup/cardreader/lcr/CrsTmnAudio;)Lcom/squareup/cardreader/TmnAudio;
    .locals 1

    .line 600
    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 606
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :pswitch_0
    sget-object p0, Lcom/squareup/cardreader/TmnAudio;->TMN_AUDIO_RE_TOUCH_LOOP:Lcom/squareup/cardreader/TmnAudio;

    goto :goto_0

    .line 605
    :pswitch_1
    sget-object p0, Lcom/squareup/cardreader/TmnAudio;->TMN_AUDIO_FAILURE:Lcom/squareup/cardreader/TmnAudio;

    goto :goto_0

    .line 604
    :pswitch_2
    sget-object p0, Lcom/squareup/cardreader/TmnAudio;->TMN_AUDIO_SUICA_SUCCESS_2:Lcom/squareup/cardreader/TmnAudio;

    goto :goto_0

    .line 603
    :pswitch_3
    sget-object p0, Lcom/squareup/cardreader/TmnAudio;->TMN_AUDIO_SUICA_SUCCESS:Lcom/squareup/cardreader/TmnAudio;

    goto :goto_0

    .line 602
    :pswitch_4
    sget-object p0, Lcom/squareup/cardreader/TmnAudio;->TMN_AUDIO_ID_SUCCESS:Lcom/squareup/cardreader/TmnAudio;

    goto :goto_0

    .line 601
    :pswitch_5
    sget-object p0, Lcom/squareup/cardreader/TmnAudio;->TMN_AUDIO_QP_SUCCESS:Lcom/squareup/cardreader/TmnAudio;

    :goto_0
    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static final toPosEnum(Lcom/squareup/cardreader/lcr/CrsTmnMessage;)Lcom/squareup/cardreader/TmnMessage;
    .locals 1

    .line 651
    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p0}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 674
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :pswitch_0
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_MIRYO_RESULT_UNKNOWN:Lcom/squareup/cardreader/TmnMessage;

    goto :goto_0

    .line 673
    :pswitch_1
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_MIRYO_RESULT_FAILURE:Lcom/squareup/cardreader/TmnMessage;

    goto :goto_0

    .line 672
    :pswitch_2
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_MIRYO_RESULT_SUCCESS:Lcom/squareup/cardreader/TmnMessage;

    goto :goto_0

    .line 671
    :pswitch_3
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_POLLING_TIMEOUT:Lcom/squareup/cardreader/TmnMessage;

    goto :goto_0

    .line 670
    :pswitch_4
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_WRONG_CARD_ERROR:Lcom/squareup/cardreader/TmnMessage;

    goto :goto_0

    .line 669
    :pswitch_5
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_LOCKED_MOBILE_SERVICE:Lcom/squareup/cardreader/TmnMessage;

    goto :goto_0

    .line 668
    :pswitch_6
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_EXPIRED_CARD:Lcom/squareup/cardreader/TmnMessage;

    goto :goto_0

    .line 667
    :pswitch_7
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_EXCEEDED_LIMIT_AMOUNT:Lcom/squareup/cardreader/TmnMessage;

    goto :goto_0

    .line 666
    :pswitch_8
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_COMMON_ERROR:Lcom/squareup/cardreader/TmnMessage;

    goto :goto_0

    .line 665
    :pswitch_9
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_DIFFERENT_CARD:Lcom/squareup/cardreader/TmnMessage;

    goto :goto_0

    .line 664
    :pswitch_a
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_CANCEL:Lcom/squareup/cardreader/TmnMessage;

    goto :goto_0

    .line 663
    :pswitch_b
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_INVALID_CARD:Lcom/squareup/cardreader/TmnMessage;

    goto :goto_0

    .line 662
    :pswitch_c
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_WRITE_ERROR:Lcom/squareup/cardreader/TmnMessage;

    goto :goto_0

    .line 661
    :pswitch_d
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_READ_ERROR:Lcom/squareup/cardreader/TmnMessage;

    goto :goto_0

    .line 660
    :pswitch_e
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_COMPLETE_NORMAL_CHECK_BAL:Lcom/squareup/cardreader/TmnMessage;

    goto :goto_0

    .line 659
    :pswitch_f
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_ONLINE_PROCESSING:Lcom/squareup/cardreader/TmnMessage;

    goto :goto_0

    .line 658
    :pswitch_10
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_RETAP_WAITING:Lcom/squareup/cardreader/TmnMessage;

    goto :goto_0

    .line 657
    :pswitch_11
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_SEVERAL_SUICA_CARD:Lcom/squareup/cardreader/TmnMessage;

    goto :goto_0

    .line 656
    :pswitch_12
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_INSUFFICIENT_BALANCE:Lcom/squareup/cardreader/TmnMessage;

    goto :goto_0

    .line 655
    :pswitch_13
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_COMPLETE_NORMAL_SETTLE:Lcom/squareup/cardreader/TmnMessage;

    goto :goto_0

    .line 654
    :pswitch_14
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_PROCESSING_TRANSACTION:Lcom/squareup/cardreader/TmnMessage;

    goto :goto_0

    .line 653
    :pswitch_15
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_WAITING:Lcom/squareup/cardreader/TmnMessage;

    goto :goto_0

    .line 652
    :pswitch_16
    sget-object p0, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_UNKNOWN_ERROR:Lcom/squareup/cardreader/TmnMessage;

    :goto_0
    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static final toPosEnum(Lcom/squareup/cardreader/lcr/TmnTransactionResult;)Lcom/squareup/cardreader/TmnTransactionResult;
    .locals 1

    .line 610
    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p0}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 648
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :pswitch_0
    sget-object p0, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_OTHER:Lcom/squareup/cardreader/TmnTransactionResult;

    goto :goto_0

    .line 647
    :pswitch_1
    sget-object p0, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_MIRYO_RESULT_FAILURE:Lcom/squareup/cardreader/TmnTransactionResult;

    goto :goto_0

    .line 645
    :pswitch_2
    sget-object p0, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_MIRYO_RESOLUTION_FAILURE:Lcom/squareup/cardreader/TmnTransactionResult;

    goto :goto_0

    .line 643
    :pswitch_3
    sget-object p0, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_ONLINE_PROCESSING_FAILURE:Lcom/squareup/cardreader/TmnTransactionResult;

    goto :goto_0

    .line 641
    :pswitch_4
    sget-object p0, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_DISABLED_TERMINAL:Lcom/squareup/cardreader/TmnTransactionResult;

    goto :goto_0

    .line 639
    :pswitch_5
    sget-object p0, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_SUMMARY_ERROR:Lcom/squareup/cardreader/TmnTransactionResult;

    goto :goto_0

    .line 637
    :pswitch_6
    sget-object p0, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_INVALID_PARAMETER:Lcom/squareup/cardreader/TmnTransactionResult;

    goto :goto_0

    .line 635
    :pswitch_7
    sget-object p0, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_CENTER_OPERATION_FAILED:Lcom/squareup/cardreader/TmnTransactionResult;

    goto :goto_0

    .line 633
    :pswitch_8
    sget-object p0, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_EXCEED_THRESHOLD:Lcom/squareup/cardreader/TmnTransactionResult;

    goto :goto_0

    .line 631
    :pswitch_9
    sget-object p0, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_MULTIPLE_CARDS_DETECTED:Lcom/squareup/cardreader/TmnTransactionResult;

    goto :goto_0

    .line 629
    :pswitch_a
    sget-object p0, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_IMPOSSIBLE_OPERATION:Lcom/squareup/cardreader/TmnTransactionResult;

    goto :goto_0

    .line 627
    :pswitch_b
    sget-object p0, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_POLLING_TIMEOUT:Lcom/squareup/cardreader/TmnTransactionResult;

    goto :goto_0

    .line 625
    :pswitch_c
    sget-object p0, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_TMN_CENTER_ERROR:Lcom/squareup/cardreader/TmnTransactionResult;

    goto :goto_0

    .line 623
    :pswitch_d
    sget-object p0, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_WAITING_FOR_RETOUCH:Lcom/squareup/cardreader/TmnTransactionResult;

    goto :goto_0

    .line 621
    :pswitch_e
    sget-object p0, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_INSUFFICIENT_BALANCE:Lcom/squareup/cardreader/TmnTransactionResult;

    goto :goto_0

    .line 619
    :pswitch_f
    sget-object p0, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_CANCELLATION:Lcom/squareup/cardreader/TmnTransactionResult;

    goto :goto_0

    .line 617
    :pswitch_10
    sget-object p0, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_INVALID_BRAND_SELECTION:Lcom/squareup/cardreader/TmnTransactionResult;

    goto :goto_0

    .line 615
    :pswitch_11
    sget-object p0, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_DISABLED_CARD:Lcom/squareup/cardreader/TmnTransactionResult;

    goto :goto_0

    .line 613
    :pswitch_12
    sget-object p0, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_CARD_READ_ERROR:Lcom/squareup/cardreader/TmnTransactionResult;

    goto :goto_0

    .line 611
    :pswitch_13
    sget-object p0, Lcom/squareup/cardreader/TmnTransactionResult;->TMN_RESULT_SUCCESS:Lcom/squareup/cardreader/TmnTransactionResult;

    :goto_0
    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static final toSwigEnum(Lcom/squareup/cardreader/PaymentAccountType;)Lcom/squareup/cardreader/lcr/CrPaymentAccountType;
    .locals 1

    const-string v0, "$this$toSwigEnum"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 698
    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$5:[I

    invoke-virtual {p0}, Lcom/squareup/cardreader/PaymentAccountType;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-ne p0, v0, :cond_0

    .line 702
    sget-object p0, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_CREDIT:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 701
    :cond_1
    sget-object p0, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_DEBIT:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    goto :goto_0

    .line 700
    :cond_2
    sget-object p0, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_SAVINGS:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    goto :goto_0

    .line 699
    :cond_3
    sget-object p0, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_DEFAULT:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    :goto_0
    return-object p0
.end method

.method public static final toSwigEnum(Lcom/squareup/cardreader/TmnBrandId;)Lcom/squareup/cardreader/lcr/CrsTmnBrandId;
    .locals 1

    const-string v0, "$this$toSwigEnum"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 687
    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$4:[I

    invoke-virtual {p0}, Lcom/squareup/cardreader/TmnBrandId;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 695
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :pswitch_0
    sget-object p0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_PITAPA:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    goto :goto_0

    .line 694
    :pswitch_1
    sget-object p0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_NANACO:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    goto :goto_0

    .line 693
    :pswitch_2
    sget-object p0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_WAON:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    goto :goto_0

    .line 692
    :pswitch_3
    sget-object p0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_RAKUTEN:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    goto :goto_0

    .line 691
    :pswitch_4
    sget-object p0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_SUICA:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    goto :goto_0

    .line 690
    :pswitch_5
    sget-object p0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_ID:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    goto :goto_0

    .line 689
    :pswitch_6
    sget-object p0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_QUICPAY:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    goto :goto_0

    .line 688
    :pswitch_7
    sget-object p0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_COMMON:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    :goto_0
    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static final toSwigEnum(Lcom/squareup/cardreader/TmnRequestType;)Lcom/squareup/cardreader/lcr/CrsTmnRequestType;
    .locals 1

    .line 677
    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$3:[I

    invoke-virtual {p0}, Lcom/squareup/cardreader/TmnRequestType;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 684
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :pswitch_0
    sget-object p0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_VOID_UNKNOWN:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    goto :goto_0

    .line 683
    :pswitch_1
    sget-object p0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_CHECK_RESULT:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    goto :goto_0

    .line 682
    :pswitch_2
    sget-object p0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_ONLINE_TEST:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    goto :goto_0

    .line 681
    :pswitch_3
    sget-object p0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_CHECK_HISTORY:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    goto :goto_0

    .line 680
    :pswitch_4
    sget-object p0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_CANCELLATION:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    goto :goto_0

    .line 679
    :pswitch_5
    sget-object p0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_CHECK_BALANCE:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    goto :goto_0

    .line 678
    :pswitch_6
    sget-object p0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_TRANSACTION:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    :goto_0
    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
