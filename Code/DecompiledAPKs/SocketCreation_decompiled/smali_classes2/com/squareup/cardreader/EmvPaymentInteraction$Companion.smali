.class public final Lcom/squareup/cardreader/EmvPaymentInteraction$Companion;
.super Ljava/lang/Object;
.source "PaymentInteraction.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/EmvPaymentInteraction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006J\u0016\u0010\u0008\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/cardreader/EmvPaymentInteraction$Companion;",
        "",
        "()V",
        "startPayment",
        "Lcom/squareup/cardreader/EmvPaymentInteraction;",
        "currentTimeMillis",
        "",
        "amountAuthorized",
        "startRefund",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/squareup/cardreader/EmvPaymentInteraction$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final startPayment(JJ)Lcom/squareup/cardreader/EmvPaymentInteraction;
    .locals 7

    .line 26
    new-instance v6, Lcom/squareup/cardreader/EmvPaymentInteraction;

    .line 27
    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;->CR_PAYMENT_TRANSACTION_TYPE_PURCHASE:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    move-object v0, v6

    move-wide v2, p1

    move-wide v4, p3

    .line 26
    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardreader/EmvPaymentInteraction;-><init>(Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;JJ)V

    return-object v6
.end method

.method public final startRefund(JJ)Lcom/squareup/cardreader/EmvPaymentInteraction;
    .locals 7

    .line 35
    new-instance v6, Lcom/squareup/cardreader/EmvPaymentInteraction;

    .line 36
    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;->CR_PAYMENT_TRANSACTION_TYPE_REFUND:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    move-object v0, v6

    move-wide v2, p1

    move-wide v4, p3

    .line 35
    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardreader/EmvPaymentInteraction;-><init>(Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;JJ)V

    return-object v6
.end method
