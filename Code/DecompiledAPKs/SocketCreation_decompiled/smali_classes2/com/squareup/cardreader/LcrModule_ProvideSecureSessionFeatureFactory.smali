.class public final Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;
.super Ljava/lang/Object;
.source "LcrModule_ProvideSecureSessionFeatureFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/SecureSessionFeatureLegacy;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderInfoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderSessionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field

.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final globalRevocationStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureSessionRevocationState;",
            ">;"
        }
    .end annotation
.end field

.field private final isReaderSdkAppProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final minesweeperTicketProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperTicket;",
            ">;"
        }
    .end annotation
.end field

.field private final secureSessionFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperTicket;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureSessionRevocationState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;->contextProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p2, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;->cardReaderSessionProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p3, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p4, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;->minesweeperTicketProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p5, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;->secureSessionFeatureNativeProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p6, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;->globalRevocationStateProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p7, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;->isReaderSdkAppProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperTicket;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureSessionRevocationState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;"
        }
    .end annotation

    .line 62
    new-instance v8, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static provideSecureSessionFeature(Landroid/app/Application;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ms/MinesweeperTicket;Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;Lcom/squareup/cardreader/SecureSessionRevocationState;Z)Lcom/squareup/cardreader/SecureSessionFeatureLegacy;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Lcom/squareup/ms/MinesweeperTicket;",
            "Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;",
            "Lcom/squareup/cardreader/SecureSessionRevocationState;",
            "Z)",
            "Lcom/squareup/cardreader/SecureSessionFeatureLegacy;"
        }
    .end annotation

    .line 70
    invoke-static/range {p0 .. p6}, Lcom/squareup/cardreader/LcrModule;->provideSecureSessionFeature(Landroid/app/Application;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ms/MinesweeperTicket;Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;Lcom/squareup/cardreader/SecureSessionRevocationState;Z)Lcom/squareup/cardreader/SecureSessionFeatureLegacy;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/SecureSessionFeatureLegacy;
    .locals 8

    .line 52
    iget-object v0, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/Application;

    iget-object v2, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;->cardReaderSessionProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/cardreader/CardReaderInfo;

    iget-object v0, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;->minesweeperTicketProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ms/MinesweeperTicket;

    iget-object v0, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;->secureSessionFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;

    iget-object v0, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;->globalRevocationStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/cardreader/SecureSessionRevocationState;

    iget-object v0, p0, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;->isReaderSdkAppProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    invoke-static/range {v1 .. v7}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;->provideSecureSessionFeature(Landroid/app/Application;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ms/MinesweeperTicket;Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;Lcom/squareup/cardreader/SecureSessionRevocationState;Z)Lcom/squareup/cardreader/SecureSessionFeatureLegacy;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;->get()Lcom/squareup/cardreader/SecureSessionFeatureLegacy;

    move-result-object v0

    return-object v0
.end method
