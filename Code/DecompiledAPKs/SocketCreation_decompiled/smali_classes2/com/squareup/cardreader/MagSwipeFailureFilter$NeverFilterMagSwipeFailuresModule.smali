.class public abstract Lcom/squareup/cardreader/MagSwipeFailureFilter$NeverFilterMagSwipeFailuresModule;
.super Ljava/lang/Object;
.source "MagSwipeFailureFilter.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/MagSwipeFailureFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "NeverFilterMagSwipeFailuresModule"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideMagSwipeFailureFilter(Lcom/squareup/cardreader/MagSwipeFailureFilter$NeverFilterMagSwipeFailures;)Lcom/squareup/cardreader/MagSwipeFailureFilter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
