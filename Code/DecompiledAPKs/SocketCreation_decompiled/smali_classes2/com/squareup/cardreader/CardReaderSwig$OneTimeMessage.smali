.class final enum Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;
.super Ljava/lang/Enum;
.source "CardReaderSwig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderSwig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "OneTimeMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

.field public static final enum REQUEST_FIRMWARE_MANIFEST:Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

.field public static final enum REQUEST_SYSTEM_INFO:Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

.field public static final enum REQUEST_TAMPER_DATA:Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

.field public static final enum REQUEST_TAMPER_STATUS:Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 917
    new-instance v0, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

    const/4 v1, 0x0

    const-string v2, "REQUEST_TAMPER_STATUS"

    invoke-direct {v0, v2, v1}, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;->REQUEST_TAMPER_STATUS:Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

    .line 918
    new-instance v0, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

    const/4 v2, 0x1

    const-string v3, "REQUEST_TAMPER_DATA"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;->REQUEST_TAMPER_DATA:Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

    .line 919
    new-instance v0, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

    const/4 v3, 0x2

    const-string v4, "REQUEST_FIRMWARE_MANIFEST"

    invoke-direct {v0, v4, v3}, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;->REQUEST_FIRMWARE_MANIFEST:Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

    .line 920
    new-instance v0, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

    const/4 v4, 0x3

    const-string v5, "REQUEST_SYSTEM_INFO"

    invoke-direct {v0, v5, v4}, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;->REQUEST_SYSTEM_INFO:Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

    .line 916
    sget-object v5, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;->REQUEST_TAMPER_STATUS:Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;->REQUEST_TAMPER_DATA:Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;->REQUEST_FIRMWARE_MANIFEST:Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;->REQUEST_SYSTEM_INFO:Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;->$VALUES:[Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 916
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;
    .locals 1

    .line 916
    const-class v0, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;
    .locals 1

    .line 916
    sget-object v0, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;->$VALUES:[Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

    return-object v0
.end method
