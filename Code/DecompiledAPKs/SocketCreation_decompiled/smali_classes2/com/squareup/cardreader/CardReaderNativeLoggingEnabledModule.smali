.class public abstract Lcom/squareup/cardreader/CardReaderNativeLoggingEnabledModule;
.super Ljava/lang/Object;
.source "CardReaderNativeLoggingEnabledModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideNativeLoggingEnabled()Z
    .locals 1
    .annotation runtime Lcom/squareup/cardreader/NativeLoggingEnabled;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const/4 v0, 0x1

    return v0
.end method
