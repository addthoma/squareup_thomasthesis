.class public Lcom/squareup/cardreader/CardReaderInfo;
.super Ljava/lang/Object;
.source "CardReaderInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;,
        Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;,
        Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;,
        Lcom/squareup/cardreader/CardReaderInfo$Callback;
    }
.end annotation


# static fields
.field public static final AUDIO_READER_ADDRESS:Ljava/lang/String; = null

.field private static final NOT_IN_A_PAYMENT:J = -0x1L

.field private static final R12_DEAD_BATTERY_LEVEL:I = 0x5

.field private static final R6_DEAD_BATTERY_LEVEL:I = 0xa

.field private static final T2_DEAD_BATTERY_LEVEL:I = 0x5

.field private static UNSET_FW_VERSIONS:[Lcom/squareup/cardreader/FirmwareAssetVersionInfo;


# instance fields
.field private final address:Ljava/lang/String;

.field private batteryInfo:Lcom/squareup/cardreader/CardReaderBatteryInfo;

.field private final bleRate:Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;

.field private callback:Lcom/squareup/cardreader/CardReaderInfo$Callback;

.field private capabilitiesSupported:Z

.field private capabilityList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrsCapability;",
            ">;"
        }
    .end annotation
.end field

.field private cardPresence:Z

.field private cardPresenceRequired:Z

.field private final cardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field private chargeCycleCount:I

.field private commsRate:Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;

.field private commsStatus:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

.field private connected:Z

.field private final connectionType:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

.field private firmwareAssetVersionInfos:[Lcom/squareup/cardreader/FirmwareAssetVersionInfo;

.field private firmwareUpdateInProgress:Z

.field private firmwareVersion:Ljava/lang/String;

.field private hardwareSerialNumber:Ljava/lang/String;

.field private hasValidTmsCountryCode:Z

.field private lcrReaderType:Lcom/squareup/cardreader/lcr/CrCardreaderType;

.field private lcrVersion:Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

.field private paymentAmountAuthorized:J

.field private ptsFwVersion:Ljava/lang/String;

.field private final readerName:Ljava/lang/String;

.field private readerRequiresFirmwareUpdate:Z

.field private readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field private readerVersion:Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

.field private secureSessionState:Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;

.field private serverRequiresFirmwareUpdate:Z

.field private tamperStatus:Lcom/squareup/cardreader/lcr/CrTamperStatus;

.field private timedOutDuringInit:Z

.field private tmsCountryCode:Lcom/squareup/CountryCode;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/squareup/cardreader/FirmwareAssetVersionInfo;

    .line 53
    sput-object v0, Lcom/squareup/cardreader/CardReaderInfo;->UNSET_FW_VERSIONS:[Lcom/squareup/cardreader/FirmwareAssetVersionInfo;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    sget-object v0, Lcom/squareup/cardreader/CardReaderInfo;->UNSET_FW_VERSIONS:[Lcom/squareup/cardreader/FirmwareAssetVersionInfo;

    iput-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->firmwareAssetVersionInfos:[Lcom/squareup/cardreader/FirmwareAssetVersionInfo;

    const/4 v0, 0x1

    .line 91
    iput-boolean v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->hasValidTmsCountryCode:Z

    const/4 v1, 0x0

    .line 92
    iput-object v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->tmsCountryCode:Lcom/squareup/CountryCode;

    .line 102
    iput-boolean v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->cardPresenceRequired:Z

    .line 108
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    .line 109
    iput-object p2, p0, Lcom/squareup/cardreader/CardReaderInfo;->connectionType:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    .line 110
    iput-object p3, p0, Lcom/squareup/cardreader/CardReaderInfo;->address:Ljava/lang/String;

    .line 111
    iput-object p4, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerName:Ljava/lang/String;

    .line 112
    sget-object p1, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->BLE:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    if-ne p2, p1, :cond_0

    new-instance p1, Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;

    invoke-direct {p1}, Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;-><init>()V

    goto :goto_0

    :cond_0
    move-object p1, v1

    :goto_0
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->bleRate:Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;

    .line 114
    sget-object p1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->UNKNOWN:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 115
    iput-object v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->commsStatus:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    const/4 p1, 0x0

    .line 116
    iput-boolean p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->capabilitiesSupported:Z

    .line 117
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/cardreader/CardReaderInfo;->capabilityList:Ljava/util/List;

    .line 118
    iput-object v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->tamperStatus:Lcom/squareup/cardreader/lcr/CrTamperStatus;

    .line 119
    sget-object p2, Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;->INVALID:Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;

    iput-object p2, p0, Lcom/squareup/cardreader/CardReaderInfo;->secureSessionState:Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;

    .line 120
    iput-boolean p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->cardPresence:Z

    const-wide/16 p1, -0x1

    .line 121
    iput-wide p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->paymentAmountAuthorized:J

    .line 123
    iput-object v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->hardwareSerialNumber:Ljava/lang/String;

    .line 124
    iput-object v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->firmwareVersion:Ljava/lang/String;

    const/4 p1, -0x1

    .line 125
    iput p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->chargeCycleCount:I

    .line 126
    sget-object p1, Lcom/squareup/cardreader/CardReaderBatteryInfo;->EMPTY_INFO:Lcom/squareup/cardreader/CardReaderBatteryInfo;

    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->batteryInfo:Lcom/squareup/cardreader/CardReaderBatteryInfo;

    .line 128
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInfo;->resetFirmwareUpdateState()V

    return-void
.end method

.method private onChange()V
    .locals 1

    .line 635
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->callback:Lcom/squareup/cardreader/CardReaderInfo$Callback;

    if-eqz v0, :cond_0

    .line 636
    invoke-interface {v0, p0}, Lcom/squareup/cardreader/CardReaderInfo$Callback;->onChange(Lcom/squareup/cardreader/CardReaderInfo;)V

    :cond_0
    return-void
.end method

.method public static readerRequiresBlockingFirmwareUpdateScreen(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Z
    .locals 1

    .line 58
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->T2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private resetFirmwareUpdateState()V
    .locals 1

    const/4 v0, 0x0

    .line 735
    iput-boolean v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerRequiresFirmwareUpdate:Z

    .line 736
    iput-boolean v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->serverRequiresFirmwareUpdate:Z

    .line 737
    iput-boolean v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->firmwareUpdateInProgress:Z

    .line 738
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInfo;->onChange()V

    return-void
.end method


# virtual methods
.method bleConnectionIntervalUpdated(I)V
    .locals 1

    .line 323
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->bleRate:Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;

    invoke-static {v0, p1}, Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;->access$102(Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;I)I

    .line 324
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInfo;->onChange()V

    return-void
.end method

.method bleMtuUpdated(I)V
    .locals 1

    .line 318
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->bleRate:Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;

    invoke-static {v0, p1}, Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;->access$002(Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;I)I

    .line 319
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInfo;->onChange()V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    if-eqz p1, :cond_2

    .line 645
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 647
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/CardReaderInfo;

    .line 649
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    iget-object p1, p1, Lcom/squareup/cardreader/CardReaderInfo;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderId;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public failedSwipesDefaultToSwipeStraight()Z
    .locals 3

    .line 258
    sget-object v0, Lcom/squareup/cardreader/CardReaderInfo$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getBatteryInfo()Lcom/squareup/cardreader/CardReaderBatteryInfo;
    .locals 1

    .line 405
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->batteryInfo:Lcom/squareup/cardreader/CardReaderBatteryInfo;

    return-object v0
.end method

.method public getBatteryMode()Lcom/squareup/cardreader/lcr/CrsBatteryMode;
    .locals 1

    .line 395
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->batteryInfo:Lcom/squareup/cardreader/CardReaderBatteryInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderBatteryInfo;->getBatteryMode()Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    move-result-object v0

    return-object v0
.end method

.method public getBatteryPercentage()I
    .locals 1

    .line 389
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->batteryInfo:Lcom/squareup/cardreader/CardReaderBatteryInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderBatteryInfo;->getPercentage()Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    .line 390
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getBleRate()Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;
    .locals 1

    .line 314
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->bleRate:Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;

    return-object v0
.end method

.method public getCapabilities()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrsCapability;",
            ">;"
        }
    .end annotation

    .line 214
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->capabilityList:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCardReaderId()Lcom/squareup/cardreader/CardReaderId;
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return-object v0
.end method

.method public getChargeCycleCount()I
    .locals 1

    .line 468
    iget v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->chargeCycleCount:I

    return v0
.end method

.method public getCommsRate()Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;
    .locals 1

    .line 306
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->commsRate:Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;

    return-object v0
.end method

.method public getCommsStatus()Lcom/squareup/cardreader/lcr/CrCommsVersionResult;
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->commsStatus:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    return-object v0
.end method

.method public getConnectionType()Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->connectionType:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    return-object v0
.end method

.method public getFirmwareAssetVersionInfos()[Lcom/squareup/cardreader/FirmwareAssetVersionInfo;
    .locals 1

    .line 430
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->firmwareAssetVersionInfos:[Lcom/squareup/cardreader/FirmwareAssetVersionInfo;

    return-object v0
.end method

.method public getFirmwareVersion()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 420
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->firmwareVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getHardwareSerialNumber()Ljava/lang/String;
    .locals 1

    .line 459
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->hardwareSerialNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getLcrReaderType()Lcom/squareup/cardreader/lcr/CrCardreaderType;
    .locals 1

    .line 196
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->lcrReaderType:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    return-object v0
.end method

.method public getLcrVersion()Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;
    .locals 1

    .line 332
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->lcrVersion:Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    return-object v0
.end method

.method public getPtsFwVersion()Ljava/lang/String;
    .locals 1

    .line 434
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->ptsFwVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getReaderName()Ljava/lang/String;
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerName:Ljava/lang/String;

    return-object v0
.end method

.method public getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;
    .locals 1

    .line 200
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object v0
.end method

.method public getReaderVersion()Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;
    .locals 1

    .line 340
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerVersion:Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    return-object v0
.end method

.method public getSecureSessionState()Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;
    .locals 1

    .line 486
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->secureSessionState:Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;

    return-object v0
.end method

.method public getTamperStatus()Lcom/squareup/cardreader/lcr/CrTamperStatus;
    .locals 1

    .line 352
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->tamperStatus:Lcom/squareup/cardreader/lcr/CrTamperStatus;

    return-object v0
.end method

.method public getTmsCountryCode()Lcom/squareup/CountryCode;
    .locals 1

    .line 533
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->tmsCountryCode:Lcom/squareup/CountryCode;

    return-object v0
.end method

.method public hasBattery()Z
    .locals 2

    .line 143
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R6:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public hasSameId(Lcom/squareup/cardreader/CardReaderInfo;)Z
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderId;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hasSecureSession()Z
    .locals 2

    .line 477
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->secureSessionState:Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;

    sget-object v1, Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;->VALID:Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hasValidTmsCountryCode()Z
    .locals 1

    .line 502
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInfo;->supportsTmsCountryCodeCheck()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 503
    iget-boolean v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->hasValidTmsCountryCode:Z

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public hashCode()I
    .locals 1

    .line 653
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderId;->hashCode()I

    move-result v0

    return v0
.end method

.method public isAudio()Z
    .locals 2

    .line 165
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->connectionType:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    sget-object v1, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->AUDIO:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isBLE()Z
    .locals 2

    .line 174
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->connectionType:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    sget-object v1, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->BLE:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isBatteryCharging()Z
    .locals 2

    .line 400
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInfo;->getBatteryMode()Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 401
    sget-object v1, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->CRS_BATTERY_MODE_CHARGING:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isBatteryDead()Z
    .locals 7

    .line 364
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->batteryInfo:Lcom/squareup/cardreader/CardReaderBatteryInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderBatteryInfo;->getPercentage()Ljava/lang/Integer;

    move-result-object v0

    .line 365
    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->batteryInfo:Lcom/squareup/cardreader/CardReaderBatteryInfo;

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderBatteryInfo;->getBatteryMode()Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    move-result-object v1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    return v2

    .line 372
    :cond_0
    sget-object v3, Lcom/squareup/cardreader/CardReaderInfo$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    iget-object v4, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {v4}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    const/4 v4, 0x2

    const/4 v5, 0x5

    const/4 v6, 0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x3

    if-eq v3, v4, :cond_3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    if-eqz v1, :cond_1

    .line 376
    sget-object v3, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->CRS_BATTERY_MODE_CHARGING:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    if-eq v1, v3, :cond_1

    .line 378
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gt v0, v5, :cond_1

    const/4 v2, 0x1

    :cond_1
    return v2

    .line 382
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to query battery morbidity on reader type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 374
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0xa

    if-gt v0, v1, :cond_4

    const/4 v2, 0x1

    :cond_4
    return v2

    .line 380
    :cond_5
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gt v0, v5, :cond_6

    const/4 v2, 0x1

    :cond_6
    return v2
.end method

.method public isCapabilitiesSupported()Z
    .locals 1

    .line 210
    iget-boolean v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->capabilitiesSupported:Z

    return v0
.end method

.method public isCardPresenceRequired()Z
    .locals 1

    .line 618
    iget-boolean v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->cardPresenceRequired:Z

    return v0
.end method

.method public isCardPresent()Z
    .locals 1

    .line 590
    iget-boolean v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->cardPresence:Z

    return v0
.end method

.method public isConnected()Z
    .locals 1

    .line 179
    iget-boolean v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->connected:Z

    return v0
.end method

.method public isFirmwareUpdateBlocking()Z
    .locals 1

    .line 545
    iget-boolean v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerRequiresFirmwareUpdate:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->serverRequiresFirmwareUpdate:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isFirmwareUpdateInProgress()Z
    .locals 1

    .line 570
    iget-boolean v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->firmwareUpdateInProgress:Z

    return v0
.end method

.method public isInPayment()Z
    .locals 5

    .line 599
    iget-wide v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->paymentAmountAuthorized:J

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSmartReader()Z
    .locals 2

    .line 136
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R6:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->X2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->T2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isSquareDeviceCardReader()Z
    .locals 2

    .line 275
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->X2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->T2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isSquareTerminal()Z
    .locals 2

    .line 282
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->T2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSystemInfoAcquired()Z
    .locals 2

    .line 584
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->firmwareVersion:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->chargeCycleCount:I

    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->hardwareSerialNumber:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isTamperStatusNormal()Z
    .locals 2

    .line 356
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->tamperStatus:Lcom/squareup/cardreader/lcr/CrTamperStatus;

    sget-object v1, Lcom/squareup/cardreader/lcr/CrTamperStatus;->CR_TAMPER_STATUS_NORMAL:Lcom/squareup/cardreader/lcr/CrTamperStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isTimedOutDuringInit()Z
    .locals 1

    .line 344
    iget-boolean v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->timedOutDuringInit:Z

    return v0
.end method

.method public isWireless()Z
    .locals 2

    .line 170
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->connectionType:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    sget-object v1, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->BLE:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->connectionType:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    sget-object v1, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->BLUETOOTH:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public requiresBlockingFirmwareUpdateScreen()Z
    .locals 1

    .line 268
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInfo;->readerRequiresBlockingFirmwareUpdateScreen(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Z

    move-result v0

    return v0
.end method

.method public requiresTalkBackWarning()Z
    .locals 2

    .line 297
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->T2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method setBatteryInfo(Lcom/squareup/cardreader/CardReaderBatteryInfo;)V
    .locals 0

    .line 409
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->batteryInfo:Lcom/squareup/cardreader/CardReaderBatteryInfo;

    .line 411
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->batteryInfo:Lcom/squareup/cardreader/CardReaderBatteryInfo;

    if-nez p1, :cond_0

    .line 412
    sget-object p1, Lcom/squareup/cardreader/CardReaderBatteryInfo;->EMPTY_INFO:Lcom/squareup/cardreader/CardReaderBatteryInfo;

    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->batteryInfo:Lcom/squareup/cardreader/CardReaderBatteryInfo;

    .line 415
    :cond_0
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInfo;->onChange()V

    return-void
.end method

.method setCallback(Lcom/squareup/cardreader/CardReaderInfo$Callback;)V
    .locals 2

    .line 626
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->callback:Lcom/squareup/cardreader/CardReaderInfo$Callback;

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 627
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot replace a Callback, it\'s already "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->callback:Lcom/squareup/cardreader/CardReaderInfo$Callback;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 630
    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->callback:Lcom/squareup/cardreader/CardReaderInfo$Callback;

    .line 631
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInfo;->onChange()V

    return-void
.end method

.method setCapabilities(ZLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrsCapability;",
            ">;)V"
        }
    .end annotation

    .line 204
    iput-boolean p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->capabilitiesSupported:Z

    .line 205
    iput-object p2, p0, Lcom/squareup/cardreader/CardReaderInfo;->capabilityList:Ljava/util/List;

    .line 206
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInfo;->onChange()V

    return-void
.end method

.method setCardPresence(Z)V
    .locals 0

    .line 594
    iput-boolean p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->cardPresence:Z

    .line 595
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInfo;->onChange()V

    return-void
.end method

.method public setCardPresenceRequired(Z)V
    .locals 0

    .line 614
    iput-boolean p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->cardPresenceRequired:Z

    return-void
.end method

.method setChargeCycleCount(I)V
    .locals 0

    .line 472
    iput p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->chargeCycleCount:I

    .line 473
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInfo;->onChange()V

    return-void
.end method

.method setCommsRate(Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;)V
    .locals 0

    .line 310
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->commsRate:Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;

    return-void
.end method

.method setCommsStatus(Lcom/squareup/cardreader/lcr/CrCommsVersionResult;)V
    .locals 0

    .line 191
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->commsStatus:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    .line 192
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInfo;->onChange()V

    return-void
.end method

.method setConnected(Z)V
    .locals 0

    .line 183
    iput-boolean p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->connected:Z

    return-void
.end method

.method setFirmwareAssetVersionInfos([Lcom/squareup/cardreader/FirmwareAssetVersionInfo;Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V
    .locals 5

    if-nez p1, :cond_0

    .line 440
    sget-object p1, Lcom/squareup/cardreader/CardReaderInfo;->UNSET_FW_VERSIONS:[Lcom/squareup/cardreader/FirmwareAssetVersionInfo;

    .line 443
    :cond_0
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_3

    aget-object v2, p1, v1

    .line 444
    invoke-virtual {v2}, Lcom/squareup/cardreader/FirmwareAssetVersionInfo;->getFirmwareAsset()Lcom/squareup/cardreader/lcr/CrFirmwareAsset;

    move-result-object v3

    sget-object v4, Lcom/squareup/cardreader/lcr/CrFirmwareAsset;->CR_FIRMWARE_UPDATE_VERSION_INFO_K21:Lcom/squareup/cardreader/lcr/CrFirmwareAsset;

    if-eq v3, v4, :cond_2

    .line 445
    invoke-virtual {v2}, Lcom/squareup/cardreader/FirmwareAssetVersionInfo;->getFirmwareAsset()Lcom/squareup/cardreader/lcr/CrFirmwareAsset;

    move-result-object v3

    sget-object v4, Lcom/squareup/cardreader/lcr/CrFirmwareAsset;->CR_FIRMWARE_UPDATE_VERSION_INFO_K450_CPU1:Lcom/squareup/cardreader/lcr/CrFirmwareAsset;

    if-ne v3, v4, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 446
    :cond_2
    :goto_1
    invoke-virtual {v2}, Lcom/squareup/cardreader/FirmwareAssetVersionInfo;->getVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->ptsFwVersion:Ljava/lang/String;

    .line 452
    :cond_3
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->ptsFwVersion:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/squareup/cardreader/SCRPFirmwareVersionHelper;->convertFwVersionToSCRPIfRequired(Ljava/lang/String;Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/cardreader/CardReaderInfo;->ptsFwVersion:Ljava/lang/String;

    .line 454
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->firmwareAssetVersionInfos:[Lcom/squareup/cardreader/FirmwareAssetVersionInfo;

    .line 455
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInfo;->onChange()V

    return-void
.end method

.method setFirmwareUpdateInProgress(Z)V
    .locals 0

    .line 574
    iput-boolean p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->firmwareUpdateInProgress:Z

    .line 575
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInfo;->onChange()V

    return-void
.end method

.method public setFirmwareUpdatesComplete()V
    .locals 0

    .line 579
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInfo;->resetFirmwareUpdateState()V

    .line 580
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInfo;->onChange()V

    return-void
.end method

.method public setFirmwareVersion(Ljava/lang/String;)V
    .locals 0

    .line 425
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->firmwareVersion:Ljava/lang/String;

    .line 426
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInfo;->onChange()V

    return-void
.end method

.method setHardwareSerialNumber(Ljava/lang/String;)V
    .locals 0

    .line 463
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->hardwareSerialNumber:Ljava/lang/String;

    .line 464
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInfo;->onChange()V

    return-void
.end method

.method setIsInPayment(J)V
    .locals 7

    .line 603
    iget-wide v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->paymentAmountAuthorized:J

    const/4 v2, 0x1

    const-wide/16 v3, -0x1

    cmp-long v5, v0, v3

    if-lez v5, :cond_1

    cmp-long v3, v0, p1

    if-nez v3, :cond_0

    goto :goto_0

    .line 605
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v5, p0, Lcom/squareup/cardreader/CardReaderInfo;->paymentAmountAuthorized:J

    .line 607
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    aput-object p1, v3, v2

    const-string p1, "Already in a payment on this reader with a different authorized amount! Was %d, now %d."

    .line 605
    invoke-static {v1, p1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 609
    :cond_1
    :goto_0
    iput-wide p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->paymentAmountAuthorized:J

    .line 610
    iput-boolean v2, p0, Lcom/squareup/cardreader/CardReaderInfo;->cardPresenceRequired:Z

    return-void
.end method

.method setLcrReaderType(Lcom/squareup/cardreader/lcr/CrCardreaderType;)V
    .locals 0

    .line 218
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->lcrReaderType:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    return-void
.end method

.method setLcrVersion(Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V
    .locals 0

    .line 328
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->lcrVersion:Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    return-void
.end method

.method public setNotInPayment()V
    .locals 2

    const-wide/16 v0, -0x1

    .line 622
    iput-wide v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->paymentAmountAuthorized:J

    return-void
.end method

.method setReaderRequiresFirmwareUpdate(Z)V
    .locals 0

    .line 554
    iput-boolean p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerRequiresFirmwareUpdate:Z

    .line 555
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInfo;->onChange()V

    return-void
.end method

.method public setReaderType(Lcom/squareup/protos/client/bills/CardData$ReaderType;)V
    .locals 0

    .line 222
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-void
.end method

.method setReaderVersion(Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V
    .locals 0

    .line 336
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerVersion:Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    return-void
.end method

.method setSecureSessionState(Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;)V
    .locals 0

    .line 481
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->secureSessionState:Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;

    .line 482
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInfo;->onChange()V

    return-void
.end method

.method setServerRequiresFirmwareUpdate(Z)V
    .locals 0

    .line 565
    iput-boolean p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->serverRequiresFirmwareUpdate:Z

    .line 566
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInfo;->onChange()V

    return-void
.end method

.method setTamperStatus(Lcom/squareup/cardreader/lcr/CrTamperStatus;)V
    .locals 0

    .line 360
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->tamperStatus:Lcom/squareup/cardreader/lcr/CrTamperStatus;

    return-void
.end method

.method setTimedOutDuringInit(Z)V
    .locals 0

    .line 348
    iput-boolean p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->timedOutDuringInit:Z

    return-void
.end method

.method public setTmsCountryCode(Ljava/lang/String;)V
    .locals 0

    .line 528
    invoke-static {p1}, Lcom/squareup/CountryCode;->parseCountryCode(Ljava/lang/String;)Lcom/squareup/CountryCode;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->tmsCountryCode:Lcom/squareup/CountryCode;

    .line 529
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInfo;->onChange()V

    return-void
.end method

.method public setTmsCountryCodeValidState(Z)V
    .locals 2

    .line 511
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInfo;->supportsTmsCountryCodeCheck()Z

    move-result v0

    const-string v1, "Can only call setTmsCountryCodeValidState if supportsTmsCountryCodeCheck is true"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 513
    iput-boolean p1, p0, Lcom/squareup/cardreader/CardReaderInfo;->hasValidTmsCountryCode:Z

    .line 514
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInfo;->onChange()V

    return-void
.end method

.method public supportsDips()Z
    .locals 2

    .line 234
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R6:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->X2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->T2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public supportsFeatureFlags()Z
    .locals 2

    .line 230
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->X2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->T2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public supportsFelicaCards()Z
    .locals 2

    .line 245
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->lcrReaderType:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_R12C:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->lcrReaderType:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_T2B:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public supportsFirmwareUpdateFromSquidImage()Z
    .locals 2

    .line 290
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->X2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->T2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public supportsNfcTimeouts()Z
    .locals 2

    .line 302
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->X2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public supportsSwipes()Z
    .locals 2

    .line 241
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInfo;->isAudio()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->X2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->T2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public supportsTaps()Z
    .locals 2

    .line 226
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->X2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->T2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public supportsTmsCountryCodeCheck()Z
    .locals 2

    .line 518
    sget-object v0, Lcom/squareup/cardreader/CardReaderInfo$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public toProto()Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;
    .locals 4

    .line 674
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->batteryInfo:Lcom/squareup/cardreader/CardReaderBatteryInfo;

    .line 675
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderBatteryInfo;->getPercentage()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->percentage(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->batteryInfo:Lcom/squareup/cardreader/CardReaderBatteryInfo;

    .line 676
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderBatteryInfo;->getMilliamps()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->current(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->batteryInfo:Lcom/squareup/cardreader/CardReaderBatteryInfo;

    .line 677
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderBatteryInfo;->getMillivolts()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->voltage(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->batteryInfo:Lcom/squareup/cardreader/CardReaderBatteryInfo;

    .line 678
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderBatteryInfo;->getCelsius()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->temperature(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->batteryInfo:Lcom/squareup/cardreader/CardReaderBatteryInfo;

    .line 679
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderBatteryInfo;->isCritical()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->is_critical(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->batteryInfo:Lcom/squareup/cardreader/CardReaderBatteryInfo;

    .line 680
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderBatteryInfo;->getBatteryMode()Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    move-result-object v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v1, v2

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->batteryInfo:Lcom/squareup/cardreader/CardReaderBatteryInfo;

    .line 681
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderBatteryInfo;->getBatteryMode()Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->swigValue()I

    move-result v1

    invoke-static {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->fromValue(I)Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    move-result-object v1

    .line 680
    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->battery_mode(Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;

    move-result-object v0

    .line 682
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    move-result-object v0

    .line 684
    new-instance v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;

    invoke-direct {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;-><init>()V

    iget-object v3, p0, Lcom/squareup/cardreader/CardReaderInfo;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    iget v3, v3, Lcom/squareup/cardreader/CardReaderId;->id:I

    .line 685
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->id(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;

    move-result-object v1

    iget-object v3, p0, Lcom/squareup/cardreader/CardReaderInfo;->connectionType:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    .line 686
    invoke-virtual {v3}, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ConnectionType;->valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ConnectionType;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->connection_type(Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ConnectionType;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;

    move-result-object v1

    iget-object v3, p0, Lcom/squareup/cardreader/CardReaderInfo;->commsStatus:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    if-nez v3, :cond_1

    goto :goto_1

    .line 688
    :cond_1
    invoke-virtual {v3}, Lcom/squareup/cardreader/lcr/CrCommsVersionResult;->swigValue()I

    move-result v2

    invoke-static {v2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;->fromValue(I)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    move-result-object v2

    .line 687
    :goto_1
    invoke-virtual {v1, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->comms_status(Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/squareup/cardreader/CardReaderInfo;->connected:Z

    .line 689
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->connected(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderInfo;->secureSessionState:Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;

    .line 691
    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;->name()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;->valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    move-result-object v2

    .line 690
    invoke-virtual {v1, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->secure_session(Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/squareup/cardreader/CardReaderInfo;->cardPresence:Z

    .line 692
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->card_presence(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderInfo;->firmwareVersion:Ljava/lang/String;

    .line 693
    invoke-virtual {v1, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->firmware_version(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderInfo;->hardwareSerialNumber:Ljava/lang/String;

    .line 694
    invoke-virtual {v1, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->hardware_serial_number(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/cardreader/CardReaderInfo;->chargeCycleCount:I

    .line 695
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->charge_cycle_count(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;

    move-result-object v1

    .line 696
    invoke-virtual {v1, v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->battery_info(Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->firmwareUpdateInProgress:Z

    .line 697
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->firmware_update_in_progress(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerRequiresFirmwareUpdate:Z

    .line 698
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->reader_requires_firmware_update(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->serverRequiresFirmwareUpdate:Z

    .line 699
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->server_requires_firmware_update(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 700
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->reader_type(Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerName:Ljava/lang/String;

    .line 701
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->reader_name(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;

    move-result-object v0

    .line 702
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 657
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CardReaderInfo{cardReaderId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", connectionType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->connectionType:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", address=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->address:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", readerName=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", readerType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInfo;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateWithProto(Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;)V
    .locals 3

    .line 706
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->comms_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object v0, v1

    goto :goto_0

    :cond_0
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->comms_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    .line 707
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;->getValue()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/cardreader/lcr/CrCommsVersionResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    move-result-object v0

    .line 706
    :goto_0
    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/CardReaderInfo;->setCommsStatus(Lcom/squareup/cardreader/lcr/CrCommsVersionResult;)V

    .line 708
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;->valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/CardReaderInfo;->setSecureSessionState(Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;)V

    .line 709
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->card_presence:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/CardReaderInfo;->setCardPresence(Z)V

    .line 710
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->firmware_version:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/CardReaderInfo;->setFirmwareVersion(Ljava/lang/String;)V

    .line 711
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->hardware_serial_number:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/CardReaderInfo;->setHardwareSerialNumber(Ljava/lang/String;)V

    .line 712
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->charge_cycle_count:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/CardReaderInfo;->setChargeCycleCount(I)V

    .line 713
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->firmware_update_in_progress:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/CardReaderInfo;->setFirmwareUpdateInProgress(Z)V

    .line 714
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_requires_firmware_update:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/CardReaderInfo;->setReaderRequiresFirmwareUpdate(Z)V

    .line 715
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->server_requires_firmware_update:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/CardReaderInfo;->setServerRequiresFirmwareUpdate(Z)V

    .line 716
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->connected:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/CardReaderInfo;->setConnected(Z)V

    .line 717
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->reader_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/CardReaderInfo;->setReaderType(Lcom/squareup/protos/client/bills/CardData$ReaderType;)V

    .line 719
    new-instance v0, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;-><init>()V

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->battery_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    iget-object v2, v2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->percentage:Ljava/lang/Integer;

    .line 720
    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->setPercentage(Ljava/lang/Integer;)Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;

    move-result-object v0

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->battery_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    iget-object v2, v2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->current:Ljava/lang/Integer;

    .line 721
    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->setCurrent(Ljava/lang/Integer;)Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;

    move-result-object v0

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->battery_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    iget-object v2, v2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->voltage:Ljava/lang/Integer;

    .line 722
    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->setVoltage(Ljava/lang/Integer;)Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;

    move-result-object v0

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->battery_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    iget-object v2, v2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->temperature:Ljava/lang/Integer;

    .line 723
    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->setTemperature(Ljava/lang/Integer;)Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;

    move-result-object v0

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->battery_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    iget-object v2, v2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->is_critical:Ljava/lang/Boolean;

    .line 724
    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->setCritical(Ljava/lang/Boolean;)Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;

    move-result-object v0

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->battery_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    iget-object v2, v2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->battery_mode:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->battery_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->battery_mode:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    .line 726
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->getValue()I

    move-result p1

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    move-result-object v1

    .line 725
    :goto_1
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->setBatteryMode(Lcom/squareup/cardreader/lcr/CrsBatteryMode;)Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;

    move-result-object p1

    .line 727
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->build()Lcom/squareup/cardreader/CardReaderBatteryInfo;

    move-result-object p1

    .line 728
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/CardReaderInfo;->setBatteryInfo(Lcom/squareup/cardreader/CardReaderBatteryInfo;)V

    return-void
.end method
