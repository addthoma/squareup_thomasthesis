.class public Lcom/squareup/cardreader/CardReaderLogBridge$CardReaderNativeLogBridge;
.super Ljava/lang/Object;
.source "CardReaderLogBridge.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderLogBridge;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderLogBridge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CardReaderNativeLogBridge"
.end annotation


# instance fields
.field private final logNative:Lcom/squareup/cardreader/lcr/LogNativeInterface;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/lcr/LogNativeInterface;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderLogBridge$CardReaderNativeLogBridge;->logNative:Lcom/squareup/cardreader/lcr/LogNativeInterface;

    return-void
.end method


# virtual methods
.method public setLoggingEnabled(Z)V
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderLogBridge$CardReaderNativeLogBridge;->logNative:Lcom/squareup/cardreader/lcr/LogNativeInterface;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/lcr/LogNativeInterface;->set_logging_enabled(Z)V

    return-void
.end method
