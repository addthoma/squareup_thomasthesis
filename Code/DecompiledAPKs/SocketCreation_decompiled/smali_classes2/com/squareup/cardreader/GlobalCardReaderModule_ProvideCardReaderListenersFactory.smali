.class public final Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideCardReaderListenersFactory;
.super Ljava/lang/Object;
.source "GlobalCardReaderModule_ProvideCardReaderListenersFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/CardReaderListeners;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/cardreader/GlobalCardReaderModule;

.field private final realCardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/GlobalCardReaderModule;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/GlobalCardReaderModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideCardReaderListenersFactory;->module:Lcom/squareup/cardreader/GlobalCardReaderModule;

    .line 24
    iput-object p2, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideCardReaderListenersFactory;->realCardReaderListenersProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/GlobalCardReaderModule;Ljavax/inject/Provider;)Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideCardReaderListenersFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/GlobalCardReaderModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;)",
            "Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideCardReaderListenersFactory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideCardReaderListenersFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideCardReaderListenersFactory;-><init>(Lcom/squareup/cardreader/GlobalCardReaderModule;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideCardReaderListeners(Lcom/squareup/cardreader/GlobalCardReaderModule;Lcom/squareup/cardreader/RealCardReaderListeners;)Lcom/squareup/cardreader/CardReaderListeners;
    .locals 0

    .line 40
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/GlobalCardReaderModule;->provideCardReaderListeners(Lcom/squareup/cardreader/RealCardReaderListeners;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/CardReaderListeners;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/CardReaderListeners;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideCardReaderListenersFactory;->module:Lcom/squareup/cardreader/GlobalCardReaderModule;

    iget-object v1, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideCardReaderListenersFactory;->realCardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideCardReaderListenersFactory;->provideCardReaderListeners(Lcom/squareup/cardreader/GlobalCardReaderModule;Lcom/squareup/cardreader/RealCardReaderListeners;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideCardReaderListenersFactory;->get()Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v0

    return-object v0
.end method
