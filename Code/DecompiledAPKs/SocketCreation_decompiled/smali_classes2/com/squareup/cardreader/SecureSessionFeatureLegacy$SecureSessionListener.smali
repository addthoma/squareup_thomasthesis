.class interface abstract Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;
.super Ljava/lang/Object;
.source "SecureSessionFeatureLegacy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/SecureSessionFeatureLegacy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "SecureSessionListener"
.end annotation


# virtual methods
.method public abstract onPinRequested(Lcom/squareup/cardreader/PinRequestData;)V
.end method

.method public abstract onSecureSessionDenied(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V
.end method

.method public abstract onSecureSessionError(Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V
.end method

.method public abstract onSecureSessionEvent(Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionData;Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;)V
.end method

.method public abstract onSecureSessionSendToServer(Lcom/squareup/cardreader/lcr/CrSecureSessionMessageType;[B)V
.end method
