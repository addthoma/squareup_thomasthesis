.class public interface abstract Lcom/squareup/cardreader/WirelessConnection;
.super Ljava/lang/Object;
.source "WirelessConnection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/WirelessConnection$Factory;
    }
.end annotation


# virtual methods
.method public abstract getAddress()Ljava/lang/String;
.end method

.method public abstract getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getType()I
.end method
