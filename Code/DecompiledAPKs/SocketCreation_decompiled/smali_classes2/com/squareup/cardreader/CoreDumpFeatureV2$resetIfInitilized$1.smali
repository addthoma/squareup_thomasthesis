.class final synthetic Lcom/squareup/cardreader/CoreDumpFeatureV2$resetIfInitilized$1;
.super Lkotlin/jvm/internal/MutablePropertyReference0;
.source "CoreDumpFeatureV2.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/CoreDumpFeatureV2;)V
    .locals 0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/MutablePropertyReference0;-><init>(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureV2$resetIfInitilized$1;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/cardreader/CoreDumpFeatureV2;

    .line 36
    invoke-virtual {v0}, Lcom/squareup/cardreader/CoreDumpFeatureV2;->getFeaturePointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "featurePointer"

    return-object v0
.end method

.method public getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 1

    const-class v0, Lcom/squareup/cardreader/CoreDumpFeatureV2;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    return-object v0
.end method

.method public getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "getFeaturePointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;"

    return-object v0
.end method

.method public set(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureV2$resetIfInitilized$1;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/cardreader/CoreDumpFeatureV2;

    .line 36
    check-cast p1, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CoreDumpFeatureV2;->setFeaturePointer(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)V

    return-void
.end method
