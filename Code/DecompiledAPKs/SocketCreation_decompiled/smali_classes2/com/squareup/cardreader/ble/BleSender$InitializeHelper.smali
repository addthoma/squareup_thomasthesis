.class interface abstract Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;
.super Ljava/lang/Object;
.source "BleSender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/BleSender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "InitializeHelper"
.end annotation


# static fields
.field public static final explodingInitializeHelper:Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 401
    new-instance v0, Lcom/squareup/cardreader/ble/BleSender$InitializeHelper$1;

    invoke-direct {v0}, Lcom/squareup/cardreader/ble/BleSender$InitializeHelper$1;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;->explodingInitializeHelper:Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;

    return-void
.end method


# virtual methods
.method public abstract initialize()Landroid/bluetooth/BluetoothGatt;
.end method

.method public abstract onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V
.end method
