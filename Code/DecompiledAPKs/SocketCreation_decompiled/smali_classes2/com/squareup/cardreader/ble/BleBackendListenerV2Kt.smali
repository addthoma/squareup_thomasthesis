.class public final Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt;
.super Ljava/lang/Object;
.source "BleBackendListenerV2.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0002\u001a\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "CONNECTION_TERMINATED_BY_READER",
        "",
        "decodeErrorType",
        "Lcom/squareup/dipper/events/BleErrorType;",
        "disconnection",
        "Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;",
        "toLegacyState",
        "Lcom/squareup/dipper/events/BleConnectionState;",
        "state",
        "Lcom/squareup/cardreader/ble/R12State;",
        "dipper_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CONNECTION_TERMINATED_BY_READER:I = 0x13


# direct methods
.method public static final synthetic access$decodeErrorType(Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;)Lcom/squareup/dipper/events/BleErrorType;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt;->decodeErrorType(Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;)Lcom/squareup/dipper/events/BleErrorType;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toLegacyState(Lcom/squareup/cardreader/ble/R12State;)Lcom/squareup/dipper/events/BleConnectionState;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt;->toLegacyState(Lcom/squareup/cardreader/ble/R12State;)Lcom/squareup/dipper/events/BleConnectionState;

    move-result-object p0

    return-object p0
.end method

.method private static final decodeErrorType(Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;)Lcom/squareup/dipper/events/BleErrorType;
    .locals 3

    .line 236
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getConnectionError()Lcom/squareup/cardreader/ble/ConnectionError;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/ConnectionError;->getConnectionEvent()Lcom/squareup/cardreader/ble/Error;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    sget-object v2, Lcom/squareup/cardreader/ble/Error;->SERVICE_CHARACTERISTIC_VERSION:Lcom/squareup/cardreader/ble/Error;

    if-ne v0, v2, :cond_1

    .line 237
    sget-object p0, Lcom/squareup/dipper/events/BleErrorType;->SERVICE_VERSION_INCOMPATIBLE:Lcom/squareup/dipper/events/BleErrorType;

    return-object p0

    .line 239
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getBleError()Lcom/squareup/blecoroutines/BleError;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/squareup/blecoroutines/BleError;->getEvent()Lcom/squareup/blecoroutines/Event;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    :goto_1
    sget-object v2, Lcom/squareup/blecoroutines/Event;->TIMEOUT:Lcom/squareup/blecoroutines/Event;

    if-ne v0, v2, :cond_3

    .line 240
    sget-object p0, Lcom/squareup/dipper/events/BleErrorType;->TIMEOUT:Lcom/squareup/dipper/events/BleErrorType;

    return-object p0

    .line 242
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getBleError()Lcom/squareup/blecoroutines/BleError;

    move-result-object p0

    if-eqz p0, :cond_4

    invoke-virtual {p0}, Lcom/squareup/blecoroutines/BleError;->getEvent()Lcom/squareup/blecoroutines/Event;

    move-result-object v1

    :cond_4
    sget-object p0, Lcom/squareup/blecoroutines/Event;->CREATE_BOND_FAILED:Lcom/squareup/blecoroutines/Event;

    if-ne v1, p0, :cond_5

    .line 243
    sget-object p0, Lcom/squareup/dipper/events/BleErrorType;->UNABLE_TO_CREATE_BOND:Lcom/squareup/dipper/events/BleErrorType;

    return-object p0

    .line 245
    :cond_5
    sget-object p0, Lcom/squareup/dipper/events/BleErrorType;->UNKNOWN_ERROR_TYPE:Lcom/squareup/dipper/events/BleErrorType;

    return-object p0
.end method

.method private static final toLegacyState(Lcom/squareup/cardreader/ble/R12State;)Lcom/squareup/dipper/events/BleConnectionState;
    .locals 1

    .line 252
    sget-object v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/R12State;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 268
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :pswitch_0
    sget-object p0, Lcom/squareup/dipper/events/BleConnectionState;->DISCONNECTED:Lcom/squareup/dipper/events/BleConnectionState;

    goto :goto_0

    .line 267
    :pswitch_1
    sget-object p0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_DISCONNECTION:Lcom/squareup/dipper/events/BleConnectionState;

    goto :goto_0

    .line 266
    :pswitch_2
    sget-object p0, Lcom/squareup/dipper/events/BleConnectionState;->READY:Lcom/squareup/dipper/events/BleConnectionState;

    goto :goto_0

    .line 265
    :pswitch_3
    sget-object p0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_DATA_NOTIFICATIONS:Lcom/squareup/dipper/events/BleConnectionState;

    goto :goto_0

    .line 264
    :pswitch_4
    sget-object p0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_MTU_NOTIFICATIONS:Lcom/squareup/dipper/events/BleConnectionState;

    goto :goto_0

    .line 263
    :pswitch_5
    sget-object p0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_COMMS_VERSION:Lcom/squareup/dipper/events/BleConnectionState;

    goto :goto_0

    .line 262
    :pswitch_6
    sget-object p0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_CONNECTION_CONTROL:Lcom/squareup/dipper/events/BleConnectionState;

    goto :goto_0

    .line 261
    :pswitch_7
    sget-object p0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_BOND:Lcom/squareup/dipper/events/BleConnectionState;

    goto :goto_0

    .line 260
    :pswitch_8
    sget-object p0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_REMOVE_BOND:Lcom/squareup/dipper/events/BleConnectionState;

    goto :goto_0

    .line 259
    :pswitch_9
    sget-object p0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_BOND_STATUS_FROM_READER:Lcom/squareup/dipper/events/BleConnectionState;

    goto :goto_0

    .line 258
    :pswitch_a
    sget-object p0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_SERIAL_NUMBER:Lcom/squareup/dipper/events/BleConnectionState;

    goto :goto_0

    .line 257
    :pswitch_b
    sget-object p0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_SERVICE_CHARACTERISTIC_VERSION:Lcom/squareup/dipper/events/BleConnectionState;

    goto :goto_0

    .line 255
    :pswitch_c
    sget-object p0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_SERVICE_DISCOVERY:Lcom/squareup/dipper/events/BleConnectionState;

    goto :goto_0

    .line 254
    :pswitch_d
    sget-object p0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_CONNECTION_TO_READER:Lcom/squareup/dipper/events/BleConnectionState;

    goto :goto_0

    .line 253
    :pswitch_e
    sget-object p0, Lcom/squareup/dipper/events/BleConnectionState;->CREATED:Lcom/squareup/dipper/events/BleConnectionState;

    :goto_0
    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
