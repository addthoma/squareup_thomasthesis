.class Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;
.super Ljava/lang/Object;
.source "BleSender.java"

# interfaces
.implements Lcom/squareup/cardreader/ble/BleSender$GattAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/BleSender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EnableNotifications"
.end annotation


# instance fields
.field final characteristicUuid:Ljava/util/UUID;

.field final serviceUuid:Ljava/util/UUID;

.field final synthetic this$0:Lcom/squareup/cardreader/ble/BleSender;


# direct methods
.method private constructor <init>(Lcom/squareup/cardreader/ble/BleSender;Ljava/util/UUID;Ljava/util/UUID;)V
    .locals 0

    .line 310
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311
    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;->serviceUuid:Ljava/util/UUID;

    .line 312
    iput-object p3, p0, Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;->characteristicUuid:Ljava/util/UUID;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/ble/BleSender;Ljava/util/UUID;Ljava/util/UUID;Lcom/squareup/cardreader/ble/BleSender$1;)V
    .locals 0

    .line 306
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;-><init>(Lcom/squareup/cardreader/ble/BleSender;Ljava/util/UUID;Ljava/util/UUID;)V

    return-void
.end method


# virtual methods
.method public perform()V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 316
    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;->characteristicUuid:Ljava/util/UUID;

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "Starting enable notifications on %s"

    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 317
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;->serviceUuid:Ljava/util/UUID;

    iget-object v4, p0, Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;->characteristicUuid:Ljava/util/UUID;

    .line 318
    invoke-static {v1, v2, v4}, Lcom/squareup/cardreader/ble/BleSender;->access$800(Lcom/squareup/cardreader/ble/BleSender;Ljava/util/UUID;Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v1

    if-nez v1, :cond_0

    .line 321
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;->serviceUuid:Ljava/util/UUID;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;->characteristicUuid:Ljava/util/UUID;

    invoke-static {v0, v1, v2}, Lcom/squareup/cardreader/ble/BleSender;->access$900(Lcom/squareup/cardreader/ble/BleSender;Ljava/util/UUID;Ljava/util/UUID;)V

    return-void

    .line 328
    :cond_0
    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    invoke-static {v2}, Lcom/squareup/cardreader/ble/BleSender;->access$600(Lcom/squareup/cardreader/ble/BleSender;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Landroid/bluetooth/BluetoothGatt;->setCharacteristicNotification(Landroid/bluetooth/BluetoothGattCharacteristic;Z)Z

    move-result v2

    if-nez v2, :cond_1

    .line 329
    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    invoke-static {v2}, Lcom/squareup/cardreader/ble/BleSender;->access$700(Lcom/squareup/cardreader/ble/BleSender;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v2

    sget-object v4, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_SET_NOTIFICATION_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 330
    invoke-static {v4, v1}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->of(Lcom/squareup/cardreader/ble/GattConnectionEventName;Landroid/bluetooth/BluetoothGattCharacteristic;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    .line 331
    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    invoke-static {v2}, Lcom/squareup/cardreader/ble/BleSender;->access$1000(Lcom/squareup/cardreader/ble/BleSender;)Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;

    move-result-object v2

    new-instance v4, Lcom/squareup/cardreader/ble/BleAction$ReconnectDueToGattError;

    new-array v0, v0, [Ljava/lang/Object;

    .line 333
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "Failed initiating notifications on characteristic %s"

    .line 332
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/squareup/cardreader/ble/BleAction$ReconnectDueToGattError;-><init>(Ljava/lang/String;)V

    .line 331
    invoke-interface {v2, v4}, Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    return-void

    .line 342
    :cond_1
    sget-object v2, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CLIENT_CHARACTERISTIC_CONFIG_DESCRIPTOR:Ljava/util/UUID;

    .line 343
    invoke-virtual {v1, v2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getDescriptor(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object v2

    if-nez v2, :cond_2

    .line 346
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleSender;->access$700(Lcom/squareup/cardreader/ble/BleSender;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    sget-object v2, Lcom/squareup/cardreader/ble/GattConnectionEventName;->DESCRIPTOR_GET_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 347
    invoke-static {v2, v1}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->of(Lcom/squareup/cardreader/ble/GattConnectionEventName;Landroid/bluetooth/BluetoothGattCharacteristic;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    return-void

    .line 353
    :cond_2
    sget-object v4, Landroid/bluetooth/BluetoothGattDescriptor;->ENABLE_INDICATION_VALUE:[B

    invoke-virtual {v2, v4}, Landroid/bluetooth/BluetoothGattDescriptor;->setValue([B)Z

    move-result v4

    if-nez v4, :cond_3

    .line 354
    iget-object v4, p0, Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    invoke-static {v4}, Lcom/squareup/cardreader/ble/BleSender;->access$700(Lcom/squareup/cardreader/ble/BleSender;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v4

    invoke-interface {v4}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v4

    sget-object v5, Lcom/squareup/cardreader/ble/GattConnectionEventName;->DESCRIPTOR_SET_VALUE_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 355
    invoke-static {v5, v1, v2}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->of(Lcom/squareup/cardreader/ble/GattConnectionEventName;Landroid/bluetooth/BluetoothGattCharacteristic;Landroid/bluetooth/BluetoothGattDescriptor;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object v1

    invoke-interface {v4, v1}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    .line 356
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    invoke-static {v1}, Lcom/squareup/cardreader/ble/BleSender;->access$1000(Lcom/squareup/cardreader/ble/BleSender;)Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;

    move-result-object v1

    new-instance v4, Lcom/squareup/cardreader/ble/BleAction$ReconnectDueToGattError;

    new-array v0, v0, [Ljava/lang/Object;

    .line 358
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothGattDescriptor;->getCharacteristic()Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v2

    aput-object v2, v0, v3

    const-string v2, "Failed setting value on descriptor for characteristic %s"

    .line 357
    invoke-static {v2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/squareup/cardreader/ble/BleAction$ReconnectDueToGattError;-><init>(Ljava/lang/String;)V

    .line 356
    invoke-interface {v1, v4}, Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    return-void

    .line 364
    :cond_3
    iget-object v4, p0, Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    invoke-static {v4}, Lcom/squareup/cardreader/ble/BleSender;->access$600(Lcom/squareup/cardreader/ble/BleSender;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/bluetooth/BluetoothGatt;->writeDescriptor(Landroid/bluetooth/BluetoothGattDescriptor;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 365
    iget-object v4, p0, Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    invoke-static {v4}, Lcom/squareup/cardreader/ble/BleSender;->access$700(Lcom/squareup/cardreader/ble/BleSender;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v4

    invoke-interface {v4}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v4

    sget-object v5, Lcom/squareup/cardreader/ble/GattConnectionEventName;->DESCRIPTOR_WRITE_INIT_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 366
    invoke-static {v5, v1, v2}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->of(Lcom/squareup/cardreader/ble/GattConnectionEventName;Landroid/bluetooth/BluetoothGattCharacteristic;Landroid/bluetooth/BluetoothGattDescriptor;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object v2

    invoke-interface {v4, v2}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    .line 367
    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleSender$EnableNotifications;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    invoke-static {v2}, Lcom/squareup/cardreader/ble/BleSender;->access$1000(Lcom/squareup/cardreader/ble/BleSender;)Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;

    move-result-object v2

    new-instance v4, Lcom/squareup/cardreader/ble/BleAction$ReconnectDueToGattError;

    new-array v0, v0, [Ljava/lang/Object;

    .line 369
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "Failed initiating write of descriptor on characteristic %s"

    .line 368
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/squareup/cardreader/ble/BleAction$ReconnectDueToGattError;-><init>(Ljava/lang/String;)V

    .line 367
    invoke-interface {v2, v4}, Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    return-void

    :cond_4
    new-array v0, v0, [Ljava/lang/Object;

    .line 373
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "Did set indications on characteristic: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
