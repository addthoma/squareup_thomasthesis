.class public final Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers_Factory;
.super Ljava/lang/Object;
.source "ScopedBluetoothReceivers_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;",
        ">;"
    }
.end annotation


# instance fields
.field private final bleBondingBroadcastReceiverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothDiscoveryBroadcastReceiverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothStatusReceiverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers_Factory;->contextProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers_Factory;->bleBondingBroadcastReceiverProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers_Factory;->bluetoothDiscoveryBroadcastReceiverProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers_Factory;->bluetoothStatusReceiverProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;",
            ">;)",
            "Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers_Factory;"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;)Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;
    .locals 1

    .line 52
    new-instance v0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;-><init>(Landroid/app/Application;Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers_Factory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers_Factory;->bleBondingBroadcastReceiverProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers_Factory;->bluetoothDiscoveryBroadcastReceiverProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;

    iget-object v3, p0, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers_Factory;->bluetoothStatusReceiverProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;)Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers_Factory;->get()Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;

    move-result-object v0

    return-object v0
.end method
