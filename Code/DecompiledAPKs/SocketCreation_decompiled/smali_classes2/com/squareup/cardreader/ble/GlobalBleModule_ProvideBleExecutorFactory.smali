.class public final Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleExecutorFactory;
.super Ljava/lang/Object;
.source "GlobalBleModule_ProvideBleExecutorFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Landroid/os/Handler;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/cardreader/ble/GlobalBleModule;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ble/GlobalBleModule;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleExecutorFactory;->module:Lcom/squareup/cardreader/ble/GlobalBleModule;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/ble/GlobalBleModule;)Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleExecutorFactory;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleExecutorFactory;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleExecutorFactory;-><init>(Lcom/squareup/cardreader/ble/GlobalBleModule;)V

    return-object v0
.end method

.method public static provideBleExecutor(Lcom/squareup/cardreader/ble/GlobalBleModule;)Landroid/os/Handler;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/GlobalBleModule;->provideBleExecutor()Landroid/os/Handler;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/os/Handler;

    return-object p0
.end method


# virtual methods
.method public get()Landroid/os/Handler;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleExecutorFactory;->module:Lcom/squareup/cardreader/ble/GlobalBleModule;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleExecutorFactory;->provideBleExecutor(Lcom/squareup/cardreader/ble/GlobalBleModule;)Landroid/os/Handler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleExecutorFactory;->get()Landroid/os/Handler;

    move-result-object v0

    return-object v0
.end method
