.class public Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BleBondingBroadcastReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver$BleBondSuccessListener;
    }
.end annotation


# static fields
.field private static final BONDING_NONE_REASON:Ljava/lang/String; = "android.bluetooth.device.extra.REASON"


# instance fields
.field private final externalListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver$BleBondSuccessListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 27
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 28
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;->externalListeners:Ljava/util/Set;

    return-void
.end method

.method private static getName(I)Ljava/lang/String;
    .locals 3

    packed-switch p0, :pswitch_data_0

    .line 72
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown int value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const-string p0, "bonded"

    return-object p0

    :pswitch_1
    const-string p0, "bonding"

    return-object p0

    :pswitch_2
    const-string p0, "none"

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public addBondSuccessListener(Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver$BleBondSuccessListener;)V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;->externalListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public destroy(Landroid/content/Context;)V
    .locals 0

    .line 36
    invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public initialize(Landroid/content/Context;)V
    .locals 2

    .line 32
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    const-string p1, "android.bluetooth.device.extra.DEVICE"

    .line 48
    invoke-virtual {p2, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Landroid/bluetooth/BluetoothDevice;

    const/high16 v0, -0x80000000

    const-string v1, "android.bluetooth.device.extra.PREVIOUS_BOND_STATE"

    .line 49
    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "android.bluetooth.device.extra.BOND_STATE"

    .line 50
    invoke-virtual {p2, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    .line 51
    invoke-static {v1}, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;->getName(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    aput-object v1, v2, v3

    .line 52
    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;->getName(I)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    aput-object v1, v2, v4

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x2

    aput-object v1, v2, v5

    const-string v1, "Bonding state changed from %s to %s for device %s"

    .line 51
    invoke-static {v1, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    .line 55
    iget-object p2, p0, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;->externalListeners:Ljava/util/Set;

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver$BleBondSuccessListener;

    .line 56
    invoke-interface {v0, p1}, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver$BleBondSuccessListener;->onBonded(Landroid/bluetooth/BluetoothDevice;)V

    goto :goto_0

    :cond_0
    const/16 p1, 0xa

    if-ne v0, p1, :cond_1

    const-string p1, "android.bluetooth.device.extra.REASON"

    .line 58
    invoke-virtual {p2, p1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-array v0, v4, [Ljava/lang/Object;

    const/4 v1, -0x1

    .line 59
    invoke-virtual {p2, p1, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    const-string p1, "Bonding none reason is: %d"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public removeBondSuccessListener(Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver$BleBondSuccessListener;)V
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;->externalListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method
