.class final Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$7;
.super Ljava/lang/Object;
.source "BleBackendListenerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cardreader/ble/BleBackendListenerV2;->connect()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "rssi",
        "",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Ljava/lang/Integer;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$7;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Integer;)V
    .locals 3

    .line 161
    new-instance v0, Lcom/squareup/dipper/events/DipperEvent$RssiReceived;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$7;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v1}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getBleDevice$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/dipper/events/BleDevice;

    move-result-object v1

    const-string v2, "rssi"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/dipper/events/DipperEvent$RssiReceived;-><init>(Lcom/squareup/dipper/events/BleDevice;I)V

    .line 162
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$7;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {p1}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getCardReaderListeners$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object p1

    check-cast v0, Lcom/squareup/dipper/events/DipperEvent;

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishDipperEvent(Lcom/squareup/dipper/events/DipperEvent;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 60
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$7;->accept(Ljava/lang/Integer;)V

    return-void
.end method
