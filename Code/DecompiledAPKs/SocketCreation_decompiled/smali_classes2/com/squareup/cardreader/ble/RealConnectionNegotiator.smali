.class public final Lcom/squareup/cardreader/ble/RealConnectionNegotiator;
.super Ljava/lang/Object;
.source "ConnectionNegotiator.kt"

# interfaces
.implements Lcom/squareup/cardreader/ble/ConnectionNegotiator;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J9\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000f\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/cardreader/ble/RealConnectionNegotiator;",
        "Lcom/squareup/cardreader/ble/ConnectionNegotiator;",
        "()V",
        "negotiateConnection",
        "Lcom/squareup/cardreader/ble/NegotiatedConnection;",
        "device",
        "Landroid/bluetooth/BluetoothDevice;",
        "events",
        "Lcom/squareup/cardreader/ble/RealConnectionEvents;",
        "connection",
        "Lcom/squareup/blecoroutines/Connection;",
        "context",
        "Landroid/content/Context;",
        "timeouts",
        "Lcom/squareup/cardreader/ble/Timeouts;",
        "(Landroid/bluetooth/BluetoothDevice;Lcom/squareup/cardreader/ble/RealConnectionEvents;Lcom/squareup/blecoroutines/Connection;Landroid/content/Context;Lcom/squareup/cardreader/ble/Timeouts;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public negotiateConnection(Landroid/bluetooth/BluetoothDevice;Lcom/squareup/cardreader/ble/RealConnectionEvents;Lcom/squareup/blecoroutines/Connection;Landroid/content/Context;Lcom/squareup/cardreader/ble/Timeouts;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/bluetooth/BluetoothDevice;",
            "Lcom/squareup/cardreader/ble/RealConnectionEvents;",
            "Lcom/squareup/blecoroutines/Connection;",
            "Landroid/content/Context;",
            "Lcom/squareup/cardreader/ble/Timeouts;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/squareup/cardreader/ble/NegotiatedConnection;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p6

    instance-of v4, v3, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;

    if-eqz v4, :cond_0

    move-object v4, v3

    check-cast v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;

    iget v5, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->label:I

    const/high16 v6, -0x80000000

    and-int/2addr v5, v6

    if-eqz v5, :cond_0

    iget v3, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->label:I

    sub-int/2addr v3, v6

    iput v3, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->label:I

    goto :goto_0

    :cond_0
    new-instance v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;

    invoke-direct {v4, v0, v3}, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;-><init>(Lcom/squareup/cardreader/ble/RealConnectionNegotiator;Lkotlin/coroutines/Continuation;)V

    :goto_0
    iget-object v3, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->result:Ljava/lang/Object;

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v5

    .line 67
    iget v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->label:I

    const/16 v7, 0xc

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x0

    packed-switch v6, :pswitch_data_0

    .line 154
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :pswitch_0
    iget-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$11:Ljava/lang/Object;

    check-cast v1, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$10:Ljava/lang/Object;

    check-cast v2, [B

    iget-object v5, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$9:Ljava/lang/Object;

    check-cast v5, Landroid/bluetooth/BluetoothGattCharacteristic;

    iget-object v5, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$8:Ljava/lang/Object;

    check-cast v5, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    iget-object v5, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$7:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    iget-object v5, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$6:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Integer;

    iget-object v5, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    check-cast v5, Lcom/squareup/cardreader/ble/Timeouts;

    iget-object v5, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    check-cast v5, Landroid/content/Context;

    iget-object v5, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    check-cast v5, Lcom/squareup/blecoroutines/Connection;

    iget-object v5, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    check-cast v5, Lcom/squareup/cardreader/ble/RealConnectionEvents;

    iget-object v5, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    check-cast v5, Landroid/bluetooth/BluetoothDevice;

    iget-object v4, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    check-cast v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator;

    invoke-static {v3}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    move-object v0, v3

    goto/16 :goto_10

    :pswitch_1
    iget-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$10:Ljava/lang/Object;

    check-cast v1, [B

    iget-object v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$9:Ljava/lang/Object;

    check-cast v2, Landroid/bluetooth/BluetoothGattCharacteristic;

    iget-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$8:Ljava/lang/Object;

    check-cast v6, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    iget-object v7, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$7:Ljava/lang/Object;

    check-cast v7, Ljava/lang/String;

    iget-object v8, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$6:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    iget-object v9, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    check-cast v9, Lcom/squareup/cardreader/ble/Timeouts;

    iget-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    check-cast v11, Landroid/content/Context;

    iget-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    check-cast v12, Lcom/squareup/blecoroutines/Connection;

    iget-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    check-cast v13, Lcom/squareup/cardreader/ble/RealConnectionEvents;

    iget-object v14, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    check-cast v14, Landroid/bluetooth/BluetoothDevice;

    iget-object v15, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    check-cast v15, Lcom/squareup/cardreader/ble/RealConnectionNegotiator;

    invoke-static {v3}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    move-object v0, v3

    move-object v3, v1

    move-object v1, v5

    goto/16 :goto_f

    :pswitch_2
    iget-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$9:Ljava/lang/Object;

    check-cast v1, Landroid/bluetooth/BluetoothGattCharacteristic;

    iget-object v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$8:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    iget-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$7:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    iget-object v8, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$6:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    iget-object v9, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    check-cast v9, Lcom/squareup/cardreader/ble/Timeouts;

    iget-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    check-cast v11, Landroid/content/Context;

    iget-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    check-cast v12, Lcom/squareup/blecoroutines/Connection;

    iget-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    check-cast v13, Lcom/squareup/cardreader/ble/RealConnectionEvents;

    iget-object v14, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    check-cast v14, Landroid/bluetooth/BluetoothDevice;

    iget-object v15, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    check-cast v15, Lcom/squareup/cardreader/ble/RealConnectionNegotiator;

    invoke-static {v3}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    move-object v7, v6

    move-object v6, v2

    move-object v2, v1

    goto/16 :goto_e

    :pswitch_3
    iget-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$9:Ljava/lang/Object;

    check-cast v1, Landroid/bluetooth/BluetoothGattCharacteristic;

    iget-object v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$8:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    iget-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$7:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    iget-object v8, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$6:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    iget-object v9, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    check-cast v9, Lcom/squareup/cardreader/ble/Timeouts;

    iget-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    check-cast v11, Landroid/content/Context;

    iget-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    check-cast v12, Lcom/squareup/blecoroutines/Connection;

    iget-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    check-cast v13, Lcom/squareup/cardreader/ble/RealConnectionEvents;

    iget-object v14, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    check-cast v14, Landroid/bluetooth/BluetoothDevice;

    iget-object v15, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    check-cast v15, Lcom/squareup/cardreader/ble/RealConnectionNegotiator;

    invoke-static {v3}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    move-object v0, v2

    goto/16 :goto_d

    :pswitch_4
    iget-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$9:Ljava/lang/Object;

    check-cast v1, Landroid/bluetooth/BluetoothGattCharacteristic;

    iget-object v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$8:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    iget-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$7:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    iget-object v8, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$6:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    iget-object v9, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    check-cast v9, Lcom/squareup/cardreader/ble/Timeouts;

    iget-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    check-cast v11, Landroid/content/Context;

    iget-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    check-cast v12, Lcom/squareup/blecoroutines/Connection;

    iget-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    check-cast v13, Lcom/squareup/cardreader/ble/RealConnectionEvents;

    iget-object v14, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    check-cast v14, Landroid/bluetooth/BluetoothDevice;

    iget-object v15, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    check-cast v15, Lcom/squareup/cardreader/ble/RealConnectionNegotiator;

    invoke-static {v3}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    move-object v0, v2

    goto/16 :goto_c

    :pswitch_5
    iget-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$9:Ljava/lang/Object;

    check-cast v1, Landroid/bluetooth/BluetoothGattCharacteristic;

    iget-object v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$8:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    iget-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$7:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    iget-object v8, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$6:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    iget-object v9, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    check-cast v9, Lcom/squareup/cardreader/ble/Timeouts;

    iget-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    check-cast v11, Landroid/content/Context;

    iget-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    check-cast v12, Lcom/squareup/blecoroutines/Connection;

    iget-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    check-cast v13, Lcom/squareup/cardreader/ble/RealConnectionEvents;

    iget-object v14, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    check-cast v14, Landroid/bluetooth/BluetoothDevice;

    iget-object v15, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    check-cast v15, Lcom/squareup/cardreader/ble/RealConnectionNegotiator;

    invoke-static {v3}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    move-object v0, v2

    goto/16 :goto_b

    :pswitch_6
    iget-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$8:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    iget-object v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$7:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$6:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    iget-object v8, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    check-cast v8, Lcom/squareup/cardreader/ble/Timeouts;

    iget-object v9, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    check-cast v9, Landroid/content/Context;

    iget-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    check-cast v11, Lcom/squareup/blecoroutines/Connection;

    iget-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    check-cast v12, Lcom/squareup/cardreader/ble/RealConnectionEvents;

    iget-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    check-cast v13, Landroid/bluetooth/BluetoothDevice;

    iget-object v14, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    check-cast v14, Lcom/squareup/cardreader/ble/RealConnectionNegotiator;

    invoke-static {v3}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto/16 :goto_9

    :pswitch_7
    iget-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$8:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    iget-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$7:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$6:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    iget-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/cardreader/ble/Timeouts;

    iget-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    check-cast v1, Landroid/content/Context;

    iget-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/blecoroutines/Connection;

    iget-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/cardreader/ble/RealConnectionEvents;

    iget-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    iget-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/cardreader/ble/RealConnectionNegotiator;

    invoke-static {v3}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto/16 :goto_7

    :pswitch_8
    iget v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->I$0:I

    iget-object v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$7:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$6:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    iget-object v8, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    check-cast v8, Lcom/squareup/cardreader/ble/Timeouts;

    iget-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    check-cast v11, Landroid/content/Context;

    iget-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    check-cast v12, Lcom/squareup/blecoroutines/Connection;

    iget-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    check-cast v13, Lcom/squareup/cardreader/ble/RealConnectionEvents;

    iget-object v14, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    check-cast v14, Landroid/bluetooth/BluetoothDevice;

    iget-object v15, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    check-cast v15, Lcom/squareup/cardreader/ble/RealConnectionNegotiator;

    invoke-static {v3}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    move-object v9, v11

    move-object v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    goto/16 :goto_6

    :pswitch_9
    iget-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$7:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$6:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    iget-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    check-cast v6, Lcom/squareup/cardreader/ble/Timeouts;

    iget-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    check-cast v11, Landroid/content/Context;

    iget-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    check-cast v12, Lcom/squareup/blecoroutines/Connection;

    iget-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    check-cast v13, Lcom/squareup/cardreader/ble/RealConnectionEvents;

    iget-object v14, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    check-cast v14, Landroid/bluetooth/BluetoothDevice;

    iget-object v15, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    check-cast v15, Lcom/squareup/cardreader/ble/RealConnectionNegotiator;

    invoke-static {v3}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto/16 :goto_4

    :pswitch_a
    iget-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$6:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    iget-object v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/cardreader/ble/Timeouts;

    iget-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    check-cast v6, Landroid/content/Context;

    iget-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    check-cast v11, Lcom/squareup/blecoroutines/Connection;

    iget-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    check-cast v12, Lcom/squareup/cardreader/ble/RealConnectionEvents;

    iget-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    check-cast v13, Landroid/bluetooth/BluetoothDevice;

    iget-object v14, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    check-cast v14, Lcom/squareup/cardreader/ble/RealConnectionNegotiator;

    invoke-static {v3}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    move-object v15, v14

    move-object v14, v13

    move-object v13, v12

    move-object v12, v11

    move-object v11, v6

    move-object v6, v2

    move-object v2, v1

    goto/16 :goto_3

    :pswitch_b
    iget-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/cardreader/ble/Timeouts;

    iget-object v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    check-cast v2, Landroid/content/Context;

    iget-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    check-cast v6, Lcom/squareup/blecoroutines/Connection;

    iget-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    check-cast v11, Lcom/squareup/cardreader/ble/RealConnectionEvents;

    iget-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    check-cast v12, Landroid/bluetooth/BluetoothDevice;

    iget-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    check-cast v13, Lcom/squareup/cardreader/ble/RealConnectionNegotiator;

    invoke-static {v3}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto/16 :goto_2

    :pswitch_c
    iget-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/cardreader/ble/Timeouts;

    iget-object v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    check-cast v2, Landroid/content/Context;

    iget-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    check-cast v6, Lcom/squareup/blecoroutines/Connection;

    iget-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    check-cast v11, Lcom/squareup/cardreader/ble/RealConnectionEvents;

    iget-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    check-cast v12, Landroid/bluetooth/BluetoothDevice;

    iget-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    check-cast v13, Lcom/squareup/cardreader/ble/RealConnectionNegotiator;

    invoke-static {v3}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_1

    :pswitch_d
    invoke-static {v3}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    .line 74
    sget-object v3, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_SERVICE_DISCOVERY:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v1, v3}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->publishLoggingState$impl_release(Lcom/squareup/cardreader/ble/R12State;)V

    .line 75
    invoke-virtual/range {p5 .. p5}, Lcom/squareup/cardreader/ble/Timeouts;->getDefaultTimeoutMs()J

    move-result-wide v11

    new-instance v3, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$2;

    invoke-direct {v3, v2, v10}, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$2;-><init>(Lcom/squareup/blecoroutines/Connection;Lkotlin/coroutines/Continuation;)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    iput-object v0, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    move-object/from16 v6, p1

    iput-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    iput-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    iput-object v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    move-object/from16 v13, p4

    iput-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    move-object/from16 v14, p5

    iput-object v14, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    iput v8, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->label:I

    invoke-static {v11, v12, v3, v4}, Lkotlinx/coroutines/TimeoutKt;->withTimeout(JLkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v3

    if-ne v3, v5, :cond_1

    return-object v5

    :cond_1
    move-object v11, v1

    move-object v12, v6

    move-object v1, v14

    move-object v6, v2

    move-object v2, v13

    move-object v13, v0

    .line 77
    :goto_1
    sget-object v3, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_SERVICE_CHARACTERISTIC_VERSION:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v11, v3}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->publishLoggingState$impl_release(Lcom/squareup/cardreader/ble/R12State;)V

    .line 78
    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/Timeouts;->getDefaultTimeoutMs()J

    move-result-wide v14

    new-instance v3, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$serviceVersion$1;

    invoke-direct {v3, v6, v10}, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$serviceVersion$1;-><init>(Lcom/squareup/blecoroutines/Connection;Lkotlin/coroutines/Continuation;)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    iput-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    iput-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    iput-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    iput-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    iput-object v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    iput-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    iput v9, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->label:I

    invoke-static {v14, v15, v3, v4}, Lkotlinx/coroutines/TimeoutKt;->withTimeout(JLkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v3

    if-ne v3, v5, :cond_2

    return-object v5

    .line 67
    :cond_2
    :goto_2
    check-cast v3, Ljava/lang/Integer;

    if-eqz v3, :cond_14

    .line 82
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v14

    if-ne v14, v8, :cond_14

    .line 86
    sget-object v14, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_SERIAL_NUMBER:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v11, v14}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->publishLoggingState$impl_release(Lcom/squareup/cardreader/ble/R12State;)V

    .line 87
    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/Timeouts;->getDefaultTimeoutMs()J

    move-result-wide v14

    new-instance v8, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$serialNumber$1;

    invoke-direct {v8, v6, v10}, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$serialNumber$1;-><init>(Lcom/squareup/blecoroutines/Connection;Lkotlin/coroutines/Continuation;)V

    check-cast v8, Lkotlin/jvm/functions/Function2;

    iput-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    iput-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    iput-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    iput-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    iput-object v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    iput-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    iput-object v3, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$6:Ljava/lang/Object;

    const/4 v9, 0x3

    iput v9, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->label:I

    invoke-static {v14, v15, v8, v4}, Lkotlinx/coroutines/TimeoutKt;->withTimeout(JLkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v8

    if-ne v8, v5, :cond_3

    return-object v5

    :cond_3
    move-object v14, v12

    move-object v15, v13

    move-object v12, v6

    move-object v13, v11

    move-object v6, v1

    move-object v11, v2

    move-object v2, v3

    move-object v3, v8

    .line 67
    :goto_3
    move-object v1, v3

    check-cast v1, Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 92
    new-instance v3, Lcom/squareup/cardreader/ble/DeviceInfo$Serial;

    invoke-direct {v3, v1}, Lcom/squareup/cardreader/ble/DeviceInfo$Serial;-><init>(Ljava/lang/String;)V

    check-cast v3, Lcom/squareup/cardreader/ble/DeviceInfo;

    invoke-virtual {v13, v3}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->publishDeviceInfo$impl_release(Lcom/squareup/cardreader/ble/DeviceInfo;)V

    .line 96
    :cond_4
    invoke-virtual {v6}, Lcom/squareup/cardreader/ble/Timeouts;->getSetupBondingDelayMs()J

    move-result-wide v8

    iput-object v15, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    iput-object v14, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    iput-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    iput-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    iput-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    iput-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    iput-object v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$6:Ljava/lang/Object;

    iput-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$7:Ljava/lang/Object;

    const/4 v3, 0x4

    iput v3, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->label:I

    invoke-static {v8, v9, v4}, Lkotlinx/coroutines/DelayKt;->delay(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v3

    if-ne v3, v5, :cond_5

    return-object v5

    .line 98
    :cond_5
    :goto_4
    invoke-virtual {v14}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v3

    if-ne v3, v7, :cond_6

    const/4 v8, 0x1

    goto :goto_5

    :cond_6
    const/4 v8, 0x0

    .line 99
    :goto_5
    sget-object v3, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_BOND_STATUS_FROM_READER:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v13, v3}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->publishLoggingState$impl_release(Lcom/squareup/cardreader/ble/R12State;)V

    move v3, v8

    .line 101
    invoke-virtual {v6}, Lcom/squareup/cardreader/ble/Timeouts;->getDefaultTimeoutMs()J

    move-result-wide v7

    new-instance v9, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$readerBondStatus$1;

    invoke-direct {v9, v12, v10}, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$readerBondStatus$1;-><init>(Lcom/squareup/blecoroutines/Connection;Lkotlin/coroutines/Continuation;)V

    check-cast v9, Lkotlin/jvm/functions/Function2;

    iput-object v15, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    iput-object v14, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    iput-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    iput-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    iput-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    iput-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    iput-object v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$6:Ljava/lang/Object;

    iput-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$7:Ljava/lang/Object;

    iput v3, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->I$0:I

    const/4 v10, 0x5

    iput v10, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->label:I

    invoke-static {v7, v8, v9, v4}, Lkotlinx/coroutines/TimeoutKt;->withTimeout(JLkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v7

    if-ne v7, v5, :cond_7

    return-object v5

    :cond_7
    move-object v8, v6

    move-object v9, v11

    move-object v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object v6, v2

    move-object v2, v1

    move v1, v3

    move-object v3, v7

    .line 100
    :goto_6
    check-cast v3, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    .line 102
    sget-object v7, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;->BONDING_FAILED:Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    if-eq v3, v7, :cond_13

    if-eqz v1, :cond_9

    .line 104
    sget-object v7, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;->BONDED_TO_CONNECTED_PEER:Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    if-eq v3, v7, :cond_9

    .line 107
    sget-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_REMOVE_BOND:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v12, v1}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->publishLoggingState$impl_release(Lcom/squareup/cardreader/ble/R12State;)V

    .line 108
    invoke-virtual {v8}, Lcom/squareup/cardreader/ble/Timeouts;->getBondingTimeoutMs()J

    move-result-wide v0

    iput-object v14, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    iput-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    iput-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    iput-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    iput-object v9, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    iput-object v8, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    iput-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$6:Ljava/lang/Object;

    iput-object v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$7:Ljava/lang/Object;

    iput-object v3, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$8:Ljava/lang/Object;

    const/4 v2, 0x6

    iput v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->label:I

    invoke-static {v13, v0, v1, v9, v4}, Lcom/squareup/blecoroutines/BondingKt;->removeBond(Landroid/bluetooth/BluetoothDevice;JLandroid/content/Context;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, v5, :cond_8

    return-object v5

    .line 112
    :cond_8
    :goto_7
    new-instance v0, Lcom/squareup/cardreader/ble/ConnectionError;

    sget-object v1, Lcom/squareup/cardreader/ble/Error;->REMOVED_BOND:Lcom/squareup/cardreader/ble/Error;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/squareup/cardreader/ble/ConnectionError;-><init>(Lcom/squareup/cardreader/ble/Error;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_9
    if-eqz v1, :cond_b

    .line 115
    sget-object v0, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;->BONDED_TO_CONNECTED_PEER:Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    if-eq v3, v0, :cond_a

    goto :goto_8

    :cond_a
    move-object v15, v14

    move-object v14, v13

    move-object v13, v12

    move-object v12, v11

    move-object v11, v9

    move-object v9, v8

    move-object v8, v6

    move-object v6, v2

    move-object v2, v3

    goto :goto_a

    .line 116
    :cond_b
    :goto_8
    sget-object v0, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_BOND:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v12, v0}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->publishLoggingState$impl_release(Lcom/squareup/cardreader/ble/R12State;)V

    .line 117
    invoke-virtual {v8}, Lcom/squareup/cardreader/ble/Timeouts;->getBondingTimeoutMs()J

    move-result-wide v0

    iput-object v14, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    iput-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    iput-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    iput-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    iput-object v9, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    iput-object v8, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    iput-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$6:Ljava/lang/Object;

    iput-object v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$7:Ljava/lang/Object;

    iput-object v3, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$8:Ljava/lang/Object;

    const/4 v7, 0x7

    iput v7, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->label:I

    invoke-static {v13, v0, v1, v9, v4}, Lcom/squareup/blecoroutines/BondingKt;->createBond(Landroid/bluetooth/BluetoothDevice;JLandroid/content/Context;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, v5, :cond_c

    return-object v5

    :cond_c
    move-object v1, v3

    :goto_9
    move-object v15, v14

    move-object v14, v13

    move-object v13, v12

    move-object v12, v11

    move-object v11, v9

    move-object v9, v8

    move-object v8, v6

    move-object v6, v2

    move-object v2, v1

    .line 120
    :goto_a
    sget-object v0, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_CONNECTION_CONTROL:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v13, v0}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->publishLoggingState$impl_release(Lcom/squareup/cardreader/ble/R12State;)V

    .line 122
    sget-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_LCR_SERVICE:Ljava/util/UUID;

    const-string v1, "UUID_LCR_SERVICE"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_CONNECTION_CONTROL:Ljava/util/UUID;

    const-string v3, "UUID_CHARACTERISTIC_CONNECTION_CONTROL"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v12, v0, v1}, Lcom/squareup/blecoroutines/Connection;->findCharacteristic(Ljava/util/UUID;Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v1

    move-object v0, v2

    .line 123
    invoke-virtual {v9}, Lcom/squareup/cardreader/ble/Timeouts;->getDefaultTimeoutMs()J

    move-result-wide v2

    new-instance v7, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$3;

    const/4 v10, 0x0

    invoke-direct {v7, v12, v1, v10}, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$3;-><init>(Lcom/squareup/blecoroutines/Connection;Landroid/bluetooth/BluetoothGattCharacteristic;Lkotlin/coroutines/Continuation;)V

    check-cast v7, Lkotlin/jvm/functions/Function2;

    iput-object v15, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    iput-object v14, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    iput-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    iput-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    iput-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    iput-object v9, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    iput-object v8, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$6:Ljava/lang/Object;

    iput-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$7:Ljava/lang/Object;

    iput-object v0, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$8:Ljava/lang/Object;

    iput-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$9:Ljava/lang/Object;

    const/16 v10, 0x8

    iput v10, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->label:I

    invoke-static {v2, v3, v7, v4}, Lkotlinx/coroutines/TimeoutKt;->withTimeout(JLkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v2

    if-ne v2, v5, :cond_d

    return-object v5

    .line 126
    :cond_d
    :goto_b
    invoke-virtual {v9}, Lcom/squareup/cardreader/ble/Timeouts;->getDefaultTimeoutMs()J

    move-result-wide v2

    new-instance v7, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$4;

    const/4 v10, 0x0

    invoke-direct {v7, v12, v1, v10}, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$4;-><init>(Lcom/squareup/blecoroutines/Connection;Landroid/bluetooth/BluetoothGattCharacteristic;Lkotlin/coroutines/Continuation;)V

    check-cast v7, Lkotlin/jvm/functions/Function2;

    iput-object v15, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    iput-object v14, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    iput-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    iput-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    iput-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    iput-object v9, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    iput-object v8, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$6:Ljava/lang/Object;

    iput-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$7:Ljava/lang/Object;

    iput-object v0, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$8:Ljava/lang/Object;

    iput-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$9:Ljava/lang/Object;

    const/16 v10, 0x9

    iput v10, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->label:I

    invoke-static {v2, v3, v7, v4}, Lkotlinx/coroutines/TimeoutKt;->withTimeout(JLkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v2

    if-ne v2, v5, :cond_e

    return-object v5

    .line 129
    :cond_e
    :goto_c
    invoke-virtual {v9}, Lcom/squareup/cardreader/ble/Timeouts;->getDefaultTimeoutMs()J

    move-result-wide v2

    new-instance v7, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$5;

    const/4 v10, 0x0

    invoke-direct {v7, v12, v1, v10}, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$5;-><init>(Lcom/squareup/blecoroutines/Connection;Landroid/bluetooth/BluetoothGattCharacteristic;Lkotlin/coroutines/Continuation;)V

    check-cast v7, Lkotlin/jvm/functions/Function2;

    iput-object v15, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    iput-object v14, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    iput-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    iput-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    iput-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    iput-object v9, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    iput-object v8, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$6:Ljava/lang/Object;

    iput-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$7:Ljava/lang/Object;

    iput-object v0, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$8:Ljava/lang/Object;

    iput-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$9:Ljava/lang/Object;

    const/16 v10, 0xa

    iput v10, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->label:I

    invoke-static {v2, v3, v7, v4}, Lkotlinx/coroutines/TimeoutKt;->withTimeout(JLkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v2

    if-ne v2, v5, :cond_f

    return-object v5

    .line 133
    :cond_f
    :goto_d
    sget-object v2, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_COMMS_VERSION:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v13, v2}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->publishLoggingState$impl_release(Lcom/squareup/cardreader/ble/R12State;)V

    .line 134
    invoke-virtual {v9}, Lcom/squareup/cardreader/ble/Timeouts;->getDefaultTimeoutMs()J

    move-result-wide v2

    new-instance v7, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$commsVersion$1;

    const/4 v10, 0x0

    invoke-direct {v7, v12, v10}, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$commsVersion$1;-><init>(Lcom/squareup/blecoroutines/Connection;Lkotlin/coroutines/Continuation;)V

    check-cast v7, Lkotlin/jvm/functions/Function2;

    iput-object v15, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    iput-object v14, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    iput-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    iput-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    iput-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    iput-object v9, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    iput-object v8, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$6:Ljava/lang/Object;

    iput-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$7:Ljava/lang/Object;

    iput-object v0, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$8:Ljava/lang/Object;

    iput-object v1, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$9:Ljava/lang/Object;

    const/16 v10, 0xb

    iput v10, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->label:I

    invoke-static {v2, v3, v7, v4}, Lkotlinx/coroutines/TimeoutKt;->withTimeout(JLkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v3

    if-ne v3, v5, :cond_10

    return-object v5

    :cond_10
    move-object v2, v1

    move-object v7, v6

    move-object v6, v0

    .line 67
    :goto_e
    check-cast v3, [B

    .line 139
    sget-object v0, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_MTU_NOTIFICATIONS:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v13, v0}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->publishLoggingState$impl_release(Lcom/squareup/cardreader/ble/R12State;)V

    .line 140
    invoke-virtual {v9}, Lcom/squareup/cardreader/ble/Timeouts;->getDefaultTimeoutMs()J

    move-result-wide v0

    new-instance v10, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$mtuChannel$1;

    move-object/from16 v16, v5

    const/4 v5, 0x0

    invoke-direct {v10, v12, v5}, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$mtuChannel$1;-><init>(Lcom/squareup/blecoroutines/Connection;Lkotlin/coroutines/Continuation;)V

    check-cast v10, Lkotlin/jvm/functions/Function2;

    iput-object v15, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    iput-object v14, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    iput-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    iput-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    iput-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    iput-object v9, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    iput-object v8, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$6:Ljava/lang/Object;

    iput-object v7, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$7:Ljava/lang/Object;

    iput-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$8:Ljava/lang/Object;

    iput-object v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$9:Ljava/lang/Object;

    iput-object v3, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$10:Ljava/lang/Object;

    const/16 v5, 0xc

    iput v5, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->label:I

    invoke-static {v0, v1, v10, v4}, Lkotlinx/coroutines/TimeoutKt;->withTimeout(JLkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v1, v16

    if-ne v0, v1, :cond_11

    return-object v1

    .line 67
    :cond_11
    :goto_f
    check-cast v0, Lkotlinx/coroutines/channels/ReceiveChannel;

    .line 146
    sget-object v5, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_DATA_NOTIFICATIONS:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v13, v5}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->publishLoggingState$impl_release(Lcom/squareup/cardreader/ble/R12State;)V

    move-object/from16 p1, v0

    move-object/from16 v16, v1

    .line 147
    invoke-virtual {v9}, Lcom/squareup/cardreader/ble/Timeouts;->getDefaultTimeoutMs()J

    move-result-wide v0

    new-instance v5, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$dataChannel$1;

    const/4 v10, 0x0

    invoke-direct {v5, v12, v10}, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$dataChannel$1;-><init>(Lcom/squareup/blecoroutines/Connection;Lkotlin/coroutines/Continuation;)V

    check-cast v5, Lkotlin/jvm/functions/Function2;

    iput-object v15, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$0:Ljava/lang/Object;

    iput-object v14, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$1:Ljava/lang/Object;

    iput-object v13, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$2:Ljava/lang/Object;

    iput-object v12, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$3:Ljava/lang/Object;

    iput-object v11, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$4:Ljava/lang/Object;

    iput-object v9, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$5:Ljava/lang/Object;

    iput-object v8, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$6:Ljava/lang/Object;

    iput-object v7, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$7:Ljava/lang/Object;

    iput-object v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$8:Ljava/lang/Object;

    iput-object v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$9:Ljava/lang/Object;

    iput-object v3, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$10:Ljava/lang/Object;

    move-object/from16 v2, p1

    iput-object v2, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->L$11:Ljava/lang/Object;

    const/16 v6, 0xd

    iput v6, v4, Lcom/squareup/cardreader/ble/RealConnectionNegotiator$negotiateConnection$1;->label:I

    invoke-static {v0, v1, v5, v4}, Lkotlinx/coroutines/TimeoutKt;->withTimeout(JLkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v1, v16

    if-ne v0, v1, :cond_12

    return-object v1

    :cond_12
    move-object v1, v2

    move-object v2, v3

    .line 67
    :goto_10
    check-cast v0, Lkotlinx/coroutines/channels/ReceiveChannel;

    .line 154
    new-instance v3, Lcom/squareup/cardreader/ble/NegotiatedConnection;

    const-string v4, "commsVersion"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v3, v2, v1, v0}, Lcom/squareup/cardreader/ble/NegotiatedConnection;-><init>([BLkotlinx/coroutines/channels/ReceiveChannel;Lkotlinx/coroutines/channels/ReceiveChannel;)V

    return-object v3

    .line 102
    :cond_13
    new-instance v0, Lcom/squareup/cardreader/ble/ConnectionError;

    sget-object v1, Lcom/squareup/cardreader/ble/Error;->BONDING_FAILED:Lcom/squareup/cardreader/ble/Error;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/squareup/cardreader/ble/ConnectionError;-><init>(Lcom/squareup/cardreader/ble/Error;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_14
    move-object v3, v10

    const/4 v2, 0x2

    .line 83
    new-instance v0, Lcom/squareup/cardreader/ble/ConnectionError;

    sget-object v1, Lcom/squareup/cardreader/ble/Error;->SERVICE_CHARACTERISTIC_VERSION:Lcom/squareup/cardreader/ble/Error;

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/squareup/cardreader/ble/ConnectionError;-><init>(Lcom/squareup/cardreader/ble/Error;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
