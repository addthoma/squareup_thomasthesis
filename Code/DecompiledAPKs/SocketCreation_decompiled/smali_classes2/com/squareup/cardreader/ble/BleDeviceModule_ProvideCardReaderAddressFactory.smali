.class public final Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderAddressFactory;
.super Ljava/lang/Object;
.source "BleDeviceModule_ProvideCardReaderAddressFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/cardreader/ble/BleDeviceModule;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ble/BleDeviceModule;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderAddressFactory;->module:Lcom/squareup/cardreader/ble/BleDeviceModule;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/ble/BleDeviceModule;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderAddressFactory;
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderAddressFactory;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderAddressFactory;-><init>(Lcom/squareup/cardreader/ble/BleDeviceModule;)V

    return-object v0
.end method

.method public static provideCardReaderAddress(Lcom/squareup/cardreader/ble/BleDeviceModule;)Ljava/lang/String;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleDeviceModule;->provideCardReaderAddress()Ljava/lang/String;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderAddressFactory;->get()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/lang/String;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderAddressFactory;->module:Lcom/squareup/cardreader/ble/BleDeviceModule;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderAddressFactory;->provideCardReaderAddress(Lcom/squareup/cardreader/ble/BleDeviceModule;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
