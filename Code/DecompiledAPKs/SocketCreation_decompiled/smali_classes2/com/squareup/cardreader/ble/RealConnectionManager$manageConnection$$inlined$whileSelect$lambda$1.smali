.class final Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "ConnectionManager.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cardreader/ble/RealConnectionManager;->manageConnection(Lcom/squareup/blecoroutines/Connection;Lcom/squareup/cardreader/ble/NegotiatedConnection;Lcom/squareup/cardreader/ble/RealConnectionEvents;Lkotlinx/coroutines/channels/Channel;Lcom/squareup/cardreader/ble/Timeouts;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Landroid/bluetooth/BluetoothGattCharacteristic;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Ljava/lang/Boolean;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u008a@\u00a2\u0006\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "characteristic",
        "Landroid/bluetooth/BluetoothGattCharacteristic;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
        "com/squareup/cardreader/ble/RealConnectionManager$manageConnection$2$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $connection$inlined:Lcom/squareup/blecoroutines/Connection;

.field final synthetic $controlCharacteristic$inlined:Landroid/bluetooth/BluetoothGattCharacteristic;

.field final synthetic $dataWriteCharacteristic$inlined:Landroid/bluetooth/BluetoothGattCharacteristic;

.field final synthetic $events$inlined:Lcom/squareup/cardreader/ble/RealConnectionEvents;

.field final synthetic $executionEnv$inlined:Lcom/squareup/cardreader/ble/Timeouts;

.field final synthetic $negotiatedConnection$inlined:Lcom/squareup/cardreader/ble/NegotiatedConnection;

.field final synthetic $sendMessageChannel$inlined:Lkotlinx/coroutines/channels/Channel;

.field label:I

.field private p$0:Landroid/bluetooth/BluetoothGattCharacteristic;


# direct methods
.method constructor <init>(Lkotlin/coroutines/Continuation;Lcom/squareup/blecoroutines/Connection;Lcom/squareup/cardreader/ble/NegotiatedConnection;Lcom/squareup/cardreader/ble/RealConnectionEvents;Lkotlinx/coroutines/channels/Channel;Lcom/squareup/cardreader/ble/Timeouts;Landroid/bluetooth/BluetoothGattCharacteristic;Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 0

    iput-object p2, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;->$connection$inlined:Lcom/squareup/blecoroutines/Connection;

    iput-object p3, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;->$negotiatedConnection$inlined:Lcom/squareup/cardreader/ble/NegotiatedConnection;

    iput-object p4, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;->$events$inlined:Lcom/squareup/cardreader/ble/RealConnectionEvents;

    iput-object p5, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;->$sendMessageChannel$inlined:Lkotlinx/coroutines/channels/Channel;

    iput-object p6, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;->$executionEnv$inlined:Lcom/squareup/cardreader/ble/Timeouts;

    iput-object p7, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;->$dataWriteCharacteristic$inlined:Landroid/bluetooth/BluetoothGattCharacteristic;

    iput-object p8, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;->$controlCharacteristic$inlined:Landroid/bluetooth/BluetoothGattCharacteristic;

    const/4 p2, 0x2

    invoke-direct {p0, p2, p1}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;

    iget-object v3, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;->$connection$inlined:Lcom/squareup/blecoroutines/Connection;

    iget-object v4, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;->$negotiatedConnection$inlined:Lcom/squareup/cardreader/ble/NegotiatedConnection;

    iget-object v5, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;->$events$inlined:Lcom/squareup/cardreader/ble/RealConnectionEvents;

    iget-object v6, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;->$sendMessageChannel$inlined:Lkotlinx/coroutines/channels/Channel;

    iget-object v7, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;->$executionEnv$inlined:Lcom/squareup/cardreader/ble/Timeouts;

    iget-object v8, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;->$dataWriteCharacteristic$inlined:Landroid/bluetooth/BluetoothGattCharacteristic;

    iget-object v9, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;->$controlCharacteristic$inlined:Landroid/bluetooth/BluetoothGattCharacteristic;

    move-object v1, v0

    move-object v2, p2

    invoke-direct/range {v1 .. v9}, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;-><init>(Lkotlin/coroutines/Continuation;Lcom/squareup/blecoroutines/Connection;Lcom/squareup/cardreader/ble/NegotiatedConnection;Lcom/squareup/cardreader/ble/RealConnectionEvents;Lkotlinx/coroutines/channels/Channel;Lcom/squareup/cardreader/ble/Timeouts;Landroid/bluetooth/BluetoothGattCharacteristic;Landroid/bluetooth/BluetoothGattCharacteristic;)V

    check-cast p1, Landroid/bluetooth/BluetoothGattCharacteristic;

    iput-object p1, v0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;->p$0:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    .line 53
    iget v0, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;->label:I

    if-nez v0, :cond_0

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;->p$0:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 54
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealConnectionManager$manageConnection$$inlined$whileSelect$lambda$1;->$events$inlined:Lcom/squareup/cardreader/ble/RealConnectionEvents;

    invoke-static {p1}, Lcom/squareup/blecoroutines/BleErrorKt;->asUint8(Landroid/bluetooth/BluetoothGattCharacteristic;)Ljava/lang/Integer;

    move-result-object p1

    const-string v1, "characteristic.asUint8()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->publishMtu$impl_release(I)V

    const/4 p1, 0x1

    .line 55
    invoke-static {p1}, Lkotlin/coroutines/jvm/internal/Boxing;->boxBoolean(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
