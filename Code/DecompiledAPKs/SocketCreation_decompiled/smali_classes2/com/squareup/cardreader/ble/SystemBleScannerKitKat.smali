.class Lcom/squareup/cardreader/ble/SystemBleScannerKitKat;
.super Ljava/lang/Object;
.source "SystemBleScannerKitKat.java"

# interfaces
.implements Lcom/squareup/cardreader/ble/SystemBleScanner;
.implements Landroid/bluetooth/BluetoothAdapter$LeScanCallback;


# static fields
.field private static final SQUARE_KEY_OFFSET:I = 0x19


# instance fields
.field private final bleScanFilter:Lcom/squareup/cardreader/ble/BleScanFilter;

.field private final bleScanner:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleScanner;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothAdapter:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;


# direct methods
.method constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/ble/BleScanFilter;Lcom/squareup/thread/executor/MainThread;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleScanner;",
            ">;",
            "Lcom/squareup/cardreader/ble/BleScanFilter;",
            "Lcom/squareup/thread/executor/MainThread;",
            ")V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/cardreader/ble/SystemBleScannerKitKat;->bluetoothAdapter:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/squareup/cardreader/ble/SystemBleScannerKitKat;->bleScanner:Ljavax/inject/Provider;

    .line 24
    iput-object p3, p0, Lcom/squareup/cardreader/ble/SystemBleScannerKitKat;->bleScanFilter:Lcom/squareup/cardreader/ble/BleScanFilter;

    .line 25
    iput-object p4, p0, Lcom/squareup/cardreader/ble/SystemBleScannerKitKat;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-void
.end method


# virtual methods
.method public synthetic lambda$onLeScan$0$SystemBleScannerKitKat(Lcom/squareup/cardreader/ble/RealBleScanResult;)V
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/cardreader/ble/SystemBleScannerKitKat;->bleScanner:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ble/BleScanner;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ble/BleScanner;->onBleDeviceFound(Lcom/squareup/cardreader/ble/BleScanResult;)V

    return-void
.end method

.method public onLeScan(Landroid/bluetooth/BluetoothDevice;I[B)V
    .locals 0

    .line 48
    iget-object p2, p0, Lcom/squareup/cardreader/ble/SystemBleScannerKitKat;->bleScanFilter:Lcom/squareup/cardreader/ble/BleScanFilter;

    invoke-virtual {p2, p3}, Lcom/squareup/cardreader/ble/BleScanFilter;->isCompatibleDevice([B)Z

    move-result p2

    if-eqz p2, :cond_1

    const/16 p2, 0x19

    .line 49
    aget-byte p2, p3, p2

    const/4 p3, 0x1

    if-ne p2, p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    .line 50
    :goto_0
    new-instance p2, Lcom/squareup/cardreader/ble/RealBleScanResult;

    invoke-direct {p2, p1, p3}, Lcom/squareup/cardreader/ble/RealBleScanResult;-><init>(Landroid/bluetooth/BluetoothDevice;Z)V

    .line 52
    iget-object p1, p0, Lcom/squareup/cardreader/ble/SystemBleScannerKitKat;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance p3, Lcom/squareup/cardreader/ble/-$$Lambda$SystemBleScannerKitKat$vAdxcHhMzi-nMbUZLb9ZbKzBUN8;

    invoke-direct {p3, p0, p2}, Lcom/squareup/cardreader/ble/-$$Lambda$SystemBleScannerKitKat$vAdxcHhMzi-nMbUZLb9ZbKzBUN8;-><init>(Lcom/squareup/cardreader/ble/SystemBleScannerKitKat;Lcom/squareup/cardreader/ble/RealBleScanResult;)V

    invoke-interface {p1, p3}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method

.method public declared-synchronized startScan()V
    .locals 1

    monitor-enter p0

    .line 29
    :try_start_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/SystemBleScannerKitKat;->bluetoothAdapter:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    .line 32
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/squareup/cardreader/ble/SystemBleScannerKitKat;->bluetoothAdapter:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, p0}, Landroid/bluetooth/BluetoothAdapter;->startLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 35
    iget-object v0, p0, Lcom/squareup/cardreader/ble/SystemBleScannerKitKat;->bleScanner:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ble/BleScanner;

    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/BleScanner;->onScanFailed()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 37
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stopScan()V
    .locals 1

    monitor-enter p0

    .line 41
    :try_start_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/SystemBleScannerKitKat;->bluetoothAdapter:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    .line 44
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/squareup/cardreader/ble/SystemBleScannerKitKat;->bluetoothAdapter:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, p0}, Landroid/bluetooth/BluetoothAdapter;->stopLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 45
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
