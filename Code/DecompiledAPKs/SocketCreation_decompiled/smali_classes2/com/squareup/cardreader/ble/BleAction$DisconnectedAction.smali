.class public Lcom/squareup/cardreader/ble/BleAction$DisconnectedAction;
.super Ljava/lang/Object;
.source "BleAction.java"

# interfaces
.implements Lcom/squareup/cardreader/ble/BleAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/BleAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DisconnectedAction"
.end annotation


# static fields
.field public static final CONNECTION_TERMINATED_BY_READER:I = 0x13


# instance fields
.field public final status:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput p1, p0, Lcom/squareup/cardreader/ble/BleAction$DisconnectedAction;->status:I

    return-void
.end method


# virtual methods
.method public describe()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 53
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleAction$DisconnectedAction;->isReaderForceUnPair()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget v1, p0, Lcom/squareup/cardreader/ble/BleAction$DisconnectedAction;->status:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "Disconnected, force un-pair: %b, status: %d"

    .line 52
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isReaderForceUnPair()Z
    .locals 2

    .line 57
    iget v0, p0, Lcom/squareup/cardreader/ble/BleAction$DisconnectedAction;->status:I

    const/16 v1, 0x13

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
