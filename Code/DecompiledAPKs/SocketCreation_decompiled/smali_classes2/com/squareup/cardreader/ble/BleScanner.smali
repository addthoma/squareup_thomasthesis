.class public Lcom/squareup/cardreader/ble/BleScanner;
.super Ljava/lang/Object;
.source "BleScanner.java"

# interfaces
.implements Lcom/squareup/cardreader/WirelessSearcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/ble/BleScanner$ScanRestarter;
    }
.end annotation


# static fields
.field public static final SCAN_FAILED_RESTART_DELAY:I = 0x5dc


# instance fields
.field private final bleScanFilter:Lcom/squareup/cardreader/ble/BleScanFilter;

.field private bleScanResultsSubject:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "Lcom/squareup/cardreader/ble/BleScanResult;",
            "Lcom/squareup/cardreader/ble/BleScanResult;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final scanRestarter:Lcom/squareup/cardreader/ble/BleScanner$ScanRestarter;

.field private final systemBleScanner:Lcom/squareup/cardreader/ble/SystemBleScanner;


# direct methods
.method public constructor <init>(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/cardreader/ble/SystemBleScanner;Lcom/squareup/cardreader/ble/BleScanFilter;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleScanner;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 29
    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleScanner;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    .line 30
    iput-object p3, p0, Lcom/squareup/cardreader/ble/BleScanner;->systemBleScanner:Lcom/squareup/cardreader/ble/SystemBleScanner;

    .line 31
    iput-object p4, p0, Lcom/squareup/cardreader/ble/BleScanner;->bleScanFilter:Lcom/squareup/cardreader/ble/BleScanFilter;

    .line 32
    new-instance p1, Lcom/squareup/cardreader/ble/BleScanner$ScanRestarter;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lcom/squareup/cardreader/ble/BleScanner$ScanRestarter;-><init>(Lcom/squareup/cardreader/ble/BleScanner;Lcom/squareup/cardreader/ble/BleScanner$1;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleScanner;->scanRestarter:Lcom/squareup/cardreader/ble/BleScanner$ScanRestarter;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/cardreader/ble/BleScanner;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleScanner;->restartSystemBleScanner()V

    return-void
.end method

.method private restartSystemBleScanner()V
    .locals 1

    .line 83
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 84
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleScanner;->systemBleScanner:Lcom/squareup/cardreader/ble/SystemBleScanner;

    invoke-interface {v0}, Lcom/squareup/cardreader/ble/SystemBleScanner;->stopScan()V

    .line 85
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleScanner;->systemBleScanner:Lcom/squareup/cardreader/ble/SystemBleScanner;

    invoke-interface {v0}, Lcom/squareup/cardreader/ble/SystemBleScanner;->startScan()V

    return-void
.end method


# virtual methods
.method public isSearching()Z
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleScanner;->bleScanResultsSubject:Lrx/subjects/Subject;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$startSearch$0$BleScanner(Lcom/squareup/cardreader/ble/BleScanResult;)Ljava/lang/Boolean;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleScanner;->bleScanFilter:Lcom/squareup/cardreader/ble/BleScanFilter;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ble/BleScanFilter;->isCompatibleDevice(Lcom/squareup/cardreader/ble/BleScanResult;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    invoke-interface {p1}, Lcom/squareup/cardreader/ble/BleScanResult;->isLookingToPair()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 58
    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method onBleDeviceFound(Lcom/squareup/cardreader/ble/BleScanResult;)V
    .locals 1

    .line 98
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 99
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleScanner;->isSearching()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleScanner;->bleScanResultsSubject:Lrx/subjects/Subject;

    invoke-virtual {v0, p1}, Lrx/subjects/Subject;->onNext(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method onBleDeviceFound(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/ble/BleScanResult;",
            ">;)V"
        }
    .end annotation

    .line 89
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 90
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleScanner;->isSearching()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ble/BleScanResult;

    .line 92
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleScanner;->bleScanResultsSubject:Lrx/subjects/Subject;

    invoke-virtual {v1, v0}, Lrx/subjects/Subject;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method onScanFailed()V
    .locals 4

    .line 105
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Scanning failed; restarting."

    .line 107
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleScanner;->stopSearch()V

    .line 109
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleScanner;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleScanner;->scanRestarter:Lcom/squareup/cardreader/ble/BleScanner$ScanRestarter;

    const-wide/16 v2, 0x5dc

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public startSearch()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/cardreader/WirelessConnection;",
            ">;"
        }
    .end annotation

    .line 42
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 43
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleScanner;->isSearching()Z

    move-result v0

    if-nez v0, :cond_2

    .line 46
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleScanner;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleScanner;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->supportsBle()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Starting to scan for ble devices."

    .line 52
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 55
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleScanner;->restartSystemBleScanner()V

    .line 56
    invoke-static {}, Lrx/subjects/ReplaySubject;->create()Lrx/subjects/ReplaySubject;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ble/BleScanner;->bleScanResultsSubject:Lrx/subjects/Subject;

    .line 57
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleScanner;->bleScanResultsSubject:Lrx/subjects/Subject;

    new-instance v1, Lcom/squareup/cardreader/ble/-$$Lambda$BleScanner$KZ376G2JxAQQ8xC9cmP1rXgt6ec;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/ble/-$$Lambda$BleScanner$KZ376G2JxAQQ8xC9cmP1rXgt6ec;-><init>(Lcom/squareup/cardreader/ble/BleScanner;)V

    .line 58
    invoke-virtual {v0, v1}, Lrx/subjects/Subject;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/squareup/cardreader/WirelessConnection;

    .line 60
    invoke-virtual {v0, v1}, Lrx/Observable;->cast(Ljava/lang/Class;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/ble/-$$Lambda$RbwlOrpjzKss02xMapKDOvgtTHA;->INSTANCE:Lcom/squareup/cardreader/ble/-$$Lambda$RbwlOrpjzKss02xMapKDOvgtTHA;

    .line 61
    invoke-virtual {v0, v1}, Lrx/Observable;->distinct(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0

    .line 50
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "BLE not supported."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "BLE not enabled."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Scan already started."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public stopSearch()V
    .locals 2

    .line 70
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 71
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleScanner;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleScanner;->scanRestarter:Lcom/squareup/cardreader/ble/BleScanner$ScanRestarter;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 72
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleScanner;->isSearching()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Stopping scanning of ble devices."

    .line 76
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleScanner;->systemBleScanner:Lcom/squareup/cardreader/ble/SystemBleScanner;

    invoke-interface {v0}, Lcom/squareup/cardreader/ble/SystemBleScanner;->stopScan()V

    .line 78
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleScanner;->bleScanResultsSubject:Lrx/subjects/Subject;

    invoke-virtual {v0}, Lrx/subjects/Subject;->onCompleted()V

    const/4 v0, 0x0

    .line 79
    iput-object v0, p0, Lcom/squareup/cardreader/ble/BleScanner;->bleScanResultsSubject:Lrx/subjects/Subject;

    return-void
.end method
