.class public final Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineV2Factory;
.super Ljava/lang/Object;
.source "BleDeviceModule_ProvideBleConnectionStateMachineV2Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/ble/StateMachineV2;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final bleDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final mainDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/cardreader/ble/BleDeviceModule;

.field private final tmnTimingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/TmnTimings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ble/BleDeviceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/BleDeviceModule;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/TmnTimings;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineV2Factory;->module:Lcom/squareup/cardreader/ble/BleDeviceModule;

    .line 39
    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineV2Factory;->applicationProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineV2Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineV2Factory;->mainDispatcherProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineV2Factory;->bleDispatcherProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineV2Factory;->tmnTimingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/ble/BleDeviceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineV2Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/BleDeviceModule;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/TmnTimings;",
            ">;)",
            "Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineV2Factory;"
        }
    .end annotation

    .line 56
    new-instance v7, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineV2Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineV2Factory;-><init>(Lcom/squareup/cardreader/ble/BleDeviceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static provideBleConnectionStateMachineV2(Lcom/squareup/cardreader/ble/BleDeviceModule;Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lkotlinx/coroutines/CoroutineDispatcher;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/tmn/TmnTimings;)Lcom/squareup/cardreader/ble/StateMachineV2;
    .locals 0

    .line 62
    invoke-virtual/range {p0 .. p5}, Lcom/squareup/cardreader/ble/BleDeviceModule;->provideBleConnectionStateMachineV2(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lkotlinx/coroutines/CoroutineDispatcher;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/tmn/TmnTimings;)Lcom/squareup/cardreader/ble/StateMachineV2;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ble/StateMachineV2;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/ble/StateMachineV2;
    .locals 6

    .line 48
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineV2Factory;->module:Lcom/squareup/cardreader/ble/BleDeviceModule;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineV2Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Application;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineV2Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/thread/executor/MainThread;

    iget-object v3, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineV2Factory;->mainDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkotlinx/coroutines/CoroutineDispatcher;

    iget-object v4, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineV2Factory;->bleDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lkotlinx/coroutines/CoroutineDispatcher;

    iget-object v5, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineV2Factory;->tmnTimingsProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/tmn/TmnTimings;

    invoke-static/range {v0 .. v5}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineV2Factory;->provideBleConnectionStateMachineV2(Lcom/squareup/cardreader/ble/BleDeviceModule;Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lkotlinx/coroutines/CoroutineDispatcher;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/tmn/TmnTimings;)Lcom/squareup/cardreader/ble/StateMachineV2;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleConnectionStateMachineV2Factory;->get()Lcom/squareup/cardreader/ble/StateMachineV2;

    move-result-object v0

    return-object v0
.end method
