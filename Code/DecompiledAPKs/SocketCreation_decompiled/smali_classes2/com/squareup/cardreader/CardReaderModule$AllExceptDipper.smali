.class public abstract Lcom/squareup/cardreader/CardReaderModule$AllExceptDipper;
.super Ljava/lang/Object;
.source "CardReaderModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "AllExceptDipper"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCardReaderListener(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 84
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInitializer;->getInternalListener()Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;

    move-result-object p0

    return-object p0
.end method
