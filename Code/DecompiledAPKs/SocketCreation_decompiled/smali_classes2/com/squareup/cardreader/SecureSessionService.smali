.class public interface abstract Lcom/squareup/cardreader/SecureSessionService;
.super Ljava/lang/Object;
.source "SecureSessionService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0012\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004H\'\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/cardreader/SecureSessionService;",
        "",
        "sendSecureSessionContents",
        "Lcom/squareup/server/AcceptedResponse;",
        "",
        "message",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract sendSecureSessionContents([B)Lcom/squareup/server/AcceptedResponse;
    .param p1    # [B
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Lcom/squareup/server/AcceptedResponse<",
            "[B>;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/flipper/session/validate"
    .end annotation
.end method
