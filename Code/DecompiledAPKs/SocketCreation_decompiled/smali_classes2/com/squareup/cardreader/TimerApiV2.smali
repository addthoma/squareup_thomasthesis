.class public final Lcom/squareup/cardreader/TimerApiV2;
.super Ljava/lang/Object;
.source "TimerApiV2.kt"

# interfaces
.implements Lcom/squareup/cardreader/TimerPointerProvider;
.implements Lcom/squareup/cardreader/CanReset;
.implements Lcom/squareup/cardreader/TimerApi;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;,
        Lcom/squareup/cardreader/TimerApiV2$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTimerApiV2.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TimerApiV2.kt\ncom/squareup/cardreader/TimerApiV2\n*L\n1#1,79:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0008\u0018\u0000 \u001b2\u00020\u00012\u00020\u00022\u00020\u0003:\u0002\u001b\u001cB\u0005\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u000f\u001a\u00020\u00102\n\u0010\u0007\u001a\u00060\u000eR\u00020\u0000H\u0002J\u0006\u0010\u0011\u001a\u00020\u0010J\u0008\u0010\u0012\u001a\u00020\u0010H\u0016J \u0010\u0013\u001a\u00020\r2\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\r2\u0006\u0010\u0017\u001a\u00020\rH\u0016J\u0010\u0010\u0018\u001a\u00020\u00102\u0006\u0010\u0019\u001a\u00020\rH\u0016J\u0008\u0010\u001a\u001a\u00020\nH\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u000b\u001a\u0012\u0012\u0004\u0012\u00020\r\u0012\u0008\u0012\u00060\u000eR\u00020\u00000\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/cardreader/TimerApiV2;",
        "Lcom/squareup/cardreader/TimerPointerProvider;",
        "Lcom/squareup/cardreader/CanReset;",
        "Lcom/squareup/cardreader/TimerApi;",
        "()V",
        "stopped",
        "",
        "timer",
        "Ljava/util/Timer;",
        "timerApi",
        "Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;",
        "timers",
        "",
        "",
        "Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;",
        "execute",
        "",
        "initialize",
        "resetIfInitilized",
        "startTimer",
        "delayInMillis",
        "",
        "methodPtr",
        "contextPtr",
        "stopTimer",
        "stopTimerId",
        "timerPointer",
        "Companion",
        "ExpiredTimer",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/cardreader/TimerApiV2$Companion;

.field private static timerIdCounter:J


# instance fields
.field private stopped:Z

.field private final timer:Ljava/util/Timer;

.field private timerApi:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;

.field private final timers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/cardreader/TimerApiV2$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/TimerApiV2$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/cardreader/TimerApiV2;->Companion:Lcom/squareup/cardreader/TimerApiV2$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/squareup/cardreader/TimerApiV2;->timer:Ljava/util/Timer;

    .line 12
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/squareup/cardreader/TimerApiV2;->timers:Ljava/util/Map;

    return-void
.end method

.method public static final synthetic access$execute(Lcom/squareup/cardreader/TimerApiV2;Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;)V
    .locals 0

    .line 9
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/TimerApiV2;->execute(Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;)V

    return-void
.end method

.method private final execute(Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;)V
    .locals 7

    .line 52
    iget-boolean v0, p0, Lcom/squareup/cardreader/TimerApiV2;->stopped:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/TimerApiV2;->timers:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;->getTimerId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 54
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;->getMethodPtr()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;->getContextPtr()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;->getTimerId()J

    move-result-wide v5

    invoke-static/range {v1 .. v6}, Lcom/squareup/cardreader/lcr/TimerNative;->on_timer_expired(JJJ)V

    .line 55
    iget-object v0, p0, Lcom/squareup/cardreader/TimerApiV2;->timers:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;->getTimerId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public final initialize()V
    .locals 3

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "initialize"

    .line 19
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 20
    iput-boolean v0, p0, Lcom/squareup/cardreader/TimerApiV2;->stopped:Z

    .line 21
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/TimerNative;->initialize_timer_api(Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;

    move-result-object v0

    const-string v1, "TimerNative.initialize_timer_api(this)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/cardreader/TimerApiV2;->timerApi:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;

    return-void
.end method

.method public declared-synchronized resetIfInitilized()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    .line 25
    :try_start_0
    iput-boolean v0, p0, Lcom/squareup/cardreader/TimerApiV2;->stopped:Z

    .line 26
    iget-object v0, p0, Lcom/squareup/cardreader/TimerApiV2;->timers:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized startTimer(IJJ)J
    .locals 9

    monitor-enter p0

    .line 35
    :try_start_0
    new-instance v8, Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;

    sget-wide v0, Lcom/squareup/cardreader/TimerApiV2;->timerIdCounter:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    sput-wide v0, Lcom/squareup/cardreader/TimerApiV2;->timerIdCounter:J

    sget-wide v2, Lcom/squareup/cardreader/TimerApiV2;->timerIdCounter:J

    move-object v0, v8

    move-object v1, p0

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v0 .. v7}, Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;-><init>(Lcom/squareup/cardreader/TimerApiV2;JJJ)V

    .line 36
    iget-object p2, p0, Lcom/squareup/cardreader/TimerApiV2;->timer:Ljava/util/Timer;

    move-object p3, v8

    check-cast p3, Ljava/util/TimerTask;

    int-to-long p4, p1

    invoke-virtual {p2, p3, p4, p5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 38
    iget-object p1, p0, Lcom/squareup/cardreader/TimerApiV2;->timers:Ljava/util/Map;

    invoke-virtual {v8}, Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;->getTimerId()J

    move-result-wide p2

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {p1, p2, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    invoke-virtual {v8}, Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;->getTimerId()J

    move-result-wide p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public stopTimer(J)V
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/squareup/cardreader/TimerApiV2;->timers:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;

    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {v0}, Lcom/squareup/cardreader/TimerApiV2$ExpiredTimer;->cancel()Z

    return-void

    .line 44
    :cond_0
    sget-object v0, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    aput-object p1, v0, v1

    array-length p1, v0

    invoke-static {v0, p1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    const-string p2, "No timer with id %d running!"

    invoke-static {p2, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "java.lang.String.format(format, *args)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public timerPointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;
    .locals 2

    .line 16
    iget-object v0, p0, Lcom/squareup/cardreader/TimerApiV2;->timerApi:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;

    if-nez v0, :cond_0

    const-string v1, "timerApi"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method
