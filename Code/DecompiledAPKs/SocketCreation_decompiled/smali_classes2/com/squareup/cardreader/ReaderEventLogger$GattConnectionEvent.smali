.class public Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;
.super Ljava/lang/Object;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderEventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GattConnectionEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;
    }
.end annotation


# static fields
.field public static final IGNORE:I = -0x1


# instance fields
.field public final bleReceiverAlreadyDestroyed:Z

.field public final callbackName:Ljava/lang/String;

.field public characteristicUuid:Ljava/util/UUID;

.field public descriptorUuid:Ljava/util/UUID;

.field public final mtu:I

.field public final name:Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public final newState:I

.field public serviceUuid:Ljava/util/UUID;

.field public final status:I


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;)V
    .locals 1

    .line 331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 332
    iget-object v0, p1, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->name:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    iput-object v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->name:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 333
    iget-object v0, p1, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->serviceUuid:Ljava/util/UUID;

    iput-object v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->serviceUuid:Ljava/util/UUID;

    .line 334
    iget-object v0, p1, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->characteristicUuid:Ljava/util/UUID;

    iput-object v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->characteristicUuid:Ljava/util/UUID;

    .line 335
    iget-object v0, p1, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->descriptorUuid:Ljava/util/UUID;

    iput-object v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->descriptorUuid:Ljava/util/UUID;

    .line 336
    iget v0, p1, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->newState:I

    iput v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->newState:I

    .line 337
    iget v0, p1, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->mtu:I

    iput v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->mtu:I

    .line 338
    iget v0, p1, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->status:I

    iput v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->status:I

    .line 339
    iget-object v0, p1, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->callbackName:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->callbackName:Ljava/lang/String;

    .line 340
    iget-boolean p1, p1, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->bleReceiverAlreadyDestroyed:Z

    iput-boolean p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->bleReceiverAlreadyDestroyed:Z

    return-void
.end method

.method public static of(Lcom/squareup/cardreader/ble/GattConnectionEventName;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;
    .locals 1

    .line 344
    new-instance v0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;-><init>(Lcom/squareup/cardreader/ble/GattConnectionEventName;)V

    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->build()Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object p0

    return-object p0
.end method

.method public static of(Lcom/squareup/cardreader/ble/GattConnectionEventName;I)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;
    .locals 1

    .line 348
    new-instance v0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;-><init>(Lcom/squareup/cardreader/ble/GattConnectionEventName;)V

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->status(I)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->build()Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object p0

    return-object p0
.end method

.method public static of(Lcom/squareup/cardreader/ble/GattConnectionEventName;Landroid/bluetooth/BluetoothGattCharacteristic;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;
    .locals 1

    .line 360
    new-instance v0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;-><init>(Lcom/squareup/cardreader/ble/GattConnectionEventName;)V

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->characteristic(Landroid/bluetooth/BluetoothGattCharacteristic;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->build()Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object p0

    return-object p0
.end method

.method public static of(Lcom/squareup/cardreader/ble/GattConnectionEventName;Landroid/bluetooth/BluetoothGattCharacteristic;I)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;
    .locals 1

    .line 365
    new-instance v0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;-><init>(Lcom/squareup/cardreader/ble/GattConnectionEventName;)V

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->characteristic(Landroid/bluetooth/BluetoothGattCharacteristic;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    move-result-object p0

    invoke-virtual {p0, p2}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->status(I)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->build()Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object p0

    return-object p0
.end method

.method public static of(Lcom/squareup/cardreader/ble/GattConnectionEventName;Landroid/bluetooth/BluetoothGattCharacteristic;Landroid/bluetooth/BluetoothGattDescriptor;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;
    .locals 1

    .line 370
    new-instance v0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;-><init>(Lcom/squareup/cardreader/ble/GattConnectionEventName;)V

    .line 371
    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->characteristic(Landroid/bluetooth/BluetoothGattCharacteristic;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    move-result-object p0

    .line 372
    invoke-virtual {p0, p2}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->descriptor(Landroid/bluetooth/BluetoothGattDescriptor;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    move-result-object p0

    .line 373
    invoke-virtual {p0}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->build()Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object p0

    return-object p0
.end method

.method public static of(Lcom/squareup/cardreader/ble/GattConnectionEventName;Landroid/bluetooth/BluetoothGattDescriptor;I)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;
    .locals 1

    .line 378
    new-instance v0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;-><init>(Lcom/squareup/cardreader/ble/GattConnectionEventName;)V

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattDescriptor;->getCharacteristic()Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->characteristic(Landroid/bluetooth/BluetoothGattCharacteristic;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    move-result-object p0

    .line 379
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->descriptor(Landroid/bluetooth/BluetoothGattDescriptor;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    move-result-object p0

    .line 380
    invoke-virtual {p0, p2}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->status(I)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    move-result-object p0

    .line 381
    invoke-virtual {p0}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->build()Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object p0

    return-object p0
.end method

.method public static of(Lcom/squareup/cardreader/ble/GattConnectionEventName;Ljava/util/UUID;Ljava/util/UUID;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;
    .locals 1

    .line 353
    new-instance v0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;-><init>(Lcom/squareup/cardreader/ble/GattConnectionEventName;)V

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->serviceUuid(Ljava/util/UUID;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    move-result-object p0

    .line 354
    invoke-virtual {p0, p2}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->characteristicUuid(Ljava/util/UUID;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    move-result-object p0

    .line 355
    invoke-virtual {p0}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->build()Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public describe()Ljava/lang/String;
    .locals 3

    .line 385
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 387
    iget v1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->status:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    if-eqz v1, :cond_0

    const-string v1, "[ERROR] "

    .line 388
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 391
    :cond_0
    iget-object v1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->name:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/GattConnectionEventName;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 393
    iget-object v1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->serviceUuid:Ljava/util/UUID;

    if-eqz v1, :cond_1

    const-string v1, ", service: "

    .line 394
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 395
    iget-object v1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->serviceUuid:Ljava/util/UUID;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 397
    :cond_1
    iget-object v1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->characteristicUuid:Ljava/util/UUID;

    if-eqz v1, :cond_2

    const-string v1, ", characteristic: "

    .line 398
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    iget-object v1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->characteristicUuid:Ljava/util/UUID;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 401
    :cond_2
    iget-object v1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->descriptorUuid:Ljava/util/UUID;

    if-eqz v1, :cond_3

    const-string v1, ", descriptor: "

    .line 402
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 403
    iget-object v1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->descriptorUuid:Ljava/util/UUID;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 405
    :cond_3
    iget v1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->mtu:I

    if-eq v1, v2, :cond_4

    const-string v1, ", mtu: "

    .line 406
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 407
    iget v1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->mtu:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 409
    :cond_4
    iget v1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->newState:I

    if-eq v1, v2, :cond_5

    const-string v1, ", new state: "

    .line 410
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 411
    iget v1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->newState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 413
    :cond_5
    iget v1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->status:I

    if-eq v1, v2, :cond_6

    const-string v1, ", status: "

    .line 414
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 415
    iget v1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->status:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 417
    :cond_6
    iget-object v1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->callbackName:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", callbackName: "

    .line 418
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 419
    iget-object v1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->callbackName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 421
    :cond_7
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
