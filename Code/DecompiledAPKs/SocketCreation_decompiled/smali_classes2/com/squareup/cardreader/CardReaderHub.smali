.class public Lcom/squareup/cardreader/CardReaderHub;
.super Ljava/lang/Object;
.source "CardReaderHub.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/CardReaderHub$WirelessCardReaderRssiListener;,
        Lcom/squareup/cardreader/CardReaderHub$CardReaderInfoListener;,
        Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;,
        Lcom/squareup/cardreader/CardReaderHub$InternalInfoListener;
    }
.end annotation


# instance fields
.field private final allReaderInfos:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/util/Collection<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private final attachListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderConnectionEvents:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/dipper/events/CardReaderConnectionEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final contexts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/cardreader/CardReaderId;",
            "Lcom/squareup/cardreader/CardReaderContext;",
            ">;"
        }
    .end annotation
.end field

.field private final infoListener:Lcom/squareup/cardreader/CardReaderHub$InternalInfoListener;

.field private final infoListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/cardreader/CardReaderHub$CardReaderInfoListener;",
            ">;"
        }
    .end annotation
.end field

.field private final perReaderInfos:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/cardreader/CardReaderId;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Lcom/squareup/cardreader/CardReaderHub$InternalInfoListener;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/CardReaderHub$InternalInfoListener;-><init>(Lcom/squareup/cardreader/CardReaderHub;)V

    iput-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->infoListener:Lcom/squareup/cardreader/CardReaderHub$InternalInfoListener;

    .line 38
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->contexts:Ljava/util/Map;

    .line 39
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->attachListeners:Ljava/util/Set;

    .line 40
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->infoListeners:Ljava/util/Set;

    .line 42
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 43
    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->allReaderInfos:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 44
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->perReaderInfos:Ljava/util/Map;

    .line 45
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->cardReaderConnectionEvents:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cardreader/CardReaderHub;)Ljava/util/Set;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderHub;->infoListeners:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/cardreader/CardReaderHub;)Ljava/util/Map;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderHub;->perReaderInfos:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/cardreader/CardReaderHub;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderHub;->allReaderInfos:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method


# virtual methods
.method addCardReader(Lcom/squareup/cardreader/CardReaderContext;)V
    .locals 3

    .line 65
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 67
    iget-object v0, p1, Lcom/squareup/cardreader/CardReaderContext;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    .line 68
    iget-object v1, p1, Lcom/squareup/cardreader/CardReaderContext;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderHub;->infoListener:Lcom/squareup/cardreader/CardReaderHub$InternalInfoListener;

    invoke-virtual {v1, v2}, Lcom/squareup/cardreader/CardReaderInfo;->setCallback(Lcom/squareup/cardreader/CardReaderInfo$Callback;)V

    .line 69
    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderHub;->contexts:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderHub;->perReaderInfos:Ljava/util/Map;

    iget-object v2, p1, Lcom/squareup/cardreader/CardReaderContext;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->allReaderInfos:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderHub;->perReaderInfos:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->attachListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;

    .line 75
    iget-object v2, p1, Lcom/squareup/cardreader/CardReaderContext;->cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v1, v2}, Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;->onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V

    goto :goto_0

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->cardReaderConnectionEvents:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/dipper/events/CardReaderConnectionEvent$CardReaderConnected;

    iget-object p1, p1, Lcom/squareup/cardreader/CardReaderContext;->cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-direct {v1, p1}, Lcom/squareup/dipper/events/CardReaderConnectionEvent$CardReaderConnected;-><init>(Lcom/squareup/cardreader/CardReader;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public addCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->attachListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addCardReaderInfoListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderInfoListener;)V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->infoListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public cardReaderConnectionEvents()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/dipper/events/CardReaderConnectionEvent;",
            ">;"
        }
    .end annotation

    .line 153
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->cardReaderConnectionEvents:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public cardReaderInfos()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/Collection<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;>;"
        }
    .end annotation

    .line 149
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->allReaderInfos:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->contexts:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/CardReaderContext;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 144
    :cond_0
    iget-object p1, p1, Lcom/squareup/cardreader/CardReaderContext;->cardReader:Lcom/squareup/cardreader/CardReader;

    :goto_0
    return-object p1
.end method

.method public getCardReaders()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/squareup/cardreader/CardReader;",
            ">;"
        }
    .end annotation

    .line 132
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderHub;->contexts:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 134
    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderHub;->contexts:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/CardReaderContext;

    .line 135
    iget-object v2, v2, Lcom/squareup/cardreader/CardReaderContext;->cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 138
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method getContext(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReaderContext;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->contexts:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/CardReaderContext;

    return-object p1
.end method

.method getContexts()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/squareup/cardreader/CardReaderContext;",
            ">;"
        }
    .end annotation

    .line 110
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->contexts:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method getInfoListener()Lcom/squareup/cardreader/CardReaderHub$InternalInfoListener;
    .locals 1

    .line 169
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->infoListener:Lcom/squareup/cardreader/CardReaderHub$InternalInfoListener;

    return-object v0
.end method

.method public hasCardReader()Z
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->contexts:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasCardReader(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Z
    .locals 2

    .line 118
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->contexts:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderContext;

    .line 119
    iget-object v1, v1, Lcom/squareup/cardreader/CardReaderContext;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v1

    if-ne v1, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method hasCardReaderContext(Lcom/squareup/cardreader/CardReaderContext;)Z
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->contexts:Ljava/util/Map;

    iget-object p1, p1, Lcom/squareup/cardreader/CardReaderContext;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method removeCardReader(Lcom/squareup/cardreader/CardReader;)V
    .locals 3

    .line 82
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 85
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->contexts:Ljava/util/Map;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->contexts:Ljava/util/Map;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderContext;

    .line 88
    iget-object v1, v0, Lcom/squareup/cardreader/CardReaderContext;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/squareup/cardreader/CardReaderInfo;->setCallback(Lcom/squareup/cardreader/CardReaderInfo$Callback;)V

    .line 90
    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderHub;->attachListeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;

    .line 91
    invoke-interface {v2, p1}, Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;->onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V

    goto :goto_0

    .line 94
    :cond_1
    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderHub;->cardReaderConnectionEvents:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v2, Lcom/squareup/dipper/events/CardReaderConnectionEvent$CardReaderDisconnected;

    invoke-direct {v2, p1}, Lcom/squareup/dipper/events/CardReaderConnectionEvent$CardReaderDisconnected;-><init>(Lcom/squareup/cardreader/CardReader;)V

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 97
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderHub;->perReaderInfos:Ljava/util/Map;

    iget-object v0, v0, Lcom/squareup/cardreader/CardReaderContext;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderHub;->allReaderInfos:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->perReaderInfos:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public removeCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->attachListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public removeCardReaderInfoListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderInfoListener;)V
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->infoListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public size()I
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderHub;->contexts:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method
