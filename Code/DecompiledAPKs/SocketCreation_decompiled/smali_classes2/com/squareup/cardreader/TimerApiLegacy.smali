.class public Lcom/squareup/cardreader/TimerApiLegacy;
.super Ljava/lang/Object;
.source "TimerApiLegacy.java"

# interfaces
.implements Lcom/squareup/cardreader/TimerApi;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/TimerApiLegacy$TimerConfig;,
        Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;
    }
.end annotation


# static fields
.field private static timerIdCounter:J


# instance fields
.field private final lcrExecutor:Ljava/util/concurrent/ExecutorService;

.field private stopped:Z

.field private final timer:Ljava/util/Timer;

.field private final timerNative:Lcom/squareup/cardreader/lcr/TimerNativeInterface;

.field private final timers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Lcom/squareup/cardreader/lcr/TimerNativeInterface;)V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/cardreader/TimerApiLegacy;->lcrExecutor:Ljava/util/concurrent/ExecutorService;

    .line 25
    iput-object p2, p0, Lcom/squareup/cardreader/TimerApiLegacy;->timerNative:Lcom/squareup/cardreader/lcr/TimerNativeInterface;

    .line 26
    new-instance p1, Ljava/util/Timer;

    invoke-direct {p1}, Ljava/util/Timer;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/TimerApiLegacy;->timer:Ljava/util/Timer;

    .line 27
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/TimerApiLegacy;->timers:Ljava/util/Map;

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/cardreader/TimerApiLegacy;)Ljava/util/concurrent/ExecutorService;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/squareup/cardreader/TimerApiLegacy;->lcrExecutor:Ljava/util/concurrent/ExecutorService;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/cardreader/TimerApiLegacy;Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;)V
    .locals 0

    .line 12
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/TimerApiLegacy;->execute(Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;)V

    return-void
.end method

.method private declared-synchronized execute(Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;)V
    .locals 8

    monitor-enter p0

    .line 62
    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/cardreader/TimerApiLegacy;->stopped:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/TimerApiLegacy;->timers:Ljava/util/Map;

    invoke-static {p1}, Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;->access$100(Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 64
    :cond_0
    iget-object v1, p0, Lcom/squareup/cardreader/TimerApiLegacy;->timerNative:Lcom/squareup/cardreader/lcr/TimerNativeInterface;

    invoke-static {p1}, Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;->access$200(Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;)J

    move-result-wide v2

    invoke-static {p1}, Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;->access$300(Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;)J

    move-result-wide v4

    invoke-static {p1}, Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;->access$100(Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;)J

    move-result-wide v6

    invoke-interface/range {v1 .. v7}, Lcom/squareup/cardreader/lcr/TimerNativeInterface;->on_timer_expired(JJJ)V

    .line 65
    iget-object v0, p0, Lcom/squareup/cardreader/TimerApiLegacy;->timers:Ljava/util/Map;

    invoke-static {p1}, Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;->access$100(Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    monitor-exit p0

    return-void

    .line 62
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method public declared-synchronized cancelTimers()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    .line 36
    :try_start_0
    iput-boolean v0, p0, Lcom/squareup/cardreader/TimerApiLegacy;->stopped:Z

    .line 37
    iget-object v0, p0, Lcom/squareup/cardreader/TimerApiLegacy;->timers:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public initialize()Lcom/squareup/cardreader/TimerApiLegacy$TimerConfig;
    .locals 2

    const/4 v0, 0x0

    .line 31
    iput-boolean v0, p0, Lcom/squareup/cardreader/TimerApiLegacy;->stopped:Z

    .line 32
    new-instance v0, Lcom/squareup/cardreader/TimerApiLegacy$TimerConfig;

    iget-object v1, p0, Lcom/squareup/cardreader/TimerApiLegacy;->timerNative:Lcom/squareup/cardreader/lcr/TimerNativeInterface;

    invoke-interface {v1, p0}, Lcom/squareup/cardreader/lcr/TimerNativeInterface;->initialize_timer_api(Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/cardreader/TimerApiLegacy$TimerConfig;-><init>(Lcom/squareup/cardreader/TimerApiLegacy;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;)V

    return-object v0
.end method

.method public declared-synchronized startTimer(IJJ)J
    .locals 10

    monitor-enter p0

    .line 42
    :try_start_0
    new-instance v9, Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;

    sget-wide v0, Lcom/squareup/cardreader/TimerApiLegacy;->timerIdCounter:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    sput-wide v2, Lcom/squareup/cardreader/TimerApiLegacy;->timerIdCounter:J

    const/4 v8, 0x0

    move-object v0, v9

    move-object v1, p0

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v0 .. v8}, Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;-><init>(Lcom/squareup/cardreader/TimerApiLegacy;JJJLcom/squareup/cardreader/TimerApiLegacy$1;)V

    .line 43
    iget-object p2, p0, Lcom/squareup/cardreader/TimerApiLegacy;->timer:Ljava/util/Timer;

    int-to-long p3, p1

    invoke-virtual {p2, v9, p3, p4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    const-string p1, "Starting timerId %d"

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    const/4 p3, 0x0

    .line 45
    invoke-static {v9}, Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;->access$100(Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;)J

    move-result-wide p4

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p4

    aput-object p4, p2, p3

    invoke-static {p1, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 46
    iget-object p1, p0, Lcom/squareup/cardreader/TimerApiLegacy;->timers:Ljava/util/Map;

    invoke-static {v9}, Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;->access$100(Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;)J

    move-result-wide p2

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {p1, p2, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    invoke-static {v9}, Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;->access$100(Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;)J

    move-result-wide p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized stopTimer(J)V
    .locals 4

    monitor-enter p0

    .line 52
    :try_start_0
    iget-object v0, p0, Lcom/squareup/cardreader/TimerApiLegacy;->timers:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;

    if-eqz v0, :cond_0

    .line 57
    invoke-virtual {v0}, Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;->cancel()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    monitor-exit p0

    return-void

    .line 54
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No timer with id %d running!"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
