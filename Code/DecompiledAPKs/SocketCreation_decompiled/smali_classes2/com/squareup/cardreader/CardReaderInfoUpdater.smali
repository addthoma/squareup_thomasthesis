.class abstract Lcom/squareup/cardreader/CardReaderInfoUpdater;
.super Ljava/lang/Object;
.source "CardReaderInfoUpdater.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;


# instance fields
.field final cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;


# direct methods
.method protected constructor <init>(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    return-void
.end method


# virtual methods
.method public logEvent(Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;)V
    .locals 0

    return-void
.end method

.method public onAudioReaderFailedToConnect()V
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderInfo;->setTimedOutDuringInit(Z)V

    return-void
.end method

.method public onCapabilitiesReceived(ZLjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrsCapability;",
            ">;)V"
        }
    .end annotation

    .line 98
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/cardreader/CardReaderInfo;->setCapabilities(ZLjava/util/List;)V

    return-void
.end method

.method public onCardPresenceChange(Z)V
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderInfo;->setCardPresence(Z)V

    return-void
.end method

.method public onCardReaderBackendInitialized()V
    .locals 2

    .line 34
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderInfo;->setConnected(Z)V

    return-void
.end method

.method public onChargeCycleCountReceived(I)V
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderInfo;->setChargeCycleCount(I)V

    return-void
.end method

.method public onCommsRateUpdated(Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;)V
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderInfo;->setCommsRate(Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;)V

    return-void
.end method

.method public onCommsVersionAcquired(Lcom/squareup/cardreader/lcr/CrCommsVersionResult;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V
    .locals 2

    .line 40
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->UNKNOWN:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v0, v1, :cond_0

    .line 41
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R6:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderInfo;->setReaderType(Lcom/squareup/protos/client/bills/CardData$ReaderType;)V

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0, p2}, Lcom/squareup/cardreader/CardReaderInfo;->setLcrVersion(Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V

    .line 45
    iget-object p2, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {p2, p3}, Lcom/squareup/cardreader/CardReaderInfo;->setReaderVersion(Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V

    .line 46
    iget-object p2, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {p2, p1}, Lcom/squareup/cardreader/CardReaderInfo;->setCommsStatus(Lcom/squareup/cardreader/lcr/CrCommsVersionResult;)V

    .line 47
    iget-object p2, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    sget-object p3, Lcom/squareup/cardreader/lcr/CrCommsVersionResult;->CR_CARDREADER_COMMS_VERSION_RESULT_FIRMWARE_UPDATE_REQUIRED:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    if-ne p1, p3, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p2, p1}, Lcom/squareup/cardreader/CardReaderInfo;->setReaderRequiresFirmwareUpdate(Z)V

    return-void
.end method

.method public onCoreDumpErased()V
    .locals 0

    return-void
.end method

.method public onCoreDumpExists(Z)V
    .locals 0

    return-void
.end method

.method public onCoreDumpProgress(III)V
    .locals 0

    return-void
.end method

.method public onCoreDumpReceived([B[B)V
    .locals 0

    return-void
.end method

.method public onCoreDumpTriggered(Z)V
    .locals 0

    return-void
.end method

.method public onFirmwareVersionReceived(Ljava/lang/String;)V
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderInfo;->setFirmwareVersion(Ljava/lang/String;)V

    return-void
.end method

.method public onHardwareSerialNumberReceived(Ljava/lang/String;)V
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderInfo;->setHardwareSerialNumber(Ljava/lang/String;)V

    return-void
.end method

.method public onPowerStatus(Lcom/squareup/cardreader/CardReaderBatteryInfo;)V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderInfo;->setBatteryInfo(Lcom/squareup/cardreader/CardReaderBatteryInfo;)V

    return-void
.end method

.method public onReaderReady(Lcom/squareup/cardreader/lcr/CrCardreaderType;)V
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderInfo;->setLcrReaderType(Lcom/squareup/cardreader/lcr/CrCardreaderType;)V

    return-void
.end method

.method public onSecureSessionDenied(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V
    .locals 0

    .line 135
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    sget-object p2, Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;->DENIED:Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;

    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/CardReaderInfo;->setSecureSessionState(Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;)V

    return-void
.end method

.method public onSecureSessionError(Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V
    .locals 1

    .line 139
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    sget-object v0, Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;->INVALID:Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/CardReaderInfo;->setSecureSessionState(Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;)V

    return-void
.end method

.method public onSecureSessionInvalid()V
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    sget-object v1, Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;->INVALID:Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderInfo;->setSecureSessionState(Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;)V

    return-void
.end method

.method public onSecureSessionSendToServer([B)V
    .locals 0

    return-void
.end method

.method public onSecureSessionValid()V
    .locals 2

    .line 126
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    sget-object v1, Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;->VALID:Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderInfo;->setSecureSessionState(Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;)V

    return-void
.end method

.method public onTamperData([B)V
    .locals 0

    return-void
.end method

.method public onTamperStatus(Lcom/squareup/cardreader/lcr/CrTamperStatus;)V
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderInfo;->setTamperStatus(Lcom/squareup/cardreader/lcr/CrTamperStatus;)V

    return-void
.end method

.method public onTmsCountryCode(Ljava/lang/String;)V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderInfo;->setTmsCountryCode(Ljava/lang/String;)V

    return-void
.end method

.method protected onVersionInfo([Lcom/squareup/cardreader/FirmwareAssetVersionInfo;Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInfoUpdater;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/cardreader/CardReaderInfo;->setFirmwareAssetVersionInfos([Lcom/squareup/cardreader/FirmwareAssetVersionInfo;Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V

    return-void
.end method
