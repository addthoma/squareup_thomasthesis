.class public final Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;
.super Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;
.source "ReaderMessage.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OnTmnTransactionComplete"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\u0014\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003\u00a2\u0006\u0002\u0010\tJ(\u0010\u000f\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0001\u00a2\u0006\u0002\u0010\u0010J\u0013\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001R\u0019\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\n\n\u0002\u0010\n\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;",
        "tmnTransactionResult",
        "Lcom/squareup/cardreader/TmnTransactionResult;",
        "paymentTimingsArray",
        "",
        "Lcom/squareup/cardreader/PaymentTiming;",
        "(Lcom/squareup/cardreader/TmnTransactionResult;[Lcom/squareup/cardreader/PaymentTiming;)V",
        "getPaymentTimingsArray",
        "()[Lcom/squareup/cardreader/PaymentTiming;",
        "[Lcom/squareup/cardreader/PaymentTiming;",
        "getTmnTransactionResult",
        "()Lcom/squareup/cardreader/TmnTransactionResult;",
        "component1",
        "component2",
        "copy",
        "(Lcom/squareup/cardreader/TmnTransactionResult;[Lcom/squareup/cardreader/PaymentTiming;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final paymentTimingsArray:[Lcom/squareup/cardreader/PaymentTiming;

.field private final tmnTransactionResult:Lcom/squareup/cardreader/TmnTransactionResult;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/TmnTransactionResult;[Lcom/squareup/cardreader/PaymentTiming;)V
    .locals 1

    const-string v0, "tmnTransactionResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentTimingsArray"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 414
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;->tmnTransactionResult:Lcom/squareup/cardreader/TmnTransactionResult;

    iput-object p2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;->paymentTimingsArray:[Lcom/squareup/cardreader/PaymentTiming;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;Lcom/squareup/cardreader/TmnTransactionResult;[Lcom/squareup/cardreader/PaymentTiming;ILjava/lang/Object;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;->tmnTransactionResult:Lcom/squareup/cardreader/TmnTransactionResult;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;->paymentTimingsArray:[Lcom/squareup/cardreader/PaymentTiming;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;->copy(Lcom/squareup/cardreader/TmnTransactionResult;[Lcom/squareup/cardreader/PaymentTiming;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/cardreader/TmnTransactionResult;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;->tmnTransactionResult:Lcom/squareup/cardreader/TmnTransactionResult;

    return-object v0
.end method

.method public final component2()[Lcom/squareup/cardreader/PaymentTiming;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;->paymentTimingsArray:[Lcom/squareup/cardreader/PaymentTiming;

    return-object v0
.end method

.method public final copy(Lcom/squareup/cardreader/TmnTransactionResult;[Lcom/squareup/cardreader/PaymentTiming;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;
    .locals 1

    const-string v0, "tmnTransactionResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentTimingsArray"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;

    invoke-direct {v0, p1, p2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;-><init>(Lcom/squareup/cardreader/TmnTransactionResult;[Lcom/squareup/cardreader/PaymentTiming;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;->tmnTransactionResult:Lcom/squareup/cardreader/TmnTransactionResult;

    iget-object v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;->tmnTransactionResult:Lcom/squareup/cardreader/TmnTransactionResult;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;->paymentTimingsArray:[Lcom/squareup/cardreader/PaymentTiming;

    iget-object p1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;->paymentTimingsArray:[Lcom/squareup/cardreader/PaymentTiming;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getPaymentTimingsArray()[Lcom/squareup/cardreader/PaymentTiming;
    .locals 1

    .line 413
    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;->paymentTimingsArray:[Lcom/squareup/cardreader/PaymentTiming;

    return-object v0
.end method

.method public final getTmnTransactionResult()Lcom/squareup/cardreader/TmnTransactionResult;
    .locals 1

    .line 412
    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;->tmnTransactionResult:Lcom/squareup/cardreader/TmnTransactionResult;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;->tmnTransactionResult:Lcom/squareup/cardreader/TmnTransactionResult;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;->paymentTimingsArray:[Lcom/squareup/cardreader/PaymentTiming;

    if-eqz v2, :cond_1

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OnTmnTransactionComplete(tmnTransactionResult="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;->tmnTransactionResult:Lcom/squareup/cardreader/TmnTransactionResult;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", paymentTimingsArray="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;->paymentTimingsArray:[Lcom/squareup/cardreader/PaymentTiming;

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
