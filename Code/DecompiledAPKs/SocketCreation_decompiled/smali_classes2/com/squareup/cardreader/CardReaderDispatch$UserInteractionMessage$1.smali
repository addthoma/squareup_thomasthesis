.class final Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$1;
.super Ljava/lang/Object;
.source "CardReaderDispatch.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;
    .locals 3

    .line 403
    new-instance v0, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object p1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/cardreader/CardReaderDispatch$1;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 401
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;
    .locals 0

    .line 407
    new-array p1, p1, [Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 401
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$1;->newArray(I)[Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;

    move-result-object p1

    return-object p1
.end method
