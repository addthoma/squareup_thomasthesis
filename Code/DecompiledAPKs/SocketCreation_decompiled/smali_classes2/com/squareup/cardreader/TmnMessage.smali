.class public final enum Lcom/squareup/cardreader/TmnMessage;
.super Ljava/lang/Enum;
.source "TmnMessage.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/TmnMessage;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0019\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016j\u0002\u0008\u0017j\u0002\u0008\u0018j\u0002\u0008\u0019\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/cardreader/TmnMessage;",
        "",
        "(Ljava/lang/String;I)V",
        "TMN_MSG_UNKNOWN_ERROR",
        "TMN_MSG_WAITING",
        "TMN_MSG_PROCESSING_TRANSACTION",
        "TMN_MSG_COMPLETE_NORMAL_SETTLE",
        "TMN_MSG_INSUFFICIENT_BALANCE",
        "TMN_MSG_SEVERAL_SUICA_CARD",
        "TMN_MSG_RETAP_WAITING",
        "TMN_MSG_ONLINE_PROCESSING",
        "TMN_MSG_COMPLETE_NORMAL_CHECK_BAL",
        "TMN_MSG_READ_ERROR",
        "TMN_MSG_WRITE_ERROR",
        "TMN_MSG_INVALID_CARD",
        "TMN_MSG_CANCEL",
        "TMN_MSG_DIFFERENT_CARD",
        "TMN_MSG_COMMON_ERROR",
        "TMN_MSG_EXCEEDED_LIMIT_AMOUNT",
        "TMN_MSG_EXPIRED_CARD",
        "TMN_MSG_LOCKED_MOBILE_SERVICE",
        "TMN_MSG_WRONG_CARD_ERROR",
        "TMN_MSG_POLLING_TIMEOUT",
        "TMN_MSG_MIRYO_RESULT_SUCCESS",
        "TMN_MSG_MIRYO_RESULT_FAILURE",
        "TMN_MSG_MIRYO_RESULT_UNKNOWN",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_CANCEL:Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_COMMON_ERROR:Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_COMPLETE_NORMAL_CHECK_BAL:Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_COMPLETE_NORMAL_SETTLE:Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_DIFFERENT_CARD:Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_EXCEEDED_LIMIT_AMOUNT:Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_EXPIRED_CARD:Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_INSUFFICIENT_BALANCE:Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_INVALID_CARD:Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_LOCKED_MOBILE_SERVICE:Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_MIRYO_RESULT_FAILURE:Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_MIRYO_RESULT_SUCCESS:Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_MIRYO_RESULT_UNKNOWN:Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_ONLINE_PROCESSING:Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_POLLING_TIMEOUT:Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_PROCESSING_TRANSACTION:Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_READ_ERROR:Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_RETAP_WAITING:Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_SEVERAL_SUICA_CARD:Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_UNKNOWN_ERROR:Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_WAITING:Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_WRITE_ERROR:Lcom/squareup/cardreader/TmnMessage;

.field public static final enum TMN_MSG_WRONG_CARD_ERROR:Lcom/squareup/cardreader/TmnMessage;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x17

    new-array v0, v0, [Lcom/squareup/cardreader/TmnMessage;

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const/4 v2, 0x0

    const-string v3, "TMN_MSG_UNKNOWN_ERROR"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_UNKNOWN_ERROR:Lcom/squareup/cardreader/TmnMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const/4 v2, 0x1

    const-string v3, "TMN_MSG_WAITING"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_WAITING:Lcom/squareup/cardreader/TmnMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const/4 v2, 0x2

    const-string v3, "TMN_MSG_PROCESSING_TRANSACTION"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_PROCESSING_TRANSACTION:Lcom/squareup/cardreader/TmnMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const/4 v2, 0x3

    const-string v3, "TMN_MSG_COMPLETE_NORMAL_SETTLE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_COMPLETE_NORMAL_SETTLE:Lcom/squareup/cardreader/TmnMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const/4 v2, 0x4

    const-string v3, "TMN_MSG_INSUFFICIENT_BALANCE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_INSUFFICIENT_BALANCE:Lcom/squareup/cardreader/TmnMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const/4 v2, 0x5

    const-string v3, "TMN_MSG_SEVERAL_SUICA_CARD"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_SEVERAL_SUICA_CARD:Lcom/squareup/cardreader/TmnMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const/4 v2, 0x6

    const-string v3, "TMN_MSG_RETAP_WAITING"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_RETAP_WAITING:Lcom/squareup/cardreader/TmnMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const/4 v2, 0x7

    const-string v3, "TMN_MSG_ONLINE_PROCESSING"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_ONLINE_PROCESSING:Lcom/squareup/cardreader/TmnMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const/16 v2, 0x8

    const-string v3, "TMN_MSG_COMPLETE_NORMAL_CHECK_BAL"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_COMPLETE_NORMAL_CHECK_BAL:Lcom/squareup/cardreader/TmnMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const/16 v2, 0x9

    const-string v3, "TMN_MSG_READ_ERROR"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_READ_ERROR:Lcom/squareup/cardreader/TmnMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const/16 v2, 0xa

    const-string v3, "TMN_MSG_WRITE_ERROR"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_WRITE_ERROR:Lcom/squareup/cardreader/TmnMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const/16 v2, 0xb

    const-string v3, "TMN_MSG_INVALID_CARD"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_INVALID_CARD:Lcom/squareup/cardreader/TmnMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const/16 v2, 0xc

    const-string v3, "TMN_MSG_CANCEL"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_CANCEL:Lcom/squareup/cardreader/TmnMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const/16 v2, 0xd

    const-string v3, "TMN_MSG_DIFFERENT_CARD"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_DIFFERENT_CARD:Lcom/squareup/cardreader/TmnMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const/16 v2, 0xe

    const-string v3, "TMN_MSG_COMMON_ERROR"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_COMMON_ERROR:Lcom/squareup/cardreader/TmnMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const-string v2, "TMN_MSG_EXCEEDED_LIMIT_AMOUNT"

    const/16 v3, 0xf

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_EXCEEDED_LIMIT_AMOUNT:Lcom/squareup/cardreader/TmnMessage;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const-string v2, "TMN_MSG_EXPIRED_CARD"

    const/16 v3, 0x10

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_EXPIRED_CARD:Lcom/squareup/cardreader/TmnMessage;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const-string v2, "TMN_MSG_LOCKED_MOBILE_SERVICE"

    const/16 v3, 0x11

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_LOCKED_MOBILE_SERVICE:Lcom/squareup/cardreader/TmnMessage;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const-string v2, "TMN_MSG_WRONG_CARD_ERROR"

    const/16 v3, 0x12

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_WRONG_CARD_ERROR:Lcom/squareup/cardreader/TmnMessage;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const-string v2, "TMN_MSG_POLLING_TIMEOUT"

    const/16 v3, 0x13

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_POLLING_TIMEOUT:Lcom/squareup/cardreader/TmnMessage;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const-string v2, "TMN_MSG_MIRYO_RESULT_SUCCESS"

    const/16 v3, 0x14

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_MIRYO_RESULT_SUCCESS:Lcom/squareup/cardreader/TmnMessage;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const-string v2, "TMN_MSG_MIRYO_RESULT_FAILURE"

    const/16 v3, 0x15

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_MIRYO_RESULT_FAILURE:Lcom/squareup/cardreader/TmnMessage;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnMessage;

    const-string v2, "TMN_MSG_MIRYO_RESULT_UNKNOWN"

    const/16 v3, 0x16

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/TmnMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnMessage;->TMN_MSG_MIRYO_RESULT_UNKNOWN:Lcom/squareup/cardreader/TmnMessage;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/TmnMessage;->$VALUES:[Lcom/squareup/cardreader/TmnMessage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/TmnMessage;
    .locals 1

    const-class v0, Lcom/squareup/cardreader/TmnMessage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/TmnMessage;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/TmnMessage;
    .locals 1

    sget-object v0, Lcom/squareup/cardreader/TmnMessage;->$VALUES:[Lcom/squareup/cardreader/TmnMessage;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/TmnMessage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/TmnMessage;

    return-object v0
.end method
