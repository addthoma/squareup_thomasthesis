.class public interface abstract Lcom/squareup/cardreader/CardReaderDispatch;
.super Ljava/lang/Object;
.source "CardReaderDispatch.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;,
        Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;,
        Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;,
        Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;,
        Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;,
        Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;
    }
.end annotation


# virtual methods
.method public abstract ackTmnWriteNotify()V
.end method

.method public abstract cancelPayment()V
.end method

.method public abstract cancelTmnRequest()V
.end method

.method public abstract checkCardPresence()V
.end method

.method public abstract clearFlaggedTamperStatus()V
.end method

.method public abstract enableSwipePassthrough(Z)V
.end method

.method public abstract eraseCoreDump()V
.end method

.method public abstract forgetReader()V
.end method

.method public abstract identify()V
.end method

.method public abstract initializeCardReader()V
.end method

.method public abstract initializeFeatures(II)V
.end method

.method public abstract initializeSecureSession()V
.end method

.method public abstract notifySecureSessionServerError()V
.end method

.method public abstract onPinBypass()V
.end method

.method public abstract onPinDigitEntered(I)V
.end method

.method public abstract onPinPadReset()V
.end method

.method public abstract onSecureTouchApplicationEvent(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V
.end method

.method public abstract pauseFirmwareUpdate()V
.end method

.method public abstract powerOff()V
.end method

.method public abstract powerOn()V
.end method

.method public abstract processARPC([B)V
.end method

.method public abstract processSecureSessionMessageFromServer([B)V
.end method

.method public abstract reinitializeSecureSession()V
.end method

.method public abstract requestCoreDump()V
.end method

.method public abstract requestFirmwareManifest()V
.end method

.method public abstract requestPowerStatus()V
.end method

.method public abstract requestSystemInfo()V
.end method

.method public abstract requestTamperData()V
.end method

.method public abstract requestTamperStatus()V
.end method

.method public abstract reset()V
.end method

.method public abstract retrieveCoreDump()V
.end method

.method public abstract selectAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V
.end method

.method public abstract selectApplication(Lcom/squareup/cardreader/EmvApplication;)V
.end method

.method public abstract sendReaderPowerupHint(I)V
.end method

.method public abstract sendTmnDataToReader([B)V
.end method

.method public abstract setReaderFeatureFlags(Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V
.end method

.method public abstract startPaymentInteraction(Lcom/squareup/cardreader/PaymentInteraction;)V
.end method

.method public abstract startTmnMiryo([B)V
.end method

.method public abstract submitPinBlock()V
.end method

.method public abstract triggerCoreDump()V
.end method

.method public abstract updateFirmware(Lcom/squareup/protos/client/tarkin/Asset;)V
.end method
