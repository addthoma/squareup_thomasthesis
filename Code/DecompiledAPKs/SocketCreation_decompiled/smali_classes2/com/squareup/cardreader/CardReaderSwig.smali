.class Lcom/squareup/cardreader/CardReaderSwig;
.super Ljava/lang/Object;
.source "CardReaderSwig.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderDispatch;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;,
        Lcom/squareup/cardreader/CardReaderSwig$OneTimeExecutor;,
        Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;,
        Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;,
        Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;,
        Lcom/squareup/cardreader/CardReaderSwig$InternalListener;,
        Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;
    }
.end annotation


# instance fields
.field private final bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

.field private final cardReader:Lcom/squareup/cardreader/CardReader;

.field private final cardReaderExecutor:Ljava/util/concurrent/Executor;

.field private final cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

.field private final cardReaderFeature:Lcom/squareup/cardreader/CardReaderFeatureLegacy;

.field private final cardReaderListener:Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private final completedPaymentListener:Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;

.field private final coreDumpFeature:Lcom/squareup/cardreader/CoreDumpFeatureLegacy;

.field private final eventLogFeature:Lcom/squareup/cardreader/EventLogFeatureLegacy;

.field private final firmwareUpdateFeature:Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;

.field private final firmwareUpdateListener:Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;

.field private final internalListener:Lcom/squareup/cardreader/CardReaderSwig$InternalListener;

.field private final internalPaymentListener:Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;

.field private final isCardReaderAlive:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final nfcPaymentListener:Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;

.field private final oneTimeExecutor:Lcom/squareup/cardreader/CardReaderSwig$OneTimeExecutor;

.field private final paymentFeature:Lcom/squareup/cardreader/PaymentFeatureLegacy;

.field private final paymentListener:Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

.field private final pendingCancels:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final powerFeature:Lcom/squareup/cardreader/PowerFeatureLegacy;

.field private final processLcrCallbackGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

.field private final secureSessionFeature:Lcom/squareup/cardreader/SecureSessionFeatureLegacy;

.field private final secureSessionRevocationFeature:Lcom/squareup/cardreader/SecureSessionRevocationFeature;

.field private final secureTouchFeature:Lcom/squareup/cardreader/SecureTouchFeatureInterface;

.field private final sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

.field private final systemFeature:Lcom/squareup/cardreader/SystemFeatureLegacy;

.field private final tamperFeature:Lcom/squareup/cardreader/TamperFeatureLegacy;

.field private final userInteractionFeature:Lcom/squareup/cardreader/UserInteractionFeatureLegacy;


# direct methods
.method constructor <init>(Lcom/squareup/thread/executor/MainThread;Ljava/util/concurrent/Executor;Lcom/squareup/cardreader/CardReaderFeatureLegacy;Lcom/squareup/cardreader/CoreDumpFeatureLegacy;Lcom/squareup/cardreader/PaymentFeatureLegacy;Lcom/squareup/cardreader/EventLogFeatureLegacy;Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;Lcom/squareup/cardreader/PowerFeatureLegacy;Lcom/squareup/cardreader/SecureSessionFeatureLegacy;Lcom/squareup/cardreader/SecureTouchFeatureInterface;Lcom/squareup/cardreader/SystemFeatureLegacy;Lcom/squareup/cardreader/TamperFeatureLegacy;Lcom/squareup/cardreader/UserInteractionFeatureLegacy;Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;Lcom/squareup/cardreader/SecureSessionRevocationFeature;Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/cardreader/CardReaderListeners;)V
    .locals 3

    move-object v0, p0

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->pendingCancels:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 92
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->isCardReaderAlive:Ljava/util/concurrent/atomic/AtomicBoolean;

    move-object v1, p1

    .line 110
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->mainThread:Lcom/squareup/thread/executor/MainThread;

    move-object v1, p2

    .line 111
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->cardReaderExecutor:Ljava/util/concurrent/Executor;

    move-object v1, p3

    .line 112
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->cardReaderFeature:Lcom/squareup/cardreader/CardReaderFeatureLegacy;

    move-object v1, p4

    .line 113
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->coreDumpFeature:Lcom/squareup/cardreader/CoreDumpFeatureLegacy;

    move-object v1, p6

    .line 114
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->eventLogFeature:Lcom/squareup/cardreader/EventLogFeatureLegacy;

    move-object v1, p7

    .line 115
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->firmwareUpdateFeature:Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;

    move-object v1, p5

    .line 116
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->paymentFeature:Lcom/squareup/cardreader/PaymentFeatureLegacy;

    move-object v1, p8

    .line 117
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->powerFeature:Lcom/squareup/cardreader/PowerFeatureLegacy;

    move-object v1, p9

    .line 118
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->secureSessionFeature:Lcom/squareup/cardreader/SecureSessionFeatureLegacy;

    move-object v1, p10

    .line 119
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->secureTouchFeature:Lcom/squareup/cardreader/SecureTouchFeatureInterface;

    move-object v1, p11

    .line 120
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->systemFeature:Lcom/squareup/cardreader/SystemFeatureLegacy;

    move-object v1, p12

    .line 121
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->tamperFeature:Lcom/squareup/cardreader/TamperFeatureLegacy;

    move-object/from16 v1, p13

    .line 122
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->userInteractionFeature:Lcom/squareup/cardreader/UserInteractionFeatureLegacy;

    move-object/from16 v1, p14

    .line 123
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->cardReaderListener:Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-object/from16 v1, p15

    .line 124
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->firmwareUpdateListener:Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;

    move-object/from16 v1, p16

    .line 125
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->paymentListener:Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    move-object/from16 v1, p17

    .line 126
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->nfcPaymentListener:Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;

    move-object/from16 v1, p18

    .line 127
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->completedPaymentListener:Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;

    move-object/from16 v1, p19

    .line 128
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->secureSessionRevocationFeature:Lcom/squareup/cardreader/SecureSessionRevocationFeature;

    move-object/from16 v1, p20

    .line 129
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->cardReader:Lcom/squareup/cardreader/CardReader;

    move-object/from16 v1, p21

    .line 130
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    move-object/from16 v1, p22

    .line 131
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    move-object/from16 v1, p23

    .line 132
    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    .line 134
    new-instance v1, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/CardReaderSwig$InternalListener;-><init>(Lcom/squareup/cardreader/CardReaderSwig;)V

    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->internalListener:Lcom/squareup/cardreader/CardReaderSwig$InternalListener;

    .line 135
    new-instance v1, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;-><init>(Lcom/squareup/cardreader/CardReaderSwig;)V

    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->internalPaymentListener:Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;

    .line 136
    new-instance v1, Lcom/squareup/cardreader/CardReaderSwig$OneTimeExecutor;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/CardReaderSwig$OneTimeExecutor;-><init>(Lcom/squareup/cardreader/CardReaderSwig;)V

    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->oneTimeExecutor:Lcom/squareup/cardreader/CardReaderSwig$OneTimeExecutor;

    .line 137
    new-instance v1, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;-><init>(Lcom/squareup/cardreader/CardReaderSwig;Lcom/squareup/cardreader/CardReaderSwig$1;)V

    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    .line 138
    new-instance v1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    invoke-direct {v1, p0, v2}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;-><init>(Lcom/squareup/cardreader/CardReaderSwig;Lcom/squareup/cardreader/CardReaderSwig$1;)V

    iput-object v1, v0, Lcom/squareup/cardreader/CardReaderSwig;->processLcrCallbackGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    return-void
.end method

.method static synthetic access$1000(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/BluetoothUtils;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderSwig;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderSwig;->firmwareUpdateListener:Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/squareup/cardreader/CardReaderSwig;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderSwig;->isCardReaderAlive:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/squareup/cardreader/CardReaderSwig;)Ljava/util/concurrent/Executor;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderSwig;->cardReaderExecutor:Ljava/util/concurrent/Executor;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/squareup/cardreader/CardReaderSwig;Ljava/lang/Runnable;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/CardReaderSwig;->executeOnMainSyncIfNoPendingCancel(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/thread/executor/MainThread;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderSwig;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderSwig;->paymentListener:Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderSwig;->processLcrCallbackGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderSwig;->nfcPaymentListener:Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderSwig;->completedPaymentListener:Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderSwig;->cardReaderListener:Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReader;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderSwig;->cardReader:Lcom/squareup/cardreader/CardReader;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderListeners;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderSwig;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/cardreader/CardReaderFactory;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderSwig;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    return-object p0
.end method

.method private executeOnMainSyncIfNoPendingCancel(Ljava/lang/Runnable;)V
    .locals 3

    .line 829
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 830
    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$JvYWMoI_sCt9dHcEoSRytkyYU_c;

    invoke-direct {v2, p0, v0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$JvYWMoI_sCt9dHcEoSRytkyYU_c;-><init>(Lcom/squareup/cardreader/CardReaderSwig;Ljava/util/concurrent/CountDownLatch;Ljava/lang/Runnable;)V

    invoke-interface {v1, v2}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    .line 838
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 840
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    .line 841
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method


# virtual methods
.method public ackTmnWriteNotify()V
    .locals 3

    .line 303
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->paymentFeature:Lcom/squareup/cardreader/PaymentFeatureLegacy;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$2_IXfiQayw4cWcHTSQnLCph7ASo;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$2_IXfiQayw4cWcHTSQnLCph7ASo;-><init>(Lcom/squareup/cardreader/PaymentFeatureLegacy;)V

    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public cancelPayment()V
    .locals 2

    .line 315
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->pendingCancels:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 316
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$JUiP3iI1KHpT7qhX62cPo5WYuAw;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$JUiP3iI1KHpT7qhX62cPo5WYuAw;-><init>(Lcom/squareup/cardreader/CardReaderSwig;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public cancelTmnRequest()V
    .locals 3

    .line 307
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->paymentFeature:Lcom/squareup/cardreader/PaymentFeatureLegacy;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$wEBwxnuVlx9hNuXeS5KOom5MsYQ;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$wEBwxnuVlx9hNuXeS5KOom5MsYQ;-><init>(Lcom/squareup/cardreader/PaymentFeatureLegacy;)V

    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public checkCardPresence()V
    .locals 3

    .line 270
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->paymentFeature:Lcom/squareup/cardreader/PaymentFeatureLegacy;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$5dwQ5bWgzA7orlsOVHSksn3nNhE;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$5dwQ5bWgzA7orlsOVHSksn3nNhE;-><init>(Lcom/squareup/cardreader/PaymentFeatureLegacy;)V

    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public clearFlaggedTamperStatus()V
    .locals 3

    .line 246
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->tamperFeature:Lcom/squareup/cardreader/TamperFeatureLegacy;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$Os7rE-7bftc0utTor0JFuYP0hSg;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$Os7rE-7bftc0utTor0JFuYP0hSg;-><init>(Lcom/squareup/cardreader/TamperFeatureLegacy;)V

    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public enableSwipePassthrough(Z)V
    .locals 2

    .line 274
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$kCtQLYLqNiYLHeaG3qpUGjIdkn0;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$kCtQLYLqNiYLHeaG3qpUGjIdkn0;-><init>(Lcom/squareup/cardreader/CardReaderSwig;Z)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public eraseCoreDump()V
    .locals 3

    .line 262
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->coreDumpFeature:Lcom/squareup/cardreader/CoreDumpFeatureLegacy;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$5xZbbrIeWyQ44Uh71uTD5mfyHR8;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$5xZbbrIeWyQ44Uh71uTD5mfyHR8;-><init>(Lcom/squareup/cardreader/CoreDumpFeatureLegacy;)V

    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public forgetReader()V
    .locals 3

    .line 225
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->cardReaderFeature:Lcom/squareup/cardreader/CardReaderFeatureLegacy;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$Rgf8a6FgAf-IVM1GZigm2gGM4cA;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$Rgf8a6FgAf-IVM1GZigm2gGM4cA;-><init>(Lcom/squareup/cardreader/CardReaderFeatureLegacy;)V

    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method getInternalListener()Lcom/squareup/cardreader/CardReaderSwig$InternalListener;
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->internalListener:Lcom/squareup/cardreader/CardReaderSwig$InternalListener;

    return-object v0
.end method

.method public identify()V
    .locals 3

    .line 361
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->userInteractionFeature:Lcom/squareup/cardreader/UserInteractionFeatureLegacy;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$xqT6E961Qc-yTDPkRQ4FCsNx-x8;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$xqT6E961Qc-yTDPkRQ4FCsNx-x8;-><init>(Lcom/squareup/cardreader/UserInteractionFeatureLegacy;)V

    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public initializeCardReader()V
    .locals 2

    .line 142
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$Kpre0Cj1wBfyrR-HwbUIloaGlCI;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$Kpre0Cj1wBfyrR-HwbUIloaGlCI;-><init>(Lcom/squareup/cardreader/CardReaderSwig;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public initializeFeatures(II)V
    .locals 2

    .line 151
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$JRSwalWhFAdjrm5FkqJJLB65vow;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$JRSwalWhFAdjrm5FkqJJLB65vow;-><init>(Lcom/squareup/cardreader/CardReaderSwig;II)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public initializeSecureSession()V
    .locals 2

    .line 194
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$iFaL8HJtIvD2sbYoRE_4teQOWZE;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$iFaL8HJtIvD2sbYoRE_4teQOWZE;-><init>(Lcom/squareup/cardreader/CardReaderSwig;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$cancelPayment$13$CardReaderSwig()V
    .locals 1

    .line 317
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->paymentFeature:Lcom/squareup/cardreader/PaymentFeatureLegacy;

    invoke-virtual {v0}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->cancelPayment()V

    .line 318
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->pendingCancels:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    return-void
.end method

.method public synthetic lambda$enableSwipePassthrough$6$CardReaderSwig(Z)V
    .locals 1

    .line 274
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->paymentFeature:Lcom/squareup/cardreader/PaymentFeatureLegacy;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->enableSwipePassthrough(Z)V

    return-void
.end method

.method public synthetic lambda$executeOnMainSyncIfNoPendingCancel$20$CardReaderSwig(Ljava/util/concurrent/CountDownLatch;Ljava/lang/Runnable;)V
    .locals 1

    .line 831
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->pendingCancels:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 832
    :goto_0
    invoke-virtual {p1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    if-eqz v0, :cond_1

    .line 834
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    :cond_1
    return-void
.end method

.method public synthetic lambda$initializeCardReader$0$CardReaderSwig()V
    .locals 3

    .line 144
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->cardReaderFeature:Lcom/squareup/cardreader/CardReaderFeatureLegacy;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->internalListener:Lcom/squareup/cardreader/CardReaderSwig$InternalListener;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->initializeCardreader(Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;)V

    .line 146
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->cardReaderListener:Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$tvD8pDwcEpgGI6GxYJo5gH2ohJ8;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$tvD8pDwcEpgGI6GxYJo5gH2ohJ8;-><init>(Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;)V

    invoke-interface {v0, v2}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$initializeFeatures$1$CardReaderSwig(II)V
    .locals 2

    .line 152
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->cardReaderFeature:Lcom/squareup/cardreader/CardReaderFeatureLegacy;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->getCardreader()Lcom/squareup/cardreader/CardReaderPointer;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string p2, "This CardReader instance is beginning tear down, ignoring"

    .line 153
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->coreDumpFeature:Lcom/squareup/cardreader/CoreDumpFeatureLegacy;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->internalListener:Lcom/squareup/cardreader/CardReaderSwig$InternalListener;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->initialize(Lcom/squareup/cardreader/CoreDumpFeatureLegacy$CoreDumpFeatureListener;)V

    .line 158
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->eventLogFeature:Lcom/squareup/cardreader/EventLogFeatureLegacy;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->internalListener:Lcom/squareup/cardreader/CardReaderSwig$InternalListener;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/EventLogFeatureLegacy;->initialize(Lcom/squareup/cardreader/EventLogFeatureLegacy$EventLogFeatureListener;)V

    .line 159
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->firmwareUpdateFeature:Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->internalListener:Lcom/squareup/cardreader/CardReaderSwig$InternalListener;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->initialize(Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;)V

    .line 160
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->powerFeature:Lcom/squareup/cardreader/PowerFeatureLegacy;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->internalListener:Lcom/squareup/cardreader/CardReaderSwig$InternalListener;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/PowerFeatureLegacy;->initialize(Lcom/squareup/cardreader/PowerFeatureLegacy$PowerListener;)V

    .line 161
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->systemFeature:Lcom/squareup/cardreader/SystemFeatureLegacy;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->internalListener:Lcom/squareup/cardreader/CardReaderSwig$InternalListener;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/SystemFeatureLegacy;->initialize(Lcom/squareup/cardreader/SystemFeatureLegacy$SystemFeatureListener;)V

    .line 162
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->tamperFeature:Lcom/squareup/cardreader/TamperFeatureLegacy;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->internalListener:Lcom/squareup/cardreader/CardReaderSwig$InternalListener;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/TamperFeatureLegacy;->initialize(Lcom/squareup/cardreader/TamperFeatureLegacy$TamperListener;)V

    .line 163
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->userInteractionFeature:Lcom/squareup/cardreader/UserInteractionFeatureLegacy;

    invoke-virtual {v0}, Lcom/squareup/cardreader/UserInteractionFeatureLegacy;->initialize()V

    .line 164
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->paymentFeature:Lcom/squareup/cardreader/PaymentFeatureLegacy;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->internalPaymentListener:Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;

    invoke-virtual {v0, v1, p1, p2}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->initializePayment(Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;II)V

    .line 165
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderSwig;->secureTouchFeature:Lcom/squareup/cardreader/SecureTouchFeatureInterface;

    iget-object p2, p0, Lcom/squareup/cardreader/CardReaderSwig;->internalListener:Lcom/squareup/cardreader/CardReaderSwig$InternalListener;

    invoke-interface {p1, p2}, Lcom/squareup/cardreader/SecureTouchFeatureInterface;->initialize(Lcom/squareup/securetouch/SecureTouchFeatureNativeListener;)V

    return-void
.end method

.method public synthetic lambda$initializeSecureSession$3$CardReaderSwig()V
    .locals 2

    .line 196
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->secureSessionRevocationFeature:Lcom/squareup/cardreader/SecureSessionRevocationFeature;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->internalListener:Lcom/squareup/cardreader/CardReaderSwig$InternalListener;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->initializeFeature(Lcom/squareup/cardreader/CardReaderSwig$InternalListener;)V

    .line 197
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->secureSessionFeature:Lcom/squareup/cardreader/SecureSessionFeatureLegacy;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->internalListener:Lcom/squareup/cardreader/CardReaderSwig$InternalListener;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->initializeSecureSession(Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;)V

    return-void
.end method

.method public synthetic lambda$notifySecureSessionServerError$8$CardReaderSwig()V
    .locals 1

    .line 287
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->secureSessionFeature:Lcom/squareup/cardreader/SecureSessionFeatureLegacy;

    invoke-virtual {v0}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->notifySecureSessionServerError()V

    return-void
.end method

.method public synthetic lambda$onPinDigitEntered$16$CardReaderSwig(I)V
    .locals 1

    .line 335
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->secureSessionFeature:Lcom/squareup/cardreader/SecureSessionFeatureLegacy;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->onPinDigitEntered(I)V

    return-void
.end method

.method public synthetic lambda$onSecureTouchApplicationEvent$17$CardReaderSwig(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V
    .locals 1

    .line 349
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->secureTouchFeature:Lcom/squareup/cardreader/SecureTouchFeatureInterface;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/SecureTouchFeatureInterface;->onSecureTouchApplicationEvent(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V

    return-void
.end method

.method public synthetic lambda$processARPC$18$CardReaderSwig([B)V
    .locals 1

    .line 353
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->paymentFeature:Lcom/squareup/cardreader/PaymentFeatureLegacy;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->processARPC([B)V

    return-void
.end method

.method public synthetic lambda$processSecureSessionMessageFromServer$7$CardReaderSwig([B)V
    .locals 1

    .line 283
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->secureSessionFeature:Lcom/squareup/cardreader/SecureSessionFeatureLegacy;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->processServerMessage([B)V

    return-void
.end method

.method public synthetic lambda$reinitializeSecureSession$4$CardReaderSwig()V
    .locals 2

    .line 203
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->secureSessionFeature:Lcom/squareup/cardreader/SecureSessionFeatureLegacy;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->internalListener:Lcom/squareup/cardreader/CardReaderSwig$InternalListener;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->reinitialize(Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;)V

    return-void
.end method

.method public synthetic lambda$reset$2$CardReaderSwig()V
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->coreDumpFeature:Lcom/squareup/cardreader/CoreDumpFeatureLegacy;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CoreDumpFeatureLegacy;->resetCoreDumpFeature()V

    .line 176
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->paymentFeature:Lcom/squareup/cardreader/PaymentFeatureLegacy;

    invoke-virtual {v0}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->resetEmvFeature()V

    .line 177
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->eventLogFeature:Lcom/squareup/cardreader/EventLogFeatureLegacy;

    invoke-virtual {v0}, Lcom/squareup/cardreader/EventLogFeatureLegacy;->resetEventLogFeature()V

    .line 178
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->firmwareUpdateFeature:Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;

    invoke-virtual {v0}, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->resetFirmwareFeature()V

    .line 179
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->powerFeature:Lcom/squareup/cardreader/PowerFeatureLegacy;

    invoke-virtual {v0}, Lcom/squareup/cardreader/PowerFeatureLegacy;->resetPowerFeature()V

    .line 180
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->systemFeature:Lcom/squareup/cardreader/SystemFeatureLegacy;

    invoke-virtual {v0}, Lcom/squareup/cardreader/SystemFeatureLegacy;->resetSystemFeature()V

    .line 181
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->tamperFeature:Lcom/squareup/cardreader/TamperFeatureLegacy;

    invoke-virtual {v0}, Lcom/squareup/cardreader/TamperFeatureLegacy;->resetTamperFeature()V

    .line 182
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->userInteractionFeature:Lcom/squareup/cardreader/UserInteractionFeatureLegacy;

    invoke-virtual {v0}, Lcom/squareup/cardreader/UserInteractionFeatureLegacy;->resetUserInteractionFeature()V

    .line 183
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->secureSessionRevocationFeature:Lcom/squareup/cardreader/SecureSessionRevocationFeature;

    invoke-virtual {v0}, Lcom/squareup/cardreader/SecureSessionRevocationFeature;->reset()V

    .line 184
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->secureSessionFeature:Lcom/squareup/cardreader/SecureSessionFeatureLegacy;

    invoke-virtual {v0}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->resetSecureSession()V

    .line 185
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->secureTouchFeature:Lcom/squareup/cardreader/SecureTouchFeatureInterface;

    invoke-interface {v0}, Lcom/squareup/cardreader/SecureTouchFeatureInterface;->reset()V

    .line 188
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->cardReaderFeature:Lcom/squareup/cardreader/CardReaderFeatureLegacy;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->resetCardreader()V

    return-void
.end method

.method public synthetic lambda$selectAccountType$15$CardReaderSwig(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V
    .locals 1

    .line 327
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->paymentFeature:Lcom/squareup/cardreader/PaymentFeatureLegacy;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->selectAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V

    return-void
.end method

.method public synthetic lambda$selectApplication$14$CardReaderSwig(Lcom/squareup/cardreader/EmvApplication;)V
    .locals 1

    .line 323
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->paymentFeature:Lcom/squareup/cardreader/PaymentFeatureLegacy;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->selectApplication(Lcom/squareup/cardreader/EmvApplication;)V

    return-void
.end method

.method public synthetic lambda$sendReaderPowerupHint$10$CardReaderSwig(I)V
    .locals 1

    .line 295
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->paymentFeature:Lcom/squareup/cardreader/PaymentFeatureLegacy;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->sendReaderPowerupHint(I)V

    return-void
.end method

.method public synthetic lambda$sendTmnDataToReader$11$CardReaderSwig([B)V
    .locals 1

    .line 299
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->paymentFeature:Lcom/squareup/cardreader/PaymentFeatureLegacy;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->sendTmnBytesToReader([B)V

    return-void
.end method

.method public synthetic lambda$setReaderFeatureFlags$19$CardReaderSwig(Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V
    .locals 1

    .line 357
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->systemFeature:Lcom/squareup/cardreader/SystemFeatureLegacy;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/SystemFeatureLegacy;->setReaderFeatureFlags(Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V

    return-void
.end method

.method public synthetic lambda$startPaymentInteraction$9$CardReaderSwig(Lcom/squareup/cardreader/PaymentInteraction;)V
    .locals 1

    .line 291
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->paymentFeature:Lcom/squareup/cardreader/PaymentFeatureLegacy;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->startPaymentInteraction(Lcom/squareup/cardreader/PaymentInteraction;)V

    return-void
.end method

.method public synthetic lambda$startTmnMiryo$12$CardReaderSwig([B)V
    .locals 1

    .line 311
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->paymentFeature:Lcom/squareup/cardreader/PaymentFeatureLegacy;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->startTmnMiryo([B)V

    return-void
.end method

.method public synthetic lambda$updateFirmware$5$CardReaderSwig(Lcom/squareup/protos/client/tarkin/Asset;)V
    .locals 1

    .line 213
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->firmwareUpdateFeature:Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->update(Lcom/squareup/protos/client/tarkin/Asset;)V

    return-void
.end method

.method public notifySecureSessionServerError()V
    .locals 2

    .line 287
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$JpKplD-ydXPxaeGy7ao21GOG0D0;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$JpKplD-ydXPxaeGy7ao21GOG0D0;-><init>(Lcom/squareup/cardreader/CardReaderSwig;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onPinBypass()V
    .locals 3

    .line 339
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->secureSessionFeature:Lcom/squareup/cardreader/SecureSessionFeatureLegacy;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$2aBqOvFmJtRzZgTeq_ezea4IG1Y;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$2aBqOvFmJtRzZgTeq_ezea4IG1Y;-><init>(Lcom/squareup/cardreader/SecureSessionFeatureLegacy;)V

    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onPinDigitEntered(I)V
    .locals 2

    .line 335
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$vStcYpwAX3xc9tz9Qqi_5fnCI0I;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$vStcYpwAX3xc9tz9Qqi_5fnCI0I;-><init>(Lcom/squareup/cardreader/CardReaderSwig;I)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onPinPadReset()V
    .locals 3

    .line 331
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->secureSessionFeature:Lcom/squareup/cardreader/SecureSessionFeatureLegacy;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$4Btbx4MeK6NgAX-WFuOf4NTdLfg;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$4Btbx4MeK6NgAX-WFuOf4NTdLfg;-><init>(Lcom/squareup/cardreader/SecureSessionFeatureLegacy;)V

    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onSecureTouchApplicationEvent(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V
    .locals 2

    .line 348
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$cVoQbLqM76qM_5HMacMrDElpRvo;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$cVoQbLqM76qM_5HMacMrDElpRvo;-><init>(Lcom/squareup/cardreader/CardReaderSwig;Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public pauseFirmwareUpdate()V
    .locals 3

    .line 217
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->firmwareUpdateFeature:Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$suNCe4IkuSesZr_fCtsvwlSVdus;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$suNCe4IkuSesZr_fCtsvwlSVdus;-><init>(Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;)V

    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public powerOff()V
    .locals 3

    .line 229
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->powerFeature:Lcom/squareup/cardreader/PowerFeatureLegacy;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$Riwi54X2b5u-7GkH1l5J0-x1f3w;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$Riwi54X2b5u-7GkH1l5J0-x1f3w;-><init>(Lcom/squareup/cardreader/PowerFeatureLegacy;)V

    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public powerOn()V
    .locals 3

    .line 233
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->cardReaderFeature:Lcom/squareup/cardreader/CardReaderFeatureLegacy;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$I34m02GP2q2KlTcz7vF63Pl8cNs;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$I34m02GP2q2KlTcz7vF63Pl8cNs;-><init>(Lcom/squareup/cardreader/CardReaderFeatureLegacy;)V

    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public processARPC([B)V
    .locals 2

    .line 353
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$m7lsLxv0zu-mPAD9zhY-Z1mbKbs;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$m7lsLxv0zu-mPAD9zhY-Z1mbKbs;-><init>(Lcom/squareup/cardreader/CardReaderSwig;[B)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public processSecureSessionMessageFromServer([B)V
    .locals 2

    .line 283
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$8sK-1vQSMObx5GBBAWRxQTj2JIE;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$8sK-1vQSMObx5GBBAWRxQTj2JIE;-><init>(Lcom/squareup/cardreader/CardReaderSwig;[B)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public reinitializeSecureSession()V
    .locals 2

    .line 202
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$TcQ_Eram5xFxlao12iuPLyrIoLY;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$TcQ_Eram5xFxlao12iuPLyrIoLY;-><init>(Lcom/squareup/cardreader/CardReaderSwig;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public requestCoreDump()V
    .locals 3

    .line 254
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->coreDumpFeature:Lcom/squareup/cardreader/CoreDumpFeatureLegacy;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$RcOWB3-1rNbsp2JhKRWNjWnd0m8;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$RcOWB3-1rNbsp2JhKRWNjWnd0m8;-><init>(Lcom/squareup/cardreader/CoreDumpFeatureLegacy;)V

    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public requestFirmwareManifest()V
    .locals 4

    .line 278
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->oneTimeExecutor:Lcom/squareup/cardreader/CardReaderSwig$OneTimeExecutor;

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;->REQUEST_FIRMWARE_MANIFEST:Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderSwig;->firmwareUpdateFeature:Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/cardreader/-$$Lambda$4dY5T28HkjxQ-Z72voLnLf2gvGg;

    invoke-direct {v3, v2}, Lcom/squareup/cardreader/-$$Lambda$4dY5T28HkjxQ-Z72voLnLf2gvGg;-><init>(Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;)V

    invoke-virtual {v0, v1, v3}, Lcom/squareup/cardreader/CardReaderSwig$OneTimeExecutor;->execute(Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;Ljava/lang/Runnable;)V

    return-void
.end method

.method public requestPowerStatus()V
    .locals 3

    .line 221
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->powerFeature:Lcom/squareup/cardreader/PowerFeatureLegacy;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$6o7HPqlBinp3WSkAQn9aeQa4UtQ;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$6o7HPqlBinp3WSkAQn9aeQa4UtQ;-><init>(Lcom/squareup/cardreader/PowerFeatureLegacy;)V

    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public requestSystemInfo()V
    .locals 4

    .line 266
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->oneTimeExecutor:Lcom/squareup/cardreader/CardReaderSwig$OneTimeExecutor;

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;->REQUEST_SYSTEM_INFO:Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderSwig;->systemFeature:Lcom/squareup/cardreader/SystemFeatureLegacy;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/cardreader/-$$Lambda$Y7MEOiDfSdI9iVycdL_Vg3ECAeE;

    invoke-direct {v3, v2}, Lcom/squareup/cardreader/-$$Lambda$Y7MEOiDfSdI9iVycdL_Vg3ECAeE;-><init>(Lcom/squareup/cardreader/SystemFeatureLegacy;)V

    invoke-virtual {v0, v1, v3}, Lcom/squareup/cardreader/CardReaderSwig$OneTimeExecutor;->execute(Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;Ljava/lang/Runnable;)V

    return-void
.end method

.method public requestTamperData()V
    .locals 4

    .line 242
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->oneTimeExecutor:Lcom/squareup/cardreader/CardReaderSwig$OneTimeExecutor;

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;->REQUEST_TAMPER_DATA:Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderSwig;->tamperFeature:Lcom/squareup/cardreader/TamperFeatureLegacy;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/cardreader/-$$Lambda$cYTiiCzo1CxRx4wJ-CvhUNLkgi4;

    invoke-direct {v3, v2}, Lcom/squareup/cardreader/-$$Lambda$cYTiiCzo1CxRx4wJ-CvhUNLkgi4;-><init>(Lcom/squareup/cardreader/TamperFeatureLegacy;)V

    invoke-virtual {v0, v1, v3}, Lcom/squareup/cardreader/CardReaderSwig$OneTimeExecutor;->execute(Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;Ljava/lang/Runnable;)V

    return-void
.end method

.method public requestTamperStatus()V
    .locals 4

    .line 237
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->oneTimeExecutor:Lcom/squareup/cardreader/CardReaderSwig$OneTimeExecutor;

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;->REQUEST_TAMPER_STATUS:Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderSwig;->tamperFeature:Lcom/squareup/cardreader/TamperFeatureLegacy;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/cardreader/-$$Lambda$sw6U57UtJvZ2VeJ12DVDLC4XVQs;

    invoke-direct {v3, v2}, Lcom/squareup/cardreader/-$$Lambda$sw6U57UtJvZ2VeJ12DVDLC4XVQs;-><init>(Lcom/squareup/cardreader/TamperFeatureLegacy;)V

    invoke-virtual {v0, v1, v3}, Lcom/squareup/cardreader/CardReaderSwig$OneTimeExecutor;->execute(Lcom/squareup/cardreader/CardReaderSwig$OneTimeMessage;Ljava/lang/Runnable;)V

    return-void
.end method

.method public reset()V
    .locals 2

    .line 172
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->isCardReaderAlive:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 174
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->cardReaderExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$eBE5m1YU7NlYtywDk0p-7R_EOOg;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$eBE5m1YU7NlYtywDk0p-7R_EOOg;-><init>(Lcom/squareup/cardreader/CardReaderSwig;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 190
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->oneTimeExecutor:Lcom/squareup/cardreader/CardReaderSwig$OneTimeExecutor;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderSwig$OneTimeExecutor;->reset()V

    return-void
.end method

.method public retrieveCoreDump()V
    .locals 3

    .line 258
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->coreDumpFeature:Lcom/squareup/cardreader/CoreDumpFeatureLegacy;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$-43EV2w8x3oE4YCdXQ6IAoiX5mU;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$-43EV2w8x3oE4YCdXQ6IAoiX5mU;-><init>(Lcom/squareup/cardreader/CoreDumpFeatureLegacy;)V

    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public selectAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V
    .locals 2

    .line 327
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$QzCK2m3UV-oV1T2wuR_fnbu_j3Q;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$QzCK2m3UV-oV1T2wuR_fnbu_j3Q;-><init>(Lcom/squareup/cardreader/CardReaderSwig;Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public selectApplication(Lcom/squareup/cardreader/EmvApplication;)V
    .locals 2

    .line 323
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$YvoZBrZIS4ulM3_00RwkraKR-g8;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$YvoZBrZIS4ulM3_00RwkraKR-g8;-><init>(Lcom/squareup/cardreader/CardReaderSwig;Lcom/squareup/cardreader/EmvApplication;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public sendReaderPowerupHint(I)V
    .locals 2

    .line 295
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$j6D1YJbYmWjB5Lp47wtJiKj3-l8;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$j6D1YJbYmWjB5Lp47wtJiKj3-l8;-><init>(Lcom/squareup/cardreader/CardReaderSwig;I)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public sendTmnDataToReader([B)V
    .locals 2

    .line 299
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$MDDUwTxAvMuGT7XQRmr7SI7tETs;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$MDDUwTxAvMuGT7XQRmr7SI7tETs;-><init>(Lcom/squareup/cardreader/CardReaderSwig;[B)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setReaderFeatureFlags(Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V
    .locals 2

    .line 357
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$gD2SEmHu3iZBNPjz8RIQfjqPbU0;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$gD2SEmHu3iZBNPjz8RIQfjqPbU0;-><init>(Lcom/squareup/cardreader/CardReaderSwig;Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public startPaymentInteraction(Lcom/squareup/cardreader/PaymentInteraction;)V
    .locals 2

    .line 291
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$eQZIn7H-9bmwQIfKk86Rzc6s19k;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$eQZIn7H-9bmwQIfKk86Rzc6s19k;-><init>(Lcom/squareup/cardreader/CardReaderSwig;Lcom/squareup/cardreader/PaymentInteraction;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public startTmnMiryo([B)V
    .locals 2

    .line 311
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$pGOzM64cx5D-cl3IncEeZ3rQKzY;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$pGOzM64cx5D-cl3IncEeZ3rQKzY;-><init>(Lcom/squareup/cardreader/CardReaderSwig;[B)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public submitPinBlock()V
    .locals 3

    .line 343
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->secureSessionFeature:Lcom/squareup/cardreader/SecureSessionFeatureLegacy;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$VMwIY1AfFFuJEOxLxv41HEwesUg;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$VMwIY1AfFFuJEOxLxv41HEwesUg;-><init>(Lcom/squareup/cardreader/SecureSessionFeatureLegacy;)V

    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public triggerCoreDump()V
    .locals 3

    .line 250
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderSwig;->coreDumpFeature:Lcom/squareup/cardreader/CoreDumpFeatureLegacy;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$RHB9Y5f6DSkTwswu199FTDTWvcM;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$RHB9Y5f6DSkTwswu199FTDTWvcM;-><init>(Lcom/squareup/cardreader/CoreDumpFeatureLegacy;)V

    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public updateFirmware(Lcom/squareup/protos/client/tarkin/Asset;)V
    .locals 2

    .line 213
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig;->sendToLcrGateExecutor:Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$BlWGK4XKtgEpicyZDCHM70a99_E;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$BlWGK4XKtgEpicyZDCHM70a99_E;-><init>(Lcom/squareup/cardreader/CardReaderSwig;Lcom/squareup/protos/client/tarkin/Asset;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderSwig$SendToLcrGateExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
