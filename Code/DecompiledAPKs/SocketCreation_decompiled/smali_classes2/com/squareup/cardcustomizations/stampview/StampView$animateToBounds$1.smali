.class final Lcom/squareup/cardcustomizations/stampview/StampView$animateToBounds$1;
.super Ljava/lang/Object;
.source "StampView.kt"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cardcustomizations/stampview/StampView;->animateToBounds(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;Landroid/graphics/Rect;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStampView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StampView.kt\ncom/squareup/cardcustomizations/stampview/StampView$animateToBounds$1\n+ 2 Point.kt\nandroidx/core/graphics/PointKt\n*L\n1#1,505:1\n121#2,3:506\n*E\n*S KotlinDebug\n*F\n+ 1 StampView.kt\ncom/squareup/cardcustomizations/stampview/StampView$animateToBounds$1\n*L\n355#1,3:506\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/animation/ValueAnimator;",
        "kotlin.jvm.PlatformType",
        "onAnimationUpdate"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0xd
    }
.end annotation


# instance fields
.field final synthetic $distance:Landroid/graphics/PointF;

.field final synthetic $lastDistanceMoved:Lkotlin/jvm/internal/Ref$ObjectRef;

.field final synthetic $this_animateToBounds:Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;


# direct methods
.method constructor <init>(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;Landroid/graphics/PointF;Lkotlin/jvm/internal/Ref$ObjectRef;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$animateToBounds$1;->$this_animateToBounds:Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    iput-object p2, p0, Lcom/squareup/cardcustomizations/stampview/StampView$animateToBounds$1;->$distance:Landroid/graphics/PointF;

    iput-object p3, p0, Lcom/squareup/cardcustomizations/stampview/StampView$animateToBounds$1;->$lastDistanceMoved:Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    .line 354
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$animateToBounds$1;->$distance:Landroid/graphics/PointF;

    const-string v1, "it"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result p1

    invoke-static {v0, p1}, Lcom/squareup/cardcustomizations/geometry/PointKt;->times(Landroid/graphics/PointF;F)Landroid/graphics/PointF;

    move-result-object p1

    .line 355
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$animateToBounds$1;->$lastDistanceMoved:Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v0, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/PointF;

    .line 506
    new-instance v1, Landroid/graphics/PointF;

    iget v2, p1, Landroid/graphics/PointF;->x:F

    iget v3, p1, Landroid/graphics/PointF;->y:F

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 507
    iget v2, v0, Landroid/graphics/PointF;->x:F

    neg-float v2, v2

    iget v0, v0, Landroid/graphics/PointF;->y:F

    neg-float v0, v0

    invoke-virtual {v1, v2, v0}, Landroid/graphics/PointF;->offset(FF)V

    .line 356
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$animateToBounds$1;->$this_animateToBounds:Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getTransform()Landroid/graphics/Matrix;

    move-result-object v0

    iget v2, v1, Landroid/graphics/PointF;->x:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 357
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$animateToBounds$1;->$lastDistanceMoved:Lkotlin/jvm/internal/Ref$ObjectRef;

    iput-object p1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    return-void
.end method
