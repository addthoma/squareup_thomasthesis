.class public Lcom/squareup/cardcustomizations/signature/Signature$Glyph;
.super Ljava/lang/Object;
.source "Signature.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardcustomizations/signature/Signature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Glyph"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable<",
        "Lcom/squareup/cardcustomizations/signature/Point$Timestamped;",
        ">;"
    }
.end annotation


# instance fields
.field private final painter:Lcom/squareup/cardcustomizations/signature/GlyphPainter;

.field private startTime:J


# direct methods
.method public constructor <init>(Lcom/squareup/cardcustomizations/signature/GlyphPainter;)V
    .locals 2

    .line 342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    .line 339
    iput-wide v0, p0, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;->startTime:J

    .line 343
    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;->painter:Lcom/squareup/cardcustomizations/signature/GlyphPainter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cardcustomizations/signature/Signature$Glyph;)Lcom/squareup/cardcustomizations/signature/GlyphPainter;
    .locals 0

    .line 338
    iget-object p0, p0, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;->painter:Lcom/squareup/cardcustomizations/signature/GlyphPainter;

    return-object p0
.end method


# virtual methods
.method public add(Lcom/squareup/cardcustomizations/signature/Point$Timestamped;)V
    .locals 5

    .line 348
    iget-wide v0, p0, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;->startTime:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    iget-wide v0, p1, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->time:J

    iput-wide v0, p0, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;->startTime:J

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;->painter:Lcom/squareup/cardcustomizations/signature/GlyphPainter;

    invoke-interface {v0, p1}, Lcom/squareup/cardcustomizations/signature/GlyphPainter;->addPoint(Lcom/squareup/cardcustomizations/signature/Point$Timestamped;)V

    return-void
.end method

.method public finish()V
    .locals 1

    .line 354
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;->painter:Lcom/squareup/cardcustomizations/signature/GlyphPainter;

    invoke-interface {v0}, Lcom/squareup/cardcustomizations/signature/GlyphPainter;->finish()V

    return-void
.end method

.method public getPointCount()I
    .locals 1

    .line 364
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;->painter:Lcom/squareup/cardcustomizations/signature/GlyphPainter;

    invoke-interface {v0}, Lcom/squareup/cardcustomizations/signature/GlyphPainter;->getPointCount()I

    move-result v0

    return v0
.end method

.method public getStartTime()J
    .locals 2

    .line 359
    iget-wide v0, p0, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;->startTime:J

    return-wide v0
.end method

.method public invalidate(Landroid/view/View;)V
    .locals 1

    .line 372
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;->painter:Lcom/squareup/cardcustomizations/signature/GlyphPainter;

    invoke-interface {v0, p1}, Lcom/squareup/cardcustomizations/signature/GlyphPainter;->invalidate(Landroid/view/View;)V

    return-void
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/squareup/cardcustomizations/signature/Point$Timestamped;",
            ">;"
        }
    .end annotation

    .line 380
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;->painter:Lcom/squareup/cardcustomizations/signature/GlyphPainter;

    invoke-interface {v0}, Lcom/squareup/cardcustomizations/signature/GlyphPainter;->points()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public points()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/signature/Point$Timestamped;",
            ">;"
        }
    .end annotation

    .line 376
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;->painter:Lcom/squareup/cardcustomizations/signature/GlyphPainter;

    invoke-interface {v0}, Lcom/squareup/cardcustomizations/signature/GlyphPainter;->points()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
