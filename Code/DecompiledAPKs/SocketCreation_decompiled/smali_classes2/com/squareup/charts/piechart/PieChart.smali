.class public final Lcom/squareup/charts/piechart/PieChart;
.super Landroid/view/View;
.source "PieChart.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/charts/piechart/PieChart$Update;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPieChart.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PieChart.kt\ncom/squareup/charts/piechart/PieChart\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,131:1\n1642#2,2:132\n*E\n*S KotlinDebug\n*F\n+ 1 PieChart.kt\ncom/squareup/charts/piechart/PieChart\n*L\n114#1,2:132\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001/B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0016\u0010\u001d\u001a\u00020\u001e2\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0017H\u0002J\u0008\u0010\u001f\u001a\u00020\u000eH\u0002J\u0008\u0010 \u001a\u00020\u001eH\u0016J\u0008\u0010!\u001a\u00020\u001eH\u0014J\u0010\u0010\"\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020$H\u0014J\u0018\u0010%\u001a\u00020\u001e2\u0006\u0010&\u001a\u00020\u00072\u0006\u0010\'\u001a\u00020\u0007H\u0014J\u0006\u0010(\u001a\u00020\u001eJ\u0006\u0010)\u001a\u00020\u001eJ\u001f\u0010*\u001a\u00020\u001e2\u0017\u0010+\u001a\u0013\u0012\u0004\u0012\u00020-\u0012\u0004\u0012\u00020\u001e0,\u00a2\u0006\u0002\u0008.R\u001e\u0010\u000b\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n@BX\u0086\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u001e\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\t\u001a\u00020\u000e@BX\u0086\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u001e\u0010\u0013\u001a\u00020\u00122\u0006\u0010\t\u001a\u00020\u0012@BX\u0086\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R0\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u00172\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0017@BX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0019\u0010\u001a\"\u0004\u0008\u001b\u0010\u001c\u00a8\u00060"
    }
    d2 = {
        "Lcom/squareup/charts/piechart/PieChart;",
        "Landroid/view/View;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "<set-?>",
        "Lcom/squareup/charts/piechart/animators/PieChartAnimator;",
        "animator",
        "getAnimator",
        "()Lcom/squareup/charts/piechart/animators/PieChartAnimator;",
        "Lcom/squareup/charts/piechart/Slice;",
        "backgroundSlice",
        "getBackgroundSlice",
        "()Lcom/squareup/charts/piechart/Slice;",
        "Lcom/squareup/charts/piechart/renderers/PieChartRenderer;",
        "renderer",
        "getRenderer",
        "()Lcom/squareup/charts/piechart/renderers/PieChartRenderer;",
        "value",
        "",
        "slices",
        "getSlices",
        "()Ljava/util/List;",
        "setSlices",
        "(Ljava/util/List;)V",
        "checkForSliceLimits",
        "",
        "defaultBackgroundSlice",
        "invalidate",
        "onDetachedFromWindow",
        "onDraw",
        "canvas",
        "Landroid/graphics/Canvas;",
        "onMeasure",
        "widthMeasureSpec",
        "heightMeasureSpec",
        "startAnimation",
        "stopAnimation",
        "update",
        "block",
        "Lkotlin/Function1;",
        "Lcom/squareup/charts/piechart/PieChart$Update;",
        "Lkotlin/ExtensionFunctionType;",
        "Update",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private animator:Lcom/squareup/charts/piechart/animators/PieChartAnimator;

.field private backgroundSlice:Lcom/squareup/charts/piechart/Slice;

.field private renderer:Lcom/squareup/charts/piechart/renderers/PieChartRenderer;

.field private slices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/charts/piechart/Slice;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/charts/piechart/PieChart;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/charts/piechart/PieChart;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/charts/piechart/PieChart;->slices:Ljava/util/List;

    .line 58
    invoke-direct {p0}, Lcom/squareup/charts/piechart/PieChart;->defaultBackgroundSlice()Lcom/squareup/charts/piechart/Slice;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/charts/piechart/PieChart;->backgroundSlice:Lcom/squareup/charts/piechart/Slice;

    .line 60
    new-instance p1, Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;

    invoke-direct {p1, p0}, Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;-><init>(Lcom/squareup/charts/piechart/PieChart;)V

    check-cast p1, Lcom/squareup/charts/piechart/animators/PieChartAnimator;

    iput-object p1, p0, Lcom/squareup/charts/piechart/PieChart;->animator:Lcom/squareup/charts/piechart/animators/PieChartAnimator;

    .line 62
    new-instance p1, Lcom/squareup/charts/piechart/renderers/FullPieChartRenderer;

    invoke-direct {p1}, Lcom/squareup/charts/piechart/renderers/FullPieChartRenderer;-><init>()V

    check-cast p1, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;

    iput-object p1, p0, Lcom/squareup/charts/piechart/PieChart;->renderer:Lcom/squareup/charts/piechart/renderers/PieChartRenderer;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 43
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 44
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/charts/piechart/PieChart;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final checkForSliceLimits(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/charts/piechart/Slice;",
            ">;)V"
        }
    .end annotation

    .line 114
    check-cast p1, Ljava/lang/Iterable;

    .line 132
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/charts/piechart/Slice;

    .line 114
    invoke-virtual {v1}, Lcom/squareup/charts/piechart/Slice;->getLength()F

    move-result v1

    add-float/2addr v0, v1

    goto :goto_0

    :cond_0
    const/high16 p1, 0x42c80000    # 100.0f

    cmpg-float p1, v0, p1

    if-gtz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_2

    return-void

    .line 118
    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Some of all slices are not allowed to exceed 100%. Currently the sum is "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v0, "%."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 117
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final defaultBackgroundSlice()Lcom/squareup/charts/piechart/Slice;
    .locals 4

    .line 124
    new-instance v0, Lcom/squareup/charts/piechart/Slice;

    .line 126
    new-instance v1, Lcom/squareup/charts/piechart/SliceColor$SolidColor;

    invoke-virtual {p0}, Lcom/squareup/charts/piechart/PieChart;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "context"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/charts/piechart/R$color;->default_background_slice_color:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Lcom/squareup/charts/piechart/SliceColor$SolidColor;-><init>(I)V

    check-cast v1, Lcom/squareup/charts/piechart/SliceColor;

    const/high16 v2, 0x42c80000    # 100.0f

    const/high16 v3, 0x41700000    # 15.0f

    .line 124
    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/charts/piechart/Slice;-><init>(FFLcom/squareup/charts/piechart/SliceColor;)V

    return-object v0
.end method

.method private final setSlices(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/charts/piechart/Slice;",
            ">;)V"
        }
    .end annotation

    .line 55
    invoke-direct {p0, p1}, Lcom/squareup/charts/piechart/PieChart;->checkForSliceLimits(Ljava/util/List;)V

    .line 56
    iput-object p1, p0, Lcom/squareup/charts/piechart/PieChart;->slices:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final getAnimator()Lcom/squareup/charts/piechart/animators/PieChartAnimator;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/charts/piechart/PieChart;->animator:Lcom/squareup/charts/piechart/animators/PieChartAnimator;

    return-object v0
.end method

.method public final getBackgroundSlice()Lcom/squareup/charts/piechart/Slice;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/charts/piechart/PieChart;->backgroundSlice:Lcom/squareup/charts/piechart/Slice;

    return-object v0
.end method

.method public final getRenderer()Lcom/squareup/charts/piechart/renderers/PieChartRenderer;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/charts/piechart/PieChart;->renderer:Lcom/squareup/charts/piechart/renderers/PieChartRenderer;

    return-object v0
.end method

.method public final getSlices()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/charts/piechart/Slice;",
            ">;"
        }
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/squareup/charts/piechart/PieChart;->slices:Ljava/util/List;

    return-object v0
.end method

.method public invalidate()V
    .locals 5

    .line 89
    iget-object v0, p0, Lcom/squareup/charts/piechart/PieChart;->renderer:Lcom/squareup/charts/piechart/renderers/PieChartRenderer;

    invoke-virtual {p0}, Lcom/squareup/charts/piechart/PieChart;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/charts/piechart/PieChart;->getMeasuredHeight()I

    move-result v2

    iget-object v3, p0, Lcom/squareup/charts/piechart/PieChart;->backgroundSlice:Lcom/squareup/charts/piechart/Slice;

    iget-object v4, p0, Lcom/squareup/charts/piechart/PieChart;->slices:Ljava/util/List;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;->notifyRemeasure(IILcom/squareup/charts/piechart/Slice;Ljava/util/List;)V

    .line 91
    invoke-super {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .line 108
    invoke-virtual {p0}, Lcom/squareup/charts/piechart/PieChart;->stopAnimation()V

    .line 109
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/charts/piechart/PieChart;->renderer:Lcom/squareup/charts/piechart/renderers/PieChartRenderer;

    iget-object v1, p0, Lcom/squareup/charts/piechart/PieChart;->backgroundSlice:Lcom/squareup/charts/piechart/Slice;

    iget-object v2, p0, Lcom/squareup/charts/piechart/PieChart;->slices:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/charts/piechart/PieChart;->animator:Lcom/squareup/charts/piechart/animators/PieChartAnimator;

    invoke-interface {v3}, Lcom/squareup/charts/piechart/animators/PieChartAnimator;->progress()F

    move-result v3

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;->onDrawPieChart(Landroid/graphics/Canvas;Lcom/squareup/charts/piechart/Slice;Ljava/util/List;F)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .line 98
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 100
    iget-object p1, p0, Lcom/squareup/charts/piechart/PieChart;->renderer:Lcom/squareup/charts/piechart/renderers/PieChartRenderer;

    invoke-virtual {p0}, Lcom/squareup/charts/piechart/PieChart;->getMeasuredWidth()I

    move-result p2

    invoke-virtual {p0}, Lcom/squareup/charts/piechart/PieChart;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/charts/piechart/PieChart;->backgroundSlice:Lcom/squareup/charts/piechart/Slice;

    iget-object v2, p0, Lcom/squareup/charts/piechart/PieChart;->slices:Ljava/util/List;

    invoke-virtual {p1, p2, v0, v1, v2}, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;->notifyRemeasure(IILcom/squareup/charts/piechart/Slice;Ljava/util/List;)V

    return-void
.end method

.method public final startAnimation()V
    .locals 1

    .line 79
    invoke-virtual {p0}, Lcom/squareup/charts/piechart/PieChart;->invalidate()V

    .line 81
    iget-object v0, p0, Lcom/squareup/charts/piechart/PieChart;->animator:Lcom/squareup/charts/piechart/animators/PieChartAnimator;

    invoke-interface {v0}, Lcom/squareup/charts/piechart/animators/PieChartAnimator;->startAnimating()V

    return-void
.end method

.method public final stopAnimation()V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/charts/piechart/PieChart;->animator:Lcom/squareup/charts/piechart/animators/PieChartAnimator;

    invoke-interface {v0}, Lcom/squareup/charts/piechart/animators/PieChartAnimator;->stopAnimating()V

    return-void
.end method

.method public final update(Lkotlin/jvm/functions/Function1;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/charts/piechart/PieChart$Update;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    new-instance v0, Lcom/squareup/charts/piechart/PieChart$Update;

    iget-object v1, p0, Lcom/squareup/charts/piechart/PieChart;->backgroundSlice:Lcom/squareup/charts/piechart/Slice;

    iget-object v2, p0, Lcom/squareup/charts/piechart/PieChart;->slices:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/charts/piechart/PieChart;->animator:Lcom/squareup/charts/piechart/animators/PieChartAnimator;

    iget-object v4, p0, Lcom/squareup/charts/piechart/PieChart;->renderer:Lcom/squareup/charts/piechart/renderers/PieChartRenderer;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/charts/piechart/PieChart$Update;-><init>(Lcom/squareup/charts/piechart/Slice;Ljava/util/List;Lcom/squareup/charts/piechart/animators/PieChartAnimator;Lcom/squareup/charts/piechart/renderers/PieChartRenderer;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    invoke-virtual {v0}, Lcom/squareup/charts/piechart/PieChart$Update;->getBackgroundSlice()Lcom/squareup/charts/piechart/Slice;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/charts/piechart/PieChart;->backgroundSlice:Lcom/squareup/charts/piechart/Slice;

    .line 71
    invoke-virtual {v0}, Lcom/squareup/charts/piechart/PieChart$Update;->getSlices()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/charts/piechart/PieChart;->setSlices(Ljava/util/List;)V

    .line 72
    invoke-virtual {v0}, Lcom/squareup/charts/piechart/PieChart$Update;->getAnimator()Lcom/squareup/charts/piechart/animators/PieChartAnimator;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/charts/piechart/PieChart;->animator:Lcom/squareup/charts/piechart/animators/PieChartAnimator;

    .line 73
    invoke-virtual {v0}, Lcom/squareup/charts/piechart/PieChart$Update;->getRenderer()Lcom/squareup/charts/piechart/renderers/PieChartRenderer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/charts/piechart/PieChart;->renderer:Lcom/squareup/charts/piechart/renderers/PieChartRenderer;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/charts/piechart/PieChart;->invalidate()V

    return-void
.end method
