.class public final synthetic Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$7JOYQDMPhAd6OhSwRrcdZjuoBfE;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/catalog/CatalogServiceEndpoint;

.field private final synthetic f$1:Lsquareup/items/merchant/BatchRequest;

.field private final synthetic f$2:Lcom/squareup/catalog/EditItemVariationsState;

.field private final synthetic f$3:Lcom/squareup/cogs/Cogs;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/catalog/CatalogServiceEndpoint;Lsquareup/items/merchant/BatchRequest;Lcom/squareup/catalog/EditItemVariationsState;Lcom/squareup/cogs/Cogs;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$7JOYQDMPhAd6OhSwRrcdZjuoBfE;->f$0:Lcom/squareup/catalog/CatalogServiceEndpoint;

    iput-object p2, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$7JOYQDMPhAd6OhSwRrcdZjuoBfE;->f$1:Lsquareup/items/merchant/BatchRequest;

    iput-object p3, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$7JOYQDMPhAd6OhSwRrcdZjuoBfE;->f$2:Lcom/squareup/catalog/EditItemVariationsState;

    iput-object p4, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$7JOYQDMPhAd6OhSwRrcdZjuoBfE;->f$3:Lcom/squareup/cogs/Cogs;

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 4

    iget-object v0, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$7JOYQDMPhAd6OhSwRrcdZjuoBfE;->f$0:Lcom/squareup/catalog/CatalogServiceEndpoint;

    iget-object v1, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$7JOYQDMPhAd6OhSwRrcdZjuoBfE;->f$1:Lsquareup/items/merchant/BatchRequest;

    iget-object v2, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$7JOYQDMPhAd6OhSwRrcdZjuoBfE;->f$2:Lcom/squareup/catalog/EditItemVariationsState;

    iget-object v3, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$7JOYQDMPhAd6OhSwRrcdZjuoBfE;->f$3:Lcom/squareup/cogs/Cogs;

    check-cast p1, Lsquareup/items/merchant/BatchResponse;

    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/squareup/catalog/CatalogServiceEndpoint;->lambda$null$10$CatalogServiceEndpoint(Lsquareup/items/merchant/BatchRequest;Lcom/squareup/catalog/EditItemVariationsState;Lcom/squareup/cogs/Cogs;Lsquareup/items/merchant/BatchResponse;)V

    return-void
.end method
