.class public Lcom/squareup/catalog/EditItemVariationsState;
.super Ljava/lang/Object;
.source "EditItemVariationsState.java"


# instance fields
.field public final converter:Lcom/squareup/catalog/CatalogObjectConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/CatalogObjectConverter<",
            "Lcom/squareup/api/items/ItemVariation;",
            ">;"
        }
    .end annotation
.end field

.field public final created:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;"
        }
    .end annotation
.end field

.field public final deleted:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;"
        }
    .end annotation
.end field

.field public final deletedIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final edited:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;"
        }
    .end annotation
.end field

.field private final hasExistingVariationOverrideFieldChanged:Z

.field private final hasOverrideFieldChanged:Z


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;ZZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;ZZZ)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/catalog/EditItemVariationsState;->created:Ljava/util/List;

    .line 40
    iput-object p2, p0, Lcom/squareup/catalog/EditItemVariationsState;->edited:Ljava/util/List;

    .line 41
    iput-object p3, p0, Lcom/squareup/catalog/EditItemVariationsState;->deleted:Ljava/util/List;

    .line 43
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/catalog/EditItemVariationsState;->deletedIds:Ljava/util/List;

    .line 44
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 45
    iget-object p3, p0, Lcom/squareup/catalog/EditItemVariationsState;->deletedIds:Ljava/util/List;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 47
    :cond_0
    iput-boolean p4, p0, Lcom/squareup/catalog/EditItemVariationsState;->hasOverrideFieldChanged:Z

    .line 48
    iput-boolean p5, p0, Lcom/squareup/catalog/EditItemVariationsState;->hasExistingVariationOverrideFieldChanged:Z

    .line 49
    new-instance p1, Lcom/squareup/catalog/-$$Lambda$EditItemVariationsState$9MGDD-Asu5maHY66mtcpKsdK0ik;

    invoke-direct {p1, p6}, Lcom/squareup/catalog/-$$Lambda$EditItemVariationsState$9MGDD-Asu5maHY66mtcpKsdK0ik;-><init>(Z)V

    invoke-static {p1}, Lcom/squareup/catalog/ItemVariationConverter;->get(Lcom/squareup/catalog/ItemVariationConverter$ItemVariationConverterConfig;)Lcom/squareup/catalog/ItemVariationConverter;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/catalog/EditItemVariationsState;->converter:Lcom/squareup/catalog/CatalogObjectConverter;

    return-void
.end method

.method static synthetic lambda$new$0(Z)Z
    .locals 0

    return p0
.end method


# virtual methods
.method public getEditedAndDeletedIds()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 58
    iget-object v1, p0, Lcom/squareup/catalog/EditItemVariationsState;->edited:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 59
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 61
    :cond_0
    iget-object v1, p0, Lcom/squareup/catalog/EditItemVariationsState;->deletedIds:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object v0
.end method

.method public getModifiedItemVariationsSize()I
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/catalog/EditItemVariationsState;->created:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/catalog/EditItemVariationsState;->edited:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/squareup/catalog/EditItemVariationsState;->deletedIds:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public hasExistingVariationOverrideFieldChanged()Z
    .locals 1

    .line 70
    iget-boolean v0, p0, Lcom/squareup/catalog/EditItemVariationsState;->hasExistingVariationOverrideFieldChanged:Z

    return v0
.end method

.method public hasOverrideFieldChanged()Z
    .locals 1

    .line 66
    iget-boolean v0, p0, Lcom/squareup/catalog/EditItemVariationsState;->hasOverrideFieldChanged:Z

    return v0
.end method

.method public hasStateChanged()Z
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/catalog/EditItemVariationsState;->created:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/catalog/EditItemVariationsState;->edited:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/catalog/EditItemVariationsState;->deletedIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method
