.class public Lcom/squareup/catalog/event/CatalogFeatureInteraction;
.super Lcom/squareup/eventstream/v2/AppEvent;
.source "CatalogFeatureInteraction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;
    }
.end annotation


# static fields
.field private static final CATALOG_NAME:Ljava/lang/String; = "catalog_feature_interaction"


# instance fields
.field private final catalog_feature_interaction_details:Ljava/lang/String;

.field private final catalog_feature_interaction_feature_name:Ljava/lang/String;

.field private final catalog_feature_interaction_id:Ljava/lang/Integer;

.field private final catalog_feature_interaction_source:Ljava/lang/String;

.field private final catalog_feature_interaction_token:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/catalog/event/CatalogFeature;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "catalog_feature_interaction"

    .line 34
    invoke-direct {p0, v0}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    .line 35
    invoke-virtual {p1}, Lcom/squareup/catalog/event/CatalogFeature;->getEventValue()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/catalog/event/CatalogFeatureInteraction;->catalog_feature_interaction_feature_name:Ljava/lang/String;

    .line 36
    iput-object p2, p0, Lcom/squareup/catalog/event/CatalogFeatureInteraction;->catalog_feature_interaction_token:Ljava/lang/String;

    .line 37
    iput-object p3, p0, Lcom/squareup/catalog/event/CatalogFeatureInteraction;->catalog_feature_interaction_id:Ljava/lang/Integer;

    .line 38
    iput-object p4, p0, Lcom/squareup/catalog/event/CatalogFeatureInteraction;->catalog_feature_interaction_details:Ljava/lang/String;

    .line 39
    iput-object p5, p0, Lcom/squareup/catalog/event/CatalogFeatureInteraction;->catalog_feature_interaction_source:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/catalog/event/CatalogFeature;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/catalog/event/CatalogFeatureInteraction$1;)V
    .locals 0

    .line 11
    invoke-direct/range {p0 .. p5}, Lcom/squareup/catalog/event/CatalogFeatureInteraction;-><init>(Lcom/squareup/catalog/event/CatalogFeature;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 43
    instance-of v0, p1, Lcom/squareup/catalog/event/CatalogFeatureInteraction;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 47
    :cond_0
    check-cast p1, Lcom/squareup/catalog/event/CatalogFeatureInteraction;

    iget-object v0, p1, Lcom/squareup/catalog/event/CatalogFeatureInteraction;->catalog_feature_interaction_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/catalog/event/CatalogFeatureInteraction;->catalog_feature_interaction_token:Ljava/lang/String;

    invoke-static {v0, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/squareup/catalog/event/CatalogFeatureInteraction;->catalog_feature_interaction_feature_name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/catalog/event/CatalogFeatureInteraction;->catalog_feature_interaction_feature_name:Ljava/lang/String;

    .line 50
    invoke-static {v0, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/squareup/catalog/event/CatalogFeatureInteraction;->catalog_feature_interaction_id:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/catalog/event/CatalogFeatureInteraction;->catalog_feature_interaction_id:Ljava/lang/Integer;

    .line 53
    invoke-static {v0, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/squareup/catalog/event/CatalogFeatureInteraction;->catalog_feature_interaction_details:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/catalog/event/CatalogFeatureInteraction;->catalog_feature_interaction_details:Ljava/lang/String;

    .line 55
    invoke-static {v0, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p1, Lcom/squareup/catalog/event/CatalogFeatureInteraction;->catalog_feature_interaction_source:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/catalog/event/CatalogFeatureInteraction;->catalog_feature_interaction_source:Ljava/lang/String;

    .line 57
    invoke-static {p1, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method
