.class public final synthetic Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$VuY22wu18kjUFGdFUE2Ab1itKvY;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/catalog/CatalogServiceEndpoint;

.field private final synthetic f$1:Ljava/util/List;

.field private final synthetic f$2:Lcom/squareup/shared/catalog/models/CatalogItem;

.field private final synthetic f$3:Lcom/squareup/catalog/EditItemVariationsState;

.field private final synthetic f$4:Z

.field private final synthetic f$5:Lcom/squareup/cogs/Cogs;

.field private final synthetic f$6:Ljava/lang/String;

.field private final synthetic f$7:Ljava/util/List;

.field private final synthetic f$8:Z


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/catalog/CatalogServiceEndpoint;Ljava/util/List;Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;ZLcom/squareup/cogs/Cogs;Ljava/lang/String;Ljava/util/List;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$VuY22wu18kjUFGdFUE2Ab1itKvY;->f$0:Lcom/squareup/catalog/CatalogServiceEndpoint;

    iput-object p2, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$VuY22wu18kjUFGdFUE2Ab1itKvY;->f$1:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$VuY22wu18kjUFGdFUE2Ab1itKvY;->f$2:Lcom/squareup/shared/catalog/models/CatalogItem;

    iput-object p4, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$VuY22wu18kjUFGdFUE2Ab1itKvY;->f$3:Lcom/squareup/catalog/EditItemVariationsState;

    iput-boolean p5, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$VuY22wu18kjUFGdFUE2Ab1itKvY;->f$4:Z

    iput-object p6, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$VuY22wu18kjUFGdFUE2Ab1itKvY;->f$5:Lcom/squareup/cogs/Cogs;

    iput-object p7, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$VuY22wu18kjUFGdFUE2Ab1itKvY;->f$6:Ljava/lang/String;

    iput-object p8, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$VuY22wu18kjUFGdFUE2Ab1itKvY;->f$7:Ljava/util/List;

    iput-boolean p9, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$VuY22wu18kjUFGdFUE2Ab1itKvY;->f$8:Z

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 10

    iget-object v0, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$VuY22wu18kjUFGdFUE2Ab1itKvY;->f$0:Lcom/squareup/catalog/CatalogServiceEndpoint;

    iget-object v1, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$VuY22wu18kjUFGdFUE2Ab1itKvY;->f$1:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$VuY22wu18kjUFGdFUE2Ab1itKvY;->f$2:Lcom/squareup/shared/catalog/models/CatalogItem;

    iget-object v3, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$VuY22wu18kjUFGdFUE2Ab1itKvY;->f$3:Lcom/squareup/catalog/EditItemVariationsState;

    iget-boolean v4, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$VuY22wu18kjUFGdFUE2Ab1itKvY;->f$4:Z

    iget-object v5, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$VuY22wu18kjUFGdFUE2Ab1itKvY;->f$5:Lcom/squareup/cogs/Cogs;

    iget-object v6, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$VuY22wu18kjUFGdFUE2Ab1itKvY;->f$6:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$VuY22wu18kjUFGdFUE2Ab1itKvY;->f$7:Ljava/util/List;

    iget-boolean v8, p0, Lcom/squareup/catalog/-$$Lambda$CatalogServiceEndpoint$VuY22wu18kjUFGdFUE2Ab1itKvY;->f$8:Z

    move-object v9, p1

    check-cast v9, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual/range {v0 .. v9}, Lcom/squareup/catalog/CatalogServiceEndpoint;->lambda$retrieveV3ItemAndItemVariationsThenSave$9$CatalogServiceEndpoint(Ljava/util/List;Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/catalog/EditItemVariationsState;ZLcom/squareup/cogs/Cogs;Ljava/lang/String;Ljava/util/List;ZLcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    return-void
.end method
