.class final Lcom/squareup/catalog/CatalogAttributeDefinition$2;
.super Lcom/squareup/catalog/WireEnumAttributeEditor;
.source "CatalogAttributeDefinition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/catalog/CatalogAttributeDefinition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/catalog/WireEnumAttributeEditor<",
        "Lcom/squareup/api/items/PricingType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/squareup/catalog/WireEnumAttributeEditor;-><init>()V

    return-void
.end method


# virtual methods
.method public fromValue(I)Lcom/squareup/api/items/PricingType;
    .locals 0

    .line 36
    invoke-static {p1}, Lcom/squareup/api/items/PricingType;->fromValue(I)Lcom/squareup/api/items/PricingType;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 33
    invoke-virtual {p0, p1}, Lcom/squareup/catalog/CatalogAttributeDefinition$2;->fromValue(I)Lcom/squareup/api/items/PricingType;

    move-result-object p1

    return-object p1
.end method
