.class public interface abstract Lcom/squareup/analytics/DeepLinkHelper;
.super Ljava/lang/Object;
.source "DeepLinkHelper.java"


# virtual methods
.method public abstract anonymousVisitorToken()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract deepLinkPath()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract encryptedEmail()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract init()V
.end method

.method public abstract onActivityCreate(Landroid/app/Activity;)V
.end method
