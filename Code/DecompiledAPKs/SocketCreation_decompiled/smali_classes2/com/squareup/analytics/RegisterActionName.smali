.class public final enum Lcom/squareup/analytics/RegisterActionName;
.super Ljava/lang/Enum;
.source "RegisterActionName.java"

# interfaces
.implements Lcom/squareup/analytics/EventNamedAction;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/analytics/RegisterActionName;",
        ">;",
        "Lcom/squareup/analytics/EventNamedAction;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ACCOUNT_OWNER_PASSCODE_AUTHORIZATION_PROMPT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ACTIVATION_CONTACTLESS_ORDER_SUCCEED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ACTIVITY_SEARCH:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ADVANCED_MODIFIER_HIDE_MODIFIER_FROM_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ADVANCED_MODIFIER_MIN_MAX_MODIFIER_USED_IN_TRANSACTION:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ADVANCED_MODIFIER_PRE_SELECTED_MODIFIER_USED_IN_TRANSACTION:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ADVANCED_MODIFIER_SKIP_ITEM_MODAL:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ADVANCED_MODIFIER_SKIP_ITEM_MODAL_OPEN_ITEM_IN_CART:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ADVANCED_MODIFIER_SKIP_ITEM_MODAL_STILL_SHOWN:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum API_BUYER_STARTED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum API_NEW_READER_SETTINGS:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum API_NEW_STORE_CARD:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum API_NEW_TRANSACTION:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum API_READER_SETTINGS_FAILURE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum API_READER_SETTINGS_STARTED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum API_READER_SETTINGS_SUCCESS:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum API_STORE_CARD_FAILURE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum API_STORE_CARD_STARTED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum API_STORE_CARD_SUCCESS:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum API_TENDER_STARTED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum API_TRANSACTION_FAILURE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum API_TRANSACTION_SUCCESS:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum APPLET_SWITCHER_DID_CLOSE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum APPLET_SWITCHER_DID_OPEN:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum APPLET_SWITCHER_SELECTED_APPLET:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum APPOINTMENT_CREATE_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum AUDIO_BLOCKING_DIALOG:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum AUTOMATICALLY_PRINT_ITEMIZED_RECEIPT_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum AVAILABLE_EMAILS:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum BACKGROUND_JOB_LOG:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum BARCODE_SCANNED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum BARCODE_SCANNER_CONNECT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum BARCODE_SCANNER_DISCONNECT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CARD_ON_FILE_ADD_CARD:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CARD_ON_FILE_ADD_CARD_POST_TRANSACTION:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CARD_ON_FILE_CHARGE_ATTEMPT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CARD_ON_FILE_CHARGE_FAILURE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CARD_ON_FILE_CHARGE_SUCCESS:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CARD_ON_FILE_REMOVE_CARD:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CASH_DRAWERS_OPENED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CASH_DRAWER_CONNECTED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CASH_DRAWER_DISCONNECTED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CASH_DRAWER_FAILED_TO_CONNECT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CASH_MANAGEMENT_DRAWER_CLOSED_FROM_ENDED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CASH_MANAGEMENT_DRAWER_CLOSED_FROM_STARTED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CASH_MANAGEMENT_DRAWER_ENDED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CASH_MANAGEMENT_DRAWER_OPENED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CASH_MANAGEMENT_END_DRAWER_CLOSE_AUTOMATICALLY:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CASH_MANAGEMENT_END_DRAWER_CLOSE_MANUALLY:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CASH_MANAGEMENT_START_SHIFT_DRAWER_MODAL:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CASH_PAYMENT_TUTORIAL_DECLINED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CHECKOUT_ADD_GIFT_CARD:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CHECKOUT_INFORMATION:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CHECKOUT_ITEMIZED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CLIENT_ACTION_TRANSLATION_HANDLED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CLIENT_ACTION_TRANSLATION_NO_TRANSLATOR:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CLIENT_ACTION_TRANSLATION_URL_NOT_HANDLED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum COMP_CART_CANCELED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum COMP_CART_COMPED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum COMP_CART_STARTED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum COMP_CART_UNCOMPED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum COMP_ITEMIZATION_CANCELED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum COMP_ITEMIZATION_COMPED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum COMP_ITEMIZATION_STARTED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum COMP_ITEMIZATION_UNCOMPED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum COUPONS_SEARCH:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CREATE_ITEM_TUTORIAL_DRAG_ITEM:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CREATE_ITEM_TUTORIAL_ITEM_NAME_ENTERED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CREATE_ITEM_TUTORIAL_PRICE_ENTERED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_ADD_CUSTOMER_TO_SALE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_CREATE_CONTACT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_CUSTOMER_MANAGEMENT_AFTER_CHECKOUT_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_CUSTOMER_MANAGEMENT_BEFORE_CHECKOUT_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_CUSTOMER_MANAGEMENT_CARD_ON_FILE_POST_TRANSACTION_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_CUSTOMER_MANAGEMENT_CARD_ON_FILE_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_EDIT_CONTACT_SAVED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_EMAIL_COLLECTION_SCREEN_NEW_SALE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_EMAIL_COLLECTION_SCREEN_NO_THANKS:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_EMAIL_COLLECTION_SCREEN_SUBMIT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_EMAIL_COLLECTION_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_GET_CONTACT_NULL:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_MESSAGING_CREATE_CONVERSATION_SEND:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_MESSAGING_SHOW_CONVERSATION_SEND:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_REMOVE_CUSTOMER_FROM_MODAL:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_FILTER_ADD_FILTER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_FILTER_APPLY:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_FILTER_CLOSE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_FILTER_REMOVE_ALL:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_FILTER_REMOVE_FILTER_PILL:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_FILTER_SAVE_AS_GROUP:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_GROUPS_CLOSE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_GROUPS_CREATE_NEW:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_GROUPS_DELETE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_GROUPS_SAVE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_GROUPS_SELECT_AUTO_SMART_GROUP:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_GROUPS_SELECT_MANUAL_GROUP:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_GROUPS_SELECT_SMART_GROUP:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_MULTI_SELECT_DELETE_CANCEL:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_MULTI_SELECT_DELETE_DELETE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_MULTI_SELECT_MERGE_CANCEL:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_MULTI_SELECT_MERGE_MERGE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_ADD_CARD:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_ADD_FILES:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_ADD_NOTE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_EDIT_FILE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_EDIT_NOTE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_EDIT_PERSONAL:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_MENU_ADD_APPOINTMENT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_MENU_ADD_CARD:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_MENU_ADD_NOTE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_MENU_ADD_TO_SALE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_MENU_DELETE_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_MENU_EDIT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_MENU_MERGE_PROFILE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_MENU_NEW_SALE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_MENU_REMOVE_FROM_ESTIMATE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_MENU_REMOVE_FROM_INVOICE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_MENU_REMOVE_FROM_SALE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_MENU_SEND_MESSAGE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_MENU_UPLOAD_FILE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_REMOVE_CARD:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_TRANSFER_LOYALTY:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_VIEW_ALL_ACTIVITY:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_VIEW_ALL_COUPONS:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_VIEW_ALL_FILES:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CRM_V2_VIEW_PROFILE_VIEW_ALL_FREQUENT_ITEMS:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum CUSTOMER_CHECKOUT_ENTER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum DEEP_LINK_BRANCH:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum DEEP_LINK_HANDLED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum DEEP_LINK_REJECTED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum EDIT_FAVORITES_PAGE_NAME:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum EMPLOYEE_ACCESS_DENIED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum FEE_TUTORIAL_DIP_DISMISS:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum FEE_TUTORIAL_DIP_JCB_DISMISS:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum FEE_TUTORIAL_INTERAC_DISMISS:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum FEE_TUTORIAL_MANUAL_DISMISS:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum FEE_TUTORIAL_SWIPE_DISMISS:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum FEE_TUTORIAL_TAP_DISMISS:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum GAP_TIME_MODIFIED_CART:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum GAP_TIME_MODIFIED_SERVICE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum INCREMENTAL_ITEMS_SYNC:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum INSTANT_DEPOSIT_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum INVOICES_APPLET_CANCEL_INVOICE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum INVOICES_APPLET_COPY_LINK:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum INVOICES_APPLET_CREATE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum INVOICES_APPLET_CREATE_SHARE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum INVOICES_APPLET_DELETE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum INVOICES_APPLET_DOWNLOAD_INVOICE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum INVOICES_APPLET_DUPLICATE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum INVOICES_APPLET_FILTER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum INVOICES_APPLET_PREVIEW:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum INVOICES_APPLET_SAVE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum INVOICES_APPLET_SCHEDULE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum INVOICES_APPLET_SEARCH:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum INVOICES_APPLET_SEND_INVOICE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum INVOICES_APPLET_SEND_RECURRING_SERIES:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum INVOICES_APPLET_SHARE_LINK:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum INVOICES_APPLET_VIEW_TIMELINE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum INVOICE_CHOOSE_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum INVOICE_CREATE_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ITEMS_APPLET_CATEGORY_CREATED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ITEMS_APPLET_CATEGORY_DELETED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ITEMS_APPLET_CATEGORY_EDITED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ITEMS_APPLET_DISCOUNT_CREATED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ITEMS_APPLET_DISCOUNT_DELETED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ITEMS_APPLET_DISCOUNT_EDITED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ITEMS_APPLET_EDIT_CATALOG_OBJECT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ITEMS_APPLET_GIFT_CARD_CREATED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ITEMS_APPLET_GIFT_CARD_DELETED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ITEMS_APPLET_GIFT_CARD_EDITED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ITEMS_APPLET_ITEM_CREATED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ITEMS_APPLET_ITEM_DELETED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ITEMS_APPLET_ITEM_EDITED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ITEMS_APPLET_MODIFIER_SET_CREATED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ITEMS_APPLET_MODIFIER_SET_DELETED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ITEMS_APPLET_MODIFIER_SET_EDITED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum LOG_OUT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum LOYALTY_CART_POINTS_MODAL_ADD_CUSTOMER_FLOW_STARTED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum LOYALTY_CART_POINTS_MODAL_APPLY_REWARD:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum LOYALTY_CART_POINTS_MODAL_CUSTOMER_ADDED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum LOYALTY_CART_POINTS_MODAL_OPENED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum LOYALTY_CART_POINTS_MODAL_REMOVE_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum LOYALTY_ENROLL_SUBMIT_BUYER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum LOYALTY_ENROLL_SUBMIT_MERCHANT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum LOYALTY_ENROLL_SUBMIT_SUCCESS_BUYER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum LOYALTY_ENROLL_SUBMIT_SUCCESS_MERCHANT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum LOYALTY_ENROLL_SUBMIT_TIMEOUT_BUYER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum LOYALTY_ENROLL_SUBMIT_TIMEOUT_MERCHANT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum LOYALTY_NO_THANKS_BUYER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum LOYALTY_NO_THANKS_MERCHANT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum LOYALTY_REPORT_CHANGE_DATE_RANGE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum NOTIFICATION_CENTER_ACCOUNT_VIEW:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum NOTIFICATION_CENTER_BROWSER_DIALOG_CANCEL:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum NOTIFICATION_CENTER_BROWSER_DIALOG_OPEN:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum NOTIFICATION_CENTER_LOADED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum NOTIFICATION_CENTER_NOTIFICATION_CLICK:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum NOTIFICATION_CENTER_NOTIFICATION_OPEN:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum NOTIFICATION_CENTER_PRODUCT_VIEW:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ONBOARDING_CONTINUE_TO_WEB:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ONBOARDING_LATER_IN_INITIAL_SHIPPING:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ONBOARDING_LATER_IN_MCC:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ONBOARDING_LATER_IN_WEB:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ONBOARDING_NOT_USE_PREFILL_EMAIL:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ONBOARDING_REDIRECT_DOWNLOAD:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ONBOARDING_WEB_ACTIVATION_COMPLETE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum OPEN_TICKETS_AS_HOME_SCREEN_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum OPEN_TICKETS_EXIT_EDIT_TICKET:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum OPEN_TICKETS_ITEMIZATION_VOID:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum OPEN_TICKETS_LOADED_EXISTING_TICKET:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum OPEN_TICKETS_MERGED_CART_TO_EXISTING_TICKET:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum OPEN_TICKETS_MERGE_TICKETS_AFTER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum OPEN_TICKETS_MERGE_TICKETS_BEFORE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum OPEN_TICKETS_MERGE_TICKETS_CANCELED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum OPEN_TICKETS_MOVE_TICKETS:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum OPEN_TICKETS_NEW_TICKET_WITHOUT_EXISTING_CART:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum OPEN_TICKETS_NEW_TICKET_WITH_EXISTING_CART:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum OPEN_TICKETS_SAVED_DISCOUNT_ONLY_CART:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum OPEN_TICKETS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum OPEN_TICKETS_UNSYNCED_TICKETS_UPLOADED_VIA_SWEEPER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum OPEN_TICKETS_VIEWED_LOCKED_ITEMIZATION:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum OPEN_TICKET_SAVED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum ORDER_ENTRY_CLEAR_SALE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum OWNER_PASSCODE_AUTHORIZATION_PROMPT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PAPER_SIGNATURE_PRINT_ADDITIONAL_AUTH_SLIP_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PAPER_SIGNATURE_QUICK_TIP_RECEIPT_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PAPER_SIGNATURE_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PASSCODE_AUTHORIZATION_ACCEPTED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PASSCODE_AUTHORIZATION_CANCELED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PASSCODE_AUTHORIZATION_FAILED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PASSCODE_AUTHORIZATION_PROMPT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PASSCODE_EMPLOYEE_MANAGEMENT_GUEST_MODE_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PASSCODE_EMPLOYEE_MANAGEMENT_SWITCH_EMPLOYEES:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PAYMENT_FLOW_QUICK_CASH_CUSTOM_OPTION:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PAYMENT_FLOW_QUICK_CASH_OPTION:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PAYMENT_FLOW_RECEIPT_VIEW_SELECT_EMAIL:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PAYMENT_TUTORIAL_DECLINED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PREDEFINED_TICKETS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PREDEFINED_TICKET_CONVERT_TO_CUSTOM:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PREDEFINED_TICKET_GROUP_CREATED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINTER_AUTONUMBER_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINTER_AUTONUMBER_TOGGLE_CANCEL:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINTER_BLOCKED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINTER_CATEGORY_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINTER_CONNECT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINTER_DISCONNECT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINTER_DISCONNECT_TCP_PING_HISTORY:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINTER_PRINT_A_TICKET_FOR_EACH_ITEM_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINTER_RECEIPTS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINTER_STUBS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINTER_TICKETS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINTER_UNCATEGORIZED_ITEMS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_AUTH_SLIP:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_AUTH_SLIP_COPY:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_AUTH_SLIP_COPY_PAPER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_AUTH_SLIP_COPY_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_AUTH_SLIP_PAPER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_AUTH_SLIP_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_BILL_PAPER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_CASH_REPORT_PAPER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_ERROR_REPRINT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_GIFT_RECEIPT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_JOB_ENQUEUED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_JOB_FAILED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_RECEIPT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_RECEIPT_PAPER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_RECEIPT_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_SALES_SUMMARY_PAPER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_SPOOLER_NO_HARDWARE_PRINTER_ERROR:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_SPOOLER_TARGET_DOES_NOT_EXIST_ERROR:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_SPOOLER_TARGET_HARDWARE_PRINTER_UNAVAILABLE_ERROR:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_STUB:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_STUB_PAPER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_STUB_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_TICKET:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_TICKET_PAPER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_TICKET_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_TIMECARDS_SUMMARY:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_VOID_TICKET:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_VOID_TICKET_PAPER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRINT_VOID_TICKET_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRODUCT_ACTIVATION:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum PRODUCT_SIGNUP:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum R12_MEET_THE_READER_MODAL_DISMISSED_BY_USER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum R12_MULTIPAGE_WALKTHROUGH_DISMISSED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum READER_SDK_INIT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum READER_SDK_LOGIN_FAILURE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum READER_SDK_LOGIN_REQUEST:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum READER_SDK_LOGIN_SUCCESS:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum RECEIPT_CLOSE_AUTOMATICALLY:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum REPORTS_EMAIL_SALES_REPORT_SENT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum REPORTS_TOGGLE_ALL_DAY_SETTING:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum REPORTS_TOGGLE_ITEM_DETAILS_SETTING:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum REPRINT_TICKET:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum RST_T2_DEFAULT_SETTING_OPEN_TICKETS:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum RST_T2_DEFAULT_SETTING_OPEN_TICKETS_AS_HOME:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum RST_T2_DEFAULT_SETTING_PREDEFINED_TICKETS:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum RST_T2_DEFAULT_SETTING_REQUIRE_EMPLOYEE_PASSCODE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SALES_REPORT_EMAILED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SALES_REPORT_PRINTED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SEND_RECEIPT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SERVICE_EDIT_DISPLAY_PRICE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SETTLED_TIPS_FROM_BATCH_VIEW:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SETTLED_TIPS_FROM_TRANSACTION_VIEW:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SIGN_IN_DECRYPT_FAIL:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SIGN_IN_SHOWED_EMAIL:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SKIP_RECEIPT_SCREEN_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SKIP_SIG_SETTING:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SKIP_SIG_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SPLIT_TENDER_CUSTOM_SPLIT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SPLIT_TENDER_DISMISS_SPLIT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SPLIT_TENDER_EVEN_SPLIT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SPLIT_TICKET_ADD_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SPLIT_TICKET_CANCEL:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SPLIT_TICKET_EDIT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SPLIT_TICKET_MOVE_ITEMS:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SPLIT_TICKET_PRINT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SPLIT_TICKET_PRINT_ALL:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SPLIT_TICKET_SAVE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SPLIT_TICKET_SAVE_ALL:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SPLIT_TICKET_START:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SPLIT_TICKET_VIEW_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SUPPORT_CASH_PAYMENT_TUTORIAL_MANUALLY_STARTED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum SUPPORT_PAYMENT_TUTORIAL_MANUALLY_STARTED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum TAX_DISABLED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum TAX_ENABLED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum TAX_RULE_APPLIED_IN_TRANSACTION:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum TILE_APPEARANCE_CANCELED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum TILE_APPEARANCE_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum TIP_SETTINGS_COLLECT_TIPS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum TOUR_CLOSED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum VOID_CART_CANCELED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum VOID_CART_STARTED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum VOID_CART_VOIDED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum VOID_ITEMIZATION_CANCELED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum VOID_ITEMIZATION_STARTED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum VOID_ITEMIZATION_VOIDED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum WELCOME_SPLASH_CREATE_ACCOUNT:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum WELCOME_SPLASH_SIGN_IN:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum WIFI_CONNECTED:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum X2_PAYMENT_CLIENT_ERROR:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum X2_PAYMENT_NETWORK_ERROR:Lcom/squareup/analytics/RegisterActionName;

.field public static final enum X2_PAYMENT_SERVER_ERROR:Lcom/squareup/analytics/RegisterActionName;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 9
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const/4 v1, 0x0

    const-string v2, "ACCOUNT_OWNER_PASSCODE_AUTHORIZATION_PROMPT"

    const-string v3, "Account Owner Passcode Authorization Prompt"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ACCOUNT_OWNER_PASSCODE_AUTHORIZATION_PROMPT:Lcom/squareup/analytics/RegisterActionName;

    .line 10
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const/4 v2, 0x1

    const-string v3, "ACTIVATION_CONTACTLESS_ORDER_SUCCEED"

    const-string v4, "Activation Flow R12 Order Succeed"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ACTIVATION_CONTACTLESS_ORDER_SUCCEED:Lcom/squareup/analytics/RegisterActionName;

    .line 11
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const/4 v3, 0x2

    const-string v4, "ACTIVITY_SEARCH"

    const-string v5, "Payment History Search Items"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ACTIVITY_SEARCH:Lcom/squareup/analytics/RegisterActionName;

    .line 12
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const/4 v4, 0x3

    const-string v5, "ADVANCED_MODIFIER_HIDE_MODIFIER_FROM_CUSTOMER"

    const-string v6, "Hide from Buyer"

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ADVANCED_MODIFIER_HIDE_MODIFIER_FROM_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    .line 13
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const/4 v5, 0x4

    const-string v6, "ADVANCED_MODIFIER_MIN_MAX_MODIFIER_USED_IN_TRANSACTION"

    const-string v7, "Min/Max Used in Transaction"

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ADVANCED_MODIFIER_MIN_MAX_MODIFIER_USED_IN_TRANSACTION:Lcom/squareup/analytics/RegisterActionName;

    .line 14
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const/4 v6, 0x5

    const-string v7, "ADVANCED_MODIFIER_PRE_SELECTED_MODIFIER_USED_IN_TRANSACTION"

    const-string v8, "Pre-selected Mod Used in Transaction"

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ADVANCED_MODIFIER_PRE_SELECTED_MODIFIER_USED_IN_TRANSACTION:Lcom/squareup/analytics/RegisterActionName;

    .line 16
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const/4 v7, 0x6

    const-string v8, "ADVANCED_MODIFIER_SKIP_ITEM_MODAL"

    const-string v9, "Skip Item Modal"

    invoke-direct {v0, v8, v7, v9}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ADVANCED_MODIFIER_SKIP_ITEM_MODAL:Lcom/squareup/analytics/RegisterActionName;

    .line 17
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const/4 v8, 0x7

    const-string v9, "ADVANCED_MODIFIER_SKIP_ITEM_MODAL_OPEN_ITEM_IN_CART"

    const-string v10, "Item With Skip Modal Opened In Cart"

    invoke-direct {v0, v9, v8, v10}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ADVANCED_MODIFIER_SKIP_ITEM_MODAL_OPEN_ITEM_IN_CART:Lcom/squareup/analytics/RegisterActionName;

    .line 18
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const/16 v9, 0x8

    const-string v10, "ADVANCED_MODIFIER_SKIP_ITEM_MODAL_STILL_SHOWN"

    const-string v11, "Skip Modal Still Shown"

    invoke-direct {v0, v10, v9, v11}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ADVANCED_MODIFIER_SKIP_ITEM_MODAL_STILL_SHOWN:Lcom/squareup/analytics/RegisterActionName;

    .line 19
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const/16 v10, 0x9

    const-string v11, "API_BUYER_STARTED"

    const-string v12, "API Buyer Flow Started"

    invoke-direct {v0, v11, v10, v12}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->API_BUYER_STARTED:Lcom/squareup/analytics/RegisterActionName;

    .line 20
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const/16 v11, 0xa

    const-string v12, "API_NEW_TRANSACTION"

    const-string v13, "API New Transaction Request"

    invoke-direct {v0, v12, v11, v13}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->API_NEW_TRANSACTION:Lcom/squareup/analytics/RegisterActionName;

    .line 21
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const/16 v12, 0xb

    const-string v13, "API_NEW_READER_SETTINGS"

    const-string v14, "API New Reader Settings Flow Request"

    invoke-direct {v0, v13, v12, v14}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->API_NEW_READER_SETTINGS:Lcom/squareup/analytics/RegisterActionName;

    .line 22
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const/16 v13, 0xc

    const-string v14, "API_NEW_STORE_CARD"

    const-string v15, "API New Store Card-on-File Request"

    invoke-direct {v0, v14, v13, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->API_NEW_STORE_CARD:Lcom/squareup/analytics/RegisterActionName;

    .line 23
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const/16 v14, 0xd

    const-string v15, "API_READER_SETTINGS_STARTED"

    const-string v13, "API Reader Settings Flow Started"

    invoke-direct {v0, v15, v14, v13}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->API_READER_SETTINGS_STARTED:Lcom/squareup/analytics/RegisterActionName;

    .line 24
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const/16 v13, 0xe

    const-string v15, "API_READER_SETTINGS_FAILURE"

    const-string v14, "API Reader Settings Flow Failure"

    invoke-direct {v0, v15, v13, v14}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->API_READER_SETTINGS_FAILURE:Lcom/squareup/analytics/RegisterActionName;

    .line 25
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v14, "API_READER_SETTINGS_SUCCESS"

    const/16 v15, 0xf

    const-string v13, "API Reader Settings Flow Success"

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->API_READER_SETTINGS_SUCCESS:Lcom/squareup/analytics/RegisterActionName;

    .line 26
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "API_STORE_CARD_STARTED"

    const/16 v14, 0x10

    const-string v15, "API Store Card-on-File Started"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->API_STORE_CARD_STARTED:Lcom/squareup/analytics/RegisterActionName;

    .line 27
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "API_STORE_CARD_FAILURE"

    const/16 v14, 0x11

    const-string v15, "API Store Card-on-File Failure"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->API_STORE_CARD_FAILURE:Lcom/squareup/analytics/RegisterActionName;

    .line 28
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "API_STORE_CARD_SUCCESS"

    const/16 v14, 0x12

    const-string v15, "API Store Card-on-File Success"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->API_STORE_CARD_SUCCESS:Lcom/squareup/analytics/RegisterActionName;

    .line 29
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "API_TENDER_STARTED"

    const/16 v14, 0x13

    const-string v15, "API Tender Flow Started"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->API_TENDER_STARTED:Lcom/squareup/analytics/RegisterActionName;

    .line 30
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "API_TRANSACTION_FAILURE"

    const/16 v14, 0x14

    const-string v15, "API Transaction Failure"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->API_TRANSACTION_FAILURE:Lcom/squareup/analytics/RegisterActionName;

    .line 31
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "API_TRANSACTION_SUCCESS"

    const/16 v14, 0x15

    const-string v15, "API Transaction Success"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->API_TRANSACTION_SUCCESS:Lcom/squareup/analytics/RegisterActionName;

    .line 32
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "APPLET_SWITCHER_DID_CLOSE"

    const/16 v14, 0x16

    const-string v15, "Applet Switcher Did Close"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->APPLET_SWITCHER_DID_CLOSE:Lcom/squareup/analytics/RegisterActionName;

    .line 33
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "APPLET_SWITCHER_DID_OPEN"

    const/16 v14, 0x17

    const-string v15, "Applet Switcher Did Open"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->APPLET_SWITCHER_DID_OPEN:Lcom/squareup/analytics/RegisterActionName;

    .line 34
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "APPLET_SWITCHER_SELECTED_APPLET"

    const/16 v14, 0x18

    const-string v15, "Applet Switcher Selected Applet"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->APPLET_SWITCHER_SELECTED_APPLET:Lcom/squareup/analytics/RegisterActionName;

    .line 35
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "APPOINTMENT_CREATE_CUSTOMER"

    const/16 v14, 0x19

    const-string v15, "Appointment Customer: Create New Customer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->APPOINTMENT_CREATE_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    .line 36
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "AUDIO_BLOCKING_DIALOG"

    const/16 v14, 0x1a

    const-string v15, "Audio Blocking Dialog"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->AUDIO_BLOCKING_DIALOG:Lcom/squareup/analytics/RegisterActionName;

    .line 37
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "AUTOMATICALLY_PRINT_ITEMIZED_RECEIPT_TOGGLED"

    const/16 v14, 0x1b

    const-string v15, "Automatically Print Itemized Receipt Toggled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->AUTOMATICALLY_PRINT_ITEMIZED_RECEIPT_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

    .line 38
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "AVAILABLE_EMAILS"

    const/16 v14, 0x1c

    const-string v15, "Available Emails"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->AVAILABLE_EMAILS:Lcom/squareup/analytics/RegisterActionName;

    .line 39
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "BACKGROUND_JOB_LOG"

    const/16 v14, 0x1d

    const-string v15, "Background Job Log"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->BACKGROUND_JOB_LOG:Lcom/squareup/analytics/RegisterActionName;

    .line 40
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "BARCODE_SCANNED"

    const/16 v14, 0x1e

    const-string v15, "Barcode Scanned"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->BARCODE_SCANNED:Lcom/squareup/analytics/RegisterActionName;

    .line 41
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "BARCODE_SCANNER_CONNECT"

    const/16 v14, 0x1f

    const-string v15, "Barcode Scanner Connected"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->BARCODE_SCANNER_CONNECT:Lcom/squareup/analytics/RegisterActionName;

    .line 42
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "BARCODE_SCANNER_DISCONNECT"

    const/16 v14, 0x20

    const-string v15, "Barcode Scanner Disconnected"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->BARCODE_SCANNER_DISCONNECT:Lcom/squareup/analytics/RegisterActionName;

    .line 43
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CARD_ON_FILE_ADD_CARD"

    const/16 v14, 0x21

    const-string v15, "Card on File: Add Card to Profile"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CARD_ON_FILE_ADD_CARD:Lcom/squareup/analytics/RegisterActionName;

    .line 44
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CARD_ON_FILE_ADD_CARD_POST_TRANSACTION"

    const/16 v14, 0x22

    const-string v15, "Card on File: Post-Transaction Add Card Button"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CARD_ON_FILE_ADD_CARD_POST_TRANSACTION:Lcom/squareup/analytics/RegisterActionName;

    .line 45
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CARD_ON_FILE_CHARGE_ATTEMPT"

    const/16 v14, 0x23

    const-string v15, "Charge Card on File Attempt"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CARD_ON_FILE_CHARGE_ATTEMPT:Lcom/squareup/analytics/RegisterActionName;

    .line 46
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CARD_ON_FILE_CHARGE_FAILURE"

    const/16 v14, 0x24

    const-string v15, "Charge Card on File Failure"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CARD_ON_FILE_CHARGE_FAILURE:Lcom/squareup/analytics/RegisterActionName;

    .line 47
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CARD_ON_FILE_CHARGE_SUCCESS"

    const/16 v14, 0x25

    const-string v15, "Charge Card on File Success"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CARD_ON_FILE_CHARGE_SUCCESS:Lcom/squareup/analytics/RegisterActionName;

    .line 48
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CARD_ON_FILE_REMOVE_CARD"

    const/16 v14, 0x26

    const-string v15, "Card on File: Unlink Card"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CARD_ON_FILE_REMOVE_CARD:Lcom/squareup/analytics/RegisterActionName;

    .line 49
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CASH_DRAWER_CONNECTED"

    const/16 v14, 0x27

    const-string v15, "Cash Drawer Connected"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CASH_DRAWER_CONNECTED:Lcom/squareup/analytics/RegisterActionName;

    .line 50
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CASH_DRAWER_DISCONNECTED"

    const/16 v14, 0x28

    const-string v15, "Cash Drawer Disconnected"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CASH_DRAWER_DISCONNECTED:Lcom/squareup/analytics/RegisterActionName;

    .line 51
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CASH_DRAWER_FAILED_TO_CONNECT"

    const/16 v14, 0x29

    const-string v15, "Cash Drawer Failed to Connect"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CASH_DRAWER_FAILED_TO_CONNECT:Lcom/squareup/analytics/RegisterActionName;

    .line 52
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CASH_DRAWERS_OPENED"

    const/16 v14, 0x2a

    const-string v15, "Cash Drawers Opened"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CASH_DRAWERS_OPENED:Lcom/squareup/analytics/RegisterActionName;

    .line 53
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CASH_MANAGEMENT_DRAWER_CLOSED_FROM_ENDED"

    const/16 v14, 0x2b

    const-string v15, "Cash Management: Drawer Closed From Ended"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CASH_MANAGEMENT_DRAWER_CLOSED_FROM_ENDED:Lcom/squareup/analytics/RegisterActionName;

    .line 54
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CASH_MANAGEMENT_DRAWER_CLOSED_FROM_STARTED"

    const/16 v14, 0x2c

    const-string v15, "Cash Management: Drawer Closed From Started"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CASH_MANAGEMENT_DRAWER_CLOSED_FROM_STARTED:Lcom/squareup/analytics/RegisterActionName;

    .line 55
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CASH_MANAGEMENT_DRAWER_ENDED"

    const/16 v14, 0x2d

    const-string v15, "Cash Management: Drawer Ended"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CASH_MANAGEMENT_DRAWER_ENDED:Lcom/squareup/analytics/RegisterActionName;

    .line 56
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CASH_MANAGEMENT_DRAWER_OPENED"

    const/16 v14, 0x2e

    const-string v15, "Cash Management: Drawer Started"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CASH_MANAGEMENT_DRAWER_OPENED:Lcom/squareup/analytics/RegisterActionName;

    .line 57
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CASH_MANAGEMENT_END_DRAWER_CLOSE_AUTOMATICALLY"

    const/16 v14, 0x2f

    const-string v15, "Cash Management: Close End Drawer Modal Automatically"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CASH_MANAGEMENT_END_DRAWER_CLOSE_AUTOMATICALLY:Lcom/squareup/analytics/RegisterActionName;

    .line 59
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CASH_MANAGEMENT_END_DRAWER_CLOSE_MANUALLY"

    const/16 v14, 0x30

    const-string v15, "Cash Management: Close End Drawer Modal Manually"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CASH_MANAGEMENT_END_DRAWER_CLOSE_MANUALLY:Lcom/squareup/analytics/RegisterActionName;

    .line 60
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CASH_MANAGEMENT_START_SHIFT_DRAWER_MODAL"

    const/16 v14, 0x31

    const-string v15, "Start Shift Drawer Modal Start Drawer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CASH_MANAGEMENT_START_SHIFT_DRAWER_MODAL:Lcom/squareup/analytics/RegisterActionName;

    .line 61
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CASH_PAYMENT_TUTORIAL_DECLINED"

    const/16 v14, 0x32

    const-string v15, "Cash Payment Tutorial Declined"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CASH_PAYMENT_TUTORIAL_DECLINED:Lcom/squareup/analytics/RegisterActionName;

    .line 62
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CHECKOUT_INFORMATION"

    const/16 v14, 0x33

    const-string v15, "Checkout Information"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CHECKOUT_INFORMATION:Lcom/squareup/analytics/RegisterActionName;

    .line 63
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CHECKOUT_ADD_GIFT_CARD"

    const/16 v14, 0x34

    const-string v15, "Checkout: Added Gift Card To Cart"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CHECKOUT_ADD_GIFT_CARD:Lcom/squareup/analytics/RegisterActionName;

    .line 64
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CHECKOUT_ITEMIZED"

    const/16 v14, 0x35

    const-string v15, "Checkout: Itemized Checkout"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CHECKOUT_ITEMIZED:Lcom/squareup/analytics/RegisterActionName;

    .line 65
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CLIENT_ACTION_TRANSLATION_HANDLED"

    const/16 v14, 0x36

    const-string v15, "Client Action Translation: Handled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CLIENT_ACTION_TRANSLATION_HANDLED:Lcom/squareup/analytics/RegisterActionName;

    .line 66
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CLIENT_ACTION_TRANSLATION_NO_TRANSLATOR"

    const/16 v14, 0x37

    const-string v15, "Client Action Translation: No Translator"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CLIENT_ACTION_TRANSLATION_NO_TRANSLATOR:Lcom/squareup/analytics/RegisterActionName;

    .line 67
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CLIENT_ACTION_TRANSLATION_URL_NOT_HANDLED"

    const/16 v14, 0x38

    const-string v15, "Client Action Translation: URL not handled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CLIENT_ACTION_TRANSLATION_URL_NOT_HANDLED:Lcom/squareup/analytics/RegisterActionName;

    .line 68
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "COMP_CART_STARTED"

    const/16 v14, 0x39

    const-string v15, "Comp Cart: Started"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->COMP_CART_STARTED:Lcom/squareup/analytics/RegisterActionName;

    .line 69
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "COMP_CART_CANCELED"

    const/16 v14, 0x3a

    const-string v15, "Comp Cart: Canceled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->COMP_CART_CANCELED:Lcom/squareup/analytics/RegisterActionName;

    .line 70
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "COMP_CART_COMPED"

    const/16 v14, 0x3b

    const-string v15, "Comp Cart: Comped"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->COMP_CART_COMPED:Lcom/squareup/analytics/RegisterActionName;

    .line 71
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "COMP_CART_UNCOMPED"

    const/16 v14, 0x3c

    const-string v15, "Comp Cart: Uncomped"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->COMP_CART_UNCOMPED:Lcom/squareup/analytics/RegisterActionName;

    .line 72
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "COMP_ITEMIZATION_STARTED"

    const/16 v14, 0x3d

    const-string v15, "Comp Itemization: Started"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->COMP_ITEMIZATION_STARTED:Lcom/squareup/analytics/RegisterActionName;

    .line 73
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "COMP_ITEMIZATION_CANCELED"

    const/16 v14, 0x3e

    const-string v15, "Comp Itemization: Canceled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->COMP_ITEMIZATION_CANCELED:Lcom/squareup/analytics/RegisterActionName;

    .line 74
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "COMP_ITEMIZATION_COMPED"

    const/16 v14, 0x3f

    const-string v15, "Comp Itemization: Comped"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->COMP_ITEMIZATION_COMPED:Lcom/squareup/analytics/RegisterActionName;

    .line 75
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "COMP_ITEMIZATION_UNCOMPED"

    const/16 v14, 0x40

    const-string v15, "Comp Itemization: Uncomped"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->COMP_ITEMIZATION_UNCOMPED:Lcom/squareup/analytics/RegisterActionName;

    .line 76
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CREATE_ITEM_TUTORIAL_DRAG_ITEM"

    const/16 v14, 0x41

    const-string v15, "Items Tutorial: Drag Item"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CREATE_ITEM_TUTORIAL_DRAG_ITEM:Lcom/squareup/analytics/RegisterActionName;

    .line 77
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CREATE_ITEM_TUTORIAL_ITEM_NAME_ENTERED"

    const/16 v14, 0x42

    const-string v15, "Items Tutorial: Item Name Entered"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CREATE_ITEM_TUTORIAL_ITEM_NAME_ENTERED:Lcom/squareup/analytics/RegisterActionName;

    .line 78
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CREATE_ITEM_TUTORIAL_PRICE_ENTERED"

    const/16 v14, 0x43

    const-string v15, "Items Tutorial: Item Price Entered"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CREATE_ITEM_TUTORIAL_PRICE_ENTERED:Lcom/squareup/analytics/RegisterActionName;

    .line 79
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_ADD_CUSTOMER_TO_SALE"

    const/16 v14, 0x44

    const-string v15, "Customer Management: Add Customer to Sale"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_ADD_CUSTOMER_TO_SALE:Lcom/squareup/analytics/RegisterActionName;

    .line 80
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_CREATE_CONTACT"

    const/16 v14, 0x45

    const-string v15, "Customer Engagement: Create Contact"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_CREATE_CONTACT:Lcom/squareup/analytics/RegisterActionName;

    .line 81
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_CUSTOMER_MANAGEMENT_AFTER_CHECKOUT_TOGGLE"

    const/16 v14, 0x46

    const-string v15, "Customer Management PostTransaction Toggle"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_CUSTOMER_MANAGEMENT_AFTER_CHECKOUT_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    .line 82
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_CUSTOMER_MANAGEMENT_BEFORE_CHECKOUT_TOGGLE"

    const/16 v14, 0x47

    const-string v15, "Customer Management InCart Toggle"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_CUSTOMER_MANAGEMENT_BEFORE_CHECKOUT_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    .line 83
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_CUSTOMER_MANAGEMENT_CARD_ON_FILE_TOGGLE"

    const/16 v14, 0x48

    const-string v15, "Card on File: Feature Toggle"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_CUSTOMER_MANAGEMENT_CARD_ON_FILE_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    .line 84
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_CUSTOMER_MANAGEMENT_CARD_ON_FILE_POST_TRANSACTION_TOGGLE"

    const/16 v14, 0x49

    const-string v15, "Card on File: PostTransaction Add Card Toggle"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_CUSTOMER_MANAGEMENT_CARD_ON_FILE_POST_TRANSACTION_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    .line 86
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_EDIT_CONTACT_SAVED"

    const/16 v14, 0x4a

    const-string v15, "Customer Engagement: Edit Contact"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_EDIT_CONTACT_SAVED:Lcom/squareup/analytics/RegisterActionName;

    .line 87
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_EMAIL_COLLECTION_SCREEN_NEW_SALE"

    const/16 v14, 0x4b

    const-string v15, "CRM Email Collection Screen: New Sale"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_EMAIL_COLLECTION_SCREEN_NEW_SALE:Lcom/squareup/analytics/RegisterActionName;

    .line 88
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_EMAIL_COLLECTION_SCREEN_NO_THANKS"

    const/16 v14, 0x4c

    const-string v15, "CRM Email Collection Screen: No Thanks"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_EMAIL_COLLECTION_SCREEN_NO_THANKS:Lcom/squareup/analytics/RegisterActionName;

    .line 89
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_EMAIL_COLLECTION_SCREEN_SUBMIT"

    const/16 v14, 0x4d

    const-string v15, "CRM Email Collection Screen: Submit"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_EMAIL_COLLECTION_SCREEN_SUBMIT:Lcom/squareup/analytics/RegisterActionName;

    .line 90
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_EMAIL_COLLECTION_TOGGLE"

    const/16 v14, 0x4e

    const-string v15, "Customer Engagement: Email Collection Setting Toggle"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_EMAIL_COLLECTION_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    .line 91
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_GET_CONTACT_NULL"

    const/16 v14, 0x4f

    const-string v15, "Customer Engagement: Null Returned for Get Contact"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_GET_CONTACT_NULL:Lcom/squareup/analytics/RegisterActionName;

    .line 92
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_MESSAGING_CREATE_CONVERSATION_SEND"

    const/16 v14, 0x50

    const-string v15, "CRM Messaging: Create Conversation: Send"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_MESSAGING_CREATE_CONVERSATION_SEND:Lcom/squareup/analytics/RegisterActionName;

    .line 93
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_MESSAGING_SHOW_CONVERSATION_SEND"

    const/16 v14, 0x51

    const-string v15, "CRM Messaging: Show Conversation: Send Comment"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_MESSAGING_SHOW_CONVERSATION_SEND:Lcom/squareup/analytics/RegisterActionName;

    .line 94
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_REMOVE_CUSTOMER_FROM_MODAL"

    const/16 v14, 0x52

    const-string v15, "Customer Management: Remove Customer from Modal"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_REMOVE_CUSTOMER_FROM_MODAL:Lcom/squareup/analytics/RegisterActionName;

    .line 95
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_MULTI_SELECT_DELETE_CANCEL"

    const/16 v14, 0x53

    const-string v15, "Directory:Contact List:Bulk Delete:Cancel"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_MULTI_SELECT_DELETE_CANCEL:Lcom/squareup/analytics/RegisterActionName;

    .line 96
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_MULTI_SELECT_DELETE_DELETE"

    const/16 v14, 0x54

    const-string v15, "Directory:Contact List:Bulk Delete:Delete"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_MULTI_SELECT_DELETE_DELETE:Lcom/squareup/analytics/RegisterActionName;

    .line 97
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_MULTI_SELECT_MERGE_CANCEL"

    const/16 v14, 0x55

    const-string v15, "Directory:Contact List:Merge Customers:Cancel"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_MULTI_SELECT_MERGE_CANCEL:Lcom/squareup/analytics/RegisterActionName;

    .line 98
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_MULTI_SELECT_MERGE_MERGE"

    const/16 v14, 0x56

    const-string v15, "Directory:Contact List:Merge Customers:Merge"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_MULTI_SELECT_MERGE_MERGE:Lcom/squareup/analytics/RegisterActionName;

    .line 99
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_FILTER_ADD_FILTER"

    const/16 v14, 0x57

    const-string v15, "Directory:Contacts List:Select Filter:Add Filter"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_FILTER_ADD_FILTER:Lcom/squareup/analytics/RegisterActionName;

    .line 100
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_FILTER_APPLY"

    const/16 v14, 0x58

    const-string v15, "Directory:Contacts List:Select Filter:Apply"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_FILTER_APPLY:Lcom/squareup/analytics/RegisterActionName;

    .line 101
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_FILTER_CLOSE"

    const/16 v14, 0x59

    const-string v15, "Directory:Contacts List:Select Filter:Close"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_FILTER_CLOSE:Lcom/squareup/analytics/RegisterActionName;

    .line 102
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_FILTER_REMOVE_ALL"

    const/16 v14, 0x5a

    const-string v15, "Directory:Contacts List:Select Filter:Remove All"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_FILTER_REMOVE_ALL:Lcom/squareup/analytics/RegisterActionName;

    .line 103
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_FILTER_REMOVE_FILTER_PILL"

    const/16 v14, 0x5b

    const-string v15, "Directory:Contacts List:Remove Filter"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_FILTER_REMOVE_FILTER_PILL:Lcom/squareup/analytics/RegisterActionName;

    .line 104
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_FILTER_SAVE_AS_GROUP"

    const/16 v14, 0x5c

    const-string v15, "Directory:Contacts List:Select Filter:Save Filters as Smart Group"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_FILTER_SAVE_AS_GROUP:Lcom/squareup/analytics/RegisterActionName;

    .line 105
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_GROUPS_CLOSE"

    const/16 v14, 0x5d

    const-string v15, "Directory:Groups:Close"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_GROUPS_CLOSE:Lcom/squareup/analytics/RegisterActionName;

    .line 106
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_GROUPS_CREATE_NEW"

    const/16 v14, 0x5e

    const-string v15, "Directory:Groups:Create Group"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_GROUPS_CREATE_NEW:Lcom/squareup/analytics/RegisterActionName;

    .line 107
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_GROUPS_DELETE"

    const/16 v14, 0x5f

    const-string v15, "Directory:Groups:Delete"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_GROUPS_DELETE:Lcom/squareup/analytics/RegisterActionName;

    .line 108
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_GROUPS_SAVE"

    const/16 v14, 0x60

    const-string v15, "Directory:Groups:Save"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_GROUPS_SAVE:Lcom/squareup/analytics/RegisterActionName;

    .line 109
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_GROUPS_SELECT_AUTO_SMART_GROUP"

    const/16 v14, 0x61

    const-string v15, "Directory:Groups:Select Auto Group"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_GROUPS_SELECT_AUTO_SMART_GROUP:Lcom/squareup/analytics/RegisterActionName;

    .line 110
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_GROUPS_SELECT_SMART_GROUP"

    const/16 v14, 0x62

    const-string v15, "Directory:Groups:Select Smart Group"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_GROUPS_SELECT_SMART_GROUP:Lcom/squareup/analytics/RegisterActionName;

    .line 111
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_GROUPS_SELECT_MANUAL_GROUP"

    const/16 v14, 0x63

    const-string v15, "Directory:Groups:Select Manual Group"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_GROUPS_SELECT_MANUAL_GROUP:Lcom/squareup/analytics/RegisterActionName;

    .line 112
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_ADD_CARD"

    const/16 v14, 0x64

    const-string v15, "Directory:Customer Profile:Add Card"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_ADD_CARD:Lcom/squareup/analytics/RegisterActionName;

    .line 113
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_ADD_FILES"

    const/16 v14, 0x65

    const-string v15, "Directory:Customer Profile:Add Files"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_ADD_FILES:Lcom/squareup/analytics/RegisterActionName;

    .line 114
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_ADD_NOTE"

    const/16 v14, 0x66

    const-string v15, "Directory:Customer Profile:Add Note"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_ADD_NOTE:Lcom/squareup/analytics/RegisterActionName;

    .line 115
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_EDIT_NOTE"

    const/16 v14, 0x67

    const-string v15, "Directory:Customer Profile:Edit Note"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_EDIT_NOTE:Lcom/squareup/analytics/RegisterActionName;

    .line 116
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_EDIT_FILE"

    const/16 v14, 0x68

    const-string v15, "Directory:Customer Profile:Edit File"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_EDIT_FILE:Lcom/squareup/analytics/RegisterActionName;

    .line 117
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_EDIT_PERSONAL"

    const/16 v14, 0x69

    const-string v15, "Directory:Customer Profile:Edit Personal Information"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_EDIT_PERSONAL:Lcom/squareup/analytics/RegisterActionName;

    .line 118
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_MENU_ADD_CARD"

    const/16 v14, 0x6a

    const-string v15, "Directory:Customer Profile:Menu:Add Card"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_ADD_CARD:Lcom/squareup/analytics/RegisterActionName;

    .line 119
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_MENU_ADD_NOTE"

    const/16 v14, 0x6b

    const-string v15, "Directory:Customer Profile:Menu:Add Note"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_ADD_NOTE:Lcom/squareup/analytics/RegisterActionName;

    .line 120
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_MENU_ADD_TO_SALE"

    const/16 v14, 0x6c

    const-string v15, "Directory:Customer Profile:Menu:Add to Sale"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_ADD_TO_SALE:Lcom/squareup/analytics/RegisterActionName;

    .line 121
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_MENU_DELETE_CUSTOMER"

    const/16 v14, 0x6d

    const-string v15, "Directory:Customer Profile:Menu:Delete Customer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_DELETE_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    .line 122
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_MENU_EDIT"

    const/16 v14, 0x6e

    const-string v15, "Directory:Customer Profile:Menu:Edit Personal Information"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_EDIT:Lcom/squareup/analytics/RegisterActionName;

    .line 123
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_MENU_MERGE_PROFILE"

    const/16 v14, 0x6f

    const-string v15, "Directory:Customer Profile:Menu:Merge Customer Profile"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_MERGE_PROFILE:Lcom/squareup/analytics/RegisterActionName;

    .line 124
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_MENU_NEW_SALE"

    const/16 v14, 0x70

    const-string v15, "Directory:Customer Profile:Menu:New Sale With Customer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_NEW_SALE:Lcom/squareup/analytics/RegisterActionName;

    .line 125
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_MENU_ADD_APPOINTMENT"

    const/16 v14, 0x71

    const-string v15, "Directory:Customer Profile:Menu:Add New Appointment For Customer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_ADD_APPOINTMENT:Lcom/squareup/analytics/RegisterActionName;

    .line 127
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_MENU_SEND_MESSAGE"

    const/16 v14, 0x72

    const-string v15, "Directory:Customer Profile:Menu:Send Message"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_SEND_MESSAGE:Lcom/squareup/analytics/RegisterActionName;

    .line 128
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_MENU_REMOVE_FROM_ESTIMATE"

    const/16 v14, 0x73

    const-string v15, "Directory:Customer Profile:Menu:Remove From Estimate"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_REMOVE_FROM_ESTIMATE:Lcom/squareup/analytics/RegisterActionName;

    .line 130
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_MENU_REMOVE_FROM_INVOICE"

    const/16 v14, 0x74

    const-string v15, "Directory:Customer Profile:Menu:Remove From Invoice"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_REMOVE_FROM_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    .line 132
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_MENU_REMOVE_FROM_SALE"

    const/16 v14, 0x75

    const-string v15, "Directory:Customer Profile:Menu:Remove From Sale"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_REMOVE_FROM_SALE:Lcom/squareup/analytics/RegisterActionName;

    .line 133
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_MENU_UPLOAD_FILE"

    const/16 v14, 0x76

    const-string v15, "Directory:Customer Profile:Menu:Upload File"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_UPLOAD_FILE:Lcom/squareup/analytics/RegisterActionName;

    .line 134
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_REMOVE_CARD"

    const/16 v14, 0x77

    const-string v15, "Directory:Customer Profile:Delete Card"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_REMOVE_CARD:Lcom/squareup/analytics/RegisterActionName;

    .line 135
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_TRANSFER_LOYALTY"

    const/16 v14, 0x78

    const-string v15, "Directory:Customer Profile:Transfer Loyalty"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_TRANSFER_LOYALTY:Lcom/squareup/analytics/RegisterActionName;

    .line 136
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_VIEW_ALL_ACTIVITY"

    const/16 v14, 0x79

    const-string v15, "Directory:Customer Profile:View All Activity"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_VIEW_ALL_ACTIVITY:Lcom/squareup/analytics/RegisterActionName;

    .line 137
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_VIEW_ALL_COUPONS"

    const/16 v14, 0x7a

    const-string v15, "Directory:Customer Profile:View All Coupons"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_VIEW_ALL_COUPONS:Lcom/squareup/analytics/RegisterActionName;

    .line 138
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_VIEW_ALL_FILES"

    const/16 v14, 0x7b

    const-string v15, "Directory:Customer Profile:View All Files"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_VIEW_ALL_FILES:Lcom/squareup/analytics/RegisterActionName;

    .line 139
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CRM_V2_VIEW_PROFILE_VIEW_ALL_FREQUENT_ITEMS"

    const/16 v14, 0x7c

    const-string v15, "Directory:Customer Profile:View All Frequent Items"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_VIEW_ALL_FREQUENT_ITEMS:Lcom/squareup/analytics/RegisterActionName;

    .line 140
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "CUSTOMER_CHECKOUT_ENTER"

    const/16 v14, 0x7d

    const-string v15, "Customer Checkout: Enter Buyer Checkout Flow"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->CUSTOMER_CHECKOUT_ENTER:Lcom/squareup/analytics/RegisterActionName;

    .line 141
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "COUPONS_SEARCH"

    const/16 v14, 0x7e

    const-string v15, "Coupons Search"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->COUPONS_SEARCH:Lcom/squareup/analytics/RegisterActionName;

    .line 142
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "DEEP_LINK_BRANCH"

    const/16 v14, 0x7f

    const-string v15, "deep link waiting for branch"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->DEEP_LINK_BRANCH:Lcom/squareup/analytics/RegisterActionName;

    .line 143
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "DEEP_LINK_HANDLED"

    const/16 v14, 0x80

    const-string v15, "deep link handled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->DEEP_LINK_HANDLED:Lcom/squareup/analytics/RegisterActionName;

    .line 144
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "DEEP_LINK_REJECTED"

    const/16 v14, 0x81

    const-string v15, "deep link rejected"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->DEEP_LINK_REJECTED:Lcom/squareup/analytics/RegisterActionName;

    .line 145
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "EDIT_FAVORITES_PAGE_NAME"

    const/16 v14, 0x82

    const-string v15, "Edit Favorites Page Name"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->EDIT_FAVORITES_PAGE_NAME:Lcom/squareup/analytics/RegisterActionName;

    .line 146
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "EMPLOYEE_ACCESS_DENIED"

    const/16 v14, 0x83

    const-string v15, "Employee Access Denied"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->EMPLOYEE_ACCESS_DENIED:Lcom/squareup/analytics/RegisterActionName;

    .line 147
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "FEE_TUTORIAL_DIP_DISMISS"

    const/16 v14, 0x84

    const-string v15, "Fee Tutorial - Dip or Tap - Dismissed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->FEE_TUTORIAL_DIP_DISMISS:Lcom/squareup/analytics/RegisterActionName;

    .line 148
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "FEE_TUTORIAL_DIP_JCB_DISMISS"

    const/16 v14, 0x85

    const-string v15, "Fee Tutorial - Dip JCB - Dismissed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->FEE_TUTORIAL_DIP_JCB_DISMISS:Lcom/squareup/analytics/RegisterActionName;

    .line 149
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "FEE_TUTORIAL_INTERAC_DISMISS"

    const/16 v14, 0x86

    const-string v15, "Fee Tutorial - Interac - Dismissed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->FEE_TUTORIAL_INTERAC_DISMISS:Lcom/squareup/analytics/RegisterActionName;

    .line 150
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "FEE_TUTORIAL_MANUAL_DISMISS"

    const/16 v14, 0x87

    const-string v15, "Fee Tutorial - Keyed-in - Dismissed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->FEE_TUTORIAL_MANUAL_DISMISS:Lcom/squareup/analytics/RegisterActionName;

    .line 151
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "FEE_TUTORIAL_SWIPE_DISMISS"

    const/16 v14, 0x88

    const-string v15, "Fee Tutorial - Swipe - Dismissed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->FEE_TUTORIAL_SWIPE_DISMISS:Lcom/squareup/analytics/RegisterActionName;

    .line 152
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "FEE_TUTORIAL_TAP_DISMISS"

    const/16 v14, 0x89

    const-string v15, "Fee Tutorial - Tap - Dismissed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->FEE_TUTORIAL_TAP_DISMISS:Lcom/squareup/analytics/RegisterActionName;

    .line 153
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "GAP_TIME_MODIFIED_CART"

    const/16 v14, 0x8a

    const-string v15, "Modify Gap Time Duration Cart"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->GAP_TIME_MODIFIED_CART:Lcom/squareup/analytics/RegisterActionName;

    .line 154
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "GAP_TIME_MODIFIED_SERVICE"

    const/16 v14, 0x8b

    const-string v15, "Modify Gap Time Duration Service"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->GAP_TIME_MODIFIED_SERVICE:Lcom/squareup/analytics/RegisterActionName;

    .line 155
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "INCREMENTAL_ITEMS_SYNC"

    const/16 v14, 0x8c

    const-string v15, "Incremental Items Sync"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->INCREMENTAL_ITEMS_SYNC:Lcom/squareup/analytics/RegisterActionName;

    .line 156
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "INSTANT_DEPOSIT_TOGGLE"

    const/16 v14, 0x8d

    const-string v15, "Instant Deposit: Toggle"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->INSTANT_DEPOSIT_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    .line 157
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "INVOICE_CHOOSE_CUSTOMER"

    const/16 v14, 0x8e

    const-string v15, "Invoices: Add Recipient"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->INVOICE_CHOOSE_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    .line 158
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "INVOICE_CREATE_CUSTOMER"

    const/16 v14, 0x8f

    const-string v15, "Invoices: Create Recipient"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->INVOICE_CREATE_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    .line 159
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "INVOICES_APPLET_CANCEL_INVOICE"

    const/16 v14, 0x90

    const-string v15, "Invoices: Cancel Invoice"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_CANCEL_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    .line 160
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "INVOICES_APPLET_COPY_LINK"

    const/16 v14, 0x91

    const-string v15, "Invoices: Copy Link"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_COPY_LINK:Lcom/squareup/analytics/RegisterActionName;

    .line 161
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "INVOICES_APPLET_CREATE_INVOICE"

    const/16 v14, 0x92

    const-string v15, "Invoices: New Invoice"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_CREATE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    .line 162
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "INVOICES_APPLET_CREATE_SHARE_INVOICE"

    const/16 v14, 0x93

    const-string v15, "Invoices: Create Share Link Invoice"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_CREATE_SHARE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    .line 163
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "INVOICES_APPLET_DELETE_INVOICE"

    const/16 v14, 0x94

    const-string v15, "Invoices: Delete Invoice"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_DELETE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    .line 164
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "INVOICES_APPLET_DOWNLOAD_INVOICE"

    const/16 v14, 0x95

    const-string v15, "Invoices: Download Invoice"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_DOWNLOAD_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    .line 165
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "INVOICES_APPLET_DUPLICATE_INVOICE"

    const/16 v14, 0x96

    const-string v15, "Invoices: Duplicate Invoice"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_DUPLICATE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    .line 166
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "INVOICES_APPLET_FILTER"

    const/16 v14, 0x97

    const-string v15, "Invoices: Filter Changed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_FILTER:Lcom/squareup/analytics/RegisterActionName;

    .line 167
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "INVOICES_APPLET_PREVIEW"

    const/16 v14, 0x98

    const-string v15, "Invoices: Preview Invoice"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_PREVIEW:Lcom/squareup/analytics/RegisterActionName;

    .line 168
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "INVOICES_APPLET_SAVE"

    const/16 v14, 0x99

    const-string v15, "Invoices: Save Invoice"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SAVE:Lcom/squareup/analytics/RegisterActionName;

    .line 169
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "INVOICES_APPLET_SEARCH"

    const/16 v14, 0x9a

    const-string v15, "Invoices: Search"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SEARCH:Lcom/squareup/analytics/RegisterActionName;

    .line 170
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "INVOICES_APPLET_SCHEDULE_INVOICE"

    const/16 v14, 0x9b

    const-string v15, "Invoices: Schedule Invoice"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SCHEDULE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    .line 171
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "INVOICES_APPLET_SHARE_LINK"

    const/16 v14, 0x9c

    const-string v15, "Invoices: Share Invoice"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SHARE_LINK:Lcom/squareup/analytics/RegisterActionName;

    .line 172
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "INVOICES_APPLET_SEND_INVOICE"

    const/16 v14, 0x9d

    const-string v15, "Invoices: Send Invoice"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SEND_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    .line 173
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "INVOICES_APPLET_SEND_RECURRING_SERIES"

    const/16 v14, 0x9e

    const-string v15, "Invoices: Send Recurring Series"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SEND_RECURRING_SERIES:Lcom/squareup/analytics/RegisterActionName;

    .line 174
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "INVOICES_APPLET_VIEW_TIMELINE"

    const/16 v14, 0x9f

    const-string v15, "Invoices: Timeline View Initiated"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_VIEW_TIMELINE:Lcom/squareup/analytics/RegisterActionName;

    .line 175
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ITEMS_APPLET_CATEGORY_CREATED"

    const/16 v14, 0xa0

    const-string v15, "Items Applet: Category Created"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_CATEGORY_CREATED:Lcom/squareup/analytics/RegisterActionName;

    .line 176
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ITEMS_APPLET_CATEGORY_DELETED"

    const/16 v14, 0xa1

    const-string v15, "Items Applet: Category Deleted"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_CATEGORY_DELETED:Lcom/squareup/analytics/RegisterActionName;

    .line 177
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ITEMS_APPLET_CATEGORY_EDITED"

    const/16 v14, 0xa2

    const-string v15, "Items Applet: Category Edited"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_CATEGORY_EDITED:Lcom/squareup/analytics/RegisterActionName;

    .line 178
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ITEMS_APPLET_DISCOUNT_CREATED"

    const/16 v14, 0xa3

    const-string v15, "Items Applet: Discount Created"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_DISCOUNT_CREATED:Lcom/squareup/analytics/RegisterActionName;

    .line 179
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ITEMS_APPLET_DISCOUNT_DELETED"

    const/16 v14, 0xa4

    const-string v15, "Items Applet: Discount Deleted"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_DISCOUNT_DELETED:Lcom/squareup/analytics/RegisterActionName;

    .line 180
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ITEMS_APPLET_DISCOUNT_EDITED"

    const/16 v14, 0xa5

    const-string v15, "Items Applet: Discount Edited"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_DISCOUNT_EDITED:Lcom/squareup/analytics/RegisterActionName;

    .line 181
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ITEMS_APPLET_EDIT_CATALOG_OBJECT"

    const/16 v14, 0xa6

    const-string v15, "Items Applet: Edit Catalog Object"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_EDIT_CATALOG_OBJECT:Lcom/squareup/analytics/RegisterActionName;

    .line 182
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ITEMS_APPLET_GIFT_CARD_CREATED"

    const/16 v14, 0xa7

    const-string v15, "Items Applet: Gift Card Created"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_GIFT_CARD_CREATED:Lcom/squareup/analytics/RegisterActionName;

    .line 183
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ITEMS_APPLET_GIFT_CARD_DELETED"

    const/16 v14, 0xa8

    const-string v15, "Items Applet: Gift Card Deleted"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_GIFT_CARD_DELETED:Lcom/squareup/analytics/RegisterActionName;

    .line 184
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ITEMS_APPLET_GIFT_CARD_EDITED"

    const/16 v14, 0xa9

    const-string v15, "Items Applet: Gift Card Edited"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_GIFT_CARD_EDITED:Lcom/squareup/analytics/RegisterActionName;

    .line 185
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ITEMS_APPLET_ITEM_CREATED"

    const/16 v14, 0xaa

    const-string v15, "Items Applet: Item Created"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_ITEM_CREATED:Lcom/squareup/analytics/RegisterActionName;

    .line 186
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ITEMS_APPLET_ITEM_DELETED"

    const/16 v14, 0xab

    const-string v15, "Items Applet: Item Deleted"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_ITEM_DELETED:Lcom/squareup/analytics/RegisterActionName;

    .line 187
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ITEMS_APPLET_ITEM_EDITED"

    const/16 v14, 0xac

    const-string v15, "Items Applet: Item Edited"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_ITEM_EDITED:Lcom/squareup/analytics/RegisterActionName;

    .line 188
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ITEMS_APPLET_MODIFIER_SET_CREATED"

    const/16 v14, 0xad

    const-string v15, "Items Applet: Modifier Set Created"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_MODIFIER_SET_CREATED:Lcom/squareup/analytics/RegisterActionName;

    .line 189
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ITEMS_APPLET_MODIFIER_SET_DELETED"

    const/16 v14, 0xae

    const-string v15, "Items Applet: Modifier Set Deleted"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_MODIFIER_SET_DELETED:Lcom/squareup/analytics/RegisterActionName;

    .line 190
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ITEMS_APPLET_MODIFIER_SET_EDITED"

    const/16 v14, 0xaf

    const-string v15, "Items Applet: Modifier Set Edited"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_MODIFIER_SET_EDITED:Lcom/squareup/analytics/RegisterActionName;

    .line 191
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "LOG_OUT"

    const/16 v14, 0xb0

    const-string v15, "Log out"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->LOG_OUT:Lcom/squareup/analytics/RegisterActionName;

    .line 192
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "LOYALTY_CART_POINTS_MODAL_OPENED"

    const/16 v14, 0xb1

    const-string v15, "Cart Points Modal - Open"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_CART_POINTS_MODAL_OPENED:Lcom/squareup/analytics/RegisterActionName;

    .line 193
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "LOYALTY_CART_POINTS_MODAL_ADD_CUSTOMER_FLOW_STARTED"

    const/16 v14, 0xb2

    const-string v15, "Cart Points Modal - Begin Add Customer Flow"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_CART_POINTS_MODAL_ADD_CUSTOMER_FLOW_STARTED:Lcom/squareup/analytics/RegisterActionName;

    .line 195
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "LOYALTY_CART_POINTS_MODAL_APPLY_REWARD"

    const/16 v14, 0xb3

    const-string v15, "Cart Points Modal - Apply Reward"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_CART_POINTS_MODAL_APPLY_REWARD:Lcom/squareup/analytics/RegisterActionName;

    .line 196
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "LOYALTY_CART_POINTS_MODAL_CUSTOMER_ADDED"

    const/16 v14, 0xb4

    const-string v15, "Cart Points Modal - Customer Added to Sale"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_CART_POINTS_MODAL_CUSTOMER_ADDED:Lcom/squareup/analytics/RegisterActionName;

    .line 197
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "LOYALTY_CART_POINTS_MODAL_REMOVE_CUSTOMER"

    const/16 v14, 0xb5

    const-string v15, "Cart Points Modal - Removed Customer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_CART_POINTS_MODAL_REMOVE_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    .line 198
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "LOYALTY_ENROLL_SUBMIT_BUYER"

    const/16 v14, 0xb6

    const-string v15, "Loyalty 2: Enroll Submit: Buyer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_ENROLL_SUBMIT_BUYER:Lcom/squareup/analytics/RegisterActionName;

    .line 199
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "LOYALTY_ENROLL_SUBMIT_MERCHANT"

    const/16 v14, 0xb7

    const-string v15, "Loyalty 2: Enroll Submit: Merchant"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_ENROLL_SUBMIT_MERCHANT:Lcom/squareup/analytics/RegisterActionName;

    .line 200
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "LOYALTY_ENROLL_SUBMIT_SUCCESS_BUYER"

    const/16 v14, 0xb8

    const-string v15, "Loyalty 2: Enroll Submit: Success: Buyer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_ENROLL_SUBMIT_SUCCESS_BUYER:Lcom/squareup/analytics/RegisterActionName;

    .line 201
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "LOYALTY_ENROLL_SUBMIT_SUCCESS_MERCHANT"

    const/16 v14, 0xb9

    const-string v15, "Loyalty 2: Enroll Submit: Success: Merchant"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_ENROLL_SUBMIT_SUCCESS_MERCHANT:Lcom/squareup/analytics/RegisterActionName;

    .line 202
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "LOYALTY_ENROLL_SUBMIT_TIMEOUT_BUYER"

    const/16 v14, 0xba

    const-string v15, "Loyalty 2: Enroll Submit: Timeout: Buyer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_ENROLL_SUBMIT_TIMEOUT_BUYER:Lcom/squareup/analytics/RegisterActionName;

    .line 203
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "LOYALTY_ENROLL_SUBMIT_TIMEOUT_MERCHANT"

    const/16 v14, 0xbb

    const-string v15, "Loyalty 2: Enroll Submit: Timeout: Merchant"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_ENROLL_SUBMIT_TIMEOUT_MERCHANT:Lcom/squareup/analytics/RegisterActionName;

    .line 204
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "LOYALTY_NO_THANKS_BUYER"

    const/16 v14, 0xbc

    const-string v15, "Loyalty 2: No Thanks: Buyer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_NO_THANKS_BUYER:Lcom/squareup/analytics/RegisterActionName;

    .line 205
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "LOYALTY_NO_THANKS_MERCHANT"

    const/16 v14, 0xbd

    const-string v15, "Loyalty 2: No Thanks: Merchant"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_NO_THANKS_MERCHANT:Lcom/squareup/analytics/RegisterActionName;

    .line 206
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "LOYALTY_REPORT_CHANGE_DATE_RANGE"

    const/16 v14, 0xbe

    const-string v15, "Loyalty Report: Change Date Range"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_REPORT_CHANGE_DATE_RANGE:Lcom/squareup/analytics/RegisterActionName;

    .line 207
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "NOTIFICATION_CENTER_ACCOUNT_VIEW"

    const/16 v14, 0xbf

    const-string v15, "Notifications: Account: View"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->NOTIFICATION_CENTER_ACCOUNT_VIEW:Lcom/squareup/analytics/RegisterActionName;

    .line 208
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "NOTIFICATION_CENTER_BROWSER_DIALOG_CANCEL"

    const/16 v14, 0xc0

    const-string v15, "Notifications: Browser Dialog: Cancel"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->NOTIFICATION_CENTER_BROWSER_DIALOG_CANCEL:Lcom/squareup/analytics/RegisterActionName;

    .line 209
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "NOTIFICATION_CENTER_BROWSER_DIALOG_OPEN"

    const/16 v14, 0xc1

    const-string v15, "Notifications: Browser Dialog: Open"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->NOTIFICATION_CENTER_BROWSER_DIALOG_OPEN:Lcom/squareup/analytics/RegisterActionName;

    .line 210
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "NOTIFICATION_CENTER_NOTIFICATION_CLICK"

    const/16 v14, 0xc2

    const-string v15, "Notifications: Notification: Click"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->NOTIFICATION_CENTER_NOTIFICATION_CLICK:Lcom/squareup/analytics/RegisterActionName;

    .line 211
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "NOTIFICATION_CENTER_NOTIFICATION_OPEN"

    const/16 v14, 0xc3

    const-string v15, "Notifications: Notification: Open"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->NOTIFICATION_CENTER_NOTIFICATION_OPEN:Lcom/squareup/analytics/RegisterActionName;

    .line 212
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "NOTIFICATION_CENTER_LOADED"

    const/16 v14, 0xc4

    const-string v15, "Notifications: Load"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->NOTIFICATION_CENTER_LOADED:Lcom/squareup/analytics/RegisterActionName;

    .line 213
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "NOTIFICATION_CENTER_PRODUCT_VIEW"

    const/16 v14, 0xc5

    const-string v15, "Notifications: Product: View"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->NOTIFICATION_CENTER_PRODUCT_VIEW:Lcom/squareup/analytics/RegisterActionName;

    .line 214
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ONBOARDING_CONTINUE_TO_WEB"

    const/16 v14, 0xc6

    const-string v15, "Onboard: Welcome Flow Web Activation - ContinueToWebActivation"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ONBOARDING_CONTINUE_TO_WEB:Lcom/squareup/analytics/RegisterActionName;

    .line 215
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ONBOARDING_LATER_IN_INITIAL_SHIPPING"

    const/16 v14, 0xc7

    const-string v15, "Onboarding: Later in Initial Shipping"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ONBOARDING_LATER_IN_INITIAL_SHIPPING:Lcom/squareup/analytics/RegisterActionName;

    .line 216
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ONBOARDING_LATER_IN_MCC"

    const/16 v14, 0xc8

    const-string v15, "Onboarding: Later in MCC"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ONBOARDING_LATER_IN_MCC:Lcom/squareup/analytics/RegisterActionName;

    .line 217
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ONBOARDING_LATER_IN_WEB"

    const/16 v14, 0xc9

    const-string v15, "Onboard: Welcome Flow Web Activation - Segue"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ONBOARDING_LATER_IN_WEB:Lcom/squareup/analytics/RegisterActionName;

    .line 218
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ONBOARDING_NOT_USE_PREFILL_EMAIL"

    const/16 v14, 0xca

    const-string v15, "Onboarding: Did not use prefill email"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ONBOARDING_NOT_USE_PREFILL_EMAIL:Lcom/squareup/analytics/RegisterActionName;

    .line 219
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ONBOARDING_REDIRECT_DOWNLOAD"

    const/16 v14, 0xcb

    const-string v15, "Onboarding: Download through mobile web redirect"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ONBOARDING_REDIRECT_DOWNLOAD:Lcom/squareup/analytics/RegisterActionName;

    .line 220
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ONBOARDING_WEB_ACTIVATION_COMPLETE"

    const/16 v14, 0xcc

    const-string v15, "Onboard: Web Activation Complete"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ONBOARDING_WEB_ACTIVATION_COMPLETE:Lcom/squareup/analytics/RegisterActionName;

    .line 221
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "OPEN_TICKETS_TOGGLE"

    const/16 v14, 0xcd

    const-string v15, "Use Open Tickets"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    .line 222
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "OPEN_TICKETS_AS_HOME_SCREEN_TOGGLE"

    const/16 v14, 0xce

    const-string v15, "Use Open Tickets Menu as Home Screen"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_AS_HOME_SCREEN_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    .line 223
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "OPEN_TICKETS_EXIT_EDIT_TICKET"

    const/16 v14, 0xcf

    const-string v15, "Open Tickets: Exit Edit Ticket"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_EXIT_EDIT_TICKET:Lcom/squareup/analytics/RegisterActionName;

    .line 224
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "OPEN_TICKETS_ITEMIZATION_VOID"

    const/16 v14, 0xd0

    const-string v15, "Open Tickets: Itemization Voided"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_ITEMIZATION_VOID:Lcom/squareup/analytics/RegisterActionName;

    .line 225
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "OPEN_TICKETS_LOADED_EXISTING_TICKET"

    const/16 v14, 0xd1

    const-string v15, "Open Tickets: Loaded Existing Ticket"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_LOADED_EXISTING_TICKET:Lcom/squareup/analytics/RegisterActionName;

    .line 226
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "OPEN_TICKETS_MERGE_TICKETS_AFTER"

    const/16 v14, 0xd2

    const-string v15, "Merge Tickets: After Merge"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_MERGE_TICKETS_AFTER:Lcom/squareup/analytics/RegisterActionName;

    .line 227
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "OPEN_TICKETS_MERGE_TICKETS_BEFORE"

    const/16 v14, 0xd3

    const-string v15, "Merge Tickets: Before Merge"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_MERGE_TICKETS_BEFORE:Lcom/squareup/analytics/RegisterActionName;

    .line 228
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "OPEN_TICKETS_MERGE_TICKETS_CANCELED"

    const/16 v14, 0xd4

    const-string v15, "Merge Tickets: Canceled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_MERGE_TICKETS_CANCELED:Lcom/squareup/analytics/RegisterActionName;

    .line 229
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "OPEN_TICKETS_MERGED_CART_TO_EXISTING_TICKET"

    const/16 v14, 0xd5

    const-string v15, "Open Tickets: Merged Cart To Existing Ticket"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_MERGED_CART_TO_EXISTING_TICKET:Lcom/squareup/analytics/RegisterActionName;

    .line 230
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "OPEN_TICKETS_MOVE_TICKETS"

    const/16 v14, 0xd6

    const-string v15, "Move Tickets"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_MOVE_TICKETS:Lcom/squareup/analytics/RegisterActionName;

    .line 231
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "OPEN_TICKETS_NEW_TICKET_WITH_EXISTING_CART"

    const/16 v14, 0xd7

    const-string v15, "Open Tickets: New Ticket With Existing Cart"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_NEW_TICKET_WITH_EXISTING_CART:Lcom/squareup/analytics/RegisterActionName;

    .line 232
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "OPEN_TICKETS_NEW_TICKET_WITHOUT_EXISTING_CART"

    const/16 v14, 0xd8

    const-string v15, "Open Tickets: New Ticket Without Existing Cart"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_NEW_TICKET_WITHOUT_EXISTING_CART:Lcom/squareup/analytics/RegisterActionName;

    .line 233
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "OPEN_TICKET_SAVED"

    const/16 v14, 0xd9

    const-string v15, "Open Ticket Saved"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKET_SAVED:Lcom/squareup/analytics/RegisterActionName;

    .line 234
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "OPEN_TICKETS_SAVED_DISCOUNT_ONLY_CART"

    const/16 v14, 0xda

    const-string v15, "Open Tickets: Saved Discount Only Cart"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_SAVED_DISCOUNT_ONLY_CART:Lcom/squareup/analytics/RegisterActionName;

    .line 235
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "OPEN_TICKETS_UNSYNCED_TICKETS_UPLOADED_VIA_SWEEPER"

    const/16 v14, 0xdb

    const-string v15, "Open Tickets: Unsynced Tickets Uploaded Via Sweeper"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_UNSYNCED_TICKETS_UPLOADED_VIA_SWEEPER:Lcom/squareup/analytics/RegisterActionName;

    .line 237
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "OPEN_TICKETS_VIEWED_LOCKED_ITEMIZATION"

    const/16 v14, 0xdc

    const-string v15, "Open Tickets: Viewed Locked Itemization"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_VIEWED_LOCKED_ITEMIZATION:Lcom/squareup/analytics/RegisterActionName;

    .line 238
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "ORDER_ENTRY_CLEAR_SALE"

    const/16 v14, 0xdd

    const-string v15, "Order Entry: Clear Sale"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->ORDER_ENTRY_CLEAR_SALE:Lcom/squareup/analytics/RegisterActionName;

    .line 239
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "OWNER_PASSCODE_AUTHORIZATION_PROMPT"

    const/16 v14, 0xde

    const-string v15, "Owner Passcode Authorization Prompt"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->OWNER_PASSCODE_AUTHORIZATION_PROMPT:Lcom/squareup/analytics/RegisterActionName;

    .line 240
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PAPER_SIGNATURE_PRINT_ADDITIONAL_AUTH_SLIP_TOGGLED"

    const/16 v14, 0xdf

    const-string v15, "Paper Signature Print Additional Auth Slip Toggled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PAPER_SIGNATURE_PRINT_ADDITIONAL_AUTH_SLIP_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

    .line 242
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PAPER_SIGNATURE_QUICK_TIP_RECEIPT_TOGGLED"

    const/16 v14, 0xe0

    const-string v15, "Paper Signature Quick Tip Receipt Toggled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PAPER_SIGNATURE_QUICK_TIP_RECEIPT_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

    .line 243
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PAPER_SIGNATURE_TOGGLED"

    const/16 v14, 0xe1

    const-string v15, "Paper Signature Toggled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PAPER_SIGNATURE_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

    .line 244
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PASSCODE_AUTHORIZATION_ACCEPTED"

    const/16 v14, 0xe2

    const-string v15, "Passcode Authorization Accepted"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PASSCODE_AUTHORIZATION_ACCEPTED:Lcom/squareup/analytics/RegisterActionName;

    .line 245
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PASSCODE_AUTHORIZATION_CANCELED"

    const/16 v14, 0xe3

    const-string v15, "Passcode Authorization Canceled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PASSCODE_AUTHORIZATION_CANCELED:Lcom/squareup/analytics/RegisterActionName;

    .line 246
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PASSCODE_AUTHORIZATION_FAILED"

    const/16 v14, 0xe4

    const-string v15, "Passcode Authorization Failed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PASSCODE_AUTHORIZATION_FAILED:Lcom/squareup/analytics/RegisterActionName;

    .line 247
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PASSCODE_AUTHORIZATION_PROMPT"

    const/16 v14, 0xe5

    const-string v15, "Passcode Authorization Prompt"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PASSCODE_AUTHORIZATION_PROMPT:Lcom/squareup/analytics/RegisterActionName;

    .line 248
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PASSCODE_EMPLOYEE_MANAGEMENT_SWITCH_EMPLOYEES"

    const/16 v14, 0xe6

    const-string v15, "Switch Employee"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PASSCODE_EMPLOYEE_MANAGEMENT_SWITCH_EMPLOYEES:Lcom/squareup/analytics/RegisterActionName;

    .line 249
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PASSCODE_EMPLOYEE_MANAGEMENT_GUEST_MODE_TOGGLED"

    const/16 v14, 0xe7

    const-string v15, "Require Employee Passcode Only For Restricted Actions"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PASSCODE_EMPLOYEE_MANAGEMENT_GUEST_MODE_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

    .line 251
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "READER_SDK_LOGIN_FAILURE"

    const/16 v14, 0xe8

    const-string v15, "PaySDK Login Failure"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->READER_SDK_LOGIN_FAILURE:Lcom/squareup/analytics/RegisterActionName;

    .line 252
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "READER_SDK_LOGIN_REQUEST"

    const/16 v14, 0xe9

    const-string v15, "PaySDK Login Request"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->READER_SDK_LOGIN_REQUEST:Lcom/squareup/analytics/RegisterActionName;

    .line 253
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "READER_SDK_LOGIN_SUCCESS"

    const/16 v14, 0xea

    const-string v15, "PaySDK Login Success"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->READER_SDK_LOGIN_SUCCESS:Lcom/squareup/analytics/RegisterActionName;

    .line 254
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "READER_SDK_INIT"

    const/16 v14, 0xeb

    const-string v15, "PaySDK Init"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->READER_SDK_INIT:Lcom/squareup/analytics/RegisterActionName;

    .line 255
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PAYMENT_FLOW_QUICK_CASH_CUSTOM_OPTION"

    const/16 v14, 0xec

    const-string v15, "Payment Flow Payment Methods: Quick Cash Custom Option Chosen"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PAYMENT_FLOW_QUICK_CASH_CUSTOM_OPTION:Lcom/squareup/analytics/RegisterActionName;

    .line 257
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PAYMENT_FLOW_QUICK_CASH_OPTION"

    const/16 v14, 0xed

    const-string v15, "Payment Flow Payment Methods:Quick Cash Option Chosen"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PAYMENT_FLOW_QUICK_CASH_OPTION:Lcom/squareup/analytics/RegisterActionName;

    .line 258
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PAYMENT_FLOW_RECEIPT_VIEW_SELECT_EMAIL"

    const/16 v14, 0xee

    const-string v15, "Payment Flow Receipt View: Selected Email"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PAYMENT_FLOW_RECEIPT_VIEW_SELECT_EMAIL:Lcom/squareup/analytics/RegisterActionName;

    .line 259
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PAYMENT_TUTORIAL_DECLINED"

    const/16 v14, 0xef

    const-string v15, "Payment Tutorial Declined"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PAYMENT_TUTORIAL_DECLINED:Lcom/squareup/analytics/RegisterActionName;

    .line 260
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PREDEFINED_TICKET_GROUP_CREATED"

    const/16 v14, 0xf0

    const-string v15, "Ticket Group Created"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PREDEFINED_TICKET_GROUP_CREATED:Lcom/squareup/analytics/RegisterActionName;

    .line 261
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PREDEFINED_TICKET_CONVERT_TO_CUSTOM"

    const/16 v14, 0xf1

    const-string v15, "Convert Predefined Ticket to Custom"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PREDEFINED_TICKET_CONVERT_TO_CUSTOM:Lcom/squareup/analytics/RegisterActionName;

    .line 262
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PREDEFINED_TICKETS_TOGGLE"

    const/16 v14, 0xf2

    const-string v15, "Use Predefined Tickets"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PREDEFINED_TICKETS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    .line 263
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_AUTH_SLIP"

    const/16 v14, 0xf3

    const-string v15, "Print Controller Print Auth Slip"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_AUTH_SLIP:Lcom/squareup/analytics/RegisterActionName;

    .line 264
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_AUTH_SLIP_PAPER"

    const/16 v14, 0xf4

    const-string v15, "Printer Paper Auth Slip"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_AUTH_SLIP_PAPER:Lcom/squareup/analytics/RegisterActionName;

    .line 265
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_AUTH_SLIP_SUCCEEDED"

    const/16 v14, 0xf5

    const-string v15, "Print Controller Auth Slip Printed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_AUTH_SLIP_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

    .line 266
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_AUTH_SLIP_COPY"

    const/16 v14, 0xf6

    const-string v15, "Print Controller Print Auth Slip Copy"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_AUTH_SLIP_COPY:Lcom/squareup/analytics/RegisterActionName;

    .line 267
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_AUTH_SLIP_COPY_PAPER"

    const/16 v14, 0xf7

    const-string v15, "Printer Paper Auth Slip Copy"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_AUTH_SLIP_COPY_PAPER:Lcom/squareup/analytics/RegisterActionName;

    .line 268
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_AUTH_SLIP_COPY_SUCCEEDED"

    const/16 v14, 0xf8

    const-string v15, "Print Controller Auth Slip Copy Printed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_AUTH_SLIP_COPY_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

    .line 269
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_BILL_PAPER"

    const/16 v14, 0xf9

    const-string v15, "Printer Paper Bill"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_BILL_PAPER:Lcom/squareup/analytics/RegisterActionName;

    .line 270
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_CASH_REPORT_PAPER"

    const/16 v14, 0xfa

    const-string v15, "Printer Paper Cash Report"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_CASH_REPORT_PAPER:Lcom/squareup/analytics/RegisterActionName;

    .line 271
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_ERROR_REPRINT"

    const/16 v14, 0xfb

    const-string v15, "Print Error Reprint Tapped"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_ERROR_REPRINT:Lcom/squareup/analytics/RegisterActionName;

    .line 272
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_GIFT_RECEIPT"

    const/16 v14, 0xfc

    const-string v15, "Print Controller Print Gift Receipt"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_GIFT_RECEIPT:Lcom/squareup/analytics/RegisterActionName;

    .line 273
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_RECEIPT"

    const/16 v14, 0xfd

    const-string v15, "Print Controller Print Receipt"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_RECEIPT:Lcom/squareup/analytics/RegisterActionName;

    .line 274
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_RECEIPT_PAPER"

    const/16 v14, 0xfe

    const-string v15, "Printer Paper Receipt"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_RECEIPT_PAPER:Lcom/squareup/analytics/RegisterActionName;

    .line 275
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_RECEIPT_SUCCEEDED"

    const/16 v14, 0xff

    const-string v15, "Print Controller Receipt Printed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_RECEIPT_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

    .line 276
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_SALES_SUMMARY_PAPER"

    const/16 v14, 0x100

    const-string v15, "Printer Paper Sales Summary"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_SALES_SUMMARY_PAPER:Lcom/squareup/analytics/RegisterActionName;

    .line 277
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_STUB"

    const/16 v14, 0x101

    const-string v15, "Print Controller Print Ticket Stub"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_STUB:Lcom/squareup/analytics/RegisterActionName;

    .line 278
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_STUB_PAPER"

    const/16 v14, 0x102

    const-string v15, "Printer Paper Ticket Stub"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_STUB_PAPER:Lcom/squareup/analytics/RegisterActionName;

    .line 279
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_STUB_SUCCEEDED"

    const/16 v14, 0x103

    const-string v15, "Print Controller Ticket Stub Printed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_STUB_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

    .line 280
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_TICKET"

    const/16 v14, 0x104

    const-string v15, "Print Controller Print Ticket"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_TICKET:Lcom/squareup/analytics/RegisterActionName;

    .line 281
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_TICKET_PAPER"

    const/16 v14, 0x105

    const-string v15, "Printer Paper Ticket"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_TICKET_PAPER:Lcom/squareup/analytics/RegisterActionName;

    .line 282
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_TICKET_SUCCEEDED"

    const/16 v14, 0x106

    const-string v15, "Print Controller Ticket Printed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_TICKET_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

    .line 283
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_TIMECARDS_SUMMARY"

    const/16 v14, 0x107

    const-string v15, "Print Team Member Shift Summary"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_TIMECARDS_SUMMARY:Lcom/squareup/analytics/RegisterActionName;

    .line 284
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_JOB_ENQUEUED"

    const/16 v14, 0x108

    const-string v15, "Print Controller Print Job Enqueued"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_JOB_ENQUEUED:Lcom/squareup/analytics/RegisterActionName;

    .line 285
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_JOB_FAILED"

    const/16 v14, 0x109

    const-string v15, "Print Controller Print Job Failed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_JOB_FAILED:Lcom/squareup/analytics/RegisterActionName;

    .line 286
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_SPOOLER_TARGET_HARDWARE_PRINTER_UNAVAILABLE_ERROR"

    const/16 v14, 0x10a

    const-string v15, "Print Spooler Error: Target Hardware Printer Unavailable"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_SPOOLER_TARGET_HARDWARE_PRINTER_UNAVAILABLE_ERROR:Lcom/squareup/analytics/RegisterActionName;

    .line 288
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_SPOOLER_NO_HARDWARE_PRINTER_ERROR"

    const/16 v14, 0x10b

    const-string v15, "Print Spooler Error: Target Has No Hardware Printer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_SPOOLER_NO_HARDWARE_PRINTER_ERROR:Lcom/squareup/analytics/RegisterActionName;

    .line 289
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_SPOOLER_TARGET_DOES_NOT_EXIST_ERROR"

    const/16 v14, 0x10c

    const-string v15, "Print Spooler Error: Target Does Not Exist"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_SPOOLER_TARGET_DOES_NOT_EXIST_ERROR:Lcom/squareup/analytics/RegisterActionName;

    .line 290
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_VOID_TICKET"

    const/16 v14, 0x10d

    const-string v15, "Print Controller Print Void Ticket"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_VOID_TICKET:Lcom/squareup/analytics/RegisterActionName;

    .line 291
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_VOID_TICKET_PAPER"

    const/16 v14, 0x10e

    const-string v15, "Printer Paper Void Ticket"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_VOID_TICKET_PAPER:Lcom/squareup/analytics/RegisterActionName;

    .line 292
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINT_VOID_TICKET_SUCCEEDED"

    const/16 v14, 0x10f

    const-string v15, "Print Controller Void Ticket Printed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_VOID_TICKET_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

    .line 293
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINTER_AUTONUMBER_TOGGLE"

    const/16 v14, 0x110

    const-string v15, "Print Ticket Auto Number Toggle"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINTER_AUTONUMBER_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    .line 294
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINTER_AUTONUMBER_TOGGLE_CANCEL"

    const/16 v14, 0x111

    const-string v15, "Print Ticket Auto Number Toggle Cancel"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINTER_AUTONUMBER_TOGGLE_CANCEL:Lcom/squareup/analytics/RegisterActionName;

    .line 295
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINTER_BLOCKED"

    const/16 v14, 0x112

    const-string v15, "Printer Blocked"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINTER_BLOCKED:Lcom/squareup/analytics/RegisterActionName;

    .line 296
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINTER_CATEGORY_TOGGLE"

    const/16 v14, 0x113

    const-string v15, "Printer Settings Print Category Toggle"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINTER_CATEGORY_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    .line 297
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINTER_CONNECT"

    const/16 v14, 0x114

    const-string v15, "Printing Service Connected Printer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINTER_CONNECT:Lcom/squareup/analytics/RegisterActionName;

    .line 298
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINTER_DISCONNECT"

    const/16 v14, 0x115

    const-string v15, "Printing Service Disconnected Printer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINTER_DISCONNECT:Lcom/squareup/analytics/RegisterActionName;

    .line 299
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINTER_DISCONNECT_TCP_PING_HISTORY"

    const/16 v14, 0x116

    const-string v15, "Printing Service: Disconnected Printer Ping History"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINTER_DISCONNECT_TCP_PING_HISTORY:Lcom/squareup/analytics/RegisterActionName;

    .line 300
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINTER_PRINT_A_TICKET_FOR_EACH_ITEM_TOGGLED"

    const/16 v14, 0x117

    const-string v15, "Printer Settings: Print Order Tickets: Print Single Ticket Toggled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINTER_PRINT_A_TICKET_FOR_EACH_ITEM_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

    .line 302
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINTER_RECEIPTS_TOGGLE"

    const/16 v14, 0x118

    const-string v15, "Printer Settings Prints Receipts Toggle"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINTER_RECEIPTS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    .line 303
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINTER_STUBS_TOGGLE"

    const/16 v14, 0x119

    const-string v15, "Printer Settings Prints Order Ticket Stubs Toggle"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINTER_STUBS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    .line 304
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINTER_TICKETS_TOGGLE"

    const/16 v14, 0x11a

    const-string v15, "Printer Settings Prints Order Tickets Toggle"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINTER_TICKETS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    .line 305
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRINTER_UNCATEGORIZED_ITEMS_TOGGLE"

    const/16 v14, 0x11b

    const-string v15, "Printer Settings Prints Uncategorized Items Toggle"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINTER_UNCATEGORIZED_ITEMS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    .line 306
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRODUCT_ACTIVATION"

    const/16 v14, 0x11c

    const-string v15, "Product Activation"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRODUCT_ACTIVATION:Lcom/squareup/analytics/RegisterActionName;

    .line 307
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "PRODUCT_SIGNUP"

    const/16 v14, 0x11d

    const-string v15, "Product Signup"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->PRODUCT_SIGNUP:Lcom/squareup/analytics/RegisterActionName;

    .line 308
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "R12_MEET_THE_READER_MODAL_DISMISSED_BY_USER"

    const/16 v14, 0x11e

    const-string v15, "Meet the Reader Modal - Dismissed by User"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->R12_MEET_THE_READER_MODAL_DISMISSED_BY_USER:Lcom/squareup/analytics/RegisterActionName;

    .line 309
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "R12_MULTIPAGE_WALKTHROUGH_DISMISSED"

    const/16 v14, 0x11f

    const-string v15, "Multipage Walkthrough - Dismissed by User"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->R12_MULTIPAGE_WALKTHROUGH_DISMISSED:Lcom/squareup/analytics/RegisterActionName;

    .line 310
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "RECEIPT_CLOSE_AUTOMATICALLY"

    const/16 v14, 0x120

    const-string v15, "Receipt: Close Automatically"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->RECEIPT_CLOSE_AUTOMATICALLY:Lcom/squareup/analytics/RegisterActionName;

    .line 311
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "REPORTS_EMAIL_SALES_REPORT_SENT"

    const/16 v14, 0x121

    const-string v15, "Reports: Email Sales Report Sent"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->REPORTS_EMAIL_SALES_REPORT_SENT:Lcom/squareup/analytics/RegisterActionName;

    .line 312
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "REPORTS_TOGGLE_ALL_DAY_SETTING"

    const/16 v14, 0x122

    const-string v15, "Reports: All Day Setting Toggled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->REPORTS_TOGGLE_ALL_DAY_SETTING:Lcom/squareup/analytics/RegisterActionName;

    .line 313
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "REPORTS_TOGGLE_ITEM_DETAILS_SETTING"

    const/16 v14, 0x123

    const-string v15, "Reports: Item Details Setting Toggled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->REPORTS_TOGGLE_ITEM_DETAILS_SETTING:Lcom/squareup/analytics/RegisterActionName;

    .line 314
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "REPRINT_TICKET"

    const/16 v14, 0x124

    const-string v15, "Reprint Ticket"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->REPRINT_TICKET:Lcom/squareup/analytics/RegisterActionName;

    .line 315
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "RST_T2_DEFAULT_SETTING_OPEN_TICKETS"

    const/16 v14, 0x125

    const-string v15, "RST Friendly T2 Default Setting: Enable Open Tickets"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->RST_T2_DEFAULT_SETTING_OPEN_TICKETS:Lcom/squareup/analytics/RegisterActionName;

    .line 317
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "RST_T2_DEFAULT_SETTING_PREDEFINED_TICKETS"

    const/16 v14, 0x126

    const-string v15, "RST Friendly T2 Default Setting: Enable Predefined Tickets"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->RST_T2_DEFAULT_SETTING_PREDEFINED_TICKETS:Lcom/squareup/analytics/RegisterActionName;

    .line 319
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "RST_T2_DEFAULT_SETTING_OPEN_TICKETS_AS_HOME"

    const/16 v14, 0x127

    const-string v15, "RST Friendly T2 Default Setting: Enable Open Tickets As Home"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->RST_T2_DEFAULT_SETTING_OPEN_TICKETS_AS_HOME:Lcom/squareup/analytics/RegisterActionName;

    .line 321
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "RST_T2_DEFAULT_SETTING_REQUIRE_EMPLOYEE_PASSCODE"

    const/16 v14, 0x128

    const-string v15, "RST Friendly T2 Default Setting: Always Require Employee Passcode"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->RST_T2_DEFAULT_SETTING_REQUIRE_EMPLOYEE_PASSCODE:Lcom/squareup/analytics/RegisterActionName;

    .line 323
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SALES_REPORT_EMAILED"

    const/16 v14, 0x129

    const-string v15, "Sales Report: Emailed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SALES_REPORT_EMAILED:Lcom/squareup/analytics/RegisterActionName;

    .line 324
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SALES_REPORT_PRINTED"

    const/16 v14, 0x12a

    const-string v15, "Sales Report: Printed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SALES_REPORT_PRINTED:Lcom/squareup/analytics/RegisterActionName;

    .line 325
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SEND_RECEIPT"

    const/16 v14, 0x12b

    const-string v15, "Send Receipt"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SEND_RECEIPT:Lcom/squareup/analytics/RegisterActionName;

    .line 326
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SERVICE_EDIT_DISPLAY_PRICE"

    const/16 v14, 0x12c

    const-string v15, "Service: Edit Display Price"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SERVICE_EDIT_DISPLAY_PRICE:Lcom/squareup/analytics/RegisterActionName;

    .line 327
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SETTLED_TIPS_FROM_BATCH_VIEW"

    const/16 v14, 0x12d

    const-string v15, "Settled Tips From Batch View"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SETTLED_TIPS_FROM_BATCH_VIEW:Lcom/squareup/analytics/RegisterActionName;

    .line 328
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SETTLED_TIPS_FROM_TRANSACTION_VIEW"

    const/16 v14, 0x12e

    const-string v15, "Settled Tips From Transaction View"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SETTLED_TIPS_FROM_TRANSACTION_VIEW:Lcom/squareup/analytics/RegisterActionName;

    .line 329
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SIGN_IN_DECRYPT_FAIL"

    const/16 v14, 0x12f

    const-string v15, "Welcome Flow Sign In Decrypt Fail"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SIGN_IN_DECRYPT_FAIL:Lcom/squareup/analytics/RegisterActionName;

    .line 330
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SIGN_IN_SHOWED_EMAIL"

    const/16 v14, 0x130

    const-string v15, "Welcome Flow Sign In Showed Email"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SIGN_IN_SHOWED_EMAIL:Lcom/squareup/analytics/RegisterActionName;

    .line 331
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SKIP_RECEIPT_SCREEN_TOGGLE"

    const/16 v14, 0x131

    const-string v15, "Skip Receipt Screen Toggle"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SKIP_RECEIPT_SCREEN_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    .line 332
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SKIP_SIG_SETTING"

    const/16 v14, 0x132

    const-string v15, "Skip Sig Setting"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SKIP_SIG_SETTING:Lcom/squareup/analytics/RegisterActionName;

    .line 333
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SKIP_SIG_TOGGLE"

    const/16 v14, 0x133

    const-string v15, "Skip Sig Toggle"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SKIP_SIG_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    .line 334
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SPLIT_TENDER_CUSTOM_SPLIT"

    const/16 v14, 0x134

    const-string v15, "Checkout: Split By Amount"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TENDER_CUSTOM_SPLIT:Lcom/squareup/analytics/RegisterActionName;

    .line 335
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SPLIT_TENDER_DISMISS_SPLIT"

    const/16 v14, 0x135

    const-string v15, "Checkout: Dismiss Split Tender"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TENDER_DISMISS_SPLIT:Lcom/squareup/analytics/RegisterActionName;

    .line 336
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SPLIT_TENDER_EVEN_SPLIT"

    const/16 v14, 0x136

    const-string v15, "Checkout: Even Payment Split"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TENDER_EVEN_SPLIT:Lcom/squareup/analytics/RegisterActionName;

    .line 337
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SPLIT_TICKET_ADD_CUSTOMER"

    const/16 v14, 0x137

    const-string v15, "Split Ticket: Add Customer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_ADD_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    .line 338
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SPLIT_TICKET_CANCEL"

    const/16 v14, 0x138

    const-string v15, "Split Ticket: Cancel"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_CANCEL:Lcom/squareup/analytics/RegisterActionName;

    .line 339
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SPLIT_TICKET_EDIT"

    const/16 v14, 0x139

    const-string v15, "Split Ticket: Edit"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_EDIT:Lcom/squareup/analytics/RegisterActionName;

    .line 340
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SPLIT_TICKET_VIEW_CUSTOMER"

    const/16 v14, 0x13a

    const-string v15, "Split Ticket: View Customer"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_VIEW_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    .line 341
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SPLIT_TICKET_MOVE_ITEMS"

    const/16 v14, 0x13b

    const-string v15, "Split Ticket: Move Items"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_MOVE_ITEMS:Lcom/squareup/analytics/RegisterActionName;

    .line 342
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SPLIT_TICKET_PRINT"

    const/16 v14, 0x13c

    const-string v15, "Split Ticket: Print"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_PRINT:Lcom/squareup/analytics/RegisterActionName;

    .line 343
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SPLIT_TICKET_PRINT_ALL"

    const/16 v14, 0x13d

    const-string v15, "Split Ticket: Print All"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_PRINT_ALL:Lcom/squareup/analytics/RegisterActionName;

    .line 344
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SPLIT_TICKET_SAVE"

    const/16 v14, 0x13e

    const-string v15, "Split Ticket: Save"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_SAVE:Lcom/squareup/analytics/RegisterActionName;

    .line 345
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SPLIT_TICKET_SAVE_ALL"

    const/16 v14, 0x13f

    const-string v15, "Split Ticket: Save All"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_SAVE_ALL:Lcom/squareup/analytics/RegisterActionName;

    .line 346
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SPLIT_TICKET_START"

    const/16 v14, 0x140

    const-string v15, "Split Ticket: Start"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_START:Lcom/squareup/analytics/RegisterActionName;

    .line 347
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SUPPORT_CASH_PAYMENT_TUTORIAL_MANUALLY_STARTED"

    const/16 v14, 0x141

    const-string v15, "Cash Payment Tutorial Manually Started"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SUPPORT_CASH_PAYMENT_TUTORIAL_MANUALLY_STARTED:Lcom/squareup/analytics/RegisterActionName;

    .line 348
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "SUPPORT_PAYMENT_TUTORIAL_MANUALLY_STARTED"

    const/16 v14, 0x142

    const-string v15, "Payment Tutorial Manually Started"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->SUPPORT_PAYMENT_TUTORIAL_MANUALLY_STARTED:Lcom/squareup/analytics/RegisterActionName;

    .line 349
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "TAX_DISABLED"

    const/16 v14, 0x143

    const-string v15, "Tax setting changed off"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->TAX_DISABLED:Lcom/squareup/analytics/RegisterActionName;

    .line 350
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "TAX_ENABLED"

    const/16 v14, 0x144

    const-string v15, "Tax setting changed on"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->TAX_ENABLED:Lcom/squareup/analytics/RegisterActionName;

    .line 351
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "TAX_RULE_APPLIED_IN_TRANSACTION"

    const/16 v14, 0x145

    const-string v15, "Tax Rule Applied In Transaction"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->TAX_RULE_APPLIED_IN_TRANSACTION:Lcom/squareup/analytics/RegisterActionName;

    .line 352
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "TILE_APPEARANCE_CANCELED"

    const/16 v14, 0x146

    const-string v15, "Tile Appearance Canceled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->TILE_APPEARANCE_CANCELED:Lcom/squareup/analytics/RegisterActionName;

    .line 353
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "TILE_APPEARANCE_TOGGLE"

    const/16 v14, 0x147

    const-string v15, "Tile Appearance Toggled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->TILE_APPEARANCE_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    .line 354
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "TIP_SETTINGS_COLLECT_TIPS_TOGGLE"

    const/16 v14, 0x148

    const-string v15, "Tip Settings Collect Tips Toggle"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->TIP_SETTINGS_COLLECT_TIPS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    .line 355
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "TOUR_CLOSED"

    const/16 v14, 0x149

    const-string v15, "Tour closed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->TOUR_CLOSED:Lcom/squareup/analytics/RegisterActionName;

    .line 356
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "VOID_CART_STARTED"

    const/16 v14, 0x14a

    const-string v15, "Void Cart: Started"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->VOID_CART_STARTED:Lcom/squareup/analytics/RegisterActionName;

    .line 357
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "VOID_CART_CANCELED"

    const/16 v14, 0x14b

    const-string v15, "Void Cart: Canceled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->VOID_CART_CANCELED:Lcom/squareup/analytics/RegisterActionName;

    .line 358
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "VOID_CART_VOIDED"

    const/16 v14, 0x14c

    const-string v15, "Void Cart: Voided"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->VOID_CART_VOIDED:Lcom/squareup/analytics/RegisterActionName;

    .line 359
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "VOID_ITEMIZATION_STARTED"

    const/16 v14, 0x14d

    const-string v15, "Void Itemization: Started"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->VOID_ITEMIZATION_STARTED:Lcom/squareup/analytics/RegisterActionName;

    .line 360
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "VOID_ITEMIZATION_CANCELED"

    const/16 v14, 0x14e

    const-string v15, "Void Itemization: Canceled"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->VOID_ITEMIZATION_CANCELED:Lcom/squareup/analytics/RegisterActionName;

    .line 361
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "VOID_ITEMIZATION_VOIDED"

    const/16 v14, 0x14f

    const-string v15, "Void Itemization: Voided"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->VOID_ITEMIZATION_VOIDED:Lcom/squareup/analytics/RegisterActionName;

    .line 362
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "WELCOME_SPLASH_CREATE_ACCOUNT"

    const/16 v14, 0x150

    const-string v15, "Welcome Flow Splash Page: Create Account"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->WELCOME_SPLASH_CREATE_ACCOUNT:Lcom/squareup/analytics/RegisterActionName;

    .line 363
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "WELCOME_SPLASH_SIGN_IN"

    const/16 v14, 0x151

    const-string v15, "Welcome Flow Splash Page: Sign-in"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->WELCOME_SPLASH_SIGN_IN:Lcom/squareup/analytics/RegisterActionName;

    .line 364
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "WIFI_CONNECTED"

    const/16 v14, 0x152

    const-string v15, "WiFi connected"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->WIFI_CONNECTED:Lcom/squareup/analytics/RegisterActionName;

    .line 365
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "X2_PAYMENT_CLIENT_ERROR"

    const/16 v14, 0x153

    const-string v15, "X2 Payment Client Error"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->X2_PAYMENT_CLIENT_ERROR:Lcom/squareup/analytics/RegisterActionName;

    .line 366
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "X2_PAYMENT_NETWORK_ERROR"

    const/16 v14, 0x154

    const-string v15, "X2 Payment Network Error"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->X2_PAYMENT_NETWORK_ERROR:Lcom/squareup/analytics/RegisterActionName;

    .line 367
    new-instance v0, Lcom/squareup/analytics/RegisterActionName;

    const-string v13, "X2_PAYMENT_SERVER_ERROR"

    const/16 v14, 0x155

    const-string v15, "X2 Payment Server Error"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->X2_PAYMENT_SERVER_ERROR:Lcom/squareup/analytics/RegisterActionName;

    const/16 v0, 0x156

    new-array v0, v0, [Lcom/squareup/analytics/RegisterActionName;

    .line 8
    sget-object v13, Lcom/squareup/analytics/RegisterActionName;->ACCOUNT_OWNER_PASSCODE_AUTHORIZATION_PROMPT:Lcom/squareup/analytics/RegisterActionName;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ACTIVATION_CONTACTLESS_ORDER_SUCCEED:Lcom/squareup/analytics/RegisterActionName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ACTIVITY_SEARCH:Lcom/squareup/analytics/RegisterActionName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ADVANCED_MODIFIER_HIDE_MODIFIER_FROM_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ADVANCED_MODIFIER_MIN_MAX_MODIFIER_USED_IN_TRANSACTION:Lcom/squareup/analytics/RegisterActionName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ADVANCED_MODIFIER_PRE_SELECTED_MODIFIER_USED_IN_TRANSACTION:Lcom/squareup/analytics/RegisterActionName;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ADVANCED_MODIFIER_SKIP_ITEM_MODAL:Lcom/squareup/analytics/RegisterActionName;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ADVANCED_MODIFIER_SKIP_ITEM_MODAL_OPEN_ITEM_IN_CART:Lcom/squareup/analytics/RegisterActionName;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ADVANCED_MODIFIER_SKIP_ITEM_MODAL_STILL_SHOWN:Lcom/squareup/analytics/RegisterActionName;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->API_BUYER_STARTED:Lcom/squareup/analytics/RegisterActionName;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->API_NEW_TRANSACTION:Lcom/squareup/analytics/RegisterActionName;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->API_NEW_READER_SETTINGS:Lcom/squareup/analytics/RegisterActionName;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->API_NEW_STORE_CARD:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->API_READER_SETTINGS_STARTED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->API_READER_SETTINGS_FAILURE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->API_READER_SETTINGS_SUCCESS:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->API_STORE_CARD_STARTED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->API_STORE_CARD_FAILURE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->API_STORE_CARD_SUCCESS:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->API_TENDER_STARTED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->API_TRANSACTION_FAILURE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->API_TRANSACTION_SUCCESS:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->APPLET_SWITCHER_DID_CLOSE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->APPLET_SWITCHER_DID_OPEN:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->APPLET_SWITCHER_SELECTED_APPLET:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->APPOINTMENT_CREATE_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->AUDIO_BLOCKING_DIALOG:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->AUTOMATICALLY_PRINT_ITEMIZED_RECEIPT_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->AVAILABLE_EMAILS:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->BACKGROUND_JOB_LOG:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->BARCODE_SCANNED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->BARCODE_SCANNER_CONNECT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->BARCODE_SCANNER_DISCONNECT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CARD_ON_FILE_ADD_CARD:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CARD_ON_FILE_ADD_CARD_POST_TRANSACTION:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CARD_ON_FILE_CHARGE_ATTEMPT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CARD_ON_FILE_CHARGE_FAILURE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CARD_ON_FILE_CHARGE_SUCCESS:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CARD_ON_FILE_REMOVE_CARD:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CASH_DRAWER_CONNECTED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CASH_DRAWER_DISCONNECTED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CASH_DRAWER_FAILED_TO_CONNECT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CASH_DRAWERS_OPENED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CASH_MANAGEMENT_DRAWER_CLOSED_FROM_ENDED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CASH_MANAGEMENT_DRAWER_CLOSED_FROM_STARTED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CASH_MANAGEMENT_DRAWER_ENDED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CASH_MANAGEMENT_DRAWER_OPENED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CASH_MANAGEMENT_END_DRAWER_CLOSE_AUTOMATICALLY:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CASH_MANAGEMENT_END_DRAWER_CLOSE_MANUALLY:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CASH_MANAGEMENT_START_SHIFT_DRAWER_MODAL:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CASH_PAYMENT_TUTORIAL_DECLINED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CHECKOUT_INFORMATION:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CHECKOUT_ADD_GIFT_CARD:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CHECKOUT_ITEMIZED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CLIENT_ACTION_TRANSLATION_HANDLED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CLIENT_ACTION_TRANSLATION_NO_TRANSLATOR:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CLIENT_ACTION_TRANSLATION_URL_NOT_HANDLED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->COMP_CART_STARTED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->COMP_CART_CANCELED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->COMP_CART_COMPED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->COMP_CART_UNCOMPED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->COMP_ITEMIZATION_STARTED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->COMP_ITEMIZATION_CANCELED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->COMP_ITEMIZATION_COMPED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->COMP_ITEMIZATION_UNCOMPED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CREATE_ITEM_TUTORIAL_DRAG_ITEM:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CREATE_ITEM_TUTORIAL_ITEM_NAME_ENTERED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CREATE_ITEM_TUTORIAL_PRICE_ENTERED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_ADD_CUSTOMER_TO_SALE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_CREATE_CONTACT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_CUSTOMER_MANAGEMENT_AFTER_CHECKOUT_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_CUSTOMER_MANAGEMENT_BEFORE_CHECKOUT_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_CUSTOMER_MANAGEMENT_CARD_ON_FILE_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_CUSTOMER_MANAGEMENT_CARD_ON_FILE_POST_TRANSACTION_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_EDIT_CONTACT_SAVED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_EMAIL_COLLECTION_SCREEN_NEW_SALE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_EMAIL_COLLECTION_SCREEN_NO_THANKS:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_EMAIL_COLLECTION_SCREEN_SUBMIT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_EMAIL_COLLECTION_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_GET_CONTACT_NULL:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_MESSAGING_CREATE_CONVERSATION_SEND:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_MESSAGING_SHOW_CONVERSATION_SEND:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_REMOVE_CUSTOMER_FROM_MODAL:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_MULTI_SELECT_DELETE_CANCEL:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_MULTI_SELECT_DELETE_DELETE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_MULTI_SELECT_MERGE_CANCEL:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_MULTI_SELECT_MERGE_MERGE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_FILTER_ADD_FILTER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_FILTER_APPLY:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_FILTER_CLOSE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x59

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_FILTER_REMOVE_ALL:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_FILTER_REMOVE_FILTER_PILL:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_FILTER_SAVE_AS_GROUP:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_GROUPS_CLOSE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_GROUPS_CREATE_NEW:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_GROUPS_DELETE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_GROUPS_SAVE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x60

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_GROUPS_SELECT_AUTO_SMART_GROUP:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_GROUPS_SELECT_SMART_GROUP:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_GROUPS_SELECT_MANUAL_GROUP:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_ADD_CARD:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_ADD_FILES:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_ADD_NOTE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_EDIT_NOTE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_EDIT_FILE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_EDIT_PERSONAL:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_ADD_CARD:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_ADD_NOTE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_ADD_TO_SALE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_DELETE_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_EDIT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x6e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_MERGE_PROFILE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x6f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_NEW_SALE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x70

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_ADD_APPOINTMENT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x71

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_SEND_MESSAGE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x72

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_REMOVE_FROM_ESTIMATE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x73

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_REMOVE_FROM_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x74

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_REMOVE_FROM_SALE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x75

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_UPLOAD_FILE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x76

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_REMOVE_CARD:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x77

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_TRANSFER_LOYALTY:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x78

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_VIEW_ALL_ACTIVITY:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x79

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_VIEW_ALL_COUPONS:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x7a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_VIEW_ALL_FILES:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x7b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_VIEW_ALL_FREQUENT_ITEMS:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x7c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CUSTOMER_CHECKOUT_ENTER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x7d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->COUPONS_SEARCH:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x7e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->DEEP_LINK_BRANCH:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x7f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->DEEP_LINK_HANDLED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x80

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->DEEP_LINK_REJECTED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x81

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->EDIT_FAVORITES_PAGE_NAME:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x82

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->EMPLOYEE_ACCESS_DENIED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x83

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->FEE_TUTORIAL_DIP_DISMISS:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x84

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->FEE_TUTORIAL_DIP_JCB_DISMISS:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x85

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->FEE_TUTORIAL_INTERAC_DISMISS:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x86

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->FEE_TUTORIAL_MANUAL_DISMISS:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x87

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->FEE_TUTORIAL_SWIPE_DISMISS:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x88

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->FEE_TUTORIAL_TAP_DISMISS:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x89

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->GAP_TIME_MODIFIED_CART:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x8a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->GAP_TIME_MODIFIED_SERVICE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x8b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INCREMENTAL_ITEMS_SYNC:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x8c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INSTANT_DEPOSIT_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x8d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICE_CHOOSE_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x8e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICE_CREATE_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x8f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_CANCEL_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x90

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_COPY_LINK:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x91

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_CREATE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x92

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_CREATE_SHARE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x93

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_DELETE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x94

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_DOWNLOAD_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x95

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_DUPLICATE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x96

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_FILTER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x97

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_PREVIEW:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x98

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SAVE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x99

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SEARCH:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x9a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SCHEDULE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x9b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SHARE_LINK:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x9c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SEND_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x9d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SEND_RECURRING_SERIES:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x9e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_VIEW_TIMELINE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x9f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_CATEGORY_CREATED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xa0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_CATEGORY_DELETED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xa1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_CATEGORY_EDITED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xa2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_DISCOUNT_CREATED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xa3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_DISCOUNT_DELETED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xa4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_DISCOUNT_EDITED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xa5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_EDIT_CATALOG_OBJECT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xa6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_GIFT_CARD_CREATED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xa7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_GIFT_CARD_DELETED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xa8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_GIFT_CARD_EDITED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xa9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_ITEM_CREATED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xaa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_ITEM_DELETED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xab

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_ITEM_EDITED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xac

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_MODIFIER_SET_CREATED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xad

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_MODIFIER_SET_DELETED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xae

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_MODIFIER_SET_EDITED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xaf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->LOG_OUT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xb0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_CART_POINTS_MODAL_OPENED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xb1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_CART_POINTS_MODAL_ADD_CUSTOMER_FLOW_STARTED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xb2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_CART_POINTS_MODAL_APPLY_REWARD:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xb3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_CART_POINTS_MODAL_CUSTOMER_ADDED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xb4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_CART_POINTS_MODAL_REMOVE_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xb5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_ENROLL_SUBMIT_BUYER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xb6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_ENROLL_SUBMIT_MERCHANT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xb7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_ENROLL_SUBMIT_SUCCESS_BUYER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xb8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_ENROLL_SUBMIT_SUCCESS_MERCHANT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xb9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_ENROLL_SUBMIT_TIMEOUT_BUYER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xba

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_ENROLL_SUBMIT_TIMEOUT_MERCHANT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xbb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_NO_THANKS_BUYER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xbc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_NO_THANKS_MERCHANT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xbd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_REPORT_CHANGE_DATE_RANGE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xbe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->NOTIFICATION_CENTER_ACCOUNT_VIEW:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xbf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->NOTIFICATION_CENTER_BROWSER_DIALOG_CANCEL:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xc0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->NOTIFICATION_CENTER_BROWSER_DIALOG_OPEN:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xc1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->NOTIFICATION_CENTER_NOTIFICATION_CLICK:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xc2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->NOTIFICATION_CENTER_NOTIFICATION_OPEN:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xc3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->NOTIFICATION_CENTER_LOADED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xc4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->NOTIFICATION_CENTER_PRODUCT_VIEW:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xc5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ONBOARDING_CONTINUE_TO_WEB:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xc6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ONBOARDING_LATER_IN_INITIAL_SHIPPING:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xc7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ONBOARDING_LATER_IN_MCC:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xc8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ONBOARDING_LATER_IN_WEB:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xc9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ONBOARDING_NOT_USE_PREFILL_EMAIL:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xca

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ONBOARDING_REDIRECT_DOWNLOAD:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xcb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ONBOARDING_WEB_ACTIVATION_COMPLETE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xcc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xcd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_AS_HOME_SCREEN_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xce

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_EXIT_EDIT_TICKET:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xcf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_ITEMIZATION_VOID:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xd0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_LOADED_EXISTING_TICKET:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xd1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_MERGE_TICKETS_AFTER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xd2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_MERGE_TICKETS_BEFORE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xd3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_MERGE_TICKETS_CANCELED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xd4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_MERGED_CART_TO_EXISTING_TICKET:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xd5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_MOVE_TICKETS:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xd6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_NEW_TICKET_WITH_EXISTING_CART:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xd7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_NEW_TICKET_WITHOUT_EXISTING_CART:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xd8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKET_SAVED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xd9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_SAVED_DISCOUNT_ONLY_CART:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xda

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_UNSYNCED_TICKETS_UPLOADED_VIA_SWEEPER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xdb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_VIEWED_LOCKED_ITEMIZATION:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xdc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ORDER_ENTRY_CLEAR_SALE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xdd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OWNER_PASSCODE_AUTHORIZATION_PROMPT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xde

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PAPER_SIGNATURE_PRINT_ADDITIONAL_AUTH_SLIP_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xdf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PAPER_SIGNATURE_QUICK_TIP_RECEIPT_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xe0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PAPER_SIGNATURE_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xe1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PASSCODE_AUTHORIZATION_ACCEPTED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xe2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PASSCODE_AUTHORIZATION_CANCELED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xe3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PASSCODE_AUTHORIZATION_FAILED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xe4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PASSCODE_AUTHORIZATION_PROMPT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xe5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PASSCODE_EMPLOYEE_MANAGEMENT_SWITCH_EMPLOYEES:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xe6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PASSCODE_EMPLOYEE_MANAGEMENT_GUEST_MODE_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xe7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->READER_SDK_LOGIN_FAILURE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xe8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->READER_SDK_LOGIN_REQUEST:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xe9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->READER_SDK_LOGIN_SUCCESS:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xea

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->READER_SDK_INIT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xeb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PAYMENT_FLOW_QUICK_CASH_CUSTOM_OPTION:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xec

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PAYMENT_FLOW_QUICK_CASH_OPTION:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xed

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PAYMENT_FLOW_RECEIPT_VIEW_SELECT_EMAIL:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xee

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PAYMENT_TUTORIAL_DECLINED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xef

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PREDEFINED_TICKET_GROUP_CREATED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xf0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PREDEFINED_TICKET_CONVERT_TO_CUSTOM:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xf1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PREDEFINED_TICKETS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xf2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_AUTH_SLIP:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xf3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_AUTH_SLIP_PAPER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xf4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_AUTH_SLIP_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xf5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_AUTH_SLIP_COPY:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xf6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_AUTH_SLIP_COPY_PAPER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xf7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_AUTH_SLIP_COPY_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xf8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_BILL_PAPER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xf9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_CASH_REPORT_PAPER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xfa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_ERROR_REPRINT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xfb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_GIFT_RECEIPT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xfc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_RECEIPT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xfd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_RECEIPT_PAPER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xfe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_RECEIPT_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0xff

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_SALES_SUMMARY_PAPER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x100

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_STUB:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x101

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_STUB_PAPER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x102

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_STUB_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x103

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_TICKET:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x104

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_TICKET_PAPER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x105

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_TICKET_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x106

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_TIMECARDS_SUMMARY:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x107

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_JOB_ENQUEUED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x108

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_JOB_FAILED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x109

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_SPOOLER_TARGET_HARDWARE_PRINTER_UNAVAILABLE_ERROR:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x10a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_SPOOLER_NO_HARDWARE_PRINTER_ERROR:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x10b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_SPOOLER_TARGET_DOES_NOT_EXIST_ERROR:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x10c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_VOID_TICKET:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x10d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_VOID_TICKET_PAPER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x10e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINT_VOID_TICKET_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x10f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINTER_AUTONUMBER_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x110

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINTER_AUTONUMBER_TOGGLE_CANCEL:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x111

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINTER_BLOCKED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x112

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINTER_CATEGORY_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x113

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINTER_CONNECT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x114

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINTER_DISCONNECT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x115

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINTER_DISCONNECT_TCP_PING_HISTORY:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x116

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINTER_PRINT_A_TICKET_FOR_EACH_ITEM_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x117

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINTER_RECEIPTS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x118

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINTER_STUBS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x119

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINTER_TICKETS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x11a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRINTER_UNCATEGORIZED_ITEMS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x11b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRODUCT_ACTIVATION:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x11c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PRODUCT_SIGNUP:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x11d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->R12_MEET_THE_READER_MODAL_DISMISSED_BY_USER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x11e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->R12_MULTIPAGE_WALKTHROUGH_DISMISSED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x11f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->RECEIPT_CLOSE_AUTOMATICALLY:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x120

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->REPORTS_EMAIL_SALES_REPORT_SENT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x121

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->REPORTS_TOGGLE_ALL_DAY_SETTING:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x122

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->REPORTS_TOGGLE_ITEM_DETAILS_SETTING:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x123

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->REPRINT_TICKET:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x124

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->RST_T2_DEFAULT_SETTING_OPEN_TICKETS:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x125

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->RST_T2_DEFAULT_SETTING_PREDEFINED_TICKETS:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x126

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->RST_T2_DEFAULT_SETTING_OPEN_TICKETS_AS_HOME:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x127

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->RST_T2_DEFAULT_SETTING_REQUIRE_EMPLOYEE_PASSCODE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x128

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SALES_REPORT_EMAILED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x129

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SALES_REPORT_PRINTED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x12a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SEND_RECEIPT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x12b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SERVICE_EDIT_DISPLAY_PRICE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x12c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SETTLED_TIPS_FROM_BATCH_VIEW:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x12d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SETTLED_TIPS_FROM_TRANSACTION_VIEW:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x12e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SIGN_IN_DECRYPT_FAIL:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x12f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SIGN_IN_SHOWED_EMAIL:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x130

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SKIP_RECEIPT_SCREEN_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x131

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SKIP_SIG_SETTING:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x132

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SKIP_SIG_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x133

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TENDER_CUSTOM_SPLIT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x134

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TENDER_DISMISS_SPLIT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x135

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TENDER_EVEN_SPLIT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x136

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_ADD_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x137

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_CANCEL:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x138

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_EDIT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x139

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_VIEW_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x13a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_MOVE_ITEMS:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x13b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_PRINT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x13c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_PRINT_ALL:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x13d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_SAVE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x13e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_SAVE_ALL:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x13f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_START:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x140

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SUPPORT_CASH_PAYMENT_TUTORIAL_MANUALLY_STARTED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x141

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SUPPORT_PAYMENT_TUTORIAL_MANUALLY_STARTED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x142

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->TAX_DISABLED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x143

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->TAX_ENABLED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x144

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->TAX_RULE_APPLIED_IN_TRANSACTION:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x145

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->TILE_APPEARANCE_CANCELED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x146

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->TILE_APPEARANCE_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x147

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->TIP_SETTINGS_COLLECT_TIPS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x148

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->TOUR_CLOSED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x149

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->VOID_CART_STARTED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x14a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->VOID_CART_CANCELED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x14b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->VOID_CART_VOIDED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x14c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->VOID_ITEMIZATION_STARTED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x14d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->VOID_ITEMIZATION_CANCELED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x14e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->VOID_ITEMIZATION_VOIDED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x14f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->WELCOME_SPLASH_CREATE_ACCOUNT:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x150

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->WELCOME_SPLASH_SIGN_IN:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x151

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->WIFI_CONNECTED:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x152

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->X2_PAYMENT_CLIENT_ERROR:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x153

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->X2_PAYMENT_NETWORK_ERROR:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x154

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->X2_PAYMENT_SERVER_ERROR:Lcom/squareup/analytics/RegisterActionName;

    const/16 v2, 0x155

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/analytics/RegisterActionName;->$VALUES:[Lcom/squareup/analytics/RegisterActionName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 372
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 373
    iput-object p3, p0, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/analytics/RegisterActionName;
    .locals 1

    .line 8
    const-class v0, Lcom/squareup/analytics/RegisterActionName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/analytics/RegisterActionName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/analytics/RegisterActionName;
    .locals 1

    .line 8
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->$VALUES:[Lcom/squareup/analytics/RegisterActionName;

    invoke-virtual {v0}, [Lcom/squareup/analytics/RegisterActionName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/analytics/RegisterActionName;

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .line 377
    iget-object v0, p0, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    return-object v0
.end method
