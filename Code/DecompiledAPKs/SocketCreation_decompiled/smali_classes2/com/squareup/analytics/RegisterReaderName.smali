.class public final enum Lcom/squareup/analytics/RegisterReaderName;
.super Ljava/lang/Enum;
.source "RegisterReaderName.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/analytics/RegisterReaderName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/analytics/RegisterReaderName;

.field public static final enum DEAD_READER:Lcom/squareup/analytics/RegisterReaderName;

.field public static final enum HEADSET_INSERTED:Lcom/squareup/analytics/RegisterReaderName;

.field public static final enum HEADSET_REMOVED:Lcom/squareup/analytics/RegisterReaderName;

.field public static final enum SWIPE_FAILED:Lcom/squareup/analytics/RegisterReaderName;

.field public static final enum SWIPE_SUCCEEDED:Lcom/squareup/analytics/RegisterReaderName;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 9
    new-instance v0, Lcom/squareup/analytics/RegisterReaderName;

    const/4 v1, 0x0

    const-string v2, "DEAD_READER"

    const-string v3, "Dead Reader"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/analytics/RegisterReaderName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterReaderName;->DEAD_READER:Lcom/squareup/analytics/RegisterReaderName;

    .line 10
    new-instance v0, Lcom/squareup/analytics/RegisterReaderName;

    const/4 v2, 0x1

    const-string v3, "HEADSET_INSERTED"

    const-string v4, "Headset Inserted"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/analytics/RegisterReaderName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterReaderName;->HEADSET_INSERTED:Lcom/squareup/analytics/RegisterReaderName;

    .line 11
    new-instance v0, Lcom/squareup/analytics/RegisterReaderName;

    const/4 v3, 0x2

    const-string v4, "HEADSET_REMOVED"

    const-string v5, "Headset Removed"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/analytics/RegisterReaderName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterReaderName;->HEADSET_REMOVED:Lcom/squareup/analytics/RegisterReaderName;

    .line 12
    new-instance v0, Lcom/squareup/analytics/RegisterReaderName;

    const/4 v4, 0x3

    const-string v5, "SWIPE_FAILED"

    const-string v6, "Swipe Failed"

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/analytics/RegisterReaderName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterReaderName;->SWIPE_FAILED:Lcom/squareup/analytics/RegisterReaderName;

    .line 13
    new-instance v0, Lcom/squareup/analytics/RegisterReaderName;

    const/4 v5, 0x4

    const-string v6, "SWIPE_SUCCEEDED"

    const-string v7, "Swipe Succeeded"

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/analytics/RegisterReaderName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterReaderName;->SWIPE_SUCCEEDED:Lcom/squareup/analytics/RegisterReaderName;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/analytics/RegisterReaderName;

    .line 8
    sget-object v6, Lcom/squareup/analytics/RegisterReaderName;->DEAD_READER:Lcom/squareup/analytics/RegisterReaderName;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/analytics/RegisterReaderName;->HEADSET_INSERTED:Lcom/squareup/analytics/RegisterReaderName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterReaderName;->HEADSET_REMOVED:Lcom/squareup/analytics/RegisterReaderName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/analytics/RegisterReaderName;->SWIPE_FAILED:Lcom/squareup/analytics/RegisterReaderName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/analytics/RegisterReaderName;->SWIPE_SUCCEEDED:Lcom/squareup/analytics/RegisterReaderName;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/analytics/RegisterReaderName;->$VALUES:[Lcom/squareup/analytics/RegisterReaderName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 19
    iput-object p3, p0, Lcom/squareup/analytics/RegisterReaderName;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/analytics/RegisterReaderName;
    .locals 1

    .line 8
    const-class v0, Lcom/squareup/analytics/RegisterReaderName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/analytics/RegisterReaderName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/analytics/RegisterReaderName;
    .locals 1

    .line 8
    sget-object v0, Lcom/squareup/analytics/RegisterReaderName;->$VALUES:[Lcom/squareup/analytics/RegisterReaderName;

    invoke-virtual {v0}, [Lcom/squareup/analytics/RegisterReaderName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/analytics/RegisterReaderName;

    return-object v0
.end method
