.class public Lcom/squareup/analytics/RedirectTrackingReceiver;
.super Landroid/content/BroadcastReceiver;
.source "RedirectTrackingReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/analytics/RedirectTrackingReceiver$Component;
    }
.end annotation


# static fields
.field private static final INSTALL_REFERRER_ACTION:Ljava/lang/String; = "com.android.vending.INSTALL_REFERRER"

.field private static final REFERRER_KEY:Ljava/lang/String; = "referrer"

.field public static final UTM_CONTENT:Ljava/lang/String; = "utm_content"

.field public static final UTM_SOURCE:Ljava/lang/String; = "utm_source"


# instance fields
.field analytics:Lcom/squareup/analytics/Analytics;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field encryptedEmailsFromReferrals:Lcom/squareup/analytics/EncryptedEmailsFromReferrals;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field postInstallLoginSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .line 59
    new-instance v0, Lcom/appsflyer/SingleInstallBroadcastReceiver;

    invoke-direct {v0}, Lcom/appsflyer/SingleInstallBroadcastReceiver;-><init>()V

    .line 60
    invoke-virtual {v0, p1, p2}, Lcom/appsflyer/SingleInstallBroadcastReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 63
    new-instance v0, Lio/branch/referral/InstallListener;

    invoke-direct {v0}, Lio/branch/referral/InstallListener;-><init>()V

    .line 64
    invoke-virtual {v0, p1, p2}, Lio/branch/referral/InstallListener;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 66
    invoke-static {}, Lcom/squareup/mortar/AppContextWrapper;->appContext()Landroid/content/Context;

    move-result-object p1

    const-class v0, Lcom/squareup/analytics/RedirectTrackingReceiver$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/analytics/RedirectTrackingReceiver$Component;

    invoke-interface {p1, p0}, Lcom/squareup/analytics/RedirectTrackingReceiver$Component;->inject(Lcom/squareup/analytics/RedirectTrackingReceiver;)V

    const-string p1, "referrer"

    .line 68
    invoke-virtual {p2, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 70
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p2

    const-string v0, "com.android.vending.INSTALL_REFERRER"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_7

    if-eqz p1, :cond_7

    :try_start_0
    const-string p2, "UTF-8"

    .line 72
    invoke-static {p1, p2}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "&"

    .line 73
    invoke-virtual {p1, p2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 74
    new-instance p2, Ljava/util/LinkedHashMap;

    invoke-direct {p2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 75
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    const/4 v3, -0x1

    if-ge v2, v0, :cond_1

    aget-object v4, p1, v2

    const-string v5, "="

    .line 76
    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-ne v5, v3, :cond_0

    goto :goto_1

    .line 81
    :cond_0
    invoke-virtual {v4, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v5, v5, 0x1

    .line 82
    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 83
    invoke-interface {p2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const-string p1, "referral qmap: %s"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/Object;

    aput-object p2, v2, v1

    .line 85
    invoke-static {p1, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string/jumbo p1, "utm_content"

    .line 87
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    if-eqz p1, :cond_7

    const-string/jumbo v2, "utm_source"

    .line 89
    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    .line 90
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v4, -0x37c8874c

    if-eq v2, v4, :cond_3

    const v4, 0x4e12b3eb    # 6.1531616E8f

    if-eq v2, v4, :cond_2

    goto :goto_2

    :cond_2
    const-string v2, "default_login"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_4

    const/4 v3, 0x1

    goto :goto_2

    :cond_3
    const-string v2, "onboard_redirect"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_4

    const/4 v3, 0x0

    :cond_4
    :goto_2
    if-eqz v3, :cond_6

    if-eq v3, v0, :cond_5

    goto :goto_3

    .line 99
    :cond_5
    iget-object p2, p0, Lcom/squareup/analytics/RedirectTrackingReceiver;->encryptedEmailsFromReferrals:Lcom/squareup/analytics/EncryptedEmailsFromReferrals;

    invoke-virtual {p2, p1}, Lcom/squareup/analytics/EncryptedEmailsFromReferrals;->postNewEmail(Ljava/lang/String;)V

    .line 100
    iget-object p2, p0, Lcom/squareup/analytics/RedirectTrackingReceiver;->postInstallLoginSetting:Lcom/squareup/settings/LocalSetting;

    new-instance v0, Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;

    invoke-direct {v0, p1}, Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    goto :goto_3

    :cond_6
    const-string p2, "avt received: %s"

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v1

    .line 93
    invoke-static {p2, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 94
    iget-object p2, p0, Lcom/squareup/analytics/RedirectTrackingReceiver;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {p2, p1}, Lcom/squareup/analytics/Analytics;->setAnonymizedUserToken(Ljava/lang/String;)V

    .line 95
    iget-object p1, p0, Lcom/squareup/analytics/RedirectTrackingReceiver;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object p2, Lcom/squareup/analytics/RegisterActionName;->ONBOARDING_REDIRECT_DOWNLOAD:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception p1

    .line 107
    invoke-static {p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    :cond_7
    :goto_3
    return-void
.end method
