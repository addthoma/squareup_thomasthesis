.class public Lcom/squareup/analytics/AssignmentEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "AssignmentEvent.java"


# instance fields
.field public final bucket_id:I

.field public final bucket_name:Ljava/lang/String;

.field public final experiment_id:I

.field public final experiment_name:Ljava/lang/String;

.field public final identifier:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 2

    .line 15
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->BANDIT_ASSIGNMENT:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v1, "None"

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 16
    iput-object p1, p0, Lcom/squareup/analytics/AssignmentEvent;->identifier:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/squareup/analytics/AssignmentEvent;->experiment_name:Ljava/lang/String;

    .line 18
    iput-object p3, p0, Lcom/squareup/analytics/AssignmentEvent;->bucket_name:Ljava/lang/String;

    .line 19
    iput p4, p0, Lcom/squareup/analytics/AssignmentEvent;->experiment_id:I

    .line 20
    iput p5, p0, Lcom/squareup/analytics/AssignmentEvent;->bucket_id:I

    return-void
.end method
