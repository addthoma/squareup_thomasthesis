.class public final Lcom/squareup/CommonAppModule_ProvidesUUIDGeneratorFactory;
.super Ljava/lang/Object;
.source "CommonAppModule_ProvidesUUIDGeneratorFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/CommonAppModule_ProvidesUUIDGeneratorFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/log/UUIDGenerator;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/CommonAppModule_ProvidesUUIDGeneratorFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/CommonAppModule_ProvidesUUIDGeneratorFactory$InstanceHolder;->access$000()Lcom/squareup/CommonAppModule_ProvidesUUIDGeneratorFactory;

    move-result-object v0

    return-object v0
.end method

.method public static providesUUIDGenerator()Lcom/squareup/log/UUIDGenerator;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/CommonAppModule;->providesUUIDGenerator()Lcom/squareup/log/UUIDGenerator;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/UUIDGenerator;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/log/UUIDGenerator;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/CommonAppModule_ProvidesUUIDGeneratorFactory;->providesUUIDGenerator()Lcom/squareup/log/UUIDGenerator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/CommonAppModule_ProvidesUUIDGeneratorFactory;->get()Lcom/squareup/log/UUIDGenerator;

    move-result-object v0

    return-object v0
.end method
