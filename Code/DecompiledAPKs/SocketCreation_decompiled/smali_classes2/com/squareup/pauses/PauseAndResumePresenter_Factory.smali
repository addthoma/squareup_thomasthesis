.class public final Lcom/squareup/pauses/PauseAndResumePresenter_Factory;
.super Ljava/lang/Object;
.source "PauseAndResumePresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/pauses/PauseAndResumePresenter_Factory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/pauses/PauseAndResumePresenter;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/pauses/PauseAndResumePresenter_Factory;
    .locals 1

    .line 17
    invoke-static {}, Lcom/squareup/pauses/PauseAndResumePresenter_Factory$InstanceHolder;->access$000()Lcom/squareup/pauses/PauseAndResumePresenter_Factory;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance()Lcom/squareup/pauses/PauseAndResumePresenter;
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/pauses/PauseAndResumePresenter;

    invoke-direct {v0}, Lcom/squareup/pauses/PauseAndResumePresenter;-><init>()V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/pauses/PauseAndResumePresenter;
    .locals 1

    .line 13
    invoke-static {}, Lcom/squareup/pauses/PauseAndResumePresenter_Factory;->newInstance()Lcom/squareup/pauses/PauseAndResumePresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/squareup/pauses/PauseAndResumePresenter_Factory;->get()Lcom/squareup/pauses/PauseAndResumePresenter;

    move-result-object v0

    return-object v0
.end method
