.class public final Lcom/squareup/checkoutfe/CheckoutClientDetails;
.super Lcom/squareup/wire/Message;
.source "CheckoutClientDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutfe/CheckoutClientDetails$ProtoAdapter_CheckoutClientDetails;,
        Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;,
        Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/checkoutfe/CheckoutClientDetails;",
        "Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/checkoutfe/CheckoutClientDetails;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final checkout_payer:Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.checkoutfe.CheckoutClientDetails$CheckoutPayer#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/checkoutfe/CheckoutClientDetails$ProtoAdapter_CheckoutClientDetails;

    invoke-direct {v0}, Lcom/squareup/checkoutfe/CheckoutClientDetails$ProtoAdapter_CheckoutClientDetails;-><init>()V

    sput-object v0, Lcom/squareup/checkoutfe/CheckoutClientDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;)V
    .locals 1

    .line 40
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/checkoutfe/CheckoutClientDetails;-><init>(Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;Lokio/ByteString;)V
    .locals 1

    .line 44
    sget-object v0, Lcom/squareup/checkoutfe/CheckoutClientDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 45
    iput-object p1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails;->checkout_payer:Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 59
    :cond_0
    instance-of v1, p1, Lcom/squareup/checkoutfe/CheckoutClientDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 60
    :cond_1
    check-cast p1, Lcom/squareup/checkoutfe/CheckoutClientDetails;

    .line 61
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CheckoutClientDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CheckoutClientDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails;->checkout_payer:Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    iget-object p1, p1, Lcom/squareup/checkoutfe/CheckoutClientDetails;->checkout_payer:Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    .line 62
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 67
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 69
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CheckoutClientDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 70
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails;->checkout_payer:Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 71
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;
    .locals 2

    .line 50
    new-instance v0, Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;-><init>()V

    .line 51
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails;->checkout_payer:Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    iput-object v1, v0, Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;->checkout_payer:Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    .line 52
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CheckoutClientDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CheckoutClientDetails;->newBuilder()Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 79
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails;->checkout_payer:Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    if-eqz v1, :cond_0

    const-string v1, ", checkout_payer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails;->checkout_payer:Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CheckoutClientDetails{"

    .line 80
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
