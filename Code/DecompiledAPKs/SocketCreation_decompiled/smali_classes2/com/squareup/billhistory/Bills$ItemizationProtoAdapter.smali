.class Lcom/squareup/billhistory/Bills$ItemizationProtoAdapter;
.super Ljava/lang/Object;
.source "Bills.java"

# interfaces
.implements Lcom/squareup/billhistory/Bills$BillItemAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistory/Bills;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ItemizationProtoAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/billhistory/Bills$BillItemAdapter<",
        "Lcom/squareup/protos/client/bills/Itemization;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 772
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/billhistory/Bills$1;)V
    .locals 0

    .line 772
    invoke-direct {p0}, Lcom/squareup/billhistory/Bills$ItemizationProtoAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getName(Lcom/squareup/protos/client/bills/Itemization;)Ljava/lang/String;
    .locals 1

    .line 775
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    if-eqz v0, :cond_0

    .line 778
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    iget-object p1, p1, Lcom/squareup/api/items/Item;->name:Ljava/lang/String;

    return-object p1

    .line 779
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;->item:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;

    if-eqz v0, :cond_1

    .line 782
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;->item:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->name:Ljava/lang/String;

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic getName(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 772
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization;

    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/Bills$ItemizationProtoAdapter;->getName(Lcom/squareup/protos/client/bills/Itemization;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getNotes(Lcom/squareup/protos/client/bills/Itemization;)Ljava/lang/String;
    .locals 0

    .line 789
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization;->custom_note:Ljava/lang/String;

    return-object p1
.end method

.method public bridge synthetic getNotes(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 772
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization;

    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/Bills$ItemizationProtoAdapter;->getNotes(Lcom/squareup/protos/client/bills/Itemization;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getQuantity(Lcom/squareup/protos/client/bills/Itemization;)Ljava/math/BigDecimal;
    .locals 1

    .line 794
    new-instance v0, Ljava/math/BigDecimal;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic getQuantity(Ljava/lang/Object;)Ljava/math/BigDecimal;
    .locals 0

    .line 772
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization;

    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/Bills$ItemizationProtoAdapter;->getQuantity(Lcom/squareup/protos/client/bills/Itemization;)Ljava/math/BigDecimal;

    move-result-object p1

    return-object p1
.end method

.method public getQuantityPrecision(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bills/Itemization;)I
    .locals 0

    .line 799
    invoke-static {p1, p2}, Lcom/squareup/quantity/UnitDisplayData;->fromItemization(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/quantity/UnitDisplayData;->getQuantityPrecision()I

    move-result p1

    return p1
.end method

.method public bridge synthetic getQuantityPrecision(Lcom/squareup/util/Res;Ljava/lang/Object;)I
    .locals 0

    .line 772
    check-cast p2, Lcom/squareup/protos/client/bills/Itemization;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/billhistory/Bills$ItemizationProtoAdapter;->getQuantityPrecision(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bills/Itemization;)I

    move-result p1

    return p1
.end method

.method public getUnitAbbreviation(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bills/Itemization;)Ljava/lang/String;
    .locals 0

    .line 809
    invoke-static {p1, p2}, Lcom/squareup/quantity/UnitDisplayData;->fromItemization(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/quantity/UnitDisplayData;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic getUnitAbbreviation(Lcom/squareup/util/Res;Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 772
    check-cast p2, Lcom/squareup/protos/client/bills/Itemization;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/billhistory/Bills$ItemizationProtoAdapter;->getUnitAbbreviation(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bills/Itemization;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public isCustomAmount(Lcom/squareup/protos/client/bills/Itemization;)Z
    .locals 1

    .line 814
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->backing_type:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->CUSTOM_AMOUNT:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic isCustomAmount(Ljava/lang/Object;)Z
    .locals 0

    .line 772
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization;

    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/Bills$ItemizationProtoAdapter;->isCustomAmount(Lcom/squareup/protos/client/bills/Itemization;)Z

    move-result p1

    return p1
.end method

.method public isUnitPriced(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bills/Itemization;)Z
    .locals 0

    .line 804
    invoke-static {p1, p2}, Lcom/squareup/quantity/UnitDisplayData;->fromItemization(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/quantity/UnitDisplayData;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public bridge synthetic isUnitPriced(Lcom/squareup/util/Res;Ljava/lang/Object;)Z
    .locals 0

    .line 772
    check-cast p2, Lcom/squareup/protos/client/bills/Itemization;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/billhistory/Bills$ItemizationProtoAdapter;->isUnitPriced(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bills/Itemization;)Z

    move-result p1

    return p1
.end method
