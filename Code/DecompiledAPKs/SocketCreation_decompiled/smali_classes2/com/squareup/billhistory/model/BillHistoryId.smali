.class public Lcom/squareup/billhistory/model/BillHistoryId;
.super Ljava/lang/Object;
.source "BillHistoryId.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/billhistory/model/BillHistoryId$BillType;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/billhistory/model/BillHistoryId;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final billIdPair:Lcom/squareup/protos/client/IdPair;

.field private final tenderId:Ljava/lang/String;

.field public final type:Lcom/squareup/billhistory/model/BillHistoryId$BillType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 156
    new-instance v0, Lcom/squareup/billhistory/model/BillHistoryId$1;

    invoke-direct {v0}, Lcom/squareup/billhistory/model/BillHistoryId$1;-><init>()V

    sput-object v0, Lcom/squareup/billhistory/model/BillHistoryId;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/billhistory/model/BillHistoryId$BillType;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;)V
    .locals 1

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "type"

    .line 70
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/BillHistoryId$BillType;

    iput-object p1, p0, Lcom/squareup/billhistory/model/BillHistoryId;->type:Lcom/squareup/billhistory/model/BillHistoryId$BillType;

    const-string p1, "billIdPair"

    .line 71
    invoke-static {p2, p1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/IdPair;

    iput-object p1, p0, Lcom/squareup/billhistory/model/BillHistoryId;->billIdPair:Lcom/squareup/protos/client/IdPair;

    .line 72
    iput-object p3, p0, Lcom/squareup/billhistory/model/BillHistoryId;->tenderId:Ljava/lang/String;

    .line 76
    iget-object p1, p2, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    if-nez p1, :cond_1

    iget-object p1, p2, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    const-string p2, "client_id and server_id cannot both be null"

    invoke-static {p1, p2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/billhistory/model/BillHistoryId$BillType;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lcom/squareup/billhistory/model/BillHistoryId$1;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/billhistory/model/BillHistoryId;-><init>(Lcom/squareup/billhistory/model/BillHistoryId$BillType;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;)V

    return-void
.end method

.method public static forBill(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/BillHistoryId;
    .locals 3

    .line 66
    new-instance v0, Lcom/squareup/billhistory/model/BillHistoryId;

    sget-object v1, Lcom/squareup/billhistory/model/BillHistoryId$BillType;->BILL:Lcom/squareup/billhistory/model/BillHistoryId$BillType;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/squareup/billhistory/model/BillHistoryId;-><init>(Lcom/squareup/billhistory/model/BillHistoryId$BillType;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;)V

    return-object v0
.end method

.method public static forBill(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/billhistory/model/BillHistoryId;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    if-nez v0, :cond_0

    .line 33
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Bill;->tender:Ljava/util/List;

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/Tender;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 35
    iget-object v0, p0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    const-string v1, "tender_server_id"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    .line 36
    invoke-static {p0}, Lcom/squareup/billhistory/model/BillHistoryId;->forPayment(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object p0

    return-object p0

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    const-string v1, "bill_server_id"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    .line 40
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-static {p0}, Lcom/squareup/billhistory/model/BillHistoryId;->forBillIdPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object p0

    return-object p0
.end method

.method public static forBillIdPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/BillHistoryId;
    .locals 3

    .line 45
    new-instance v0, Lcom/squareup/billhistory/model/BillHistoryId;

    sget-object v1, Lcom/squareup/billhistory/model/BillHistoryId$BillType;->BILL:Lcom/squareup/billhistory/model/BillHistoryId$BillType;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/squareup/billhistory/model/BillHistoryId;-><init>(Lcom/squareup/billhistory/model/BillHistoryId$BillType;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;)V

    return-object v0
.end method

.method public static forPayment(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/BillHistoryId;
    .locals 3

    .line 62
    new-instance v0, Lcom/squareup/billhistory/model/BillHistoryId;

    sget-object v1, Lcom/squareup/billhistory/model/BillHistoryId$BillType;->PAYMENT:Lcom/squareup/billhistory/model/BillHistoryId$BillType;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/squareup/billhistory/model/BillHistoryId;-><init>(Lcom/squareup/billhistory/model/BillHistoryId$BillType;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;)V

    return-object v0
.end method

.method public static forPayment(Ljava/lang/String;)Lcom/squareup/billhistory/model/BillHistoryId;
    .locals 3

    .line 53
    new-instance v0, Lcom/squareup/billhistory/model/BillHistoryId;

    sget-object v1, Lcom/squareup/billhistory/model/BillHistoryId$BillType;->PAYMENT:Lcom/squareup/billhistory/model/BillHistoryId$BillType;

    new-instance v2, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    invoke-virtual {v2, p0}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object p0

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/squareup/billhistory/model/BillHistoryId;-><init>(Lcom/squareup/billhistory/model/BillHistoryId$BillType;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 135
    :cond_0
    instance-of v1, p1, Lcom/squareup/billhistory/model/BillHistoryId;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    .line 136
    check-cast p1, Lcom/squareup/billhistory/model/BillHistoryId;

    .line 137
    iget-object v1, p0, Lcom/squareup/billhistory/model/BillHistoryId;->type:Lcom/squareup/billhistory/model/BillHistoryId$BillType;

    iget-object v3, p1, Lcom/squareup/billhistory/model/BillHistoryId;->type:Lcom/squareup/billhistory/model/BillHistoryId$BillType;

    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/billhistory/model/BillHistoryId;->billIdPair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/billhistory/model/BillHistoryId;->billIdPair:Lcom/squareup/protos/client/IdPair;

    .line 138
    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/billhistory/model/BillHistoryId;->tenderId:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/billhistory/model/BillHistoryId;->tenderId:Ljava/lang/String;

    .line 139
    invoke-static {v1, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    return v2
.end method

.method public forTender(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/BillHistoryId;
    .locals 3

    .line 91
    new-instance v0, Lcom/squareup/billhistory/model/BillHistoryId;

    iget-object v1, p0, Lcom/squareup/billhistory/model/BillHistoryId;->type:Lcom/squareup/billhistory/model/BillHistoryId$BillType;

    iget-object v2, p0, Lcom/squareup/billhistory/model/BillHistoryId;->billIdPair:Lcom/squareup/protos/client/IdPair;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    iget-object p1, p1, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    :goto_0
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/billhistory/model/BillHistoryId;-><init>(Lcom/squareup/billhistory/model/BillHistoryId$BillType;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;)V

    return-object v0
.end method

.method public forTenderId(Ljava/lang/String;)Lcom/squareup/billhistory/model/BillHistoryId;
    .locals 3

    .line 95
    new-instance v0, Lcom/squareup/billhistory/model/BillHistoryId;

    iget-object v1, p0, Lcom/squareup/billhistory/model/BillHistoryId;->type:Lcom/squareup/billhistory/model/BillHistoryId$BillType;

    iget-object v2, p0, Lcom/squareup/billhistory/model/BillHistoryId;->billIdPair:Lcom/squareup/protos/client/IdPair;

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/billhistory/model/BillHistoryId;-><init>(Lcom/squareup/billhistory/model/BillHistoryId$BillType;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;)V

    return-object v0
.end method

.method public getId()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistoryId;->billIdPair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public getPaymentId()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x1

    .line 105
    invoke-virtual {p0, v0}, Lcom/squareup/billhistory/model/BillHistoryId;->getPaymentId(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPaymentId(Z)Ljava/lang/String;
    .locals 0

    if-eqz p1, :cond_1

    .line 116
    iget-object p1, p0, Lcom/squareup/billhistory/model/BillHistoryId;->billIdPair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/billhistory/model/BillHistoryId;->billIdPair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/billhistory/model/BillHistoryId;->billIdPair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    :goto_0
    return-object p1

    .line 118
    :cond_1
    iget-object p1, p0, Lcom/squareup/billhistory/model/BillHistoryId;->billIdPair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/squareup/billhistory/model/BillHistoryId;->billIdPair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lcom/squareup/billhistory/model/BillHistoryId;->billIdPair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    :goto_1
    return-object p1
.end method

.method public getPaymentIdForReceipt()Ljava/lang/String;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistoryId;->tenderId:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistoryId;->getPaymentId()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getTenderId()Ljava/lang/String;
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistoryId;->tenderId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 145
    iget-object v1, p0, Lcom/squareup/billhistory/model/BillHistoryId;->type:Lcom/squareup/billhistory/model/BillHistoryId$BillType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/billhistory/model/BillHistoryId;->billIdPair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/billhistory/model/BillHistoryId;->tenderId:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BillId{type=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/billhistory/model/BillHistoryId;->type:Lcom/squareup/billhistory/model/BillHistoryId$BillType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", billIdPair=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/billhistory/model/BillHistoryId;->billIdPair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", tenderId=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/billhistory/model/BillHistoryId;->tenderId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public withoutTender()Lcom/squareup/billhistory/model/BillHistoryId;
    .locals 4

    .line 100
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistoryId;->tenderId:Ljava/lang/String;

    if-nez v0, :cond_0

    move-object v0, p0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/billhistory/model/BillHistoryId;

    iget-object v1, p0, Lcom/squareup/billhistory/model/BillHistoryId;->type:Lcom/squareup/billhistory/model/BillHistoryId$BillType;

    iget-object v2, p0, Lcom/squareup/billhistory/model/BillHistoryId;->billIdPair:Lcom/squareup/protos/client/IdPair;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/billhistory/model/BillHistoryId;-><init>(Lcom/squareup/billhistory/model/BillHistoryId$BillType;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 85
    iget-object p2, p0, Lcom/squareup/billhistory/model/BillHistoryId;->type:Lcom/squareup/billhistory/model/BillHistoryId$BillType;

    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistoryId$BillType;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 86
    iget-object p2, p0, Lcom/squareup/billhistory/model/BillHistoryId;->billIdPair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 87
    iget-object p2, p0, Lcom/squareup/billhistory/model/BillHistoryId;->tenderId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
