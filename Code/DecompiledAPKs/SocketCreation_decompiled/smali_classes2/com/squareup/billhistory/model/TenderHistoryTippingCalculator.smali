.class public Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;
.super Ljava/lang/Object;
.source "TenderHistoryTippingCalculator.java"


# instance fields
.field private final paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

.field private final tippingCalculator:Lcom/squareup/tipping/TippingCalculator;


# direct methods
.method public constructor <init>(Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    .line 28
    new-instance p1, Lcom/squareup/tipping/TippingCalculator;

    invoke-virtual {p2}, Lcom/squareup/settings/server/AccountStatusSettings;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/tipping/TippingCalculator;-><init>(Lcom/squareup/settings/server/TipSettings;)V

    iput-object p1, p0, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;->tippingCalculator:Lcom/squareup/tipping/TippingCalculator;

    return-void
.end method

.method private getRemainingBalance(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 66
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory;->getReceiptDisplayDetails()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 68
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->remaining_tender_balance_money:Lcom/squareup/protos/common/Money;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method


# virtual methods
.method public calculateTipOptions(Lcom/squareup/billhistory/model/TenderHistory;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation

    const-string v0, "tender"

    .line 43
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 45
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory;->hasClientCalculatedTipOptions()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory;->getClientCalculatedTipOptions()Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;->tippingCalculator:Lcom/squareup/tipping/TippingCalculator;

    invoke-direct {p0, p1}, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;->getRemainingBalance(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/tipping/TippingCalculator;->withRemainingBalance(Lcom/squareup/protos/common/Money;)Lcom/squareup/tipping/TippingCalculator;

    move-result-object v0

    .line 50
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory;->amountExcludingTip()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory;->hasAutoGratuity()Z

    move-result p1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/tipping/TippingCalculator;->tipOptions(Lcom/squareup/protos/common/Money;Z)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public getCustomTipMaxMoney(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/protos/common/Money;
    .locals 2

    const-string v0, "tender"

    .line 60
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 61
    iget-object v0, p0, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;->tippingCalculator:Lcom/squareup/tipping/TippingCalculator;

    invoke-direct {p0, p1}, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;->getRemainingBalance(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/tipping/TippingCalculator;->withRemainingBalance(Lcom/squareup/protos/common/Money;)Lcom/squareup/tipping/TippingCalculator;

    move-result-object v0

    .line 62
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory;->amountExcludingTip()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/tipping/TippingCalculator;->customTipMaxMoney(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method public shouldDisplayQuickTipOptions(Lcom/squareup/billhistory/model/TenderHistory;)Z
    .locals 1

    .line 74
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory;->hasAutoGratuity()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 79
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory;->hasReceiptDisplayDetails()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory;->shouldDisplayQuickTipOptions()Z

    move-result p1

    return p1

    .line 83
    :cond_1
    iget-object p1, p0, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {p1}, Lcom/squareup/papersignature/PaperSignatureSettings;->isQuickTipEnabled()Z

    move-result p1

    return p1
.end method
