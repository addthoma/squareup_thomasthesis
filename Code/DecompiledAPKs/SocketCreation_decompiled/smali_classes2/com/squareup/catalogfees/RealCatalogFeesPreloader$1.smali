.class Lcom/squareup/catalogfees/RealCatalogFeesPreloader$1;
.super Ljava/lang/Object;
.source "RealCatalogFeesPreloader.java"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/catalogfees/RealCatalogFeesPreloader;->loadAndPost(Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogCallback<",
        "Lcom/squareup/catalogfees/RealCatalogFeesPreloader$Fees;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/catalogfees/RealCatalogFeesPreloader;

.field final synthetic val$callback:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/squareup/catalogfees/RealCatalogFeesPreloader;Ljava/lang/Runnable;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/squareup/catalogfees/RealCatalogFeesPreloader$1;->this$0:Lcom/squareup/catalogfees/RealCatalogFeesPreloader;

    iput-object p2, p0, Lcom/squareup/catalogfees/RealCatalogFeesPreloader$1;->val$callback:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/CatalogResult<",
            "Lcom/squareup/catalogfees/RealCatalogFeesPreloader$Fees;",
            ">;)V"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/catalogfees/RealCatalogFeesPreloader$1;->this$0:Lcom/squareup/catalogfees/RealCatalogFeesPreloader;

    iget-object v1, p0, Lcom/squareup/catalogfees/RealCatalogFeesPreloader$1;->val$callback:Ljava/lang/Runnable;

    invoke-static {v0, p1, v1}, Lcom/squareup/catalogfees/RealCatalogFeesPreloader;->access$000(Lcom/squareup/catalogfees/RealCatalogFeesPreloader;Lcom/squareup/shared/catalog/CatalogResult;Ljava/lang/Runnable;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " => "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/catalogfees/RealCatalogFeesPreloader$1;->val$callback:Ljava/lang/Runnable;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
