.class public final Lcom/squareup/appointmentsapi/NoServicesCustomization;
.super Ljava/lang/Object;
.source "ServicesCustomization.kt"

# interfaces
.implements Lcom/squareup/appointmentsapi/ServicesCustomization;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\u00050\u0004H\u0016J\u000e\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0004H\u0016J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/appointmentsapi/NoServicesCustomization;",
        "Lcom/squareup/appointmentsapi/ServicesCustomization;",
        "()V",
        "allStaffInfo",
        "Lio/reactivex/Observable;",
        "",
        "Lcom/squareup/appointmentsapi/StaffInfo;",
        "businessInfo",
        "Lcom/squareup/appointmentsapi/BusinessInfo;",
        "maybeShowGapTimeEducation",
        "",
        "scope",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "useCardOverSheet",
        "",
        "appointments_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public allStaffInfo()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/appointmentsapi/StaffInfo;",
            ">;>;"
        }
    .end annotation

    .line 55
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.just(emptyList())"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public businessInfo()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/appointmentsapi/BusinessInfo;",
            ">;"
        }
    .end annotation

    .line 51
    new-instance v0, Lcom/squareup/appointmentsapi/BusinessInfo;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/appointmentsapi/BusinessInfo;-><init>(Z)V

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.just(BusinessInfo(false))"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public maybeShowGapTimeEducation(Lcom/squareup/ui/main/RegisterTreeKey;Z)V
    .locals 0

    const-string p2, "scope"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
