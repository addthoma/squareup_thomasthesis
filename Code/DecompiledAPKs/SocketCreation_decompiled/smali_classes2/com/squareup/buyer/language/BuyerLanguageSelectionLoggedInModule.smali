.class public abstract Lcom/squareup/buyer/language/BuyerLanguageSelectionLoggedInModule;
.super Ljava/lang/Object;
.source "BuyerLanguageSelectionLoggedInModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindBuyerLocaleOverride(Lcom/squareup/buyer/language/RealBuyerLocaleOverride;)Lcom/squareup/buyer/language/BuyerLocaleOverride;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
