.class public Lcom/squareup/checkout/OrderModifierList;
.super Ljava/lang/Object;
.source "OrderModifierList.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/squareup/checkout/OrderModifierList;",
        ">;"
    }
.end annotation


# static fields
.field static final SORT_MODIFIERS_BY_ORDINAL_NAME_AND_ID:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
            ">;"
        }
    .end annotation
.end field

.field private static final SORT_OPTIONS_BY_ORDINAL_NAME_AND_ID:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final clientOrdinal:I

.field private final hasModifierSelectionOverrides:Z

.field public final id:Ljava/lang/String;

.field public final isConversational:Z

.field public final itemModifierList:Lcom/squareup/api/items/ItemModifierList;

.field private final maxSelectedModifiers:I

.field private final minSelectedModifiers:I

.field public final modifiers:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;"
        }
    .end annotation
.end field

.field private final multipleSelection:Z

.field public final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    sget-object v0, Lcom/squareup/checkout/-$$Lambda$OrderModifierList$ySXCGNozPfRLuklZVxb_XDLc67E;->INSTANCE:Lcom/squareup/checkout/-$$Lambda$OrderModifierList$ySXCGNozPfRLuklZVxb_XDLc67E;

    sput-object v0, Lcom/squareup/checkout/OrderModifierList;->SORT_MODIFIERS_BY_ORDINAL_NAME_AND_ID:Ljava/util/Comparator;

    .line 39
    sget-object v0, Lcom/squareup/checkout/-$$Lambda$OrderModifierList$3iIHKLmAb_ncYtvHpL7dhT6ld8c;->INSTANCE:Lcom/squareup/checkout/-$$Lambda$OrderModifierList$3iIHKLmAb_ncYtvHpL7dhT6ld8c;

    sput-object v0, Lcom/squareup/checkout/OrderModifierList;->SORT_OPTIONS_BY_ORDINAL_NAME_AND_ID:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/api/items/ItemModifierList;ZLjava/util/SortedMap;ZII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Lcom/squareup/api/items/ItemModifierList;",
            "Z",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;ZII)V"
        }
    .end annotation

    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    iput-object p1, p0, Lcom/squareup/checkout/OrderModifierList;->id:Ljava/lang/String;

    .line 158
    iput-object p2, p0, Lcom/squareup/checkout/OrderModifierList;->name:Ljava/lang/String;

    .line 159
    iput p3, p0, Lcom/squareup/checkout/OrderModifierList;->clientOrdinal:I

    .line 160
    iput-object p4, p0, Lcom/squareup/checkout/OrderModifierList;->itemModifierList:Lcom/squareup/api/items/ItemModifierList;

    .line 161
    iput-boolean p5, p0, Lcom/squareup/checkout/OrderModifierList;->multipleSelection:Z

    .line 162
    iput-object p6, p0, Lcom/squareup/checkout/OrderModifierList;->modifiers:Ljava/util/SortedMap;

    .line 163
    iput-boolean p7, p0, Lcom/squareup/checkout/OrderModifierList;->hasModifierSelectionOverrides:Z

    .line 164
    iput p8, p0, Lcom/squareup/checkout/OrderModifierList;->minSelectedModifiers:I

    .line 165
    iput p9, p0, Lcom/squareup/checkout/OrderModifierList;->maxSelectedModifiers:I

    if-eqz p4, :cond_0

    .line 166
    iget-object p1, p4, Lcom/squareup/api/items/ItemModifierList;->is_conversational:Ljava/lang/Boolean;

    if-eqz p1, :cond_0

    .line 167
    iget-object p1, p4, Lcom/squareup/api/items/ItemModifierList;->is_conversational:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/checkout/OrderModifierList;->isConversational:Z

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 169
    iput-boolean p1, p0, Lcom/squareup/checkout/OrderModifierList;->isConversational:Z

    :goto_0
    return-void
.end method

.method static synthetic lambda$static$0(Lcom/squareup/shared/catalog/models/CatalogItemModifierList;Lcom/squareup/shared/catalog/models/CatalogItemModifierList;)I
    .locals 3

    .line 32
    invoke-static {}, Lcom/squareup/util/ComparisonChain;->start()Lcom/squareup/util/ComparisonChain;

    move-result-object v0

    .line 33
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getOrdinal()I

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getOrdinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/ComparisonChain;->compare(II)Lcom/squareup/util/ComparisonChain;

    move-result-object v0

    .line 34
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/ComparisonChain;->compare(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/squareup/util/ComparisonChain;

    move-result-object v0

    .line 35
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getId()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p0, p1}, Lcom/squareup/util/ComparisonChain;->compare(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/squareup/util/ComparisonChain;

    move-result-object p0

    .line 36
    invoke-virtual {p0}, Lcom/squareup/util/ComparisonChain;->result()I

    move-result p0

    return p0
.end method

.method static synthetic lambda$static$1(Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;)I
    .locals 3

    .line 42
    invoke-static {}, Lcom/squareup/util/ComparisonChain;->start()Lcom/squareup/util/ComparisonChain;

    move-result-object v0

    .line 43
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->getOrdinal()I

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->getOrdinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/ComparisonChain;->compare(II)Lcom/squareup/util/ComparisonChain;

    move-result-object v0

    .line 44
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/ComparisonChain;->compare(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/squareup/util/ComparisonChain;

    move-result-object v0

    .line 45
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->getId()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p0, p1}, Lcom/squareup/util/ComparisonChain;->compare(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/squareup/util/ComparisonChain;

    move-result-object p0

    .line 46
    invoke-virtual {p0}, Lcom/squareup/util/ComparisonChain;->result()I

    move-result p0

    return p0
.end method

.method public static modifierListsFrom(Ljava/util/Map;Ljava/util/Map;)Ljava/util/SortedMap;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;",
            ">;>;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/ItemModifierOverride;",
            ">;)",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifierList;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    if-eqz v0, :cond_7

    .line 89
    invoke-interface/range {p0 .. p0}, Ljava/util/Map;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_6

    .line 93
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    .line 94
    invoke-interface/range {p0 .. p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 96
    sget-object v2, Lcom/squareup/checkout/OrderModifierList;->SORT_MODIFIERS_BY_ORDINAL_NAME_AND_ID:Ljava/util/Comparator;

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 98
    new-instance v2, Ljava/util/TreeMap;

    invoke-direct {v2}, Ljava/util/TreeMap;-><init>()V

    .line 101
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v14, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    .line 103
    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 105
    new-instance v6, Ljava/util/LinkedHashMap;

    invoke-direct {v6}, Ljava/util/LinkedHashMap;-><init>()V

    .line 107
    sget-object v7, Lcom/squareup/api/items/ItemItemModifierListMembership;->DEFAULT_MIN_SELECTED_MODIFIERS:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 108
    sget-object v8, Lcom/squareup/api/items/ItemItemModifierListMembership;->DEFAULT_MAX_SELECTED_MODIFIERS:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 109
    sget-object v9, Lcom/squareup/api/items/ItemItemModifierListMembership;->DEFAULT_HIDDEN_FROM_CUSTOMER:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    .line 112
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getId()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v15, p1

    invoke-interface {v15, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/shared/catalog/models/ItemModifierOverride;

    const/16 v16, 0x1

    if-eqz v10, :cond_4

    .line 114
    iget v7, v10, Lcom/squareup/shared/catalog/models/ItemModifierOverride;->minSelectedModifiers:I

    .line 115
    iget v8, v10, Lcom/squareup/shared/catalog/models/ItemModifierOverride;->maxSelectedModifiers:I

    .line 116
    iget-boolean v9, v10, Lcom/squareup/shared/catalog/models/ItemModifierOverride;->hiddenFromCustomer:Z

    .line 117
    iget-object v11, v10, Lcom/squareup/shared/catalog/models/ItemModifierOverride;->optionOverride:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;

    .line 118
    iget-object v13, v12, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->key:Lcom/squareup/api/sync/ObjectId;

    iget-object v13, v13, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    iget-object v12, v12, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->value:Lcom/squareup/api/items/ItemModifierOptionOverride;

    invoke-interface {v6, v13, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 120
    :cond_1
    sget-object v11, Lcom/squareup/api/items/ItemItemModifierListMembership;->DEFAULT_MIN_SELECTED_MODIFIERS:Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    if-ne v7, v11, :cond_3

    sget-object v11, Lcom/squareup/api/items/ItemItemModifierListMembership;->DEFAULT_MAX_SELECTED_MODIFIERS:Ljava/lang/Integer;

    .line 121
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    if-ne v8, v11, :cond_3

    iget-object v10, v10, Lcom/squareup/shared/catalog/models/ItemModifierOverride;->optionOverride:Ljava/util/List;

    .line 122
    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_2

    goto :goto_2

    :cond_2
    const/4 v10, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v10, 0x1

    :goto_3
    move v12, v7

    move v13, v8

    move v11, v10

    goto :goto_4

    :cond_4
    move v12, v7

    move v13, v8

    const/4 v11, 0x0

    .line 127
    :goto_4
    sget-object v7, Lcom/squareup/checkout/OrderModifierList;->SORT_OPTIONS_BY_ORDINAL_NAME_AND_ID:Ljava/util/Comparator;

    invoke-static {v5, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 130
    new-instance v10, Ljava/util/TreeMap;

    invoke-direct {v10}, Ljava/util/TreeMap;-><init>()V

    const/4 v7, 0x0

    .line 131
    :goto_5
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v8

    if-ge v7, v8, :cond_5

    .line 132
    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;

    .line 134
    invoke-virtual {v8}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/ItemModifierOptionOverride;

    .line 133
    invoke-static {v8, v3, v11, v9}, Lcom/squareup/checkout/OrderModifier;->fromOverride(Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;Lcom/squareup/api/items/ItemModifierOptionOverride;ZZ)Lcom/squareup/checkout/OrderModifier;

    move-result-object v3

    .line 137
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v10, v8, v3}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 141
    :cond_5
    new-instance v3, Lcom/squareup/checkout/OrderModifierList;

    .line 142
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getName()Ljava/lang/String;

    move-result-object v6

    .line 143
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->object()Lcom/squareup/wire/Message;

    move-result-object v7

    move-object v8, v7

    check-cast v8, Lcom/squareup/api/items/ItemModifierList;

    .line 144
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->isMultipleSelection()Z

    move-result v9

    move-object v4, v3

    move v7, v14

    invoke-direct/range {v4 .. v13}, Lcom/squareup/checkout/OrderModifierList;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/api/items/ItemModifierList;ZLjava/util/SortedMap;ZII)V

    .line 146
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4, v3}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_0

    :cond_6
    return-object v2

    .line 90
    :cond_7
    :goto_6
    invoke-static {}, Lcom/squareup/util/SquareCollections;->emptySortedMap()Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public compareTo(Lcom/squareup/checkout/OrderModifierList;)I
    .locals 1

    .line 174
    iget v0, p0, Lcom/squareup/checkout/OrderModifierList;->clientOrdinal:I

    iget p1, p1, Lcom/squareup/checkout/OrderModifierList;->clientOrdinal:I

    sub-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 26
    check-cast p1, Lcom/squareup/checkout/OrderModifierList;

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/OrderModifierList;->compareTo(Lcom/squareup/checkout/OrderModifierList;)I

    move-result p1

    return p1
.end method

.method public getMaxSelectedModifiers()I
    .locals 3

    .line 198
    iget-object v0, p0, Lcom/squareup/checkout/OrderModifierList;->modifiers:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->size()I

    move-result v0

    .line 199
    iget v1, p0, Lcom/squareup/checkout/OrderModifierList;->maxSelectedModifiers:I

    sget-object v2, Lcom/squareup/api/items/ItemItemModifierListMembership;->DEFAULT_MAX_SELECTED_MODIFIERS:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v1, v2, :cond_0

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/squareup/checkout/OrderModifierList;->maxSelectedModifiers:I

    .line 201
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_0
    return v0
.end method

.method public getMinSelectedModifiers()I
    .locals 3

    .line 187
    iget-object v0, p0, Lcom/squareup/checkout/OrderModifierList;->modifiers:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->size()I

    move-result v0

    .line 188
    iget v1, p0, Lcom/squareup/checkout/OrderModifierList;->minSelectedModifiers:I

    sget-object v2, Lcom/squareup/api/items/ItemItemModifierListMembership;->DEFAULT_MIN_SELECTED_MODIFIERS:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/squareup/checkout/OrderModifierList;->minSelectedModifiers:I

    .line 190
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_0
    return v0
.end method

.method public hasMaxRequiredSelection()Z
    .locals 2

    .line 225
    iget v0, p0, Lcom/squareup/checkout/OrderModifierList;->maxSelectedModifiers:I

    sget-object v1, Lcom/squareup/api/items/ItemItemModifierListMembership;->DEFAULT_MAX_SELECTED_MODIFIERS:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasMinRequiredSelection()Z
    .locals 2

    .line 217
    iget v0, p0, Lcom/squareup/checkout/OrderModifierList;->minSelectedModifiers:I

    sget-object v1, Lcom/squareup/api/items/ItemItemModifierListMembership;->DEFAULT_MIN_SELECTED_MODIFIERS:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isMultipleSelection()Z
    .locals 1

    .line 232
    iget-boolean v0, p0, Lcom/squareup/checkout/OrderModifierList;->multipleSelection:Z

    return v0
.end method

.method public modifiers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;"
        }
    .end annotation

    .line 240
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/checkout/OrderModifierList;->modifiers:Ljava/util/SortedMap;

    invoke-interface {v1}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/squareup/checkout/OrderModifierList;->modifiers:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->size()I

    move-result v0

    return v0
.end method

.method public useMinMaxSelection()Z
    .locals 1

    .line 209
    iget-boolean v0, p0, Lcom/squareup/checkout/OrderModifierList;->hasModifierSelectionOverrides:Z

    return v0
.end method
