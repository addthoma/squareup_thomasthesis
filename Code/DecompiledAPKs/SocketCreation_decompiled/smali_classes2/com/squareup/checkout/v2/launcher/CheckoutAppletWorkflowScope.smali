.class public final Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "CheckoutAppletWorkflowScope.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowScope$ParentComponent;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCheckoutAppletWorkflowScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CheckoutAppletWorkflowScope.kt\ncom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,22:1\n63#2:23\n*E\n*S KotlinDebug\n*F\n+ 1 CheckoutAppletWorkflowScope.kt\ncom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowScope\n*L\n17#1:23\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c7\u0002\u0018\u00002\u00020\u0001:\u0001\u0007B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowScope;",
        "Lcom/squareup/ui/main/InMainActivityScope;",
        "()V",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "ParentComponent",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    new-instance v0, Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowScope;

    invoke-direct {v0}, Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowScope;-><init>()V

    sput-object v0, Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowScope;->INSTANCE:Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowScope;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-super {p0, p1}, Lcom/squareup/ui/main/InMainActivityScope;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object v0

    .line 23
    const-class v1, Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowScope$ParentComponent;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->componentInParent(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowScope$ParentComponent;

    .line 18
    invoke-interface {p1}, Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowScope$ParentComponent;->checkoutAppletWorkflowRunner()Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowRunner;

    move-result-object p1

    invoke-interface {p1, v0}, Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    const-string p1, "super.buildScope(parentS\u2026egisterServices\n        )"

    .line 16
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
