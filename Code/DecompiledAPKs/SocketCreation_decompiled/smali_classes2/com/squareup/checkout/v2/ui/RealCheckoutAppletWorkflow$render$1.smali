.class final Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealCheckoutAppletWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;->render(Lkotlin/Unit;Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/checkout/v2/data/transaction/model/CartData;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;",
        "+",
        "Lcom/squareup/checkout/v2/CheckoutAppletWorkflowOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;",
        "Lcom/squareup/checkout/v2/CheckoutAppletWorkflowOutput;",
        "cartData",
        "Lcom/squareup/checkout/v2/data/transaction/model/CartData;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$render$1;->this$0:Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/checkout/v2/data/transaction/model/CartData;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/v2/data/transaction/model/CartData;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;",
            "Lcom/squareup/checkout/v2/CheckoutAppletWorkflowOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "cartData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$render$1;->this$0:Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;

    invoke-static {v0, p1}, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;->access$handleCartData(Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;Lcom/squareup/checkout/v2/data/transaction/model/CartData;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/checkout/v2/data/transaction/model/CartData;

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$render$1;->invoke(Lcom/squareup/checkout/v2/data/transaction/model/CartData;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
