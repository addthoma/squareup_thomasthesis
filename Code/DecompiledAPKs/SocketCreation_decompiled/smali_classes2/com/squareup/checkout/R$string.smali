.class public final Lcom/squareup/checkout/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkout/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final add_customer:I = 0x7f12007e

.field public static final auto_gratuity_disclosure:I = 0x7f120109

.field public static final auto_gratuity_name_customer_facing:I = 0x7f12010a

.field public static final auto_gratuity_name_merchant_facing:I = 0x7f12010b

.field public static final buyer_add_a_tip:I = 0x7f1201d8

.field public static final buyer_amount_tip_action_bar:I = 0x7f1201da

.field public static final buyer_approved:I = 0x7f1201db

.field public static final buyer_authorizing:I = 0x7f1201de

.field public static final buyer_payment_note_tip:I = 0x7f120214

.field public static final buyer_printed_receipt_entry_method_contactless:I = 0x7f12021d

.field public static final buyer_printed_receipt_entry_method_emv:I = 0x7f12021e

.field public static final buyer_printed_receipt_entry_method_keyed:I = 0x7f12021f

.field public static final buyer_printed_receipt_entry_method_on_file:I = 0x7f120220

.field public static final buyer_printed_receipt_entry_method_swipe:I = 0x7f120221

.field public static final buyer_printed_receipt_tender_card:I = 0x7f120235

.field public static final buyer_printed_receipt_tender_card_detail_all:I = 0x7f120236

.field public static final buyer_printed_receipt_tender_card_detail_no_entry_method:I = 0x7f120237

.field public static final buyer_printed_receipt_tender_card_detail_no_suffix:I = 0x7f120238

.field public static final buyer_printed_receipt_tender_cash:I = 0x7f120239

.field public static final buyer_printed_receipt_tender_other:I = 0x7f12023c

.field public static final buyer_remaining_payment_due:I = 0x7f12025a

.field public static final buyer_send_receipt_subtitle:I = 0x7f120264

.field public static final buyer_send_receipt_title:I = 0x7f120266

.field public static final buyer_send_receipt_title_cash_change_only:I = 0x7f120268

.field public static final buyer_send_receipt_title_no_change:I = 0x7f12026a

.field public static final buyer_send_receipt_title_no_change_sub:I = 0x7f12026b

.field public static final buyer_send_receipt_title_no_change_sub_with_remaining_balance:I = 0x7f12026c

.field public static final buyer_tip_additional_tip:I = 0x7f120272

.field public static final buyer_tip_custom:I = 0x7f120273

.field public static final buyer_tip_no_additional_tip:I = 0x7f120274

.field public static final buyer_tip_no_tip:I = 0x7f120275

.field public static final cart_comps:I = 0x7f120348

.field public static final charge:I = 0x7f1203f6

.field public static final comped_with_reason:I = 0x7f12046e

.field public static final configure_item_summary_quantity_only:I = 0x7f12047b

.field public static final conversational_mode_add:I = 0x7f1204d2

.field public static final conversational_mode_allergy:I = 0x7f1204d3

.field public static final conversational_mode_extra:I = 0x7f1204d4

.field public static final conversational_mode_no:I = 0x7f1204d5

.field public static final conversational_mode_side:I = 0x7f1204d6

.field public static final conversational_mode_sub:I = 0x7f1204d7

.field public static final conversational_mode_unknown:I = 0x7f1204d8

.field public static final coupon_discount_description_format_amount:I = 0x7f12059d

.field public static final coupon_discount_description_format_percentage:I = 0x7f12059e

.field public static final coupon_discount_description_format_percentage_and_amount:I = 0x7f12059f

.field public static final coupon_free_item:I = 0x7f1205a0

.field public static final coupon_name_cart_scope_format_amount:I = 0x7f1205a1

.field public static final coupon_name_cart_scope_format_percentage:I = 0x7f1205a2

.field public static final coupon_name_cart_scope_format_percentage_and_amount:I = 0x7f1205a3

.field public static final coupon_name_item_scope_format_amount:I = 0x7f1205a4

.field public static final coupon_name_item_scope_format_percentage:I = 0x7f1205a5

.field public static final coupon_name_item_scope_format_percentage_and_amount:I = 0x7f1205a6

.field public static final custom_surcharge_amount:I = 0x7f120798

.field public static final custom_surcharge_percentage:I = 0x7f120799

.field public static final emv_reader_disconnected_msg:I = 0x7f120a5c

.field public static final emv_reader_disconnected_title:I = 0x7f120a5d

.field public static final invoice_automatic_payment_fee_message:I = 0x7f120c7e

.field public static final invoice_automatic_payment_fee_message_v2:I = 0x7f120c7f

.field public static final invoice_automatic_payment_fee_message_with_rate:I = 0x7f120c80

.field public static final invoice_automatic_payment_fee_message_with_rate_v2:I = 0x7f120c81

.field public static final item_variation_default_name:I = 0x7f120e41

.field public static final item_variation_default_name_regular:I = 0x7f120e42

.field public static final kitchen_printing_cardholder_name_name_order:I = 0x7f120e86

.field public static final kitchen_printing_cardholder_name_retrieving:I = 0x7f120e87

.field public static final kitchen_printing_cardholder_name_unavailable:I = 0x7f120e8a

.field public static final modifier_no_modes:I = 0x7f121009

.field public static final modifier_one_mode:I = 0x7f12100a

.field public static final modifier_three_modes:I = 0x7f12101c

.field public static final modifier_two_modes:I = 0x7f12101d

.field public static final new_sale:I = 0x7f12106b

.field public static final online_only_reason:I = 0x7f121163

.field public static final pay_card_card_on_file_hint:I = 0x7f121398

.field public static final pay_card_card_on_file_hint_with_rate:I = 0x7f121399

.field public static final pay_card_card_on_file_hint_with_rate_higher:I = 0x7f12139a

.field public static final pay_card_cnp_hint:I = 0x7f12139d

.field public static final pay_card_cnp_hint_with_rate:I = 0x7f12139e

.field public static final pay_card_cnp_hint_with_rate_higher:I = 0x7f12139f

.field public static final pay_card_cnp_url:I = 0x7f1213a0

.field public static final seating_seat_format:I = 0x7f121793

.field public static final seating_table_shared:I = 0x7f121794

.field public static final service_charge_disclosure:I = 0x7f1217c9

.field public static final service_charge_name:I = 0x7f1217ca

.field public static final square_support:I = 0x7f1218a4

.field public static final support_center:I = 0x7f1218d9

.field public static final void_with_reason:I = 0x7f121bbc


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
