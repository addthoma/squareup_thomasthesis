.class public final Lcom/squareup/checkout/OrderDestination;
.super Ljava/lang/Object;
.source "OrderDestination.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkout/OrderDestination$Config;,
        Lcom/squareup/checkout/OrderDestination$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderDestination.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderDestination.kt\ncom/squareup/checkout/OrderDestination\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,74:1\n1412#2,9:75\n1642#2,2:84\n1421#2:86\n*E\n*S KotlinDebug\n*F\n+ 1 OrderDestination.kt\ncom/squareup/checkout/OrderDestination\n*L\n30#1,9:75\n30#1,2:84\n30#1:86\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u00122\u00020\u0001:\u0002\u0012\u0013B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0010\u001a\u00020\u0011R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058F\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/checkout/OrderDestination;",
        "",
        "destination",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;",
        "seats",
        "",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
        "(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;Ljava/util/List;)V",
        "config",
        "Lcom/squareup/checkout/OrderDestination$Config;",
        "getConfig",
        "()Lcom/squareup/checkout/OrderDestination$Config;",
        "getSeats",
        "()Ljava/util/List;",
        "description",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "Companion",
        "Config",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/checkout/OrderDestination$Companion;


# instance fields
.field private final config:Lcom/squareup/checkout/OrderDestination$Config;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/checkout/OrderDestination$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkout/OrderDestination$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkout/OrderDestination;->Companion:Lcom/squareup/checkout/OrderDestination$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;)V"
        }
    .end annotation

    const-string v0, "destination"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "seats"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Lcom/squareup/checkout/OrderDestination$seatProvider$1;

    invoke-direct {v0, p2}, Lcom/squareup/checkout/OrderDestination$seatProvider$1;-><init>(Ljava/util/List;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 27
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->type:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;

    if-nez p2, :cond_0

    goto :goto_1

    :cond_0
    sget-object v1, Lcom/squareup/checkout/OrderDestination$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;->ordinal()I

    move-result p2

    aget p2, v1, p2

    const/4 v1, 0x1

    if-eq p2, v1, :cond_5

    const/4 v1, 0x2

    if-eq p2, v1, :cond_4

    const/4 v1, 0x3

    if-ne p2, v1, :cond_3

    .line 30
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->seat_id_pair:Ljava/util/List;

    const-string p2, "destination.seat_id_pair"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 75
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    check-cast p2, Ljava/util/Collection;

    .line 84
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 83
    check-cast v1, Lcom/squareup/protos/client/IdPair;

    .line 30
    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    const-string v2, "it.client_id"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    if-eqz v1, :cond_1

    .line 83
    invoke-interface {p2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 86
    :cond_2
    check-cast p2, Ljava/util/List;

    .line 30
    new-instance p1, Lcom/squareup/checkout/OrderDestination$Config$Seats;

    invoke-direct {p1, p2}, Lcom/squareup/checkout/OrderDestination$Config$Seats;-><init>(Ljava/util/List;)V

    check-cast p1, Lcom/squareup/checkout/OrderDestination$Config;

    goto :goto_2

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 29
    :cond_4
    sget-object p1, Lcom/squareup/checkout/OrderDestination$Config$Shared;->INSTANCE:Lcom/squareup/checkout/OrderDestination$Config$Shared;

    check-cast p1, Lcom/squareup/checkout/OrderDestination$Config;

    goto :goto_2

    .line 28
    :cond_5
    :goto_1
    sget-object p1, Lcom/squareup/checkout/OrderDestination$Config$None;->INSTANCE:Lcom/squareup/checkout/OrderDestination$Config$None;

    check-cast p1, Lcom/squareup/checkout/OrderDestination$Config;

    .line 27
    :goto_2
    iput-object p1, p0, Lcom/squareup/checkout/OrderDestination;->config:Lcom/squareup/checkout/OrderDestination$Config;

    return-void
.end method

.method public static final fromFeatureDetails(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;Ljava/util/List;)Lcom/squareup/checkout/OrderDestination;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;)",
            "Lcom/squareup/checkout/OrderDestination;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/checkout/OrderDestination;->Companion:Lcom/squareup/checkout/OrderDestination$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/checkout/OrderDestination$Companion;->fromFeatureDetails(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;Ljava/util/List;)Lcom/squareup/checkout/OrderDestination;

    move-result-object p0

    return-object p0
.end method

.method public static final seatsFromFeatureDetails(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/checkout/OrderDestination;->Companion:Lcom/squareup/checkout/OrderDestination$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/checkout/OrderDestination$Companion;->seatsFromFeatureDetails(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final description(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 10

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/checkout/OrderDestination;->config:Lcom/squareup/checkout/OrderDestination$Config;

    .line 47
    instance-of v1, v0, Lcom/squareup/checkout/OrderDestination$Config$None;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    goto :goto_0

    .line 48
    :cond_0
    instance-of v1, v0, Lcom/squareup/checkout/OrderDestination$Config$Shared;

    if-eqz v1, :cond_1

    sget v0, Lcom/squareup/checkout/R$string;->seating_table_shared:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 49
    :cond_1
    instance-of v1, v0, Lcom/squareup/checkout/OrderDestination$Config$Seats;

    if-eqz v1, :cond_3

    check-cast v0, Lcom/squareup/checkout/OrderDestination$Config$Seats;

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderDestination$Config$Seats;->getSeats()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 50
    iget-object v0, p0, Lcom/squareup/checkout/OrderDestination;->config:Lcom/squareup/checkout/OrderDestination$Config;

    check-cast v0, Lcom/squareup/checkout/OrderDestination$Config$Seats;

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderDestination$Config$Seats;->getSeats()Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/Iterable;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v0, Lcom/squareup/checkout/OrderDestination$description$1;

    invoke-direct {v0, p1}, Lcom/squareup/checkout/OrderDestination$description$1;-><init>(Lcom/squareup/util/Res;)V

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/16 v8, 0x1f

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :cond_2
    :goto_0
    return-object v2

    .line 49
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final getConfig()Lcom/squareup/checkout/OrderDestination$Config;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/checkout/OrderDestination;->config:Lcom/squareup/checkout/OrderDestination$Config;

    return-object v0
.end method

.method public final getSeats()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/squareup/checkout/OrderDestination;->config:Lcom/squareup/checkout/OrderDestination$Config;

    .line 42
    sget-object v1, Lcom/squareup/checkout/OrderDestination$Config$None;->INSTANCE:Lcom/squareup/checkout/OrderDestination$Config$None;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/checkout/OrderDestination$Config$Shared;->INSTANCE:Lcom/squareup/checkout/OrderDestination$Config$Shared;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_1

    .line 43
    :cond_1
    instance-of v0, v0, Lcom/squareup/checkout/OrderDestination$Config$Seats;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/checkout/OrderDestination;->config:Lcom/squareup/checkout/OrderDestination$Config;

    check-cast v0, Lcom/squareup/checkout/OrderDestination$Config$Seats;

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderDestination$Config$Seats;->getSeats()Ljava/util/List;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_2
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method
