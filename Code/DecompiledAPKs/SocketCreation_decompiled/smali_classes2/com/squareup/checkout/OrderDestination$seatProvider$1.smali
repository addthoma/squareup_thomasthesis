.class final Lcom/squareup/checkout/OrderDestination$seatProvider$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderDestination.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkout/OrderDestination;-><init>(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/String;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderDestination.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderDestination.kt\ncom/squareup/checkout/OrderDestination$seatProvider$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,74:1\n250#2,2:75\n*E\n*S KotlinDebug\n*F\n+ 1 OrderDestination.kt\ncom/squareup/checkout/OrderDestination$seatProvider$1\n*L\n25#1,2:75\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
        "clientId",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $seats:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkout/OrderDestination$seatProvider$1;->$seats:Ljava/util/List;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;
    .locals 3

    const-string v0, "clientId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iget-object v0, p0, Lcom/squareup/checkout/OrderDestination$seatProvider$1;->$seats:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 75
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    .line 25
    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->seat_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, v2, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 76
    :goto_0
    check-cast v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    return-object v1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/OrderDestination$seatProvider$1;->invoke(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    move-result-object p1

    return-object p1
.end method
