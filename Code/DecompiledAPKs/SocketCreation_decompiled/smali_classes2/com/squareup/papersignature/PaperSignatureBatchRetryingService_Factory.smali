.class public final Lcom/squareup/papersignature/PaperSignatureBatchRetryingService_Factory;
.super Ljava/lang/Object;
.source "PaperSignatureBatchRetryingService_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;",
        ">;"
    }
.end annotation


# instance fields
.field private final paperSignatureBatchServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/papersignature/PaperSignatureBatchService;",
            ">;"
        }
    .end annotation
.end field

.field private final retryExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/papersignature/PaperSignatureBatchService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService_Factory;->paperSignatureBatchServiceProvider:Ljavax/inject/Provider;

    .line 22
    iput-object p2, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService_Factory;->retryExecutorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/papersignature/PaperSignatureBatchRetryingService_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/papersignature/PaperSignatureBatchService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)",
            "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService_Factory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/server/papersignature/PaperSignatureBatchService;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;
    .locals 1

    .line 38
    new-instance v0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

    invoke-direct {v0, p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;-><init>(Lcom/squareup/server/papersignature/PaperSignatureBatchService;Lcom/squareup/thread/executor/MainThread;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;
    .locals 2

    .line 27
    iget-object v0, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService_Factory;->paperSignatureBatchServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/papersignature/PaperSignatureBatchService;

    iget-object v1, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService_Factory;->retryExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/thread/executor/MainThread;

    invoke-static {v0, v1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService_Factory;->newInstance(Lcom/squareup/server/papersignature/PaperSignatureBatchService;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService_Factory;->get()Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

    move-result-object v0

    return-object v0
.end method
