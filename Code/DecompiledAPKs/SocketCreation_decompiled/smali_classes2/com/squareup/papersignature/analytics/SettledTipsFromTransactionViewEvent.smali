.class public Lcom/squareup/papersignature/analytics/SettledTipsFromTransactionViewEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "SettledTipsFromTransactionViewEvent.java"


# instance fields
.field public final quick_tip_setting:Ljava/lang/String;

.field public final searched_for_transaction:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->SETTLED_TIPS_FROM_TRANSACTION_VIEW:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 15
    iput-object p1, p0, Lcom/squareup/papersignature/analytics/SettledTipsFromTransactionViewEvent;->quick_tip_setting:Ljava/lang/String;

    .line 16
    iput-boolean p2, p0, Lcom/squareup/papersignature/analytics/SettledTipsFromTransactionViewEvent;->searched_for_transaction:Z

    return-void
.end method
