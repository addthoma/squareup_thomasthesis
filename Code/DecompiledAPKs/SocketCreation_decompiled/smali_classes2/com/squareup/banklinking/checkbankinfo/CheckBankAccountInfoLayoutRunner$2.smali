.class final Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$2;
.super Ljava/lang/Object;
.source "CheckBankAccountInfoLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;-><init>(Landroid/view/View;Lcom/squareup/register/widgets/GlassSpinner;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "kotlin.jvm.PlatformType",
        "checkedId",
        "",
        "<anonymous parameter 2>",
        "onCheckedChanged"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$2;->this$0:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Lcom/squareup/noho/NohoCheckableGroup;II)V
    .locals 0

    .line 91
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$2;->this$0:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;

    .line 92
    sget p3, Lcom/squareup/banklinking/impl/R$id;->checking:I

    if-ne p2, p3, :cond_0

    sget-object p2, Lcom/squareup/protos/client/bankaccount/BankAccountType;->CHECKING:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    goto :goto_0

    .line 93
    :cond_0
    sget p3, Lcom/squareup/banklinking/impl/R$id;->business_checking:I

    if-ne p2, p3, :cond_1

    sget-object p2, Lcom/squareup/protos/client/bankaccount/BankAccountType;->BUSINESS_CHECKING:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    goto :goto_0

    .line 94
    :cond_1
    sget p3, Lcom/squareup/banklinking/impl/R$id;->savings:I

    if-ne p2, p3, :cond_2

    sget-object p2, Lcom/squareup/protos/client/bankaccount/BankAccountType;->SAVINGS:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    goto :goto_0

    :cond_2
    const/4 p2, 0x0

    .line 91
    :goto_0
    invoke-static {p1, p2}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->access$setBankAccountType$p(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;Lcom/squareup/protos/client/bankaccount/BankAccountType;)V

    return-void
.end method
