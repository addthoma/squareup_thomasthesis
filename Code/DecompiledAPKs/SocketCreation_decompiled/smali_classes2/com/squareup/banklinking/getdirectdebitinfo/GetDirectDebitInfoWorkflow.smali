.class public final Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "GetDirectDebitInfoWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoProps;",
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState;",
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nGetDirectDebitInfoWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 GetDirectDebitInfoWorkflow.kt\ncom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 6 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,121:1\n32#2,12:122\n85#3:134\n240#4:135\n276#5:136\n149#6,5:137\n149#6,5:142\n149#6,5:147\n*E\n*S KotlinDebug\n*F\n+ 1 GetDirectDebitInfoWorkflow.kt\ncom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow\n*L\n41#1,12:122\n50#1:134\n50#1:135\n50#1:136\n58#1,5:137\n83#1,5:142\n91#1,5:147\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u001a\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u00022\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016J&\u0010\u0011\u001a\u00020\u00122\u001c\u0010\u0013\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0015j\u0002`\u00160\u0014H\u0002J \u0010\u0017\u001a\u0012\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0015j\u0002`\u00162\u0006\u0010\u0018\u001a\u00020\u0019H\u0002JN\u0010\u001a\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u000e\u001a\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u00032\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001dH\u0016J\u0010\u0010\u001e\u001a\u00020\u00102\u0006\u0010\u001b\u001a\u00020\u0003H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoProps;",
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState;",
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "bankAccountSettings",
        "Lcom/squareup/banklinking/BankAccountSettings;",
        "(Lcom/squareup/banklinking/BankAccountSettings;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "onCancel",
        "",
        "sink",
        "Lcom/squareup/workflow/Sink;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/banklinking/getdirectdebitinfo/Action;",
        "onDirectDebitInfo",
        "directDebitInfo",
        "Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/banklinking/BankAccountSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bankAccountSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    return-void
.end method

.method public static final synthetic access$onCancel(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;Lcom/squareup/workflow/Sink;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;->onCancel(Lcom/squareup/workflow/Sink;)V

    return-void
.end method

.method public static final synthetic access$onDirectDebitInfo(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;->onDirectDebitInfo(Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final onCancel(Lcom/squareup/workflow/Sink;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState;",
            "+",
            "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoOutput;",
            ">;>;)V"
        }
    .end annotation

    .line 118
    sget-object v0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$onCancel$1;->INSTANCE:Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$onCancel$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p0, v1, v0, v2, v1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method

.method private final onDirectDebitInfo(Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState;",
            "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoOutput;",
            ">;"
        }
    .end annotation

    .line 99
    instance-of v0, p1, Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo$DirectDebitInfoSuccess;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$onDirectDebitInfo$1;

    invoke-direct {v0, p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$onDirectDebitInfo$1;-><init>(Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, v0, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 109
    :cond_0
    instance-of v0, p1, Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo$DirectDebitInfoFailure;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$onDirectDebitInfo$2;

    invoke-direct {v0, p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$onDirectDebitInfo$2;-><init>(Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, v0, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 122
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 127
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 129
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 130
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 131
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 133
    :cond_3
    check-cast v1, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 41
    :cond_4
    sget-object p1, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState$Loading;->INSTANCE:Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState$Loading;

    move-object v1, p1

    check-cast v1, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;->initialState(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoProps;

    check-cast p2, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;->render(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoProps;Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoProps;Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoProps;",
            "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState;",
            "-",
            "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v7, p3

    const-string v2, "props"

    move-object/from16 v3, p1

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "state"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "context"

    invoke-static {v7, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    sget-object v2, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState$Loading;->INSTANCE:Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState$Loading;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    const-string v8, ""

    if-eqz v2, :cond_0

    .line 50
    iget-object v1, v0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoProps;->getSortCode()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/banklinking/BankAccountSettings;->getDirectDebitInfo(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v1

    .line 134
    sget-object v2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v2, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$render$$inlined$asWorker$1;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 135
    invoke-static {v2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v1

    .line 136
    const-class v2, Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    new-instance v3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v3, v2, v1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v2, v3

    check-cast v2, Lcom/squareup/workflow/Worker;

    const/4 v3, 0x0

    .line 51
    new-instance v1, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$render$1;

    invoke-direct {v1, v0}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$render$1;-><init>(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;)V

    move-object v4, v1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object/from16 v1, p3

    .line 49
    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 53
    new-instance v1, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;

    .line 54
    new-instance v2, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Loading;

    .line 55
    new-instance v3, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$render$2;

    invoke-direct {v3, v0, v7}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$render$2;-><init>(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 54
    invoke-direct {v2, v3}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Loading;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;

    .line 53
    invoke-direct {v1, v2}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;-><init>(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 138
    new-instance v2, Lcom/squareup/workflow/legacy/Screen;

    .line 139
    const-class v3, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v8}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 140
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 138
    invoke-direct {v2, v3, v1, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 59
    sget-object v1, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {v2, v1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_0

    .line 61
    :cond_0
    instance-of v2, v1, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState$Success;

    if-eqz v2, :cond_1

    new-instance v2, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;

    .line 62
    new-instance v4, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Success;

    .line 63
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoProps;->getHolderName()Ljava/lang/String;

    move-result-object v10

    .line 64
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoProps;->getAccountNumber()Ljava/lang/String;

    move-result-object v11

    .line 65
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoProps;->getSortCode()Ljava/lang/String;

    move-result-object v12

    .line 66
    move-object v3, v1

    check-cast v3, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState$Success;

    invoke-virtual {v3}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState$Success;->getName()Ljava/lang/String;

    move-result-object v13

    .line 67
    invoke-virtual {v3}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState$Success;->getEmail()Ljava/lang/String;

    move-result-object v14

    .line 68
    invoke-virtual {v3}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState$Success;->getOrganization()Ljava/lang/String;

    move-result-object v15

    .line 69
    invoke-virtual {v3}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState$Success;->getAddress()Ljava/lang/String;

    move-result-object v16

    .line 70
    invoke-virtual {v3}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState$Success;->getReference()Ljava/lang/String;

    move-result-object v17

    .line 71
    new-instance v3, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$render$3;

    invoke-direct {v3, v0, v7, v1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$render$3;-><init>(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState;)V

    move-object/from16 v18, v3

    check-cast v18, Lkotlin/jvm/functions/Function0;

    .line 80
    new-instance v1, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$render$4;

    invoke-direct {v1, v0, v7}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$render$4;-><init>(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;Lcom/squareup/workflow/RenderContext;)V

    move-object/from16 v19, v1

    check-cast v19, Lkotlin/jvm/functions/Function0;

    move-object v9, v4

    .line 62
    invoke-direct/range {v9 .. v19}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Success;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v4, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;

    .line 61
    invoke-direct {v2, v4}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;-><init>(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;)V

    check-cast v2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 143
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 144
    const-class v3, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v8}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 145
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 143
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 83
    sget-object v2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    goto :goto_0

    .line 84
    :cond_1
    instance-of v2, v1, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState$Failure;

    if-eqz v2, :cond_2

    new-instance v2, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;

    .line 85
    new-instance v3, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Failure;

    .line 86
    check-cast v1, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState$Failure;

    invoke-virtual {v1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState$Failure;->getTitle()Ljava/lang/String;

    move-result-object v4

    .line 87
    invoke-virtual {v1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState$Failure;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 88
    new-instance v5, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$render$5;

    invoke-direct {v5, v0, v7}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow$render$5;-><init>(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 85
    invoke-direct {v3, v4, v1, v5}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Failure;-><init>(Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    check-cast v3, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;

    .line 84
    invoke-direct {v2, v3}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;-><init>(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;)V

    check-cast v2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 148
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 149
    const-class v3, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v8}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 150
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 148
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 91
    sget-object v2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_2
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public snapshotState(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState;

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoWorkflow;->snapshotState(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
