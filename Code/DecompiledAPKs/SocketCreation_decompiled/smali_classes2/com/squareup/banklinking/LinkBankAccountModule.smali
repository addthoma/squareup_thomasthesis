.class public abstract Lcom/squareup/banklinking/LinkBankAccountModule;
.super Ljava/lang/Object;
.source "LinkBankAccountModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH!J\u0010\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u000c\u001a\u00020\rH!J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H!J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0010\u001a\u00020\u0014H!J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0010\u001a\u00020\u0017H!J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH!J\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u0010\u001a\u00020\u001eH!J\u0010\u0010\u001f\u001a\u00020 2\u0006\u0010\u0010\u001a\u00020!H!\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/banklinking/LinkBankAccountModule;",
        "",
        "()V",
        "bindAdditionalMainActivityScopeServices",
        "Lmortar/Scoped;",
        "bankLinkingStarter",
        "Lcom/squareup/banklinking/BankLinkingStarter;",
        "bindBankAccountSettings",
        "Lcom/squareup/banklinking/BankAccountSettings;",
        "realBankAccountSettings",
        "Lcom/squareup/banklinking/RealBankAccountSettings;",
        "bindBankLinkingStarter",
        "realBankLinkingStarter",
        "Lcom/squareup/banklinking/RealBankLinkingStarter;",
        "bindCheckBankAccountInfoWorkflow",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoWorkflow;",
        "workflow",
        "Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;",
        "bindCheckPasswordWorkflow",
        "Lcom/squareup/banklinking/checkpassword/CheckPasswordWorkflow;",
        "Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;",
        "bindFetchBankAccountWorkflow",
        "Lcom/squareup/banklinking/fetchbank/FetchBankAccountWorkflow;",
        "Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow;",
        "bindLinkBankAccountViewFactory",
        "Lcom/squareup/banklinking/LinkBankAccountViewFactory;",
        "viewFactory",
        "Lcom/squareup/banklinking/RealLinkBankAccountViewFactory;",
        "bindLinkBankAccountWorkflow",
        "Lcom/squareup/banklinking/LinkBankAccountWorkflow;",
        "Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;",
        "bindShowResultWorkflow",
        "Lcom/squareup/banklinking/showresult/ShowResultWorkflow;",
        "Lcom/squareup/banklinking/showresult/RealShowResultWorkflow;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindAdditionalMainActivityScopeServices(Lcom/squareup/banklinking/BankLinkingStarter;)Lmortar/Scoped;
    .annotation runtime Lcom/squareup/ForMainActivity;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method public abstract bindBankAccountSettings(Lcom/squareup/banklinking/RealBankAccountSettings;)Lcom/squareup/banklinking/BankAccountSettings;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindBankLinkingStarter(Lcom/squareup/banklinking/RealBankLinkingStarter;)Lcom/squareup/banklinking/BankLinkingStarter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindCheckBankAccountInfoWorkflow(Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;)Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindCheckPasswordWorkflow(Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;)Lcom/squareup/banklinking/checkpassword/CheckPasswordWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindFetchBankAccountWorkflow(Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow;)Lcom/squareup/banklinking/fetchbank/FetchBankAccountWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindLinkBankAccountViewFactory(Lcom/squareup/banklinking/RealLinkBankAccountViewFactory;)Lcom/squareup/banklinking/LinkBankAccountViewFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindLinkBankAccountWorkflow(Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;)Lcom/squareup/banklinking/LinkBankAccountWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindShowResultWorkflow(Lcom/squareup/banklinking/showresult/RealShowResultWorkflow;)Lcom/squareup/banklinking/showresult/ShowResultWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
