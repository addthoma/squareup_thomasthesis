.class public final Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;
.super Lcom/squareup/banklinking/LinkBankAccountState;
.source "LinkBankAccountState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/banklinking/LinkBankAccountState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ShowingResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u000c\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u001f\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u00c6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\u0013\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\u0019\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u000fH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;",
        "Lcom/squareup/banklinking/LinkBankAccountState;",
        "linkBankAccountState",
        "Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;",
        "verificationState",
        "Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;",
        "(Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V",
        "getLinkBankAccountState",
        "()Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;",
        "getVerificationState",
        "()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;",
        "component1",
        "component2",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

.field private final verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult$Creator;

    invoke-direct {v0}, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult$Creator;-><init>()V

    sput-object v0, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V
    .locals 1

    const-string v0, "linkBankAccountState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, v0}, Lcom/squareup/banklinking/LinkBankAccountState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;->linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    iput-object p2, p0, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;->verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;ILjava/lang/Object;)Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;->linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;->verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;->copy(Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;->linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;->verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    return-object v0
.end method

.method public final copy(Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;
    .locals 1

    const-string v0, "linkBankAccountState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;

    invoke-direct {v0, p1, p2}, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;-><init>(Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;

    iget-object v0, p0, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;->linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    iget-object v1, p1, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;->linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;->verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    iget-object p1, p1, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;->verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getLinkBankAccountState()Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;->linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    return-object v0
.end method

.method public final getVerificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;->verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;->linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;->verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ShowingResult(linkBankAccountState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;->linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", verificationState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;->verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;->linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;->verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    :goto_0
    return-void
.end method
