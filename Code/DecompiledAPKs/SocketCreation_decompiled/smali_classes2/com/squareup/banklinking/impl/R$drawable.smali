.class public final Lcom/squareup/banklinking/impl/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/banklinking/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final bank_logo_ally:I = 0x7f08008f

.field public static final bank_logo_bankofamerica:I = 0x7f080090

.field public static final bank_logo_bankofthewest:I = 0x7f080091

.field public static final bank_logo_bnymellon:I = 0x7f080092

.field public static final bank_logo_branchbankingandtrust:I = 0x7f080093

.field public static final bank_logo_capitalone:I = 0x7f080094

.field public static final bank_logo_chase:I = 0x7f080095

.field public static final bank_logo_citibank:I = 0x7f080096

.field public static final bank_logo_citizensfinancialbank:I = 0x7f080097

.field public static final bank_logo_compass:I = 0x7f080098

.field public static final bank_logo_etrade:I = 0x7f080099

.field public static final bank_logo_fifththird:I = 0x7f08009a

.field public static final bank_logo_harris:I = 0x7f08009b

.field public static final bank_logo_hsbc:I = 0x7f08009c

.field public static final bank_logo_huntington:I = 0x7f08009d

.field public static final bank_logo_ing:I = 0x7f08009e

.field public static final bank_logo_key:I = 0x7f08009f

.field public static final bank_logo_manufacturersandtraderstrust:I = 0x7f0800a0

.field public static final bank_logo_northerntrust:I = 0x7f0800a1

.field public static final bank_logo_pnc:I = 0x7f0800a2

.field public static final bank_logo_rbc:I = 0x7f0800a3

.field public static final bank_logo_rbscitizens:I = 0x7f0800a4

.field public static final bank_logo_regions:I = 0x7f0800a5

.field public static final bank_logo_sovereign:I = 0x7f0800a6

.field public static final bank_logo_statestreetbankandtrust:I = 0x7f0800a7

.field public static final bank_logo_suntrust:I = 0x7f0800a8

.field public static final bank_logo_td:I = 0x7f0800a9

.field public static final bank_logo_union:I = 0x7f0800aa

.field public static final bank_logo_us:I = 0x7f0800ab

.field public static final bank_logo_usaa:I = 0x7f0800ac

.field public static final bank_logo_wellsfargo:I = 0x7f0800ad

.field public static final direct_debit_logo:I = 0x7f080128


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
