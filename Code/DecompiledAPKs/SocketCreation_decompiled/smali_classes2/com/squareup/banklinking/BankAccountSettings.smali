.class public interface abstract Lcom/squareup/banklinking/BankAccountSettings;
.super Ljava/lang/Object;
.source "BankAccountSettings.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;,
        Lcom/squareup/banklinking/BankAccountSettings$State;,
        Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;,
        Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;,
        Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;,
        Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;,
        Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;,
        Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;,
        Lcom/squareup/banklinking/BankAccountSettings$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\u0008f\u0018\u00002\u00020\u0001:\u0008\u0019\u001a\u001b\u001c\u001d\u001e\u001f J\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\'J\u0016\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0006\u001a\u00020\u0007H\'J\u0016\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\t\u001a\u00020\u0007H\'J\u0016\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u00032\u0006\u0010\u000c\u001a\u00020\u0007H\'J*\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\n\u0008\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0007H\'J\u000e\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\'J\u000e\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\'J\u0008\u0010\u0015\u001a\u00020\u0016H&J\u000e\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0018H\'\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/banklinking/BankAccountSettings;",
        "Lmortar/Scoped;",
        "cancelVerification",
        "Lio/reactivex/Single;",
        "Lcom/squareup/banklinking/BankAccountSettings$State;",
        "checkPassword",
        "password",
        "",
        "confirmBankAccount",
        "confirmationToken",
        "getDirectDebitInfo",
        "Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;",
        "sortCode",
        "linkBankAccount",
        "bankAccountDetails",
        "Lcom/squareup/protos/client/bankaccount/BankAccountDetails;",
        "isOnboarding",
        "",
        "idempotenceKey",
        "refresh",
        "resendVerificationEmail",
        "resetRequestState",
        "",
        "state",
        "Lio/reactivex/Observable;",
        "CancelVerificationState",
        "CheckPasswordState",
        "ConfirmBankAccountState",
        "DirectDebitInfo",
        "LatestBankAccountState",
        "LinkBankAccountState",
        "ResendEmailState",
        "State",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract cancelVerification()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation
.end method

.method public abstract checkPassword(Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation
.end method

.method public abstract confirmBankAccount(Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getDirectDebitInfo(Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;",
            ">;"
        }
    .end annotation
.end method

.method public abstract linkBankAccount(Lcom/squareup/protos/client/bankaccount/BankAccountDetails;ZLjava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bankaccount/BankAccountDetails;",
            "Z",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation
.end method

.method public abstract refresh()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation
.end method

.method public abstract resendVerificationEmail()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation
.end method

.method public abstract resetRequestState()V
.end method

.method public abstract state()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation
.end method
