.class public final Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealCheckPasswordWorkflow.kt"

# interfaces
.implements Lcom/squareup/banklinking/checkpassword/CheckPasswordWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action;,
        Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/banklinking/checkpassword/CheckPasswordState;",
        "Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/banklinking/checkpassword/CheckPasswordWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealCheckPasswordWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealCheckPasswordWorkflow.kt\ncom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 6 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,141:1\n32#2,12:142\n85#3:154\n240#4:155\n276#5:156\n149#6,5:157\n149#6,5:162\n*E\n*S KotlinDebug\n*F\n+ 1 RealCheckPasswordWorkflow.kt\ncom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow\n*L\n73#1,12:142\n82#1:154\n82#1:155\n82#1:156\n110#1,5:157\n120#1,5:162\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 %2\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002:\u0002$%B\u0017\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u001f\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u00032\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016\u00a2\u0006\u0002\u0010\u0014J\u001c\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J*\u0010\u0019\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t2\u0006\u0010\u0017\u001a\u00020\u00042\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001bH\u0002JS\u0010\u001d\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0017\u001a\u00020\u00042\u0012\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001fH\u0016\u00a2\u0006\u0002\u0010 J\u0010\u0010!\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020\u0004H\u0016J*\u0010\"\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t2\u0006\u0010\u0017\u001a\u00020#2\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001bH\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;",
        "Lcom/squareup/banklinking/checkpassword/CheckPasswordWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "",
        "Lcom/squareup/banklinking/checkpassword/CheckPasswordState;",
        "Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "bankAccountSettings",
        "Lcom/squareup/banklinking/BankAccountSettings;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/analytics/Analytics;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/banklinking/checkpassword/CheckPasswordState;",
        "onBankSettingsState",
        "Lcom/squareup/workflow/WorkflowAction;",
        "state",
        "Lcom/squareup/banklinking/BankAccountSettings$State;",
        "passwordCheckScreen",
        "sink",
        "Lcom/squareup/workflow/Sink;",
        "Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action;",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/banklinking/checkpassword/CheckPasswordState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;",
        "snapshotState",
        "warningDialogScreen",
        "Lcom/squareup/banklinking/checkpassword/CheckPasswordState$InvalidPassword;",
        "Action",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Companion;

.field public static final EDIT_BANK_ACCOUNT_PASSWORD:Ljava/lang/String; = "Settings Bank Account: Edit Bank Account Password"

.field public static final EDIT_BANK_ACCOUNT_PASSWORD_CANCEL:Ljava/lang/String; = "Settings Bank Account: Edit Bank Account Password Cancel"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;->Companion:Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bankAccountSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    iput-object p2, p0, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$onBankSettingsState(Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;Lcom/squareup/banklinking/BankAccountSettings$State;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;->onBankSettingsState(Lcom/squareup/banklinking/BankAccountSettings$State;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final onBankSettingsState(Lcom/squareup/banklinking/BankAccountSettings$State;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/banklinking/checkpassword/CheckPasswordState;",
            "Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput;",
            ">;"
        }
    .end annotation

    .line 125
    iget-object v0, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->checkPasswordState:Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;

    sget-object v1, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 128
    iget-object p1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 129
    :cond_0
    new-instance v0, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$FailInPasswordCheck;

    new-instance v1, Lcom/squareup/widgets/warning/WarningStrings;

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/widgets/warning/Warning;

    invoke-direct {v0, v1}, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$FailInPasswordCheck;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 131
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected checkPasswordState: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->checkPasswordState:Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 126
    :cond_2
    sget-object p1, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$SucceedInPasswordCheck;->INSTANCE:Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$SucceedInPasswordCheck;

    move-object v0, p1

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object v0
.end method

.method private final passwordCheckScreen(Lcom/squareup/banklinking/checkpassword/CheckPasswordState;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/banklinking/checkpassword/CheckPasswordState;",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 100
    new-instance v0, Lcom/squareup/banklinking/checkpassword/CheckPasswordScreen;

    .line 101
    instance-of p1, p1, Lcom/squareup/banklinking/checkpassword/CheckPasswordState$CheckingPassword;

    .line 102
    new-instance v1, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$passwordCheckScreen$1;

    invoke-direct {v1, p0, p2}, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$passwordCheckScreen$1;-><init>(Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;Lcom/squareup/workflow/Sink;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 106
    new-instance v2, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$passwordCheckScreen$2;

    invoke-direct {v2, p0, p2}, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$passwordCheckScreen$2;-><init>(Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;Lcom/squareup/workflow/Sink;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 100
    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/banklinking/checkpassword/CheckPasswordScreen;-><init>(ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 158
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 159
    const-class p2, Lcom/squareup/banklinking/checkpassword/CheckPasswordScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 160
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 158
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method private final warningDialogScreen(Lcom/squareup/banklinking/checkpassword/CheckPasswordState$InvalidPassword;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/banklinking/checkpassword/CheckPasswordState$InvalidPassword;",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 117
    new-instance v0, Lcom/squareup/banklinking/WarningDialogScreen;

    .line 118
    invoke-virtual {p1}, Lcom/squareup/banklinking/checkpassword/CheckPasswordState$InvalidPassword;->getWarning()Lcom/squareup/widgets/warning/Warning;

    move-result-object p1

    .line 119
    new-instance v1, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$warningDialogScreen$1;

    invoke-direct {v1, p2}, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$warningDialogScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 117
    invoke-direct {v0, p1, v1}, Lcom/squareup/banklinking/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 163
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 164
    const-class p2, Lcom/squareup/banklinking/WarningDialogScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 165
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 163
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method


# virtual methods
.method public initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/banklinking/checkpassword/CheckPasswordState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 142
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 147
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 149
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 150
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 151
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 153
    :cond_3
    check-cast v1, Lcom/squareup/banklinking/checkpassword/CheckPasswordState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 73
    :cond_4
    sget-object p1, Lcom/squareup/banklinking/checkpassword/CheckPasswordState$PrepareToCheckPassword;->INSTANCE:Lcom/squareup/banklinking/checkpassword/CheckPasswordState$PrepareToCheckPassword;

    move-object v1, p1

    check-cast v1, Lcom/squareup/banklinking/checkpassword/CheckPasswordState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 40
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;->initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/banklinking/checkpassword/CheckPasswordState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 40
    check-cast p1, Lkotlin/Unit;

    check-cast p2, Lcom/squareup/banklinking/checkpassword/CheckPasswordState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;->render(Lkotlin/Unit;Lcom/squareup/banklinking/checkpassword/CheckPasswordState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/banklinking/checkpassword/CheckPasswordState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/banklinking/checkpassword/CheckPasswordState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/banklinking/checkpassword/CheckPasswordState;",
            "-",
            "Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    sget-object p1, Lcom/squareup/banklinking/checkpassword/CheckPasswordState$PrepareToCheckPassword;->INSTANCE:Lcom/squareup/banklinking/checkpassword/CheckPasswordState$PrepareToCheckPassword;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p1

    invoke-direct {p0, p2, p1}, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;->passwordCheckScreen(Lcom/squareup/banklinking/checkpassword/CheckPasswordState;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 81
    :cond_0
    instance-of p1, p2, Lcom/squareup/banklinking/checkpassword/CheckPasswordState$CheckingPassword;

    if-eqz p1, :cond_1

    .line 82
    iget-object p1, p0, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    move-object v0, p2

    check-cast v0, Lcom/squareup/banklinking/checkpassword/CheckPasswordState$CheckingPassword;

    invoke-virtual {v0}, Lcom/squareup/banklinking/checkpassword/CheckPasswordState$CheckingPassword;->getPassword()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/banklinking/BankAccountSettings;->checkPassword(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 154
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$render$$inlined$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 155
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 156
    const-class v0, Lcom/squareup/banklinking/BankAccountSettings$State;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/Worker;

    const/4 v4, 0x0

    .line 82
    new-instance p1, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$render$1;

    invoke-direct {p1, p0}, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$render$1;-><init>(Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, p3

    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 86
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p1

    invoke-direct {p0, p2, p1}, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;->passwordCheckScreen(Lcom/squareup/banklinking/checkpassword/CheckPasswordState;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 88
    :cond_1
    instance-of p1, p2, Lcom/squareup/banklinking/checkpassword/CheckPasswordState$InvalidPassword;

    if-eqz p1, :cond_2

    sget-object p1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/Pair;

    const/4 v1, 0x0

    .line 89
    sget-object v2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v3

    invoke-direct {p0, p2, v3}, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;->passwordCheckScreen(Lcom/squareup/banklinking/checkpassword/CheckPasswordState;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 90
    sget-object v2, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    check-cast p2, Lcom/squareup/banklinking/checkpassword/CheckPasswordState$InvalidPassword;

    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p3

    invoke-direct {p0, p2, p3}, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;->warningDialogScreen(Lcom/squareup/banklinking/checkpassword/CheckPasswordState$InvalidPassword;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p2

    invoke-static {v2, p2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p2

    aput-object p2, v0, v1

    .line 88
    invoke-virtual {p1, v0}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/banklinking/checkpassword/CheckPasswordState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 40
    check-cast p1, Lcom/squareup/banklinking/checkpassword/CheckPasswordState;

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;->snapshotState(Lcom/squareup/banklinking/checkpassword/CheckPasswordState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
