.class public final Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealLinkBankAccountWorkflow.kt"

# interfaces
.implements Lcom/squareup/banklinking/LinkBankAccountWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/banklinking/LinkBankAccountProps;",
        "Lcom/squareup/banklinking/LinkBankAccountState;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/banklinking/LinkBankAccountWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealLinkBankAccountWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealLinkBankAccountWorkflow.kt\ncom/squareup/banklinking/RealLinkBankAccountWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n*L\n1#1,191:1\n32#2,12:192\n*E\n*S KotlinDebug\n*F\n+ 1 RealLinkBankAccountWorkflow.kt\ncom/squareup/banklinking/RealLinkBankAccountWorkflow\n*L\n105#1,12:192\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u00012\u00020\n:\u0001)B/\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\u0002\u0010\u0015J$\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u001c\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00172\u0006\u0010\u001a\u001a\u00020\u001dH\u0002J\u001c\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00172\u0006\u0010\u001a\u001a\u00020\u001fH\u0002J\u001a\u0010 \u001a\u00020\u00032\u0006\u0010!\u001a\u00020\u00022\u0008\u0010\"\u001a\u0004\u0018\u00010#H\u0016JN\u0010$\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010!\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00032\u0012\u0010%\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040&H\u0016J\u0014\u0010\'\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0017H\u0002J\u0010\u0010(\u001a\u00020#2\u0006\u0010\u0018\u001a\u00020\u0003H\u0016R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/banklinking/LinkBankAccountProps;",
        "Lcom/squareup/banklinking/LinkBankAccountState;",
        "",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/banklinking/LinkBankAccountWorkflow;",
        "fetchBankAccountWorkflow",
        "Lcom/squareup/banklinking/fetchbank/FetchBankAccountWorkflow;",
        "checkPasswordWorkflow",
        "Lcom/squareup/banklinking/checkpassword/CheckPasswordWorkflow;",
        "checkBankAccountInfoWorkflow",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoWorkflow;",
        "showResultWorkflow",
        "Lcom/squareup/banklinking/showresult/ShowResultWorkflow;",
        "bankAccountSettings",
        "Lcom/squareup/banklinking/BankAccountSettings;",
        "(Lcom/squareup/banklinking/fetchbank/FetchBankAccountWorkflow;Lcom/squareup/banklinking/checkpassword/CheckPasswordWorkflow;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoWorkflow;Lcom/squareup/banklinking/showresult/ShowResultWorkflow;Lcom/squareup/banklinking/BankAccountSettings;)V",
        "handleCheckPasswordOutput",
        "Lcom/squareup/workflow/WorkflowAction;",
        "state",
        "Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;",
        "output",
        "Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput;",
        "handleFetchBankAccountOutput",
        "Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput;",
        "handleLinkBankAccountOutput",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "resetAndFinish",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

.field private final checkBankAccountInfoWorkflow:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoWorkflow;

.field private final checkPasswordWorkflow:Lcom/squareup/banklinking/checkpassword/CheckPasswordWorkflow;

.field private final fetchBankAccountWorkflow:Lcom/squareup/banklinking/fetchbank/FetchBankAccountWorkflow;

.field private final showResultWorkflow:Lcom/squareup/banklinking/showresult/ShowResultWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/banklinking/fetchbank/FetchBankAccountWorkflow;Lcom/squareup/banklinking/checkpassword/CheckPasswordWorkflow;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoWorkflow;Lcom/squareup/banklinking/showresult/ShowResultWorkflow;Lcom/squareup/banklinking/BankAccountSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "fetchBankAccountWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkPasswordWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkBankAccountInfoWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "showResultWorkflow"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bankAccountSettings"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;->fetchBankAccountWorkflow:Lcom/squareup/banklinking/fetchbank/FetchBankAccountWorkflow;

    iput-object p2, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;->checkPasswordWorkflow:Lcom/squareup/banklinking/checkpassword/CheckPasswordWorkflow;

    iput-object p3, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;->checkBankAccountInfoWorkflow:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoWorkflow;

    iput-object p4, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;->showResultWorkflow:Lcom/squareup/banklinking/showresult/ShowResultWorkflow;

    iput-object p5, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    return-void
.end method

.method public static final synthetic access$handleCheckPasswordOutput(Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 58
    invoke-direct {p0, p1, p2}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;->handleCheckPasswordOutput(Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleFetchBankAccountOutput(Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 58
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;->handleFetchBankAccountOutput(Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleLinkBankAccountOutput(Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 58
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;->handleLinkBankAccountOutput(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$resetAndFinish(Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 58
    invoke-direct {p0}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;->resetAndFinish()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final handleCheckPasswordOutput(Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;",
            "Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/banklinking/LinkBankAccountState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 171
    sget-object v0, Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput$CheckPasswordSucceeded;->INSTANCE:Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput$CheckPasswordSucceeded;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p2, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$LinkBank;

    .line 172
    invoke-virtual {p1}, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->getRequiresPassword()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->getPrimaryInstitutionNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;->getVerificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object p1

    .line 171
    invoke-direct {p2, v0, v1, p1}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$LinkBank;-><init>(ZLjava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V

    check-cast p2, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 174
    :cond_0
    sget-object p1, Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput$CheckPasswordCanceled;->INSTANCE:Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput$CheckPasswordCanceled;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;->resetAndFinish()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p2

    :goto_0
    return-object p2

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final handleFetchBankAccountOutput(Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/banklinking/LinkBankAccountState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 155
    instance-of v0, p1, Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput$FetchBankAccountSucceeded;

    if-eqz v0, :cond_1

    .line 156
    check-cast p1, Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput$FetchBankAccountSucceeded;

    invoke-virtual {p1}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput$FetchBankAccountSucceeded;->getRequiresPassword()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    new-instance v0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$CheckPassword;

    .line 158
    invoke-virtual {p1}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput$FetchBankAccountSucceeded;->getRequiresPassword()Z

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput$FetchBankAccountSucceeded;->getPrimaryInstitutionNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput$FetchBankAccountSucceeded;->getVerificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object p1

    .line 157
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$CheckPassword;-><init>(ZLjava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V

    check-cast v0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action;

    goto :goto_0

    .line 161
    :cond_0
    new-instance v0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$LinkBank;

    invoke-virtual {p1}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput$FetchBankAccountSucceeded;->getRequiresPassword()Z

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput$FetchBankAccountSucceeded;->getPrimaryInstitutionNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput$FetchBankAccountSucceeded;->getVerificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object p1

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$LinkBank;-><init>(ZLjava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V

    check-cast v0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action;

    .line 156
    :goto_0
    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_1

    .line 164
    :cond_1
    sget-object v0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput$FetchBankAccountCanceled;->INSTANCE:Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput$FetchBankAccountCanceled;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;->resetAndFinish()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final handleLinkBankAccountOutput(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/banklinking/LinkBankAccountState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 180
    instance-of v0, p1, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput$CheckBankAccountInfoCompleted;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$ShowResult;

    .line 181
    check-cast p1, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput$CheckBankAccountInfoCompleted;

    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput$CheckBankAccountInfoCompleted;->getLinkBankAccountState()Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput$CheckBankAccountInfoCompleted;->getVerificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object p1

    .line 180
    invoke-direct {v0, v1, p1}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$ShowResult;-><init>(Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 183
    :cond_0
    sget-object v0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput$CheckBankAccountInfoCanceled;->INSTANCE:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput$CheckBankAccountInfoCanceled;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;->resetAndFinish()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final resetAndFinish()Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/banklinking/LinkBankAccountState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 187
    iget-object v0, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    invoke-interface {v0}, Lcom/squareup/banklinking/BankAccountSettings;->resetRequestState()V

    .line 188
    sget-object v0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$Finish;->INSTANCE:Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$Finish;

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method


# virtual methods
.method public initialState(Lcom/squareup/banklinking/LinkBankAccountProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/banklinking/LinkBankAccountState;
    .locals 3

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 192
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v2

    :goto_1
    if-eqz p2, :cond_3

    .line 197
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v2, "Parcel.obtain()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 199
    array-length v2, p2

    invoke-virtual {v0, p2, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 200
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 201
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p2, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 202
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 203
    :cond_3
    check-cast v2, Lcom/squareup/banklinking/LinkBankAccountState;

    if-eqz v2, :cond_4

    goto :goto_2

    .line 106
    :cond_4
    sget-object p2, Lcom/squareup/banklinking/LinkBankAccountProps$StartFromFetchingBankAccount;->INSTANCE:Lcom/squareup/banklinking/LinkBankAccountProps$StartFromFetchingBankAccount;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_5

    sget-object p1, Lcom/squareup/banklinking/LinkBankAccountState$FetchingBankAccount;->INSTANCE:Lcom/squareup/banklinking/LinkBankAccountState$FetchingBankAccount;

    move-object v2, p1

    check-cast v2, Lcom/squareup/banklinking/LinkBankAccountState;

    goto :goto_2

    .line 107
    :cond_5
    instance-of p2, p1, Lcom/squareup/banklinking/LinkBankAccountProps$SkipFetchingBankAccount;

    if-eqz p2, :cond_7

    check-cast p1, Lcom/squareup/banklinking/LinkBankAccountProps$SkipFetchingBankAccount;

    invoke-virtual {p1}, Lcom/squareup/banklinking/LinkBankAccountProps$SkipFetchingBankAccount;->getRequiresPassword()Z

    move-result p2

    if-eqz p2, :cond_6

    .line 108
    new-instance p2, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;

    .line 109
    invoke-virtual {p1}, Lcom/squareup/banklinking/LinkBankAccountProps$SkipFetchingBankAccount;->getRequiresPassword()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/banklinking/LinkBankAccountProps$SkipFetchingBankAccount;->getPrimaryInstitutionNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/banklinking/LinkBankAccountProps$SkipFetchingBankAccount;->getVerificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object p1

    .line 108
    invoke-direct {p2, v0, v1, p1}, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;-><init>(ZLjava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V

    move-object v2, p2

    check-cast v2, Lcom/squareup/banklinking/LinkBankAccountState;

    goto :goto_2

    .line 112
    :cond_6
    new-instance p2, Lcom/squareup/banklinking/LinkBankAccountState$CheckingBankAccountInfo;

    .line 113
    invoke-virtual {p1}, Lcom/squareup/banklinking/LinkBankAccountProps$SkipFetchingBankAccount;->getRequiresPassword()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/banklinking/LinkBankAccountProps$SkipFetchingBankAccount;->getPrimaryInstitutionNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/banklinking/LinkBankAccountProps$SkipFetchingBankAccount;->getVerificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object p1

    .line 112
    invoke-direct {p2, v0, v1, p1}, Lcom/squareup/banklinking/LinkBankAccountState$CheckingBankAccountInfo;-><init>(ZLjava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V

    move-object v2, p2

    check-cast v2, Lcom/squareup/banklinking/LinkBankAccountState;

    :goto_2
    return-object v2

    .line 107
    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 58
    check-cast p1, Lcom/squareup/banklinking/LinkBankAccountProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;->initialState(Lcom/squareup/banklinking/LinkBankAccountProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/banklinking/LinkBankAccountState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 58
    check-cast p1, Lcom/squareup/banklinking/LinkBankAccountProps;

    check-cast p2, Lcom/squareup/banklinking/LinkBankAccountState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;->render(Lcom/squareup/banklinking/LinkBankAccountProps;Lcom/squareup/banklinking/LinkBankAccountState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/banklinking/LinkBankAccountProps;Lcom/squareup/banklinking/LinkBankAccountState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/banklinking/LinkBankAccountProps;",
            "Lcom/squareup/banklinking/LinkBankAccountState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/banklinking/LinkBankAccountState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    sget-object p1, Lcom/squareup/banklinking/LinkBankAccountState$FetchingBankAccount;->INSTANCE:Lcom/squareup/banklinking/LinkBankAccountState$FetchingBankAccount;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;->fetchBankAccountWorkflow:Lcom/squareup/banklinking/fetchbank/FetchBankAccountWorkflow;

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    const/4 v2, 0x0

    new-instance p1, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$render$1;

    invoke-direct {p1, p0}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$render$1;-><init>(Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;)V

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContextKt;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto/16 :goto_0

    .line 127
    :cond_0
    instance-of p1, p2, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;->checkPasswordWorkflow:Lcom/squareup/banklinking/checkpassword/CheckPasswordWorkflow;

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    const/4 v2, 0x0

    new-instance p1, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$render$2;

    invoke-direct {p1, p0, p2}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$render$2;-><init>(Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;Lcom/squareup/banklinking/LinkBankAccountState;)V

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContextKt;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto :goto_0

    .line 131
    :cond_1
    instance-of p1, p2, Lcom/squareup/banklinking/LinkBankAccountState$CheckingBankAccountInfo;

    if-eqz p1, :cond_2

    .line 132
    new-instance v2, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;

    .line 133
    check-cast p2, Lcom/squareup/banklinking/LinkBankAccountState$CheckingBankAccountInfo;

    invoke-virtual {p2}, Lcom/squareup/banklinking/LinkBankAccountState$CheckingBankAccountInfo;->getRequiresPassword()Z

    move-result p1

    invoke-virtual {p2}, Lcom/squareup/banklinking/LinkBankAccountState$CheckingBankAccountInfo;->getPrimaryInstitutionNumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/banklinking/LinkBankAccountState$CheckingBankAccountInfo;->getVerificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object p2

    .line 132
    invoke-direct {v2, p1, v0, p2}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;-><init>(ZLjava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V

    .line 136
    iget-object p1, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;->checkBankAccountInfoWorkflow:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoWorkflow;

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    const/4 v3, 0x0

    new-instance p1, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$render$3;

    invoke-direct {p1, p0}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$render$3;-><init>(Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;)V

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto :goto_0

    .line 141
    :cond_2
    instance-of p1, p2, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;

    if-eqz p1, :cond_3

    .line 142
    new-instance v2, Lcom/squareup/banklinking/showresult/ShowResultProps;

    check-cast p2, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;

    invoke-virtual {p2}, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;->getLinkBankAccountState()Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;->getVerificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object p2

    invoke-direct {v2, p1, p2}, Lcom/squareup/banklinking/showresult/ShowResultProps;-><init>(Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V

    .line 144
    iget-object p1, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;->showResultWorkflow:Lcom/squareup/banklinking/showresult/ShowResultWorkflow;

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    const/4 v3, 0x0

    new-instance p1, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$render$4;

    invoke-direct {p1, p0}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$render$4;-><init>(Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;)V

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/banklinking/LinkBankAccountState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 58
    check-cast p1, Lcom/squareup/banklinking/LinkBankAccountState;

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;->snapshotState(Lcom/squareup/banklinking/LinkBankAccountState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
