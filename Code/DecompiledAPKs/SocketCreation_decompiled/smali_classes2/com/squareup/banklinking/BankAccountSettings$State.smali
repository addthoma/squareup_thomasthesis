.class public final Lcom/squareup/banklinking/BankAccountSettings$State;
.super Ljava/lang/Object;
.source "BankAccountSettings.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/banklinking/BankAccountSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBankAccountSettings.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BankAccountSettings.kt\ncom/squareup/banklinking/BankAccountSettings$State\n*L\n1#1,174:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0012\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B{\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\t\u0012\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c\u0012\u0008\u0008\u0002\u0010\r\u001a\u00020\u000e\u0012\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u0010\u0012\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u0012\u0012\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u0014\u0012\n\u0008\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0016\u00a2\u0006\u0002\u0010\u0017J\u0006\u0010!\u001a\u00020\u0007J\u0006\u0010\"\u001a\u00020\u0007J\t\u0010#\u001a\u00020\u0003H\u00c6\u0003J\t\u0010$\u001a\u00020\u0014H\u00c6\u0003J\u000b\u0010%\u001a\u0004\u0018\u00010\u0016H\u00c6\u0003J\u000b\u0010&\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\t\u0010\'\u001a\u00020\u0007H\u00c6\u0003J\u000b\u0010(\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\u000b\u0010)\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\t\u0010*\u001a\u00020\u000cH\u00c6\u0003J\t\u0010+\u001a\u00020\u000eH\u00c6\u0003J\t\u0010,\u001a\u00020\u0010H\u00c6\u0003J\t\u0010-\u001a\u00020\u0012H\u00c6\u0003J\u007f\u0010.\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\t2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\r\u001a\u00020\u000e2\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u00102\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u00122\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u00142\n\u0008\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00c6\u0001J\u0013\u0010/\u001a\u00020\u00072\u0008\u00100\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u00101\u001a\u000202H\u00d6\u0001J\t\u00103\u001a\u00020\u0005H\u00d6\u0001R\u0012\u0010\u0008\u001a\u0004\u0018\u00010\t8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0013\u0010\u0018\u001a\u0004\u0018\u00010\t8G\u00a2\u0006\u0006\u001a\u0004\u0008\u0018\u0010\u0019R\u0010\u0010\u000f\u001a\u00020\u00108\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u00020\u000c8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u00020\u00148\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0015\u001a\u0004\u0018\u00010\u00168\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u001a\u001a\u00020\u00078G\u00a2\u0006\u0006\u001a\u0004\u0008\u001a\u0010\u001bR\u0011\u0010\u001c\u001a\u00020\u00078F\u00a2\u0006\u0006\u001a\u0004\u0008\u001c\u0010\u001bR\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u00020\u000e8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\n\u001a\u0004\u0018\u00010\t8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u00020\u00128\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u001d\u001a\u00020\u00078G\u00a2\u0006\u0006\u001a\u0004\u0008\u001d\u0010\u001bR\u0013\u0010\u001e\u001a\u0004\u0018\u00010\u001f8G\u00a2\u0006\u0006\u001a\u0004\u0008\u001e\u0010 \u00a8\u00064"
    }
    d2 = {
        "Lcom/squareup/banklinking/BankAccountSettings$State;",
        "",
        "latestBankAccountState",
        "Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;",
        "password",
        "",
        "requiresPassword",
        "",
        "activeBankAccount",
        "Lcom/squareup/protos/client/bankaccount/BankAccount;",
        "pendingBankAccount",
        "checkPasswordState",
        "Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;",
        "linkBankAccountState",
        "Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;",
        "cancelVerificationState",
        "Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;",
        "resendEmailState",
        "Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;",
        "confirmBankAccountState",
        "Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;",
        "failureMessage",
        "Lcom/squareup/receiving/FailureMessage;",
        "(Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;)V",
        "bankAccount",
        "()Lcom/squareup/protos/client/bankaccount/BankAccount;",
        "hasBankAccount",
        "()Z",
        "isSuccessful",
        "showLinkBankAccount",
        "verificationState",
        "Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;",
        "()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;",
        "canBeCanceled",
        "canResendEmail",
        "component1",
        "component10",
        "component11",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public final activeBankAccount:Lcom/squareup/protos/client/bankaccount/BankAccount;

.field public final cancelVerificationState:Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;

.field public final checkPasswordState:Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;

.field public final confirmBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;

.field public final failureMessage:Lcom/squareup/receiving/FailureMessage;

.field public final latestBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

.field public final linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

.field public final password:Ljava/lang/String;

.field public final pendingBankAccount:Lcom/squareup/protos/client/bankaccount/BankAccount;

.field public final requiresPassword:Z

.field public final resendEmailState:Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;


# direct methods
.method public constructor <init>()V
    .locals 14

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x7ff

    const/4 v13, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v13}, Lcom/squareup/banklinking/BankAccountSettings$State;-><init>(Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;)V
    .locals 1

    const-string v0, "latestBankAccountState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkPasswordState"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "linkBankAccountState"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cancelVerificationState"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resendEmailState"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "confirmBankAccountState"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->latestBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    iput-object p2, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->password:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->requiresPassword:Z

    iput-object p4, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->activeBankAccount:Lcom/squareup/protos/client/bankaccount/BankAccount;

    iput-object p5, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->pendingBankAccount:Lcom/squareup/protos/client/bankaccount/BankAccount;

    iput-object p6, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->checkPasswordState:Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;

    iput-object p7, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    iput-object p8, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->cancelVerificationState:Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;

    iput-object p9, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->resendEmailState:Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;

    iput-object p10, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->confirmBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;

    iput-object p11, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 12

    move/from16 v0, p12

    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_0

    .line 92
    sget-object v1, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->LOADING:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    goto :goto_0

    :cond_0
    move-object v1, p1

    :goto_0
    and-int/lit8 v2, v0, 0x2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    .line 93
    move-object v2, v3

    check-cast v2, Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v2, p2

    :goto_1
    and-int/lit8 v4, v0, 0x4

    if-eqz v4, :cond_2

    const/4 v4, 0x0

    goto :goto_2

    :cond_2
    move v4, p3

    :goto_2
    and-int/lit8 v5, v0, 0x8

    if-eqz v5, :cond_3

    .line 95
    move-object v5, v3

    check-cast v5, Lcom/squareup/protos/client/bankaccount/BankAccount;

    goto :goto_3

    :cond_3
    move-object/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v0, 0x10

    if-eqz v6, :cond_4

    .line 96
    move-object v6, v3

    check-cast v6, Lcom/squareup/protos/client/bankaccount/BankAccount;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v0, 0x20

    if-eqz v7, :cond_5

    .line 97
    sget-object v7, Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;->INITIAL:Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v0, 0x40

    if-eqz v8, :cond_6

    .line 98
    sget-object v8, Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;->INITIAL:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v0, 0x80

    if-eqz v9, :cond_7

    .line 99
    sget-object v9, Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;->INITIAL:Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v0, 0x100

    if-eqz v10, :cond_8

    .line 100
    sget-object v10, Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;->INITIAL:Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v0, 0x200

    if-eqz v11, :cond_9

    .line 101
    sget-object v11, Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;->INITIAL:Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;

    goto :goto_9

    :cond_9
    move-object/from16 v11, p10

    :goto_9
    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_a

    .line 102
    move-object v0, v3

    check-cast v0, Lcom/squareup/receiving/FailureMessage;

    goto :goto_a

    :cond_a
    move-object/from16 v0, p11

    :goto_a
    move-object p1, p0

    move-object p2, v1

    move-object p3, v2

    move/from16 p4, v4

    move-object/from16 p5, v5

    move-object/from16 p6, v6

    move-object/from16 p7, v7

    move-object/from16 p8, v8

    move-object/from16 p9, v9

    move-object/from16 p10, v10

    move-object/from16 p11, v11

    move-object/from16 p12, v0

    invoke-direct/range {p1 .. p12}, Lcom/squareup/banklinking/BankAccountSettings$State;-><init>(Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;ILjava/lang/Object;)Lcom/squareup/banklinking/BankAccountSettings$State;
    .locals 12

    move-object v0, p0

    move/from16 v1, p12

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/banklinking/BankAccountSettings$State;->latestBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/banklinking/BankAccountSettings$State;->password:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-boolean v4, v0, Lcom/squareup/banklinking/BankAccountSettings$State;->requiresPassword:Z

    goto :goto_2

    :cond_2
    move v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/banklinking/BankAccountSettings$State;->activeBankAccount:Lcom/squareup/protos/client/bankaccount/BankAccount;

    goto :goto_3

    :cond_3
    move-object/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/banklinking/BankAccountSettings$State;->pendingBankAccount:Lcom/squareup/protos/client/bankaccount/BankAccount;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/banklinking/BankAccountSettings$State;->checkPasswordState:Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/banklinking/BankAccountSettings$State;->linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/squareup/banklinking/BankAccountSettings$State;->cancelVerificationState:Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lcom/squareup/banklinking/BankAccountSettings$State;->resendEmailState:Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget-object v11, v0, Lcom/squareup/banklinking/BankAccountSettings$State;->confirmBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;

    goto :goto_9

    :cond_9
    move-object/from16 v11, p10

    :goto_9
    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_a

    iget-object v1, v0, Lcom/squareup/banklinking/BankAccountSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    goto :goto_a

    :cond_a
    move-object/from16 v1, p11

    :goto_a
    move-object p1, v2

    move-object p2, v3

    move p3, v4

    move-object/from16 p4, v5

    move-object/from16 p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v10

    move-object/from16 p10, v11

    move-object/from16 p11, v1

    invoke-virtual/range {p0 .. p11}, Lcom/squareup/banklinking/BankAccountSettings$State;->copy(Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bankAccount()Lcom/squareup/protos/client/bankaccount/BankAccount;
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->pendingBankAccount:Lcom/squareup/protos/client/bankaccount/BankAccount;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->activeBankAccount:Lcom/squareup/protos/client/bankaccount/BankAccount;

    :goto_0
    return-object v0
.end method

.method public final canBeCanceled()Z
    .locals 4

    .line 124
    invoke-virtual {p0}, Lcom/squareup/banklinking/BankAccountSettings$State;->bankAccount()Lcom/squareup/protos/client/bankaccount/BankAccount;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 127
    iget-object v2, v0, Lcom/squareup/protos/client/bankaccount/BankAccount;->verification_state:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    sget-object v3, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->AWAITING_DEBIT_AUTHORIZATION:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    if-eq v2, v3, :cond_0

    iget-object v2, v0, Lcom/squareup/protos/client/bankaccount/BankAccount;->verification_state:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    sget-object v3, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->AWAITING_EMAIL_CONFIRMATION:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    if-eq v2, v3, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bankaccount/BankAccount;->verification_state:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    sget-object v2, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->VERIFICATION_IN_PROGRESS:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    if-ne v0, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public final canResendEmail()Z
    .locals 2

    .line 132
    invoke-virtual {p0}, Lcom/squareup/banklinking/BankAccountSettings$State;->bankAccount()Lcom/squareup/protos/client/bankaccount/BankAccount;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bankaccount/BankAccount;->verification_state:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->AWAITING_EMAIL_CONFIRMATION:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public final component1()Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->latestBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    return-object v0
.end method

.method public final component10()Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->confirmBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;

    return-object v0
.end method

.method public final component11()Lcom/squareup/receiving/FailureMessage;
    .locals 1

    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->password:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->requiresPassword:Z

    return v0
.end method

.method public final component4()Lcom/squareup/protos/client/bankaccount/BankAccount;
    .locals 1

    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->activeBankAccount:Lcom/squareup/protos/client/bankaccount/BankAccount;

    return-object v0
.end method

.method public final component5()Lcom/squareup/protos/client/bankaccount/BankAccount;
    .locals 1

    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->pendingBankAccount:Lcom/squareup/protos/client/bankaccount/BankAccount;

    return-object v0
.end method

.method public final component6()Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->checkPasswordState:Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;

    return-object v0
.end method

.method public final component7()Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    return-object v0
.end method

.method public final component8()Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->cancelVerificationState:Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;

    return-object v0
.end method

.method public final component9()Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->resendEmailState:Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;

    return-object v0
.end method

.method public final copy(Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;)Lcom/squareup/banklinking/BankAccountSettings$State;
    .locals 13

    const-string v0, "latestBankAccountState"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkPasswordState"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "linkBankAccountState"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cancelVerificationState"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resendEmailState"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "confirmBankAccountState"

    move-object/from16 v11, p10

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/banklinking/BankAccountSettings$State;

    move-object v1, v0

    move-object v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v12, p11

    invoke-direct/range {v1 .. v12}, Lcom/squareup/banklinking/BankAccountSettings$State;-><init>(Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/banklinking/BankAccountSettings$State;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/banklinking/BankAccountSettings$State;

    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->latestBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    iget-object v1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->latestBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->password:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->password:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->requiresPassword:Z

    iget-boolean v1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->requiresPassword:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->activeBankAccount:Lcom/squareup/protos/client/bankaccount/BankAccount;

    iget-object v1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->activeBankAccount:Lcom/squareup/protos/client/bankaccount/BankAccount;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->pendingBankAccount:Lcom/squareup/protos/client/bankaccount/BankAccount;

    iget-object v1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->pendingBankAccount:Lcom/squareup/protos/client/bankaccount/BankAccount;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->checkPasswordState:Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;

    iget-object v1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->checkPasswordState:Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    iget-object v1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->cancelVerificationState:Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;

    iget-object v1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->cancelVerificationState:Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->resendEmailState:Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;

    iget-object v1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->resendEmailState:Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->confirmBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;

    iget-object v1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->confirmBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    iget-object p1, p1, Lcom/squareup/banklinking/BankAccountSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final hasBankAccount()Z
    .locals 2

    .line 117
    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->latestBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    sget-object v1, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->HAS_BANK_ACCOUNT:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->latestBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->password:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->requiresPassword:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->activeBankAccount:Lcom/squareup/protos/client/bankaccount/BankAccount;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->pendingBankAccount:Lcom/squareup/protos/client/bankaccount/BankAccount;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->checkPasswordState:Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_6
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->cancelVerificationState:Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_7
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->resendEmailState:Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_8
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->confirmBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_8

    :cond_9
    const/4 v2, 0x0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_a
    add-int/2addr v0, v1

    return v0
.end method

.method public final isSuccessful()Z
    .locals 2

    .line 105
    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->latestBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    sget-object v1, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->NO_BANK_ACCOUNT:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->latestBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    sget-object v1, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->HAS_BANK_ACCOUNT:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public final showLinkBankAccount()Z
    .locals 2

    .line 113
    iget-object v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->latestBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    sget-object v1, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->NO_BANK_ACCOUNT:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->requiresPassword:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "State(latestBankAccountState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->latestBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", password="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->password:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", requiresPassword="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->requiresPassword:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", activeBankAccount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->activeBankAccount:Lcom/squareup/protos/client/bankaccount/BankAccount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", pendingBankAccount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->pendingBankAccount:Lcom/squareup/protos/client/bankaccount/BankAccount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", checkPasswordState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->checkPasswordState:Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", linkBankAccountState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cancelVerificationState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->cancelVerificationState:Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", resendEmailState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->resendEmailState:Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", confirmBankAccountState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->confirmBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", failureMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/banklinking/BankAccountSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final verificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;
    .locals 1

    .line 121
    invoke-virtual {p0}, Lcom/squareup/banklinking/BankAccountSettings$State;->bankAccount()Lcom/squareup/protos/client/bankaccount/BankAccount;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bankaccount/BankAccount;->verification_state:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method
