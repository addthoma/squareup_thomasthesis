.class public final Lcom/squareup/banklinking/RealLinkBankAccountViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "RealLinkBankAccountViewFactory.kt"

# interfaces
.implements Lcom/squareup/banklinking/LinkBankAccountViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0017\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/banklinking/RealLinkBankAccountViewFactory;",
        "Lcom/squareup/banklinking/LinkBankAccountViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "checkPasswordLayoutRunnerFactory",
        "Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$Factory;",
        "checkBankAccountInfoLayoutRunnerFactory",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$Factory;",
        "(Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$Factory;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$Factory;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$Factory;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "checkPasswordLayoutRunnerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkBankAccountInfoLayoutRunnerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 17
    sget-object v1, Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner;->Companion:Lcom/squareup/banklinking/fetchbank/FetchBankAccountLayoutRunner$Companion;

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 18
    new-instance v1, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$Binding;

    invoke-direct {v1, p1}, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$Binding;-><init>(Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$Factory;)V

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p1, 0x1

    aput-object v1, v0, p1

    .line 19
    new-instance p1, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$Binding;

    invoke-direct {p1, p2}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$Binding;-><init>(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$Factory;)V

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p2, 0x2

    aput-object p1, v0, p2

    .line 20
    sget-object p1, Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner;->Companion:Lcom/squareup/banklinking/showresult/ShowResultLayoutRunner$Companion;

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p2, 0x3

    aput-object p1, v0, p2

    .line 21
    sget-object p1, Lcom/squareup/banklinking/WarningDialogFactory;->Companion:Lcom/squareup/banklinking/WarningDialogFactory$Companion;

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p2, 0x4

    aput-object p1, v0, p2

    .line 22
    sget-object p1, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->Companion:Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner$Companion;

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p2, 0x5

    aput-object p1, v0, p2

    .line 16
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
