.class public final Lcom/squareup/billhistoryui/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistoryui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final activity_applet_bulk_settle_badge:I = 0x7f0a0151

.field public static final activity_applet_bulk_settle_button_glyph:I = 0x7f0a0152

.field public static final activity_applet_list_center_message_panel:I = 0x7f0a0155

.field public static final activity_applet_list_top_message_panel:I = 0x7f0a0156

.field public static final activity_applet_list_top_message_panel_deprecated:I = 0x7f0a0157

.field public static final activity_applet_list_top_message_panel_message:I = 0x7f0a0158

.field public static final activity_applet_list_top_message_panel_tap_state_message:I = 0x7f0a0159

.field public static final activity_applet_list_top_message_panel_title:I = 0x7f0a015a

.field public static final activity_applet_receipt_content:I = 0x7f0a015b

.field public static final activity_applet_receipt_message_panel:I = 0x7f0a015c

.field public static final activity_applet_search_clickable_text:I = 0x7f0a015e

.field public static final activity_applet_search_message:I = 0x7f0a015f

.field public static final bill_details_loading_panel:I = 0x7f0a022a

.field public static final bill_history_detail_empty_view_container:I = 0x7f0a022b

.field public static final bill_history_loyalty_section_row_subtitle:I = 0x7f0a022c

.field public static final bill_history_loyalty_section_row_title:I = 0x7f0a022d

.field public static final bill_history_loyalty_section_row_value:I = 0x7f0a022e

.field public static final bill_history_loyalty_section_title:I = 0x7f0a022f

.field public static final bill_history_view:I = 0x7f0a0230

.field public static final bill_history_view_content:I = 0x7f0a0231

.field public static final bill_history_view_loading_panel:I = 0x7f0a0232

.field public static final bulk_settle_content:I = 0x7f0a024b

.field public static final bulk_settle_focused_spacer:I = 0x7f0a024c

.field public static final bulk_settle_glyph_message:I = 0x7f0a024d

.field public static final bulk_settle_header:I = 0x7f0a024e

.field public static final bulk_settle_loading:I = 0x7f0a024f

.field public static final bulk_settle_primary_button:I = 0x7f0a0250

.field public static final bulk_settle_quicktip_editor:I = 0x7f0a0251

.field public static final bulk_settle_search:I = 0x7f0a0252

.field public static final bulk_settle_sort_amount:I = 0x7f0a0253

.field public static final bulk_settle_sort_employee:I = 0x7f0a0254

.field public static final bulk_settle_sort_options:I = 0x7f0a0255

.field public static final bulk_settle_sort_time:I = 0x7f0a0256

.field public static final bulk_settle_tender_amount:I = 0x7f0a0257

.field public static final bulk_settle_tender_employee_name:I = 0x7f0a0258

.field public static final bulk_settle_tender_receipt_number:I = 0x7f0a0259

.field public static final bulk_settle_tender_row_total:I = 0x7f0a025a

.field public static final bulk_settle_tender_time_with_employee_name:I = 0x7f0a025b

.field public static final bulk_settle_tender_time_with_no_employee_name:I = 0x7f0a025c

.field public static final bulk_settle_tenders_list:I = 0x7f0a025d

.field public static final bulk_settle_tenders_list_container:I = 0x7f0a025e

.field public static final bulk_settle_time_and_employee_name_container:I = 0x7f0a025f

.field public static final bulk_settle_up_button:I = 0x7f0a0260

.field public static final bulk_settle_up_text:I = 0x7f0a0261

.field public static final bulk_settle_view_all_button:I = 0x7f0a0262

.field public static final bulk_settle_view_all_container:I = 0x7f0a0263

.field public static final button_container:I = 0x7f0a0277

.field public static final circle:I = 0x7f0a033b

.field public static final circle_initials_text:I = 0x7f0a033c

.field public static final crm_customer_display_name:I = 0x7f0a0425

.field public static final customer_row:I = 0x7f0a053a

.field public static final customer_row_container:I = 0x7f0a053b

.field public static final gross:I = 0x7f0a07bd

.field public static final header_text_view:I = 0x7f0a07cb

.field public static final instant_deposit_button:I = 0x7f0a0849

.field public static final instant_deposit_container:I = 0x7f0a084a

.field public static final instant_deposit_error:I = 0x7f0a084b

.field public static final instant_deposit_hint:I = 0x7f0a084c

.field public static final instant_deposit_progress:I = 0x7f0a084d

.field public static final instant_deposits_done_button:I = 0x7f0a084e

.field public static final instant_deposits_glyph_message:I = 0x7f0a084f

.field public static final instant_deposits_learn_more_button:I = 0x7f0a0850

.field public static final instant_deposits_progress:I = 0x7f0a0856

.field public static final issue_refund_button:I = 0x7f0a08c5

.field public static final items_title:I = 0x7f0a08ee

.field public static final label:I = 0x7f0a090d

.field public static final loading_panel:I = 0x7f0a0953

.field public static final loyalty_container:I = 0x7f0a0971

.field public static final net:I = 0x7f0a0a11

.field public static final print_gift_receipt_button:I = 0x7f0a0c67

.field public static final receipt_detail_add_tip_button:I = 0x7f0a0cff

.field public static final receipt_detail_items_container:I = 0x7f0a0d00

.field public static final receipt_detail_quicktip_editor:I = 0x7f0a0d01

.field public static final receipt_detail_settle_tips_section:I = 0x7f0a0d02

.field public static final receipt_detail_tax_breakdown_container:I = 0x7f0a0d03

.field public static final receipt_detail_tender_tip_title:I = 0x7f0a0d04

.field public static final receipt_detail_total_container:I = 0x7f0a0d05

.field public static final receipt_details_view_animator:I = 0x7f0a0d06

.field public static final receipt_list_row_amount:I = 0x7f0a0d09

.field public static final receipt_list_row_color_strip:I = 0x7f0a0d0a

.field public static final receipt_list_row_subtitle:I = 0x7f0a0d0b

.field public static final receipt_list_row_thumbnail:I = 0x7f0a0d0c

.field public static final receipt_list_row_title_and_quantity:I = 0x7f0a0d0d

.field public static final receipt_list_row_title_and_subtitle_container:I = 0x7f0a0d0e

.field public static final refund_past_deadline:I = 0x7f0a0d40

.field public static final refund_title:I = 0x7f0a0d45

.field public static final refunds_container:I = 0x7f0a0d4e

.field public static final related_bill_container:I = 0x7f0a0d50

.field public static final related_bill_employee:I = 0x7f0a0d51

.field public static final related_bill_timestamp:I = 0x7f0a0d52

.field public static final reprint_ticket_button:I = 0x7f0a0d6a

.field public static final sales_history_view:I = 0x7f0a0db7

.field public static final search_field:I = 0x7f0a0e1e

.field public static final select_gift_receipt_tender_view:I = 0x7f0a0e42

.field public static final select_receipt_tender_view:I = 0x7f0a0e4e

.field public static final select_refund_tenders_container:I = 0x7f0a0e4f

.field public static final send_receipt_button:I = 0x7f0a0e68

.field public static final status_container:I = 0x7f0a0f36

.field public static final status_message:I = 0x7f0a0f38

.field public static final status_title:I = 0x7f0a0f39

.field public static final swipe_refresh:I = 0x7f0a0f54

.field public static final tax:I = 0x7f0a0f70

.field public static final tender_employee:I = 0x7f0a0f84

.field public static final tender_employee_deprecated:I = 0x7f0a0f85

.field public static final tender_section_tax_breakdown_container:I = 0x7f0a0f87

.field public static final tender_section_total_container:I = 0x7f0a0f88

.field public static final tender_timestamp:I = 0x7f0a0f89

.field public static final tender_type:I = 0x7f0a0f8a

.field public static final tenders_container:I = 0x7f0a0f8c

.field public static final ticket_note:I = 0x7f0a0fc0

.field public static final tip_amount_editor:I = 0x7f0a1031

.field public static final tip_option_custom:I = 0x7f0a1038

.field public static final tip_options_view:I = 0x7f0a1039

.field public static final total_title:I = 0x7f0a1058

.field public static final transactions_history_animator:I = 0x7f0a1073

.field public static final transactions_history_list:I = 0x7f0a1074

.field public static final transactions_history_search_bar:I = 0x7f0a1075


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
