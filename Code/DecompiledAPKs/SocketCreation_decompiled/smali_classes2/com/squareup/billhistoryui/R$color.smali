.class public final Lcom/squareup/billhistoryui/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistoryui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final instant_transfer_button_text_blue:I = 0x7f060113

.field public static final instant_transfer_button_text_white:I = 0x7f060114

.field public static final transaction_details_tender_customer_initials:I = 0x7f060300

.field public static final transaction_details_tender_customer_name:I = 0x7f060301

.field public static final transactions_history_divider:I = 0x7f060302

.field public static final transactions_history_header_background:I = 0x7f060303

.field public static final transactions_history_row_subtitle:I = 0x7f060304

.field public static final transactions_history_search_bar_icon:I = 0x7f060305

.field public static final transactions_history_search_bar_tap_disabled_text:I = 0x7f060306

.field public static final transactions_history_search_bar_tap_enabled_text:I = 0x7f060307

.field public static final transactions_history_search_bar_text:I = 0x7f060308

.field public static final transactions_history_search_bar_text_hint:I = 0x7f060309

.field public static final transactions_history_top_message_panel_message_text:I = 0x7f06030a


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
