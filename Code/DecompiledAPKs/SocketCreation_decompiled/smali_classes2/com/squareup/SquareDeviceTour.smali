.class public interface abstract Lcom/squareup/SquareDeviceTour;
.super Ljava/lang/Object;
.source "SquareDeviceTour.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/SquareDeviceTour$NoSquareDeviceTour;,
        Lcom/squareup/SquareDeviceTour$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u0000 \u00062\u00020\u0001:\u0002\u0006\u0007J\u0012\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/SquareDeviceTour;",
        "",
        "redirectForTour",
        "Lcom/squareup/container/RedirectStep$Result;",
        "traversal",
        "Lflow/Traversal;",
        "Companion",
        "NoSquareDeviceTour",
        "square-device-tour_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/SquareDeviceTour$Companion;

.field public static final LEAVING:Ljava/lang/String; = "Leaving Device or Feature Tour"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/SquareDeviceTour$Companion;->$$INSTANCE:Lcom/squareup/SquareDeviceTour$Companion;

    sput-object v0, Lcom/squareup/SquareDeviceTour;->Companion:Lcom/squareup/SquareDeviceTour$Companion;

    return-void
.end method


# virtual methods
.method public abstract redirectForTour(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
.end method
