.class public abstract Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;
.super Lcom/squareup/balance/activity/data/UnifiedActivityResult;
.source "UnifiedActivityResult.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/activity/data/UnifiedActivityResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "HasData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData$Success;,
        Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData$ErrorFetchingMore;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u000c\rB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0018\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007R\u0014\u0010\u0008\u001a\u0004\u0018\u00010\tX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000b\u0082\u0001\u0002\u000e\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;",
        "Lcom/squareup/balance/activity/data/UnifiedActivityResult;",
        "()V",
        "activities",
        "",
        "Lcom/squareup/protos/bizbank/UnifiedActivity;",
        "getActivities",
        "()Ljava/util/List;",
        "batchToken",
        "",
        "getBatchToken",
        "()Ljava/lang/String;",
        "ErrorFetchingMore",
        "Success",
        "Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData$Success;",
        "Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData$ErrorFetchingMore;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 7
    invoke-direct {p0, v0}, Lcom/squareup/balance/activity/data/UnifiedActivityResult;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 7
    invoke-direct {p0}, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getActivities()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/bizbank/UnifiedActivity;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getBatchToken()Ljava/lang/String;
.end method
