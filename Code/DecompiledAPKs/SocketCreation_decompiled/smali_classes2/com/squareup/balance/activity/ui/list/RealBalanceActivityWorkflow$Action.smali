.class abstract Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action;
.super Ljava/lang/Object;
.source "RealBalanceActivityWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$NewTabSelected;,
        Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$ActivityTapped;,
        Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$BackFromDetails;,
        Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$OpenTransferReports;,
        Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$BackFromTransferReports;,
        Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$Exit;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/activity/ui/list/BalanceActivityState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00082\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0006\u0008\t\n\u000b\u000c\rB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0019\u0010\u0005\u001a\u0004\u0018\u00010\u0003*\u0008\u0012\u0004\u0012\u00020\u00020\u0006H\u0016\u00a2\u0006\u0002\u0010\u0007\u0082\u0001\u0006\u000e\u000f\u0010\u0011\u0012\u0013\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/activity/ui/list/BalanceActivityState;",
        "",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;",
        "ActivityTapped",
        "BackFromDetails",
        "BackFromTransferReports",
        "Exit",
        "NewTabSelected",
        "OpenTransferReports",
        "Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$NewTabSelected;",
        "Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$ActivityTapped;",
        "Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$BackFromDetails;",
        "Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$OpenTransferReports;",
        "Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$BackFromTransferReports;",
        "Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$Exit;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 95
    invoke-direct {p0}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 95
    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/balance/activity/ui/list/BalanceActivityState;",
            ">;)",
            "Lkotlin/Unit;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    instance-of v0, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$NewTabSelected;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 99
    new-instance v0, Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingBalanceActivity;

    move-object v2, p0

    check-cast v2, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$NewTabSelected;

    invoke-virtual {v2}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$NewTabSelected;->getType()Lcom/squareup/balance/activity/data/BalanceActivityType;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingBalanceActivity;-><init>(Lcom/squareup/balance/activity/data/BalanceActivityType;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 102
    :cond_0
    instance-of v0, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$ActivityTapped;

    if-eqz v0, :cond_1

    .line 103
    new-instance v0, Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingBalanceDetails;

    move-object v2, p0

    check-cast v2, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$ActivityTapped;

    invoke-virtual {v2}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$ActivityTapped;->getType()Lcom/squareup/balance/activity/data/BalanceActivityType;

    move-result-object v3

    invoke-virtual {v2}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$ActivityTapped;->getActivity()Lcom/squareup/protos/bizbank/UnifiedActivity;

    move-result-object v2

    invoke-direct {v0, v3, v2}, Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingBalanceDetails;-><init>(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/protos/bizbank/UnifiedActivity;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 106
    :cond_1
    instance-of v0, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$BackFromDetails;

    if-eqz v0, :cond_2

    .line 107
    new-instance v0, Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingBalanceActivity;

    move-object v2, p0

    check-cast v2, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$BackFromDetails;

    invoke-virtual {v2}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$BackFromDetails;->getType()Lcom/squareup/balance/activity/data/BalanceActivityType;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingBalanceActivity;-><init>(Lcom/squareup/balance/activity/data/BalanceActivityType;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 110
    :cond_2
    instance-of v0, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$OpenTransferReports;

    if-eqz v0, :cond_3

    .line 111
    new-instance v0, Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingTransferReports;

    move-object v2, p0

    check-cast v2, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$OpenTransferReports;

    invoke-virtual {v2}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$OpenTransferReports;->getType()Lcom/squareup/balance/activity/data/BalanceActivityType;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingTransferReports;-><init>(Lcom/squareup/balance/activity/data/BalanceActivityType;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 114
    :cond_3
    instance-of v0, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$BackFromTransferReports;

    if-eqz v0, :cond_4

    .line 115
    new-instance v0, Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingBalanceActivity;

    move-object v2, p0

    check-cast v2, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$BackFromTransferReports;

    invoke-virtual {v2}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$BackFromTransferReports;->getType()Lcom/squareup/balance/activity/data/BalanceActivityType;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingBalanceActivity;-><init>(Lcom/squareup/balance/activity/data/BalanceActivityType;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 118
    :cond_4
    sget-object p1, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$Exit;->INSTANCE:Lcom/squareup/balance/activity/ui/list/RealBalanceActivityWorkflow$Action$Exit;

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    :goto_0
    return-object v1

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/balance/activity/ui/list/BalanceActivityState;",
            "-",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
