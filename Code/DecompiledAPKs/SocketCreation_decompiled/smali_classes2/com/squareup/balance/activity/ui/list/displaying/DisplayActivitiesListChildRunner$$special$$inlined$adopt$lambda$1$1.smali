.class final Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$1$1;
.super Ljava/lang/Object;
.source "DisplayActivitiesListChildRunner.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$1;->invoke(ILjava/lang/Object;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005\u00a8\u0006\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onClick",
        "com/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$1$1$1$1",
        "com/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$bind$1$lambda$1",
        "com/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$nohoRow$lambda$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $activity:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;

.field final synthetic this$0:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$1;


# direct methods
.method constructor <init>(Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$1$1;->$activity:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$1$1;->this$0:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    .line 90
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$1$1;->this$0:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$1;

    iget-object p1, p1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;

    invoke-static {p1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;->access$getLastScreen$p(Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;)Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;->getOnActivitySelected()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$1$1;->$activity:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_0
    return-void
.end method
