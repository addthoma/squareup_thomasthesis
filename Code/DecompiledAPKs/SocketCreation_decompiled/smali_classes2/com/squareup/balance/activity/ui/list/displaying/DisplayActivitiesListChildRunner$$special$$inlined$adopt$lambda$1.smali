.class public final Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "BinderRowSpec.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;-><init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function3<",
        "Ljava/lang/Integer;",
        "TS;TV;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBinderRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BinderRowSpec.kt\ncom/squareup/cycler/BinderRowSpec$bind$1\n+ 2 DisplayActivitiesListChildRunner.kt\ncom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner\n*L\n1#1,56:1\n65#2,27:57\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00de\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0006\n\u0002\u0008\u0006\n\u0002\u0008\u0006\n\u0002\u0008\u0006\n\u0002\u0008\u0006\n\u0002\u0008\u0007\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0002\u0010\u0005*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u0002H\u00042\u0006\u0010\n\u001a\u0002H\u0005H\n\u00a2\u0006\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u000f"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "<anonymous parameter 0>",
        "",
        "item",
        "view",
        "invoke",
        "(ILjava/lang/Object;Landroid/view/View;)V",
        "com/squareup/cycler/BinderRowSpec$bind$1",
        "com/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$bind$1",
        "com/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$nohoRow$lambda$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;

    const/4 p1, 0x3

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p3, Landroid/view/View;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$1;->invoke(ILjava/lang/Object;Landroid/view/View;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILjava/lang/Object;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITS;TV;)V"
        }
    .end annotation

    const-string p1, "item"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p1, "view"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    check-cast p3, Lcom/squareup/noho/NohoRow;

    check-cast p2, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;

    .line 57
    invoke-virtual {p2}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->getTransactionAmount()Lcom/squareup/balance/activity/ui/common/Amount;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/common/Amount;->getText()Lcom/squareup/resources/TextModel;

    move-result-object p1

    invoke-virtual {p3}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "row.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 59
    invoke-virtual {p2}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->getTransactionAmount()Lcom/squareup/balance/activity/ui/common/Amount;

    move-result-object v0

    .line 60
    instance-of v2, v0, Lcom/squareup/balance/activity/ui/common/Amount$CrossedOutAmount;

    if-eqz v2, :cond_0

    .line 61
    sget v0, Lcom/squareup/balance/activity/impl/R$style;->BalanceActivityAmount_CrossOut:I

    invoke-virtual {p3, v0}, Lcom/squareup/noho/NohoRow;->setValueAppearanceId(I)V

    .line 62
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 63
    new-instance v2, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v2}, Landroid/text/style/StrikethroughSpan;-><init>()V

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, p1, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 62
    move-object p1, v0

    check-cast p1, Ljava/lang/CharSequence;

    goto :goto_0

    .line 66
    :cond_0
    instance-of v2, v0, Lcom/squareup/balance/activity/ui/common/Amount$PositiveAmount;

    if-eqz v2, :cond_1

    .line 67
    sget v0, Lcom/squareup/balance/activity/impl/R$style;->BalanceActivityAmount_Positive:I

    invoke-virtual {p3, v0}, Lcom/squareup/noho/NohoRow;->setValueAppearanceId(I)V

    goto :goto_0

    .line 70
    :cond_1
    instance-of v0, v0, Lcom/squareup/balance/activity/ui/common/Amount$UndecoratedAmount;

    if-eqz v0, :cond_3

    .line 71
    sget v0, Lcom/squareup/balance/activity/impl/R$style;->BalanceActivityAmount:I

    invoke-virtual {p3, v0}, Lcom/squareup/noho/NohoRow;->setValueAppearanceId(I)V

    .line 76
    :goto_0
    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 77
    invoke-virtual {p2}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->getRunningBalance()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoRow;->setSubValue(Ljava/lang/CharSequence;)V

    .line 78
    invoke-virtual {p2}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->getLabel()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 79
    invoke-virtual {p2}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->getSubLabel()Lcom/squareup/resources/TextModel;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p3}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    .line 80
    :goto_1
    invoke-static {p1}, Lcom/squareup/util/Strings;->valueOrEmpty(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 82
    new-instance p1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$1$1;

    invoke-direct {p1, p2, p0}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$1$1;-><init>(Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$1;)V

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    .line 72
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
