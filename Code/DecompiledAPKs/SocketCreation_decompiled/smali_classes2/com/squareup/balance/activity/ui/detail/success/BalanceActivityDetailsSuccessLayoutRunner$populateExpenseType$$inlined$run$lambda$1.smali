.class final Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$populateExpenseType$$inlined$run$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "BalanceActivityDetailsSuccessLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->populateExpenseType(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "status",
        "Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;",
        "invoke",
        "com/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$populateExpenseType$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $data$inlined:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;

.field final synthetic this$0:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$populateExpenseType$$inlined$run$lambda$1;->this$0:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$populateExpenseType$$inlined$run$lambda$1;->$data$inlined:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$populateExpenseType$$inlined$run$lambda$1;->invoke(Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;)V
    .locals 1

    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$populateExpenseType$$inlined$run$lambda$1;->this$0:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;

    invoke-static {v0}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->access$dismissError(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;)V

    .line 153
    sget-object v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 155
    :cond_0
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$populateExpenseType$$inlined$run$lambda$1;->$data$inlined:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->getOnPersonalExpenseTypeSelected()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    goto :goto_0

    .line 154
    :cond_1
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$populateExpenseType$$inlined$run$lambda$1;->$data$inlined:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->getOnBusinessExpenseTypeSelected()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    :goto_0
    return-void
.end method
