.class public final Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;
.super Ljava/lang/Object;
.source "RealBalanceActivityRepository.kt"

# interfaces
.implements Lcom/squareup/balance/activity/data/BalanceActivityRepository;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$ResultToUpdate;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u0001:\u0001!B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\t0\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0016\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00132\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u0016\u001a\u00020\tH\u0002J\u001e\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00132\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u000e\u001a\u00020\tH\u0002J\u0008\u0010\u0018\u001a\u00020\u0006H\u0002J\u0008\u0010\u0019\u001a\u00020\u001aH\u0016J\u001a\u0010\u001b\u001a\u0010\u0012\u000c\u0012\n \n*\u0004\u0018\u00010\t0\t0\u0008*\u00020\u0011H\u0002J \u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\t0\u000f*\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J8\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0013*\u0008\u0012\u0004\u0012\u00020\t0\u00082\u000e\u0010\u001e\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u001f0\u00132\u000e\u0010 \u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u001f0\u0013H\u0002R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0007\u001a\u0010\u0012\u000c\u0012\n \n*\u0004\u0018\u00010\t0\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u000b\u001a\u0010\u0012\u000c\u0012\n \n*\u0004\u0018\u00010\t0\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\r\u001a\u0010\u0012\u000c\u0012\n \n*\u0004\u0018\u00010\t0\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;",
        "Lcom/squareup/balance/activity/data/BalanceActivityRepository;",
        "remoteStore",
        "Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;",
        "(Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;)V",
        "_maxLatestEventAt",
        "Lcom/squareup/protos/common/time/DateTime;",
        "allActivityRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/balance/activity/data/BalanceActivity;",
        "kotlin.jvm.PlatformType",
        "cardSpendRelay",
        "initial",
        "transfersRelay",
        "balanceActivity",
        "Lio/reactivex/Observable;",
        "type",
        "Lcom/squareup/balance/activity/data/BalanceActivityType;",
        "fetchMoreBalanceActivity",
        "Lio/reactivex/Single;",
        "findWhichResultToUpdate",
        "Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$ResultToUpdate;",
        "activity",
        "loadMoreData",
        "maxLatestEventAt",
        "reset",
        "",
        "findRelay",
        "refetchOnErrorOrLoading",
        "zipAndFetch",
        "pendingRequest",
        "Lcom/squareup/balance/activity/data/UnifiedActivityResult;",
        "completedRequest",
        "ResultToUpdate",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private _maxLatestEventAt:Lcom/squareup/protos/common/time/DateTime;

.field private final allActivityRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ">;"
        }
    .end annotation
.end field

.field private final cardSpendRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ">;"
        }
    .end annotation
.end field

.field private final initial:Lcom/squareup/balance/activity/data/BalanceActivity;

.field private final remoteStore:Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;

.field private final transfersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "remoteStore"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->remoteStore:Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;

    .line 36
    new-instance p1, Lcom/squareup/balance/activity/data/BalanceActivity;

    sget-object v0, Lcom/squareup/balance/activity/data/UnifiedActivityResult$Loading;->INSTANCE:Lcom/squareup/balance/activity/data/UnifiedActivityResult$Loading;

    check-cast v0, Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    sget-object v1, Lcom/squareup/balance/activity/data/UnifiedActivityResult$Loading;->INSTANCE:Lcom/squareup/balance/activity/data/UnifiedActivityResult$Loading;

    check-cast v1, Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    invoke-direct {p1, v0, v1}, Lcom/squareup/balance/activity/data/BalanceActivity;-><init>(Lcom/squareup/balance/activity/data/UnifiedActivityResult;Lcom/squareup/balance/activity/data/UnifiedActivityResult;)V

    iput-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->initial:Lcom/squareup/balance/activity/data/BalanceActivity;

    .line 38
    iget-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->initial:Lcom/squareup/balance/activity/data/BalanceActivity;

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string v0, "BehaviorRelay.createDefault(initial)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->allActivityRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 39
    iget-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->initial:Lcom/squareup/balance/activity/data/BalanceActivity;

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->cardSpendRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 40
    iget-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->initial:Lcom/squareup/balance/activity/data/BalanceActivity;

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->transfersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$getRemoteStore$p(Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;)Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->remoteStore:Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;

    return-object p0
.end method

.method public static final synthetic access$loadMoreData(Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/balance/activity/data/BalanceActivity;)Lio/reactivex/Single;
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->loadMoreData(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/balance/activity/data/BalanceActivity;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$maxLatestEventAt(Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;)Lcom/squareup/protos/common/time/DateTime;
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->maxLatestEventAt()Lcom/squareup/protos/common/time/DateTime;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$zipAndFetch(Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;Lcom/jakewharton/rxrelay2/BehaviorRelay;Lio/reactivex/Single;Lio/reactivex/Single;)Lio/reactivex/Single;
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->zipAndFetch(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lio/reactivex/Single;Lio/reactivex/Single;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private final findRelay(Lcom/squareup/balance/activity/data/BalanceActivityType;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivityType;",
            ")",
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ">;"
        }
    .end annotation

    .line 79
    sget-object v0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivityType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 82
    iget-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->transfersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 81
    :cond_1
    iget-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->cardSpendRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    goto :goto_0

    .line 80
    :cond_2
    iget-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->allActivityRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    :goto_0
    return-object p1
.end method

.method private final findWhichResultToUpdate(Lcom/squareup/balance/activity/data/BalanceActivity;)Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$ResultToUpdate;
    .locals 2

    .line 88
    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivity;->getPendingResult()Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/balance/activity/data/UnifiedActivityResultKt;->hasBatchToken(Lcom/squareup/balance/activity/data/UnifiedActivityResult;)Z

    move-result v0

    const-string v1, "null cannot be cast to non-null type com.squareup.balance.activity.data.UnifiedActivityResult.HasData"

    if-eqz v0, :cond_1

    .line 89
    new-instance v0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$ResultToUpdate;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivity;->getPendingResult()Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$ResultToUpdate;-><init>(Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;Z)V

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 91
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivity;->getCompletedResult()Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/balance/activity/data/UnifiedActivityResultKt;->hasBatchToken(Lcom/squareup/balance/activity/data/UnifiedActivityResult;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 92
    new-instance v0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$ResultToUpdate;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivity;->getCompletedResult()Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    move-result-object p1

    if-eqz p1, :cond_2

    check-cast p1, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$ResultToUpdate;-><init>(Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;Z)V

    goto :goto_0

    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private final loadMoreData(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/balance/activity/data/BalanceActivity;)Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivityType;",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ">;"
        }
    .end annotation

    .line 104
    invoke-direct {p0, p2}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->findWhichResultToUpdate(Lcom/squareup/balance/activity/data/BalanceActivity;)Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$ResultToUpdate;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 106
    invoke-virtual {v0}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$ResultToUpdate;->component1()Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;

    move-result-object v1

    .line 104
    invoke-virtual {v0}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$ResultToUpdate;->component2()Z

    move-result v0

    .line 108
    invoke-direct {p0}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->maxLatestEventAt()Lcom/squareup/protos/common/time/DateTime;

    move-result-object v2

    if-eqz v0, :cond_1

    .line 110
    iget-object v3, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->remoteStore:Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;

    .line 113
    invoke-virtual {v1}, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;->getBatchToken()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 110
    :cond_0
    invoke-interface {v3, p1, v2, v4}, Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;->fetchPendingBalanceActivity(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 116
    :cond_1
    iget-object v3, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->remoteStore:Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;

    .line 119
    invoke-virtual {v1}, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;->getBatchToken()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 116
    :cond_2
    invoke-interface {v3, p1, v2, v4}, Lcom/squareup/balance/activity/data/RemoteBalanceActivityDataStore;->fetchCompletedBalanceActivity(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 124
    :goto_0
    new-instance v2, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$loadMoreData$1;

    invoke-direct {v2, v1, v0, p2}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$loadMoreData$1;-><init>(Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;ZLcom/squareup/balance/activity/data/BalanceActivity;)V

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "loadMoreResult\n        .\u2026ta)\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 106
    :cond_3
    invoke-static {p2}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.just(balanceActivity)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final maxLatestEventAt()Lcom/squareup/protos/common/time/DateTime;
    .locals 1

    .line 186
    iget-object v0, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->_maxLatestEventAt:Lcom/squareup/protos/common/time/DateTime;

    if-nez v0, :cond_0

    .line 188
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-static {v0}, Lcom/squareup/util/ProtoTimes;->asDateTime(Ljava/util/Date;)Lcom/squareup/protos/common/time/DateTime;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->_maxLatestEventAt:Lcom/squareup/protos/common/time/DateTime;

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->_maxLatestEventAt:Lcom/squareup/protos/common/time/DateTime;

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    return-object v0
.end method

.method private final refetchOnErrorOrLoading(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/balance/activity/data/BalanceActivityType;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ">;",
            "Lcom/squareup/balance/activity/data/BalanceActivityType;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ">;"
        }
    .end annotation

    .line 158
    sget-object v0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$1;->INSTANCE:Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$1;

    check-cast v0, Lio/reactivex/functions/BiPredicate;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged(Lio/reactivex/functions/BiPredicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 165
    new-instance v1, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$refetchOnErrorOrLoading$2;-><init>(Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/balance/activity/data/BalanceActivityType;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "distinctUntilChanged { p\u2026etedRequest\n      )\n    }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final zipAndFetch(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lio/reactivex/Single;Lio/reactivex/Single;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ">;",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/balance/activity/data/UnifiedActivityResult;",
            ">;",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/balance/activity/data/UnifiedActivityResult;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ">;"
        }
    .end annotation

    .line 198
    sget-object v0, Lcom/squareup/util/rx2/Singles;->INSTANCE:Lcom/squareup/util/rx2/Singles;

    check-cast p2, Lio/reactivex/SingleSource;

    check-cast p3, Lio/reactivex/SingleSource;

    invoke-virtual {v0, p2, p3}, Lcom/squareup/util/rx2/Singles;->zip(Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;)Lio/reactivex/Single;

    move-result-object p2

    .line 199
    sget-object p3, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$zipAndFetch$1;->INSTANCE:Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$zipAndFetch$1;

    check-cast p3, Lio/reactivex/functions/Function;

    invoke-virtual {p2, p3}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p2

    .line 202
    new-instance p3, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$zipAndFetch$2;

    invoke-direct {p3, p1}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$zipAndFetch$2;-><init>(Lcom/jakewharton/rxrelay2/BehaviorRelay;)V

    check-cast p3, Lio/reactivex/functions/Consumer;

    invoke-virtual {p2, p3}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Singles.zip(pendingReque\u2026alanceActivity)\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public balanceActivity(Lcom/squareup/balance/activity/data/BalanceActivityType;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivityType;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ">;"
        }
    .end annotation

    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0, p1}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->findRelay(Lcom/squareup/balance/activity/data/BalanceActivityType;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    .line 44
    invoke-direct {p0, v0, p1}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->refetchOnErrorOrLoading(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/balance/activity/data/BalanceActivityType;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public fetchMoreBalanceActivity(Lcom/squareup/balance/activity/data/BalanceActivityType;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivityType;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ">;"
        }
    .end annotation

    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-direct {p0, p1}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->findRelay(Lcom/squareup/balance/activity/data/BalanceActivityType;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    .line 52
    new-instance v1, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$1;-><init>(Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;Lcom/squareup/balance/activity/data/BalanceActivityType;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 63
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    .line 65
    new-instance v1, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$2;

    invoke-direct {v1, v0}, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository$fetchMoreBalanceActivity$2;-><init>(Lcom/jakewharton/rxrelay2/BehaviorRelay;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v1}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "relay\n        .switchMap\u2026cess { relay.accept(it) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public reset()V
    .locals 2

    const/4 v0, 0x0

    .line 70
    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->_maxLatestEventAt:Lcom/squareup/protos/common/time/DateTime;

    .line 73
    iget-object v0, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->allActivityRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->initial:Lcom/squareup/balance/activity/data/BalanceActivity;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->cardSpendRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->initial:Lcom/squareup/balance/activity/data/BalanceActivity;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->transfersRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityRepository;->initial:Lcom/squareup/balance/activity/data/BalanceActivity;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
