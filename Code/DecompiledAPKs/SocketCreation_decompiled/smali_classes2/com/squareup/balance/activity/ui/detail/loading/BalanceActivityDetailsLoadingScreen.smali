.class public final Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingScreen;
.super Ljava/lang/Object;
.source "BalanceActivityDetailsLoadingScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "title",
        "Lcom/squareup/util/ViewString;",
        "onBack",
        "Lkotlin/Function0;",
        "",
        "(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V",
        "getOnBack",
        "()Lkotlin/jvm/functions/Function0;",
        "getTitle",
        "()Lcom/squareup/util/ViewString;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final onBack:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final title:Lcom/squareup/util/ViewString;


# direct methods
.method public constructor <init>(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/ViewString;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onBack"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingScreen;->title:Lcom/squareup/util/ViewString;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingScreen;->onBack:Lkotlin/jvm/functions/Function0;

    return-void
.end method


# virtual methods
.method public final getOnBack()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 8
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingScreen;->onBack:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getTitle()Lcom/squareup/util/ViewString;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingScreen;->title:Lcom/squareup/util/ViewString;

    return-object v0
.end method
