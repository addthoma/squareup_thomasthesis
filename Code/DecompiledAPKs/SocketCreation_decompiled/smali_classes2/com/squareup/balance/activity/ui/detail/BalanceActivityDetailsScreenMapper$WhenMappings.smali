.class public final synthetic Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I

.field public static final synthetic $EnumSwitchMapping$2:[I

.field public static final synthetic $EnumSwitchMapping$3:[I

.field public static final synthetic $EnumSwitchMapping$4:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 12

    invoke-static {}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->values()[Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ATM_WITHDRAWAL:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->CARD_PAYMENT:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->CARD_PROCESSING_SALES:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->CARD_PROCESSING_ADJUSTMENTS:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result v1

    const/4 v5, 0x4

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->DEBIT_CARD_TRANSFER_IN:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result v1

    const/4 v6, 0x5

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->INSTANT_TRANSFER_OUT:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result v1

    const/4 v7, 0x6

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->STANDARD_TRANSFER_OUT:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result v1

    const/4 v8, 0x7

    aput v8, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->PROMOTIONAL_REWARD:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result v1

    const/16 v9, 0x8

    aput v9, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->PAYROLL:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result v1

    const/16 v10, 0x9

    aput v10, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->PAYROLL_CANCELLATION:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result v1

    const/16 v11, 0xa

    aput v11, v0, v1

    invoke-static {}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->values()[Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->CARD_PAYMENT:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    invoke-static {}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->values()[Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->DO_NOT_USE:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->COMMUNITY_REWARD:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->ATM_WITHDRAWAL_FEE:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->INSTANT_TRANSFER_FEE:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->CASHBACK:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->ordinal()I

    move-result v1

    aput v6, v0, v1

    invoke-static {}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->values()[Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->CARD_PROCESSING_ADJUSTMENTS:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    invoke-static {}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->values()[Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->DECLINED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->DISPUTE_REVERSED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->DISPUTE_WON:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->DISPUTED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->SETTLED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->ordinal()I

    move-result v1

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->REVERSED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->ordinal()I

    move-result v1

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->VOIDED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->ordinal()I

    move-result v1

    aput v8, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->REFUNDED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->ordinal()I

    move-result v1

    aput v9, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->PENDING:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->ordinal()I

    move-result v1

    aput v10, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->IN_REVIEW:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->ordinal()I

    move-result v1

    aput v11, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->IN_REVERSAL:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->WAITING_ON_EXTERNAL:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1

    return-void
.end method
