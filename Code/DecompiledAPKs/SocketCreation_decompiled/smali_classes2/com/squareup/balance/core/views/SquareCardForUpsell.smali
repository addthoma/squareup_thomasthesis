.class public final Lcom/squareup/balance/core/views/SquareCardForUpsell;
.super Landroid/widget/FrameLayout;
.source "SquareCardForUpsell.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardForUpsell.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardForUpsell.kt\ncom/squareup/balance/core/views/SquareCardForUpsell\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,49:1\n1103#2,7:50\n*E\n*S KotlinDebug\n*F\n+ 1 SquareCardForUpsell.kt\ncom/squareup/balance/core/views/SquareCardForUpsell\n*L\n46#1,7:50\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0014\u0010\u000b\u001a\u00020\u000c2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000eR\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/balance/core/views/SquareCardForUpsell;",
        "Landroid/widget/FrameLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "actionButton",
        "Lcom/squareup/noho/NohoButton;",
        "onPrimaryActionClickListener",
        "",
        "listener",
        "Lkotlin/Function0;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionButton:Lcom/squareup/noho/NohoButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/core/views/SquareCardForUpsell;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/core/views/SquareCardForUpsell;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    sget p2, Lcom/squareup/balance/core/R$layout;->square_card_for_upsell:I

    move-object p3, p0

    check-cast p3, Landroid/view/ViewGroup;

    invoke-static {p1, p2, p3}, Landroid/widget/FrameLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 37
    sget p1, Lcom/squareup/balance/core/R$id;->square_card_upsell_preview:I

    invoke-virtual {p0, p1}, Lcom/squareup/balance/core/views/SquareCardForUpsell;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    .line 38
    invoke-virtual {p1}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget p3, Lcom/squareup/balance/core/R$string;->square_card_upsell_sample_name:I

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->setName(Ljava/lang/String;)V

    .line 39
    sget p2, Lcom/squareup/balance/core/R$drawable;->square_card_sample_signature:I

    invoke-virtual {p1, p2}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->setSignature(I)V

    .line 42
    sget p1, Lcom/squareup/balance/core/R$id;->square_card_primary_action_button:I

    invoke-virtual {p0, p1}, Lcom/squareup/balance/core/views/SquareCardForUpsell;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.square\u2026rd_primary_action_button)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/balance/core/views/SquareCardForUpsell;->actionButton:Lcom/squareup/noho/NohoButton;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 28
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 29
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/balance/core/views/SquareCardForUpsell;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public final onPrimaryActionClickListener(Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/balance/core/views/SquareCardForUpsell;->actionButton:Lcom/squareup/noho/NohoButton;

    check-cast v0, Landroid/view/View;

    .line 50
    new-instance v1, Lcom/squareup/balance/core/views/SquareCardForUpsell$onPrimaryActionClickListener$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/balance/core/views/SquareCardForUpsell$onPrimaryActionClickListener$$inlined$onClickDebounced$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
