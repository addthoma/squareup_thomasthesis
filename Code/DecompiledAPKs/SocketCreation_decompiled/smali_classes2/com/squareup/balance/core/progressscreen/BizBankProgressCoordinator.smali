.class public final Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "BizBankProgressCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBizBankProgressCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BizBankProgressCoordinator.kt\ncom/squareup/balance/core/progressscreen/BizBankProgressCoordinator\n+ 2 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n*L\n1#1,141:1\n151#2,2:142\n*E\n*S KotlinDebug\n*F\n+ 1 BizBankProgressCoordinator.kt\ncom/squareup/balance/core/progressscreen/BizBankProgressCoordinator\n*L\n110#1,2:142\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B)\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0010H\u0016J\u0010\u0010\u0014\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0010H\u0002J\u001c\u0010\u0015\u001a\u00020\u00122\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u00120\u0017H\u0002J\u0010\u0010\u0019\u001a\u00020\u00122\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0018\u0010\u001c\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u001d\u001a\u00020\u0005H\u0002J\u0010\u0010\u001e\u001a\u00020\u00122\u0006\u0010\u001f\u001a\u00020 H\u0002J\u0010\u0010!\u001a\u00020\u00122\u0006\u0010\"\u001a\u00020#H\u0002J$\u0010$\u001a\u00020\u00122\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u00120\u00172\u0006\u0010%\u001a\u00020&H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "loadingText",
        "Landroid/widget/TextView;",
        "resultsMessage",
        "Lcom/squareup/noho/NohoMessageView;",
        "spinnerHolder",
        "Landroid/view/View;",
        "attach",
        "",
        "view",
        "bindViews",
        "handleBack",
        "onEvent",
        "Lkotlin/Function1;",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
        "toggleLoadingState",
        "isLoading",
        "",
        "update",
        "screen",
        "updateFailed",
        "failed",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;",
        "updateLoading",
        "loading",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;",
        "updateSucceeded",
        "succeeded",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private loadingText:Landroid/widget/TextView;

.field private resultsMessage:Lcom/squareup/noho/NohoMessageView;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private spinnerHolder:Landroid/view/View;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$handleBack(Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->handleBack(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;Landroid/view/View;Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->update(Landroid/view/View;Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 135
    sget-object v0, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 136
    sget v0, Lcom/squareup/balance/core/R$id;->square_card_progress_spinner_holder:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->spinnerHolder:Landroid/view/View;

    .line 137
    sget v0, Lcom/squareup/balance/core/R$id;->square_card_progress_spinner_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->loadingText:Landroid/widget/TextView;

    .line 138
    sget v0, Lcom/squareup/balance/core/R$id;->square_card_progress_result:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoMessageView;

    iput-object p1, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    return-void
.end method

.method private final handleBack(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 131
    sget-object v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event$GoBack;->INSTANCE:Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event$GoBack;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private final toggleLoadingState(Z)V
    .locals 2

    .line 126
    iget-object v0, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->spinnerHolder:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "spinnerHolder"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 127
    iget-object v0, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_1

    const-string v1, "resultsMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    xor-int/lit8 p1, p1, 0x1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)V
    .locals 4

    .line 55
    invoke-virtual {p2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    .line 56
    new-instance v1, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator$update$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator$update$1;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 57
    iget-object p1, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    const-string v1, "resultsMessage"

    if-nez p1, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 58
    :cond_0
    new-instance v2, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator$update$2;

    invoke-direct {v2, v0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator$update$2;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v2

    const-string v3, "debounceRunnable { onEvent(PrimaryAction) }"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-virtual {p1, v2}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 59
    iget-object p1, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 60
    :cond_1
    new-instance v1, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator$update$3;

    invoke-direct {v1, v0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator$update$3;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    const-string v2, "debounceRunnable { onEvent(SecondaryAction) }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 62
    invoke-virtual {p2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;->getData()Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;

    move-result-object p1

    .line 63
    instance-of p2, p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;

    if-eqz p2, :cond_2

    check-cast p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;

    invoke-direct {p0, p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->updateLoading(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;)V

    goto :goto_0

    .line 64
    :cond_2
    instance-of p2, p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;

    if-eqz p2, :cond_3

    check-cast p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;

    invoke-direct {p0, p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->updateFailed(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;)V

    goto :goto_0

    .line 65
    :cond_3
    instance-of p2, p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;

    if-eqz p2, :cond_4

    check-cast p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;

    invoke-direct {p0, v0, p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->updateSucceeded(Lkotlin/jvm/functions/Function1;Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;)V

    :cond_4
    :goto_0
    return-void
.end method

.method private final updateFailed(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;)V
    .locals 5

    const/4 v0, 0x0

    .line 76
    invoke-direct {p0, v0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->toggleLoadingState(Z)V

    .line 78
    iget-object v0, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    .line 80
    iget-object v0, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    const-string v1, "resultsMessage"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/noho/NohoMessageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 81
    iget-object v2, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v2, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget v3, Lcom/squareup/vectoricons/R$drawable;->circle_alert_96:I

    invoke-virtual {v2, v3}, Lcom/squareup/noho/NohoMessageView;->setDrawable(I)V

    .line 82
    iget-object v2, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v2, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;->getTitle()Lcom/squareup/util/ViewString;

    move-result-object v3

    const-string v4, "resources"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/noho/NohoMessageView;->setTitle(Ljava/lang/CharSequence;)V

    .line 83
    iget-object v2, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v2, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;->getMessage()Lcom/squareup/util/ViewString;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoMessageView;->setMessage(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;->getPrimaryButtonText()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonText(I)V

    .line 86
    iget-object v0, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;->getSecondaryButtonText()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonText(I)V

    return-void
.end method

.method private final updateLoading(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;)V
    .locals 2

    const/4 v0, 0x1

    .line 70
    invoke-direct {p0, v0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->toggleLoadingState(Z)V

    .line 71
    iget-object v0, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    .line 72
    iget-object v0, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->loadingText:Landroid/widget/TextView;

    if-nez v0, :cond_1

    const-string v1, "loadingText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;->getMessage()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method private final updateSucceeded(Lkotlin/jvm/functions/Function1;Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 93
    invoke-direct {p0, v0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->toggleLoadingState(Z)V

    .line 95
    invoke-virtual {p2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;->getActionBarText()I

    move-result v1

    const/4 v2, -0x1

    const-string v3, "actionBar"

    if-eq v1, v2, :cond_2

    .line 99
    iget-object v1, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v1, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 96
    :cond_0
    new-instance v4, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v4}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 97
    new-instance v5, Lcom/squareup/util/ViewString$ResourceString;

    invoke-virtual {p2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;->getActionBarText()I

    move-result v6

    invoke-direct {v5, v6}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v5, Lcom/squareup/resources/TextModel;

    invoke-virtual {v4, v5}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v4

    .line 98
    sget-object v5, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v6, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator$updateSucceeded$1;

    invoke-direct {v6, p0, p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator$updateSucceeded$1;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;Lkotlin/jvm/functions/Function1;)V

    check-cast v6, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v4, v5, v6}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 99
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 100
    iget-object p1, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez p1, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    goto :goto_0

    .line 102
    :cond_2
    iget-object p1, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez p1, :cond_3

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    .line 105
    :goto_0
    iget-object p1, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    const-string v0, "resultsMessage"

    if-nez p1, :cond_4

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    sget v1, Lcom/squareup/vectoricons/R$drawable;->circle_check_96:I

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoMessageView;->setDrawable(I)V

    .line 106
    iget-object p1, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_5

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;->getTitle()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoMessageView;->setTitle(I)V

    .line 107
    invoke-virtual {p2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;->getMessage()I

    move-result p1

    if-eq p1, v2, :cond_9

    .line 109
    iget-object p1, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_6

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p1}, Lcom/squareup/noho/NohoMessageView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;->getMessage()I

    move-result v1

    invoke-static {p1, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 111
    invoke-virtual {p2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;->getMessageArgs()Ljava/util/Map;

    move-result-object v1

    .line 142
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 112
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    goto :goto_1

    .line 115
    :cond_7
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 117
    iget-object v1, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v1, :cond_8

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {v1, p1}, Lcom/squareup/noho/NohoMessageView;->setMessage(Ljava/lang/CharSequence;)V

    .line 120
    :cond_9
    iget-object p1, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_a

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {p2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;->getPrimaryButtonText()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonText(I)V

    .line 121
    iget-object p1, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_b

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {p2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;->getSecondaryButtonText()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonText(I)V

    .line 122
    iget-object p1, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->resultsMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_c

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    invoke-virtual {p2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;->getHelpText()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoMessageView;->setHelp(I)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0, p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->bindViews(Landroid/view/View;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator$attach$1;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { upda\u2026iew, it.unwrapV2Screen) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
